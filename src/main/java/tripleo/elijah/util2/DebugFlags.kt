package tripleo.elijah.util2

object DebugFlags {
    const val MANUAL_DISABLED: Boolean = false
    const val _DefaultCompilationBus: Boolean = true
    const val CLOJURE_FLAG: Boolean = false
    const val NEVER_REACHED_BY_IDE: Boolean = false
    var Lawabidingcitizen_disabled: Boolean = true
    var CIS_ocp_ci__FeatureGate: Boolean = true
    var writeDumps: Boolean = false
    var CCI_gate: Boolean = false
    @JvmField
    var MakeSense: Boolean = true
    var _pancake_lcm_gate: Boolean = true

    /**
     * DONT change this
     */
    const val FORCE: Boolean = true

    /**
     * for you, Luke...
     */
    const val FORCE_IGNORE: Boolean = false
}
