package tripleo.elijah.comp

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.g.GPipelineMember
import tripleo.elijah.stages.hooligan.pipeline_impl.LawabidingcitizenPipelineImpl

class LawabidingcitizenPipeline @Contract(pure = true) constructor(pa0: GPipelineAccess) : PipelineMember(), GPipelineMember {
    private val pa = pa0 as IPipelineAccess
    private val i = LawabidingcitizenPipelineImpl()

    override fun run(aSt: CR_State, aOutput: CB_Output?) {
        try {
            val compilation = pa.compilation
            i.run(compilation)
        } catch (t: Throwable) {
            t.printStackTrace()
            var y = 2
            y = y
        }
    }

    override fun finishPipeline_asString(): String {
        return this.javaClass.toString()
    }
}
