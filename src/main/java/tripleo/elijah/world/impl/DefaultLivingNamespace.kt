package tripleo.elijah.world.impl

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.NamespaceStatement
import tripleo.elijah.stages.d.Stages
import tripleo.elijah.stages.garish.GarishNamespace
import tripleo.elijah.stages.garish.GarishNamespace_Generator
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.util2.__Extensionable
import tripleo.elijah.world.i.LivingCreatorSpec
import tripleo.elijah.world.i.LivingNamespace
import java.util.*
import java.util.function.Function

class DefaultLivingNamespace @Contract(pure = true) constructor(private val node: EvaNamespace) : __Extensionable(), LivingNamespace {
    private var _garish: GarishNamespace? = null
    private var _code = 0
    private var _generatedFlag = false

    override fun evaNode(): EvaNamespace {
        return node
    }

    override fun getCode(): Int {
        return _code
    }

    override fun setCode(aCode: Int) {
        _code = aCode
    }

    override fun getElement(): NamespaceStatement {
        return node.element as NamespaceStatement
    }

    override fun getGarish(): GarishNamespace {
        if (_garish == null) {
            _garish = GarishNamespace(this)
        }
        return _garish!!
    }

    override fun generateWith(aResultSink: GenerateResultSink, aGarishNamespace: GarishNamespace, aGr: GenerateResult, aGenerateFiles: GenerateFiles) {
        if (!_generatedFlag) {
            val generateC = aGenerateFiles as GenerateC
            val xg = GarishNamespace_Generator(evaNode())
            xg.provide(aResultSink, aGarishNamespace, aGr, generateC)

            _generatedFlag = true
        }
    }

    override fun <T : Any> getForStage(stg: Stages): Optional<T> {
        // TODO Auto-generated method stub
        val r = getExt(stg.javaClass)
        return if (r == null) Optional.empty()
        else Optional.of(r as T) // FIXME 12/09 this too // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
    }

    override fun <T> getForStage(stg: Stages, factory: Function<LivingCreatorSpec, T>): T {
        when (stg) {
            Stages.GARISH -> {
                val r = GarishNamespace(this)
                putExt(stg.javaClass, r)
                return r as T // FIXME 12/09 ugh // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
            }

            else -> throw IllegalArgumentException("Unexpected value: $stg")
        }
    }
}
