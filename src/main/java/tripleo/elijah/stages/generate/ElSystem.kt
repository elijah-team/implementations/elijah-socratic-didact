/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.generate

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Compilation
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.stages.gen_generic.GenerateResultItem
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.text.MessageFormat
import java.util.function.Supplier

/**
 * Created 1/8/21 11:02 PM
 */
class ElSystem(private val verbose: Boolean,
               private val c: Compilation,
               @JvmField val outputStrategyCreator: Supplier<OutputStrategy>) {
    private val gfm_map: MutableMap<EvaFunction, String?> = HashMap()
    private val _os: OutputStrategyC? = null

    fun getFilenameForNode(node: EvaNode,
                           ty: TY,
                           outputStrategyC: OutputStrategyC): String? {
        val s: String?

        if (node is EvaNamespace) {
            s = outputStrategyC.nameForNamespace(node, ty)

            logProgress(41, node, s)

            for (gf in node.functionMap.values) {
                val ss = getFilenameForNode(gf, ty, outputStrategyC)
                gfm_map[gf] = ss
            }
        } else if (node is EvaClass) {
            s = outputStrategyC.nameForClass(node, ty)

            logProgress(48, node, s)

            for (gf in node.functionMap.values) {
                val ss = getFilenameForNode(gf, ty, outputStrategyC)
                gfm_map[gf] = ss
            }
        } else if (node is EvaFunction) {
            s = outputStrategyC.nameForFunction(node, ty)

            logProgress(30, node, s)
        } else if (node is IEvaConstructor) {
            s = outputStrategyC.nameForConstructor(node, ty)

            logProgress(55, node, s)
            // throw new IllegalStateException("Unexpected value: " + node);
        } else {
            logProgress(140, null, null)

            throw IllegalStateException("Can't be here.")
        }

        return s
    }

    fun getFilenameForNode__(aGenerateResultItem: GenerateResultItem): String? {
        return getFilenameForNode(aGenerateResultItem.node(), aGenerateResultItem.__ty(), _os!!)
    }

    @Contract(pure = true)
    private fun logProgress(code: Int, evaNode: EvaNode?, s: String?) {
        // code:
        // 41: EvaNamespace
        // 48: EvaClass
        // 30: EvaFunction
        // 55: EvaConstructor
        // 140: not above
        SimplePrintLoggerToRemoveSoon.println_out_2(MessageFormat.format("{0} {1} {2}", code, evaNode.toString(), s))
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

