package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.elijah.g.GEvaClass
import tripleo.elijah.g.GEvaNamespace
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.stages.gen_c.C2C_Result
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_generic.GRS_Addable
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah.world.i.LivingClass
import tripleo.elijah.world.i.LivingNamespace

/**
 * All methods implemented here should throw `UnintendedUseException`
 */
abstract class DeadGenerateResultSink : GenerateResultSink {
    override fun add(node: GRS_Addable) {
        throw UnintendedUseException()
    }

    override fun addFunction(aGf: BaseEvaFunction, aRs: List<C2C_Result>, aGenerateFiles: GenerateFiles) {
        throw UnintendedUseException()
    }

    override fun additional(aGenerateResult: GenerateResult) {
        throw UnintendedUseException()
    }

    override fun getLivingClassForEva(aEvaClass: GEvaClass): LivingClass? {
        throw UnintendedUseException()
    }

    override fun getLivingNamespaceForEva(aEvaClass: GEvaNamespace): LivingNamespace? {
        throw UnintendedUseException()
    }

    override fun pa(): GPipelineAccess {
        throw UnintendedUseException()
    }
}
