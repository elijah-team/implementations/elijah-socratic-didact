package tripleo.elijah.stages.gen_c

import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.util.BufferTabbedOutputStream

internal class C2C_CodeForConstructor_Statement(private val class_name: String, private val class_code: Int, private val constructorName: String,
                                                private val decl: CClassDecl, private val x: EvaClass) : EG_Statement {

    override val explanation: EX_Explanation
        get() = withMessage("C2C_CodeForConstructor_Statement")

    override val text: String
        get() {
            val tos = BufferTabbedOutputStream()

            getTextInto(tos)

            return tos.toString()
        }

    fun getTextInto(tos: BufferTabbedOutputStream) {
        tos.put_string_ln(String.format("%s* ZC%d%s() {", class_name, class_code, constructorName))
        tos.incr_tabs()
        tos.put_string_ln(String.format("%s* R = GC_malloc(sizeof(%s));", class_name, class_name))
        tos.put_string_ln(String.format("R->_tag = %d;", class_code))
        if (decl.prim) {
            // TODO consider NULL, and floats and longs, etc
            if (decl.prim_decl != "bool") tos.put_string_ln("R->vsv = 0;")
            else if (decl.prim_decl == "bool") tos.put_string_ln("R->vsv = false;")
        } else {
            for (o in x.varTable) {
//					final String typeName = getTypeNameForVarTableEntry(o);
                // TODO this should be the result of getDefaultValue for each type
                tos.put_string_ln(String.format("R->vm%s = 0;", o.nameToken))
            }
        }

        tos.dec_tabs()
    }
}
