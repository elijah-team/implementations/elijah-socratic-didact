package tripleo.elijah.comp.process

import tripleo.elijah.comp.i.CompilerDriven
import tripleo.elijah.comp.i.CompilerDriver
import tripleo.elijah.comp.i.DriverToken
import tripleo.elijah.comp.i.ICompilationBus
import tripleo.elijah.comp.internal.CompilationImpl
import tripleo.elijah.util.Operation

class DefaultCompilerDriver(@field:Suppress("unused") private val cb: ICompilationBus) : CompilerDriver {
    private val defaults: MutableMap<DriverToken, CompilerDriven> = HashMap()
    private val drivens: Map<DriverToken, CompilerDriven> = HashMap()

    // dont like this one
    private /* static */ var initialized = false

    init {
        if (!initialized) {
            defaults[CompilationImpl.CompilationAlways.Tokens.COMPILATION_RUNNER_START] = CD_CompilationRunnerStart_1()
            defaults[CompilationImpl.CompilationAlways.Tokens.COMPILATION_RUNNER_FIND_STDLIB2] = CD_FindStdLibImpl()
            initialized = true
        } else assert(false)
    }

    override fun get(aToken: DriverToken): Operation<CompilerDriven> {
        val o: Operation<CompilerDriven>

        if (drivens.containsKey(aToken)) {
            o = Operation.success(drivens[aToken])
            return o
        }

        if (defaults.containsKey(aToken)) {
            val x = defaults[aToken]
            o = Operation.success(x)
        } else {
            o = Operation.failure(Exception("Compiler Driven get failure"))
        }

        return o
    }
}
