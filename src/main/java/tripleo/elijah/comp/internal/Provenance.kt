package tripleo.elijah.comp.internal

enum class Provenance(private val p: Int) {
    EvaPipeline__lgc_slot(117),
    PipelineLogic__nextModule(7059),
    DefaultCompilationAccess__writeLogs(92)
}
