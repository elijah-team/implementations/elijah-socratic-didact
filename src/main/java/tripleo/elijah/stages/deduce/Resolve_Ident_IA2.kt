/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.lang.types.OS_UnknownType
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.DeduceUtils.MatchFunctionArgs
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.Mode
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util2.ReadySupplier_1
import tripleo.elijah.util2.UnintendedUseException
import java.util.function.Predicate

/**
 * Created 7/21/21 7:33 PM
 */
internal class Resolve_Ident_IA2(private val deduceTypes2: DeduceTypes2,
                                 private val errSink: ErrSink,
                                 private val phase: DeducePhase,
                                 private val generatedFunction: BaseEvaFunction,
                                 private val foundElement: FoundElement) {
    inner class RIA_Clear_98(private val idte: IdentTableEntry, private val vs: VariableStatement, private val ctx: Context) {
        fun run() {
            try {
                val has_initial_value = vs.initialValue() !== LangGlobals.UNASSIGNED
                if (!vs.typeName().isNull) {
                    typeName_is_not_null(has_initial_value)
                } else if (has_initial_value) {
                    typeName_is_null_with_initial_value()
                } else {
                    LOG.err("1936 Empty Variable Expression")
                    throw IllegalStateException("1936 Empty Variable Expression")
                    // return; // TODO call noFoundElement, raise exception
                }
            } catch (aResolveError: ResolveError) {
                LOG.err("1937 resolve error " + vs.name)
                // aResolveError.printStackTrace();
                errSink.reportDiagnostic(aResolveError)
            }
        }

        @Throws(ResolveError::class)
        private fun typeName_is_not_null(has_initial_value: Boolean) {
            // 1.lookup initial value if we have it or create GenType from typeName
            val attached: GenType?
            if (has_initial_value) {
                attached = DeduceLookupUtils.deduceExpression(deduceTypes2, vs.initialValue(), ctx)
            } else { // if (vs.typeName() != null) {
                attached = _inj().new_GenTypeImpl()
                attached.set(_inj().new_OS_UserType(vs.typeName()))
            }

            val initialValue = if (has_initial_value) {
                vs.initialValue()
            } else {
                null // README presumably there is none, ie when generated
            }

            // 2. get OS_Type
            val resolvedOrTypename = if (attached!!.resolved != null) attached.resolved
            else attached.typeName

            // 3.create tte
            val tte = generatedFunction.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, resolvedOrTypename,
                    initialValue)

            // 4. commit tte
            idte.type = tte
        }

        @Throws(ResolveError::class)
        private fun typeName_is_null_with_initial_value() {
            // 1. lookup initial value
            val attached = DeduceLookupUtils.deduceExpression(deduceTypes2, vs.initialValue(), ctx)

            // 2. get OS_Type
            val resolvedOrTypename = if (attached!!.resolved != null) attached.resolved
            else attached.typeName

            // 3. create tte
            val tte = generatedFunction.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT,
                    resolvedOrTypename, vs.initialValue())

            // 4. commit tte (idte.type)
            idte.type = tte
        }
    }

    internal enum class RIA_STATE {
        CONTINUE, NEXT, RETURN
    }

    //
    private val LOG = deduceTypes2.LOG
    var ectx: Context? = null

    var el: OS_Element? = null

    private fun _inj(): DeduceTypes2Injector {
        return deduceTypes2._inj()
    }

    /* @requires idte2.type.getAttached() != null; */
    private fun findResolved(idte2: IdentTableEntry, ctx: Context): Boolean {
        var ctx: Context? = ctx
        try {
            if (idte2.type?.attached is OS_UnknownType) // TODO ??
                return false

            val attached = idte2.type?.attached
            if (attached!!.type == OS_Type.Type.USER_CLASS) {
                if (idte2.type?.genType?.resolved == null) {
                    val rtype = deduceTypes2.resolve_type(attached, ctx)
                    if (rtype.resolved != null) {
                        when (rtype.resolved.type) {
                            OS_Type.Type.USER_CLASS -> ctx = rtype.resolved.classOf.context
                            OS_Type.Type.FUNCTION -> ctx = rtype.resolved.element.context
                            else -> {throw UnintendedUseException("fndsjklfnasjlfknsajkl")}
                        }
                        idte2.type?.setAttached(rtype) // TODO may be losing alias information here
                    }
                }
            }
        } catch (resolveError: ResolveError) {
            if (resolveError.resultsList().size > 1) errSink.reportDiagnostic(resolveError)
            else LOG.info("1089 Can't attach type to " + idte2.type?.attached)
            //				resolveError.printStackTrace(); // TODO print diagnostic
            return true
        }
        return false
    }

    private fun ia2_IdentIA(ia2: IdentIA, ectx: Context): RIA_STATE {
        val idte2 = ia2.entry
        val text = idte2.ident.get()

        if (idte2.status == BaseTableEntry.Status.KNOWN) {
            el = idte2.resolvedElement
            // assert el != null;
            if (el == null) {
                if (idte2._ident != null) {
                    val ident = idte2._ident!!

                    if (ident.node != null) {
                        val evaFunction = ident.node
                        el = evaFunction.fd
                    }
                }
            }
        } else {
            val lrl = text.lookup(ectx)
            el = lrl.chooseBest(null)

            if (el == null) {
                errSink.reportDiagnostic(deduceTypes2._inj().new_ResolveError(idte2.ident.get(), lrl))
                //				errSink.reportError("1007 Can't resolve " + text);
                foundElement.doNoFoundElement()
                return RIA_STATE.RETURN
            }
        }

        if (!idte2.hasResolvedElement()) {
            idte2.setStatus(BaseTableEntry.Status.KNOWN, deduceTypes2._inj().new_GenericElementHolder(el))
        }
        if (idte2.type == null) {
            if (el is VariableStatement) {
                val el1=el as VariableStatement
                ia2_IdentIA_VariableStatement(idte2, el1, ectx)
            } else if (el is FunctionDef) {
                ia2_IdentIA_FunctionDef(idte2)
            }
        }
        if (idte2.type != null) {
            // assert idte2.type.getAttached() != null;
            if (idte2.type?.attached == null) {
                return RIA_STATE.CONTINUE // FIXME/TODO 06/19
            }
            if (findResolved(idte2, ectx)) return RIA_STATE.CONTINUE
        } else {
//			throw new IllegalStateException("who knows");
            errSink.reportWarning("2010 idte2.type == null for $text")
        }

        return RIA_STATE.NEXT
    }

    private fun ia2_IdentIA_FunctionDef(idte2: IdentTableEntry) {
        val attached = _inj().new_OS_UnknownType(el)
        val tte = generatedFunction.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, attached, null, idte2)
        idte2.type = tte

        // Set type to something other than Unknown when found
        val pte = idte2.callablePTE
        if (pte == null) {
            assert(false)
        } else {
//			assert pte != null;
            var fi = pte.functionInvocation
            if (fi == null) {
                val bl = idte2.backlink
                var invocation: IInvocation? = null
                if (bl is IntegerIA) {
                    val vte: VariableTableEntry = bl.entry
                    if (vte.constructable_pte != null) {
                        val cpte = vte.constructable_pte
                        invocation = cpte?.functionInvocation?.classInvocation
                    } else if (vte.typeDeferred_isResolved()) {
                        val ainvocation = arrayOfNulls<IInvocation>(1)
                        vte.typePromise().then { result ->
                            if (result.ci == null) {
                                result.genCIForGenType2(deduceTypes2)
                            }
                            ainvocation[0] = result.ci
                        }
                        invocation = ainvocation[0]
                    }
                } else if (bl is IdentIA) {
                    val ite: IdentTableEntry = bl.entry
                    if (ite.type?.genType?.ci != null) invocation = ite.type?.genType?.ci
                    else if (ite.type?.attached != null) {
                        val attached1 = ite.type!!.attached!!
                        assert(attached1.type == OS_Type.Type.USER_CLASS)
                        invocation = phase.registerClassInvocation(attached1.classOf, null,
                                ReadySupplier_1(deduceTypes2)) // TODO will fail one day
                        // TODO dont know if next line is right
                        val oi = DeduceTypes2.ClassInvocationMake.withGenericPart(
                                attached1.classOf, null, tte.genType.nonGenericTypeName as NormalTypeName,
                                deduceTypes2)
                        assert(oi.mode() == Mode.SUCCESS)
                        val invocation2 = oi.success()
                        val y = 2
                    }
                } else if (bl is ProcIA) {
                    val bl_fi: FunctionInvocation = bl.entry.functionInvocation!!
                    if (bl_fi.classInvocation != null) {
                        invocation = bl_fi.classInvocation
                    } else if (bl_fi.namespaceInvocation != null) {
                        invocation = bl_fi.namespaceInvocation
                    }
                }

                if (invocation == null) {
                    val y = 2
                    // assert false;
                }
                fi = phase.newFunctionInvocation(el as FunctionDef?, pte, invocation!!)
                pte.functionInvocation = fi
            }

            pte.functionInvocation?.generatePromise()?.then { result ->
                result.typePromise().then { result -> // NOTE there is no Promise-type notification for when type changes
                    idte2.type?.setAttached(result)
                }
            }
        }
    }

    private fun ia2_IdentIA_VariableStatement(idte: IdentTableEntry,
                                              vs: VariableStatement,
                                              ctx: Context) {
        val clr = deduceTypes2._inj().new_RIA_Clear_98(idte, vs, ctx, this)
        clr.run()
    }

    private fun ia2_IntegerIA(ia2: IntegerIA, ctx: Context) {
        val vte = generatedFunction.getVarTableEntry(DeduceTypes2.to_int(ia2))
        val text = vte.name

        run {
            val pot = deduceTypes2.getPotentialTypesVte(vte)
            if (pot.size == 1) {
                val attached = pot[0].attached
                if (attached == null) {
                    ia2_IntegerIA_null_attached(ctx, pot)
                } else {
                    // TODO what is the state of vte.genType here?
                    ectx = when (attached.type) {
                        OS_Type.Type.USER_CLASS -> attached.classOf.context // TODO can combine later
                        OS_Type.Type.FUNCTION -> attached.element.context
                        OS_Type.Type.USER -> attached.typeName.context
                        else -> {
                            LOG.err("1098 " + attached.type)
                            throw IllegalStateException("Can't be here.")
                        }
                    }
                }
            }
        }

        val attached = vte.typeTableEntry.attached
        if (attached != null) {
            when (attached.type) {
                OS_Type.Type.USER_CLASS -> ectx = attached.classOf.context
                OS_Type.Type.FUNCTION -> ectx = attached.element.context
                OS_Type.Type.USER -> try {
                    val ty = deduceTypes2.resolve_type(attached, ctx)
                    ectx = ty.resolved.classOf.context
                } catch (resolveError: ResolveError) {
                    LOG.err("1300 Can't resolve $attached")
                    resolveError.printStackTrace()
                }

                else -> throw IllegalStateException("Unexpected value: " + attached.type)
            }
        } else {
            if (vte.potentialTypes().size == 1) {
                ia2_IntegerIA_potentialTypes_equals_1(vte, text)
            }
        }
    }

    /* @requires pot.get(0).getAttached() == null; */
    private fun ia2_IntegerIA_null_attached(ctx: Context, pot: List<TypeTableEntry>) {
        try {
            var fd: FunctionDef? = null
            var pte: ProcTableEntry? = null
            val xx = pot[0].tableEntry
            if (xx != null) {
                if (xx is ProcTableEntry) {
                    pte = xx
                    val xxx: InstructionArgument = xx.expression_num
                    if (xxx is IdentIA) {
                        val ite: IdentTableEntry = xxx.entry
                        val deducePath = ite.buildDeducePath(generatedFunction)
                        val el5 = deducePath.getElement(deducePath.size() - 1)
                        val y = 2
                        fd = el5 as FunctionDef?
                    }
                }
            } else {
                val lrl = DeduceLookupUtils.lookupExpression(pot[0].__debug_expression.left, ctx,
                        deduceTypes2)
                val best = lrl.chooseBest(Helpers.List_of<Predicate<OS_Element>>(
                        MatchFunctionArgs(pot[0].__debug_expression as ProcedureCallExpression)))
                if (best is FunctionDef) {
                    fd = best
                } else {
                    fd = null
                    LOG.err("1195 Can't find match")
                }
            }
            if (fd != null) {
                val invocation = deduceTypes2.getInvocation((generatedFunction as EvaFunction))
                val fd2 = fd
                deduceTypes2.forFunction(deduceTypes2.newFunctionInvocation(fd2, pte, invocation, phase),
                        object : ForFunction() {
                            override fun typeDecided(aType: GenType?) {
                                assert(fd2 === generatedFunction.getFD())
                                //
                                pot[0].attached = deduceTypes2.gt(aType!!)
                            }
                        })
            } else {
                errSink.reportError("1196 Can't find function")
            }
        } catch (aResolveError: ResolveError) {
            aResolveError.printStackTrace()
            val y = 2
            throw NotImplementedException()
        }
    }

    private fun ia2_IntegerIA_potentialTypes_equals_1(aVte: VariableTableEntry, aText: String) {
        var state = 0
        val pot = deduceTypes2.getPotentialTypesVte(aVte)
        val attached1 = pot[0].attached
        val te = pot[0].tableEntry
        if (te is ProcTableEntry) {
            // This is how it should be done, with an Incremental
            te.functionInvocation?.generateDeferred()!!
                    .then { result: BaseEvaFunction ->
                        result.typePromise().then { result1: GenType? ->
                            val y = 2
                            aVte.resolveType(result1!!) // save for later
                        }
                    }
            // but for now, just set ectx
            val en: InstructionArgument = te.expression_num
            if (en is IdentIA) {
                val ded: DeducePath = en.entry.buildDeducePath(generatedFunction)
                val el2 = ded.getElement(ded.size() - 1)
                if (el2 != null) {
                    state = 1
                    ectx = el2.context
                    aVte.typeTableEntry.attached = attached1
                }
            }
        }
        when (state) {
            0 -> {
                assert(attached1 != null)
                aVte.typeTableEntry.attached = attached1
                when (attached1!!.type) {
                    OS_Type.Type.USER -> {
                        val attached1TypeName = attached1.typeName
                        assert(attached1TypeName is RegularTypeName)
                        val realName = (attached1TypeName as RegularTypeName).realName
                        try {
                            val lrl = DeduceLookupUtils.lookupExpression(realName, ectx!!, deduceTypes2)
                                    .results()
                            ectx = lrl[0].element.context
                        } catch (aResolveError: ResolveError) {
                            aResolveError.printStackTrace()
                            val y = 2
                            throw NotImplementedException()
                        }
                    }

                    OS_Type.Type.USER_CLASS -> ectx = attached1.classOf.context
                    else -> {
                        val typeName = attached1.typeName
                        errSink.reportError("1442 Don't know " + typeName.javaClass.name)
                        throw NotImplementedException()
                    }
                }
            }

            1 -> {
            }

            else -> LOG.info("1006 Can't find type of $aText")
        }
    }

    fun resolveIdentIA2_(ctx: Context, identIA: IdentIA?,
                         s: List<InstructionArgument?>?) {
        var s = s
        el = null
        ectx = ctx

        assert(identIA != null || s != null)
        if (s == null) s = BaseEvaFunction._getIdentIAPathList(identIA!!)

        if (identIA != null) {
            val dp = identIA.entry.buildDeducePath(generatedFunction)
            val index = dp.size() - 1
            val ia2 = dp.getIA(index)
            // ia2 is not == equals to identIA, but functionally equivalent
            if (ia2 is IdentIA) {
                val ite = ia2.entry
                if (ite.backlink != null) {
                    val backlink = ite.backlink
                    if (backlink is IntegerIA) {
                        val vte: VariableTableEntry = backlink.entry
                        val pe = deduceTypes2.promiseExpectation<GenType>(vte,
                                "TypePromise for vte $vte")
                        vte.typePromise().then { result ->
                            pe.satisfy(result)
                            ectx = when (result.resolved.type) {
                                OS_Type.Type.FUNCTION -> result.resolved.element.context
                                OS_Type.Type.USER_CLASS -> result.resolved.classOf.context
                                else -> throw IllegalStateException(
                                        "Unexpected value: " + result.resolved.type)
                            }
                            ia2_IdentIA(ia2, ectx!!)
                            foundElement.doFoundElement(el!!)
                        }
                    } else if (backlink is IdentIA) {
                        dp.getElementPromise(index, { result: OS_Element? ->
                            el = result
                            ectx = result!!.context
                            ia2_IdentIA(ia2, ectx!!)
                            foundElement.doFoundElement(el!!)
                        }, { result: Diagnostic? -> foundElement.doNoFoundElement() })
                        dp.getElementPromise(index - 1, { result: OS_Element? ->
                            ia2_IdentIA(dp.getIA(index - 1) as IdentIA, result!!.context) // might fail
                        }, null)
                    }
                } else {
                    if (!ite.hasResolvedElement()) {
                        ia2_IdentIA(ia2, ectx!!)
                        foundElement.doFoundElement(el!!)
                    }
                }
            }
            //			el = dp.getElement(dp.size()-1);
        } else {
            for (ia2 in s) {
                if (ia2 is IntegerIA) {
                    ia2_IntegerIA(ia2, ectx!!)
                } else if (ia2 is IdentIA) {
                    val st = ia2_IdentIA(ia2, ectx!!)

                    when (st) {
                        RIA_STATE.CONTINUE -> continue
                        RIA_STATE.NEXT -> {}
                        RIA_STATE.RETURN -> return
                    }
                } else if (ia2 is ProcIA) {
                    LOG.err("1373 ProcIA")
                    //						@NotNull ProcTableEntry pte = ((ProcIA) ia2).getEntry(); // README ectx seems to be set up already
                    return
                } else throw NotImplementedException()
            }
            foundElement.doFoundElement(el!!)
        }
    }
}

fun IdentExpression.lookup(context: Context): LookupResultList {
    return context.lookup(this.text)
}
