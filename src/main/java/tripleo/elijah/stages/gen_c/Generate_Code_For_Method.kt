/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_c

import com.google.common.base.Supplier
import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.NormalTypeName
import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.lang.types.OS_UnitType
import tripleo.elijah.nextgen.outputstatement.EG_SingleStatement
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.nextgen.outputstatement.EX_Explanation_withMessage
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.nextgen.DR_Ident
import tripleo.elijah.stages.deduce.post_bytecode.GCFM_Diagnostic
import tripleo.elijah.stages.gen_c.GenerateC.GetTypeName
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.util.*
import tripleo.elijah.util2.DebugFlags

/**
 * Created 6/21/21 5:53 AM
 */
class Generate_Code_For_Method(private val gc: GenerateC, aLog: ElLog?) {
    val tos: BufferTabbedOutputStream = BufferTabbedOutputStream()
    val tosHdr: BufferTabbedOutputStream = BufferTabbedOutputStream()
    val LOG: ElLog = aLog!! // use log from GenerateC
    var is_constructor: Boolean = false
    var is_unit_type: Boolean = false

    private fun _action_DECL(instruction: Instruction,
                             yf: WhyNotGarish_BaseFunction): Operation2<EG_Statement> {
        val decl_type = instruction.getArg(0) as SymbolIA
        val vte_num = instruction.getArg(1) as IntegerIA

        val target = GCR_VTE_Target(yf, vte_num)
        target.feed(AOG.GET, gc)

        val target_name = target.target_name

        val vte = vte_num.entry

        var dt2: DeduceTypes2? = null
        var gf1: BaseEvaFunction? = null
        var qqq = false
        run {
            val x = yf.gf.drs
            if (!x.isEmpty()) {
                if (x[0] is DR_Ident) {
                    val ident = x[0] as DR_Ident

                    dt2 = ident.identTableEntry()?._deduceTypes2()
                    gf1 = ident.identTableEntry()?.__gf

                    qqq = true
                }
            }
        }

        val de_vte = if (qqq) {
            vte.getDeduceElement3(dt2, gf1)
        } else {
            vte.deduceElement3
        }

        val diag1 = de_vte.decl_test_001(yf.cheat())

        if (diag1.mode() == Mode.FAILURE) {
            val diag_ = diag1.failure()
            val diag = diag_ as GCFM_Diagnostic

            when (diag.severity()) {
                Diagnostic.Severity.INFO -> LOG.info(diag._message())
                Diagnostic.Severity.ERROR -> LOG.err(diag._message())
                Diagnostic.Severity.LINT, Diagnostic.Severity.WARN -> throw NotImplementedException()
                else -> throw NotImplementedException()
            }
            return Operation2.failure(diag_)
        }

        val res = vte.resolvedType()
        if (res is EvaClass) {
            val z: String = GetTypeName.Companion.forGenClass(res)
            val s = String.format("%s* %s;", z, target_name)
            return Operation2
                    .success(EG_SingleStatement(s, withMessage("actionDECL with resolved type")))
        }

        val x = diag1.success() // vte.type.getAttached();

        if (x != null) {
            when (x.type) {
                OS_Type.Type.USER_CLASS -> {
                    val z: String = GetTypeName.forOSType(x, LOG)
                    val s = String.format("%s* %s;", z, target_name)
                    return Operation2
                            .success(EG_SingleStatement(s, withMessage("actionDECL with USER_CLASS")))
                }

                OS_Type.Type.USER -> {
                    val typeName = x.typeName
                    if (typeName is NormalTypeName) {
                        val z2 = if (typeName.name == "Any") "void *" // TODO Technically this is wrong
                        else GetTypeName.forTypeName(typeName, gc.errSink)
                        val s1 = String.format("%s %s;", z2, target_name)
                        return Operation2
                                .success(EG_SingleStatement(s1, withMessage("actionDECL with USER")))
                    }

                    if (typeName != null) {
                        //
                        // VARIABLE WASN'T FULLY DEDUCED YET
                        //
                        return Operation2.failure(Diagnostic_8887(typeName))
                    }
                }

                OS_Type.Type.BUILT_IN -> return Operation2.success(actionDECL_with_BUILT_IN(yf, target_name, x, gc))
                OS_Type.Type.FUNC_EXPR -> return Operation2.success(object : EG_Statement {
                    override val explanation: EX_Explanation
                        get() = EX_Explanation_withMessage("gcfm:type:func_decl")

//                    override fun getText(): String = text

                    override val text: String
                        get() = "void (*fun)()"
                })

//                else -> {/*passthru*/}
                OS_Type.Type.ANY -> TODO()
                OS_Type.Type.FUNCTION -> TODO()
                OS_Type.Type.GENERIC_TYPENAME -> TODO()
                OS_Type.Type.UNIT_TYPE -> TODO()
                OS_Type.Type.UNKNOWN -> TODO()
                OS_Type.Type.USER_NAMESPACE -> TODO()
            }
        }

        //
        // VARIABLE WASN'T FULLY DEDUCED YET
        // MTL A TEMP VARIABLE
        //
        val pt_ = vte.potentialTypes()
        val pt: List<TypeTableEntry> = ArrayList(pt_)
        if (pt.size == 1) {
            val ty = pt[0]
            if (ty.genType.node != null) {
                val node1 = ty.genType.node
                if (node1 is EvaFunction) {
                    val node: WhyNotGarish_Function = gc.a_lookup(node1)!!

                    val y = node
                    //					((EvaFunction)node).typeDeferred()
                    // get signature
                    val z =  /* Emit.emit("/ *552* /") + */"bool (*%s)(char*)".formatted(target_name)
                    val s = String.format("/*8889*/%s;", z)
                    return Operation2.success(EG_SingleStatement(s, EX_Explanation_withMessage("Generate_Code_For_Method 8889")))
                }
            } else {
//				LOG.err("8885 " +ty.attached);
                val attached = ty.attached
                val z = if (attached != null) gc.getTypeName(attached)
                else Emit.emit("/*763*/") + "Unknown"
                val s = String.format("/*8890*/Z<%s> %s;", z, target_name)
                return Operation2.success(EG_SingleStatement(s, EX_Explanation_withMessage("Generate_Code_For_Method 8890")))
            }
        }

        return Operation2.failure(Diagnostic_8886())
    }

    private fun action_AGN(gf: WhyNotGarish_BaseFunction, aInstruction: Instruction) {
        val inst = GCFM_Inst_AGN(this, gc, gf, aInstruction)

        val s = inst.text

        tos.put_string_ln(s)
    }

    private fun action_AGNK(gf: WhyNotGarish_BaseFunction, aInstruction: Instruction) {
        val inst = GCFM_Inst_AGNK(this, gc, gf, aInstruction)

        val s = inst.text

        tos.put_string_ln(s)
    }

    private fun action_CALL(gf: WhyNotGarish_BaseFunction, aInstruction: Instruction) {
        // LOG.err("9000 "+inst.getName());

        val gcx_fc = GCX_FunctionCall(gf, gc, aInstruction)

        tos.put_string_ln(gcx_fc.text)
    }

    private fun action_CALLS(gf: WhyNotGarish_BaseFunction, aInstruction: Instruction) {
        val x = aInstruction.getArg(0)

        assert(x is ProcIA)
        val pte = gf.getProcTableEntry(DeduceTypes2.to_int(x!!))

        val gcx_fc = GCX_FunctionCall_Special(pte, gf, gc, aInstruction)

        tos.put_string_ln(gcx_fc.text)
    }

    private fun action_CAST(instruction: Instruction, tos: BufferTabbedOutputStream,
                            gf: WhyNotGarish_BaseFunction) {
        val vte_num_ = instruction.getArg(0) as IntegerIA
        val vte_type_ = instruction.getArg(1) as IntegerIA
        val vte_targ_ = instruction.getArg(2) as IntegerIA
        val target_name = gc.getRealTargetName(gf, vte_num_, AOG.GET)
        val target_type_ = gf.getTypeTableEntry(vte_type_.index)
        //		final String target_type = gc.getTypeName(target_type_.getAttached());
        val target_type = gc.getTypeName(target_type_.genType.node)
        val source_target = gc.getRealTargetName(gf, vte_targ_, AOG.GET)

        tos.put_string_ln(String.format("%s = (%s)%s;", target_name, target_type, source_target))
    }

    private fun action_CONSTRUCT(gf: WhyNotGarish_BaseFunction, aInstruction: Instruction) {
        val _arg0 = aInstruction.getArg(0)
        assert(_arg0 is ProcIA)
        val gi_proc = gc._repo.itemFor(_arg0 as ProcIA)

        val gcx_construct = GCX_Construct(gi_proc, aInstruction, gc)

        tos.put_string_ln(gcx_construct.text)
    }

    private fun action_DECL(instruction: Instruction, tos: BufferTabbedOutputStream,
                            gf: WhyNotGarish_BaseFunction) {
        val op = _action_DECL(instruction, gf)

        if (op.mode() == Mode.SUCCESS) {
            tos.put_string_ln(op.success().text)
        } else {
            // throw new
            // ignore
        }
    }

    private fun action_E(gf: WhyNotGarish_BaseFunction, aGmh: Generate_Method_Header) {
        tos.put_string_ln("bool vsb;")
        var state = 0

        if (gf.pointsToConstructor()) state = 2
        else if (aGmh.tte == null) state = 3
        else if (aGmh.tte!!.isResolved) state = 1
        else if (aGmh.tte!!.attached is OS_UnitType) state = 4

        when (state) {
            0 -> tos.put_string_ln("Error_TTE_Not_Resolved " + aGmh.tte)
            1 -> {
                val ty: String = GetTypeName.Companion.forTypeTableEntry(aGmh.tte!!)
                tos.put_string_ln(String.format("%s* vsr;", ty))
            }

            2 -> is_constructor = true
            3 -> {
                // TODO don't know what this is for now
                // Assuming ctor
                is_constructor = gf.pointsToConstructor2()
                val genClass = gf.genClass
                val ty2: String = GetTypeName.Companion.getTypeNameForEvaNode(genClass!!)

                val return_type1 = aGmh.__find_return_type(gf, gc.LOG)
                if (return_type1 != null) {
                    tos.put_string_ln(String.format("%s vsr;", return_type1))
                } else {
                    tos.put_string_ln(String.format("// *171* %s vsr;", ty2))
                }
            }

            4 ->            // don't print anything
                is_unit_type = true
        }
        tos.put_string_ln("{")
        tos.incr_tabs()
    }

    private fun action_ES() {
        tos.put_string_ln("{")
        tos.incr_tabs()
    }

    fun action_invariant(yf: WhyNotGarish_BaseFunction, aGmh: Generate_Method_Header) {
        tos.incr_tabs()
        //
        val instructions = yf.instructions()

        for (instruction_index in instructions.indices) {
            val instruction = instructions[instruction_index]
            //			LOG.err("8999 "+instruction);
            val label = yf.findLabel(instruction!!.index)
            if (label != null) {
                tos.put_string_ln_no_tabs(label.name + ":")
            }

            when (instruction.name) {
                InstructionName.E -> action_E(yf, aGmh)
                InstructionName.X -> action_X(aGmh)
                InstructionName.ES -> action_ES()
                InstructionName.XS -> action_XS()
                InstructionName.AGN -> action_AGN(yf, instruction)
                InstructionName.AGNK -> action_AGNK(yf, instruction)
                InstructionName.AGNT -> {}
                InstructionName.AGNF -> {}
                InstructionName.JE -> action_JE(yf, instruction)
                InstructionName.JNE -> action_JNE(yf, instruction)
                InstructionName.JL -> action_JL(yf, instruction)
                InstructionName.JMP -> action_JMP(instruction)
                InstructionName.CONSTRUCT -> action_CONSTRUCT(yf, instruction)
                InstructionName.CALL -> action_CALL(yf, instruction)
                InstructionName.CALLS -> action_CALLS(yf, instruction)
                InstructionName.RET -> {}
                InstructionName.YIELD -> throw NotImplementedException()
                InstructionName.TRY -> throw NotImplementedException()
                InstructionName.PC -> {}
                InstructionName.IS_A -> action_IS_A(instruction, tos, yf)
                InstructionName.DECL -> action_DECL(instruction, tos, yf)
                InstructionName.CAST_TO -> action_CAST(instruction, tos, yf)
                InstructionName.NOP -> {}
                else -> throw IllegalStateException("Unexpected value: " + instruction.name)
            }
        }
        tos.dec_tabs()
        tos.put_string_ln("}")
    }

    private fun action_IS_A(instruction: Instruction, tos: BufferTabbedOutputStream,
                            gf: WhyNotGarish_BaseFunction) {
        val testing_var_ = instruction.getArg(0) as IntegerIA
        val testing_type_ = instruction.getArg(1) as IntegerIA
        val target_label = (instruction.getArg(2) as LabelIA).label

        val testing_var = gf.getVarTableEntry(testing_var_.index)
        val testing_type__ = gf.getTypeTableEntry(testing_type_.index)

        val testing_type = testing_type__.resolved()
        val z = (testing_type as EvaContainerNC).code

        val bt = BT()

        bt.text0("vsb = ")

        // ZS%d_is_a(%s
        bt.text0("ZS")
        bt.text0("" + z)
        bt.text0("_is_a")

        bt.text("(")
        bt.text0(gc.getRealTargetName(gf, testing_var_, AOG.GET))
        bt.text(");")

        bt.text0("if (!vsb) goto ")
        bt.text0(target_label.name)
        bt.text(";")

        tos.put_string_ln(bt.text)
    }

    private fun action_JE(gf: WhyNotGarish_BaseFunction, aInstruction: Instruction) {
        val inst = GCFM_Inst_JE(this, gc, gf, aInstruction)

        val s = inst.text

        tos.put_string_ln(s)
    }

    private fun action_JL(gf: WhyNotGarish_BaseFunction, aInstruction: Instruction) {
        val lhs = aInstruction.getArg(0)
        val rhs = aInstruction.getArg(1)
        val target = aInstruction.getArg(2)

        val realTarget = target as Label

        val vte = gf.getVarTableEntry((lhs as IntegerIA).index)
        assert(rhs != null)
        val bt = BT()

        if (rhs is ConstTableIA) {
            val cte = gf.getConstTableEntry(rhs.index)

            bt.text0("vsb = ")
            bt.text0(gc.getRealTargetName(gf, lhs, AOG.GET))
            bt.text0(" < ")
            bt.text0(gc.getAssignmentValue(gf.self, rhs, gf))
            bt.text(";")

            bt.text0("if (!vsb) goto ")
            bt.text0(realTarget.name)
            bt.text(";")
        } else {
            //
            // TODO need to lookup special __lt__ function
            //

            bt.text0("vsb = ")
            bt.text0(gc.getRealTargetName(gf, lhs, AOG.GET))
            bt.text0(" < ")
            bt.text0(gc.getAssignmentValue(gf.self, rhs, gf))
            bt.text(";")

            bt.text0("if (!vsb) goto ")
            bt.text0(realTarget.name)
            bt.text(";")
        }

        tos.put_string(bt.text)
    }

    private fun action_JMP(aInstruction: Instruction) {
        val target = aInstruction.getArg(0)

        //		InstructionArgument value  = instruction.getArg(1);
        val realTarget = target as Label

        val bt = BT()

        bt.text0("goto ")
        bt.text0(realTarget.name)
        bt.text(";")

        tos.put_string_ln(bt.text)
    }

    private fun action_JNE(gf: WhyNotGarish_BaseFunction, aInstruction: Instruction) {
        val lhs = aInstruction.getArg(0)
        val rhs = aInstruction.getArg(1)
        val target = aInstruction.getArg(2)

        val realTarget = target as Label

        val vte = gf.getVarTableEntry((lhs as IntegerIA).index)
        assert(rhs != null)
        if (rhs is ConstTableIA) {
            val cte = gf.getConstTableEntry(rhs.index)
            val realTargetName = gc.getRealTargetName(gf, lhs, AOG.GET)
            tos.put_string_ln(
                    String.format("vsb = %s != %s;", realTargetName, gc.getAssignmentValue(gf.self, rhs, gf)))
            tos.put_string_ln(String.format("if (!vsb) goto %s;", realTarget.name))
        } else {
            //
            // TODO need to lookup special __ne__ function ??
            //
            val realTargetName = gc.getRealTargetName(gf, lhs, AOG.GET)
            tos.put_string_ln(
                    String.format("vsb = %s != %s;", realTargetName, gc.getAssignmentValue(gf.self, rhs, gf)))
            tos.put_string_ln(String.format("if (!vsb) goto %s;", realTarget.name))

            val y = 2
        }
    }

    private fun action_X(aGmh: Generate_Method_Header) {
        // TODO functions are being marked as constructor when they are not

        if (is_constructor) {
            tos.dec_tabs()
            tos.put_string_ln("}")
            return
        }

        tos.dec_tabs()
        tos.put_string_ln("}")
        if (!is_unit_type) {
            if (aGmh.tte != null && aGmh.tte!!.isResolved) {
                tos.put_string_ln("return vsr;")
            }
        }
    }

    private fun action_XS() {
        tos.dec_tabs()
        tos.put_string_ln("}")
    }

    fun generateCodeForConstructor(dgf: DeducedEvaConstructor,
                                   fileGen: GenerateResultEnv) {
        val gf = dgf.carrier
        val gr = fileGen.gr()
        val aWorkList = fileGen.wl()
        val yf = gc.a_lookup(gf)
        val cfm = C2C_CodeForConstructor(this, fileGen, yf!!)

        // cfm.calculate();
        val rs = cfm.results

        //		GenerateResult gr = cfm.getGenerateResult();
        val gcfc = GCFC(rs, gf, gr)

        gf.reactive().add(gcfc)

        if (!DebugFlags.MANUAL_DISABLED) {
            gcfc.respondTo(this.gc)
        }
    }

    /*
	public void generateCodeForMethod2(final @NotNull DeducedBaseEvaFunction dgf, final @NotNull GenerateResultEnv aFileGen) {
		assert false;

		final BaseEvaFunction gf = (BaseEvaFunction) dgf.getCarrier();

		assert gf.deducedAlready;

		var yf = gc.a_lookup(gf);
		//var dgf = yf.deduced(gf);

        //assert dgf != null;
        generateCodeForMethod(dgf, aFileGen);
	}
*/
    /*
	public void generateCodeForMethod2(final @NotNull EvaConstructor gf) {
		assert gf.deducedAlready;

		final var yf = gc.a_lookup(gf);
		final var dgf = yf.deduced(gf);

		assert dgf != null;
		generateCodeForConstructor(dgf, null); // TODO !! will fail
	}
*/
    fun generateCodeForMethod(yf: WhyNotGarish_Function, aFileGen: GenerateResultEnv) {
        val dgf = yf.deduced()
        val gf = dgf.carrier
        val bef = gf as BaseEvaFunction

        // yf.onFileGen(aFileGen);
        // TODO separate into method and method_header??
        val cfm = C2C_CodeForMethod(this, yf, aFileGen)

        // cfm.calculate();
        val rs = cfm.results

        val gr = cfm.generateResult

        //		var yf = gc.a_lookup(bef);
//		var dgf = yf.deduced(bef);
        val gcfm = GCFM(rs, dgf, gr)

        dgf.reactive().add(gcfm)

        if (!DebugFlags.MANUAL_DISABLED) {
            gcfm.respondTo(this.gc)
        }

        // FIXME 06/17
        val sink = aFileGen.resultSink()

        if (sink != null) {
            sink.addFunction(bef, rs, gc)
        } else {
            logProgress(9990, "sink failed")
        }
    }

    fun _gc(): GenerateC {
        return this.gc
    }

    enum class AOG {
        ASSIGN, GET
    }

    internal inner class BT {
        private val btos = BufferTabbedOutputStream()

        val text: String
            get() = btos.buffer.text

        fun text(s: String?) {
            btos.put_string_ln(s)
        }

        fun text0(s: String?) {
            btos.put_string(s)
        }

        fun text2(s: Supplier<String?>) {
            btos.put_string_ln(s.get())
        }
    }


    class GCR_VTE_Target(private val gf: WhyNotGarish_BaseFunction, private val vteNum: IntegerIA) : EG_Statement {
        var target_name: String? = null

        // README 10/14 read vs provide
        fun feed(aAOG: AOG?, gc: GenerateC) {
            target_name = gc.getRealTargetName(gf, vteNum, aAOG)
        }

        override val explanation: EX_Explanation
            get() = withMessage("GCR_VTE_Target")

        override val text: String
            get() = "------------------------- just make it compile -------------------------"
    }

    private fun logProgress(code: Int, message: String) {
        gc.ce.logProgress(CompProgress.GenerateC, Pair.of(code, message))
    }
}

internal interface C2C_Results {
    val results: List<C2C_Result>
}
