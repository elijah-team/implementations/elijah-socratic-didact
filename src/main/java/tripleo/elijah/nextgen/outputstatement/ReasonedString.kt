package tripleo.elijah.nextgen.outputstatement

internal class ReasonedString(var text: String, var reason: String) : IReasonedString {
    override fun text(): String? {
        return text
    }

    override fun reason(): String {
        return reason
    }
}
