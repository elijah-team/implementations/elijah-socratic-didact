package tripleo.elijah.stateful

abstract class DefaultStateful : Stateful {
    private var _state: State? = null

    override fun mvState(aBeginState: State?, aState: State) {
        assert(aBeginState == null)
        if (!aState.checkState(this)) {
            //throw new BadState();
            return
        }

        aState.apply(this)
        this.setState(aState)
    }

    override fun setState(aState: State) {
        _state = aState
    }
}
