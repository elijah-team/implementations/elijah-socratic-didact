package tripleo.elijah.nextgen.outputstatement

class ReasonedStatementString(private val text: EG_Statement, private val reason: String) : IReasonedString {
    override fun text(): String? {
        return text.text
    }

    override fun reason(): String {
        return reason
    }
}
