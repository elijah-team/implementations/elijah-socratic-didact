package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.stages.deduce.NamespaceInvocation
import tripleo.elijah.stages.gen_fn.GenType

class SGTA_SetNamespaceInvocation : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        val nsi = arena.get<NamespaceInvocation>("nsi")

        gt.ci = nsi
    }
}
