/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import tripleo.elijah.lang.i.Context
import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.lang.i.SmallWriter
import tripleo.elijah.lang.impl.AbstractExpression
import tripleo.elijah.lang.impl.StatementWrapperImpl
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.lang2.ElElementVisitor
import tripleo.elijah.util2.UnintendedUseException

/**
 * Created 9/18/21 4:03 AM
 */
class WrappedStatementWrapper(aExpression: IExpression,
                              aContext: Context?,
                              aParent: OS_Element?,
                              val variableStatement: VariableStatementImpl) : StatementWrapperImpl(aExpression, aContext, aParent), OS_Element {
    val wrapped: Wrapped = Wrapped(variableStatement, aExpression)

    override fun getContext(): Context? {
        throw UnintendedUseException()
    }

    override fun getParent(): OS_Element? {
        throw UnintendedUseException()
    }

    override fun serializeTo(sw: SmallWriter) {
        throw UnintendedUseException()
    }

    override fun visitGen(visit: ElElementVisitor) {
        // TODO Auto-generated method stub
//		visit.visitStatementWrapper(this);
        throw UnintendedUseException()
    }

    inner class Wrapped(private val variableStatement: VariableStatementImpl, private val expression: IExpression) : AbstractExpression() {
        override fun is_simple(): Boolean {
            return expression.is_simple
        }
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

