package tripleo.elijah.typinf

import java.util.function.Consumer

abstract class AstNode {
    // Used by the type inference algorithm.
    internal var _type: Type? = null

    // Used by passes that traverse the AST. Each concrete node class lists the
    // sub-nodes it has as children.
    internal var _children: List<AstNode> = ArrayList()

    /**
     * Visit all children with a function that takes a child node.
     */
    fun visit_children(func: Consumer<AstNode>) {
        for (child in get_children()) {
            func.accept(child)
        }
    }

    fun get_children(): List<AstNode> {
        return _children
    }

    fun set_children(a_children: List<AstNode>) {
        _children = a_children
    }

    fun get_type(): Type? {
        return _type
    }

    fun set_type(a_type: Type?) {
        _type = a_type
    }
}
