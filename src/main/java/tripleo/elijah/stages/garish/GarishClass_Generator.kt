package tripleo.elijah.stages.garish

import tripleo.elijah.lang.i.ClassTypes
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink

class GarishClass_Generator(private val carrier: EvaClass) {
    private var generatedAlready = false

    fun provide(aResultSink: GenerateResultSink,
                aGarishClass: GarishClass,
                aGenerateResult: GenerateResult,
                aGenerateC: GenerateC) {
        if (!generatedAlready) {
            when (carrier.klass.type) {
                ClassTypes.INTERFACE, ClassTypes.SIGNATURE, ClassTypes.ABSTRACT -> return // ignore
                ClassTypes.ANNOTATION, ClassTypes.EXCEPTION, ClassTypes.NORMAL, ClassTypes.STRUCTURE -> {
                    /*passthru*/
                    aResultSink.add(GarishClass__addClass_1(aGarishClass, aGenerateResult, aGenerateC))
                    generatedAlready = true
                }
            }
        }
    }
}
