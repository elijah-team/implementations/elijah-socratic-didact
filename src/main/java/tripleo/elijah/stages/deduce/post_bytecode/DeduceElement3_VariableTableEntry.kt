/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce.post_bytecode

import org.apache.commons.lang3.tuple.ImmutablePair
import org.apache.commons.lang3.tuple.Pair
import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Locatable
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.AliasStatementImpl
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceClient3
import tripleo.elijah.stages.deduce.post_bytecode.DED.DED_VTE
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3.DeduceElement3_Kind
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stateful.DefaultStateful
import tripleo.elijah.stateful.State
import tripleo.elijah.stateful.StateRegistrationToken
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.Operation2
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.ReadySupplier_1
import java.io.PrintStream
import java.util.*
import java.util.stream.Collectors

class DeduceElement3_VariableTableEntry : DefaultStateful, IDeduceElement3 {
    class Diagnostic_8884(private val vte: VariableTableEntry, private val gf: BaseEvaFunction) : GCFM_Diagnostic {
        private val _code = 8884

        override fun _message(): String {
            return String.format("%d temp variable has no type %s %s", _code, vte, gf)
        }

        override fun code(): String {
            return "" + _code
        }

        override fun primary(): Locatable {
            return null!!
        }

        override fun report(stream: PrintStream) {
            stream.printf(_message())
        }

        override fun secondary(): List<Locatable?> {
            return null!!
        }

        override fun severity(): Diagnostic.Severity {
            return Diagnostic.Severity.ERROR
        }
    }

    class Diagnostic_8885(private val vte: VariableTableEntry) : GCFM_Diagnostic {
        private val _code = 8885

        override fun _message(): String {
            return String.format("%d x is null (No typename specified) for %s%n", _code, vte.name)
        }

        override fun code(): String {
            return "" + _code
        }

        override fun primary(): Locatable {
            return null!!
        }

        override fun report(stream: PrintStream) {
            stream.printf(_message())
        }

        override fun secondary(): List<Locatable?> {
            return null!!
        }

        override fun severity(): Diagnostic.Severity {
            return Diagnostic.Severity.ERROR
        }
    }

    enum class ST {
        ;

        internal class ExitConvertUserTypes : State {
            private var identity: StateRegistrationToken? = null

            override fun apply(element: DefaultStateful) {
                val vte = (element as DeduceElement3_VariableTableEntry).principal

                val dt2 = element.deduceTypes2()
                val errSink = dt2._errSink()
                val phase = dt2._phase()
                val LOG = dt2._LOG()

                if (vte.typeTableEntry == null) return  // TODO only for tests


                val attached = vte.typeTableEntry.attached ?: return

                if (Objects.requireNonNull<OS_Type.Type>(attached.type) == OS_Type.Type.USER) {
                    val x = attached.typeName
                    if (x is NormalTypeName) {
                        val tn = x.name
                        apply_normal(vte, dt2, errSink, phase, LOG, attached, x, tn)
                    }
                }
            }

            override fun checkState(aElement3: DefaultStateful): Boolean {
                return true
            }

            override fun setIdentity(aId: StateRegistrationToken) {
                identity = aId
            }

            companion object {
                private fun apply_normal(vte: VariableTableEntry, dt2: DeduceTypes2,
                                         errSink: ErrSink, phase: DeducePhase, LOG: ElLog,
                                         attached: OS_Type, x: TypeName, tn: String) {
                    val lrl = x.context.lookup(tn)
                    var best = lrl.chooseBest(null)

                    while (best is AliasStatementImpl) {
                        best = DeduceLookupUtils._resolveAlias((best as AliasStatementImpl?)!!, dt2)
                    }

                    if (best != null) {
                        if (!(OS_Type.isConcreteType(best))) {
                            errSink.reportError(String.format("Not a concrete type %s for (%s)", best, tn))
                        } else {
                            LOG.info("705 $best")
                            // NOTE that when we set USER_CLASS from USER generic information is
                            // still contained in constructable_pte
                            val genType = dt2._inj().new_GenTypeImpl(attached,
                                    (best as ClassStatement).oS_Type, true, x, dt2, errSink, phase)
                            vte.setLikelyType(genType)
                        }
                        // vte.el = best;
                        // NOTE we called resolve_var_table_entry above
                        LOG.info("200 $best")
                        assert(vte.resolvedElement == null || vte.status == BaseTableEntry.Status.KNOWN)                    // vte.setStatus(BaseTableEntry.Status.KNOWN, best/*vte.el*/);
                    } else {
                        errSink.reportDiagnostic(dt2._inj().new_ResolveError(x, lrl))
                    }
                }
            }
        }

        internal class ExitResolveState : State {
            private var identity: StateRegistrationToken? = null

            override fun apply(element: DefaultStateful) {
                val vte = (element as DeduceElement3_VariableTableEntry).principal
                vte.resolve_var_table_entry_for_exit_function()
            }

            override fun checkState(aElement3: DefaultStateful): Boolean {
                return (aElement3 as DeduceElement3_VariableTableEntry).st === INITIAL
            }

            override fun setIdentity(aId: StateRegistrationToken) {
                identity = aId
            }
        }

        internal class InitialState : State {
            private var identity: StateRegistrationToken? = null

            override fun apply(element: DefaultStateful) {
            }

            override fun checkState(aElement3: DefaultStateful): Boolean {
                return true
            }

            override fun setIdentity(aId: StateRegistrationToken) {
                identity = aId
            }
        }

        companion object {
            @JvmField
            var EXIT_CONVERT_USER_TYPES: State? = null

            var EXIT_RESOLVE: State? = null

            var INITIAL: State? = null

            @JvmStatic
            fun register(aDeducePhase: DeducePhase) {
                EXIT_RESOLVE = aDeducePhase.registerState(ExitResolveState())
                INITIAL = aDeducePhase.registerState(InitialState())
                EXIT_CONVERT_USER_TYPES = aDeducePhase.registerState(ExitConvertUserTypes())
            }
        }
    }

    private lateinit var principal: VariableTableEntry

    private var st: State? = null

    private var deduceTypes2: DeduceTypes2? = null

    private var generatedFunction: BaseEvaFunction? = null

    private var genType: GenType? = null

    internal constructor(vte_type_attached: OS_Type?) {
        throw UnsupportedOperationException("Not supported yet.") // Generated from

        // nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Contract(pure = true)
    constructor(aVariableTableEntry: VariableTableEntry) {
        principal = aVariableTableEntry
        st = ST.INITIAL
    }

    constructor(aVariableTableEntry: VariableTableEntry,
                aDeduceTypes2: DeduceTypes2?, aGeneratedFunction: BaseEvaFunction?) : this(aVariableTableEntry) {
        setDeduceTypes2(aDeduceTypes2, aGeneratedFunction)
    }

    fun __action_VAR_pot_1_tableEntry_null(aVariableStatement: VariableStatement) {
        val iv = aVariableStatement.initialValue()
        if (iv === LangGlobals.UNASSIGNED) {
            NotImplementedException.raise()
        }

        val gt = __getGenTypeFromInitialValue(iv)!!
    }

    fun __action_vp1o(vte: VariableTableEntry, aPot: TypeTableEntry,
                      pte1: ProcTableEntry, e: OS_Element) {
        assert(vte === principal)
        if (e is FunctionDef) {
            __action_vp1o__FunctionDef(vte, pte1, e)
        } else {
            var ee: OS_Element? = e
            val es = arrayOf<OS_Element?>(null)
            val b = booleanArrayOf(false)

            if (e is AliasStatementImpl) {
                val dg = deduceTypes2().DG_AliasStatement(e, deduceTypes2)
                val p = dg.resolvePromise()

                if (p!!.isResolved) {
                    p.then { xx: DG_Item? ->
                        if (xx is DG_ClassStatement) {
                            es[0] = xx.evaClass()!!.klass
                        } else if (xx is DG_FunctionDef) {
                            __action_vp1o(vte, aPot, pte1, xx.functionDef)
                            b[0] = true
                        }
                    }
                }

                if (es[0] != null) ee = es[0]
            }

            if (!b[0]) {
                assert(ee != null)
                if (ee is AliasStatement) {
                    ee = DeduceLookupUtils._resolveAlias(ee, deduceTypes2!!)
                }
                if (ee is ClassStatement) {
                    val dg = deduceTypes2().DG_ClassStatement(ee as ClassStatement?)

                    vte.setStatus(BaseTableEntry.Status.KNOWN, dg.GenericElementHolder())
                    pte1.setStatus(BaseTableEntry.Status.KNOWN, dg.ConstructableElementHolder(e, vte))

                    //			vte.setCallablePTE(pte1);
                    val gt = aPot.genType
                    setup_GenType(e, gt)

                    //			if (gt.node == null)
//				gt.node = vte.genType.node;
                    vte.genType.copy(gt)
                }
            }
        }
    }

    private fun __action_vp1o__FunctionDef(vte: VariableTableEntry, pte1: ProcTableEntry,
                                           e: FunctionDef) {
        NotImplementedException.raise()

        if (pte1.expression_num is IdentIA) {
            val ite1 = pte1.expression_num.entry
            __action_vp1o__FunctionDef__ITE(pte1, e, ite1)
        } else if (pte1.expression_num is IntegerIA) {
            val vte1 = pte1.expression_num.entry
            __action_vp1o__FunctionDef__VTE(vte, pte1, e, vte1)
        }
    }

    private fun __action_vp1o__FunctionDef__ITE(pte1: ProcTableEntry, e: FunctionDef,
                                                ite1: IdentTableEntry) {
        val dt2 = principal._deduceTypes2()!!
        val dp = ite1.buildDeducePath(generatedFunction)
        val t = dp.getType(dp.size() - 1)
        ite1.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(e))
        pte1.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(e))
        pte1.typePromise().then { result ->
            if (t == null) {
                ite1.makeType(generatedFunction!!, TypeTableEntry.Type.TRANSIENT, result.resolved)
                ite1.setGenType(result)
            } else {
                // assert false; // we don't expect this, but note there is no problem if it
                // happens
                t.copy(result)
            }
        }
    }

    private fun __action_vp1o__FunctionDef__VTE(vte: VariableTableEntry,
                                                pte1: ProcTableEntry, e: FunctionDef, vte1: VariableTableEntry) {
        val dt2 = principal._deduceTypes2()!!
        // DeducePath dp = vte1.buildDeducePath(generatedFunction);
        val de3_vte = vte1.deduceElement3
        val t = de3_vte.genType

        // assert t != null;
        vte.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(e))
        pte1.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(e))

        pte1.typePromise().then { genType1: GenType? ->
            t?.copy(genType1)
        }
    }

    private fun __doLogic_pot_size_0(p: Promise<GenType, Void, Void>, LOG: ElLog,
                                     vte1: VariableTableEntry, errSink: ErrSink, ctx: Context,
                                     e_text: String, vte: VariableTableEntry) {
        // README moved up here to elimiate work
        if (p.isResolved) {
            System.out.printf("890-1 Already resolved type: vte1.type = %s, gf = %s %n", vte1.typeTableEntry,
                    generatedFunction)
            return
        }
        val lrl = ctx.lookup(e_text)
        val best = lrl.chooseBest(null)
        if (best is FormalArgListItem) {
            __doLogic_pot_size_0__fali(vte1, errSink, vte, best)
        } else if (best is VariableStatementImpl) {
            __doLogic_pot_size_0__varstmt(p, vte1, e_text, vte, best, best)
        } else {
            val y = 2
            LOG.err("543 " + best!!.javaClass.name)
            throw NotImplementedException()
        }
    }

    private fun __doLogic_pot_size_0__fali(vte1: VariableTableEntry, errSink: ErrSink,
                                           vte: VariableTableEntry, fali: FormalArgListItem) {
        val osType: OS_Type = deduceTypes2!!._inj().new_OS_UserType(fali.typeName())
        if (osType != vte.typeTableEntry.attached) {
            val tte1 = generatedFunction!!.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, osType,
                    fali.nameToken, vte1)
            /*
			 * if (p.isResolved()) System.out.
			 * printf("890 Already resolved type: vte1.type = %s, gf = %s, tte1 = %s %n",
			 * vte1.type, generatedFunction, tte1); else
			 */
            run {
                val attached = tte1.attached
                when (attached!!.type) {
                    OS_Type.Type.USER -> vte1.typeTableEntry.attached = attached
                    OS_Type.Type.USER_CLASS -> {
                        val gt = vte1.genType
                        gt.resolved = attached
                        vte1.resolveType(gt)
                    }

                    else -> errSink.reportWarning("2853 Unexpected value: " + attached.type)
                }
            }
        }
    }

    private fun __doLogic_pot_size_0__varstmt(p: Promise<GenType, Void, Void>,
                                              vte1: VariableTableEntry, e_text: String, vte: VariableTableEntry,
                                              vs: VariableStatementImpl, best: OS_Element) {
        //
        assert(vs.name == e_text)
        //
        val vte2_ia = generatedFunction!!.vte_lookup(vs.name)
        val vte2 = generatedFunction!!.getVarTableEntry(DeduceTypes2.to_int(vte2_ia!!))
        if (p.isResolved) System.out.printf("915 Already resolved type: vte2.type = %s, gf = %s %n", vte1.typeTableEntry,
                generatedFunction)
        else {
            val gt = vte1.genType
            val attached = vte2.typeTableEntry.attached
            gt.resolved = attached
            vte1.resolveType(gt)
        }
        //								vte.type = vte2.type;
//								tte.attached = vte.type.attached;
        vte.setStatus(BaseTableEntry.Status.KNOWN, deduceTypes2!!._inj().new_GenericElementHolder(best))
        vte2.setStatus(BaseTableEntry.Status.KNOWN, deduceTypes2!!._inj().new_GenericElementHolder(best)) // TODO ??
    }

    private fun __doLogic_pot_size_1(potentialTypes: List<TypeTableEntry>,
                                     p: Promise<GenType, Void, Void>, LOG: ElLog,
                                     vte1: VariableTableEntry, errSink: ErrSink) {
        // tte.attached = ll.get(0).attached;
//							vte.addPotentialType(instructionIndex, ll.get(0));
        if (p.isResolved) {
            LOG.info(String.format("1047 (vte already resolved) %s vte1.type = %s, gf = %s, tte1 = %s %n",
                    vte1.name, vte1.typeTableEntry, generatedFunction, potentialTypes[0]))
        } else {
            val attached = potentialTypes[0].attached ?: return
            when (attached.type) {
                OS_Type.Type.USER -> vte1.typeTableEntry.attached = attached // !!
                OS_Type.Type.USER_CLASS -> {
                    val gt = vte1.genType
                    gt.resolved = attached
                    vte1.resolveType(gt)
                }

                else -> errSink.reportWarning("Unexpected value: " + attached.type)
            }
        }
    }

    private fun __doLogic_pot_size_default(potentialTypes: List<TypeTableEntry>,
                                           p: Promise<GenType, Void, Void>, LOG: ElLog,
                                           vte1: VariableTableEntry, errSink: ErrSink, ctx: Context,
                                           e_text: String, vte: VariableTableEntry) {
        // TODO hopefully this works
        val potentialTypes1 = potentialTypes.stream().filter { input: TypeTableEntry -> input.attached != null }
                .collect(Collectors.toList())

        // prevent infinite recursion
        if (potentialTypes1.size < potentialTypes.size) doLogic(potentialTypes1, p, LOG, vte1, errSink, ctx, e_text, vte)
        else LOG.info("913 Don't know")
    }

    fun __getGenTypeFromInitialValue(iv: IExpression): GenType? {
        /*
		 * final @NotNull IExpression iv = aVariableStatement.initialValue(); if (iv ==
		 * IExpression.UNASSIGNED) { NotImplementedException.raise(); }
		 */

        when (iv.kind) {
            ExpressionKind.PROCEDURE_CALL -> {
                val procedureCallExpression = iv as ProcedureCallExpression
                val name_exp = procedureCallExpression.left
                assert(name_exp is IdentExpression)
                val name2 = name_exp as IdentExpression
                val lrl2 = name2.context.lookup(name2.text)
                val el2 = lrl2.chooseBest(null) ?: return null

                when (DecideElObjectType.getElObjectType(el2)) {
                    ElObjectType.CLASS -> {
                        val classStatement = el2 as ClassStatement

                        val dt2 = principal._deduceTypes2()!!
                        val genType1 = dt2._inj().new_GenTypeImpl(classStatement)

                        // deferredMember.typeResolved().resolve(genType1);
                        genType1.genCIForGenType2(deduceTypes2)

                        // TODO and what? 05/24
                        this.genType = genType1

                        return genType1
                    }

                    else -> {
                        NotImplementedException.raise()
                    }
                }
            }

            ExpressionKind.IDENT -> {
                val identExpression = iv as IdentExpression
                val ident = identExpression.text
                val y = 2
            }

            else -> {
                NotImplementedException.raise()
            }
        }
        return null
    }

    private fun __itemFali__isFunctionDef(aDeduceTypes2: DeduceTypes2, fd: FunctionDef) {
        val pte: ProcTableEntry? = null
        val invocation = aDeduceTypes2.getInvocation((generatedFunction as EvaFunction?)!!)
        aDeduceTypes2.forFunction(aDeduceTypes2.newFunctionInvocation(fd, pte, invocation, aDeduceTypes2.phase),
                object : ForFunction() {
                    override fun typeDecided(aType: GenType?) {
                        assert(fd === generatedFunction!!.fd)
                        //
                        val tte1 = generatedFunction!!.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT,
                                aDeduceTypes2.gt(aType!!), principal) // TODO expression?
                        principal.setType(tte1)
                    }
                })
    }

    fun _action_002_no_resolved_element(errSink: ErrSink, pte: ProcTableEntry,
                                        ite: IdentTableEntry, dc: DeduceClient3,
                                        phase: DeducePhase) {
        val d = DeferredObject<Context, Void, Void>()
        d.then { context: Context? ->
            try {
//				final ContextImpl context = resolvedElement.getContext();
                val lrl2 = dc.lookupExpression(ite.ident.get(), context!!)
                val best = lrl2.chooseBest(null)!!
                ite.setStatus(BaseTableEntry.Status.KNOWN, dc._deduceTypes2()._inj().new_GenericElementHolder(best))
                action_002_1(pte, ite, false, dc, phase)
            } catch (aResolveError: ResolveError) {
                errSink.reportDiagnostic(aResolveError)
                assert(false)
            }
        }

        val backlink = principal

        val resolvedElement = backlink.resolvedElement!!
        if (resolvedElement is IdentExpression) {
            backlink.typePromise().then { result: GenType ->
                val context: Context = result.resolved.classOf.context
                d.resolve(context)
            }
        } else {
            val context = resolvedElement.context
            d.resolve(context)
        }
    }

    private fun action_002_1(pte: ProcTableEntry, ite: IdentTableEntry,
                             setClassInvocation: Boolean, dc: DeduceClient3,
                             phase: DeducePhase) {
        val resolvedElement = ite.resolvedElement!!

        action_002_1_001(pte, setClassInvocation, dc, phase, resolvedElement)
    }

    private fun action_002_1_001(pte: ProcTableEntry, setClassInvocation: Boolean,
                                 dc: DeduceClient3, phase: DeducePhase,
                                 resolvedElement: OS_Element) {
        if (pte.functionInvocation != null) return

        val p = action_002_1_002_1(pte, dc, phase, resolvedElement) ?: throw IllegalStateException()
        val ci = p.left
        val fi = p.right

        if (setClassInvocation) {
            if (ci != null) {
                pte.classInvocation = ci
            } else SimplePrintLoggerToRemoveSoon.println_err2("542 Null ClassInvocation")
        }

        pte.functionInvocation = fi
    }

    private fun action_002_1_002_1(pte: ProcTableEntry,
                                   dc: DeduceClient3, phase: DeducePhase,
                                   resolvedElement: OS_Element): Pair<ClassInvocation?, FunctionInvocation>? {
        val p: Pair<ClassInvocation?, FunctionInvocation>?
        val fi: FunctionInvocation
        var ci: ClassInvocation?

        val dt2 = principal._deduceTypes2()!!
        if (resolvedElement is ClassStatement) {
            // assuming no constructor name or generic parameters based on function syntax
            ci = dt2._inj().new_ClassInvocation(resolvedElement, null, ReadySupplier_1(dt2))
            ci = phase.registerClassInvocation(ci)
            fi = phase.newFunctionInvocation(null, pte, ci)
            p = ImmutablePair(ci, fi)
        } else if (resolvedElement is FunctionDef) {
            val invocation = dc.getInvocation((generatedFunction as EvaFunction?)!!)
            fi = phase.newFunctionInvocation(resolvedElement, pte, invocation)
            if (resolvedElement.getParent() is ClassStatement) {
                val classStatement = fi.function?.parent as ClassStatement?
                ci = dt2._inj().new_ClassInvocation(classStatement, null, ReadySupplier_1(dt2)) // TODO generics
                ci = phase.registerClassInvocation(ci)
            } else {
                ci = null
            }
            p = ImmutablePair(ci, fi)
        } else {
            p = null
        }

        return p
    }

    fun decl_test_001(gf: BaseEvaFunction): Operation2<OS_Type> {
        // var dt2 = principal.getDeduceElement3().deduceTypes2();
        // assert dt2 != null;

        val vte = principal

        val x = vte.typeTableEntry.attached
        if (x == null && vte.potentialTypes().isEmpty()) {
            val diag: Diagnostic = if (vte.vtt == VariableTableType.TEMP) {
                Diagnostic_8884(vte, gf)
            } else {
                Diagnostic_8885(vte)
            }
            return Operation2.failure(diag)
        }

        if (x == null) {
            return Operation2.failure(GCFM_Diagnostic.forThis("113/133 x is null", "133", Diagnostic.Severity.INFO))
        }

        return Operation2.success(x)
    }

    override fun deduceTypes2(): DeduceTypes2 {
        return deduceTypes2!!
    }

    fun doLogic(potentialTypes: List<TypeTableEntry>,
                p: Promise<GenType, Void, Void>, LOG: ElLog,
                vte1: VariableTableEntry, errSink: ErrSink, ctx: Context,
                e_text: String, vte: VariableTableEntry) {
        when (potentialTypes.size) {
            1 -> __doLogic_pot_size_1(potentialTypes, p, LOG, vte1, errSink)
            0 -> __doLogic_pot_size_0(p, LOG, vte1, errSink, ctx, e_text, vte)
            else -> __doLogic_pot_size_default(potentialTypes, p, LOG, vte1, errSink, ctx, e_text, vte)
        }
    }

    override fun elementDiscriminator(): DED {
        return DED_VTE(principal)
    }

    override fun generatedFunction(): BaseEvaFunction {
        return generatedFunction!!
    }

    override fun genType(): GenType {
        return genType!!
    }

    fun getItemFali(aCtx: Context?, aDeduceTypes2: DeduceTypes2,
                    genType: GenType) {
        assert(generatedFunction != null)
        val errSink = aDeduceTypes2._errSink()

        val ty2 = genType.typeName!! /* .getAttached() */
        var rtype: GenType? = null
        try {
            rtype = aDeduceTypes2.resolve_type(ty2, aCtx)
        } catch (resolveError: ResolveError) {
            errSink.reportError("Cant resolve $ty2") // TODO print better diagnostic
            return
        }

        if (rtype.resolved != null && rtype.resolved.type == OS_Type.Type.USER_CLASS) {
            val lrl2 = rtype.resolved.classOf.context.lookup("__getitem__")
            val best2 = lrl2.chooseBest(null)
            if (best2 != null) {
                if (best2 is FunctionDef) {
                    __itemFali__isFunctionDef(aDeduceTypes2, best2)
                } else {
                    throw NotImplementedException()
                }
            } else {
                throw NotImplementedException()
            }
        }
    }

    override fun getPrincipal(): OS_Element {
        return principal.resolvedElement!!
    }

    override fun kind(): DeduceElement3_Kind {
        return DeduceElement3_Kind.GEN_FN__VTE
    }

    fun potentialTypesRunnableDo(vte_ia: InstructionArgument?, aLOG: ElLog,
                                 aVte1: VariableTableEntry, errSink: ErrSink, ctx: Context,
                                 aE_text: String, aVte: VariableTableEntry) {
        val ll: List<TypeTableEntry> = getPotentialTypesVte((generatedFunction as EvaFunction?)!!, vte_ia!!)
        doLogic(ll, aVte1.typePromise(), aLOG, aVte1, errSink, ctx, aE_text, aVte)
    }

    override fun resolve(aContext: Context, aDeduceTypes2: DeduceTypes2) {
        throw UnsupportedOperationException("Should not be reached")
    }

    override fun resolve(aIdentIA: IdentIA, aContext: Context, aFoundElement: FoundElement) {
        throw UnsupportedOperationException("Should not be reached")
    }

    fun setDeduceTypes2(aDeduceTypes2: DeduceTypes2?, aGeneratedFunction: BaseEvaFunction?) {
        deduceTypes2 = aDeduceTypes2
        generatedFunction = aGeneratedFunction
    }

    private fun setup_GenType(element: OS_Element, aGt: GenType) {
        val list: MutableList<setup_GenType_Action> = ArrayList()
        val arena = setup_GenType_Action_Arena()

        val phase = deduceTypes2!!._phase()

        when (DecideElObjectType.getElObjectType(element)) {
            ElObjectType.NAMESPACE -> {
                val namespaceStatement = element as NamespaceStatement

                list.add(deduceTypes2!!._inj().new_SGTA_SetResolvedNamespace(namespaceStatement))
                list.add(deduceTypes2!!._inj().new_SGTA_RegisterNamespaceInvocation(namespaceStatement, phase))

                //			pte.setNamespaceInvocation(nsi);
                list.add(deduceTypes2!!._inj().new_SGTA_SetNamespaceInvocation())

                //			fi = newFunctionInvocation(fd, pte, nsi, phase);
                for (action in list) {
                    action.run(aGt, arena)
                }
            }

            ElObjectType.CLASS -> {
                val classStatement = element as ClassStatement

                list.add(deduceTypes2!!._inj().new_SGTA_SetResolvedClass(classStatement))
                list.add(deduceTypes2!!._inj().new_SGTA_RegisterClassInvocation(classStatement, phase))
                list.add(deduceTypes2!!._inj().new_SGTA_SetClassInvocation())

                for (action in list) {
                    action.run(aGt, arena)
                }
            }

            ElObjectType.FUNCTION -> {
                // TODO this seems to be an elaborate copy of the above with no differentiation
                // for functionDef
                val functionDef = element as FunctionDef
                val parent = functionDef.parent
                val inv: IInvocation?
                when (DecideElObjectType.getElObjectType(parent!!)) {
                    ElObjectType.CLASS -> {
                        aGt.resolved = (parent as ClassStatement?)!!.oS_Type
                        inv = phase.registerClassInvocation((parent as ClassStatement?)!!, null, ReadySupplier_1(deduceTypes2!!))
                        inv!!.resolveDeferred().then { result: EvaClass ->
                            result.functionMapDeferred(functionDef) { aResult: EvaFunction? -> aGt.node = aResult }
                        }
                    }

                    ElObjectType.NAMESPACE -> {
                        aGt.resolvedn = parent as NamespaceStatement?
                        inv = phase.registerNamespaceInvocation(parent as NamespaceStatement)
                        inv!!.resolveDeferred().then { result: EvaNamespace ->
                            result.functionMapDeferred(functionDef) { aResult: EvaFunction? -> aGt.node = aResult }
                        }
                    }

                    else -> throw NotImplementedException()
                }
                aGt.ci = inv
            }

            ElObjectType.ALIAS -> {
                var el: OS_Element? = element
                while (el is AliasStatementImpl) {
                    el = DeduceLookupUtils._resolveAlias((el as AliasStatementImpl?)!!, deduceTypes2!!)
                }
                setup_GenType(el!!, aGt)
            }

            else -> throw IllegalStateException("Unknown parent")
        }
    }

    companion object {
        private fun getPotentialTypesVte(generatedFunction: EvaFunction,
                                         vte_index: InstructionArgument): ArrayList<TypeTableEntry> {
            return getPotentialTypesVte(generatedFunction.getVarTableEntry(DeduceTypes2.to_int(vte_index)))
        }

        fun getPotentialTypesVte(vte: VariableTableEntry): ArrayList<TypeTableEntry> {
            return ArrayList(vte.potentialTypes())
        }
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

