package tripleo.elijah.stages.gen_c

import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_ProcTableEntry

class EX_ProcTableEntryExplanation(private val pte: DeduceElement3_ProcTableEntry) : EX_Explanation {
    override fun message(): String {
        return "EX_ProcTableEntryExplanation"
    }
}
