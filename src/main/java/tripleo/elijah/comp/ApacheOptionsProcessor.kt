package tripleo.elijah.comp

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.ICompilationBus
import tripleo.elijah.comp.i.OptionsProcessor
import tripleo.elijah.comp.impl.CC_SetSilent
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.internal.CompilationImpl.Companion.isGitlab_ci
import tripleo.elijah.util.*
import tripleo.vendor.org_apache_commons_cli.CommandLineParser
import tripleo.vendor.org_apache_commons_cli.DefaultParser
import tripleo.vendor.org_apache_commons_cli.Options
import tripleo.vendor.org_apache_commons_cli.ParseException

class ApacheOptionsProcessor @Contract(pure = true) constructor() : OptionsProcessor {
    private val clp: CommandLineParser = DefaultParser()
    private val options = Options()

    init {
        options.addOption("s", true, "stage: E: parse; O: output")
        options.addOption("silent", false, "suppress DeduceType output to console")
    }

    fun processImpl(c: Compilation,
                aInputs: List<CompilerInput?>,
                aCb: ICompilationBus): Operation<Ok> {
        try {
            val cmd = clp.parse(options, aInputs)

            /**
             * [ICompilationBus.option]
             */

            // TODO 09/08 promises??
            // c.getCompilationEnclosure().getCompilationBus().option();
            if (isGitlab_ci || cmd.hasOption("silent")) {
                aCb.addCompilerChange(CC_SetSilent::class.java)
                //				new CC_SetSilent(true).apply(c);
            }

            return Operation.success(Ok.instance())
        } catch (aE: ParseException) {
            return Operation.failure( /* new DiagnosticException */(aE))
        }
    }

    override fun process(aC: Compilation0, aInputs: List<CompilerInput>, aCb: ICompilationBus): Operation<Ok> {
        return processImpl(aC as Compilation, aInputs, aCb)
    }
}
