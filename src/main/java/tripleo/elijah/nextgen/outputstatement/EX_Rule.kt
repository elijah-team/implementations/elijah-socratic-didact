package tripleo.elijah.nextgen.outputstatement

open class EX_Rule(private val rule: String) : EX_Explanation {
    override fun message(): String {
        return rule
    }
}
