package tripleo.elijah.comp.impl

import tripleo.elijah.comp.i.IProgressSink
import tripleo.elijah.comp.i.IProgressSink.Codes
import tripleo.elijah.comp.i.ProgressSinkComponent
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

class DefaultProgressSink : IProgressSink {
    override fun note(code: Codes,
                      component: ProgressSinkComponent,
                      type: Int,
                      params: Array<Any>) {
        // component.note(code, type, params);
        if (component.isPrintErr(code, type)) {
            val s = component.printErr(code, type, params)
            SimplePrintLoggerToRemoveSoon.println_out_4(s)
        }
    }
}
