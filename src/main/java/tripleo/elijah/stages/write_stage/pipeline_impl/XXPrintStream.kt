package tripleo.elijah.stages.write_stage.pipeline_impl

import org.jetbrains.annotations.Contract
import java.io.PrintStream

class XXPrintStream @Contract(pure = true) constructor(private val p: PrintStream) : XPrintStream {
    override fun println(aS: String) {
        p.println(aS)
    }
}
