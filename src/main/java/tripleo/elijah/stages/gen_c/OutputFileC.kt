/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_c

import tripleo.elijah.stages.gen_generic.Dependency
import tripleo.elijah.stages.gen_generic.DependencyRef
import tripleo.elijah.stages.gen_generic.IOutputFile
import tripleo.util.buffer.Buffer
import java.util.function.Predicate
import java.util.stream.Collectors

/**
 * Created 9/13/21 10:50 PM
 */
class OutputFileC(private val output: String) : IOutputFile {
    private val buffers: MutableList<Buffer> = ArrayList() // LinkedList??
    private val dependencies: MutableList<DependencyRef> = ArrayList()
    private val notedDeps: MutableList<Dependency> = ArrayList()

    override fun getOutput(): String {
        val sb = StringBuilder()

        val dependencyPredicate = Predicate { next: Dependency ->
            for (dependency in dependencies) {
                if (next.ref === dependency) {
                    return@Predicate true
                }
            }
            false
        }

        val wnd = notedDeps.stream().filter(dependencyPredicate).collect(Collectors.toList())

        assert(wnd.size == dependencies.size)
        for (dependencyRaw in dependencies) {
            val dependency = dependencyRaw as CDependencyRef
            val headerFile = dependency.headerFile
            val output = String.format("#include \"%s\"\n", headerFile!!.substring(1))
            sb.append(output)
        }

        sb.append('\n')

        for (dependency in wnd) {
            val resolvedString = dependency.resolved.toString()
            val output = String.format("//#include \"%s\" // for %s\n", "nothing.h", resolvedString)
            sb.append(output)
        }

        sb.append('\n')

        for (buffer in buffers) {
            sb.append(buffer.text)
            sb.append('\n')
        }
        return sb.toString()
    }

    override fun putBuffer(aBuffer: Buffer) {
        buffers.add(aBuffer)
    }

    override fun putDependencies(aDependencies: List<DependencyRef>) {
        dependencies.addAll(aDependencies)
    }

    fun putDependencies(aNotedDeps: Set<Dependency>) {
        notedDeps.addAll(aNotedDeps)
    }
} //
//
//

