package tripleo.elijah.stages.instructions

import org.jetbrains.annotations.Contract
import tripleo.elijah.util.IFixedList

class InstructionFixedList @Contract(pure = true) constructor(private val instruction: Instruction) : IFixedList<InstructionArgument> {
    override fun get(at: Int): InstructionArgument {
        return instruction.getArg(at)!!
    }

    override fun size(): Int {
        return instruction.argsSize
    }
}
