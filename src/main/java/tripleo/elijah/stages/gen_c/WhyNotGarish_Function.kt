package tripleo.elijah.stages.gen_c

import org.jdeferred2.impl.DeferredObject
import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.ConstructorDef
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaConstructor
import tripleo.elijah.stages.gen_fn.EvaFunction
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

class WhyNotGarish_Function(override val gf: BaseEvaFunction, private val generateC: GenerateC) : WhyNotGarish_BaseFunction(), WhyNotGarish_Item {
    private val __declaringContext: WhyNotGarish_DeclaringContext

    private val fileGenPromise = DeferredObject<GenerateResultEnv, Void, Void>()
    private var __deduced: DefaultDeducedBaseEvaFunction? = null

    init {
        fileGenPromise.then { aFileGen: GenerateResultEnv -> this.onFileGen(aFileGen) }
        __declaringContext = object : WhyNotGarish_DeclaringContext {
            override fun isClassStatement(): Boolean {
                return gf.fd.parent is ClassStatement
            }

            override fun pointsToConstructor(): Boolean {
                return gf.getFD() is ConstructorDef
            }

            override fun pointsToConstructor2(): Boolean {
                return gf is EvaConstructor
            }

            override fun isDefaultConstructor(): Boolean {
                return false
            }
        }
    }

    @Contract(pure = true)
    fun deduced(): DeducedBaseEvaFunction {
        if (__deduced == null) {
            val generateModule = generateC.fileGen__.gmgm()
            val deducePhase = generateModule.gmr().env().pa().compilationEnclosure.pipelineLogic.dp

            // TODO 10/16 cached: tho this may not matter
            val dt2 = deducePhase._inj().new_DeduceTypes2(gf.module(), deducePhase, ElLog.Verbosity.VERBOSE)

            dt2.deduceOneFunction((gf as EvaFunction), deducePhase)

            __deduced = DefaultDeducedBaseEvaFunction(gf)
        }

        return __deduced!!
    }

    override fun hasFileGen(): Boolean {
        return fileGenPromise.isResolved
    }

    fun onFileGen(aFileGen: GenerateResultEnv) {
        checkNotNull(gf.fd) {
            // FIXME why? when?
            "[WhyNotGarish_Function::onFileGen] gf.getFD() == null"
        }
        val gcfm = Generate_Code_For_Method(generateC, generateC.LOG)
        gcfm.generateCodeForMethod(this, aFileGen)
    }

    override fun provideFileGen(fg: GenerateResultEnv) {
        fileGenPromise.resolve(fg)
    }

    override fun declaringContext(): WhyNotGarish_DeclaringContext {
        return __declaringContext
    }

    fun resolveFileGenPromise(aFileGen: GenerateResultEnv) {
        if (!fileGenPromise.isResolved) {
            fileGenPromise.resolve(aFileGen)
        } else {
            SimplePrintLoggerToRemoveSoon.println_out_4("twice for $generateC")
        }
    }
}
