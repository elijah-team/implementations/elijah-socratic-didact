package tripleo.elijah.comp

import tripleo.elijah.compiler_model.CM_Factory
import tripleo.elijah.compiler_model.CM_Filename
import tripleo.elijah.contexts.ModuleContext
import tripleo.elijah.contexts.ModuleContext__
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.lang.impl.OS_ModuleImpl
import tripleo.elijah.util.Mode
import tripleo.elijah.world.i.WorldModule

class ModuleBuilder(aCompilation: Compilation) {
    // private final Compilation compilation;
    //			compilation = aCompilation;
    private val mod: OS_Module = OS_ModuleImpl()
    private var _addToCompilation = false
    private var _fn: CM_Filename? = null

    init {
        mod.setParent(aCompilation)
    }

    fun addToCompilation(): ModuleBuilder {
        _addToCompilation = true
        return this
    }

    fun build(): OS_Module {
        if (_addToCompilation) {
            checkNotNull(_fn) { "Filename not set in ModuleBuilder" }

            val compilation = mod.compilation as Compilation
            val world = compilation.world()
            world.addModule(mod, _fn, compilation)
        }
        return mod
    }

    fun setContext(): ModuleBuilder {
        val mctx: ModuleContext = ModuleContext__(mod)
        mod.setContext(mctx)
        return this
    }

    fun withFileName(aS: String?): ModuleBuilder {
        return withFileName(CM_Factory.Filename__of(aS!!))
    }

    fun withFileName(aFn: CM_Filename?): ModuleBuilder {
        _fn = aFn
        mod.setFileName(aFn)
        return this
    }

    fun withPrelude(aPrelude: String?): ModuleBuilder {
        val prelude = mod.compilation.findPrelude(aPrelude)

        assert(prelude.mode() == Mode.SUCCESS)
        mod.setPrelude((prelude.success() as WorldModule).module())

        return this
    }
}
