/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import lombok.Getter
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.AccessBus.AB_GenerateResultListener
import tripleo.elijah.comp.graph.i.CK_Action
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.graph.i.CK_Steps
import tripleo.elijah.comp.graph.i.CK_StepsContext
import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.g.GPipelineMember
import tripleo.elijah.stages.gen_c.CDependencyRef
import tripleo.elijah.stages.gen_c.OutputFileC
import tripleo.elijah.stages.gen_generic.*
import tripleo.elijah.stages.generate.ElSystem
import tripleo.elijah.stages.generate.OutputStrategy
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.logging.ElLog.Verbosity
import tripleo.elijah.stages.logging.ElLog_
import tripleo.elijah.stages.write_stage.pipeline_impl.*
import tripleo.elijah.stages.write_stage.pipeline_impl.WP_Flow.OPS
import tripleo.elijah.util.*
import tripleo.elijah.util2.Eventual
import java.util.*
import java.util.function.Consumer
import java.util.function.Supplier
import java.util.stream.Collectors

/**
 * Created 8/21/21 10:19 PM
 */
class WritePipeline(pa0: GPipelineAccess) : PipelineMember(), Consumer<Supplier<GenerateResult?>>, AB_GenerateResultListener, GPipelineMember {
    // 24/01/04 back and forth
    @JvmField
	@Getter
    val generateResultPromise: Eventual<GenerateResult> = Eventual()

    // 24/01/04 back and forth
    @JvmField
	@Getter
    val st: WritePipelineSharedState
    private val cih: CompletedItemsHandler
    private val latch: DoubleLatch<GenerateResult>

    private val ops: OPS? = null

    private val monitor: CK_Monitor = object : CK_Monitor {
        override fun reportSuccess() {
            val y = 2
        }

        override fun reportFailure() {
            val y = 2
        }
    }

    init {
        val pa = pa0 as IPipelineAccess

        st = WritePipelineSharedState(pa)

        // computed

        // created
        latch = DoubleLatch { gr: GenerateResult -> doubleLatch_GenerateResult(gr, pa) }

        // state
        st.mmb = ArrayListMultimap.create()
        st.lsp_outputs = ArrayListMultimap.create()
        st.grs = pa.generateResultSink

        // ??
        st.sys = ElSystem(false, st.c) { this.createOutputStratgy() }

        cih = CompletedItemsHandler(st)

        pa.accessBus.subscribe_GenerateResult { this.gr_slot(it) }
        pa.accessBus.subscribe_GenerateResult { generateResultPromise.resolve(it) }

        pa.setWritePipeline(this)

        // st.outputs = pa.getOutputs();
    }

    private fun doubleLatch_GenerateResult(gr: GenerateResult, pa: IPipelineAccess) {
        st.gr = gr
        st._up = this@WritePipeline

        val wpis_go: WP_Individual_Step = WPIS_GenerateOutputs()
        val wpis_wi: WP_Individual_Step = WPIS_WriteInputs()
        val wpis_wb: WP_Individual_Step = WPIS_WriteBuffers()

        // TODO: Do something with op, like set in {@code pa} to proceed to next pipeline
        // TODO 10/18 this is a CK_Steps
        val pises = Helpers.List_of(wpis_go, wpis_wi, wpis_wb)
        val f = WP_Flow(this@WritePipeline, pa, pises)


        // comment
        //ops = f.act();
        val steps = CK_Steps {
            // TODO 10/18 I'm sure there is a better way to do this
            pises.stream()
                    .map { when (it) {
                        is WP_Individual_Step -> CK_Action { x,y -> it.execute(x, y )}
                        else -> it
                    } }
                    .collect(Collectors.toList())
        }

        // comment
        f.sc = WP_State_Control_1()

        val stepsContext: CK_StepsContext = f
        pa.runStepsNow(steps, stepsContext)

        pa.finishPipeline(this@WritePipeline, ops)
    }

    //WritePipeline_CK_StepsContext stepsContext = new WritePipeline_CK_StepsContext();
    override fun accept(aGenerateResultSupplier: Supplier<GenerateResult?>) {
        //final GenerateResult gr = aGenerateResultSupplier.get();
        val y = 2
    }

    fun consumer(): Consumer<Supplier<GenerateResult?>> {
        if (false) {
            return Consumer {
                // final GenerateResult gr = aGenerateResultSupplier.get();
            }
        }

        return Consumer { x: Supplier<GenerateResult?>? -> }
    }

    fun createOutputStratgy(): OutputStrategy {
        val os = OutputStrategy()
        os.per(OutputStrategy.Per.PER_CLASS) // TODO this needs to be configured per lsp

        return os
    }

    override fun gr_slot(gr1: GenerateResult) {
        Objects.requireNonNull(gr1)
        latch.notifyData(gr1)
        gr1.subscribeCompletedItems(cih.observer())
    }

    override fun run(aSt: CR_State, aOutput: CB_Output?) {
        latch.notifyLatch(true)
    }

    override fun finishPipeline_asString(): String {
        return this.javaClass.toString()
    }

    private class CompletedItemsHandler(private val sharedState: WritePipelineSharedState) {
        // README debugging purposes
        private val abs: MutableList<GenerateResultItem> = ArrayList()
        private val gris: Multimap<Dependency, GenerateResultItem> = ArrayListMultimap.create()
        private val LOG: ElLog
        private var observer: Observer<GenerateResultItem>? = null

        init {
            val verbosity: Verbosity = if (sharedState.c.cfg().silent) ElLog.Verbosity.SILENT
            else ElLog.Verbosity.VERBOSE

            LOG = ElLog_("(WRITE-PIPELINE)", verbosity, "(write-pipeline)")

            sharedState.pa.addLog(LOG)
        }

        fun addItem(ab: GenerateResultItem) {
            NotImplementedException.raise()

            // README debugging purposes
            abs.add(ab)

            LOG.info("-----------=-----------=-----------=-----------=-----------=-----------")
            LOG.info("GenerateResultItem >> " + ab.jsonString())
            LOG.info("abs.size >> " + abs.size)

            val dependency = ab.dependency

            LOG.info("ab.getDependency >> " + dependency.jsonString())

            // README debugging purposes
            val dependencyRef = dependency.ref

            LOG.info("dependencyRef >> " + (if (dependencyRef != null) dependencyRef.jsonString() else "null"))

            if (dependencyRef == null) {
                gris.put(dependency, ab)
            } else {
                val output = (dependencyRef as CDependencyRef).headerFile

                LOG.info("CDependencyRef.getHeaderFile >> $output")

                sharedState.mmb?.put(output, ab.buffer())
                sharedState.lsp_outputs?.put(ab.lsp().instructions, output)
                for (generateResultItem in gris[dependency]) {
                    val output1 = generateResultItem.output()
                    sharedState.mmb?.put(output1, generateResultItem.buffer())
                    sharedState.lsp_outputs?.put(generateResultItem.lsp().instructions, output1)
                }

                // for (Map.Entry<Dependency, Collection<GenerateResultItem>> entry :
                // gris.asMap().entrySet()) {
                // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4(entry.getKey().jsonString());
                // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4(entry.getValue());
                // }

                /*
				 * if (gris.containsKey(dependency)) tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4("*** 235 yes"); else
				 * tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4("*** 235 no");
				 */
                gris.removeAll(dependency)
            }

            LOG.info("-----------=-----------=-----------=-----------=-----------=-----------")
        }

        fun completeSequence() {
            val generateResult = sharedState.gr

            generateResult?.outputFiles { outputFiles: Map<String?, OutputFileC?> -> }
        }

        @Contract(mutates = "this")
        fun observer(): Observer<GenerateResultItem> {
            if (observer == null) {
                observer = object : Observer<GenerateResultItem> {
                    override fun onComplete() {
                        completeSequence()
                    }

                    override fun onError(e: Throwable) {
                    }

                    override fun onNext(ab: GenerateResultItem) {
                        addItem(ab)
                    }

                    override fun onSubscribe(d: Disposable) {
                    }
                }
            }

            return observer!!
        }
    }
} //
//
//

