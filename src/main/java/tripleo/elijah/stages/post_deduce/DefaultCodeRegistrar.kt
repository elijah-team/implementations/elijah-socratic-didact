package tripleo.elijah.stages.post_deduce

import tripleo.elijah.comp.Compilation
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.world.i.LivingRepo

class DefaultCodeRegistrar(private val compilation: Compilation) : ICodeRegistrar {
    private val livingRepo: LivingRepo
        get() = compilation.world()

    override fun registerClass(aClass: EvaClass) {
        livingRepo.addClass(aClass, LivingRepo.Add.MAIN_CLASS)
    }

    override fun registerClass1(aClass: EvaClass) {
        livingRepo.addClass(aClass, LivingRepo.Add.NONE)
    }

    override fun registerFunction(aFunction: BaseEvaFunction) {
        livingRepo.addFunction(aFunction, LivingRepo.Add.MAIN_FUNCTION)
    }

    override fun registerFunction1(aFunction: BaseEvaFunction) {
        livingRepo.addFunction(aFunction, LivingRepo.Add.NONE)
    }

    override fun registerNamespace(aNamespace: EvaNamespace) {
        livingRepo.addNamespace(aNamespace, LivingRepo.Add.NONE)
    }
}
