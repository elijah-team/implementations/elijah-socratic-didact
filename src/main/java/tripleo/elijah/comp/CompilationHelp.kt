/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.*
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation

// FIXME 10/18 move to Processbuilder (Steps)
internal class DStageProcess @Contract(pure = true) constructor(private val ca: ICompilationAccess, private val pr: ProcessRecord) : RuntimeProcess {
    override fun postProcess() {
    }

    override fun prepare() {
        // assert pr.stage == Stages.D; // FIXME
    }

    fun run_(aComp: Compilation?, st: CR_State?, output: CB_Output?) {
    }

    override fun run(aComp: Compilation0, ctx: RP_Context): Operation<Ok> {
        run_(aComp as Compilation, null, null)
        return Operation.success(Ok.instance())
    }
}

internal class EmptyProcess(aCompilationAccess: ICompilationAccess?, aPr: ProcessRecord?) : RuntimeProcess {
    override fun postProcess() {
    }

    override fun prepare() {
    }

    fun run_(aComp: Compilation?, st: CR_State?, output: CB_Output?) {
    }

    override fun run(aComp: Compilation0, ctx: RP_Context): Operation<Ok> {
        run_(aComp as Compilation, null, null)
        return Operation.success(Ok.instance())
    }
} //
//
//

