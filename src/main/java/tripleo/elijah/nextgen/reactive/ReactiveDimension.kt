package tripleo.elijah.nextgen.reactive

/**
 * The other base element of **Reactive**
 *
 * Implement this to have [Reactive]s notified of
 * (all) Dimensions.
 *
 * FYI where does one add [ReactiveDimension] ??
 * answer: CE#addReactiveDimension(...) in your constructor
 * - looks like a job for AutoService or sisu/goog.inject...
 * TODO add "when" directive/annotation/parameter etc
 * ie: #when(ReactiveEvent) ...
 * CE#addReactiveEvent(...)
 */
//@MarkerInterface
interface ReactiveDimension 
