package tripleo.elijah.stages.deduce.post_bytecode

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.stages.gen_fn.GenType

class SGTA_SetResolvedClass @Contract(pure = true) constructor(private val classStatement: ClassStatement) : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        gt.resolved = classStatement.oS_Type
    }
}
