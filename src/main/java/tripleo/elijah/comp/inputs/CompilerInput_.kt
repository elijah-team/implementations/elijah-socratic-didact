package tripleo.elijah.comp.inputs

import com.google.common.base.Preconditions
import lombok.Getter
import lombok.experimental.Accessors
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.i.ILazyCompilerInstructions
import tripleo.elijah.comp.inputs.CompilerInput.CompilerInputField
import tripleo.elijah.comp.queries.CompilerInstructions_Result
import tripleo.elijah.util.Maybe
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah.util2.__Extensionable
import tripleo.wrap.File
import java.util.*
import java.util.regex.Pattern

class CompilerInput_ : __Extensionable, CompilerInput {
    private val oc: Optional<Compilation>?

    //	@Getter
    private val inp: String
    private var accept_ci: Maybe<ILazyCompilerInstructions>? = null
    private var dir_carrier: File? = null

    @Getter
    @Accessors(fluent = true)
    private var ty: CompilerInput.Ty? = null
    private var hash: String? = null
    private var master: CompilerInputMaster? = null
    private var directoryResults: CompilerInstructions_Result? = null

    constructor(aS: String, aCompilation: Optional<Compilation>?) {
        inp = aS
        oc = aCompilation
    }

    constructor(aS: String) {
        inp = aS
        ty = CompilerInput.Ty.NULL
        oc = null
    }

    override fun getFile(): File {
        throw UnintendedUseException()
    }

    override fun getFileForDirectory(): File {
        val directory = File(inp)
        this.directory = directory
        return directory
    }

    override fun accept_ci(compilerInstructionsMaybe: Maybe<ILazyCompilerInstructions>) {
        accept_ci = compilerInstructionsMaybe

        if (master != null) master!!.notifyChange(this, CompilerInputField.ACCEPT_CI)
    }

    override fun accept_hash(hash: String) {
        this.hash = hash

        if (master != null) master!!.notifyChange(this, CompilerInputField.HASH)
    }

    override fun acceptance_ci(): Maybe<ILazyCompilerInstructions> {
        return accept_ci!!
    }

    override fun certifyRoot() {
        ty = CompilerInput.Ty.ROOT

        if (master != null) master!!.notifyChange(this, CompilerInputField.TY)
    }

    override fun getDirectory(): File {
        Preconditions.checkNotNull(dir_carrier)

        return dir_carrier!!
    }

    override fun setDirectory(f: File) {
        ty = CompilerInput.Ty.SOURCE_ROOT
        dir_carrier = f

        if (master != null) master!!.notifyChange(this, CompilerInputField.TY)
    }

    override fun isElijjahFile(): Boolean {
        return Pattern.matches(".+\\.elijjah$", inp) || Pattern.matches(".+\\.elijah$", inp)
    }

    override fun isEzFile(): Boolean {
        // new QuerySearchEzFiles.EzFilesFilter().accept()
        return Pattern.matches(".+\\.ez$", inp)
    }

    override fun isNull(): Boolean {
        return ty == CompilerInput.Ty.NULL
    }

    override fun isSourceRoot(): Boolean {
        return ty == CompilerInput.Ty.SOURCE_ROOT
    }

    override fun setArg() {
        ty = CompilerInput.Ty.ARG

        if (master != null) master!!.notifyChange(this, CompilerInputField.TY)
    }

    override fun setMaster(master: CompilerInputMaster) {
        this.master = master
    }

    override fun setSourceRoot() {
        ty = CompilerInput.Ty.SOURCE_ROOT

        if (master != null) master!!.notifyChange(this, CompilerInputField.TY)
    }

    override fun toString(): String {
        return "CompilerInput{ty=%s, inp='%s'}".formatted(ty, inp)
    }

    override fun ty(): CompilerInput.Ty {
        return if (ty != null) ty!! else CompilerInput.Ty.NULL
    }

    override fun makeFile(): File? {
        return when (ty) {
            CompilerInput.Ty.SOURCE_ROOT -> dir_carrier!!
            CompilerInput.Ty.ROOT -> File(File(inp).parentFile.wrapped())
            else -> null
        }
    }

    override fun getDirectoryResults(): CompilerInstructions_Result? = directoryResults

    override fun setDirectoryResults(aLoci: CompilerInstructions_Result) {
        this.directoryResults = aLoci

        for ((key) in aLoci.directoryResult2) {
            val focus = key.success()
            focus.advise(this)
        }

        if (master != null) master!!.notifyChange(this, CompilerInputField.DIRECTORY_RESULTS)
    }

    override fun getInp(): String {
        // aback and forth
        return inp
    }
}
