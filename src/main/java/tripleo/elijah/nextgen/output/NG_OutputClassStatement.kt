package tripleo.elijah.nextgen.output

import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.nextgen.inputtree.EIT_ModuleInput
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.inputtree.EIT_InputType
import tripleo.elijah.nextgen.inputtree.EIT_ModuleInputImpl
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.util.BufferTabbedOutputStream
import tripleo.util.buffer.Buffer

class NG_OutputClassStatement(aBufferTabbedOutputStream: BufferTabbedOutputStream,
                              aModuleDependency: OS_Module,
                              override val ty: TY) : NG_OutputStatement {
    private val buf: Buffer = aBufferTabbedOutputStream.buffer
    private val moduleDependency = NG_OutDep(aModuleDependency)

    override val explanation: EX_Explanation
        get() = EX_Explanation.Companion.withMessage("NG_OutputClassStatement")

    override val moduleInput: EIT_ModuleInput
        get() {
            val m = moduleDependency().module()

            val moduleInput: EIT_ModuleInput = EIT_ModuleInputImpl(m, m!!.compilation as Compilation, type = EIT_InputType.ELIJAH_SOURCE)
            return moduleInput
        }

    override val text: String
        get() = buf.text

    fun moduleDependency(): NG_OutDep {
        return moduleDependency
    }
}
