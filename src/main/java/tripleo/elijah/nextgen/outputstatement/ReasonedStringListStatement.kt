package tripleo.elijah.nextgen.outputstatement

import java.util.function.Supplier

open /*data*/ class ReasonedStringListStatement(override val explanation: EX_Explanation = default_explanation) : EG_Statement {
    private val rss: MutableList<IReasonedString> = ArrayList()


    override val text: String
        get() {
            val result = StringBuilder()
            for (reasonedString in rss) {
                result.append(reasonedString.text())
            }
            return result.toString()
        }

    fun append(aText: String, aReason: String) {
        rss.add(ReasonedString(aText, aReason))
    }

    fun append(aReasonedString: IReasonedString) {
        rss.add(aReasonedString)
    }

    fun append(aText: Supplier<String>, aReason: String) {
        rss.add(ReasonedSuppliedString(aText, aReason))
    }

    fun append(aText: EG_Statement, aReason: String) {
        rss.add(ReasonedStatementString(aText, aReason))
    }
}

private val default_explanation: EX_Explanation = EX_Explanation_withMessage("ReasonedStringListStatement")
