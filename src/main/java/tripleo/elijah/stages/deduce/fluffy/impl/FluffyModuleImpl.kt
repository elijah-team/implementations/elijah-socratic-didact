package tripleo.elijah.stages.deduce.fluffy.impl

import tripleo.elijah.comp.Compilation
import tripleo.elijah.entrypoints.MainClassEntryPoint
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.OS_ModuleImpl.Complaint
import tripleo.elijah.stages.deduce.fluffy.i.FluffyComp
import tripleo.elijah.stages.deduce.fluffy.i.FluffyLsp
import tripleo.elijah.stages.deduce.fluffy.i.FluffyMember
import tripleo.elijah.stages.deduce.fluffy.i.FluffyModule
import java.util.function.Consumer

class FluffyModuleImpl(private val module: OS_Module, private val compilation: Compilation?) : FluffyModule {
    internal class FluffyModuleImplInjector {
        fun new_MainClassEntryPoint(x: ClassStatement?): MainClassEntryPoint {
            return MainClassEntryPoint(x!!)
        }
    }

    private val __inj = FluffyModuleImplInjector()

    private fun _inj(): FluffyModuleImplInjector {
        return this.__inj
    }

    override fun find_all_entry_points() {
        //
        // FIND ALL ENTRY POINTS (should only be one per module)
        //
        val ccs = Consumer { x: ClassStatement? -> module.entryPoints().add(_inj().new_MainClassEntryPoint(x)) }

        module.items.stream().filter { item: ModuleItem? -> item is ClassStatement }
                .filter { classStatement: ModuleItem? -> MainClassEntryPoint.isMainClass((classStatement as ClassStatement?)!!) }
                .forEach { classStatement: ModuleItem -> faep_002(classStatement as ClassStatement, ccs) }
    }

    override fun find_multiple_items(aFluffyComp: FluffyComp, c: Complaint) {
        aFluffyComp.find_multiple_items(module, c)
    }

    override fun lsp(): FluffyLsp? {
        return null
    }

    override fun members(): List<FluffyMember>? {
        return null
    }

    override fun name(): String? {
        return null
    }

    companion object {
        /**
         * If classStatement is a "main class", send to consumer
         *
         * @param classStatement
         * @param ccs
         */
        private fun faep_002(classStatement: ClassStatement, ccs: Consumer<ClassStatement?>) {
            val x = classStatement.findFunction("main")
            val found = x.stream().filter { input: OS_Element2 -> FluffyCompImpl.Companion.isMainClassEntryPoint(input) }
                    .map<FunctionDef> { x7: OS_Element2 -> x7 as FunctionDef }

            //		final int eps = aModule.entryPoints.size();
            found.map { aFunctionDef: FunctionDef -> aFunctionDef.parent as ClassStatement? }.forEach(ccs)

            //		assert aModule.entryPoints.size() == eps || aModule.entryPoints.size() == eps+1; // TODO this will fail one day

//		tripleo.elijah.util.Stupidity.println2("243 " + entryPoints +" "+ _fileName);
//		break; // allow for "extend" class
        }
    }
}
