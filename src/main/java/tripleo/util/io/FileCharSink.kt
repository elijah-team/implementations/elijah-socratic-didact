/**
 *
 */
package tripleo.util.io

import tripleo.elijah.util.NotImplementedException
import java.io.IOException
import java.io.OutputStream

/**
 * @author Tripleo(sb)
 *
 *
 * Created 	Dec 9, 2019 at 3:23:57 PM
 */
class FileCharSink(private val fos: OutputStream) : DisposableCharSink {
    /* (non-Javadoc)
	 * @see tripleo.util.io.CharSink#accept(char)
	 */
    override fun accept(char1: Char) {
        try {
            fos.write(char1.code)
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
    }

    /* (non-Javadoc)
	 * @see tripleo.util.io.CharSink#accept(java.lang.String)
	 */
    override fun accept(string1: String) {
        try {
            fos.write(string1.toByteArray())
        } catch (e: IOException) {
            NotImplementedException.raise()
            //e.printStackTrace();
        }
    }

    override fun close() {
        try {
            fos.close()
        } catch (aE: IOException) {
            NotImplementedException.raise()
            //aE.printStackTrace();
        }
    }

    override fun dispose() {
        close()
    }
} //
//
//

