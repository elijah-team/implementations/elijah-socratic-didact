/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import lombok.Getter
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.i.ModuleListener
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.g.GWorldModule
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.reactive.ReactiveDimension
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.stages.logging.ElLog.Verbosity
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.work.WorkManager
import tripleo.elijah.work.WorkManager__

/**
 * Created 5/16/21 12:35 AM
 */
class GeneratePhase(// 24/01/04 back and forth
        @field:Getter val verbosity: Verbosity, private val pa: IPipelineAccess, private val pipelineLogic: PipelineLogic) : ReactiveDimension, ModuleListener {
    // 24/01/04 back and forth
    @Getter
    val wm: WorkManager = WorkManager__()
    private val generateFunctions: MutableMap<OS_Module, GenerateFunctions> = HashMap()

    // 24/01/04 back and forth
    @JvmField
    @Getter
    var codeRegistrar: ICodeRegistrar? = null

    init {
        pa.compilationEnclosure.addReactiveDimension(this)
        // pa.getCompilationEnclosure().addModuleListener(this);
    }

    override fun close() {
        NotImplementedException.raise_stop()
    }

    fun getGenerateFunctions(mod: OS_Module): GenerateFunctions {
        val Result: GenerateFunctions
        if (generateFunctions.containsKey(mod)) {
            Result = generateFunctions[mod]!!
        } else {
            Result = GenerateFunctions(mod, pipelineLogic, pa)
            generateFunctions[mod] = Result
        }
        return Result
    }

    override fun listen(module: GWorldModule) {
        //final GenerateFunctions x = getGenerateFunctions(module.module());
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

