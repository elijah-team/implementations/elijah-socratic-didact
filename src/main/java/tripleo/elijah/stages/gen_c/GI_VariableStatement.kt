package tripleo.elijah.stages.gen_c

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.lang.impl.VariableSequenceImpl
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_fn.IEvaFunctionBase
import tripleo.elijah.stages.gen_fn.IdentTableEntry
import tripleo.elijah.util.*

class GI_VariableStatement(private val variableStatement: VariableStatementImpl, private val repo: GI_Repo) : GenerateC_Item {
    private var _evaNode: EvaNode? = null
    private var item: CR_ReferenceItem? = null

    /**
     * Create a [tripleo.elijah.stages.gen_c.CReference.Reference] for
     * [this.variableStatement]
     *
     *
     *
     * If the parent of the variableStatemnt is the same as the generatedFunction's
     * parent, create a DIRECT_MEMBER with the string `value`
     *
     *
     *
     * If the parent of the variableStatemnt is the same as the generatedFunction,
     * create a LOCAL with no value
     *
     *
     *
     * Otherwise, create a MEMBER with the string `value` for value
     *
     */
    @Contract("_, _ -> new")
    private fun __createReferenceForVariableStatement(
            generatedFunction: IEvaFunctionBase, value: String?): CReference.Reference {
        val text2 = variableStatement.name

        // first getParent is VariableSequenceImpl
        val variableSequence = variableStatement.parent as VariableSequenceImpl?
        val parent = variableSequence!!.parent

        val fd = generatedFunction.fd

        if (parent === fd.parent) {
            // A direct member value. Doesn't handle when indirect
//				text = Emit.emit("/*124*/")+"vsc->vm" + text2;
            return CReference.Reference(text2, CReference.Ref.DIRECT_MEMBER, value)
        } else if (parent === fd) {
            return CReference.Reference(text2, CReference.Ref.LOCAL)
        }

        // if (parent instanceof NamespaceStatement) {
        // int y=2;
        // }
        return CReference.Reference(text2, CReference.Ref.MEMBER, value)
    }

    fun _createReferenceForVariableStatement(aCReference: CReference,
                                             generatedFunction: IEvaFunctionBase, value: String?) {
        val r = __createReferenceForVariableStatement(generatedFunction, value)
        aCReference.addRef(r)
    }

    override fun getEvaNode(): EvaNode {
        return _evaNode!!
    }

    val text: String
        get() {
            var text2 = variableStatement.name

            if (text2 == "argument_count_") {
                val pp = variableStatement.parent!!.parent

                if (pp is ClassStatement) {
                    assert(pp.name == "Arguments")
                    val cp = pp.getParent()

                    if (cp is OS_Module) {
                        assert(cp.fileName == "lib_elijjah/lib-c/Prelude.elijjah")
                    }

                    val gf = (item!!.tableEntry as IdentTableEntry).__gf
                    val dt2 = (item!!.tableEntry as IdentTableEntry)._deduceTypes2()

                    //				dt2._phase()._functionMap().asMap().entrySet().stream()
//					.filter(entry -> entry.)
                    val cs = dt2._zero().findClassesFor(pp)

                    assert(cs.size == 1)
                    val ec = cs[0]

                    val world = repo.generateC.ce.compilation.world()

                    //				var wcs = world.getClassesForClassStatement(cls);
//				
//				assert wcs.size() == 2;
//				
//				var lc = wcs.get(0);
//				
//				text2 = String.format("z%d%s", ec.getCode(), text2);
                    val wcs2 = world.getClassesForClassNamed("Main")

                    assert(wcs2.size == 2)
                    val lc2 = wcs2[0]

                    assert(lc2.element.packageName.name == "")
                    text2 = String.format("z%d%s", lc2.code, text2)

                    NotImplementedException.raise()

                    //				tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4(gf);
//				tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4(dt2);
                }
            }

            // TODO ExitSuccess, ExitCode
            return text2
        }

    override fun setEvaNode(a_evaNode: EvaNode) {
        _evaNode = a_evaNode
    }

    fun setItem(aItem: CR_ReferenceItem?) {
        item = aItem
    }
}
