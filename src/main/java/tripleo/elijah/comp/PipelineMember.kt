package tripleo.elijah.comp

import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.internal.CR_State

abstract class PipelineMember {
    @Throws(Exception::class)
    abstract fun run(aSt: CR_State, aOutput: CB_Output?)

    abstract fun finishPipeline_asString(): String
}
