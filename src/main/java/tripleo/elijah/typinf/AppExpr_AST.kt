package tripleo.elijah.typinf

import tripleo.elijah.util.Helpers
import java.util.stream.Collectors

/**
 * Application of a function to a sequence of arguments.
 *
 *
 * func is a node, args is a sequence of nodes.
 */
class AppExpr_AST(val func: AstNode, val args: List<AstNode>) : AstNode() {
    init {
        this._children = Helpers.List_of(this.func)
        (this._children as MutableList<AstNode>).addAll(args)
    }

    override fun toString(): String {
        return String.format("App(%s, [%s])", func,
                Helpers.String_join(", ", args
                        .stream()
                        .map { a: AstNode? -> a.toString() }
                        .collect(Collectors.toList())))
    }
}
