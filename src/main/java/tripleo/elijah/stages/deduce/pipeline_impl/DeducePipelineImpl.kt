package tripleo.elijah.stages.deduce.pipeline_impl

import tripleo.elijah.comp.i.extra.IPipelineAccess

class DeducePipelineImpl(private val pa: IPipelineAccess) {
    private val plrs: MutableList<PipelineLogicRunnable> = ArrayList()
    private val __inj = DeducePipelineImplInjector()

    init {
        addRunnable(_inj().new_PL_AddModules(pa))
        addRunnable(_inj().new_PL_EverythingBeforeGenerate())
        addRunnable(_inj().new_PL_SaveGeneratedClasses(pa))
    }

    private fun _inj(): DeducePipelineImplInjector {
        return __inj
    }

    private fun addRunnable(plr: PipelineLogicRunnable) {
        plrs.add(plr)
    }

    fun run() {
        val c = pa.compilation
        val compilationEnclosure = c.compilationEnclosure
        val pipelineLogic = compilationEnclosure.pipelineLogic!!

        for (plr in plrs) {
            plr.run(pipelineLogic)
        }
    }

    internal class DeducePipelineImplInjector {
        fun new_PL_AddModules(aPipelineAccess: IPipelineAccess): PipelineLogicRunnable {
            return PL_AddModules(aPipelineAccess)
        }

        fun new_PL_EverythingBeforeGenerate(): PipelineLogicRunnable {
            return PL_EverythingBeforeGenerate()
        }

        fun new_PL_SaveGeneratedClasses(aPa: IPipelineAccess): PipelineLogicRunnable {
            return PL_SaveGeneratedClasses(aPa)
        }
    }
}
