package tripleo.elijah.nextgen.outputstatement

import org.jetbrains.annotations.Contract

/**
 * @author Tripleo Nova
 */
class EG_CompoundStatement @Contract(pure = true) constructor(private val beginning: EG_SingleStatement,
                                                              private val ending: EG_SingleStatement,
                                                              private val middle: EG_Statement,
                                                              private val indent: Boolean,
                                                              override val explanation: EX_Explanation) : EG_Statement {
    override val text: String
        get() {
            val sb = beginning.text +
                    middle.text +
                    ending.text

            return sb
        }
}
