package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.gen_fn.GenType

class SGTA_SetClassInvocation : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        val ci = arena.get<ClassInvocation>("ci")
        gt.ci = ci
    }
}
