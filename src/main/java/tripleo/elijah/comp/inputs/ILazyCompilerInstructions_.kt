package tripleo.elijah.comp.inputs

import org.jetbrains.annotations.Contract
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.graph.i.CK_SourceFile
import tripleo.elijah.comp.i.CompilationClosure
import tripleo.elijah.comp.i.ILazyCompilerInstructions
import tripleo.elijah.comp.nextgen.wonka.CK_SourceFileFactory
import tripleo.elijah.util.Mode
import tripleo.elijah.util.Operation2
import tripleo.elijah.util2.UnintendedUseException
import tripleo.wrap.File

object ILazyCompilerInstructions_ {
    @JvmStatic
    @Contract(value = "_, _ -> new", pure = true)
    fun of(input: CompilerInput,
           cc: CompilationClosure): ILazyCompilerInstructions {
        val file_name = input.inp
        val f = File(file_name)

        return object : ILazyCompilerInstructions {
            private var operation: Operation2<CompilerInstructions>? = null

            override fun get(): CompilerInstructions {
                // 1. Ask the factory
                // 2. "Associate" our givens
                // 3. Ask the source file to process
                // 4. Just return on success
                // 5. Return null for failure

                val sf: CK_SourceFile<CompilerInstructions> = CK_SourceFileFactory.get(f, CK_SourceFileFactory.K.SpecifiedEzFile) as CK_SourceFile<CompilerInstructions>
                sf.associate(input, cc)
                operation = sf.process_query()

                if (operation!!.mode() == Mode.SUCCESS) {
                    val parsed = operation!!.success()
                    return parsed
                }

                throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
            }

            override fun getOperation(): Operation2<CompilerInstructions>? {
                return operation
            }
        }
    }

    @JvmStatic
    @Contract(value = "_ -> new", pure = true)
    fun of(aCompilerInstructions: CompilerInstructions): ILazyCompilerInstructions {
        return object : ILazyCompilerInstructions {
            override fun get(): CompilerInstructions {
                return aCompilerInstructions
            }

            override fun getOperation(): Operation2<CompilerInstructions>? {
                return null
            }
        }
    }
}
