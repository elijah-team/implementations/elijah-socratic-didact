package tripleo.elijah.comp.impl

import tripleo.elijah.comp.i.CB_Action
import tripleo.elijah.comp.i.CB_Process
import tripleo.elijah.util.Helpers

class SingleActionProcess(// README tape
        private val a: CB_Action, private val name: String) : CB_Process {
    override fun steps(): List<CB_Action> {
        return Helpers.List_of(a)
    }

    override fun name(): String {
        return name //"SingleActionProcess";
    }
}
