package tripleo.elijah.nextgen.outputtree

import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.inputtree.EIT_Input_HashSourceFile_Triple
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.util.Helpers

data class EOT_OutputFileImpl(
		override val inputs: List<EIT_Input?>,
		val filename__: EOT_FileNameProvider,
		override val type: EOT_OutputType, // TODO List<?> ??
		override val statementSequence: EG_Statement,
) : EOT_OutputFile {
	class DefaultFileNameProvider(private val r: String) : EOT_FileNameProvider {
		override fun getFilename(): String {
			return r
		}
	}

	//    private val _inputs: MutableList<EIT_Input?> = ArrayList()
	@JvmField
	var x: List<EIT_Input_HashSourceFile_Triple>? = null

	init {
		//_inputs.addAll(inputs!!)
	}

	override val filename: String
		get() = filename__.getFilename()

//    constructor(inputs: List<EIT_Input?>, filename: String,
//                type: EOT_OutputType, sequence: EG_Statement) : this(inputs, DefaultFileNameProvider(filename), type, sequence)

//    override val inputs: List<EIT_Input?>
//        get() = _inputs
//    override val statementSequence: EG_Statement
//        get() = super.statementSequence
//    override val type: EOT_OutputType
//        get() = TODO("Not yet implemented")

	override fun toString(): String {
		return "(%s) '%s'".formatted(type, filename__.getFilename())
	} // rules/constraints whatever
}

//private fun EOT_OutputFileImpl(filename__: EOT_FileNameProvider, type: String, statementSequence: EOT_OutputType, seq: EG_Statement): EOT_OutputFileImpl {
//	return EOT_OutputFileImpl(Helpers.List_of(), filename__.getFilename(), statementSequence, seq)
//}
