package tripleo.elijah.stages.deduce

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.stages.logging.ElLog
import java.util.*

class DT_Env(private val LOG: ElLog, private val errSink: ErrSink, private val central: DeduceCentral) {
    fun LOG(): ElLog {
        return LOG
    }

    fun errSink(): ErrSink {
        return errSink
    }

    fun central(): DeduceCentral {
        return central
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as DT_Env
        return this.LOG == that.LOG && (this.errSink == that.errSink) && (this.central == that.central)
    }

    override fun hashCode(): Int {
        return Objects.hash(LOG, errSink, central)
    }

    override fun toString(): String {
        return "DT_Env[" +
                "LOG=" + LOG + ", " +
                "errSink=" + errSink + ", " +
                "central=" + central + ']'
    }
}
