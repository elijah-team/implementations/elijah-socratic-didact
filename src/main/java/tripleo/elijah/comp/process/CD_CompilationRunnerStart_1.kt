package tripleo.elijah.comp.process

import lombok.Getter
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.graph.i.CK_AbstractStepsContext
import tripleo.elijah.comp.graph.i.CK_Action
import tripleo.elijah.comp.graph.i.CK_Steps
import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.CR_Action
import tripleo.elijah.comp.internal.CD_CompilationRunnerStart
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation
import tripleo.elijah.util2.UnintendedUseException

class CD_CompilationRunnerStart_1 : CD_CompilationRunnerStart {
    // 24/01/04 back and forth
    @Getter
    var crActionResultList: MutableList<Operation<Ok>>? = null
        private set

    @Getter
    private var stepContext: CK_AbstractStepsContext? = null

    override fun start(aRootCI: CompilerInstructions,
                       crState: CR_State,
                       out: CB_Output) {
        //final @NotNull CK_Monitor monitor = compilationEnclosure.getDefaultMonitor();

        stepContext = CD_CRS_StepsContext(crState, out)

        val stepActions: MutableList<CK_Action> = ArrayList()
        stepActions.addAll(getCrActions(aRootCI))

        val crActionList: List<CR_Action> = ArrayList()
        for (action in crActionList) {
            stepActions.add(CD_CRS_CK_Action(this, action))
        }

        val steps = CK_Steps { stepActions }

        crActionResultList = ArrayList(crActionList.size)

        val compilationEnclosure = crState.compilationEnclosure

        compilationEnclosure.compilation.contribute(CPX_RunStepsContribution(steps, stepContext as CD_CRS_StepsContext))

        compilationEnclosure.compilation.signals()
                .subscribeRunStepLoop { throw UnintendedUseException("breakpoint") }
    }

    private fun getCrActions(aRootCI: CompilerInstructions): List<CK_Action> {
        // TODO 10/20 remove k2 ??
        val k2 = CK_ProcessInitialAction(aRootCI)
        val k3 = CK_AlmostComplete()
        val k4 = CK_RunBetterAction()

        val al = Helpers.List_of(k2, k3, k4)

        return al
    }
} //
//
//

