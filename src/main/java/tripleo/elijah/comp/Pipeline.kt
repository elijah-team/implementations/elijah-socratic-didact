/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp

import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.RP_Context
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.g.GPipeline
import tripleo.elijah.util2.__Extensionable

/**
 * Created 8/21/21 10:09 PM
 */
class Pipeline : GPipeline {
    private val pls: MutableList<PipelineMember> = ArrayList()

    fun add(aPipelineMember: PipelineMember) {
        pls.add(aPipelineMember)
    }

    @Throws(Exception::class)
    fun run(aSt: CR_State, aOutput: CB_Output?) {
        for (pl in pls) {
            pl.run(aSt, aOutput)
        }
    }

    fun size(): Int {
        return pls.size
    }

    class RP_Context_1(aSt: CR_State?, aOutput: CB_Output?) : __Extensionable(), RP_Context {
        init {
            putExt(CR_State::class.java, aSt!!)
            putExt(CB_Output::class.java, aOutput!!)
        }

        val state: CR_State
            get() =// README this is only necessary in an object-oriented fantasy world
                    //  Honestly, it was done to remove casting, but here we are anyway
                getExt(CR_State::class.java) as CR_State

        val context: CB_Output
            get() = getExt(CB_Output::class.java) as CB_Output
    }
} //
//
//

