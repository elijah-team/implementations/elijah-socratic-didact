package tripleo.elijah.stages.deduce.post_bytecode

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.NamespaceStatement
import tripleo.elijah.stages.gen_fn.GenType

class SGTA_SetResolvedNamespace @Contract(pure = true) constructor(private val namespaceStatement: NamespaceStatement) : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        gt.resolvedn = namespaceStatement
    }
}
