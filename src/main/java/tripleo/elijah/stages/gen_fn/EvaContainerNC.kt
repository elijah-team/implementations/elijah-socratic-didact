/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Locatable
import tripleo.elijah.lang.i.AccessNotation
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.i.VariableStatement
import tripleo.elijah.stages.deduce.FunctionMapDeferred
import tripleo.elijah.stages.deduce.RegisterClassInvocation_env
import tripleo.elijah.stages.gen_fn.EvaContainer.VarTableEntry
import tripleo.elijah.stages.gen_generic.CodeGenerator
import tripleo.elijah.stages.gen_generic.Dependency
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.gen_generic.IDependencyReferent
import tripleo.elijah.util.Maybe
import tripleo.elijah.util2.UnintendedUseException
import java.io.PrintStream

/**
 * Created 3/16/21 10:45 AM
 */
abstract class EvaContainerNC : AbstractDependencyTracker(), EvaContainer, IDependencyReferent {
    internal class VarNotFound : Diagnostic {
        override fun code(): String? {
            throw UnintendedUseException("just make it compile")
        }

        override fun primary(): Locatable {
            throw UnintendedUseException("just make it compile")
        }

        override fun report(stream: PrintStream) {
        }

        override fun secondary(): List<Locatable?> {
            throw UnintendedUseException("just make it compile")
        }

        override fun severity(): Diagnostic.Severity? {
            throw UnintendedUseException("just make it compile")
        }
    }

    @JvmField
    val dependency: Dependency = Dependency(this)

    @JvmField
    var classMap: MutableMap<ClassStatement, EvaClass> = HashMap()

    @JvmField
    var functionMap: MutableMap<FunctionDef, EvaFunction> = HashMap()

    @JvmField
    var varTable: MutableList<VarTableEntry> = ArrayList()

    @JvmField
    var functionMapDeferreds: Multimap<FunctionDef, FunctionMapDeferred> = ArrayListMultimap.create()

    @get:Deprecated("")
    @set:Deprecated("")
    abstract var code: Int

    fun addClass(aClassStatement: ClassStatement, aEvaClass: EvaClass) {
        classMap[aClassStatement] = aEvaClass
    }

    fun addFunction(generatedFunction: EvaFunction) {
        val functionDef = generatedFunction.fd

        check(!functionMap.containsKey(functionDef)) { "Function already generated" } // TODO there can be overloads, although we


        // don't handle that yet
        functionMap[functionDef] = generatedFunction
        functionMapDeferreds[functionDef].stream().forEach { deferred: FunctionMapDeferred -> deferred.onNotify(generatedFunction) }
    }

    fun addVarTableEntry(
        an: AccessNotation?, vs: VariableStatement,
        aPassthruEnv: RegisterClassInvocation_env?
    ) {
        // TODO dont ignore AccessNotationImpl
        varTable.add(VarTableEntry(vs, vs.nameToken, vs.initialValue(), vs.typeName(),
            vs.parent!!.parent, aPassthruEnv))
    }

    fun functionMapDeferred(aFunctionDef: FunctionDef, aFunctionMapDeferred: FunctionMapDeferred) {
        functionMapDeferreds.put(aFunctionDef, aFunctionMapDeferred)
    }

    abstract fun generateCode(aFileGen: GenerateResultEnv?, aGgc: CodeGenerator)

    /**
     * Get a [EvaFunction]
     *
     * @param fd the function searching for
     * @return null if no such key exists
     */
    fun getFunction(fd: FunctionDef): EvaFunction? {
        return functionMap[fd]
    }

    override fun getVariable(aVarName: String): Maybe<VarTableEntry> {
        for (varTableEntry in varTable) {
            if (varTableEntry.nameToken.text == aVarName) return Maybe.of(varTableEntry)
        }
        return Maybe.of_exc(_def_VarNotFound)
    }

    companion object {
        var _def_VarNotFound: Diagnostic = VarNotFound()
    }

	abstract fun getGn(): GNCoded
//    abstract fun genGn(): GNCoded
}
