package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.stages.deduce.DeducePhase.GeneratedClasses
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.NamespaceInvocation
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.GenerateFunctions
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.util2.Eventual
import tripleo.elijah.work.WorkList
import tripleo.elijah.work.WorkManager
import java.util.function.Consumer
import java.util.function.Supplier

class __LFOE_Q(aWorkManager: WorkManager?, private val wl: WorkList, private val deduceTypes2: DeduceTypes2) : _LFOE_Q {
    private val generateFunctions = deduceTypes2.phase.generatePhase.getGenerateFunctions(deduceTypes2.module)

    override fun enqueue_ctor(generateFunctions1: GenerateFunctions, fi: FunctionInvocation,
                              aConstructorName: IdentExpression) {
        // assert generateFunctions1 == generateFunctions;
        val wlgf = deduceTypes2._inj().new_WlGenerateCtor(generateFunctions, fi, aConstructorName,
                deduceTypes2.phase.codeRegistrar)
        wl.addJob(wlgf)
    }

    override fun enqueue_default_ctor(generateFunctions1: GenerateFunctions,
                                      fi: FunctionInvocation,
                                      cef: Consumer<Eventual<BaseEvaFunction>>) {
        // assert generateFunctions1 == generateFunctions;
        val codeRegistrar = deduceTypes2._phase().codeRegistrar
        enqueue_default_ctor(generateFunctions1, fi, codeRegistrar, cef)
    }

    override fun enqueue_default_ctor(generateFunctions1: GenerateFunctions,
                                      fi: FunctionInvocation,
                                      codeRegistrar: ICodeRegistrar,
                                      cef: Consumer<Eventual<BaseEvaFunction>>) {
        assert(generateFunctions1 === generateFunctions)
        val wlgf = deduceTypes2._inj().new_WlGenerateDefaultCtor(generateFunctions,
                fi,
                deduceTypes2.creationContext(),
                codeRegistrar)

        val x = wlgf.generated
        if (cef != null) cef.accept(x)

        wl.addJob(wlgf)
    }

    override fun enqueue_function(generateFunctions1: GenerateFunctions, fi: FunctionInvocation,
                                  cr: ICodeRegistrar) {
        // assert generateFunctions1 == generateFunctions;
        val wlgf = deduceTypes2._inj().new_WlGenerateFunction(generateFunctions, fi, cr)
        wl.addJob(wlgf)
    }

    override fun enqueue_function(som: Supplier<GenerateFunctions>,
                                  aFi: FunctionInvocation, cr: ICodeRegistrar) {
        val wlgf = deduceTypes2._inj().new_WlGenerateFunction(som.get(), aFi, cr)
        wl.addJob(wlgf)
    }

    override fun enqueue_namespace(som: Supplier<GenerateFunctions>,
                                   aNsi: NamespaceInvocation, aGeneratedClasses: GeneratedClasses,
                                   aCr: ICodeRegistrar) {
        wl.addJob(deduceTypes2._inj().new_WlGenerateNamespace(som.get(), aNsi, aGeneratedClasses, aCr))
    }
}
