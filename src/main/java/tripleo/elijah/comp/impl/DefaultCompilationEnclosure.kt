package tripleo.elijah.comp.impl

import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.ReplaySubject
import io.reactivex.rxjava3.subjects.Subject
import org.apache.commons.lang3.tuple.Pair
import org.apache.commons.lang3.tuple.Triple
import org.jdeferred2.DoneCallback
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.AccessBus
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.PipelineMember
import tripleo.elijah.comp.generated.__Plugins.*
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.graph.i.CK_Steps
import tripleo.elijah.comp.graph.i.CK_StepsContext
import tripleo.elijah.comp.i.*
import tripleo.elijah.comp.i.extra.ICompilationRunner
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.internal.CompilationImpl
import tripleo.elijah.comp.internal.CompilationRunner
import tripleo.elijah.comp.internal.PipelinePlugin
import tripleo.elijah.comp.internal.Provenance
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.nextgen.CK_DefaultStepRunner
import tripleo.elijah.comp.nextgen.i.AsseverationLogProgress
import tripleo.elijah.comp.notation.GN_WriteLogs
import tripleo.elijah.comp.scaffold.CB_ListBackedOutput
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.g.GModuleThing
import tripleo.elijah.g.GOS_Module
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.inputtree.EIT_ModuleList
import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.nextgen.reactive.Reactivable
import tripleo.elijah.nextgen.reactive.Reactive
import tripleo.elijah.nextgen.reactive.ReactiveDimension
import tripleo.elijah.pre_world.Mirror_EntryPoint
import tripleo.elijah.stages.gen_fn.IClassGenerator
import tripleo.elijah.stages.generate.OutputStrategyC.*
import tripleo.elijah.stages.inter.ModuleThing
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.write_stage.pipeline_impl.NG_OutputRequest
import tripleo.elijah.util.CompletableProcess
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.Eventual
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah.world.i.WorldModule
import java.util.*
import java.util.function.Consumer
import java.util.function.Function
import java.util.function.Supplier

class DefaultCompilationEnclosure(private val compilation: Compilation) : CompilationEnclosure {
	val pipelineAccessPromise_: Eventual<IPipelineAccess> = Eventual()
	private val ecr = Eventual<ICompilationRunner>()
	private val accessBusPromise = Eventual<AccessBus>()
	private val _cbOutput: CB_Output = CB_ListBackedOutput()
	private val pipelinePlugins: MutableMap<String, PipelinePlugin> = HashMap()
	private val moduleThings: MutableMap<OS_Module, ModuleThing> = HashMap()
	private val dimensionSubject: Subject<ReactiveDimension> = ReplaySubject.create()
	private val reactivableSubject: Subject<Reactivable> = ReplaySubject.create()
	private val elLogs: MutableList<ElLog> = LinkedList()
	private val _moduleListeners: MutableList<ModuleListener> = ArrayList()
	private val outFileAssertions: MutableList<Triple<AssOutFile, EOT_FileNameProvider, NG_OutputRequest>> = ArrayList()
	private val ofa = OFA()
	private val dimensionObserver: Observer<ReactiveDimension> = _ReactiveDimensionObserver()
	private val reactivableObserver: Observer<Reactivable> = _ReactivableObserver()
	private lateinit var ab: AccessBus //= null
	private var ca: ICompilationAccess? = null
	private var compilationBus: ICompilationBus? = null
	private lateinit var compilationRunner: ICompilationRunner //= null
	private var compilerDriver: CompilerDriver? = null
	private var inp: List<CompilerInput>? = null
	private var pa: IPipelineAccess? = null
	private var pipelineLogic: PipelineLogic? = null
	private val _p_CompilationBus = Eventual<ICompilationBus>()

	init {
		getPipelineAccessPromise().then { pa: IPipelineAccess ->
			val ce = compilation.compilationEnclosure
			ab = AccessBus(getCompilation(), pa)

			accessBusPromise.resolve(ab)

			// README these need pipeline access to be created. wanted ce, but went with that
			ce.addPipelinePlugin(LawabidingcitizenPipelinePlugin())
			ce.addPipelinePlugin(EvaPipelinePlugin())
			ce.addPipelinePlugin(DeducePipelinePlugin())
			ce.addPipelinePlugin(WritePipelinePlugin())
			ce.addPipelinePlugin(WriteMakefilePipelinePlugin())
			ce.addPipelinePlugin(WriteMesonPipelinePlugin())
			ce.addPipelinePlugin(WriteOutputTreePipelinePlugin())

			pa._setAccessBus(ab)
			this.pa = pa
		}

		getPipelineAccessPromise().then { pa: IPipelineAccess ->
			val ce = compilation.compilationEnclosure
			ce.waitCompilationRunner { compilationRunner1: CompilationRunner -> compilationRunner = compilationRunner1 }

			compilation.world().addModuleProcess(ModuleListener_ModuleCompletableProcess())
		}
	}

	//	@Override
	override fun addEntryPoint(aMirrorEntryPoint: Mirror_EntryPoint, dcg: IClassGenerator) {
		aMirrorEntryPoint.generate(dcg)
	}

	override fun addModuleListener(aModuleListener: ModuleListener) {
		_moduleListeners.add(aModuleListener)
	}

	override fun addModuleThing(aMod: OS_Module): ModuleThing {
		val mt = ModuleThing(aMod)
		moduleThings[aMod] = mt
		return mt
	}

	override fun addReactive(r: Reactivable) {
		val y = 2
		// reactivableObserver.onNext(r);
		reactivableSubject.onNext(r)

		// reactivableObserver.
		dimensionSubject.subscribe(object : Observer<ReactiveDimension> {
			override fun onSubscribe(d: Disposable) {
			}

			override fun onNext(aReactiveDimension: ReactiveDimension) {
				// r.join(aReactiveDimension);
				r.respondTo(aReactiveDimension)
			}

			override fun onError(e: Throwable) {
				e.printStackTrace()
			}

			override fun onComplete() {
			}
		})
	}

	override fun addReactive(r: Reactive) {
		dimensionSubject.subscribe(object : Observer<ReactiveDimension> {
			override fun onSubscribe(d: Disposable) {
			}

			override fun onNext(dim: ReactiveDimension) {
				r.join(dim)
			}

			override fun onError(e: Throwable) {
				e.printStackTrace()
			}

			override fun onComplete() {
			}
		})
	}

	override fun addReactiveDimension(aReactiveDimension: ReactiveDimension) {
		dimensionSubject.onNext(aReactiveDimension)

		reactivableSubject.subscribe(object : Observer<Reactivable> {
			override fun onSubscribe(d: Disposable) {
			}

			override fun onNext(aReactivable: Reactivable) {
				addReactive(aReactivable)
			}

			override fun onError(e: Throwable) {
				e.printStackTrace()
			}

			override fun onComplete() {
			}
		})

		//		aReactiveDimension.setReactiveSink(addReactive);
	}

	override fun AssertOutFile(aOutputRequest: NG_OutputRequest) {
		val fileName = aOutputRequest.fileName()
		if (fileName is OSC_NFC) {
			AssertOutFile_Class(fileName, aOutputRequest)
		} else if (fileName is OSC_NFF) {
			AssertOutFile_Function(fileName, aOutputRequest)
		} else if (fileName is OSC_NFN) {
			AssertOutFile_Namespace(fileName, aOutputRequest)
		} else {
			throw NotImplementedException()
		}
	}

	override fun getAccessBusPromise(): Eventual<AccessBus> {
		return accessBusPromise
	}

	@Contract(pure = true)
	override fun getCB_Output(): CB_Output {
		return this._cbOutput
	}

	@Contract(pure = true)
	override fun getCompilation(): Compilation {
		return compilation
	}

	@Contract(pure = true)
	override fun getCompilationAccess(): ICompilationAccess {
		return ca!!
	}

	override fun provideCompilationAccess(aca: ICompilationAccess): Boolean {
		return provideCompilationAccess { aca }
	}

	override fun provideCompilationAccess(aSca: Supplier<ICompilationAccess>): Boolean {
		if (ca != null) return false
		ca = aSca.get()
		return true
	}

	@Contract(pure = true)
	override fun getCompilationBus(): ICompilationBus {
		return compilationBus!!
	}

	override fun setCompilationBus(aCompilationBus: ICompilationBus) {
		compilationBus = aCompilationBus
		//		_p_CompilationBus.resolve(aCompilationBus);
	}

	override fun getCompilationClosure(): CompilationClosure {
		return getCompilation().compilationClosure
	}

	@Contract(pure = true)
	override fun getCompilationDriver(): CompilerDriver {
		return getCompilationBus().compilerDriver
	}

	@Contract(pure = true)
	override fun getCompilationRunner(): ICompilationRunner {
		return compilationRunner
	}

//	override fun waitCompilationRunner(crcb: DoneCallback<ICompilationRunner>) {
//		ecr.then(crcb)
//	}

	override fun setCompilationRunner(aCompilationRunner: ICompilationRunner) {
		compilationRunner = aCompilationRunner

		if (ecr.isPending) {
			ecr.resolve(compilationRunner)
		} else {
			SimplePrintLoggerToRemoveSoon.println_err_4("903365 compilationRunner already set")
		}
	}

	@Contract(pure = true)
	override fun getCompilerInput(): List<CompilerInput> {
		return inp!!
	}

	override fun setCompilerInput(aInputs: List<CompilerInput>) {
		//assert inp == null;

		inp = aInputs
	}

	override fun getModuleThing(aMod: OS_Module): ModuleThing {
		if (moduleThings.containsKey(aMod)) {
			return moduleThings[aMod]!!
		}
		return addModuleThing(aMod)
	}

	@Contract(pure = true)
	override fun getPipelineAccess(): IPipelineAccess {
		return pa!!
	}

	@Contract(pure = true)
	override fun getPipelineAccessPromise(): Eventual<IPipelineAccess> {
		return pipelineAccessPromise_
	}

	@Contract(pure = true)
	override fun getPipelineLogic(): PipelineLogic? {
		return pipelineLogic
	}

	override fun setPipelineLogic(aPipelineLogic: PipelineLogic) {
		pipelineLogic = aPipelineLogic
//
//		pipelineAccessPromise.then { pa: IPipelineAccess -> pa.resolvePipelinePromise(aPipelineLogic) }
	}

	override fun logProgress(aCompProgress: CompProgress, x: Any) {
		aCompProgress.deprecated_print(x, System.out, System.err)
	}

	override fun noteAccept(aWorldModule: WorldModule) {
		val mod = aWorldModule.module()
		//var aMt = EventualExtract.of(aWorldModule.getErq()).mt();
		SimplePrintLoggerToRemoveSoon.println_err_4(mod)
		//tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4(aMt);
	}

	override fun OutputFileAsserts(): OFA {
		return ofa
	}

	override fun reactiveJoin(aReactive: Reactive) {
		// throw new IllegalStateException("Error");

		// aReactive.join();

		SimplePrintLoggerToRemoveSoon.println_err_4("reactiveJoin $aReactive")
	}

	override fun setCompilerDriver(aCompilerDriver: CompilerDriver) {
		compilerDriver = aCompilerDriver
	}

	override fun AssertOutFile_Class(aNfc: OSC_NFC, aOutputRequest: NG_OutputRequest) {
		outFileAssertions.add(Triple.of(AssOutFile.CLASS, aNfc, aOutputRequest))
	}

	override fun AssertOutFile_Function(aNff: OSC_NFF, aOutputRequest: NG_OutputRequest) {
		outFileAssertions.add(Triple.of(AssOutFile.FUNCTION, aNff, aOutputRequest))
	}

	override fun AssertOutFile_Namespace(aNfn: OSC_NFN, aOutputRequest: NG_OutputRequest) {
		outFileAssertions.add(Triple.of(AssOutFile.NAMESPACE, aNfn, aOutputRequest))
	}

	override fun _resolvePipelineAccessPromise(aPa: IPipelineAccess) {
		if (!pipelineAccessPromise_.isResolved) { // FIXME 10/18 ugh
			pipelineAccessPromise_.resolve(aPa)
		}
	}

	override fun waitCompilationRunner(ccr: Consumer<CompilationRunner>) {
		ecr.then { t: ICompilationRunner -> ccr.accept(t as CompilationRunner) }
	}

	override fun logProgress2(aCompProgress: CompProgress, alp: AsseverationLogProgress) {
		alp.call(System.out, System.err)
	}

	override fun getDefaultMonitor(): CK_Monitor {
		return (compilation as CompilationImpl).defaultMonitor
	}

	override fun runStepsNow(aSteps: CK_Steps, aStepContext: CK_StepsContext) {
		val monitor = defaultMonitor
		CK_DefaultStepRunner.runStepsNow(aSteps, aStepContext, monitor)
	}

	override fun getPipelinePlugin(aPipelineName: String): PipelinePlugin {
		if (pipelinePlugins.containsKey(aPipelineName)) {
			return pipelinePlugins[aPipelineName]!!
		}

		throw IllegalStateException("implement me when you are feeling unfulfilled")
	}

	override fun addPipelinePlugin(aCr: Function<GPipelineAccess, PipelineMember>) {
		pipelineAccessPromise_.then { pa: IPipelineAccess ->
			val plugin = aCr.apply(pa) as PipelinePlugin
			pipelinePlugins[plugin.name()] = plugin
		}
	}

	override fun addPipelinePlugin(aPlugin: PipelinePlugin) {
		pipelinePlugins[aPlugin.name()] = aPlugin
	}

	override fun writeLogs() {
		val pipelineLogic = this.getPipelineLogic()
		val notable = GN_WriteLogs(this.compilationAccess, pipelineLogic?._pa()?.compilationEnclosure?.logs!!)

		pa!!.notate(Provenance.DefaultCompilationAccess__writeLogs, notable)
	}

	override fun addLog(aLOG: ElLog) {
		elLogs.add(aLOG)
	}

	override fun getLogs(): List<ElLog> {
		return elLogs
	}

	override fun getModuleList(): EIT_ModuleList {
		return compilation.objectTree.moduleList
	}

	override fun onCompilationBus(cb: DoneCallback<ICompilationBus>) {
		_p_CompilationBus.then(cb)
	}

	override fun provideCompilationBus(aScb: Supplier<ICompilationBus>): Boolean {
		if (compilationBus != null) return false
		compilationBus = aScb.get()
		return true
	}

	override fun provideCompilationRunner(aScr: Supplier<ICompilationRunner>): Boolean {
		// https://stackoverflow.com/a/57597536
		if (::compilationRunner.isInitialized) return false
		setCompilationRunner(aScr.get())
		return true
	}

	override fun addModuleThing(aModule: GOS_Module): GModuleThing {
		return addModuleThing(aModule as OS_Module)
	}

	override fun logProgress(aCompProgress: CompProgress, aCodeMessagePair: Pair<Int, String>) {
		aCompProgress.deprecated_print(aCodeMessagePair, System.out, System.err)
	}

	override fun getModuleThing(aModule: GOS_Module): GModuleThing {
		return getModuleThing(aModule as OS_Module)
	}

	private class _ReactiveDimensionObserver : Observer<ReactiveDimension> {
		override fun onSubscribe(d: Disposable) {
			throw UnintendedUseException()
		}

		override fun onNext(aReactiveDimension: ReactiveDimension) {
			// aReactiveDimension.observe();
			throw UnintendedUseException()
		}

		override fun onError(e: Throwable) {
			throw UnintendedUseException()
		}

		override fun onComplete() {
			throw UnintendedUseException()
		}
	}

	private class _ReactivableObserver : Observer<Reactivable> {
		override fun onSubscribe(d: Disposable) {
			throw UnintendedUseException()
		}

		override fun onNext(aReactivable: Reactivable) {
//			ReplaySubject
			throw UnintendedUseException()
		}

		override fun onError(e: Throwable) {
			throw UnintendedUseException()
		}

		override fun onComplete() {
			throw UnintendedUseException()
		}
	}

	private inner class ModuleListener_ModuleCompletableProcess : CompletableProcess<WorldModule?> {
		override fun add(item: WorldModule?) {
//			tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("[ModuleListener_ModuleCompletableProcess] add " + item.module().getFileName());

			// TODO Reactive pattern (aka something ala ReplaySubject)

			for (moduleListener in _moduleListeners) {
				moduleListener.listen(item)
			}
		}

		override fun complete() {
			// 09/26 tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("[ModuleListener_ModuleCompletableProcess] complete");

			// TODO Reactive pattern (aka something ala ReplaySubject)

			for (moduleListener in _moduleListeners) {
				moduleListener.close()
			}
		}

		override fun error(d: Diagnostic) {
		}

		override fun preComplete() {
//			tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("[ModuleListener_ModuleCompletableProcess] preComplete");
		}

		override fun start() {
//			tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("[ModuleListener_ModuleCompletableProcess] start");
		}
	}

	inner class OFA : Iterable<Triple<AssOutFile, EOT_FileNameProvider, NG_OutputRequest>> {
		// public OFA(final List<Triple<AssOutFile, EOT_OutputFile.FileNameProvider,
		// NG_OutputRequest>> aOutFileAssertions) {
		// _l = aOutFileAssertions;
		// }
		fun contains(aFileName: String): Boolean {
			for (outFileAssertion in outFileAssertions) {
				val containedFilename = outFileAssertion.middle.getFilename()

				if (containedFilename == aFileName) {
					return true
				}
			}

			return false
		}

		override fun iterator(): Iterator<Triple<AssOutFile, EOT_FileNameProvider, NG_OutputRequest>> {
			return outFileAssertions.stream().iterator()
		}
	}
}
