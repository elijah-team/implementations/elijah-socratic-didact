package tripleo.elijah.comp.notation

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CompilationImpl.CompilationAlways.Companion.defaultPrelude
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.inputtree.EIT_ModuleList
import tripleo.elijah.stages.gen_generic.*
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.stages.gen_generic.pipeline_impl.ProcessedNode
import tripleo.elijah.stages.logging.ElLog.Verbosity
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.world.i.WorldModule
import java.util.*
import java.util.function.Supplier

data class GN_GenerateNodesIntoSinkEnv(val lgc: List<ProcessedNode>,
                                       val resultSink1: GenerateResultSink,
                                       val verbosity: Verbosity,
                                       val gr: GenerateResult,
                                       val ce: CompilationEnclosure
) : GN_Env {
	private val moduleList: EIT_ModuleList = ce.compilation.objectTree.moduleList
	private val pa: IPipelineAccess = ce.pipelineAccess

	@Contract("_, _ -> new")
	fun getParams(mod: WorldModule?,
	              aGNGenerateNodesIntoSink: GN_GenerateNodesIntoSink): OutputFileFactoryParams {
		return OutputFileFactoryParams(mod!!, aGNGenerateNodesIntoSink._env().ce)
	}

//	fun lgc(): List<ProcessedNode> = lgc

//	fun resultSink1(): GenerateResultSink = resultSink1

	fun moduleList(): EIT_ModuleList = moduleList

//	fun verbosity(): Verbosity = verbosity

//	fun gr(): GenerateResult = gr

	fun pa(): IPipelineAccess = pa

//	fun ce(): CompilationEnclosure = ce

	companion object {
		fun getLang(mod: OS_Module): String? {
			val lsp = mod.lsp

			if (lsp == null) {
				SimplePrintLoggerToRemoveSoon.println_err_2("7777777777777777777 mod.getFilename " + mod.fileName)
				return null
			}

			val ci = lsp.instructions
			val lang2 = ci.genLang()

			val lang = lang2 ?: "c"
			return lang
		}

		fun getGenerateFiles(params: OutputFileFactoryParams, wm: WorldModule,
		                     fgs: Supplier<GenerateResultEnv?>): GenerateFiles {
			val fileGen: GenerateResultEnv?
			val mod = wm.module()

			// TODO creates more than one GenerateC, look into this
			// TODO ^^ validate this or not plz 09/07
			val lang = getLang(mod)
			if (lang == null) {
				// 09/26 tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("lang==null for " + mod.getFileName());
				// throw new NotImplementedException();
			}

			fileGen = if (lang == "c") {
				fgs.get() // FIXME "deep" implementation detail
			} else {
				// fileGen = null;
				fgs.get()
			}

			val lang1: String = Optional.ofNullable(lang).orElse(defaultPrelude())
			return OutputFileFactory.create(lang1, params, fileGen!!)
		}
	}
}
