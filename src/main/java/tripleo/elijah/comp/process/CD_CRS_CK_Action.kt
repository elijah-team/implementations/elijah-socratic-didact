package tripleo.elijah.comp.process

import tripleo.elijah.comp.graph.i.CK_Action
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.graph.i.CK_StepsContext
import tripleo.elijah.comp.i.CR_Action
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation

internal class CD_CRS_CK_Action(private val CDCompilationRunnerStart1: CD_CompilationRunnerStart_1, private val action: CR_Action) : CK_Action {
    override fun execute(context1: CK_StepsContext, aMonitor: CK_Monitor): Operation<Ok> {
        val context = context1 as CD_CRS_StepsContext

        val result = action.execute(context.state, context.output)

        CDCompilationRunnerStart1.crActionResultList?.add(result) // FIXME 10/20 associate result with action in list (steps)

        return result
    }
}
