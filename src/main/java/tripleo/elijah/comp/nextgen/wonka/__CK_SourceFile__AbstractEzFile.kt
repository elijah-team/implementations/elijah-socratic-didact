package tripleo.elijah.comp.nextgen.wonka

import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.graph.i.Asseverate
import tripleo.elijah.comp.graph.i.CK_SourceFile
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.comp.i.CompilationClosure
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.local.CX_ParseEzFile
import tripleo.elijah.comp.nextgen.i.Asseverable
import tripleo.elijah.comp.nextgen.i.Asseveration
import tripleo.elijah.comp.nextgen.i.AsseverationLogProgress
import tripleo.elijah.comp.specs.EzCache
import tripleo.elijah.comp.specs.EzSpec
import tripleo.elijah.diagnostic.ExceptionDiagnostic
import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.outputtree.EOT_OutputFile
import tripleo.elijah.util.CA_getHashForFile
import tripleo.elijah.util.Mode
import tripleo.elijah.util.Operation2
import tripleo.elijah.util2.UnintendedUseException
import tripleo.wrap.File
import java.util.regex.Pattern

abstract class __CK_SourceFile__AbstractEzFile : CK_SourceFile<Any?> {
    protected var compilation: Compilation? = null
    protected var input: CompilerInput? = null

    override fun associate(aCc: CompilationClosure) {
        compilation = aCc.compilation as Compilation
    }

    override fun associate(aInput: CompilerInput, aCc: CompilationClosure) {
        input = aInput
        compilation = aCc.compilation as Compilation
    }

    override fun compilerInput(): CompilerInput {
        return input!!
    }

    override fun input(): EIT_Input {
        throw UnintendedUseException()
    }

    override fun output(): EOT_OutputFile {
        throw UnintendedUseException()
    }

    protected fun asserverate() {
        if (fileName == null) return
        if (compilation == null) return

        val file_name = fileName
        val file = file
        val hash = CA_getHashForFile().apply(file_name, file)

        compilation!!.objectTree.asseverate(object : Asseveration {
            override fun target(): Any {
                return this@__CK_SourceFile__AbstractEzFile
            }

            override fun code(): Asseverate {
                return Asseverate.CI_HASHED
            }

            override fun onLogProgress(asseverable_ce: Asseverable) {
                // !!
                if (asseverable_ce is CompilationEnclosure) {
                    asseverable_ce.logProgress2(CompProgress.Ez__HasHash, AsseverationLogProgress { out, err -> out.printf("[-- Ez has HASH ] %s %s%n", file_name, hash.success()) })
                } else {
                    throw AssertionError()
                }
            }
        })
    }

    fun realParseEzFile__(ezSpec: EzSpec, ezCache: EzCache): Operation2<CompilerInstructions> {
        return realParseEzFile(ezSpec, ezCache)
    }

    protected abstract val file: File
        get

    abstract val fileName: String?

    companion object {
        /**
         * - I don't remember what absolutePath is for - Cache doesn't add to QueryDB
         *
         *
         * STEPS ------
         *
         *
         * 1. Get absolutePath<br></br>
         * 2. Check cache, return early<br></br>
         * 3. Parse (Query is incorrect I think)<br></br>
         * 4. Cache new result<br></br>
         *
         *
         * @param spec
         * @param cache
         * @return
         */
        fun realParseEzFile(spec: EzSpec, cache: EzCache): Operation2<CompilerInstructions> {
            val op = spec.absolute1()

            if (op.mode() == Mode.FAILURE) {
                return Operation2.failure(ExceptionDiagnostic(op.failure()))
            }

            val absolutePath = op.success()

            val early = cache[absolutePath]

            if (early.isPresent) {
                return Operation2.success(early.get())
            }

            val cio = CX_ParseEzFile.parseAndCache(spec, cache, absolutePath)
            return cio
        }

        fun isEzFile(aFileName: String?): Boolean {
            return Pattern.matches(".+\\.ez$", aFileName)
        }
    }
}
