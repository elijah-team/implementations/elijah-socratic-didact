/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 */
package tripleo.elijah.ci_impl

import tripleo.elijah.ci.CiIndexingStatement
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.ci.GenerateStatement
import tripleo.elijah.ci.LibraryStatementPart
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.compiler_model.CM_Filename
import tripleo.elijah.g.GDirective
import tripleo.elijah.lang.i.StringExpression
import tripleo.elijah.util.Helpers
import tripleo.elijah.xlang.LocatableString
import tripleo.wrap.File
import java.util.*

/**
 * Created 9/6/20 11:20 AM
 */
class CompilerInstructionsImpl : CompilerInstructions {
    private var advisedCompilerInput: CompilerInput? = null

    override fun genLang(): String {
        val genLang = gen!!.dirStream()
                .filter { input: GDirective -> input.sameName("gen") }
                .findAny() // README if you need more than one, comment this out
                .stream().map { gin0: GDirective ->
                    val gin = gin0 as GenerateStatementImpl.Directive
                    val lang_raw = gin.expression
                    if (lang_raw is StringExpression) {
                        val s = Helpers.remove_single_quotes_from_string(lang_raw.text)
                        return@map Optional.of<String>(s)
                    } else {
                        return@map Optional.empty<String>()
                    }
                }
                .findFirst()
                .flatMap { x: Optional<String>? -> x }

        // TODO 12/30 why not just return the Optional??
        return genLang.orElse(null)
    }

    override fun advise(aCompilerInput: CompilerInput) {
        advisedCompilerInput = aCompilerInput
    }

    override fun makeFile(): File {
        return if (advisedCompilerInput != null) {
            File.wrap(java.io.File(advisedCompilerInput!!.makeFile().wrapped(), getFilename().string))
        } else {
            // TODO 12/30 Shouldn't we always have something advised?
            File(getFilename().string)
        }
    }

    @Throws(IllegalStateException::class)
    override fun profferCompilerInput(): CompilerInput {
        checkNotNull(this.advisedCompilerInput) { "CompilerInstructions >> called before join" }
        return advisedCompilerInput!!
    }

    var lsps: MutableList<LibraryStatementPart> = ArrayList()
    private var _idx: CiIndexingStatement? = null
    private var filename: CM_Filename? = null
    private var gen: GenerateStatement? = null
    private val name: String? = null

    override fun add(generateStatement: GenerateStatement) {
        assert(gen == null)
        gen = generateStatement
    }

    override fun add(libraryStatementPart: LibraryStatementPart) {
        libraryStatementPart.instructions = this
        lsps.add(libraryStatementPart)
    }

    override fun getLibraryStatementParts(): List<LibraryStatementPart> {
        return lsps
    }

    override fun getFilename(): CM_Filename {
        return filename!!
    }

    override fun getName(): String {
        return name!!
    }

    override fun getLocatableName(): LocatableString? {
        return null
    }

    override fun setFilename(filename: CM_Filename) {
        this.filename = filename
    }

    override fun indexingStatement(): CiIndexingStatement {
        if (_idx == null) _idx = CiIndexingStatementImpl(this)

        return _idx!!
    }

    override fun setName(name: LocatableString) {
    }

    override fun toString(): String {
        return printableString()
    }

    fun printableString(): String {
        return "CompilerInstructionsImpl{" +
                "name='" + name + '\'' +
                ", filename='" + filename!!.string + '\'' +
                '}'
    }
}
