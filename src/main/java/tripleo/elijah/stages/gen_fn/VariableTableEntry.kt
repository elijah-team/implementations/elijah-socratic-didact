/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.lang.i.*
import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.DeduceLocalVariable
import tripleo.elijah.stages.deduce.DeduceTypeResolve
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceClient1
import tripleo.elijah.stages.deduce.DeduceTypes2.ExpectationBase
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_VariableTableEntry
import tripleo.elijah.stages.deduce.post_bytecode.PostBC_Processor
import tripleo.elijah.stages.gen_fn.BaseTableEntry.StatusListener
//import tripleo.elijah.stages.gen_fn.ForwardingGenType.unsparkled
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Created 9/10/20 4:51 PM
 */
class VariableTableEntry(// region constructable
        val index: Int, @JvmField val vtt: VariableTableType, @JvmField val name: String,
        var typeTableEntry: TypeTableEntry, el: OS_Element) : BaseTableEntry1(), Constructable, TableEntryIV, ExpectationBase {
    private val typeDeferred = DeferredObject2<GenType, Void, Void>()

    private val _p_constructableDeferred = DeferredObject<ProcTableEntry, Void, Void>()
    var _vs: VariableStatement? = null
    var tempNum: Int = -1
    private var _resolveTypeCalled: GenType? = null

    // endregion constructable
    var potentialTypes: MutableMap<Int, TypeTableEntry> = HashMap()
    var dlv: DeduceLocalVariable = DeduceLocalVariable(this)
    internal val genType: GenType = GenTypeImpl()
    var constructable_pte: ProcTableEntry? = null
    private var _de3: DeduceElement3_VariableTableEntry? = null
    private var _resolvedType: EvaNode? = null

    init {
        if (el is VariableStatement) {
            assert(vtt == VariableTableType.VAR)
            this._vs = el
            this.resolvedElement = el.nameToken
        } else {
            when (vtt) {
                VariableTableType.ARG -> {
                    val y = 2
                }

                VariableTableType.SELF, VariableTableType.RESULT, VariableTableType.TEMP -> {
                }

                VariableTableType.VAR -> {/*passthru*/}

            }
            this.resolvedElement = el
            this._vs = null
        }

        setupResolve()

        typeTableEntry.genType = ForwardingGenType(typeTableEntry.genType, true)
    }

    fun _resolvedType__notNull(): Boolean {
        return _resolvedType != null
    }

    fun addPotentialType(instructionIndex: Int, tte: TypeTableEntry) {
        if (!typeDeferred.isPending) {
            SimplePrintLoggerToRemoveSoon
                    .println_err_2("62 addPotentialType while typeDeferred is already resolved $this") // throw new
            // AssertionError();
            return
        }
        //
        if (!potentialTypes.containsKey(instructionIndex)) potentialTypes[instructionIndex] = tte
        else {
            val v = potentialTypes[instructionIndex]
            if (v!!.attached == null) {
                v.attached = tte.attached
                typeTableEntry.genType.copy(tte.genType) // README don't lose information
            } else if (tte.lifetime == TypeTableEntry.Type.TRANSIENT && v.lifetime == TypeTableEntry.Type.SPECIFIED) {
                // v.attached = v.attached; // leave it as is
            } else if (tte.lifetime == v.lifetime && v.attached === tte.attached) {
                // leave as is
            } else if (v.attached == tte.attached) {
                // leave as is
            } else {
                assert(false)
                //
                // Make sure you check the differences between USER and USER_CLASS types
                // May not be any
                //
//				tripleo.elijah.util.Stupidity.println_err_2("v.attached: " + v.attached);
//				tripleo.elijah.util.Stupidity.println_err_2("tte.attached: " + tte.attached);
                SimplePrintLoggerToRemoveSoon.println_out_2("72 WARNING two types at the same location.")
                if ((tte.attached != null && tte.attached!!.type != OS_Type.Type.USER)
                        || v.attached!!.type != OS_Type.Type.USER_CLASS) {
                    // TODO prefer USER_CLASS as we are assuming it is a resolved version of the
                    // other one
                    if (tte.attached == null) tte.attached = v.attached
                    else if (tte.attached!!.type == OS_Type.Type.USER_CLASS) v.attached = tte.attached
                }
            }
        }
    }

    override fun constructablePromise(): Promise<ProcTableEntry, Void, Void> {
        return _p_constructableDeferred.promise()
    }

    override fun expectationString(): String {
        return "VariableTableEntry{index=$index, name='$name'}"
    }

    //	public DeferredObject<GenType, Void, Void> typeDeferred() {
    //		return typeDeferred;
    //	}
    fun get_resolveTypeCalled(): GenType? {
        return _resolveTypeCalled
    }

    val deduceElement3: DeduceElement3_VariableTableEntry
        get() {
            if (_de3 == null) {
                _de3 = DeduceElement3_VariableTableEntry(this)
                // _de3.generatedFunction = generatedFunction;
                // _de3.deduceTypes2 = deduceTypes2;
            }
            return _de3!!
        }

    fun getDeduceElement3(aDt2: DeduceTypes2?, aGf1: BaseEvaFunction?): DeduceElement3_VariableTableEntry {
        if (_de3 == null) {
            _de3 = DeduceElement3_VariableTableEntry(this)
            _de3!!.setDeduceTypes2(aDt2, aGf1)
        }
        return _de3!!
    }

    fun getGenType(): GenType {
        return genType
    }

    fun getPostBC_Processor(aFd_ctx: Context?, aDeduceClient1: DeduceClient1?): PostBC_Processor {
        return PostBC_Processor.make_VTE(this, aFd_ctx, aDeduceClient1)
    }

    fun potentialTypes(): Collection<TypeTableEntry> {
        return potentialTypes.values
    }

    fun resolve_var_table_entry_for_exit_function() {
        dlv.resolve_var_table_entry_for_exit_function()
    }

    fun resolvedType(): EvaNode? {
        return _resolvedType
    }

    fun resolveType(aGenType: GenType) {
        try {
            if (_resolveTypeCalled != null) { // TODO what a hack
                if (_resolveTypeCalled!!.resolved != null) {
                    if (aGenType != _resolveTypeCalled) {
                        SimplePrintLoggerToRemoveSoon
                                .println_err_2(String.format("** 130 Attempting to replace %s with %s in %s",
                                        _resolveTypeCalled!!.asString(), aGenType.asString(), this))
                        // throw new AssertionError();
                    }
                } else {
                    _resolveTypeCalled = aGenType
                    typeDeferred.reset()
                    typeDeferred.resolve(aGenType)
                }
                return
            }
            if (typeDeferred.isResolved) {
                SimplePrintLoggerToRemoveSoon.println_err_2("126 typeDeferred is resolved $this")
            }
            _resolveTypeCalled = aGenType
            typeDeferred.resolve(aGenType)

            val x = typeResolve!!

            //			var vte_ident = __gf.getIdent(this);
        } finally {
            getGenType().copy(aGenType)
            // FIXME do we want to setStatus to KNOWN even without knowing here what the
            // element may be??
        }
    }

    override fun resolveTypeToClass(aNode: EvaNode) {
        _resolvedType = aNode
        getGenType().node = aNode
        typeTableEntry.resolve(aNode) // TODO maybe this obviates above

        if (getGenType() is ForwardingGenType) {
            val fgt = getGenType() as ForwardingGenType
            fgt.unsparkled()
        }
    }

    fun set_resolveTypeCalled(_resolveTypeCalled: GenType?) {
        this._resolveTypeCalled = _resolveTypeCalled
    }

    override fun setConstructable(aPte: ProcTableEntry) {
        if (constructable_pte != aPte) {
            constructable_pte = aPte
            _p_constructableDeferred.resolve(constructable_pte)
        }
    }

    fun setDeduceTypes2(aDeduceTypes2: DeduceTypes2?, aContext: Context?,
                        aGeneratedFunction: BaseEvaFunction?) {
        dlv.setDeduceTypes2(aDeduceTypes2, aContext, aGeneratedFunction)
    }

    override fun setGenType(aGenType: GenType) {
        genType.copy(aGenType)
        resolveType(aGenType)
    }

    fun setLikelyType(aGenType: GenType?) {
        val bGenType = typeTableEntry.genType

        // 1. copy arg into member
        bGenType.copy(aGenType)

        (bGenType as ForwardingGenType).unsparkled()

        // 2. set node when available
        (bGenType.ci as ClassInvocation).resolvePromise().done { aGeneratedClass: EvaClass ->
            bGenType.setNode(aGeneratedClass)
            resolveTypeToClass(aGeneratedClass)
            setGenType(bGenType) // TODO who knows if this is necessary?
        }

        bGenType.unsparkled()
    }

    fun setType(tte1: TypeTableEntry) {
        typeTableEntry = tte1
    }

    override fun setupResolve() {
        super.setupResolve()

        addStatusListener(StatusListener { eh, newStatus ->
            if (newStatus != Status.KNOWN) return@StatusListener
            __gf!!.getIdent(this@VariableTableEntry).resolve()
        })
    }

    override fun toString(): String {
        return ("VariableTableEntry{" + "index=" + index + ", name='" + name + '\'' + ", status=" + status + ", type="
                + typeTableEntry.index + ", vtt=" + vtt + ", potentialTypes=" + potentialTypes + '}')
    }

    fun typeDeferred_isPending(): Boolean {
        return typeDeferred.isPending
    }

    fun typeDeferred_isResolved(): Boolean {
        return typeDeferred.isResolved
    }

    fun typePromise(): Promise<GenType, Void, Void> {
        return typeDeferred.promise()
    }

    fun typeResolve(): DeduceTypeResolve {
        return typeResolve!!
    }
} //
//
//

