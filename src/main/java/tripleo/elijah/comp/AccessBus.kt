package tripleo.elijah.comp

import org.jdeferred2.DoneCallback
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CompilationImpl
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.Old_GenerateResult
import tripleo.elijah.util2.Eventual
import tripleo.vendor.mal.stepA_mal.MalEnv2

class AccessBus(val compilation: Compilation, val pipelineAccess: IPipelineAccess) {
    fun interface AB_GenerateResultListener {
        fun gr_slot(gr: GenerateResult)
    }

    fun interface AB_LgcListener {
        fun lgc_slot(lgc: List<EvaNode>)
    }

    fun interface AB_PipelineLogicListener {
        fun pl_slot(lgc: PipelineLogic?)
    }

    val gr: Old_GenerateResult = Old_GenerateResult()
    private val env = MalEnv2(null) // TODO what does null mean?

    private val generateResultPromise = Eventual<GenerateResult>()
    private val lgcPromise = Eventual<List<EvaNode>>()

    init {
        generateResultPromise.register((compilation as CompilationImpl))
        lgcPromise.register(compilation)
    }

    fun env(): MalEnv2 {
        return env
    }

    fun resolveGenerateResult(aGenerateResult: GenerateResult) {
        generateResultPromise.resolve(aGenerateResult)
    }

    fun resolvePipelineLogic(aPipelineLogic: PipelineLogic?) {
        pipelineAccess.pipelineLogicPromise.resolve(aPipelineLogic)
    }

    fun subscribe_GenerateResult(aGenerateResultListener: AB_GenerateResultListener) {
        generateResultPromise.then { gr: GenerateResult -> aGenerateResultListener.gr_slot(gr) }
    }

    fun subscribe_lgc(aLgcListener: AB_LgcListener) {
        lgcPromise.then { lgc: List<EvaNode> -> aLgcListener.lgc_slot(lgc) }
    }

    fun subscribePipelineLogic(aPipelineLogicDoneCallback: DoneCallback<PipelineLogic?>?) {
        pipelineAccess.pipelineLogicPromise.then(aPipelineLogicDoneCallback)
    }
}
