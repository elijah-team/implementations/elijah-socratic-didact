package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.lang.i.Context
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.lang.types.OS_BuiltinType
import tripleo.elijah.lang2.BuiltInTypes
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.FoundElement
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3.DeduceElement3_Kind
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaConstructor
import tripleo.elijah.stages.gen_fn.GenType
import tripleo.elijah.stages.gen_fn.GenTypeImpl
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.util.NotImplementedException

class DeduceElement3_Function(private val deduceTypes2: DeduceTypes2,
                              private val generatedFunction: BaseEvaFunction) : IDeduceElement3 {
    private val _gt: GenType

    init {
        _gt = _inj().new_GenTypeImpl()
    }

    private fun _inj(): DeduceTypes2Injector {
        return deduceTypes2()._inj()
    }

    override fun deduceTypes2(): DeduceTypes2 {
        return deduceTypes2
    }

    override fun elementDiscriminator(): DED {
        throw NotImplementedException()
        // return null;
    }

    override fun generatedFunction(): BaseEvaFunction {
        return generatedFunction
    }

    override fun genType(): GenType {
        return _gt
    }

    override fun getPrincipal(): OS_Element {
        throw NotImplementedException()
        // return null;
    }

    override fun kind(): DeduceElement3_Kind {
        return DeduceElement3_Kind.FUNCTION
    }

    override fun resolve(aContext: Context, dt2: DeduceTypes2) {
        throw NotImplementedException()
    }

    override fun resolve(aIdentIA: IdentIA, aContext: Context, aFoundElement: FoundElement) {
        throw NotImplementedException()
    }

    fun resolve_function_return_type_int(errSink: ErrSink): GenType? {
        // MODERNIZATION Does this have any affinity with DeferredMember?
        val vte_index = generatedFunction.vte_lookup("Result")
        if (vte_index != null) {
            val vte = generatedFunction.getVarTableEntry(DeduceTypes2.to_int(vte_index))

            if (vte.typeTableEntry.attached != null) {
                vte.resolveType(vte.typeTableEntry.genType) // TODO doesn't fit pattern of returning and then setting
                return vte.typeTableEntry.genType
            } else {
                val pot1 = vte.potentialTypes()
                val pot = ArrayList(pot1)

                when (pot.size) {
                    1 -> return pot[0].genType
                    0 -> return unitType
                    else ->                    // TODO report some kind of error/diagnostic and/or let ForFunction know...
                        errSink.reportWarning("Can't resolve type of `Result'. potentialTypes > 1 for $vte")
                }
            }
        } else {
            if (generatedFunction is EvaConstructor) {
                // cant set return type of constructors
            } else {
                // if Result is not present, then make function return Unit
                // TODO May not be correct in all cases, such as when Value is present
                // but works for current code structure, where Result is a always present
                return unitType
            }
        }
        return null
    }

    companion object {
        // TODO what about resolved?
        val unitType: GenType = GenTypeImpl()

        init {
            unitType.typeName = OS_BuiltinType(BuiltInTypes.Unit)
        }
    }
}
