/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.*
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.nextgen.DeduceCreationContext
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.util.Holder
import tripleo.elijah.work.WorkJob
import tripleo.elijah.work.WorkManager

/**
 * Created 7/3/21 6:24 AM
 */
class WlGenerateCtor @Contract(pure = true) constructor(private val generateFunctions: GenerateFunctions,
                                                        private val functionInvocation: FunctionInvocation,
                                                        private val constructorName: IdentExpression?,
                                                        private val codeRegistrar: ICodeRegistrar?) : WorkJob {
    private var _isDone = false
    var result: EvaConstructor? = null
        private set

    constructor(aModule: OS_Module?,
                aNameNode: IdentExpression?,
                aFunctionInvocation: FunctionInvocation,
                aCl: DeduceCreationContext) : this(aCl.generatePhase.getGenerateFunctions(aModule!!),
            aFunctionInvocation,
            aNameNode,
            aCl.generatePhase.codeRegistrar)

    private fun getPragma(aAuto_construct: String): Boolean {
        return false
    }

    override fun isDone(): Boolean {
        return _isDone
    }

    override fun run(aWorkManager: WorkManager?) {
        if (functionInvocation.generateDeferred().isPending) {
            val klass = functionInvocation.classInvocation!!.klass
            val hGenClass = Holder<EvaClass>()
            functionInvocation.classInvocation!!.resolvePromise().then { result -> hGenClass.set(result) }
            val genClass = hGenClass.get()!!
            var ccc: ConstructorDef? = null
            if (constructorName != null) {
                val cs = klass.constructors
                for (c in cs) {
                    if (c.name() == constructorName.text) {
                        ccc = c
                        break
                    }
                }
            }

            val cd: ConstructorDef
            if (ccc == null) {
                cd = ConstructorDefImpl(constructorName, klass as _CommonNC, klass.context)
                val scope3 = Scope3Impl(cd)
                cd.scope(scope3)
                for (varTableEntry in genClass.varTable) {
                    if (varTableEntry.initialValue !== LangGlobals.UNASSIGNED) {
                        val left: IExpression = varTableEntry.nameToken
                        val right = varTableEntry.initialValue

                        val e: IExpression = ExpressionBuilder.build(left, ExpressionKind.ASSIGNMENT, right)
                        scope3.add(StatementWrapperImpl(e, cd.getContext(), cd))
                    } else {
                        if (true) {
                            scope3.add(ConstructStatementImpl(cd, cd.getContext(), varTableEntry.nameToken, null,
                                    null))
                        }
                    }
                }
            } else cd = ccc

            val classStatement_ = cd.parent
            assert(classStatement_ is ClassStatement)
            val classStatement = classStatement_ as ClassStatement?
            val cs = classStatement!!.constructors
            var c: ConstructorDef? = null
            if (constructorName != null) {
                for (cc in cs) {
                    if (cc.name() == constructorName.text) {
                        c = cc
                        break
                    }
                }
            } else {
                // TODO match based on arguments
                val pte = functionInvocation.pte
                val args = pte!!.args
                // isResolved -> GeneratedNode, etc or getAttached -> OS_Element
                for (cc in cs) {
                    val cc_args = cc.args
                    if (args != null) {
                        if (cc_args.size == args.size) {
                            if (args.isEmpty()) {
                                c = cc
                                break
                            }
                            val y = 2
                        }
}
                }
            }

            run {
                // TODO what about multiple inheritance?
                // add inherit statement, if any

                // add code from c
                if (c != null && c !== cd) {
                    val `is` = ArrayList(c.items)

                    // skip initializers (already present in cd)
//				FunctionItem firstElement = is.get(0);
//				if (firstElement instanceof InheritStatement) {
//					cd.insertInherit(firstElement);
//					is.remove(0);
//				}
                    for (item in `is`) {
                        cd.add(item)
                    }
                }
            }

            val gf = generateFunctions.generateConstructor(cd, classStatement_!!, functionInvocation)

            //			lgf.add(gf);
            val ci = functionInvocation.classInvocation
            ci!!.resolvePromise().done { result: EvaClass ->
                codeRegistrar!!.registerFunction1(gf)
                gf.setClass(result)
                result.constructors[cd] = gf
            }

            functionInvocation.generateDeferred().resolve(gf)
            functionInvocation.generated = gf

            result = gf
        }

        _isDone = true
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

