package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.lang.i.TypeName
import tripleo.elijah.lang.types.OS_GenericTypeNameType
import tripleo.elijah.nextgen.outputstatement.EG_DottedStatement
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Rule
import tripleo.elijah.stages.gen_c.GenerateC.GetTypeName
import tripleo.elijah.stages.gen_c.c_ast1.C_HeaderString
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.util.Helpers.String_join
import java.util.stream.Collectors

class Generate_Method_Header(yf: WhyNotGarish_BaseFunction,
                             aGenerateC: GenerateC,
                             LOG: ElLog) {
	val args_string: String
	val header_string: String
	private val args_statement: EG_Statement
	private val gc: GenerateC
	private val name: String
	private val return_type: String
	var type: OS_Type? = null

	var tte: TypeTableEntry? = null

	init {
		val gf = yf.cheat()

		gc = aGenerateC
		name = gf!!.fd.name()
		//
		return_type = find_return_type(gf, LOG)
		args_statement = find_args_statement(gf)
		args_string = args_statement.text
		header_string = find_header_string(gf, LOG)
	}

	fun __find_header_string(gf: BaseEvaFunction, LOG: ElLog): String {
		val result: String

		// TODO buffer for gf.parent.<element>.locatable

		// NOTE getGenClass is always a class or namespace, getParent can be a function
		val parent = gf.genClass as EvaContainerNC

		if (parent is EvaClass) {
			val st = parent

			val chs = C_HeaderString.forClass(st, { GetTypeName.Companion.forGenClass(st) },
					return_type, name, args_string, LOG)

			result = chs.result
		} else if (parent is EvaNamespace) {
			val st = parent

			val chs = C_HeaderString.forNamespace(st, { GetTypeName.Companion.forGenNamespace(st) },
					return_type, name, args_string, LOG)
			result = chs.result
		} else {
			val chs = C_HeaderString.forOther(parent, return_type, name, args_string)
			// result = String.format("%s %s(%s)", return_type, name, args_string);
			result = chs.result
		}

		return result
	}

	fun __find_return_type(gf: WhyNotGarish_BaseFunction, LOG: ElLog): String {
		var returnType: String? = null
		if (gf.pointsToConstructor2()) {
			// Get it from resolved
			tte = gf.tte_for_self()
			val res = tte!!.resolved()
			if (res is EvaContainerNC) {
				val code: Int = res.code
				return String.format("Z%d*", code)
			}

			// Get it from type.attached
			type = tte!!.attached

			LOG.info("228-1 $type")
			if (type!!.isUnitType) {
				assert(false)
			} else if (type != null) {
				returnType = String.format("/*267*/%s*", gc.getTypeName(type!!))
			} else {
				LOG.err("655 Shouldn't be here (type is null)")
				returnType = "void/*2*/"
			}
		} else {
			val p = gf.tte_for_result()

			if (p.left != null) return p.left!!

			tte = p.right

			val res = tte!!.resolved()
			if (res is EvaContainerNC) {
				val code: Int = res.code

				// HACK
				if (res is EvaClass) {
					val classStatement: ClassStatement = res.klass

					val module = classStatement.context.module()
					val classStatementName = classStatement.name

					if (module.isPrelude && classStatementName == "Integer64") {
						return "/*190*/int"
					}
				}

				return String.format("Z%d*", code)
			}

			// Get it from type.attached
			type = tte!!.attached

			LOG.info("228 $type")
			if (type == null) {
				LOG.err("655 Shouldn't be here (type is null)")
				returnType = "ERR_type_attached_is_null/*2*/"
			} else if (type!!.isUnitType) {
				returnType = "void/*Unit-197*/"
			} else if (type != null) {
				if (type is OS_GenericTypeNameType) {
					val tn: TypeName = type!!.getRealTypeName()

					val gp = gf.classInvcationGenericPart()

					var realType: OS_Type? = null

					for ((key, value) in gp!!) {
						if (key == tn) {
							realType = value
							break
						}
					}

					assert(realType != null)
					returnType = String.format("/*267*/%s*", gc.getTypeName(realType!!))
				} else returnType = String.format("/*267*/%s*", gc.getTypeName(type!!))
			} else {
				throw IllegalStateException()
				//					LOG.err("656 Shouldn't be here (can't reason about type)");
//					returnType = "void/*656*/";
			}
		}
		return returnType!!
	}

	fun find_args_statement(gf: BaseEvaFunction): EG_Statement {
		val rule = "gen_c:gcfm:Generate_Method_Header:find_args_statement"

		// TODO EG_Statement, rule
		val args_list = gf.vte_list.stream()
				.filter { it.vtt == VariableTableType.ARG } // rule=vte:args_at
				.map { input: VariableTableEntry -> String.format("%s va%s", GetTypeName.Companion.forVTE(input), input.name) }
				.collect(Collectors.toList<String?>())
		val args: EG_Statement = EG_DottedStatement(", ", args_list, EX_Rule(rule))

		return args
	}

	fun find_args_string(gf: BaseEvaFunction): String {
		val args2 = gf.vte_list.stream()
				.filter { it.vtt == VariableTableType.ARG }
				.map {
					val tn = GetTypeName.forVTE(it)
					val n = it.name

					"$tn va$n"
				}

		val args = String_join(", ", args2.toList())
		return args
	}

	fun find_header_string(gf: BaseEvaFunction, LOG: ElLog): String {
		// NOTE getGenClass is always a class or namespace, getParent can be a function
		val parent = gf.genClass as EvaContainerNC

		val s2: String
		val headerString: C_HeaderString

		if (parent is EvaClass) {
			val st2: WhyNotGarish_Class = gc.a_lookup(parent)!!

			headerString = C_HeaderString.forClass(parent, { st2.typeNameString }, return_type, name, args_string,
					LOG)
		} else if (parent is EvaNamespace) {
			val st2: WhyNotGarish_Namespace = gc.a_lookup(parent)!!

			headerString = C_HeaderString.forNamespace(parent, { st2.typeNameString }, return_type, name,
					args_string, LOG)
		} else {
			headerString = C_HeaderString.forOther(parent, return_type, name, args_string)
		}
		s2 = headerString.result
		return s2
	}

	fun find_return_type(gf: BaseEvaFunction?, LOG: ElLog): String {
		return discriminator(gf, LOG, gc).find_return_type(this)
	}
}

private fun OS_Type.getRealTypeName(): TypeName {
	TODO("Not yet implemented")
}

fun discriminator(bgf: BaseEvaFunction?, aLOG: ElLog, aGc: GenerateC): GCM_D {
	when (bgf) {
		is EvaConstructor -> return GCM_GC(bgf as IEvaConstructor, aLOG, aGc)
		is EvaFunction -> return GCM_GF(bgf, aLOG, aGc)
		else -> throw IllegalStateException()
	}
}