package tripleo.elijah.comp.process

import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.graph.i.CK_Action
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.graph.i.CK_StepsContext
import tripleo.elijah.comp.internal.USE_Reasonings.Companion.initial
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation

class CK_ProcessInitialAction(private val rootCI: CompilerInstructions) : CK_Action {
    override fun execute(context1: CK_StepsContext, aMonitor: CK_Monitor): Operation<Ok> {
        val context = context1 as CD_CRS_StepsContext
        val crState = context.state
        val output = context.output

        val compilationRunner = crState!!.runner()

        try {
            compilationRunner!!._accessCompilation().use(rootCI, initial(this, compilationRunner, output!!))
            return Operation.success(Ok.instance())
        } catch (aE: Exception) {
            return Operation.failure(aE)
        }
    }

    //@Override
    //public @NotNull String name() {
    //	return "process initial";
    //}
    fun maybeFoundResult(): CompilerInstructions {
        return rootCI
    }
}
