/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce.tastic

import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.AliasStatementImpl
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.lang2.BuiltInTypes
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceClient4
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.declarations.DeferredMemberFunction
import tripleo.elijah.stages.deduce.tastic.FT_FnCallArgs.DoAssignCall
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.*
import java.util.stream.Collectors

/*static*/
class FT_FCA_IdentIA(private val FTFnCallArgs: FT_FnCallArgs, private val identIA: IdentIA?, private val vte: VariableTableEntry) {
    private inner class __FT_FCA_IdentIA_Runnable(private val dt2: DeduceTypes2,
                                                  private val generatedFunction: BaseEvaFunction,
                                                  private val vte: VariableTableEntry,
                                                  private val vte1: VariableTableEntry,
                                                  private val errSink: ErrSink,
                                                  private val e_text: String,
                                                  private val p: Promise<GenType?, Void?, Void?>,
                                                  private val ctx: Context,
                                                  private val dc: DeduceClient4,
                                                  private val vte_ia: InstructionArgument) : Runnable {
        var isDone: Boolean = false

        private fun __doLogic0__FormalArgListItem(fali: FormalArgListItem) {
            // TODO 09/07 DEW??
            FT_FCA_FormalArgListItem(fali, generatedFunction)._FunctionCall_Args_doLogic0(vte, vte1, errSink)
        }

        private fun __doLogic0__VariableStatement(vs: VariableStatementImpl) {
            // TODO 09/07 DEW??
            FT_FCA_VariableStatement(vs, generatedFunction)._FunctionCall_Args_doLogic0(vte, vte1, e_text, p)
        }

        fun doLogic(potentialTypes: List<TypeTableEntry>) {
            when (potentialTypes.size) {
                1 -> doLogic1(potentialTypes)
                0 -> doLogic0()
                else -> doLogic_default(potentialTypes)
            }
        }

        private fun doLogic_default(potentialTypes: List<TypeTableEntry>) {
            // TODO hopefully this works
            val potentialTypes1 = potentialTypes.stream()
                    .filter { input: TypeTableEntry -> input.attached != null }.collect(Collectors.toList())

            // prevent infinite recursion
            if (potentialTypes1.size < potentialTypes.size) doLogic(potentialTypes1)
            else FTFnCallArgs.LOG().info("913 Don't know")
        }

        private fun doLogic0() {
            // README moved up here to elimiate work
            if (p.isResolved) {
                System.out.printf("890-1 Already resolved type: vte1.type = %s, gf = %s %n", vte1.typeTableEntry,
                        generatedFunction)
                return
            }
            val lrl = ctx.lookup(e_text)
            val best = lrl.chooseBest(null)
            if (best is FormalArgListItem) {
                __doLogic0__FormalArgListItem(best)
            } else if (best is VariableStatementImpl) {
                __doLogic0__VariableStatement(best)
            } else {
                val y = 2
                FTFnCallArgs.LOG().err("543 " + best!!.javaClass.name)
                throw NotImplementedException()
            }
        }

        private fun doLogic1(potentialTypes: List<TypeTableEntry>) {
//						tte.attached = ll.get(0).attached;
//						vte.addPotentialType(instructionIndex, ll.get(0));
            if (p.isResolved) {
                FTFnCallArgs.LOG()
                        .info(String.format("1047 (vte already resolved) %s vte1.type = %s, gf = %s, tte1 = %s %n",
                                vte1.name, vte1.typeTableEntry, generatedFunction, potentialTypes[0]))
                return
            }

            val attached = potentialTypes[0].attached ?: return
            when (attached.type) {
                OS_Type.Type.USER -> vte1.typeTableEntry.attached = attached // !!
                OS_Type.Type.USER_CLASS -> {
                    val gt = vte1.genType
                    gt.resolved = attached
                    vte1.resolveType(gt)
                }

                else -> errSink.reportWarning("Unexpected value: " + attached.type)
            }
        }

        override fun run() {
            if (isDone) return
            val ll = dc.getPotentialTypesVte((generatedFunction as EvaFunction), vte_ia)
            doLogic(ll)
            isDone = true
        }
    }

    inner class FakeDC4(private val dc4: DeduceClient4) {
        fun _deduceTypes2(): DeduceTypes2Injector {
            return dc4.get()._inj()
        }

        fun _resolveAlias(aAliasStatement: AliasStatement?): OS_Element? {
            return dc4._resolveAlias(aAliasStatement!!)
        }

        fun deferred_member_function(aParent: OS_Element?, aInvocation2: IInvocation?,
                                     aResolvedElement: FunctionDef?, aFi: FunctionInvocation?): DeferredMemberFunction {
            return dc4.deferred_member_function(aParent, aInvocation2, aResolvedElement!!, aFi!!)
        }

        fun found_element_for_ite(aGeneratedFunction: BaseEvaFunction?, aIte: IdentTableEntry?,
                                  aE: OS_Element?, aCtx: Context?) {
            dc4.found_element_for_ite(aGeneratedFunction, aIte!!, aE, aCtx)
        }

        val phase: DeducePhase
            get() = dc4.phase

        fun newFunctionInvocation(aResolvedElement: FunctionDef?, aPte: ProcTableEntry?,
                                  aInvocation2: IInvocation): FunctionInvocation {
            return dc4.newFunctionInvocation(aResolvedElement, aPte, aInvocation2)
        }

        fun promiseExpectation(aBgf: BaseEvaFunction?,
                               aFunctionResultType: String?): PromiseExpectation<GenType> {
            return dc4.promiseExpectation(aBgf, aFunctionResultType)
        }

        fun registerClassInvocation(invocation2: ClassInvocation?): ClassInvocation {
            return dc4.phase.registerClassInvocation(invocation2!!)
        }

        fun resolveIdentIA_(aCtx: Context?, aIdentIA: IdentIA?,
                            aGeneratedFunction: BaseEvaFunction?, aFunctionResultType: FoundElement?) {
            dc4.resolveIdentIA_(aCtx!!, aIdentIA!!, aGeneratedFunction!!, aFunctionResultType!!)
        }
    }

    class Resolve_VTE(private val vte: VariableTableEntry, private val ctx: Context, private val pte: ProcTableEntry, private val instruction: Instruction,
                      private val fca: FnCallArgs) {
        fun vte(): VariableTableEntry {
            return vte
        }

        fun ctx(): Context {
            return ctx
        }

        fun pte(): ProcTableEntry {
            return pte
        }

        fun instruction(): Instruction {
            return instruction
        }

        fun fca(): FnCallArgs {
            return fca
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as Resolve_VTE
            return this.vte == that.vte && (this.ctx == that.ctx) && (this.pte == that.pte) && (this.instruction == that.instruction) && (this.fca == that.fca)
        }

        override fun hashCode(): Int {
            return Objects.hash(vte, ctx, pte, instruction, fca)
        }

        override fun toString(): String {
            return "Resolve_VTE[" +
                    "vte=" + vte + ", " +
                    "ctx=" + ctx + ", " +
                    "pte=" + pte + ", " +
                    "instruction=" + instruction + ", " +
                    "fca=" + fca + ']'
        }
    }

    class FT_FCA_Ctx(private val generatedFunction: BaseEvaFunction, private val tte: TypeTableEntry, private val ctx: Context, private val errSink: ErrSink,
                     private val dc: DeduceClient4) {
        fun generatedFunction(): BaseEvaFunction {
            return generatedFunction
        }

        fun tte(): TypeTableEntry {
            return tte
        }

        fun ctx(): Context {
            return ctx
        }

        fun errSink(): ErrSink {
            return errSink
        }

        fun dc(): DeduceClient4 {
            return dc
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as FT_FCA_Ctx
            return this.generatedFunction == that.generatedFunction && (this.tte == that.tte) && (this.ctx == that.ctx) && (this.errSink == that.errSink) && (this.dc == that.dc)
        }

        override fun hashCode(): Int {
            return Objects.hash(generatedFunction, tte, ctx, errSink, dc)
        }

        override fun toString(): String {
            return "FT_FCA_Ctx[" +
                    "generatedFunction=" + generatedFunction + ", " +
                    "tte=" + tte + ", " +
                    "ctx=" + ctx + ", " +
                    "errSink=" + errSink + ", " +
                    "dc=" + dc + ']'
        }
    }

    inner class FT_FCA_ProcedureCall(private val pce: ProcedureCallExpression, private val ctx: Context)

    private fun __loop1__DOT_EXP(de: DotExpression, fdctx: FT_FCA_Ctx) {
        val dc = fdctx.dc()
        val errSink = fdctx.errSink()
        val tte = fdctx.tte()
        val generatedFunction = fdctx.generatedFunction()
        val ctx = fdctx.ctx()

        try {
            val lrl = dc.lookupExpression(de.left, ctx)
            var best = lrl.chooseBest(null)
            if (best != null) {
                while (best is AliasStatementImpl) {
                    best = dc._resolveAlias2((best as AliasStatementImpl?)!!)
                }
                if (best is FunctionDef) {
                    tte.attached = best.oS_Type
                    // vte.addPotentialType(instructionIndex, tte);
                } else if (best is ClassStatement) {
                    tte.attached = best.oS_Type
                } else if (best is VariableStatementImpl) {
                    val vte_ia = generatedFunction.vte_lookup(best.name)
                    val tte1 = (Objects.requireNonNull(vte_ia) as IntegerIA).entry.typeTableEntry
                    tte.attached = tte1.attached
                } else {
                    val y = 2
                    FTFnCallArgs.LOG().err(best!!.javaClass.name)
                    throw NotImplementedException()
                }
            } else {
                val y = 2
                throw NotImplementedException()
            }
        } catch (aResolveError: ResolveError) {
            aResolveError.printStackTrace()
            val y = 2
            throw NotImplementedException()
        }
    }

    private fun __loop1__PROCEDURE_CALL(pte: ProcTableEntry, pce: ProcedureCallExpression,
                                        fdctx: FT_FCA_Ctx) {
        val dc = fdctx.dc()
        val errSink = fdctx.errSink()
        val tte = fdctx.tte()
        val generatedFunction = fdctx.generatedFunction()
        val ctx = fdctx.ctx()

        val dt2 = dc.get()

        val fcapce = dt2._inj().new_FT_FCA_ProcedureCall(pce, ctx, this)

        try {
            val lrl = dc.lookupExpression(pce.left, ctx)
            var best = lrl.chooseBest(null)
            if (best != null) {
                while (best is AliasStatementImpl) {
                    best = dc._resolveAlias2((best as AliasStatementImpl?)!!)
                }
                if (best is FunctionDef) {
                    val parent = best.getParent()
                    val invocation = if (parent is NamespaceStatement) {
                        dc.registerNamespaceInvocation(parent)
                    } else if (parent is ClassStatement) {
                        dc.registerClassInvocation(parent, null)
                    } else throw NotImplementedException() // TODO implement me


                    // tte transitiveOver: {best/codePoint >> FunctionInvocation} resolvedReturnType
                    dc.forFunction(dc.newFunctionInvocation(best as FunctionDef?, pte, invocation!!), object : ForFunction() {
                        override fun typeDecided(aType: GenType?) {
                            tte.setAttached(aType)
                            //									vte.addPotentialType(instructionIndex, tte);
                        }
                    })

                    //							tte.setAttached(_inj().new_OS_FuncType((FunctionDef) best));
                } else {
                    val y = 2
                    throw NotImplementedException()
                }
            } else {
                val y = 2
                throw NotImplementedException()
            }
        } catch (aResolveError: ResolveError) {
//					aResolveError.printStackTrace();
//					int y=2;
//					throw new NotImplementedException();
            dc.reportDiagnostic(aResolveError)
            tte.attached = dt2._inj().new_OS_UnknownType(dt2._inj().new_StatementWrapperImpl(pce.left, null, null))
        }
    }

    private fun __vte_ia__is_null(aInstructionIndex: Int, aPte: ProcTableEntry, aI: Int,
                                  aExpression: IdentExpression, generatedFunction: BaseEvaFunction,
                                  ctx: Context, aTte: TypeTableEntry, dc: DeduceClient4,
                                  dt2: DeduceTypes2) {
        val ia = generatedFunction.addIdentTableEntry(aExpression, ctx)
        val idte = generatedFunction.getIdentTableEntry(ia)
        idte.addPotentialType(aInstructionIndex, aTte) // TODO DotExpression??
        val ii = aI
        idte.onType(dc.phase, object : OnType {
            override fun noTypeFound() {
                FTFnCallArgs.LOG().err("719 no type found "
                        + generatedFunction.getIdentIAPathNormal(dt2._inj().new_IdentIA(ia, generatedFunction)))
            }

            override fun typeDeduced(aType: OS_Type) {
                aPte.setArgType(ii, aType) // TODO does this belong here or in FunctionInvocation?
                aTte.attached = aType // since we know that tte.attached is always null here
            }
        })
    }

    private fun __vte_ia__not_null(vte: VariableTableEntry,
                                   generatedFunction: BaseEvaFunction, vte_ia: InstructionArgument,
                                   aTte: TypeTableEntry, dt2: DeduceTypes2, errSink: ErrSink, e_text: String,
                                   ctx: Context, dc: DeduceClient4) {
        val vte1 = generatedFunction.getVarTableEntry(DeduceTypes2.to_int(vte_ia))
        val p = VTE_TypePromises.do_assign_call_args_ident_vte_promise(aTte, vte1)
        val runnable: Runnable = __FT_FCA_IdentIA_Runnable(dt2, generatedFunction, vte, vte1, errSink, e_text, p, ctx,
                dc, vte_ia)
        dc.onFinish(runnable)
    }

    fun do_assign_call_args_ident(vte: VariableTableEntry, aInstructionIndex: Int, aPte: ProcTableEntry,
                                  aI: Int, aExpression: IdentExpression, fdctx: FT_FCA_Ctx) {
        val dc = fdctx.dc()
        val errSink = fdctx.errSink()
        val aTte = fdctx.tte()
        val generatedFunction = fdctx.generatedFunction()
        val ctx = fdctx.ctx()

        val dt2 = dc.get()

        val e_text = aExpression.text
        val vte_ia = generatedFunction.vte_lookup(e_text)
        //		LOG.info("10000 "+vte_ia);
        if (vte_ia != null) {
            __vte_ia__not_null(vte, generatedFunction, vte_ia, aTte, dt2, errSink, e_text, ctx, dc)
        } else {
            __vte_ia__is_null(aInstructionIndex, aPte, aI, aExpression, generatedFunction, ctx, aTte, dc, dt2)
        }
    }

    fun do_assign_call_GET_ITEM(gie: GetItemExpression, fdctx: FT_FCA_Ctx) {
        val dc = fdctx.dc()
        val errSink = fdctx.errSink()
        val tte = fdctx.tte()
        val generatedFunction = fdctx.generatedFunction()
        val ctx = fdctx.ctx()

        val dt2 = dc.get()

        try {
            val lrl = dc.lookupExpression(gie.left, ctx)
            val best = lrl.chooseBest(null)
            if (best != null) {
                if (best is VariableStatementImpl) { // TODO what about alias?
                    val s: String = best.name
                    val vte_ia = generatedFunction.vte_lookup(s)
                    if (vte_ia != null) {
                        val vte1 = generatedFunction.getVarTableEntry(DeduceTypes2.to_int(vte_ia))
                        throw NotImplementedException()
                    } else {
                        val idte = generatedFunction.getIdentTableEntryFor(best.nameToken)!!
                        if (idte.type == null) {
                            val identIA = dt2._inj().new_IdentIA(idte.index, generatedFunction)
                            dc.resolveIdentIA_(ctx, identIA, generatedFunction,
                                    dt2._inj().new_FT_FnCallArgs_DoAssignCall_NullFoundElement(dc))
                        }
                        val ty = if (idte.type == null) null
                        else idte.type!!.attached
                        idte.onType(dc.phase, object : OnType {
                            override fun noTypeFound() {
                                throw NotImplementedException()
                            }

                            override fun typeDeduced(ty: OS_Type) {
                                assert(ty != null)
                                var rtype: GenType? = null
                                try {
                                    rtype = dc.resolve_type(ty, ctx)
                                } catch (resolveError: ResolveError) {
                                    // resolveError.printStackTrace();
                                    errSink.reportError("Cant resolve $ty") // TODO print better diagnostic
                                    return
                                }
                                if (rtype.resolved != null
                                        && rtype.resolved.type == OS_Type.Type.USER_CLASS) {
                                    val lrl2 = rtype.resolved.classOf.context
                                            .lookup("__getitem__")
                                    val best2 = lrl2.chooseBest(null)
                                    if (best2 != null) {
                                        if (best2 is FunctionDef) {
                                            val pte: ProcTableEntry? = null
                                            val invocation = dc
                                                    .getInvocation((generatedFunction as EvaFunction))
                                            dc.forFunction(dc.newFunctionInvocation(best2, pte, invocation),
                                                    object : ForFunction() {
                                                        override fun typeDecided(aType: GenType?) {
                                                            assert(best2 === generatedFunction.getFD())
                                                            //
                                                            if (idte.type == null) {
                                                                idte.makeType(generatedFunction,
                                                                        TypeTableEntry.Type.TRANSIENT, dc.gt(aType!!)) // TODO
                                                                // expression?
                                                            } else idte.type!!.attached = dc.gt(aType!!)
                                                        }
                                                    })
                                        } else {
                                            throw NotImplementedException()
                                        }
                                    } else {
                                        throw NotImplementedException()
                                    }
                                }
                            }
                        })
                        if (ty == null) {
                            val tte3: TypeTableEntry = generatedFunction.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED,
                                    dt2._inj().new_OS_UserType(best.typeName()), best.nameToken)
                            idte.type = tte3
                            //							ty = idte.type.getAttached();
                        }
                    }

                    // tte.attached = _inj().new_OS_FuncType((FunctionDef) best); // TODO: what is
                    // this??
                    // vte.addPotentialType(instructionIndex, tte);
                } else if (best is FormalArgListItem) {
                    val s: String = best.name()
                    val vte_ia = generatedFunction.vte_lookup(s)
                    if (vte_ia != null) {
                        val vte2 = generatedFunction.getVarTableEntry(DeduceTypes2.to_int(vte_ia))

                        //						final @Nullable OS_Type ty2 = vte2.type.attached;
                        VTE_TypePromises.getItemFali(generatedFunction, ctx, vte2, dc.get())
                        //					vte2.onType(phase, _inj().new_OnType() {
//						@Override public void typeDeduced(final OS_Type ty2) {
//						}
//
//						@Override
//						public void noTypeFound() {
//							throw new NotImplementedException();
//						}
//					});
                        /*
						 * if (ty2 == null) {
						 * 
						 * @NotNull TypeTableEntry tte3 = generatedFunction.newTypeTableEntry(
						 * TypeTableEntry.Type.SPECIFIED, _inj().new_OS_UserType(fali.typeName()),
						 * fali.getNameToken()); vte2.type = tte3; // ty2 = vte2.type.attached; // TODO
						 * this is final, but why assign anyway? }
						 */
                    }
                } else {
                    val y = 2
                    throw NotImplementedException()
                }
            } else {
                val y = 2
                throw NotImplementedException()
            }
        } catch (aResolveError: ResolveError) {
            aResolveError.printStackTrace()
            val y = 2
            throw NotImplementedException()
        }
    }

    fun loop1(dac: DoAssignCall, rvte: Resolve_VTE) {
        val generatedFunction = dac.generatedFunction()
        val dc = dac.dc()
        val errSink = dac.errSink
        val ctx = rvte.ctx()
        val pte = rvte.pte()
        val instructionIndex = rvte.instruction().index

        val dt2 = dac.dc().get()

        val args = pte.args

        for (i in args?.indices!!) {
            val tte = args?.get(i) // TODO this looks wrong

            //			LOG.info("770 "+tte);
            val fdctx = dt2._inj().new_FT_FCA_Ctx(generatedFunction, tte, ctx, errSink, dc)

            var e: IExpression? = tte?.__debug_expression ?: continue
            if (e is SubExpression) e = e.expression
            when (e?.kind) {
                ExpressionKind.NUMERIC -> tte.attached = dt2._inj().new_OS_BuiltinType(BuiltInTypes.SystemInteger)
                ExpressionKind.CHAR_LITERAL -> tte.attached = dt2._inj().new_OS_BuiltinType(BuiltInTypes.SystemCharacter)
                ExpressionKind.IDENT -> do_assign_call_args_ident(vte, instructionIndex, pte, i, e as IdentExpression, fdctx)
                ExpressionKind.PROCEDURE_CALL -> __loop1__PROCEDURE_CALL(pte, e as ProcedureCallExpression, fdctx)
                ExpressionKind.DOT_EXP -> {
                    val de = e as DotExpression
                    __loop1__DOT_EXP(de, fdctx)
                }

                ExpressionKind.ADDITION, ExpressionKind.MODULO, ExpressionKind.SUBTRACTION -> {
                    val y = 2
                    SimplePrintLoggerToRemoveSoon.println_err_2("2363")
                }

                ExpressionKind.GET_ITEM -> {
                    val gie = e as GetItemExpression
                    do_assign_call_GET_ITEM(gie, fdctx)
                }

                else -> throw IllegalStateException("Unexpected value: " + (e?.kind ?: "dsadmaksl;dmaskd;amskdla;sm"))
            }
        }
    }

    fun loop2(dac: DoAssignCall, rvte: Resolve_VTE) {
        val ctx = rvte.ctx()
        val pte = rvte.pte()
        val instruction = rvte.instruction()
        val instructionIndex = instruction.index
        val vte = rvte.vte()
        val fca = rvte.fca()

        if (pte.expression_num == null) {
            if (fca.expression.name != InstructionName.CALLS) {
                val text = (pte.__debug_expression as IdentExpression).text
                val lrl = ctx.lookup(text)

                val best = lrl.chooseBest(null)
                if (best != null) pte.resolvedElement = best // TODO do we need to add a dependency for class?
                else {
                    dac.errSink.reportError("Cant resolve $text")
                }
            } else {
                dac.dc().implement_calls(dac.generatedFunction(), ctx.parent!!, instruction.getArg(1)!!, pte,
                        instructionIndex)
            }
        } else {
            val idte = identIA!!.entry
            dac.dc().resolveIdentIA_(ctx, identIA, dac.generatedFunction(), object : FoundElement(dac.dc().phase) {
                val x: String = dac.generatedFunction().getIdentIAPathNormal(identIA)

                override fun foundElement(el: OS_Element?) {
                    if (pte.resolvedElement == null) pte.resolvedElement = el
                    val deduceTypes2 = dac.dc().get()
                    if (el is FunctionDef) {
                        val fcafd = deduceTypes2._inj().new_FT_FCA_FunctionDef(el,
                                deduceTypes2)
                        fcafd.loop2_i(dac, pte, vte, instructionIndex)
                    } else if (el is ClassStatement) {
                        val fcafd = deduceTypes2._inj()
                                .new_FT_FCA_ClassStatement(el)
                        loop2_i(el)
                    } else {
                        dac.log.err("7890 " + el?.javaClass?.name)
                    }
                }

                private fun loop2_i(kl: ClassStatement) {
                    val type = kl.oS_Type
                    val tte = dac.generatedFunction().newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, type,
                            pte.__debug_expression, pte)
                    vte.addPotentialType(instructionIndex, tte)
                    vte.setConstructable(pte)

                    dac.dc().register_and_resolve(vte, kl)
                }

                override fun noFoundElement() {
                    dac.log.err("IdentIA path cannot be resolved $x")
                }
            })
        }
    }

    @Throws(FCA_Stop::class)
    fun make2(dac: DoAssignCall, rvte: Resolve_VTE) {
        val generatedFunction = dac.generatedFunction()
        val dc4 = dac.dc()
        val errSink = dac.errSink
        val LOG = dac.log
        val module = dac.module
        val ctx = rvte.ctx()
        val instructionIndex = rvte.instruction().index
        val pte = rvte.pte()

        val dt2 = dc4.get()

        if (identIA != null) {
//				LOG.info("594 "+identIA.getEntry().getStatus());

            val dc = dt2._inj().new_FakeDC4(dc4, this)

            val resolved_element = identIA.entry.resolvedElement ?: return

            val ext = dt2._inj().new_DT_External_2(identIA.entry,
                    module,
                    pte,
                    dc,
                    LOG,
                    ctx,
                    generatedFunction,
                    instructionIndex,
                    identIA,
                    vte)

            FTFnCallArgs.deduceTypes2().addExternal(ext)
        }
    }

    fun resolve_vte(dac: DoAssignCall, rvte: Resolve_VTE) {
        val aGeneratedFunction = dac.generatedFunction()
        val dc = dac.dc()
        val ctx = rvte.ctx()
        val pte = rvte.pte()

        val dt2 = dc.get()

        val resolvedElement = vte.resolvedElement
        if (resolvedElement != null) {
            val p = DeferredObject<IdentExpression, Void, Void>()
            val p2 = DeferredObject<Void, ResolveError, Void>()
            p.then { identExpression: IdentExpression? ->
                val el: OS_Element?
                try {
                    el = dc.lookup(identExpression!!, ctx)
                } catch (aE: ResolveError) {
                    p2.reject(aE)
                    return@then
                }

                val ident = aGeneratedFunction.getIdent(identExpression, vte)
                ident.addUnderstanding(dt2._inj().new_DR_Ident_ElementUnderstanding(el))

                if (el is VariableStatement) {
                    if (el.typeModifiers == (TypeModifiers.CONST)) { // FIXME not a list
                        val idex: IdentExpression = el.nameToken
                        idex.name.addUnderstanding(dt2._inj().new_ENU_LangConstVar())
                    }
                }
                vte.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(el))
            }
            p2.fail { aResolveError: ResolveError? -> dc.reportDiagnostic(aResolveError) }

            if (resolvedElement is IdentExpression) {
                p.resolve(resolvedElement)
            } else {
                val nameToken = (resolvedElement as VariableStatementImpl).nameToken
                p.resolve(nameToken)
            }
        }

        if (vte.status == BaseTableEntry.Status.UNCHECKED) {
            pte.typePromise().then { aGenType: GenType? -> vte.resolveType(aGenType!!) }
        }
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

