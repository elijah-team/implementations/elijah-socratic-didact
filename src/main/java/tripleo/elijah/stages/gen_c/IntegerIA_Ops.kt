package tripleo.elijah.stages.gen_c

import org.jetbrains.annotations.Contract
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.instructions.IntegerIA

class IntegerIA_Ops(private val integerIA: IntegerIA, private val sSize: Int) {
    private inner class ConstructorPathOp1 : ConstructorPathOp {
        private var _calculated = false

        var _resolved: EvaNode? = null

        private fun calculate() {
            val vte = integerIA.entry

            if (sSize == 1) {
                val resolved = vte.typeTableEntry.resolved()
                _resolved = resolved ?: vte.resolvedType()
            }

            _calculated = true
        }

        override fun getCtorName(): String? {
            return null
        }

        override fun getResolved(): EvaNode? {
            if (!_calculated) {
                calculate()
            }

            return _resolved
        }
    }

    val constructorPath: ConstructorPathOp
        get() = ConstructorPathOp1()

    companion object {
        @Contract(value = "_, _ -> new", pure = true)
        fun get(aIntegerIA: IntegerIA, aSSize: Int): IntegerIA_Ops {
            return IntegerIA_Ops(aIntegerIA, aSSize)
        }
    }
}
