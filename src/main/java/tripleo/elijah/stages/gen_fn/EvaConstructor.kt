/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import tripleo.elijah.lang.i.*
import tripleo.elijah.nextgen.reactive.DefaultReactive
import tripleo.elijah.stages.deduce.DeduceElement3_Constructor
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.gen_fn.IEvaConstructor.BaseEvaConstructor_Reactive
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.util2.Eventual
import tripleo.elijah.util2.UnintendedUseException
import java.util.function.Consumer

/**
 * Created 6/27/21 9:45 AM
 */
class EvaConstructor(val cd: ConstructorDef?) : BaseEvaFunction(), IEvaConstructor { //
    private val _de3_Promise = Eventual<DeduceElement3_Constructor>()

    class __Reactive : DefaultReactive(), BaseEvaConstructor_Reactive {
        override fun <T> addListener(t: Consumer<T>?) {
            throw UnintendedUseException()
        }
    }


    override fun de3_Promise(): Eventual<DeduceElement3_Constructor> {
        return _de3_Promise
    }

    override fun getFD(): FunctionDef {
        checkNotNull(cd) { "No function" }
        return cd
    }

    override fun getSelf(): VariableTableEntry? {
        return if (fd.parent is ClassStatement) getVarTableEntry(0)
        else null
    }

    override fun identityString(): String {
        return cd.toString()
    }

    override fun module(): OS_Module {
        return cd!!.context.module()
    }

    override fun name(): String {
        requireNotNull(cd) { "null cd" }
        return cd.name()
    }

    override fun setFunctionInvocation(fi: FunctionInvocation) {
        val genType: GenType = GenTypeImpl()
        if (fi.classInvocation != null) {
            val classInvocation = fi.classInvocation
            genType.ci = classInvocation

            val classStatement = classInvocation!!.klass
            val osType = classStatement.oS_Type
            genType.resolved = osType
        } else {
            val namespaceInvocation = fi.namespaceInvocation
            genType.ci = namespaceInvocation

            val namespaceStatement = namespaceInvocation!!.namespace
            val osType = namespaceStatement.oS_Type
            genType.resolved = osType
        }
        genType.node = this
        typeDeferred().resolve(genType)
    }

    override fun toString(): String {
        return String.format("<GeneratedConstructor %s>", cd)
    }

    override fun addVariableTableEntry(name: String?, vtt: VariableTableType?, type: TypeTableEntry?, el: OS_Element?): Int {
        TODO("Not yet implemented")
    }

    override fun getCode(): Int = code
}
