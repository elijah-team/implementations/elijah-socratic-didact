package tripleo.elijah.stages.gen_c

import io.reactivex.rxjava3.subjects.Subject
import tripleo.elijah.lang.i.*
import tripleo.elijah.stages.deduce.DeduceElement
import tripleo.elijah.stages.deduce.DeduceElement3_Constructor
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.OnGenClass
import tripleo.elijah.stages.deduce.nextgen.DR_Ident
import tripleo.elijah.stages.deduce.nextgen.DR_ProcCall
import tripleo.elijah.stages.deduce.nextgen.DR_Type
import tripleo.elijah.stages.deduce.nextgen.DR_Variable
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_fn.IEvaConstructor.BaseEvaConstructor_Reactive
import tripleo.elijah.stages.gen_generic.Dependency
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.util2.Eventual
import tripleo.util.range.Range

class DefaultDeducedEvaConstructor(private val carrier: EvaConstructor) : DeducedEvaConstructor {
    fun addDependentFunction(aFunction: FunctionInvocation) {
        carrier.addDependentFunction(aFunction)
    }

    fun addDependentType(aType: GenType) {
        carrier.addDependentType(aType)
    }

    fun dependentFunctions(): List<FunctionInvocation> {
        return carrier.dependentFunctions()
    }

    fun dependentFunctionSubject(): Subject<FunctionInvocation> {
        return carrier.dependentFunctionSubject()
    }

    fun dependentTypes(): List<GenType> {
        return carrier.dependentTypes()
    }

    fun dependentTypesSubject(): Subject<GenType> {
        return carrier.dependentTypesSubject()
    }

    fun noteDependencies(d: Dependency) {
        carrier.noteDependencies(d)
    }

    fun _getIdentIAResolvable(aIdentIA: IdentIA): DT_Resolvabley {
        return carrier._getIdentIAResolvable(aIdentIA)
    }

    override fun add(aName: InstructionName, args_: List<InstructionArgument>, ctx: Context): Int {
        return carrier.add(aName, args_, ctx)
    }

    override fun addContext(context: Context, r: Range) {
        carrier.addContext(context, r)
    }

    override fun addElement(aElement: OS_Element, aDeduceElement: DeduceElement) {
        carrier.addElement(aElement, aDeduceElement)
    }

    override fun addIdentTableEntry(ident: IdentExpression, context: Context): Int {
        return carrier.addIdentTableEntry(ident, context)
    }

    override fun addLabel(): Label {
        return carrier.addLabel()
    }

    override fun addLabel(base_name: String, append_int: Boolean): Label {
        return carrier.addLabel(base_name, append_int)
    }

    override fun addVariableTableEntry(name: String?, vtt: VariableTableType?, type: TypeTableEntry?, el: OS_Element?): Int {
        return carrier.addVariableTableEntry(name, vtt, type, el)
    }

    fun buildDrTypeFromNonGenericTypeName(aNonGenericTypeName: TypeName?): DR_Type {
        return carrier.buildDrTypeFromNonGenericTypeName(aNonGenericTypeName)
    }

    fun elements(): Map<OS_Element, DeduceElement> {
        return carrier.elements()
    }

    override fun expectationString(): String {
        return carrier.expectationString()
    }

    override fun findLabel(index: Int): Label? {
        return carrier.findLabel(index)
    }

    override fun get_assignment_path(expression: IExpression, generateFunctions: GenerateFunctions, context: Context): InstructionArgument {
        return carrier.get_assignment_path(expression, generateFunctions, context)
    }

    override fun getCode(): Int {
        return carrier.code
    }

    override fun getConstTableEntry(index: Int): ConstantTableEntry {
        return carrier.getConstTableEntry(index)
    }

    override fun getContextFromPC(pc: Int): Context {
        return carrier.getContextFromPC(pc)
    }

    override fun getDependency(): Dependency {
        return carrier.dependency
    }

    override fun getFunctionName(): String {
        return carrier.functionName
    }

    override fun getGenClass(): EvaNode {
        return carrier.genClass
    }

    fun getIdent(aIdent: IdentExpression?, aVteBl1: VariableTableEntry?): DR_Ident {
        return carrier.getIdent(aIdent, aVteBl1)
    }

    fun getIdent(aIdentTableEntry: IdentTableEntry): DR_Ident {
        return carrier.getIdent(aIdentTableEntry)
    }

    fun getIdent(aVteBl1: VariableTableEntry?): DR_Ident {
        return carrier.getIdent(aVteBl1)
    }

    override fun getIdentIAPathNormal(ia2: IdentIA): String {
        return carrier.getIdentIAPathNormal(ia2)
    }

    override fun getIdentTableEntry(index: Int): IdentTableEntry {
        return carrier.getIdentTableEntry(index)
    }

    override fun getIdentTableEntryFor(expression: IExpression): IdentTableEntry? {
        return carrier.getIdentTableEntryFor(expression)
    }

    override fun getInstruction(anIndex: Int): Instruction {
        return carrier.getInstruction(anIndex)
    }

    override fun getParent(): EvaContainerNC {
        return carrier.parent
    }

    fun getProcCall(aZ: IExpression?, aPte: ProcTableEntry?): DR_ProcCall {
        return carrier.getProcCall(aZ, aPte)
    }

    override fun getProcTableEntry(index: Int): ProcTableEntry {
        return carrier.getProcTableEntry(index)
    }

    override fun getTypeTableEntry(index: Int): TypeTableEntry {
        return carrier.getTypeTableEntry(index)
    }

    fun getVar(aElement: VariableStatement?): DR_Variable {
        return carrier.getVar(aElement)
    }

    override fun getVarTableEntry(index: Int): VariableTableEntry {
        return carrier.getVarTableEntry(index)
    }

    override fun instructions(): List<Instruction> {
        return carrier.instructions()
    }

    override fun labels(): List<Label> {
        return carrier.labels()
    }

    override fun newTypeTableEntry(type1: TypeTableEntry.Type, type: OS_Type): TypeTableEntry {
        return carrier.newTypeTableEntry(type1, type)
    }

    override fun newTypeTableEntry(type1: TypeTableEntry.Type, type: OS_Type, expression: IExpression): TypeTableEntry {
        return carrier.newTypeTableEntry(type1, type, expression)
    }

    override fun newTypeTableEntry(type1: TypeTableEntry.Type, type: OS_Type, expression: IExpression, aTableEntryIV: TableEntryIV): TypeTableEntry {
        return carrier.newTypeTableEntry(type1, type, expression, aTableEntryIV)
    }

    override fun newTypeTableEntry(type1: TypeTableEntry.Type, type: OS_Type, aTableEntryIV: TableEntryIV): TypeTableEntry {
        return carrier.newTypeTableEntry(type1, type, aTableEntryIV)
    }

    override fun nextTemp(): Int {
        return carrier.nextTemp()
    }

    override fun onGenClass(aOnGenClass: OnGenClass) {
        carrier.onGenClass(aOnGenClass)
    }

    override fun place(label: Label) {
        carrier.place(label)
    }

    //    @Override
    //    public BaseEvaFunction.@NotNull __Reactive reactive() {
    //        return carrier.reactive();
    //    }
    override fun resolveTypeDeferred(aType: GenType) {
        carrier.resolveTypeDeferred(aType)
    }

    override fun setClass(aNode: EvaNode) {
        carrier.setClass(aNode)
    }

    override fun setCode(aCode: Int) {
        carrier.code = aCode
    }

    override fun setParent(aGeneratedContainerNC: EvaContainerNC) {
        carrier.parent = aGeneratedContainerNC
    }

    override fun typeDeferred(): Eventual<GenType> {
        return carrier.typeDeferred()
    }

    override fun typePromise(): Eventual<GenType> {
        return carrier.typePromise()
    }

    override fun vte_lookup(text: String): InstructionArgument? {
        return carrier.vte_lookup(text)
    }

    fun de3_Promise(): Eventual<DeduceElement3_Constructor> {
        return carrier.de3_Promise()
    }

    override fun getFD(): FunctionDef {
        return carrier.fd
    }

    override fun getSelf(): VariableTableEntry? {
        return carrier.self
    }

    fun identityString(): String {
        return carrier.identityString()
    }

    fun module(): OS_Module {
        return carrier.module()
    }

    fun name(): String {
        return carrier.name()
    }

    fun setFunctionInvocation(fi: FunctionInvocation) {
        carrier.setFunctionInvocation(fi)
    }

    override fun toString(): String {
        return carrier.toString()
    }

    override fun reactive(): BaseEvaConstructor_Reactive {
        return carrier.reactive() as BaseEvaConstructor_Reactive
    }

    override fun getCarrier(): EvaConstructor {
        return carrier
    }

    companion object {
        fun _getIdentIAPathList(oo: InstructionArgument): List<InstructionArgument> {
            return BaseEvaFunction._getIdentIAPathList(oo)
        }

        fun _getIdentIAResolvableList(oo: InstructionArgument): List<DT_Resolvable> {
            return BaseEvaFunction._getIdentIAResolvableList(oo)
        }
    }
}

private fun EvaConstructor._getIdentIAResolvable(aIdentIA: IdentIA): DT_Resolvabley {
    TODO("Not yet implemented")
}

private fun BaseEvaFunction.Companion._getIdentIAResolvableList(oo: InstructionArgument): List<DT_Resolvable> {
    TODO("Not yet implemented")
}
