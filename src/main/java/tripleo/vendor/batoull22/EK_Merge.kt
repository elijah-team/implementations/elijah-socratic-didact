package tripleo.vendor.batoull22

/**
 * @author tripleo
 */
class EK_Merge(var first: EK_Fact?, var second: EK_Fact?, var result: EK_Fact?) {
    fun result(): EK_Fact? {
        return this.result
    }

    fun second(): EK_Fact? {
        return this.second
    }

    fun first(): EK_Fact? {
        return this.first
    }
}
