package tripleo.elijah.stages.deduce.tastic

import org.jdeferred2.DoneCallback
import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.comp.Finally.Outs
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.AliasStatementImpl
import tripleo.elijah.lang.nextgen.names.i.EN_Name
import tripleo.elijah.lang.nextgen.names.impl.ENU_ResolveToFunction
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.tastic.FT_FCA_IdentIA.FakeDC4
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

class DT_External_2(private val ite: IdentTableEntry,
                    aModule: OS_Module,
                    aPte: ProcTableEntry,
                    aDc: FakeDC4,
                    aLOG: ElLog,
                    aCtx: Context,
                    aGeneratedFunction: BaseEvaFunction,
                    aInstructionIndex: Int,
                    aIdentIA: IdentIA,
                    private val vte: VariableTableEntry) : DT_External {
    private val mod1Promise = DeferredObject<OS_Module, Void, Void>()
    private val module: OS_Module
    private var mod1: OS_Module? = null
    private val pte: ProcTableEntry
    private val dc: FakeDC4
    private val LOG: ElLog

    // README in essence, why is this set in the constructor? Isn't this what we are
    // supposed to be doing?
    private val resolved_element = ite.resolvedElement
    private val ctx: Context
    private val generatedFunction: BaseEvaFunction
    private val instructionIndex: Int
    private val identIA: IdentIA

    private val _p_resolvedElementPromise = DeferredObject2<OS_Element, ResolveError, Void>()

    init {
        if (resolved_element != null) {
            _p_resolvedElementPromise.resolve(resolved_element)
            mod1 = resolved_element.context.module()
            mod1Promise.resolve(mod1)
        }

        module = aModule
        pte = aPte
        dc = aDc
        LOG = aLOG
        identIA = aIdentIA
        ctx = aCtx
        generatedFunction = aGeneratedFunction
        instructionIndex = aInstructionIndex

        //
        _p_resolvedElementPromise.then { el: OS_Element -> this.onResolvedElement(el) }
    }

    private /* static */ fun __make2_1__createFunctionInvocation(pte__: ProcTableEntry,
                                                                 dc__: FakeDC4, LOG__: ElLog, resolved_element: FunctionDef,
                                                                 parent: OS_Element?, invocation2: IInvocation) {
        val fi = dc.newFunctionInvocation(resolved_element, pte, invocation2)

        val dmf = dc.deferred_member_function(parent, invocation2, resolved_element, fi)

        dmf.typeResolved().then { result: GenType ->
            LOG.info("2717 " + dmf.functionDef + " " + result)
            if (pte.typeDeferred().isPending) pte.typeDeferred().resolve(result)
            else {
                val y = 2
            }
        }
    }

    private /* static */ fun __make2_1__hasFunctionInvocation(pte: ProcTableEntry,
                                                              fi: FunctionInvocation) {
        fi.generateDeferred().then { ef: BaseEvaFunction ->
            val result = _inj().new_GenTypeImpl()
            result.functionInvocation = fi
            result.node = ef

            assert(fi.pte !== pte)
            if (fi.pte == null) {
                SimplePrintLoggerToRemoveSoon.println_err_4("******************************* Unexpected error")
                return@then
            }

            if (fi.pte.typeResolvePromise().isResolved) {
                fi.pte.typeResolvePromise().then { gt: GenType ->
                    result.resolved = gt.resolved
                    gt.copy(result)
                }
            }

            LOG.info("2717c " + ef.fd + " " + result)
            if (pte.typeDeferred().isPending) {
                pte.typeDeferred().resolve(result)
            }
        }
    }

    private fun _inj(): DeduceTypes2Injector {
        return dc._deduceTypes2()
    }

    override fun actualise(aDt2: DeduceTypes2) {
        // if (mod1 != module) { // README this is kinda by construction
        assert(aDt2.module !== module)
        val ite1 = identIA.entry

        // assert ite._p_resolvedElementPromise.isResolved();
        ite1._p_resolvedElementPromise.then { orig_e: OS_Element ->
            var e = orig_e
            //			LOG.info(String.format("600 %s %s", xx ,e));
//			LOG.info("601 "+identIA.getEntry().getStatus());
            val pre_resolved_element = ite1.resolvedElement

            if (pre_resolved_element == null) {
                dc.found_element_for_ite(generatedFunction, ite1, e, ctx)
            }

            val resolved_element1 = ite1.resolvedElement

            var set_alias = false

            if (orig_e is AliasStatement) {
                set_alias = true

                while (e is AliasStatement) {
                    e = dc._resolveAlias(e)!!
                }
            }

            assert(e === resolved_element1 ||  /* HACK */resolved_element1 is AliasStatementImpl || resolved_element1 == null)
            if (e is OS_Element2) {
                val name: EN_Name = e.enName
                if (set_alias) name.addUnderstanding(_inj().new_ENU_AliasedFrom(orig_e as AliasStatement))
            }

            if (pte.status != BaseTableEntry.Status.KNOWN) {
                pte.setStatus(BaseTableEntry.Status.KNOWN, _inj().new_ConstructableElementHolder(e, identIA))
            }
            pte.onFunctionInvocation { functionInvocation: FunctionInvocation ->
                functionInvocation.generateDeferred().then { bgf: BaseEvaFunction ->
                    val pe = dc.promiseExpectation(bgf, "Function Result type")
                    bgf.typePromise().then { result: GenType ->
                        pe.satisfy(result)
                        val tte = generatedFunction.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT,
                                result.resolved) // TODO there has to be a better way
                        tte.genType.copy(result)
                        vte.addPotentialType(instructionIndex, tte)
                    }
                }
            }
        }

        ite1._p_resolvedElementPromise.fail { aResolveError: ResolveError ->
            // TODO create Diagnostic and quit
            LOG.info("1005 Can't find element for $aResolveError")
        }
    }

    fun onResolve(cb: DoneCallback<OS_Element>?) {
        _p_resolvedElementPromise.then(cb) // TODO ?? shaky
    }

    private fun onResolvedElement(el: OS_Element) {
        if (el is FunctionDef) {
            val name: EN_Name = el.enName

            var __test_resolved: FunctionDef? = null

            for (understanding in name.understandings) {
                if (understanding is ENU_ResolveToFunction) {
                    __test_resolved = understanding.fd()
                }
            }

            val parent = el.getParent()

            if (__test_resolved != null) {
                assert(__test_resolved === el)
            }

            val target_p2 = pte.dpc.targetP2()

            target_p2!!.then { target0: DeduceElement? ->
                if (target0 == null) {
                    val c = el.getContext().module().compilation
                    if (c.reports().outputOn(Outs.OutDeduce_220)) SimplePrintLoggerToRemoveSoon.println_err_4("542542  ")
                    return@then
                }
                var invocation2 = target0.declAnchor().invocation
                if (invocation2 is ClassInvocation) {
                    invocation2 = dc.registerClassInvocation(invocation2)
                }
                if (invocation2 !is FunctionInvocation) {
                    __make2_1__createFunctionInvocation(pte, dc, LOG, el, parent, invocation2)
                } else {
                    __make2_1__hasFunctionInvocation(pte, invocation2)
                }
            }
        }
    }

    override fun onTargetModulePromise(): Promise<OS_Module, Void, Void> {
        return mod1Promise
    }

    override fun targetModule(): OS_Module { // [T1160118]
        // assert mod1Promise.isResolved(); // !!
        return mod1!!
    }

    /* extra large wtf */
    override fun tryResolve() {
        val bl = ite.backlink

        if (bl is ProcIA) {
            val bl_pte: ProcTableEntry = bl.entry

            if (bl_pte.evaExpression?.entry is IdentTableEntry) {
                val ite1 = bl_pte.evaExpression?.entry as IdentTableEntry
                ite1._p_resolvedElementPromise.then(DoneCallback<OS_Element> { el: OS_Element ->
                    val ctx2 = el.context
                    val lrl = ctx2.lookup(ite.ident.get().text)
                    val e = lrl.chooseBest(null)
                    if (e != null) {
                        _p_resolvedElementPromise.resolve(e)
                    }
                })
            }
        }
    }
}
