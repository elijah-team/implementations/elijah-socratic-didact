package tripleo.elijah.stages.hooligan.pipeline_impl

import tripleo.elijah.comp.Compilation
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.nextgen.outputstatement.EG_Statement.Companion.of
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.nextgen.outputtree.EOT_OutputFileImpl
import tripleo.elijah.nextgen.outputtree.EOT_OutputType
import tripleo.elijah.stages.write_stage.pipeline_impl.EOT_FileNameProvider__ofString
import tripleo.elijah.stages.write_stage.pipeline_impl.mkf11
import tripleo.elijah.util.CompletableProcess
import tripleo.elijah.util2.DebugFlags
import tripleo.elijah.world.i.WorldModule
import java.util.stream.Collectors

class LawabidingcitizenPipelineImpl {
    fun run(compilation: Compilation) {
        val hooligan = Lawabidingcitizen()

        compilation.world().addModuleProcess(object : CompletableProcess<WorldModule> {
            override fun add(item: WorldModule) {
                // README ignored, we are taking list at end
            }

            override fun complete() {
                val worldModules = compilation.world().modules()

                if (!DebugFlags.Lawabidingcitizen_disabled) {
                    val sw = hooligan.__modules2(worldModules)
                    val cot = compilation.outputTree

                    val inputs = worldModules.stream()
                            .map { obj: WorldModule -> obj.eitInput }
                            .collect(Collectors.toList())

                    val text = sw.text
                    val seq = of(text, withMessage("modules-sw-writer"))
                    val off = EOT_OutputFileImpl(inputs, EOT_FileNameProvider__ofString("modules-sw-writer"), EOT_OutputType.SWW, seq)

                    cot.add(off)
                }
            }

            override fun error(d: Diagnostic) {
            }

            override fun preComplete() {
            }

            override fun start() {
            }
        })
    }
}
