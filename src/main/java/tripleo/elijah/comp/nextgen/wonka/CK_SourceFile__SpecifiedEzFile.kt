package tripleo.elijah.comp.nextgen.wonka

import com.google.common.base.Preconditions
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.IO
import tripleo.elijah.comp.specs.EzCache
import tripleo.elijah.comp.specs.EzSpec__
import tripleo.elijah.util.Operation2
import tripleo.wrap.File
import java.io.FileNotFoundException
import java.util.function.Supplier

class CK_SourceFile__SpecifiedEzFile(override val file: File) : __CK_SourceFile__AbstractEzFile() {
    override fun process_query(): Operation2<Any?>? {
        val io = compilation!!.io
        val ezCache = compilation!!.compilationEnclosure.compilationRunner.ezCache()
        val oci = process_query(io, ezCache)

        super.asserverate()

        return oci as Operation2<Any?>
    }

    private fun process_query(io: IO, ezCache: EzCache): Operation2<CompilerInstructions> {
        val fileName = file_name()
        Preconditions.checkArgument(__CK_SourceFile__AbstractEzFile.Companion.isEzFile(fileName))

        // FIXME 12/03 use of // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
        val ezSpec = EzSpec__(
                fileName,
                file, Supplier {
            try {
                return@Supplier file.readFile(io)
            } catch (aE: FileNotFoundException) {
                throw RuntimeException(aE)
            }
        }
        )

        return super.realParseEzFile__(ezSpec, ezCache)
    }

    private fun file_name(): String {
        return file.name
    }

    override val fileName: String?
        get() = file_name()
}
