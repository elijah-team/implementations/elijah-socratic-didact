package tripleo.elijah.typinf

class BoolType : Type {
    override fun equals(obj: Any?): Boolean {
        return obj is BoolType || super.equals(obj)
    }

    override fun toString(): String {
        return "Bool"
    }
}
