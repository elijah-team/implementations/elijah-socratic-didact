package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.elijah.comp.Compilation
import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.inputtree.EIT_InputType
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EG_Statement.Companion.of
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.util.Helpers
import java.util.*
import java.util.stream.Collectors

class LSPrintStream : XPrintStream {
    private val sb = StringBuilder()
    private val ff: MutableList<String> = ArrayList()

    fun addFile(aS: String) {
        ff.add(aS)
    }

    val result: LSResult
        get() = LSResult(Helpers.List_of(string), ff)

    val string: String
        get() = sb.toString()

    override fun println(aS: String) {
        sb.append(aS)
        sb.append('\n')
    }

    class LSResult(private val buffer: List<String>, private val fs: List<String>) {
        fun fs2(c: Compilation?): List<EIT_Input> {
            return fs.stream().map { s: String? -> MyEIT_Input(c, s) }.collect(Collectors.toList())
        }

        val statement: List<EG_Statement>
            get() = buffer.stream().map { str: String? -> of(str!!, withMessage("WriteBuffers")) }
                    .collect(Collectors.toList())

        fun buffer(): List<String> {
            return buffer
        }

        fun fs(): List<String> {
            return fs
        }

        override fun hashCode(): Int {
            return Objects.hash(buffer, fs)
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as LSResult
            return this.buffer == that.buffer && (this.fs == that.fs)
        }

        override fun toString(): String {
            return "LSResult[" +
                    "buffer=" + buffer + ", " +
                    "fs=" + fs + ']'
        }
    }

    class MyEIT_Input(private val c: Compilation?, private val s: String?) : EIT_Input {
        override val type: EIT_InputType
            get() = EIT_InputType.ELIJAH_SOURCE
    }
}
