package tripleo.elijah.comp

import tripleo.elijah.ci.LibraryStatementPart
import tripleo.elijah.util.Operation2
import tripleo.elijah.world.i.WorldModule
import java.io.File

class InputRequest {
    // TODO 09/08 Convert to record
    private val _file: File
    private val lsp: LibraryStatementPart?
    private var op: Operation2<WorldModule>? = null

    constructor(aFile: File, aLsp: LibraryStatementPart?) {
        _file = aFile
        lsp = aLsp
    }

    constructor(aFile: tripleo.wrap.File, aLsp: LibraryStatementPart?) {
        _file = aFile.wrapped()
        lsp = aLsp
    }

    fun file(): File {
        return _file
    }

    fun lsp(): LibraryStatementPart? {
        return lsp
    }

    fun op(): Operation2<WorldModule>? {
        return op
    }

    fun setOp(aOwm: Operation2<WorldModule>?) {
        op = aOwm
    }
} //public record InputRequest (File file, boolean do_out, @Nullable LibraryStatementPart lsp) {
//	private       Holder<Operation2<WorldModule>> hop = new Holder<>(); // FIXME record members mus be static; use lombok, but eclipse, arrg -- no, use a generator!! (xtend??)
//
//	public void setOp(final Operation2<WorldModule> op) {
//		//op = op;
//		hop.set(op);
//	}
//
//	public Operation2<WorldModule> op() {
//		return hop.get();
//	}
//
//}
