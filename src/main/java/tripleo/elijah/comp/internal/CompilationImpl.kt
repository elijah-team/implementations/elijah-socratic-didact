/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp.internal

import com.google.common.base.Preconditions
import io.reactivex.rxjava3.core.Observer
import org.apache.commons.lang3.tuple.Triple
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.*
import tripleo.elijah.comp.Compilation.CompilationConfig
import tripleo.elijah.comp.chewtoy.JarWork
import tripleo.elijah.comp.chewtoy.JarWorkImpl
import tripleo.elijah.comp.chewtoy.PW_CompilerController
import tripleo.elijah.comp.graph.CM_Ez
import tripleo.elijah.comp.graph.CM_Ez_
import tripleo.elijah.comp.graph.CM_Module_
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.graph.i.CK_ObjectTree
import tripleo.elijah.comp.graph.i.CM_Module
import tripleo.elijah.comp.i.*
import tripleo.elijah.comp.i.extra.CompilerInputListener
import tripleo.elijah.comp.i.extra.ICompilationRunner
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.impl.DefaultCompFactory
import tripleo.elijah.comp.impl.DefaultCompilationAccess
import tripleo.elijah.comp.impl.DefaultCompilationEnclosure
import tripleo.elijah.comp.impl.LCM_Event_RootCI
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.inputs.CompilerInputMaster
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.nextgen.CP_Paths__
import tripleo.elijah.comp.nextgen.i.CP_Paths
import tripleo.elijah.comp.nextgen.pn.PN_Ping
import tripleo.elijah.comp.nextgen.pw.PW_Controller
import tripleo.elijah.comp.nextgen.pw.PW_PushWork
import tripleo.elijah.comp.percy.CN_CompilerInputWatcher
import tripleo.elijah.comp.percy.CN_CompilerInputWatcher.e
import tripleo.elijah.comp.process.CPX_CalculateFinishParse
import tripleo.elijah.comp.process.CPX_RunStepLoop
import tripleo.elijah.comp.process.CPX_RunStepsContribution
import tripleo.elijah.comp.process.CPX_Signals
import tripleo.elijah.comp.specs.ElijahCache
import tripleo.elijah.comp.specs.ElijahSpec
import tripleo.elijah.comp.specs.EzSpec
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.g.GWorldModule
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.lang.i.OS_Package
import tripleo.elijah.lang.i.Qualident
import tripleo.elijah.nextgen.comp_model.CM_CompilerInput
import tripleo.elijah.nextgen.inputtree.EIT_InputTree
import tripleo.elijah.nextgen.outputtree.EOT_OutputTree
import tripleo.elijah.stages.deduce.IFunctionMapHook
import tripleo.elijah.stages.deduce.fluffy.i.FluffyComp
import tripleo.elijah.stages.deduce.fluffy.impl.FluffyCompImpl
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.logging.ElLog.Verbosity
import tripleo.elijah.util.*
import tripleo.elijah.util2.DebugFlags
import tripleo.elijah.util2.Eventual
import tripleo.elijah.util2.EventualRegister
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah.util3._AbstractEventualRegister
import tripleo.elijah.world.i.LivingRepo
import tripleo.elijah_elevated.comp.model.Elevated_CM_Factory
import tripleo.elijah_elevated.comp.model.Elevated_CM_FactoryImpl
import tripleo.wrap.File
import java.util.*
import java.util.function.Supplier
import tripleo.elijah.comp.ET_SourceRoot as ET_SourceRoot1

class CompilationImpl(private val errSink: ErrSink, private var io: IO) : _AbstractEventualRegister(), Compilation, EventualRegister { //
	private val _ciws: MutableList<CN_CompilerInputWatcher>
	private val _ci_models: MutableMap<CompilerInput, CM_CompilerInput>
	private val _ciw_buffer: MutableList<Triple<e, CompilerInput, Any>>

	private val _fluffyComp: FluffyCompImpl

	//	@Getter
	private val cfg: CompilationConfig

	//	@Getter
	private val compilationEnclosure: CompilationEnclosure

	//	@Getter
	private val _cis: CIS

	//	@Getter
	//	private final CK_Monitor                          defaultMonitor;
	//	@Getter
	private val use: USE
	private val _con: CompFactory
	private val _repo: LivingRepo
	private val paths: CP_Paths
	private val _compilationNumber: Int
	private val master: CompilerInputMaster
	private val _finally: Finally
	private val objectTree: CK_ObjectTree
	private val specToModuleMap: MutableMap<ElijahSpec, CM_Module>
	private val moduleToCMMap: MutableMap<OS_Module, CM_Module>
	private val specToEzMap: MutableMap<EzSpec, CM_Ez>
	private val xxx: MutableList<CompilerInstructions>
	private val cci_listener: CCI_Acceptor__CompilerInputListener
	private val pw_controller: PW_Controller
	private val lcm: LCM

	@get:Throws(WorkException::class)
	var jarwork: JarWork? = null
		get() {
			if (field == null) field = JarWorkImpl()
			return field
		}
		private set
	private var _input_tree: EIT_InputTree? = null
	private var _output_tree: EOT_OutputTree? = null
	private val _pa: IPipelineAccess? = null
	private var _inside = false
	private var __advisement: CompilerInput? = null
	private var aICompilationAccess3: ICompilationAccess3? = null

	// back and forth
	val defaultMonitor: CK_Monitor
	private var cpxSignals: CPX_Signals? = null
	private var _stepsContribution: CPX_RunStepsContribution? = null

	override fun ____m() {
		try {
			val jarwork1 = jarwork!!
			jarwork1.work()
		} catch (aE: WorkException) {
			//throw new RuntimeException(aE);
			aE.printStackTrace()
		}
	}

	override fun __pw_controller(): PW_Controller {
		return pw_controller
	}

	fun _access(): ICompilationAccess {
		return DefaultCompilationAccess(this)
	}

	override fun getObjectTree(): CK_ObjectTree {
		return objectTree
	}

	override fun errorCount(): Int {
		return errSink.errorCount()
	}

	val compilationAccess3: ICompilationAccess3
		get() {
			if (aICompilationAccess3 == null) {
				aICompilationAccess3 = object : ICompilationAccess3 {
					override fun getComp(): Compilation {
						return this@CompilationImpl
					}

					override fun getSilent(): Boolean {
						return comp.cfg().silent
					}

					override fun addLog(aLog: ElLog) {
						comp.compilationEnclosure.addLog(aLog)
					}

					override fun writeLogs(aSilent: Boolean) {
						assert(!aSilent)
						comp.compilationEnclosure.writeLogs()
					}

					override fun getPipelineLogic(): PipelineLogic {
						return comp.compilationEnclosure.pipelineLogic
					}

					override fun getLogs(): List<ElLog> {
						return comp.compilationEnclosure.logs
					}
				}
			}
			return aICompilationAccess3!!
		}

	override fun feedInputs(
			aCompilerInputs: List<CompilerInput>,
			aController: CompilerController,
	) {
		if (aCompilerInputs.isEmpty()) {
			aController.printUsage()
			return
		}

		// FIXME 12/04 This seems like alot (esp here)
		compilationEnclosure.compilerInput = aCompilerInputs
		aController.setEnclosure(compilationEnclosure)

//        aCompilerInputs.stream()
//                .filter { it.ty() == null  }
//                .map { it.setNull()}

		aCompilerInputs.stream()
				.filter { it.ty() == CompilerInput.Ty.SOURCE_ROOT }
				.forEach() {
					val sr = object : ET_SourceRoot1 {
						override fun getDirectoryName(): String {
							throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
						}

						override fun getDirectory(): File {
							throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
						}
					}

					this.addSourceRoot(sr)
				}

		for (compilerInput in aCompilerInputs) {
			compilerInput.setMaster(master) // FIXME this is too much i think
		}

		aController.processOptions()
		aController.runner()
	}

	override fun getCompilationClosure(): CompilationClosure {
		return object : CompilationClosure {
			override fun errSink(): ErrSink {
				return errSink
			}

			override fun getCompilation(): Compilation {
				return this@CompilationImpl
			}

			override fun io(): IO {
				return io
			}
		}
	}

	override fun getCompilationNumberString(): String {
		return String.format("%08x", _compilationNumber)
	}

	override fun getCompilerInputListener(): CompilerInputListener {
		return cci_listener
	}

	override fun getErrSink(): ErrSink {
		return errSink
	}

	override fun getPackage(pkg_name: Qualident): OS_Package {
		return _repo.getPackage(pkg_name.toString())
	}

	override fun getProjectName(): String {
		return rootCI.name
	}

	override fun getRootCI(): CompilerInstructions {
		val compilerInstructions = cci_listener._root()
		if (/*compilerInstructions != null &&*/ __advisement == null) {
			try {
				__advisement = compilerInstructions.profferCompilerInput()
			} catch (e: IllegalStateException) {
				/*debug-skip*/
			}
		}
		return compilerInstructions!!
	}

	override fun setRootCI(rootCI: CompilerInstructions) {
		//cci_listener.id.root = rootCI;
//		throw new UnintendedUseException("maybe we can just remove this??");
		val y = 2
	}

	override fun makePackage(pkg_name: Qualident): OS_Package = world().makePackage(pkg_name)

	override fun getIO(): IO = io

	override fun setIO(io: IO) {
		this.io = io
	}

	override fun getFluffy(): FluffyComp = _fluffyComp

	override fun hasInstructions(cis: List<CompilerInstructions>, pa: IPipelineAccess): Operation<Ok> {
		if (false && DebugFlags._pancake_lcm_gate) {
			assert(cis.size > 0)
			// don't inline yet b/c getProjectName
			val rootCI = cis[0]

			setRootCI(rootCI)

			lcm.asv(rootCI, LCM_Event_RootCI.INSTANCE)

			return Operation.success(Ok.instance())
		} else {
			if (cis.isEmpty()) {
				rootCI = cci_listener._root()
			} else if (rootCI == null) {
				rootCI = cis[0]
			}

			if (!_inside) {
				_inside = true

				val rootCI = rootCI
				rootCI.advise(__advisement!!)

				val lcmHandleEvent = LCM_HandleEvent(this.lcmAccess,
						this.lcm,
						null,
						LCM_Event_RootCI.INSTANCE)
				LCM_Event_RootCI.INSTANCE.handle(lcmHandleEvent)
			} else {
				NotImplementedException.raise_stop()
				//throw new UnintendedUseException();
			}

			return Operation.success(Ok.instance())
		}
	}

	override fun moduleBuilder(): ModuleBuilder = ModuleBuilder(this)

	override fun paths(): CP_Paths {
//		assert /*m*/paths != null;
		return paths
	}

	override fun provideCio(aSupplier: Supplier<CompilerInstructionsObserver>?) {
		if (aSupplier == null) {
			val cio = CompilerInstructionsObserver(this)
			_cis().set_cio(cio)
		} else {
			throw UnintendedUseException("case not implemented")
		}
	}

	override fun pushItem(aci: CompilerInstructions) {
		if (xxx.contains(aci)) {
			SimplePrintLoggerToRemoveSoon.println_err_4("** [CompilerInstructions::pushItem] duplicate instructions: " + aci.filename)
			return
		} else {
			xxx.add(aci)
			_cis.onNext(aci)
		}
	}

	override fun reports(): Finally = _finally

	override fun subscribeCI(aCio: Observer<CompilerInstructions>) = _cis.subscribe(aCio)

	override fun cfg(): CompilationConfig = cfg

	override fun getInputs(): List<CompilerInput> {
		//return _inputs;
		throw UnintendedUseException("Generally advised to try another way, here for consistency")
	}

	override fun use(compilerInstructions: CompilerInstructions, aReasoning: USE_Reasoning) {
		val obj = listOf(compilerInstructions, aReasoning)

		val listener = LCM_Event {
//			TODO("Not yet implemented")
			// 	void handle(LCM_HandleEvent aHandleEvent);
//			when (it.event) {
//				is
//			}

			val ci = (it.obj as List<*>)[0] as CompilerInstructions
			val reasoning = (it.obj as List<*>)[1] as USE_Reasoning

			if (reasoning.ty() == USE_Reasoning.Type.USE_Reasoning__findStdLib) {
				pushItem(ci)
			}

			use.use(compilerInstructions)
			//cci_listener.id.add(compilerInstructions);
		}

		lcm.asv(obj, listener)
	}

	override fun world(): LivingRepo = _repo

	override fun use_elijahCache(): ElijahCache = use.elijahCache

	override fun pushWork(aInstance: PW_PushWork, aPing: PN_Ping<*>?) =
			(pw_controller as PW_CompilerController).submitWork(aInstance)

	override fun megaGrande(aSpec: ElijahSpec, aModuleOperation: Operation2<OS_Module>): CM_Module {
		val result: CM_Module
		if (specToModuleMap.containsKey(aSpec)) {
			result = specToModuleMap[aSpec]!!
		} else {
			assert(aModuleOperation.mode() == Mode.SUCCESS)
			result = megaGrande(aModuleOperation.success())
			specToModuleMap[aSpec] = result
		}

		result.advise(aSpec)

		//result.advise(aModuleOperation);
		return result
	}

	override fun megaGrande(aSpec: EzSpec): CM_Ez {
		val result: CM_Ez
		if (specToEzMap.containsKey(aSpec)) {
			result = specToEzMap[aSpec]!!
		} else {
			//assert aModuleOperation.mode() == Mode.SUCCESS;
			//result = megaGrande(aModuleOperation.success());
			result = CM_Ez_()
			specToEzMap[aSpec] = result
		}

		result.advise(aSpec)

		//result.advise(aModuleOperation);
		return result
	}

	override fun getLCMAccess(): LCM_CompilerAccess {
		val _c = this
		return object : LCM_CompilerAccess {
			override fun c(): Compilation {
				return _c
			}

			override fun cr(): ICompilationRunner {
				return c().runner
			}

			override fun cfg(): CompilationConfig {
				return c()._cfg()
			}

			override fun getModel(): LCM {
				return (c() as CompilationImpl).lcm
			}
		}
	}

	override fun getRunner(): ICompilationRunner {
		return getCompilationEnclosure().compilationRunner
	}

	override fun _cfg(): CompilationConfig {
		return this.cfg
	}

	override fun get(aCompilerInput: CompilerInput): CM_CompilerInput {
		if (_ci_models.containsKey(aCompilerInput)) return _ci_models[aCompilerInput]!!

		val result = CM_CompilerInput(aCompilerInput, this)
		_ci_models[aCompilerInput] = result
		return result
	}

	/**
	 * This one is interesting as it doesnt quite fit
	 */
	override fun megaGrande(aModule: OS_Module): CM_Module {
		val singleModule = modelFactory().singleModule(aModule)

		val result: CM_Module
		if (moduleToCMMap.containsKey(aModule)) {
			result = moduleToCMMap[aModule]!!
		} else {
			result = CM_Module_()
			moduleToCMMap[aModule] = result
		}

		result.advise(Operation2.success(aModule))

		assert(singleModule.module === result._getModule() // what are we checking??
		)
		return result
	}

	var _modelFactory: Elevated_CM_Factory? = null

	override fun modelFactory(): Elevated_CM_Factory {
		if (this._modelFactory == null) {
			_modelFactory = Elevated_CM_FactoryImpl(this)
		}
		return _modelFactory!!
	}

	override fun contribute(o: Any) {
		if (o is CPX_RunStepsContribution) {
			_stepsContribution = o
		} else {
			assert(false)
		}
	}

	override fun addSourceRoot(aSourceRoot: ET_SourceRoot1?) {
		//throw UnintendedUseException("implement me when you are feeling unfulfilled")
		//val y=2
	}

	override fun addStageAssertion(string: String?) {
		throw UnintendedUseException("implement me when you are feeling unfulfilled")
	}

	override fun triggerResults() {
		throw UnintendedUseException("implement me when you are feeling unfulfilled")
	}

	override fun get(aO: Any): CM_CompilerInput {
		System.err.println(aO.javaClass.name)
		if (aO.javaClass.name == "tripleo.elijah.nextgen.comp_model.CM_CompilerInput") {
			return aO as CM_CompilerInput
		}
		throw UnintendedUseException("failure mode")
	}

	override fun world2(): LivingRepo {
		return _repo
	}

	override fun hasInstructions2(cis: List<CompilerInstructions>, pa: IPipelineAccess): Operation<Ok> {
		val pa1 = arrayOfNulls<IPipelineAccess>(1)
		compilationEnclosure.pipelineAccessPromise.then { apa: IPipelineAccess? -> pa1[0] = apa }
		Preconditions.checkNotNull(pa1[0])
		return hasInstructions(cis, pa1[0]!!)
	}

	override fun pa(): IPipelineAccess {
		val pa = arrayOfNulls<IPipelineAccess>(1)

		compilationEnclosure.pipelineAccessPromise.then { apa: IPipelineAccess? -> pa[0] = apa }

		Preconditions.checkNotNull(pa[0])
		return pa[0]!!
	}

	override fun findPrelude(prelude_name: String): Operation2<GWorldModule> {
		val prelude = use.findPrelude(prelude_name)

		if (prelude!!.mode() == Mode.SUCCESS) {
			val m = prelude.success()
			val prelude1 = _con.createWorldModule(m)

			return Operation2.success(prelude1)
		} else {
			return Operation2.failure(prelude.failure()) // FIXME 10/15 chain
		}
	}

	override fun set_pa(aPipelineAccess: GPipelineAccess) {
		//set_pa((IPipelineAccess) aPipelineAccess);
		throw UnintendedUseException("not in ep")
	}

	override fun _cis(): CIS {
		return _cis
	}

	override fun con(): CompFactory {
		return _con
	}

	override fun getOutputTree(): EOT_OutputTree {
		if (_output_tree == null) {
			_output_tree = _con.createOutputTree()
		}

		return _output_tree!!
	}

	override fun getInputTree(): EIT_InputTree {
		if (_input_tree == null) {
			_input_tree = _con.createInputTree()
		}

		return _input_tree!!
	}

	override fun subscribeCI(aCio: ICompilerInstructionsObserver) {
		throw UnintendedUseException() // If this doesn't trigger on Core tests, remove
	}

	fun testMapHooks(ignoredAMapHooks: List<IFunctionMapHook?>?) {
		// pipelineLogic.dp.
	}

	fun _paths(): CP_Paths {
		assert(paths != null)
		return paths
	}

	override fun addCompilerInputWatcher(aCNCompilerInputWatcher: CN_CompilerInputWatcher) {
		_ciws.add(aCNCompilerInputWatcher)
	}

	override fun compilerInputWatcher_Event(aEvent: e, aCompilerInput: CompilerInput, aO: Any) {
		if (_ciws.isEmpty()) {
			_ciw_buffer.add(Triple.of(aEvent, aCompilerInput, aO))
		} else {
			if (!_ciw_buffer.isEmpty()) {
				for (triple in _ciw_buffer) {
					for (ciw in _ciws) {
						ciw.event(triple.left, triple.middle, triple.right)
					}
				}
				_ciw_buffer.clear()
			}
			for (ciw in _ciws) {
				ciw.event(aEvent, aCompilerInput, aO)
			}
		}
	}

	enum class CompilationAlways {
		;

		enum class Tokens {
			;

			companion object {
				// README 10/20 Disjointed needs annotation
				//  12/04 ServiceLoader
				@JvmField
				val COMPILATION_RUNNER_FIND_STDLIB2: DriverToken = DriverToken.makeToken("COMPILATION_RUNNER_FIND_STDLIB2")

				@JvmField
				val COMPILATION_RUNNER_START: DriverToken = DriverToken.makeToken("COMPILATION_RUNNER_START")
			}
		}

		companion object {
			@JvmStatic
			fun defaultPrelude(): String {
				return "c"
			}
		}
	}

	override fun getCompilationEnclosure(): CompilationEnclosure {
		// TODO Auto-generated method stub
		return compilationEnclosure
	}

	override fun signals(): CPX_Signals {
		if (cpxSignals == null) {
			cpxSignals = object : CPX_Signals {
				override fun subscribeCalculateFinishParse(cp_OutputPath: CPX_CalculateFinishParse) {
					pathsEventual.then { paths1: CP_Paths -> paths1.subscribeCalculateFinishParse(cp_OutputPath) }
				}

				override fun subscribeRunStepLoop(cp_RunStepLoop: CPX_RunStepLoop) {
					// TODO 24/01/13 create process and rpc to it
					Preconditions.checkNotNull(_stepsContribution)
					val steps = _stepsContribution!!.steps
					val stepContext = _stepsContribution!!.stepsContext
					compilationEnclosure.runStepsNow(steps, stepContext)
				}

				override fun signalRunStepLoop(aRoot: CompilerInstructions) {
					compilationEnclosure.pipelineAccessPromise.then { runner.start(aRoot, it) }
				}
			}
		}
		return cpxSignals!!
	}

	private val pathsEventual = Eventual<CP_Paths>()

	init {
		_con = DefaultCompFactory(this)
		lcm = LCM(this)
		specToModuleMap = HashMap()
		moduleToCMMap = HashMap()
		specToEzMap = HashMap()
		xxx = ArrayList()
		_compilationNumber = Random().nextInt(Int.MAX_VALUE)
		_fluffyComp = FluffyCompImpl(this)
		cfg = CompilationConfig()
		use = USE(this.compilationClosure)
		_cis = CIS()
		paths = CP_Paths__(this)
		pathsEventual.resolve(paths)
		_repo = _con.getLivingRepo()
		compilationEnclosure = DefaultCompilationEnclosure(this)
		defaultMonitor = _con.createCkMonitor()
		objectTree = _con.createObjectTree()
		master = _con.createCompilerInputMaster()
		_finally = _con.createFinally()
		pw_controller = _con.createPwController(this)
		cci_listener = CCI_Acceptor__CompilerInputListener(this)
		master.addListener(cci_listener)

		_ciws = ArrayList()
		_ci_models = HashMap()
		_ciw_buffer = ArrayList()
	}

	companion object {
		@JvmStatic
		fun gitlabCIVerbosity(): Verbosity {
			val gitlab_ci = isGitlab_ci
			return if (gitlab_ci) ElLog.Verbosity.SILENT else ElLog.Verbosity.VERBOSE
		}

		@JvmStatic
		val isGitlab_ci: Boolean
			get() = System.getenv("GITLAB_CI") != null
	}
}
//
//

