package tripleo.elijah.stages.deduce

import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.stages.gen_fn.BaseTableEntry
import tripleo.elijah.stages.gen_fn.GenType
import tripleo.elijah.stages.gen_fn.GenericElementHolderWithIntegerIA
import tripleo.elijah.stages.gen_fn.IElementHolder
import tripleo.elijah.stages.instructions.IntegerIA

class DTR_IdentExpression(private val deduceTypeResolve: DeduceTypeResolve, private val identExpression: IdentExpression,
                          private val bte: BaseTableEntry) {
    private var genType: GenType? = null

    private fun q(result: GenType) {
        if (genType === result) return  // !!??

        genType!!.copy(result) // TODO genType = result?? because we want updates...
    }

    fun run(eh: IElementHolder, genType1: GenType?) {
        this.genType = genType1

        if (eh is GenericElementHolderWithIntegerIA) {
            val integerIA: IntegerIA = eh.integerIA
            val variableTableEntry = integerIA.entry
            assert(variableTableEntry === bte)
            variableTableEntry.typeResolvePromise().then { result: GenType -> this.q(result) }
        } else {
            // final DeduceLocalVariable dlv = ((VariableTableEntry) bte).dlv;
            // dlv.setDeduceTypes2(deduceTypeResolve.backlink.__dt2, null,
            // deduceTypeResolve.backlink.__gf);

            bte.typeResolvePromise().then { result: GenType -> this.q(result) }
        }
    }
}
