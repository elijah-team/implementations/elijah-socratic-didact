package tripleo.elijah.stages.deduce.nextgen

import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action_Arena
import tripleo.elijah.stages.gen_fn.GenType

class SGTA_SetDrType(private val drType: DR_Type) : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        gt.setDrType(drType)
    }
}
