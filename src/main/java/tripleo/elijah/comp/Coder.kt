package tripleo.elijah.comp

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.world.i.WorldModule
import java.util.function.Consumer

class Coder @Contract(pure = true) constructor(private val codeRegistrar: ICodeRegistrar) {
    fun codeNode(generatedNode: EvaNode?, mod: WorldModule?) {
        val coder = this

        if (generatedNode is EvaFunction) {
            coder.codeNodeFunction(generatedNode, mod)
        } else if (generatedNode is EvaClass) {
            coder.codeNodeClass(generatedNode, mod)
        } else if (generatedNode is EvaNamespace) {
            coder.codeNodeNamespace(generatedNode, mod)
        }
    }

    fun codeNodeClass(generatedClass: EvaClass, wm: WorldModule?) {
        assert(generatedClass.living!!.code == 0)
        codeRegistrar.registerClass(generatedClass)
    }

    fun codeNodeFunction(generatedFunction: BaseEvaFunction, mod: WorldModule?) {
        assert(generatedFunction.code == 0)
        codeRegistrar.registerFunction(generatedFunction)
    }

    fun codeNodeNamespace(generatedNamespace: EvaNamespace, mod: WorldModule?) {
        assert(generatedNamespace.code1234567 == 0)
        codeRegistrar.registerNamespace(generatedNamespace)
    }

    fun codeNodes(wm: WorldModule, resolved_nodes: MutableList<EvaNode>,
                  generatedNode: EvaNode) {
        val mod = wm.module()

        if (generatedNode is EvaFunction) {
            codeNodeFunction(generatedNode, wm)
        } else if (generatedNode is EvaClass) {
            // assert generatedClass.getCode() == 0;
            if (generatedNode.living!!.code == 0) {
                codeNodeClass(generatedNode, wm)
            }

            setClassmapNodeCodes(generatedNode.classMap, wm)

            extractNodes_toResolvedNodes(generatedNode.functionMap, resolved_nodes)
        } else if (generatedNode is EvaNamespace) {
            if (generatedNode.living!!.code != 0) {
                codeNodeNamespace(generatedNode, wm)
            }

            setClassmapNodeCodes(generatedNode.classMap, wm)

            extractNodes_toResolvedNodes(generatedNode.functionMap, resolved_nodes)
        }
    }

    private fun setClassmapNodeCodes(aClassMap: Map<ClassStatement, EvaClass>, mod: WorldModule) {
        aClassMap.values.forEach(Consumer { generatedClass: EvaClass -> codeNodeClass(generatedClass, mod) })
    }

    companion object {
        private fun extractNodes_toResolvedNodes(aFunctionMap: Map<FunctionDef, EvaFunction>,
                                                 resolved_nodes: MutableList<EvaNode>) {
            aFunctionMap.values.stream()
                    .map { generatedFunction: EvaFunction ->
                        generatedFunction.idte_list.stream()
                                .filter { obj: IdentTableEntry -> obj != null }
                                .filter { obj: IdentTableEntry -> obj.isResolved }
                                .map { obj: IdentTableEntry -> obj.resolvedType() }
//                                .collect(Collectors.toList())
//                    }
                                .forEach { c -> resolved_nodes.add(c!!) }
                    }
        }
    }
}
