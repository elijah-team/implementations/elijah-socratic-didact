package tripleo.elijah.stages.write_stage.pipeline_impl

import com.google.common.collect.Multimap
import org.jetbrains.annotations.Contract
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.WritePipeline
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.stages.generate.ElSystem
import tripleo.util.buffer.Buffer

/**
 * Really a record, but state is not all set at once
 */
class WritePipelineSharedState(pa0: GPipelineAccess) {
    lateinit var _up: WritePipeline

    val c: Compilation
    val pa: IPipelineAccess = pa0 as IPipelineAccess

    var grs: GenerateResultSink? = null
    var lsp_outputs: Multimap<CompilerInstructions, String>? = null
    var mmb: Multimap<String, Buffer>? = null

    // public List<NG_OutputItem> outputs;
    var sys: ElSystem? = null

    @get:Contract(pure = true)
    @set:Contract(mutates = "this")
    var gr: GenerateResult? = null

    init {
        c = pa.compilation
    }
}
