package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputstatement.EX_Explanation_withMessage

class actionDECL_with_BUILT_IN(
		private val gf: WhyNotGarish_BaseFunction,
		private val target_name: String?,
		private val x: OS_Type,
		private val gc: GenerateC,
) : EG_Statement {
	override val explanation: EX_Explanation
		get() = EX_Explanation_withMessage("actionDECL with BUILT_IN")

	override val text: String
		get() {
			val context = gf.fd.context!!
			val type = x.resolve(context)
			val s1: String
			if (type.isUnitType) {
				// TODO still should not happen
				s1 = String.format("/*%s is declared as the Unit type*/", target_name)
			} else {
				// LOG.err("Bad potentialTypes size " + type);
				val z3 = gc.getTypeName(type)
				s1 = String.format("/*535*/Z<%s> %s; /*%s*/", z3, target_name, type.classOf)
			}
			return s1
		}
}
