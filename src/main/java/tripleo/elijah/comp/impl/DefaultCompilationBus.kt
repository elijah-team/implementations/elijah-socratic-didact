package tripleo.elijah.comp.impl

import lombok.Getter
import org.jctools.queues.MessagePassingQueue
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.hairball.H
import tripleo.elijah.comp.i.*
import tripleo.elijah.comp.i.IProgressSink.Codes
import tripleo.elijah.comp.internal.CompilationRunner.__CompRunner_Monitor
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.process.DefaultCompilerDriver
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.UnintendedUseException
import java.lang.reflect.InvocationTargetException

class DefaultCompilationBus(ace: CompilationEnclosure) : ICompilationBus {
    private val _monitor: CB_Monitor = __CompRunner_Monitor()

    @Getter
    private val compilerDriver: CompilerDriver = DefaultCompilerDriver(this)
    private val c = ace.compilationAccess.compilation as Compilation
    private val pq: MessagePassingQueue<CB_Process> = H.createQueue()
    private val _defaultProgressSink: IProgressSink = DefaultProgressSink()

    init {
        ace.setCompilerDriver(compilerDriver)
    }

    override fun defaultProgressSink(): IProgressSink {
        return _defaultProgressSink
    }

    override fun getMonitor(): CB_Monitor {
        return _monitor
    }

    override fun add(action: CB_Action) {
        pq.offer(SingleActionProcess(action, "CB_FindStdLibProcess"))
    }

    override fun add(aProcess: CB_Process) {
        pq.offer(aProcess)
    }

    override fun inst(aLazyCompilerInstructions: ILazyCompilerInstructions) {
        _defaultProgressSink.note(
                Codes.LazyCompilerInstructions_inst,
                ProgressSinkComponent.CompilationBus_,
                -1,
                arrayOf<Any>(aLazyCompilerInstructions.get()))
    }

    override fun option(aChange: CompilationChange) {
        aChange.apply(c)
    }

    override fun processes(): List<CB_Process> {
        throw UnintendedUseException("not with jctools")
        //return pq.stream().toList();//_processes;
    }

    fun runProcesses() {
        H.runProcesses_(this, pq, c)
    }

    fun logProgress(code: Int, message: String) {
        SimplePrintLoggerToRemoveSoon.println_out_4("$code $message")
    }

    // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
    override fun addCompilerChange(class1: Class<*>) {
        if (class1.isInstance(CompilationChange::class.java)) {
            try {
                val compilationChange = class1.getDeclaredConstructor(*arrayOf()).newInstance() as CompilationChange
                c.compilationEnclosure.compilationBus.option(compilationChange)
            } catch (e: InstantiationException) {
                throw Error()
            } catch (e: IllegalAccessException) {
                throw Error()
            } catch (e: InvocationTargetException) {
                throw Error()
            } catch (e: NoSuchMethodException) {
                throw Error()
            }
        }
    }

    //	@Override public void addCompilerChange(Class<?> compilationChangeClass) {
    //		if (compilationChangeClass.isInstance(CC_SetSilent.class)) {
    //			try {
    //				CompilationChange ccc = (CompilationChange) compilationChangeClass.getDeclaredConstructor(null).newInstance(null);
    //				ccc.apply(this.c);
    //			} catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
    //				throw new RuntimeException(e);
    //			}
    //		}
    //	}
    override fun getCompilerDriver(): CompilerDriver {
        // 24/01/04 back and forth
        return this.compilerDriver
    }

    companion object {
        const val DEFAULT_COMPILATION_BUS__RUN_PROCESS__EXECUTE_LOG: Int = 5757
    }
}
