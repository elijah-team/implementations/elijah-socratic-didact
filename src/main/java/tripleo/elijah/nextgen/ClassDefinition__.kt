/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.nextgen

import tripleo.elijah.g.GClassInvocation
import tripleo.elijah.g.GEvaClass
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.nextgen.composable.IComposable
import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.gen_fn.EvaClass

/**
 * Created 3/4/22 7:14 AM
 */
class ClassDefinition__(override var invocation: GClassInvocation) : ClassDefinition {
    override val primary: ClassStatement =( invocation as ClassInvocation).klass
    override val extended: Set<ClassStatement> = HashSet()

    override var node: GEvaClass? = null
        set(aNode) {
            field = aNode
        }

    override var composable: IComposable? = null

    fun getInvocation(): ClassInvocation = invocation as ClassInvocation

//    fun setInvocation(aInvocation: GClassInvocation) {
//        invocation = aInvocation as ClassInvocation
//    }
}
