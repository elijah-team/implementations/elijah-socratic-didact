package tripleo.elijah.comp.impl

import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.local.CW.CX_ParseElijahFile.ElijahSpecReader
import tripleo.elijah.comp.nextgen.i.CP_Path
import tripleo.elijah.util.Operation
import java.io.FileNotFoundException
import java.io.InputStream

internal class DefaultElijahSpecReader(private val local_prelude: CP_Path, private val c: Compilation) : ElijahSpecReader {
    override fun get(): Operation<InputStream> {
        try {
            val readFile = local_prelude.getReadInputStream(c.io)
            return Operation.success(readFile)
        } catch (aE: FileNotFoundException) {
            return Operation.failure(aE)
        }
    }
}
