package tripleo.elijah.typinf

import tripleo.elijah.util.Helpers

/**
 * Declaration mapping name = expr.
 *
 *
 * For functions expr is a Lambda node.
 */
class Decl_AST(var name: String, var expr: AstNode) : AstNode() {
    init {
        _children = Helpers.List_of(expr)
    }

    override fun toString(): String {
        return String.format("Decl(%s, %s)", name, expr)
    }
}
