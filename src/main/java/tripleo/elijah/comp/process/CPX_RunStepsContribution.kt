package tripleo.elijah.comp.process

import tripleo.elijah.comp.graph.i.CK_Steps
import tripleo.elijah.comp.graph.i.CK_StepsContext

@JvmRecord
data class CPX_RunStepsContribution(val steps: CK_Steps, val stepsContext: CK_StepsContext)
