package tripleo.elijah.comp.nextgen.wonka

import com.google.common.base.Preconditions
import com.google.common.base.Supplier
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.IO
import tripleo.elijah.comp.specs.EzCache
import tripleo.elijah.comp.specs.EzSpec__
import tripleo.elijah.util.Mode
import tripleo.elijah.util.Operation
import tripleo.elijah.util.Operation2
import tripleo.wrap.File
import java.io.FileNotFoundException
import java.io.InputStream

class CK_SourceFile__ElaboratedEzFile(protected val directory: File, protected val file_name: String) : __CK_SourceFile__AbstractEzFile() {
    override val file: File = File(directory, file_name)

    override fun process_query(): Operation2<Any?> {
        val ezCache = compilation!!.compilationEnclosure.compilationRunner.ezCache()
        val oci = process_query(compilation!!.io, ezCache)

        super.asserverate()

        return oci as Operation2<Any?>
    }

    private fun process_query(io: IO, ezCache: EzCache): Operation2<CompilerInstructions> {
        val fileName = file_name()
        Preconditions.checkArgument(isEzFile(fileName))

        val specOp = EzSpec__.of(
                fileName,
                file,  //file.getFileInputStreamOperationSupplier() // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
                Supplier {
                    try {
                        return@Supplier Operation.success<InputStream>(file.readFile(io))
                    } catch (aE: FileNotFoundException) {
                        return@Supplier Operation.failure<InputStream>(aE)
                    }
                }.get()
        )

        if (specOp.mode() == Mode.SUCCESS) {
            val ezSpec = specOp.success()
            return realParseEzFile(specOp.success(), ezCache)
        } else {
            return Operation2.failure(specOp.failure())
        }
    }

    private fun file_name(): String {
        //return this.file.wrappedFileName(); // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
        return file.wrapped().toString() // README 12/07 not just toString, but wrapped.toString
    }

    override val fileName: String
        get() = file_name()
}
