package tripleo.elijah.stages.deduce.post_bytecode

import com.google.common.base.Preconditions
import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.AliasStatementImpl
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3.DeduceElement3_Kind
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaContainer.VarTableEntry
import tripleo.elijah.stages.gen_fn.GenType
import tripleo.elijah.stages.gen_fn.TypeTableEntry
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.ReadySupplier_1

class DeduceElement3_VarTableEntry : IDeduceElement3 {
    private class STOP : Exception()

    private val _principal: VarTableEntry

    private val _deduceTypes2: DeduceTypes2?

    @JvmField
    var __passthru: RegisterClassInvocation_env? = null

    @Contract(pure = true)
    constructor(aVarTableEntry: VarTableEntry) {
        _principal = aVarTableEntry
        _deduceTypes2 = null
    }

    @Contract(pure = true)
    constructor(aVarTableEntry: VarTableEntry,
                aDeduceTypes2: DeduceTypes2) {
        _principal = aVarTableEntry
        _deduceTypes2 = aDeduceTypes2
    }

    @Throws(STOP::class)
    private fun __one_potential(aDeducePhase: DeducePhase,
                                varTableEntry: VarTableEntry, potentialTypes: List<TypeTableEntry>,
                                typeName: TypeName, ci: ClassInvocation) {
        var sc = false

        val potentialType = potentialTypes[0]
        if (!potentialType.isResolved) {
            assert(potentialType.attached != null)
            val attachedType = potentialType.attached!!.type
            // assert attachedType == OS_Type.Type.USER_CLASS;
            if (attachedType != OS_Type.Type.USER_CLASS) {
                val att = potentialType.attached
                noteNonsense(105, att.toString())
            }

            run {
                //
                // HACK
                //
                if (attachedType != OS_Type.Type.USER_CLASS) {
                    val tn = potentialType.attached!!.typeName
                    if (ci.genericPart().hasGenericPart()) {
                        val v = ci.genericPart().valueForKey(tn)

                        if (v != null) {
                            potentialType.attached = v
                            assert(attachedType == OS_Type.Type.USER_CLASS // FIXME logical fallacy
                            )
                        }
                    }
                }
            }

            val dt2 = deduceTypes2()

            // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4((__passthru));
            if (attachedType == OS_Type.Type.USER_CLASS) {
                var xci = dt2._inj().new_ClassInvocation(potentialType.attached!!.classOf, null,
                        ReadySupplier_1(dt2))
                val v = xci.genericPart().valueForKey(typeName)

                if (v != null) {
                    xci.genericPart().record(typeName, varTableEntry)
                }

                xci = aDeducePhase.registerClassInvocation(xci)
                val gf = aDeducePhase.generatePhase
                        .getGenerateFunctions(xci.klass.context.module())
                val wgc = dt2._inj().new_WlGenerateClass(gf, xci, aDeducePhase.generatedClasses,
                        aDeducePhase.codeRegistrar)

                wgc.setConsumer { aResolved: EvaClass? -> potentialType.resolve(aResolved) }

                wgc.run(null) // !
                potentialType.genType.ci = xci // just for completeness
                sc = true
            } else if (attachedType == OS_Type.Type.BUILT_IN) {
                // README be explicit about doing nothing
            } else {
                NotImplementedException.raise()
                noteNonsense(177, "not a USER_CLASS " + potentialType.attached)
            }
        }
        if (potentialType.isResolved) varTableEntry.resolve(potentialType.resolved())
        else noteNonsense(114, "Can't resolve $varTableEntry")

        if (!sc) throw STOP()
    }

    @Contract("_, null -> fail")
    private fun __zero_potential(varTableEntry: VarTableEntry, tn: TypeName?) {
        Preconditions.checkNotNull(tn)
        assert(tn is NormalTypeName)
        if (tn != null) {
            if (tn is NormalTypeName) {
                if (tn.isNull()) return

                __zero_potential__1(varTableEntry, tn)
            } else assert(false)
        } else {
            // must be unknown
            assert(false)
        }
    }

    private fun __zero_potential__1(
            varTableEntry: VarTableEntry,
            aNormalTypeName: NormalTypeName) {
        // 0. preflight
        if (aNormalTypeName.isNull) throw NotImplementedException()

        // 1. st...
        val typeNameName = aNormalTypeName.name

        // 2. stage 1
        val lrl = aNormalTypeName.context.lookup(typeNameName)
        var best = lrl.chooseBest(null)

        // 3. validation
        if (best != null) {
            // A)

            // 4. handle special case here

            while (best is AliasStatementImpl) {
                NotImplementedException.raise()
                // assert false;
                best = DeduceLookupUtils._resolveAlias((best as AliasStatementImpl?)!!, deduceTypes2())
            }

            assert(best is ClassStatement)
            varTableEntry.resolve_varType((best as ClassStatement?)!!.oS_Type)
        } else {
            // B)

            // 4. do later...

            // TODO shouldn't this already be calculated?

            throw NotImplementedException()
        }
    }

    override fun deduceTypes2(): DeduceTypes2 {
        return _deduceTypes2!!
    }

    override fun elementDiscriminator(): DED {
        return DED.dispatch(_principal)
    }

    override fun generatedFunction(): BaseEvaFunction {
        throw NotImplementedException()
        // return null;
    }

    override fun genType(): GenType {
        throw NotImplementedException()
        // return null;
    }

    override fun getPrincipal(): OS_Element {
        return _principal.vs
    }

    override fun kind(): DeduceElement3_Kind {
        return DeduceElement3_Kind.GEN_FN__GC_VTE
    }

    override fun resolve(aContext: Context, dt2: DeduceTypes2) {
        throw NotImplementedException()
    }

    override fun resolve(aIdentIA: IdentIA, aContext: Context, aFoundElement: FoundElement) {
        throw NotImplementedException()
    }

    fun resolve_var_table_entries(aDeducePhase: DeducePhase, ci: ClassInvocation) {
        val varTableEntry = _principal

        val potentialTypes = varTableEntry.potentialTypes
        val typeName = varTableEntry.typeName

        try {
            if (potentialTypes.size == 0 && (varTableEntry.varType == null || typeName.isNull)) {
                __zero_potential(varTableEntry, typeName)
            } else {
                noteNonsenseErr(108, String.format("%s %s", varTableEntry.nameToken, potentialTypes))

                if (potentialTypes.size == 1) {
                    __one_potential(aDeducePhase, varTableEntry, potentialTypes, typeName, ci)
                }
            }
        } catch (stop: STOP) {
            NotImplementedException.raise()
        }
    }

    companion object {
        private fun noteNonsense(code: Int, message: String) {
            SimplePrintLoggerToRemoveSoon.println_out_2(String.format("%d %s%n", code, message))
        }

        private fun noteNonsenseErr(code: Int, message: String) {
            SimplePrintLoggerToRemoveSoon.println_err2(String.format("** [noteNonsenseErr] %d %s%n", code, message))
        }
    }
}
