package tripleo.elijah.nextgen.outputtree

interface EOT_Nameable {
    val nameableString: String
}
