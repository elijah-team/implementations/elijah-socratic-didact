package tripleo.vendor.batoull22

import tripleo.elijah.util.Operation
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * @author batoul
 * @author tripleo
 */
class EK_ExpertSystem {
    internal val listrule: List<EK_Production> = getListrule()

    var goal: EK_Fact? = null
        private set

    private val Listfacts: MutableList<EK_Fact> = ArrayList()
    private val Listrule : MutableList<EK_Production> = ArrayList()

    fun actualizeMerge(current_production: EK_Production, aEKMerge: EK_Merge) {
        Listfacts.add(aEKMerge.result()!!)
        logProgress(-1, "add fact:" + aEKMerge.result())
        Listrule.remove(current_production)
        logProgress(-1, "remove rule: $current_production")
    }

    fun actualizePush(aPush: EK_Push, current_production: EK_Production) {
        Listfacts.add(aPush.resultant()!!)
        logProgress(-1, "add fact: " + aPush.resultant())
        Listrule.remove(current_production)
        logProgress(-1, "remove rule: $current_production")
    }

    fun Backwardchaining(): Boolean {
        checkBackwardchaining(goal)
        return Listfacts.contains(goal)
    }

    fun canAcceptMerge(aMerge2: EK_Merge): Boolean {
        return (Listfacts.contains(aMerge2.first()) && Listfacts.contains(aMerge2.second())
                && !Listfacts.contains(aMerge2.result()))
    }

    fun canAcceptPush(aPush: EK_Push): Boolean {
        return Listfacts.contains(aPush.predicating()) && !Listfacts.contains(aPush.resultant())
    }

    fun checkBackwardchaining(g: EK_Fact?) {
        for (i in Listrule.indices) {
            val ep = Listrule[i]

            if (ep.isMerge()) {
                val m = ep.merge

                if (m.result() === g) { // example A.B-->C
                    logProgress(-1, "First case income : " + m.result())

                    // His right side fact
                    val hasFirst = Listfacts.contains(m.first())
                    val hasSecond = Listfacts.contains(m.second())

                    if (hasFirst && hasSecond) {
                        Listfacts.add(g!!)
                        Listrule.remove(ep)
                    } else if (!hasFirst && hasSecond) {
                        logProgress(-1, "1 not fact 2 fact")
                        checkBackwardchaining(m.first())
                    } else if (hasFirst && !hasSecond) {
                        logProgress(-1, "1 fact 2 not fact")
                        checkBackwardchaining(m.second())
                    } else if (!hasFirst && !hasSecond) {
                        logProgress(-1, "both not fact")
                        checkBackwardchaining(m.first())
                        checkBackwardchaining(m.second())
                    }
                    updateBackwardChaining(ep) // result.st()
                }
            } // end if (ch[4] == g)
            else if (ep.isPush()) {
                val push = ep.push

                val resultant = push.resultant()
                val predicating = push.predicating()

                if (resultant === g) { // example A-->C

                    logProgress(-1, "second case income :$resultant")

                    if (Listfacts.contains(predicating)) {
                        Listfacts.add(resultant!!)
                        Listrule.remove(ep)
                    } else {
                        checkBackwardchaining(predicating)
                        updateBackwardChaining(ep)
                    }
                }
            }
        } // end for
    } // end chekBackwardchaining

    fun Forwardchaining(): Boolean {
        Forwardchaining(goal)
        return Listfacts.contains(goal)
    }

    fun Forwardchaining(goal: EK_Fact?) {
        for (i in Listrule.indices) {
            val current_production = Listrule[i]

            if (current_production.isMerge()) {
                val merge = current_production.merge

                if (canAcceptMerge(merge)) {
                    actualizeMerge(current_production, merge)

                    if (Listfacts.contains(goal)) break
                    else Forwardchaining(goal)
                }
            } else if (current_production.isPush()) {
                val push = current_production.push

                // test
                if (canAcceptPush(push)) {
                    // actualize
                    actualizePush(push, current_production)

                    // recurse
                    if (Listfacts.contains(goal)) break
                    else Forwardchaining(goal)
                }
            }
        }
    }

    fun print() {
        logProgress(-1, "factlist:$Listfacts")
        logProgress(-1, "rulelist:$Listrule")
        logProgress(-1, "goal:$goal")
        logProgress(-1, " ")
        // logProgress(-1,  c);
        // logProgress(-1,  j);
    }

    // Interpretation of input
    fun proof(st: String) {
        if (st.length == 1) {
            Listfacts.add(EK_Fact(st[0]))
        } else {
            val proof_str = "proof:"
            if (st.startsWith(proof_str)) {
                val u = st.substring(proof_str.length)
                goal = EK_Fact(u[0])
            } else {
                Listrule.add(EK_Production(st))
            }
        }
    }

    fun updateBackwardChaining(prod: EK_Production) {
        val m = prod.merge

        if (Listfacts.contains(m.first()) && Listfacts.contains(m.second())) {
            if (!Listfacts.contains(m.result())) { // check it does not exist in fact
                Listfacts.add(m.result()!!)
                Listrule.remove(prod)
            }
        }
    }

    fun getListfacts(): List<EK_Fact> {
        return Listfacts
    }

    fun getListrule(): List<EK_Production> {
        return Listrule
    }

    private fun logProgress(code: Int, message: String) {
        SimplePrintLoggerToRemoveSoon.println_out_4("$code $message")
    }

    companion object {
        @JvmStatic
        fun openfile_2(ekExpertsystem: EK_ExpertSystem): Operation<EK_Reader> {
            try {
                val stream = ekExpertsystem.javaClass.getResourceAsStream("KB3.txt")
                return Operation.success(EK_Reader1(ekExpertsystem, stream))
            } catch (ex: Exception) {
                ekExpertsystem.logProgress(-1, "Error:the input file dose not exist")
                return Operation.failure(ex)
            }
        }
    }
}
