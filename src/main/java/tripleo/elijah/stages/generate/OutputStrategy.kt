/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.generate

/**
 * Created 1/8/21 10:31 PM
 */
class OutputStrategy {
    enum class By {
        BY_EZ, BY_PACKAGE
    }

    enum class Name {
        NAME_CLASS_NAME, NAME_Z_TYPE
    }

    enum class Per {
        PER_CLASS, PER_MODULE, PER_PACKAGE, PER_PROGRAM
    }

    protected var _by: By = By.BY_EZ

    protected var _per: Per = Per.PER_MODULE

    protected var _name: Name = Name.NAME_Z_TYPE

    fun by(): By {
        return _by
    }

    fun by(aBy: By) {
        _by = aBy
    }

    fun name(): Name {
        return _name
    }

    fun name(aName: Name) {
        _name = aName
    }

    fun per(): Per {
        return _per
    }

    fun per(aPer: Per) {
        _per = aPer
    }
} //
//
//

