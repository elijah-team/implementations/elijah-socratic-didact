package tripleo.elijah.stages.write_stage.pipeline_impl

import com.google.common.base.Preconditions
import org.jdeferred2.DoneCallback
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.notation.GM_GenerateModule
import tripleo.elijah.comp.notation.GM_GenerateModuleRequest
import tripleo.elijah.comp.notation.GN_GenerateNodesIntoSink
import tripleo.elijah.comp.notation.GN_GenerateNodesIntoSinkEnv
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.output.NG_OutputFunction
import tripleo.elijah.stages.gen_c.C2C_Result
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaConstructor
import tripleo.elijah.stages.gen_fn.EvaFunction
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.gen_generic.pipeline_impl.DefaultGenerateResultSink
import tripleo.elijah.stages.gen_generic.pipeline_impl.ProcessedNode
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.logging.ElLog_
import tripleo.elijah.stages.write_stage.pipeline_impl.WPIS_GenerateOutputs.OutputItems
import tripleo.elijah.util.Helpers
import tripleo.elijah.work.WorkList__
import tripleo.elijah.work.WorkManager__

internal class AmazingFunction(private val f: BaseEvaFunction,
                               private val itms: OutputItems,
                               private val result: GenerateResult,
                               private val pa: IPipelineAccess) : Amazing {
    // created
    private val of = NG_OutputFunction()

    // given
    private val mod: OS_Module = f.module()

    fun mod(): OS_Module {
        return mod
    }

    fun waitGenC(ggc: GenerateC) {
        // TODO latch
        pa.accessBus.subscribePipelineLogic { aPipelineLogic: PipelineLogic? ->
            // FIXME check arguments --> this doesn't seem like it will give the desired
            // results
            val generateResultSink = DefaultGenerateResultSink(pa)

            val gr = result // new Old_GenerateResult();
            val ce = pa.compilationEnclosure

            val env = GN_GenerateNodesIntoSinkEnv(Helpers.List_of<ProcessedNode>(),
                    generateResultSink,
                    ElLog.Verbosity.VERBOSE,
                    gr,
                    ce)

            val world = ce.compilation.world()
            val wm = world.findModule(mod)

            val generateModuleRequest = GM_GenerateModuleRequest(GN_GenerateNodesIntoSink(env), wm!!, env)
            val generateModule = GM_GenerateModule(generateModuleRequest)

            val fileGen = GenerateResultEnv(MyGenerateResultSink(of),
                    result,
                    WorkManager__(),
                    WorkList__(),
                    generateModule)

            //			var generateModuleResult = generateModule.getModuleResult(fileGen.wm(), fileGen.resultSink());
            ProgressiveGenerateFiles.of(this)
                    .then { ggc1: GenerateC ->
                        if (f is EvaFunction) {
                            ggc1.generateCodeForMethod(fileGen, f)
                        } else if (f is EvaConstructor) {
                            ggc1.generateCodeForConstructor_1(f, fileGen)
                        }
                    }
                    .with(ggc) // NOTE 11/26 too involved to back out. this is cheating, tho.
            itms.addItem(of)
        }
    }

    internal interface ProgressiveGenerateFiles_Amazing_ {
        fun with(aGgc: GenerateC)
    }

    internal class ProgressiveGenerateFiles_AmazingFunction(private val amazingFunction: AmazingFunction) : ProgressiveGenerateFiles, ProgressiveGenerateFiles_Amazing_ {
        private var cb: DoneCallback<GenerateC>? = null

        override fun then(aGenerateC: DoneCallback<GenerateC>?): ProgressiveGenerateFiles_Amazing_ {
            this.cb = aGenerateC
            return this
        }

        override fun with(aGenerateC: GenerateC) {
            Preconditions.checkNotNull(cb)
            cb!!.onDone(aGenerateC)
        }
    }

    internal interface ProgressiveGenerateFiles {
        fun then(aGenerateC: DoneCallback<GenerateC>?): ProgressiveGenerateFiles_Amazing_

        companion object {
            fun of(amazingFunction: AmazingFunction): ProgressiveGenerateFiles {
                return ProgressiveGenerateFiles_AmazingFunction(amazingFunction)
            }
        }
    }

    private class MyGenerateResultSink(private val of: NG_OutputFunction) : DeadGenerateResultSink() {
        override fun addFunction(aGf: BaseEvaFunction,
                                 aRs: List<C2C_Result>,
                                 aGenerateFiles: GenerateFiles) {
            of.setFunction(aGf, aGenerateFiles, aRs)
        }
    }
}
