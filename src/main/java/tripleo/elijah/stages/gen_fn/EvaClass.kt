/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import tripleo.elijah.g.GEvaClass
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.*
import tripleo.elijah.lang.types.OS_BuiltinType
import tripleo.elijah.lang.types.OS_GenericTypeNameType
import tripleo.elijah.lang.types.OS_UnknownType
import tripleo.elijah.lang.types.OS_UserClassType
import tripleo.elijah.nextgen.reactive.DefaultReactive
import tripleo.elijah.nextgen.reactive.Reactive
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.garish.GarishClass_Generator
import tripleo.elijah.stages.gen_fn.EvaContainer.VarTableEntry.UpdatePotentialTypesCB
import tripleo.elijah.stages.gen_generic.CodeGenerator
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.util.*
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah.world.i.LivingClass
import java.util.function.Consumer

/**
 * Created 10/29/20 4:26 AM
 */
class EvaClass(
    @JvmField val klass: ClassStatement,
    private val module: OS_Module,
    override var code: Int = -75000,
) : EvaContainerNC(), GEvaClass {

//    override fun setCode(aCode: Int) {
//        world()!!.code = aCode
//    }

    inner class _Reactive_EvaClass : DefaultReactive() {
        override fun <T> addListener(t: Consumer<T>?) {
            throw UnintendedUseException("missed out on it?")
        }
    }

    private var _living: LivingClass? = null
    @JvmField
    var ci: ClassInvocation? = null
    @JvmField
    var constructors: MutableMap<ConstructorDef, IEvaConstructor> = HashMap()

    private var resolve_var_table_entries_already = false

    val reactiveEvaClass: _Reactive_EvaClass = _Reactive_EvaClass()

    // TODO Reactive??
    private val _gcg = GarishClass_Generator(this)

    fun addAccessNotation(an: AccessNotation?) {
        throw NotImplementedException()
    }

    fun addConstructor(aConstructorDef: ConstructorDef, aGeneratedFunction: IEvaConstructor) {
        constructors[aConstructorDef] = aGeneratedFunction
    }

    fun createCtor0() {
        // TODO implement me
        val fd: FunctionDef = FunctionDefImpl(klass, klass.context)
        fd.setName(Helpers0.string_to_ident("<ctor$0>"))
        val scope3: Scope3 = Scope3Impl(fd)
        fd.scope(scope3)
        for (varTableEntry in varTable) {
            if (varTableEntry.initialValue !== LangGlobals.UNASSIGNED) {
                val left: IExpression = varTableEntry.nameToken
                val right = varTableEntry.initialValue

                val e: IExpression = ExpressionBuilder.build(left, ExpressionKind.ASSIGNMENT, right)
                scope3.add(StatementWrapperImpl(e, fd.context, fd))
            } else {
                if (getPragma("auto_construct")) {
                    scope3.add(ConstructStatementImpl(fd, fd.context, varTableEntry.nameToken, null, null))
                }
            }
        }
    }

    fun fixupUserClasses(aDeduceTypes2: DeduceTypes2, aContext: Context?) {
        for (varTableEntry in varTable) {
            varTableEntry.updatePotentialTypesCB = object : UpdatePotentialTypesCB {
                override fun call(aEvaContainer: EvaContainer): Operation<Boolean> {
                    val potentialTypes00 = potentialTypes

                    assert(potentialTypes00.mode() == Mode.SUCCESS)
                    var potentialTypes = potentialTypes.success() as MutableList<GenType>

                    //

                    //
                    // HACK TIME
                    //
                    if (potentialTypes.size == 2) {
                        val resolvedClass1 = potentialTypes[0].resolved.classOf
                        val resolvedClass2 = potentialTypes[1].resolved.classOf
                        val prelude: OS_Module
                        if (potentialTypes[1].resolved is OS_BuiltinType
                                && potentialTypes[0].resolved is OS_UserClassType) {
                            val resolved = potentialTypes[1].resolved as OS_BuiltinType

                            try {
                                val rt = ResolveType.resolve_type(resolvedClass1.context.module(),
                                        resolved, resolvedClass1.context, aDeduceTypes2._LOG(), aDeduceTypes2)
                                val y = 2

                                potentialTypes = Helpers.List_of(rt)
                            } catch (aE: ResolveError) {
                                return Operation.failure(aE)
                            }
                        } else if (potentialTypes[0].resolved is OS_BuiltinType
                                && potentialTypes[1].resolved is OS_UserClassType) {
                            val resolved = potentialTypes[0].resolved as OS_BuiltinType

                            try {
                                val rt = aDeduceTypes2.resolve_type(resolved, resolvedClass2.context)
                                val y = 2

                                potentialTypes = Helpers.List_of(rt)
                            } catch (aE: ResolveError) {
                                return Operation.failure(aE)
                            }
                        } else {
                            prelude = resolvedClass1.context.module().prelude()

                            // TODO might not work when we split up prelude
                            // Thats why I was testing for package name before
                            if (resolvedClass1.context.module() === prelude
                                    && resolvedClass2.context.module() === prelude) {
                                // Favor String over ConstString
                                if (resolvedClass1.name() == "ConstString" && resolvedClass2.name() == "String") {
                                    potentialTypes.removeAt(0)
                                } else if (resolvedClass2.name() == "ConstString" && resolvedClass1.name() == "String") {
                                    potentialTypes.removeAt(1)
                                }
                            }
                        }
                    }

                    if (potentialTypes.size == 1) {
                        val genericPart = ci!!.genericPart()
                        if (genericPart != null) {
                            if (genericPart.hasGenericPart()) {
                                val t = varTableEntry.varType
                                if (t.type == OS_Type.Type.USER) {
                                    try {
                                        val genType = aDeduceTypes2.resolve_type(t,
                                                t.typeName.context)
                                        if (genType.resolved is OS_GenericTypeNameType) {
                                            val xxci = (aEvaContainer as EvaClass).ci

                                            val v = xxci!!.genericPart().valueForKey(t.typeName)
                                            if (v != null) {
                                                varTableEntry.varType = v
                                            }
                                        }
                                    } catch (aResolveError: ResolveError) {
                                        aResolveError.printStackTrace()
                                        // assert false;
                                        return Operation.failure(aResolveError)
                                    }
                                }
                            }
                        } else {
                            logProgress(99980, "************************** no generic")
                        }
                    }
                    return Operation.success(true)
                }

                val potentialTypes: Operation<List<GenType>>
                    get() {
                        val potentialTypes: MutableList<GenType> = ArrayList()
                        for (potentialType in varTableEntry.potentialTypes) {
                            val y = 2
                            val genType: GenType
                            try {
                                if (potentialType.genType.typeName == null) {
                                    val attached = potentialType.attached ?: continue

                                    genType = aDeduceTypes2.resolve_type(attached, aContext)
                                    if (genType.resolved == null
                                            && genType.typeName.type == OS_Type.Type.USER_CLASS) {
                                        genType.resolved = genType.typeName
                                        genType.typeName = null
                                    }
                                } else {
                                    if (potentialType.genType.resolved == null
                                            && potentialType.genType.resolvedn == null) {
                                        val attached = potentialType.genType.typeName

                                        genType = aDeduceTypes2.resolve_type(attached, aContext)
                                    } else genType = potentialType.genType
                                }
                                if (genType.typeName != null) {
                                    val typeName = genType.typeName.typeName
                                    if (typeName is NormalTypeName) {
                                        val genericPart = typeName.genericPart
                                        if (genericPart != null && genericPart.size() > 0) {
                                            genType.setNonGenericTypeName(typeName)
                                        }
                                    }
                                }
                                genType.genCIForGenType2(aDeduceTypes2)
                                potentialTypes.add(genType)
                            } catch (aResolveError: ResolveError) {
                                aResolveError.printStackTrace()
                                return Operation.failure(aResolveError)
                            }
                        }
                        //
                        val set: Set<GenType> = HashSet(potentialTypes)
                        //					final Set<GenType> s = Collections.unmodifiableSet(set);
                        return Operation.success(ArrayList(set))
                    }
            }
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            /* ======================================= */
            if (!varTableEntry._p_updatePotentialTypesCBPromise.isResolved) {
                varTableEntry._p_updatePotentialTypesCBPromise.resolve(varTableEntry.updatePotentialTypesCB)
            }
        }
    }

    // does the same thing
//    override var code: Int = -1
//        get() = world()!!.code

    override fun generateCode(aFileGen: GenerateResultEnv?, aCodeGenerator: CodeGenerator) =
        aCodeGenerator.generate_class(aFileGen, this)

    fun generator(): GarishClass_Generator = _gcg

    override fun getElement(): OS_Element = klass

    var living: LivingClass?
        get() = world()
        set(_living) {
            this._living = _living
        }

    val name: String
        get() {
            val sb = StringBuilder()
            sb.append(klass.name)
            val ciGenericPart = ci!!.genericPart()
            if (ciGenericPart != null) {
                if (ciGenericPart.hasGenericPart()) {
                    val map = ci!!.genericPart().map!!

                    if (!map.isEmpty()) {
                        sb.append("[")
                        val joined = getNameHelper(map)
                        sb.append(joined)
                        sb.append("]")
                    }
                }
            }
            return sb.toString()
        }

    private fun getNameHelper(aGenericPart: Map<TypeName, OS_Type>): String {
        val ls: MutableList<String> = ArrayList()
        for ((_, value) in aGenericPart) {
            // TODO Is this guaranteed to be in order?
            val name = if (value is OS_UnknownType) {
                "?"
            } else {
                value.classOf.name
            }
            ls.add(name) // TODO Could be nested generics
        }
        return Helpers.String_join(", ", ls)
    }

    val numberedName: String
        get() = klass.name + "_" + code

    private fun getPragma(auto_construct: String): Boolean { // TODO this should be part of ContextImpl
        return false
    }

    override fun getGn():GNCoded {
        val _c = this
        return object : GNCoded {
            override fun getCode1234567(): Int = _c.code

            override fun getRole(): GNCoded.Role = GNCoded.Role.CLASS

            override fun register(aCr: ICodeRegistrar) = aCr.registerClass1(_c)

            override fun setCode(aCode: Int) {
                TODO("Not yet implemented")
                //world()!!.code = aCode
            }
        }
    }

    override fun identityString(): String {
        return klass.toString()
    }

    val isGeneric: Boolean
        get() = klass.genericPart.size > 0

    override fun module(): OS_Module {
        return module
    }

    fun reactive(): Reactive {
        return reactiveEvaClass
    }

    fun resolve_var_table_entries(aDeducePhase: DeducePhase): Boolean {
        val Result = false

        if (resolve_var_table_entries_already) return true

        for (varTableEntry in varTable) {
            varTableEntry.deduceElement3.resolve_var_table_entries(aDeducePhase, ci!!)
        }

        resolve_var_table_entries_already = true // TODO is this right?
        return Result
    }

    override fun toString(): String {
        return ("EvaClass{" + "klass=" + klass + ", code=" + code + ", module=" + module.fileName + ", ci="
                + ci!!.finalizedGenericPrintable() + '}')
    }

    fun world(): LivingClass? = _living

    companion object {
        private fun logProgress(code: Int, message: String) {
            SimplePrintLoggerToRemoveSoon.println_err_4("$code $message")
        }
    }
}
