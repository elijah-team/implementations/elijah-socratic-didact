package tripleo.elijah.typinf

import tripleo.elijah.util.Helpers

/**
 * lambda [args] -> expr
 */
class LambdaExpr_AST(val argnames: List<String?>, val expr: AstNode) : AstNode() {
    /**
     * Used by the type inference algorithm to map discovered types for the
     * arguments of the lambda. Since we list arguments as names (strings) and
     * not ASTNodes, we can't keep their _type on the node.
     */
    var _arg_types: HashMap<String?, Type?>? = null

    init {
        this._children = Helpers.List_of(expr)
    }

    override fun toString(): String {
        return String.format("Lambda([%s], %s)", Helpers.String_join(", ", argnames), expr)
    }
}
