package tripleo.elijah.nextgen.output

import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.stages.garish.GarishClass
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.stages.generate.OutputStrategyC
import tripleo.elijah.util.Helpers

class NG_OutputClass : NG_OutputItem {
    private var garishClass: GarishClass? = null
    private var generateFiles: GenerateFiles? = null

    override val outputs: List<NG_OutputStatement>
        get() {
            val x = garishClass!!.living.evaNode() as EvaClass

            val generateC = generateFiles as GenerateC?

            val tos = garishClass!!.getImplBuffer(generateC!!)
            val implText = NG_OutputClassStatement(tos, x.module(), TY.IMPL)

            val tosHdr = garishClass!!.getHeaderBuffer(generateC)
            val headerText = NG_OutputClassStatement(tosHdr, x.module(), TY.HEADER)

            return Helpers.List_of<NG_OutputStatement>(implText, headerText)
        }

    override fun outName(aOutputStrategyC: OutputStrategyC,
                         ty: TY): EOT_FileNameProvider {
        val x = garishClass!!.living.evaNode() as EvaClass

        return aOutputStrategyC.nameForClass1(x, ty)
    }

    fun setClass(aGarishClass: GarishClass?, aGenerateFiles: GenerateFiles?) {
        garishClass = aGarishClass
        generateFiles = aGenerateFiles
    }
}
