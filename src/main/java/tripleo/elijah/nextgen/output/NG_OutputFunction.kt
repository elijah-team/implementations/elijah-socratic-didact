package tripleo.elijah.nextgen.output

import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.stages.gen_c.C2C_Result
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaFunction
import tripleo.elijah.stages.gen_fn.IEvaConstructor
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.stages.generate.OutputStrategyC

class NG_OutputFunction : NG_OutputItem {
    private var collect: List<C2C_Result>? = null
    private var generateFiles: GenerateFiles? = null
    private var gf: BaseEvaFunction? = null

    override val outputs: List<NG_OutputStatement>
        get() {
            val r: MutableList<NG_OutputStatement> = ArrayList()

            if (collect != null) {
                for (c2c in collect!!) {
                    val x = c2c.statement
                    val y = c2c.ty()

                    r.add(NG_OutputFunctionStatement(c2c))
                }
            }

            return r
        }

    override fun outName(aOutputStrategyC: OutputStrategyC,
                         ty: TY): EOT_FileNameProvider {
        return if (gf is EvaFunction) {
            aOutputStrategyC.nameForFunction1((gf as EvaFunction?)!!, ty)
        } else {
            aOutputStrategyC.nameForConstructor1((gf as IEvaConstructor?)!!, ty)
        }
    }

    fun setFunction(aGf: BaseEvaFunction?, aGenerateFiles: GenerateFiles?,
                    aCollect: List<C2C_Result>?) {
        gf = aGf
        generateFiles = aGenerateFiles
        collect = aCollect
    }
}
