package tripleo.elijah.stages.gen_c

import tripleo.elijah.nextgen.reactive.Reactivable
import tripleo.elijah.nextgen.reactive.ReactiveDimension
import tripleo.elijah.stages.gen_c.GenerateC.GenerateResultProgressive
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaConstructor
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.util.buffer.Buffer

class GCFC(rs: List<C2C_Result?>, private val gf: BaseEvaFunction, private val gr: GenerateResult) : Reactivable {
    private var buf: Buffer? = null
    private var bufHdr: Buffer? = null

    init {
        for (r in rs) {
            // TODO store a Map<TY, Buffer/*GRI??*/> in rs
            when (r!!.ty()) {
                TY.HEADER -> buf = r.buffer
                TY.IMPL -> bufHdr = r.buffer
                else -> throw IllegalStateException()
            }
        }
    }

    override fun respondTo(aDimension: ReactiveDimension?) {
        if (aDimension is GenerateC) {
            val lsp = gf.module().lsp

            gr.addConstructor(gf as EvaConstructor, buf, TY.IMPL, lsp)
            gr.addConstructor(gf, bufHdr, TY.HEADER, lsp)

            val gr2: GenerateResultProgressive = aDimension.generateResultProgressive()
            gr2.addConstructor(gf, buf, TY.IMPL)
            gr2.addConstructor(gf, bufHdr, TY.HEADER)
        }
    }
}
