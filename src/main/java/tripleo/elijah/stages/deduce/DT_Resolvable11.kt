package tripleo.elijah.stages.deduce

import org.jdeferred2.DoneCallback
import org.jdeferred2.impl.DeferredObject

class DT_Resolvable11<T> {
    private val _p_res = DeferredObject<T, ResolveError, Void>()

    fun reject(aResolveError: ResolveError) {
        _p_res.reject(aResolveError)
    }

    fun resolve(t: T) {
        _p_res.resolve(t)
    }

    fun then(then: DoneCallback<T>?) {
        _p_res.then(then)
    }
}
