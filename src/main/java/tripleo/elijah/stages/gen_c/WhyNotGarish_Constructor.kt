package tripleo.elijah.stages.gen_c

import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.ConstructorDef
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.stages.garish.GarishConstructor__addFunction
import tripleo.elijah.stages.gen_c.GenerateC.WlGenerateFunctionC
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.gen_generic.Old_GenerateResult
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.logging.ElLog.Verbosity
import tripleo.elijah.stages.logging.ElLog_
import tripleo.elijah.util.*
import tripleo.elijah.util2.DebugFlags
import tripleo.elijah.util2.Eventual
import tripleo.elijah.work.WorkList

class WhyNotGarish_Constructor(override val gf: EvaConstructor, private val generateC: GenerateC) : WhyNotGarish_BaseFunction(), WhyNotGarish_Item {
    private val __declaringContext: WhyNotGarish_DeclaringContext
    private val fileGenPromise = Eventual<GenerateResultEnv>()
    private var __deduced: DefaultDeducedEvaConstructor? = null
    var results: List<C2C_Result?>? = null
        private set

    init {
        fileGenPromise.then { aFileGen: GenerateResultEnv -> this.onFileGen(aFileGen) }
        __declaringContext = object : WhyNotGarish_DeclaringContext {
            override fun isClassStatement(): Boolean {
                return gf.fd.parent is ClassStatement
            }

            override fun pointsToConstructor(): Boolean {
                return gf.fd is ConstructorDef
            }

            override fun pointsToConstructor2(): Boolean {
                return gf is EvaConstructor
            }

            override fun isDefaultConstructor(): Boolean {
                val constructorName_ = cd()!!.name()
                if (constructorName_ == "<>") return true
                return false
            }
        }
    }

    fun onFileGen(aFileGen: GenerateResultEnv) {
        val gcfm = Generate_Code_For_Method(generateC, generateC.elLog())

        val yf = generateC.a_lookup(gf)
        assert(yf === this)
        // TODO separate into method and method_header??
        val cfm = C2C_CodeForConstructor(gcfm, aFileGen, yf!!)

        // cfm.calculate();
        val rs = cfm.results
        val gr: GenerateResult = Old_GenerateResult()
        val gcfc = GCFC(rs, gf, gr) // TODO 08/12 preload this??

        results = rs

        gf.reactive().add(gcfc)

        if (!DebugFlags.MANUAL_DISABLED) {
            gcfc.respondTo(generateC)
        }

        // FIXME 06/17; 10/13 what's wrong with it?
        val sink = aFileGen.resultSink()
        if (sink != null) {
            sink.add(GarishConstructor__addFunction(gf, rs, generateC))
        } else {
            logProgress(9992, "sink failed")
        }
    }

    private fun logProgress(code: Int, message: String) {
        generateC._ce().logProgress(CompProgress.GenerateC, Pair.of(code, message))
    }

    @Deprecated("")
    override fun cheat(): EvaConstructor {
        return gf
    }

//    override fun getGf(): BaseEvaFunction {
//        return gf
//    }

    override fun provideFileGen(fg: GenerateResultEnv) {
        fileGenPromise.resolve(fg)
    }

    override fun declaringContext(): WhyNotGarish_DeclaringContext {
        return __declaringContext
    }

    val constructorNameText: String
        get() {
            val constructorName = gf.fd.nameNode
            val constructorNameText = if (constructorName === LangGlobals.emptyConstructorName) {
                ""
            } else {
                constructorName.text
            }
            return constructorNameText
        }

    override fun hasFileGen(): Boolean {
        return fileGenPromise.isResolved
    }

    fun postGenerateCodeForConstructor(aWl: WorkList, aFileGen: GenerateResultEnv) {
        for (identTableEntry in gf.idte_list) {
            // IdentTableEntry.;

            identTableEntry.reactive().addResolveListener { x: IdentTableEntry ->
                generateIdent(x, aFileGen)
            }

            if (identTableEntry.isResolved) {
                generateIdent(identTableEntry, aFileGen)
            }
        }
        for (pte in gf.prte_list) {
//                      ClassInvocation ci = pte.getClassInvocation();
            val fi = pte.functionInvocation
            if (fi == null) {
                // TODO constructor
                val y = 2
            } else {
                val gf = fi.eva
                if (gf != null) {
                    aWl.addJob(WlGenerateFunctionC(aFileGen, gf, generateC))
                }
            }
        }
    }

    private fun generateIdent(identTableEntry: IdentTableEntry, aFileGen: GenerateResultEnv) {
        assert(identTableEntry.isResolved)
        val x = identTableEntry.resolvedType()
        val wl = aFileGen.wl()

        if (x is EvaClass) {
            generateC.generate_class(aFileGen, x)
        } else if (x is EvaFunction) {
            wl.addJob(WlGenerateFunctionC(aFileGen, x, generateC))
        } else {
            generateC.LOG.err(x.toString())
            throw NotImplementedException()
        }
    }

    fun resolveFileGenPromise(aFileGen: GenerateResultEnv) {
        fileGenPromise.resolve(aFileGen)
    }

    fun deduced(): DeducedEvaConstructor {
        if (__deduced == null) {
            val generateModule = generateC.fileGen__.gmgm()
            val deducePhase = generateModule.gmr().env().pa().compilationEnclosure.pipelineLogic.dp

            // TODO 10/16 cached: tho this may not matter
            val dt2 = deducePhase._inj().new_DeduceTypes2(gf.module(), deducePhase, Verbosity.VERBOSE)

            dt2.deduceOneConstructor(gf, deducePhase)

            __deduced = DefaultDeducedEvaConstructor(gf)
        }

        return __deduced!!
    }

    fun cd(): ConstructorDef? {
        return gf.cd
    }
}
