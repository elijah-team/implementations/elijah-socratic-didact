package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_fn.IdentTableEntry

internal class GI_ClassStatement(private val e: ClassStatement, private val giRepo: GI_Repo) : GenerateC_Item {
    private var _evaNaode: EvaNode? = null
    private var _ite: IdentTableEntry? = null

    override fun getEvaNode(): EvaNode {
        return _evaNaode!!
    }

    override fun setEvaNode(a_evaNode: EvaNode) {
        _evaNaode = a_evaNode
    }

    fun setITE(ite: IdentTableEntry) {
        var resolved: EvaNode? = null

        if (ite.type != null) resolved = ite.type!!.resolved()
        if (resolved == null) resolved = ite.resolvedType()
        if (resolved == null) {
            val de3_idte = ite.deduceElement3
            resolved = de3_idte.resolved
        }

        // assert resolved != null;
        _ite = ite
        _evaNaode = resolved
    }
}
