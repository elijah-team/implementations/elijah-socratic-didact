package tripleo.elijah.comp.impl

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.Finally_
import tripleo.elijah.comp.chewtoy.PW_CompilerController
import tripleo.elijah.comp.chewtoy.Startable
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.graph.i.CK_ObjectTree
import tripleo.elijah.comp.i.*
import tripleo.elijah.comp.i.extra.CompilerInputListener
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.inputs.CompilerInput.CompilerInputField
import tripleo.elijah.comp.inputs.CompilerInputMaster
import tripleo.elijah.comp.inputs.ILazyCompilerInstructions_
import tripleo.elijah.comp.internal.CompilationImpl
import tripleo.elijah.comp.local.CW.CX_ParseElijahFile.ElijahSpecReader
import tripleo.elijah.comp.local.CX_realParseElijjahFile2
import tripleo.elijah.comp.nextgen.i.CP_Path
import tripleo.elijah.comp.nextgen.inputtree.EIT_ModuleInput
import tripleo.elijah.comp.nextgen.pw.PW_PushWorkQueue
import tripleo.elijah.comp.nextgen.pw.PW_PushWorkQueue__JC
import tripleo.elijah.comp.specs.ElijahCache
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.lang.i.Qualident
import tripleo.elijah.lang.impl.QualidentImpl
import tripleo.elijah.nextgen.comp_model.CM_UleLog
import tripleo.elijah.nextgen.inputtree.EIT_InputTree
import tripleo.elijah.nextgen.inputtree.EIT_InputTreeImpl
import tripleo.elijah.nextgen.inputtree.EIT_InputType
import tripleo.elijah.nextgen.inputtree.EIT_ModuleInputImpl
import tripleo.elijah.nextgen.outputtree.EOT_OutputTree
import tripleo.elijah.nextgen.outputtree.EOT_OutputTreeImpl
import tripleo.elijah.util.Helpers0
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.world.i.LivingRepo
import tripleo.elijah.world.i.WorldModule
import tripleo.elijah.world.impl.DefaultLivingRepo
import tripleo.elijah.world.impl.DefaultWorldModule
import java.util.*

class DefaultCompFactory(private val compilation: CompilationImpl) : CompFactory {
    private var _log: CM_UleLog? = null

    @Contract(" -> new")
    override fun createCompilationAccess(): ICompilationAccess {
        return DefaultCompilationAccess(compilation)
    }

    @Contract(" -> new")
    override fun createCompilationBus(): ICompilationBus {
        return DefaultCompilationBus(Objects.requireNonNull(compilation.compilationEnclosure))
    }

    @Contract("_ -> new")
    override fun createModuleInput(aModule: OS_Module): EIT_ModuleInput {
        return EIT_ModuleInputImpl(aModule, compilation, type = EIT_InputType.ELIJAH_SOURCE)
    }

    @Contract("_ -> new")
    override fun createQualident(sl: List<String>): Qualident {
        val R: Qualident = QualidentImpl()
        // README 10/13 avoid inclination to
        for (s in sl) {
            R.append(Helpers0.string_to_ident(s))
        }
        return R
    }

    @Contract(" -> new")
    override fun createObjectTree(): CK_ObjectTree {
        return DefaultObjectTree(compilation)
    }

    @Contract("_ -> new")
    override fun defaultElijahSpecParser(elijahCache: ElijahCache): CY_ElijahSpecParser {
        return CY_ElijahSpecParser { spec ->
            val c: Compilation = compilation
            val om = CX_realParseElijjahFile2.realParseElijjahFile2(spec, elijahCache, c)
            om
        }
    }

    override fun createWorldModule(aModule: OS_Module): WorldModule {
        return DefaultWorldModule(aModule, compilation.compilationEnclosure)
    }

    override fun createWorkQueue(): PW_PushWorkQueue {
        return PW_PushWorkQueue__JC()
        //		return new PW_PushWorkQueue_Blocking();
        //return new PW_PushWorkQueue_Concurrent();
    }

    override fun askConcurrent(aRunnable: Runnable, aThreadName: String): Startable {
        val thread = Thread(aRunnable)
        thread.name = aThreadName
        return object : Startable {
            override fun start() {
                thread.start()
            }

            @Deprecated("")
            override fun stealThread(): Thread {
                return thread
            }
        }
    }

    override fun createCompilerInputMaster(): CompilerInputMaster {
        return object : CompilerInputMaster {
            private val listeners: MutableList<CompilerInputListener> = ArrayList()

            override fun addListener(compilerInputListener: CompilerInputListener) {
                listeners.add(compilerInputListener)
            }

            override fun notifyChange(compilerInput: CompilerInput, compilerInputField: CompilerInputField) {
                for (listener in listeners) {
                    listener.baseNotify(compilerInput, compilerInputField)
                }
            }
        }
    }

    override fun createOutputTree(): EOT_OutputTree {
        return EOT_OutputTreeImpl()
    }

    override fun createInputTree(): EIT_InputTree {
        return EIT_InputTreeImpl()
    }

    override fun defaultElijahSpecReader(aLocalPrelude: CP_Path): ElijahSpecReader {
        return DefaultElijahSpecReader(aLocalPrelude, compilation)
    }

    override fun createCkMonitor(): CK_Monitor {
        return DefaultCK_Monitor()
    }

    override fun createPwController(aCompilation: CompilationImpl): PW_CompilerController {
        return PW_CompilerController(aCompilation)
    }

    override fun createFinally(): Finally_ {
        return Finally_()
    }

    override fun getLivingRepo(): LivingRepo {
        return DefaultLivingRepo()
    }

    override fun getULog(): CM_UleLog {
        if (_log == null) {
            _log = CM_UleLog { string: String -> SimplePrintLoggerToRemoveSoon.println2(string) }
        }
        return _log!!
    }

    override fun createLazyCompilerInstructions(aCompilerInput: CompilerInput): ILazyCompilerInstructions {
        return ILazyCompilerInstructions_.of(aCompilerInput, compilation.compilationClosure)
    }
}

