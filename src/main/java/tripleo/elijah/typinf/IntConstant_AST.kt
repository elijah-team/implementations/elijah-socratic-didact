package tripleo.elijah.typinf

class IntConstant_AST : AstNode {
    private val value: Int

    internal constructor(aValue: Int) {
        value = aValue
    }

    constructor(aValue: String) {
        value = aValue.toInt()
    }

    override fun toString(): String {
        return "" + value
    }
}
