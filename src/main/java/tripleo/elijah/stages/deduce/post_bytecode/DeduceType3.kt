package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.gen_fn.*

internal class DeduceType3 : DED {
    private val deduceElement3: IDeduceElement3?
    private val diagnostic: Diagnostic?

    private var _genType: GenType? = null

    private val osType: OS_Type

    constructor(aDeduceElement3: IDeduceElement3?, aOSType: OS_Type, aDiagnostic1: Diagnostic?) {
        deduceElement3 = aDeduceElement3
        osType = aOSType
        diagnostic = aDiagnostic1
    }

    //	public static IDeduceElement3 dispatch(final @NotNull IdentTableEntry aIdentTableEntry) {
    //		return aIdentTableEntry.getDeduceElement3(null/*aDeduceTypes2*/, null/*aGeneratedFunction*/);
    //	}
    //	public static IDeduceElement3 dispatch(final @NotNull ConstantTableEntry aConstantTableEntry) {
    //		return aConstantTableEntry.getDeduceElement3();
    //	}
    constructor(aOSType: OS_Type, aDiagnostic: Diagnostic?) {
        deduceElement3 = null
        osType = aOSType
        diagnostic = aDiagnostic
    }

    val genType: GenType
        get() {
            if (_genType == null) {
                // _genType = _inj().new_GenTypeImpl();
                _genType = GenTypeImpl()
                _genType?.setResolved(osType)
            }

            return _genType!!
        }

    val isException: Boolean
        get() = diagnostic != null

    override fun kind(): DED.Kind {
        return DED.Kind.DED_Kind_Type
    }

    fun reportDiagnostic(aErrSink: ErrSink) {
        assert(isException)
        aErrSink.reportDiagnostic(diagnostic)
    } // private DeduceTypes2Injector _inj() {
    // return deduceTypes2()._inj();
    // }

    companion object {
        fun dispatch(aIdentTableEntry: IdentTableEntry,
                     aDeduceTypes2: DeduceTypes2?, aGeneratedFunction: BaseEvaFunction?): IDeduceElement3 {
            return aIdentTableEntry.getDeduceElement3(aDeduceTypes2, aGeneratedFunction)
        }

        @JvmStatic
        fun dispatch(aVariableTableEntry: VariableTableEntry): IDeduceElement3 {
            return aVariableTableEntry.deduceElement3
        }
    }
}
