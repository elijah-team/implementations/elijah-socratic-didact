/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import io.reactivex.rxjava3.subjects.ReplaySubject
import io.reactivex.rxjava3.subjects.Subject
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.gen_generic.Dependency

/**
 * Created 6/21/21 11:36 PM
 */
abstract class AbstractDependencyTracker : DependencyTracker {
    private val dependentFunctions: List<FunctionInvocation> = ArrayList()
    private val dependentTypes: List<GenType> = ArrayList()
    var dependentFunctionsSubject: Subject<FunctionInvocation> = ReplaySubject
            .create(2) /*
						 * new Publisher<FunctionInvocation>() { List<Subscriber<FunctionInvocation>>
						 * subscribers = new ArrayList<>(2);
						 * 
						 * @Override public void subscribe(final Subscriber<? super FunctionInvocation>
						 * aSubscriber) { subscribers.add((Subscriber<FunctionInvocation>) aSubscriber);
						 * } };
						 */
    var dependentTypesSubject: Subject<GenType> = ReplaySubject
            .create(2) /*
						 * new Publisher<GenType>() { List<Subscriber<GenType>> subscribers = new
						 * ArrayList<>(2);
						 * 
						 * @Override public void subscribe(final Subscriber<? super GenType>
						 * aSubscriber) { subscribers.add((Subscriber<GenType>) aSubscriber); } };
						 */

    fun addDependentFunction(aFunction: FunctionInvocation) {
//		dependentFunctions.add(aFunction);
        dependentFunctionsSubject.onNext(aFunction)
    }

    fun addDependentType(aType: GenType) {
//		dependentTypes.add(aType);
        dependentTypesSubject.onNext(aType)
    }

    // @Override
    override fun dependentFunctions(): List<FunctionInvocation> {
        return dependentFunctions
    }

    fun dependentFunctionSubject(): Subject<FunctionInvocation> {
        return dependentFunctionsSubject
    }

    // @Override
    override fun dependentTypes(): List<GenType> {
        return dependentTypes
    }

    fun dependentTypesSubject(): Subject<GenType> {
        return dependentTypesSubject
    }

    fun noteDependencies(d: Dependency) {
        d.noteDependencies(this, dependentFunctions, dependentTypes)
    }
} //
//
//

