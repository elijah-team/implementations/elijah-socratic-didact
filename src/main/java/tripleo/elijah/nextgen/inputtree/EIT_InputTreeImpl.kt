package tripleo.elijah.nextgen.inputtree

import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.util.Operation
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah_elevated.comp.model.CM_Resource
import tripleo.elijah_elevated.comp.model.CM_ResourceCompute2
import java.util.*

class EIT_InputTreeImpl : EIT_InputTree {
    // TODO 09/20 where is this used?
    class _Node(private val operation: Operation<*>) {
        fun operation(): Operation<*> {
            return operation
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as _Node
            return this.operation == that.operation
        }

        override fun hashCode(): Int {
            return Objects.hash(operation)
        }

        override fun toString(): String {
            return "_Node[operation=$operation]"
        }
    }

    override fun addNode(i: CompilerInput?) {
        val y = 2
    }


    override fun setNodeOperation(input: CompilerInput, operation: Operation<*>?) {
        val o = input.getExt(EIT_InputTreeImpl::class.java)
        if (o == null) {
            input.putExt(EIT_InputTreeImpl::class.java, _Node(operation!!))
        } else {
            input.putExt(EIT_InputTreeImpl::class.java, _Node(operation!!))
        }
    }

    override fun addResourceNode(aResource: CM_Resource?, aResourceCompute: CM_ResourceCompute2?) {
        System.err.println("IMPL-ME 24/01/06")
    }
}
