package tripleo.elijah.stages.gen_fn

import tripleo.elijah.lang.i.TypeName
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action_Arena

class SGTA_SetNonGenericTypeName(private val typeName: TypeName) : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        gt.nonGenericTypeName = typeName
    }
}
