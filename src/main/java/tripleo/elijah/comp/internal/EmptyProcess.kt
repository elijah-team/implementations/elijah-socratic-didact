package tripleo.elijah.comp.internal

import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.Compilation0
import tripleo.elijah.comp.Pipeline.RP_Context_1
import tripleo.elijah.comp.i.*
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation
import tripleo.elijah.util2.UnintendedUseException

class EmptyProcess(aCompilationAccess: ICompilationAccess?, aPr: ProcessRecord?) : RuntimeProcess {
    override fun postProcess() {
    }

    override fun prepare() {
    }

    override fun run(aComp: Compilation0, ctx0: RP_Context): Operation<Ok> {
        val ctx = ctx0 as RP_Context_1
        run(aComp as Compilation, ctx.state, ctx.context)
        throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
    }

    //@Override
    fun run(aComp: Compilation?, st: CR_State?, output: CB_Output?) {
        throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
    }
}
