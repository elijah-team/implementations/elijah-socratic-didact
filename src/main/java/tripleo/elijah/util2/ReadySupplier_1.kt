package tripleo.elijah.util2

import java.util.*

class ReadySupplier_1<T>(aT: T) : ReadySupplier<T> {
    // README why supply a NULL??
    private val t: T = Objects.requireNonNull(aT)

    override fun get(): T {
        check(ready()) {
            "not ready" // FIXME move this
        }
        return t
    }

    override fun ready(): Boolean {
        return t != null
    }
}
