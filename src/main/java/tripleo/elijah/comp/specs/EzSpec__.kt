package tripleo.elijah.comp.specs

import tripleo.elijah.diagnostic.ExceptionDiagnostic
import tripleo.elijah.util.Mode
import tripleo.elijah.util.Operation
import tripleo.elijah.util.Operation2
import tripleo.wrap.File
import java.io.IOException
import java.io.InputStream
import java.util.function.Supplier

data class EzSpec__
(
    val file_name: String,
    val file: File,
    val sis: Supplier<InputStream>?
) : EzSpec {
    override fun absolute1(): Operation<String> {
        // [T920907]
        val absolutePath: String
        try {
            absolutePath = file.canonicalFile.toString()
        } catch (aE: IOException) {
            return Operation.failure(aE)
        }
        return Operation.success(absolutePath)
    }

    override fun file_name(): String = file_name
    override fun file(): File = file
    override fun sis(): Supplier<InputStream>? = sis

    // TODO how do you do this??
    companion object {
        fun of(aFileName: String, aFile: File, aInputStreamOperation: Operation<InputStream?>): Operation2<EzSpec> {
            if (aInputStreamOperation.mode() == Mode.SUCCESS) {
                val ezSpec = EzSpec__(aFileName, aFile) { aInputStreamOperation.success()!! }
                return Operation2.success(ezSpec)
            } else {
                return Operation2.failure(ExceptionDiagnostic(aInputStreamOperation.failure()))
            }
        }
    }
}
