package tripleo.elijah.nextgen.outputstatement

import org.jetbrains.annotations.Contract
import tripleo.util.buffer.Buffer
import java.util.function.Function
import java.util.stream.Collectors

// TODO 11/29 looks a little bit like it doesn't belong
class GE_BuffersStatement @Contract(pure = true) constructor(aEntry: Map.Entry<String, Collection<Buffer>>) : EG_Statement {
	private val entry: Map.Entry<String, Collection<Buffer>> = aEntry


	override val explanation: EX_Explanation
		get() = GE_BuffersExplanation(this)

	override val text: String
		get() {
			val stringIterable: List<String?> = entry.value.stream() //.filter(entry)
					.map { obj: Buffer -> obj.text }
					.collect(Collectors.toList())

			return String_join("\n\n", stringIterable)
		}

	// TODO 11/29 why is message and getText different here?
	private data class GE_BuffersExplanation(val statement: GE_BuffersStatement) : EX_Explanation {
		private var message: String = "buffers to statement"

		override fun message(): String = "GE_BuffersExplanation"

		fun setMessage(aMessage: String) {
			message = aMessage
		}

		fun getText(): String = message
	}
}
