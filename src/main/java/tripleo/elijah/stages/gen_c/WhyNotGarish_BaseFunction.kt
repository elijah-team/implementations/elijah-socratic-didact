package tripleo.elijah.stages.gen_c

import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.lang.i.TypeName
import tripleo.elijah.nextgen.outputstatement.IReasonedString
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.instructions.*

abstract class WhyNotGarish_BaseFunction : WhyNotGarish_Item {
    /**
     * In places where this is used, change to "something else"
     * @return
     */
    @Deprecated("")
    open fun cheat(): BaseEvaFunction {
        return gf
    }

    abstract val gf: BaseEvaFunction

    fun classInvcationGenericPart(): Map<TypeName, OS_Type>? {
        return gf.fi!!.classInvocation!!.genericPart().map
    }

    fun findLabel(aIndex: Int): Label? {
        return gf.findLabel(aIndex)
    }

    fun getConstTableEntry(aIndex: Int): ConstantTableEntry {
        return gf.getConstTableEntry(aIndex)
    }

    val fd: FunctionDef
        get() = gf.fd

    val genClass: EvaNode
        get() = gf.genClass

    fun getProcTableEntry(aIndex: Int): ProcTableEntry {
        return gf.getProcTableEntry(aIndex)
    }

    val self: VariableTableEntry?
        get() = gf.self

    fun getTypeTableEntry(aIndex: Int): TypeTableEntry {
        return gf.getTypeTableEntry(aIndex)
    }

    fun getVarTableEntry(aIndex: Int): VariableTableEntry {
        return gf.getVarTableEntry(aIndex)
    }

    fun instructions(): List<Instruction> {
        return gf.instructions()
    }

    fun pointsToConstructor(): Boolean {
        return declaringContext().pointsToConstructor()
    }

    fun pointsToConstructor2(): Boolean {
        return declaringContext().pointsToConstructor2()
    }

    abstract override fun provideFileGen(fg: GenerateResultEnv)

    fun tte_for_result(): Pair<String?, TypeTableEntry?> {
        var result_index = gf.vte_lookup("Result")
        if (result_index == null) {
            // if there is no Result, there should be Value
            result_index = gf.vte_lookup("Value")
            // but Value might be passed in. If it is, discard value
            val vte = (result_index as IntegerIA?)!!.entry
            if (vte.vtt != VariableTableType.RESULT) result_index = null
            if (result_index == null) return Pair.of("void", null) // README Assuming Unit
        }

        // Get it from resolved
        val tte1 = gf.getTypeTableEntry((result_index as IntegerIA).index)
        return Pair.of(null, tte1)
    }

    fun tte_for_self(): TypeTableEntry {
        val result_index = gf.vte_lookup("self")
        val resultIA = result_index as IntegerIA?
        val vte = resultIA!!.entry
        assert(vte.vtt == VariableTableType.SELF)
        val tte1 = gf.getTypeTableEntry(resultIA.index)

        return tte1
    }

    fun vte_lookup(aText: String): InstructionArgument? {
        return gf.vte_lookup(aText)
    }

    abstract fun declaringContext(): WhyNotGarish_DeclaringContext

    fun getRealTargetName(gc: GenerateC, target: IntegerIA, aAOG: AOG?): String? {
        return gc.getRealTargetName(gf, target, aAOG)
    }

    fun getAssignmentValue(gc: GenerateC, value: InstructionArgument?): String {
        val gf = gf
        val assignmentValue = gc.getAssignmentValue(gf.self, value, gf)
        return assignmentValue
    }

    //public String getRealTargetName(final GenerateC gc, final IdentIA target, final Generate_Code_For_Method.AOG aAOG, final String assignmentValue) {
    //	final ZoneITE zi = gc._zone.get(target);
    //
    //	return zi.getRealTargetName2(aAOG, assignmentValue);
    //}

    fun getRealTargetName(gc: GenerateC, target: IdentIA, assignmentValue: String?): Garish_TargetName {
        val zi = gc._zone[target]

        return zi!!.getRealTargetName3(assignmentValue)
    }

    fun getRealTargetNameReasonedString(aGc: GenerateC, aTarget: IdentIA, aAssignmentValue: String?, aReason: String?, aAOG: AOG?): IReasonedString {
        val text = getRealTargetName(aGc, aTarget, aAssignmentValue)
        return text.reasonedForAOG(aAOG)
    }
}
