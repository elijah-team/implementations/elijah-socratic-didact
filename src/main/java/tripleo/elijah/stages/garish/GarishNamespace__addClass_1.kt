package tripleo.elijah.stages.garish

import tripleo.elijah.nextgen.output.NG_OutputNamespace
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_generic.GRS_Addable
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.DefaultGenerateResultSink
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink

class GarishNamespace__addClass_1 //gr              = aGr;
(private val garishNamespace: GarishNamespace, aGr: GenerateResult?, //private final GenerateResult gr;
 private val generateC: GenerateC) : GRS_Addable {
    override fun action(aResultSink: GenerateResultSink) {
        val o = NG_OutputNamespace()
        o.setNamespace(garishNamespace, generateC)
        (aResultSink as DefaultGenerateResultSink).pa().addOutput(o)
    }
}
