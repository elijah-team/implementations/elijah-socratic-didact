package tripleo.elijah.comp.internal

import com.google.common.base.Preconditions
import tripleo.elijah.comp.AccessBus
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.Compilation0
import tripleo.elijah.comp.Pipeline
import tripleo.elijah.comp.Pipeline.RP_Context_1
import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.ICompilationAccess
import tripleo.elijah.comp.i.RP_Context
import tripleo.elijah.comp.i.RuntimeProcess
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.util.*
import tripleo.vendor.mal.stepA_mal.MalEnv2
import tripleo.vendor.mal.types
import tripleo.vendor.mal.types.*

class OStageProcess(// private final ProcessRecord pr;
        private val ca: ICompilationAccess) : RuntimeProcess {
    private class _AddPipeline__MAL(private val ce: CompilationEnclosure) : MalFunction() {
        @Throws(MalThrowable::class)
        override fun apply(args: MalList): MalVal {
            val a0 = args.nth(0)

            if (a0 is MalSymbol) {
                // 0. accessors
                val pipelineName: String = a0.name

                // 1. observe side effect
                val pipelinePlugin = ce.getPipelinePlugin(pipelineName) ?: return types.False

                // 2. produce effect
                ce.addPipelinePlugin(pipelinePlugin)
                return types.True
            } else {
                // TODO exception? errSink??
                return types.False
            }
        }
    }

    private var ab: AccessBus? = null

    private var env: MalEnv2? = null

    init {
        val compilation = ca.compilation as Compilation
        val compilationEnclosure = compilation.compilationEnclosure
        compilationEnclosure.accessBusPromise.then { iab: AccessBus? ->
            Preconditions.checkNotNull(iab)
            ab = iab // NOTE Apparently not watching that hard
            env = ab!!.env()
            env!!.set(MalSymbol("add-pipeline"), _AddPipeline__MAL(compilationEnclosure))
        }
    }

    override fun postProcess() {
    }

    @Throws(Exception::class)
    override fun prepare() {
        env!!.re("(def! EvaPipeline 'native)")

        env!!.re("(add-pipeline 'LawabidingcitizenPipeline)")
        env!!.re("(add-pipeline 'EvaPipeline)")

        env!!.re("(add-pipeline 'DeducePipeline)") // FIXME note moved from ...

        env!!.re("(add-pipeline 'WritePipeline)")
        // env.re("(add-pipeline 'WriteMesonPipeline)");
        env!!.re("(add-pipeline 'WriteMakefilePipeline)")
        env!!.re("(add-pipeline 'WriteOutputTreePipeline)") // TODO add error checking
    }

    override fun run(aComp: Compilation0, ctx0: RP_Context): Operation<Ok> {
        val ctx = ctx0 as RP_Context_1

        return run_(aComp as Compilation, ctx.state, ctx.context)
    }

    fun run_(aCompilation: Compilation, st: CR_State?, output: CB_Output?): Operation<Ok> {
        val gPipeline = aCompilation.compilationEnclosure.compilationAccess.internal_pipelines()
        val ps = gPipeline as Pipeline

        try {
            ps.run(st!!, output)
            return Operation.success(Ok.instance())
        } catch (ex: Exception) {
            //Logger.getLogger(OStageProcess.class.getName()).log(Level.SEVERE, null, ex);
            //ex.printStackTrace();
            //throw new RuntimeException(ex);
            return Operation.failure(ex)
        }
    }
}
