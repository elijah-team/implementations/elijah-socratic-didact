/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.nextgen.diagnostic

import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Locatable
import tripleo.elijah.g.GClassInvocation
import tripleo.elijah.g.GGenerateFunctions
import tripleo.elijah.nextgen.ClassDefinition
import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.DeducePhase
import tripleo.elijah.stages.gen_fn.GenerateFunctions
import tripleo.elijah.stages.gen_fn.WlGenerateClass
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util2.UnintendedUseException
import java.io.PrintStream

/**
 * Created 3/5/22 4:55 PM
 */
class CouldntGenerateClass : Diagnostic {
    private val classDefinition: ClassDefinition?
    private val classInvocation: GClassInvocation?
    private val generateFunctions: GGenerateFunctions?
    private val gen: WlGenerateClass?
    private val deducePhase: DeducePhase?

    constructor(aClassDefinition: ClassDefinition?,
                aGenerateFunctions: GGenerateFunctions?,
                aClassInvocation: GClassInvocation?) {
        classDefinition = aClassDefinition
        generateFunctions = aGenerateFunctions
        classInvocation = aClassInvocation

        gen = null
        deducePhase = null
    }

    constructor(gen: WlGenerateClass?, deducePhase: DeducePhase?) {
        this.gen = gen
        this.deducePhase = deducePhase

        classDefinition = null
        generateFunctions = null
        classInvocation = null
    }

    override fun code(): String {
        return "E2000"
    }

    fun getClassDefinition(): ClassDefinition? {
        return if (gen != null) {
            null // !!
        } else {
            classDefinition
        }
    }

    fun getClassInvocation(): ClassInvocation? {
        return if (gen != null) {
            gen.classInvocation
        } else {
            classInvocation as ClassInvocation?
        }
    }

    fun getGenerateFunctions(): GenerateFunctions? {
        return if (gen != null) {
            gen.generateFunctions
        } else {
            generateFunctions as GenerateFunctions?
        }
    }

    override fun primary(): Locatable {
        throw UnintendedUseException("implement me when you are feeling unfulfilled")
    }

    override fun report(stream: PrintStream) {
        NotImplementedException.raise()
    }

    override fun secondary(): List<Locatable?> {
        throw UnintendedUseException("implement me when you are feeling unfulfilled")
    }

    override fun severity(): Diagnostic.Severity {
        return Diagnostic.Severity.ERROR
    }
}
