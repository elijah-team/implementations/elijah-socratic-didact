/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce.declarations

import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.DeduceTypes2.OS_SpecialVariable
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.IInvocation
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaConstructor
import tripleo.elijah.stages.gen_fn.EvaFunction
import tripleo.elijah.stages.gen_fn.GenType

/**
 * Created 11/21/21 6:32 AM
 */
class DeferredMemberFunction(@JvmField val parent: OS_Element,
                             /**
                              * A [tripleo.elijah.stages.deduce.ClassInvocation] or
                              * [tripleo.elijah.stages.deduce.NamespaceInvocation]. useless if parent
                              * is a [tripleo.elijah.stages.deduce.DeduceTypes2.OS_SpecialVariable] and
                              * its
                              * [tripleo.elijah.stages.deduce.DeduceTypes2.OS_SpecialVariable.memberInvocation]
                              * role value is
                              * [tripleo.elijah.stages.deduce.DeduceTypes2.MemberInvocation.Role.INHERITED]
                              */
                             private var invocation: IInvocation?,
                             @JvmField val functionDef: FunctionDef, private val deduceTypes2: DeduceTypes2,
                             private val functionInvocation: FunctionInvocation) {
    private val externalRef: DeferredObject<BaseEvaFunction, Void, Void>
    private val typePromise: DeferredObject<GenType, Diagnostic, Void>

    init { // TODO can this be nullable?

        typePromise = _inj().new_DeferredObject__GenType()
        externalRef = _inj().new_DeferredObject__BaseEvaFunction()

        functionInvocation.generatePromise().then { result ->
            if (result is EvaFunction) {
                deduceTypes2.deduceOneFunction(result, deduceTypes2._phase()) // !!
            } else {
                deduceTypes2.deduceOneConstructor(result as EvaConstructor, deduceTypes2._phase())
            }
            result.typePromise().then { result -> typePromise.resolve(result) }
        }
    }

    private fun _inj(): DeduceTypes2Injector {
        return deduceTypes2._inj()
    }

    fun externalRef(): Promise<BaseEvaFunction, Void, Void> {
        return externalRef.promise()
    }

    fun externalRefDeferred(): DeferredObject<BaseEvaFunction, Void, Void> {
        return externalRef
    }

    fun functionInvocation(): FunctionInvocation {
        return functionInvocation
    }

    fun getInvocation(): IInvocation? {
        if (invocation == null) {
            if (parent is OS_SpecialVariable) {
                invocation = parent.getInvocation(deduceTypes2)
            }
        }
        return invocation
    }

    override fun toString(): String {
        return "DeferredMemberFunction{" + "parent=" + parent + ", functionName=" + functionDef.name() + '}'
    }

    fun typePromise(): Promise<GenType, Diagnostic, Void> {
        return typePromise
    }

    // for DeducePhase
    fun typeResolved(): DeferredObject<GenType, Diagnostic, Void> {
        return typePromise
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

