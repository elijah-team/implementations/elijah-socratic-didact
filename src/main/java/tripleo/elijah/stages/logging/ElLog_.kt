/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.logging

import tripleo.elijah.stages.logging.ElLog.Verbosity
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Created 8/3/21 3:46 AM
 */
class ElLog_(private val fileName: String, private val verbose: Verbosity, private val phase: String) : ElLog {
    private val entries: MutableList<LogEntry> = ArrayList()

    override fun err(aMessage: String) {
        val time = System.currentTimeMillis()
        entries.add(LogEntry_(time, LogEntry.Level.ERROR, aMessage))
        if (verbose == Verbosity.VERBOSE) SimplePrintLoggerToRemoveSoon.println_err_2(aMessage)
    }

    override fun getEntries(): List<LogEntry> {
        return entries
    }

    override fun getFileName(): String {
        return fileName
    }

    override fun getPhase(): String {
        return phase
    }

    override fun info(aMessage: String) {
        val time = System.currentTimeMillis()
        entries.add(LogEntry_(time, LogEntry.Level.INFO, aMessage))
        if (verbose == Verbosity.VERBOSE) SimplePrintLoggerToRemoveSoon.println_out_2(aMessage)
    }
} //
//
//

