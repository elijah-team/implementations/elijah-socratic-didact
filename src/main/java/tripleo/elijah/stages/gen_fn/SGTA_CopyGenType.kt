package tripleo.elijah.stages.gen_fn

import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action_Arena

class SGTA_CopyGenType(private val _gt: GenType) : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        gt.copy(_gt)
    }
}
