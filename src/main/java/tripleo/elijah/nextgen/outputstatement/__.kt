package tripleo.elijah.nextgen.outputstatement

import tripleo.elijah.util2.ngosDebugFlags
import java.util.stream.Stream

//internal object `__` {
    fun String_join(separator: String, stringIterable: Iterable<String?>): String {
        if (ngosDebugFlags.FORCE_IGNORE) {
            val sb = StringBuilder()

            for (part in stringIterable) {
                sb.append(part)
                sb.append(separator)
            }
            val ss = sb.toString()
            val substring = separator.substring(0, ss.length - separator.length)
            return substring
        }
        // since Java 1.8
        return java.lang.String.join(separator, stringIterable)
    }

    // NOTE 11/30 this can be prettier ... ??
    fun String_join(separator: String, stringIterable: Stream<String?>): String {
        val sb = StringBuilder()

        stringIterable
                .forEach { part: String? ->
                    sb.append(part)
                    sb.append(separator)
                }

        val ss = sb.toString()
        val substring = separator.substring(0, ss.length - separator.length)
        return substring
    }
//}
