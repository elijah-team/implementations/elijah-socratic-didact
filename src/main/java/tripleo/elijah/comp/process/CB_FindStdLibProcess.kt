package tripleo.elijah.comp.process

import org.jetbrains.annotations.Contract
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.i.*
import tripleo.elijah.comp.i.extra.ICompilationRunner
import tripleo.elijah.comp.impl.SingleActionProcess
import tripleo.elijah.comp.internal.CompilationImpl
import tripleo.elijah.comp.internal.CompilationImpl.CompilationAlways.Companion.defaultPrelude
import tripleo.elijah.comp.internal.USE_Reasonings.Companion.findStdLib
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.Mode
import tripleo.elijah.util.Operation2

class CB_FindStdLibProcess(ce: CompilationEnclosure, cr: ICompilationRunner) : CB_Process {
    private val action = CB_FindStdLibAction(ce, cr)

    override fun steps(): List<CB_Action> {
        return Helpers.List_of<CB_Action>(action)
    }

    override fun name(): String {
        return "CB_FindStdLibProcess"
    }

    internal inner class CB_FindStdLibAction(private val ce: CompilationEnclosure, aCr: ICompilationRunner) : CB_Action {
        private val crState = aCr.crState
        private val o: List<CB_OutputString> = ArrayList() // FIXME 07/01 how is this modified?
        private var findStdLib: CD_FindStdLib? = null

        init {
            obtain() // TODO 09/08 Make this more complicated
        }

        override fun execute(aMonitor: CB_Monitor) {
            if (findStdLib != null) {
                val preludeName = defaultPrelude()
                findStdLib!!.findStdLib(crState, preludeName) { oci: Operation2<CompilerInstructions> -> this.getPushItem(oci) }

                val o = ce.cB_Output
                aMonitor.reportSuccess(this, o)
            }
        }

        private fun getPushItem(oci: Operation2<CompilerInstructions>) {
            if (oci.mode() == Mode.SUCCESS) {
                val c = ce.compilation
                val compilerInstructions = oci.success()

                c.use(compilerInstructions, findStdLib(findStdLib!!))
            } else {
                //throw new IllegalStateException();//oci.failure());
                System.err.println("6363 " + oci.failure().get())
            }
        }

        @Contract(pure = true)
        override fun name(): String {
            return "find std lib"
        }

        private fun obtain() {
            val x = ce.compilationDriver[CompilationImpl.CompilationAlways.Tokens.COMPILATION_RUNNER_FIND_STDLIB2]

            if (x.mode() == Mode.SUCCESS) {
                findStdLib = x.success() as CD_FindStdLib
            }
        }

        @Contract(value = " -> new", pure = true)
        fun process(): CB_Process {
            return SingleActionProcess(this, this@CB_FindStdLibProcess.name())
        }
    }
}
