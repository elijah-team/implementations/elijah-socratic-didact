package tripleo.elijah.stages.deduce.post_bytecode

import org.jdeferred2.DoneCallback
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_ProcTableEntry._0
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_ProcTableEntry._1
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util2.DebugFlags
import tripleo.elijah.util2.ReadySupplier_1
import tripleo.elijah.work.WorkList
import java.util.function.Consumer

internal class lfoe_action__FunctionInvocationDoneCallback(private val deduceElement3ProcTableEntry: DeduceElement3_ProcTableEntry, private val deduceTypes2: DeduceTypes2, private val addJobs: Consumer<WorkList>, private val q: __LFOE_Q, private val wl: WorkList) : DoneCallback<FunctionInvocation> {
    override fun onDone(fi: FunctionInvocation) {
        if (fi.function == null) {
            if (fi.pte == null) {
                return
            } else {
                //					LOG.err("592 " + fi.getClassInvocation());
                if (fi.pte.classInvocation != null) fi.classInvocation = fi.pte.classInvocation
                //					else
                //						fi.pte.setClassInvocation(fi.getClassInvocation());
            }
        }

        var ci = fi.classInvocation
        if (ci == null) {
            ci = fi.pte?.classInvocation
        }
        var fd3 = fi.function

        val principal = deduceElement3ProcTableEntry.tablePrincipal
        val deduceTypes2Injector = deduceElement3ProcTableEntry._inj()

        if (fd3 === LangGlobals.defaultVirtualCtor) {
            if (ci == null) {
                if ( /* fi.getClassInvocation() == null && */fi.namespaceInvocation == null) {
                    // Assume default constructor
                    ci = deduceTypes2.phase.registerClassInvocation((principal.resolvedElement as ClassStatement?)!!)
                    fi.classInvocation = ci
                } else throw NotImplementedException()
            }
            val klass = ci.klass

            val cis = klass.constructors

            if (DebugFlags.FORCE_IGNORE) {
                for (constructorDef in cis) {
                    val constructorDefArgs: Iterable<FormalArgListItem> = constructorDef.args

                    if (!constructorDefArgs.iterator().hasNext()) { // zero-sized arg list
                        fd3 = constructorDef
                        break
                    }
                }
            }

            val ocd = cis.stream()
                    .filter { acd: ConstructorDef -> acd.args.iterator().hasNext() }
                    .findFirst()
            if (ocd.isPresent) {
                fd3 = ocd.get()
            }
        }

        val parent: OS_Element?
        if (fd3 != null) {
            parent = fd3.parent
            if (parent is ClassStatement) {
                val parentClass = parent as ClassStatement
                if (ci !== principal.classInvocation) {
                    ci = deduceTypes2Injector.new_ClassInvocation(parentClass, null,
                            ReadySupplier_1(deduceElement3ProcTableEntry.deduceTypes2()!!))
                    run {
                        val classInvocation = principal.classInvocation
                        if (classInvocation != null) {
                            val gp = classInvocation.genericPart().map
                            if (gp != null) {
                                var i = 0
                                for ((key, value) in gp) {
                                    ci[i, key] = value
                                    i++
                                }
                            }
                        }
                    }
                }
                deduceElement3ProcTableEntry.__lfoe_action__proceed(_0(fi, ci!!, parentClass, deduceTypes2.phase, addJobs, q))
            } else if (parent is NamespaceStatement) {
                deduceElement3ProcTableEntry.__lfoe_action__proceed(_1(fi, parent, wl, deduceTypes2.phase, addJobs, q))
            }
        } else {
            parent = ci!!.klass
            run {
                val classInvocation = principal.classInvocation
                if (classInvocation != null && classInvocation.genericPart().hasGenericPart()) {
                    val gp = classInvocation.genericPart().map
                    var i = 0
                    for ((key, value) in gp!!) {
                        ci[i, key] = value
                        i++
                    }
                }
            }
            deduceElement3ProcTableEntry.__lfoe_action__proceed(_0(fi, ci, (parent as ClassStatement?)!!, deduceTypes2.phase, addJobs, q))
        }
    }
}
