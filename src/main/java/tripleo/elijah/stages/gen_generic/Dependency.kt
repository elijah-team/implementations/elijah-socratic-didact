/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_generic

import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.gen_fn.AbstractDependencyTracker
import tripleo.elijah.stages.gen_fn.EvaContainerNC
import tripleo.elijah.stages.gen_fn.GenType
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Created 9/13/21 4:00 AM
 */
class Dependency(val referent: IDependencyReferent?) {
    val dref: DependencyRef
        get() = TODO("just make it compile")




    val deps: MutableSet<Dependency> = HashSet()
    var ref: DependencyRef? = null
    var resolved: OS_Element? = null

    val notedDeps: Set<Dependency>
        get() = deps

    // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
    fun jsonString(): String {
        val sb = ("{\".class\": \"Dependency\", " + "referent: " + referent + ", " + "dref: "
                + (if (ref != null) ref!!.jsonString() + ", " else "null, ") + "deps: " + deps + ", " + "resolved: "
                + resolved +  /* +", " */
                "}")
        return sb
    }

    fun noteDependencies(aDependencyTracker: AbstractDependencyTracker?,
                         aDependentFunctions: List<FunctionInvocation>,
                         aDependentTypes: List<GenType>) {
        for (dependentFunction in aDependentFunctions) {
            val generatedFunction = dependentFunction.generated
            if (generatedFunction != null) deps.add(generatedFunction.dependency)
            else SimplePrintLoggerToRemoveSoon.println_err_2("52 false FunctionInvocation $dependentFunction")
        }
        for (dependentType in aDependentTypes) {
            val node = dependentType.node as EvaContainerNC
            if (node != null) deps.add(node.dependency)
            else {
                SimplePrintLoggerToRemoveSoon.println_err_2(
                        "46 node is null " + (if (dependentType.resolved != null) dependentType.resolved
                        else dependentType.resolvedn))
                val d = Dependency(null)
                d.resolved = if (dependentType.resolved != null) dependentType.resolved.classOf
                else dependentType.resolvedn
                deps.add(d)
            }
        }
    }
}
