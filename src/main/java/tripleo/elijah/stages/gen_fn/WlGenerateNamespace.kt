/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jdeferred2.Promise
import tripleo.elijah.stages.deduce.DeducePhase.GeneratedClasses
import tripleo.elijah.stages.deduce.NamespaceInvocation
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.work.WorkJob
import tripleo.elijah.work.WorkManager

/**
 * Created 5/31/21 3:01 AM
 */
class WlGenerateNamespace(private val generateFunctions: GenerateFunctions,
                          private val namespaceInvocation: NamespaceInvocation,
                          private val coll: GeneratedClasses?,
                          private val cr: ICodeRegistrar) : WorkJob {
    private val namespaceStatement = namespaceInvocation.namespace
    private var _isDone = false
    private var result: EvaNamespace? = null

    fun getResult(): EvaNode? {
        return result
    }

    override fun isDone(): Boolean {
        return _isDone
    }

    override fun run(aWorkManager: WorkManager?) {
        val resolvePromise = namespaceInvocation.resolveDeferred()
        when (resolvePromise.state()) {
            Promise.State.PENDING -> {
                val ns = generateFunctions.generateNamespace(namespaceStatement)

                // ns.setCode(generateFunctions.module.getCompilation().nextClassCode());
                cr.registerNamespace(ns)

                coll?.add(ns)

                resolvePromise.resolve(ns)
                result = ns
            }

            Promise.State.RESOLVED -> resolvePromise.then { result -> this@WlGenerateNamespace.result = result }
            Promise.State.REJECTED -> throw NotImplementedException()
        }
        _isDone = true
        //		tripleo.elijah.util.Stupidity.println_out_2(String.format("** GenerateNamespace %s at %s", namespaceInvocation.getNamespace().getName(), this));
    }
}
