package tripleo.elijah.nextgen.outputtree

import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.outputstatement.EG_Statement

interface EOT_OutputFile {
    val filename: String?

    val inputs: List<EIT_Input?>

    val statementSequence: EG_Statement

    val type: EOT_OutputType

    override fun toString(): String

//    override fun toString(): String // Any??
//    {
//        return "(%s) '%s'".formatted(type, filename.getFilename)
//    }

}
