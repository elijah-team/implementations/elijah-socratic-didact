/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import lombok.Getter
import lombok.Setter
import tripleo.elijah.lang.i.ConstructorDef
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.nextgen.DeduceCreationContext
import tripleo.elijah.stages.deduce.post_bytecode.__LFOE_Q
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.util.Helpers
import tripleo.elijah.util2.Eventual
import tripleo.elijah.util2.EventualRegister
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah.work.WorkList__

/**
 * Created 1/21/21 9:04 PM
 */
class FunctionInvocation(val function: FunctionDef?, @JvmField val pte: ProcTableEntry?, invocation: IInvocation,
						 phase: GeneratePhase?) : IInvocation {
    private val generateDeferred = Eventual<BaseEvaFunction>()

    @JvmField
    var hint: CI_Hint? = null
    var generated: BaseEvaFunction? = null

    // 24/01/04 back and forth
    // 24/01/04 back and forth
    @JvmField
    @Setter
    @Getter
    var namespaceInvocation: NamespaceInvocation? = null

    // 24/01/04 back and forth
    // 24/01/04 back and forth
    @JvmField
    @Setter
    @Getter
    var classInvocation: ClassInvocation? = null

    init {
        assert(invocation != null)
        invocation.setForFunctionInvocation(this)
        //		setPhase(deducePhase);
    }

    /*
	 * public void setPhase(final GeneratePhase generatePhase) { if (pte != null)
	 * pte.completeDeferred().then(new DoneCallback<ProcTableEntry>() {
	 *
	 * @Override public void onDone(ProcTableEntry result) {
	 * makeGenerated(generatePhase, null); } }); else makeGenerated(generatePhase,
	 * null); }
	 */
    fun generateFunction(aDeduceTypes2: DeduceTypes2?, aBest: OS_Element?): WlGenerateFunction {
        throw IllegalStateException("Error")
    }

    fun generateDeferred(): Eventual<BaseEvaFunction> {
        return generateDeferred
    }

    fun generatePromise(): Eventual<BaseEvaFunction> {
        return generateDeferred
    }

    val args: List<TypeTableEntry>
        get() {
            if (pte == null) return Helpers.List_of()
            return pte!!.args!!
        }

    val eva: BaseEvaFunction?
        get() {
            throw UnintendedUseException() // TODO 10/15

            //return null; // TODO 04/15
        }

    fun makeGenerated__Eventual(cl: DeduceCreationContext,
                                register: EventualRegister?): Eventual<BaseEvaFunction> {
        val deduceTypes2 = cl.deduceTypes2
        val eef = Eventual<BaseEvaFunction>()

        if (register != null) {
            eef.register(register)
        }

        var module: OS_Module? = null
        if (function != null) {
            val ___fd_context = function.context
            if (___fd_context != null) {
                if (___fd_context.parent != null) module = ___fd_context.module()
            }
        }
        if (module == null) module = classInvocation!!.klass.context.module() // README for constructors


        // TODO 10/15 is this q?; 10/17 yes, now find a way to use it
        val q = __LFOE_Q(null, WorkList__(), deduceTypes2)
        val injector = deduceTypes2._inj()

        if (function === LangGlobals.defaultVirtualCtor) {
            xxx___forDefaultVirtualCtor(cl, injector, module).then { p: BaseEvaFunction -> eef.resolve(p) }
            return eef
        } else if (function is ConstructorDef) {
            eef.resolve(xxxForConstructorDef(cl, function, injector, module))
            return eef
        } else {
            eef.resolve(xxx__forFunction(cl, injector, module))
            return eef
        }

        // {
        // eef.fail(null);
        // return eef;
        // }
    }

    private fun xxx___forDefaultVirtualCtor(cl: DeduceCreationContext,
                                            injector: DeduceTypes2Injector,
                                            module: OS_Module): Eventual<BaseEvaFunction> {
        val wlgdc = injector.new_WlGenerateDefaultCtor(module, this, cl)
        wlgdc.run(null)
        return wlgdc.generated
    }

    private fun xxxForConstructorDef(cl: DeduceCreationContext,
                                     cd: ConstructorDef,
                                     injector: DeduceTypes2Injector,
                                     module: OS_Module): BaseEvaFunction {
        val wlgf = injector.new_WlGenerateCtor(module, cd.nameNode, this, cl)
        wlgf.run(null)

        val gf: BaseEvaFunction = wlgf.result!!
        return gf
    }

    private fun xxx__forFunction(cl: DeduceCreationContext,
                                 injector: DeduceTypes2Injector,
                                 module: OS_Module): BaseEvaFunction {
        val generatePhase = cl.generatePhase
        val deducePhase = cl.deducePhase

        val wlgf = injector.new_WlGenerateFunction(module, this, cl)

        wlgf.run(null)

        val gf = wlgf.result

        if (gf?.genClass == null) {
            if (namespaceInvocation != null) {
                // namespaceInvocation =
                // deducePhase.registerNamespaceInvocation(namespaceInvocation.getNamespace());

                val wlgn = injector.new_WlGenerateNamespace(generatePhase.getGenerateFunctions(module),
                    namespaceInvocation,
                    deducePhase.generatedClasses,
                    deducePhase.codeRegistrar)
                wlgn.run(null)
                val y = 2
            }
        }

        return gf!!
    }

    override fun setForFunctionInvocation(aFunctionInvocation: FunctionInvocation) {
        throw IllegalStateException("maybe this shouldn't be done?")
    }

    override fun onResolve(function: (EvaClass) -> Unit) {
        TODO("Not yet implemented")
    }
}
