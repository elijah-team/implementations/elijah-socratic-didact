package tripleo.elijah.comp

import tripleo.elijah.comp.PipelineLogic.ModMap
import tripleo.elijah.comp.i.ModuleListener
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.notation.GN_PL_Run2.GenerateFunctionsRequest
import tripleo.elijah.g.GWorldModule
import tripleo.elijah.stages.deduce.DeducePhase
import tripleo.elijah.stages.deduce.DeducePhase.GeneratedClasses
import tripleo.elijah.util2.Eventual
import tripleo.elijah.world.i.WorldModule

internal class PL_ModuleListener(private val pipelineLogic: PipelineLogic, aPa: IPipelineAccess?) : ModuleListener {
    //	@SuppressWarnings("FieldCanBeLocal")
    //	private final IPipelineAccess      pa1;
    //		pa1           = aPa;
    private val fml = PL_ForModuleListener(pipelineLogic)

    override fun close() {
        //NotImplementedException.raise_stop();
    }

    override fun listen(module1: GWorldModule) {
        val module = module1 as WorldModule
        module.erq.then { rq: GenerateFunctionsRequest? -> fml.action(module, rq, pipelineLogic.dp, pipelineLogic.modMap) }
    }

    internal class PL_ForModuleListener(private val pipelineLogic: PipelineLogic) {
        fun action(module: WorldModule,
                   rq: GenerateFunctionsRequest?,
                   dp: DeducePhase?,
                   p: ModMap) {
            val mod = module.module()
            val gfm = pipelineLogic.getGenerateFunctions(mod)

            gfm.generateFromEntryPoints(rq!!)

            p.then(mod) { eventual: Eventual<GeneratedClasses> -> this._onDone(eventual) }
        }

        fun _onDone(eventual: Eventual<GeneratedClasses>) {
            val lgc = pipelineLogic.dp.generatedClasses
            eventual.resolve(lgc)
        }
    }
}
