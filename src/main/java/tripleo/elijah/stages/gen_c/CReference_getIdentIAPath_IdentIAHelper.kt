package tripleo.elijah.stages.gen_c

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Compilation
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.AliasStatementImpl
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.deduce.OnGenClass
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.util.*
import tripleo.elijah.world.impl.DefaultLivingRepo

internal class CReference_getIdentIAPath_IdentIAHelper @Contract(pure = true) constructor(@get:Contract(pure = true) val ia_next: InstructionArgument?, @get:Contract(pure = true) val sl: MutableList<String>, @get:Contract(pure = true) val i: Int,
                                                                                          private val sSize: Int, @get:Contract(pure = true) val resolved_element: OS_Element?, @get:Contract(pure = true) val generatedFunction: BaseEvaFunction,
                                                                                          @get:Contract(pure = true) val resolved: EvaNode?, @get:Contract(pure = true) val value: String?) {
    internal class CodeResolver {
        var code: Int = 0
            private set
        private var reason: String? = null
        var isSet: Boolean = false
            private set
        var isAnti: Boolean = false
            private set

        fun anti_provide(_code: Int, _reason: String?) { // !!
            code = _code
            reason = _reason
            isAnti = true
        }

        fun provide(aNc: EvaContainerNC, aReason: String?) {
            code = aNc.code
            reason = aReason
            isSet = true
        }

        fun provide(_code: Int, _reason: String?) {
            code = _code
            reason = _reason
            isSet = true
        }
    }

    var code: Int = -1

    private fun _act_ClassStatement(aCReference: CReference, b: Boolean): Boolean {
        // Assuming constructor call
        var b = b
        val code: Int
        if (resolved != null) {
            code = (resolved as EvaContainerNC?)!!.code
        } else {
            code = -1
            SimplePrintLoggerToRemoveSoon.println_err("** 31116 not resolved " + resolved_element)
        }
        // README might be calling reflect or Type or Name
        // TODO what about named constructors -- should be called with construct keyword
        if (ia_next is IdentIA) {
            val ite = (ia_next as IdentIA?)!!.entry
            val text = ite.ident.text
            if (text == "reflect") {
                b = true
                val text2 = String.format("ZS%d_reflect", code)
                aCReference.addRef(text2, CReference.Ref.FUNCTION)
            } else if (text == "Type") {
                b = true
                val text2 = String.format("ZST%d", code) // return a TypeInfo structure
                aCReference.addRef(text2, CReference.Ref.FUNCTION)
            } else if (text == "Name") {
                b = true
                val text2 = String.format("ZSN%d", code)
                aCReference.addRef(text2, CReference.Ref.FUNCTION) // TODO make this not a function
            } else {
                assert(i == getsSize() - 1 // Make sure we are ending with a constructor call
                )
                // README Assuming this is for named constructors
                val text2 = String.format("ZC%d%s", code, text)
                aCReference.addRef(text2, CReference.Ref.CONSTRUCTOR)
            }
        } else {
            assert(i == getsSize() - 1 // Make sure we are ending with a constructor call
            )
            val text2 = String.format("ZC%d", code)
            aCReference.addRef(text2, CReference.Ref.CONSTRUCTOR)
        }
        return b
    }

    private fun _act_ConstructorDef(aCReference: CReference) {
        assert(i == getsSize() - 1 // Make sure we are ending with a constructor call
        )
        val code: Int
        if (resolved != null) {
            code = (resolved as BaseEvaFunction?)!!.code
        } else {
            code = -1
            SimplePrintLoggerToRemoveSoon.println_err("** 31161 not resolved " + resolved_element)
        }
        // README Assuming this is for named constructors
        val text = (resolved_element as ConstructorDef?)!!.name()
        val text2 = String.format("ZC%d%s", code, text)
        aCReference.addRef(text2, CReference.Ref.CONSTRUCTOR)
    }

    private fun _act_DefFunctionDef(aCReference: CReference) {
        val parent = resolved_element!!.parent
        var code = -100

        val cr = CodeResolver()

        if (resolved != null) {
            if ((resolved is BaseEvaFunction)) {
                val rf = resolved as BaseEvaFunction
                val gc: EvaNode = rf.getGenClass()
                if (gc is EvaContainerNC) // and not another function
                {
                    code = gc.code

                    cr.provide(gc,
                            "_act_DefFunctionDef:getResolved-instanceof-BaseEvaFunction:genClass-instanceof-EvaContainerNC")
                } else {
                    code = -2

                    cr.anti_provide(-2,
                            "_act_DefFunctionDef:getResolved-instanceof-BaseEvaFunction:genClass-NOT-instanceof-EvaContainerNC")
                }
            } else {
                assert(false)
            }
        } else {
            code = if (parent is ClassStatement) {
                -3
            } else if (parent is NamespaceStatement) {
                -3
            } else {
                // TODO what about FunctionDef, etc
                -1
            }
        }
        assert(i == getsSize() - 1 // Make sure we are ending with a ProcedureCall
        )
        sl.clear()
        if (code == -1) {
//				text2 = String.format("ZT%d_%d", enclosing_function._a.getCode(), closure_index);
        }
        val defFunctionDef = resolved_element as DefFunctionDef?
        val text2 = String.format("z%d%s", code, defFunctionDef!!.name())
        aCReference.addRef(text2, CReference.Ref.FUNCTION)
    }

    private fun _act_FormalArgListItem(aCReference: CReference, fali: FormalArgListItem) {
        val y = 2
        val text2 = "va" + fali.nameToken.text
        aCReference.addRef(text2, CReference.Ref.LOCAL) // TODO
    }

    private fun _act_FunctionDef(aCReference: CReference) {
        val parent = resolved_element!!.parent
        var our_code = -1
        val resolved_node = resolved

        val cr = CodeResolver()

        if (resolved_node != null) {
            if (resolved_node is BaseEvaFunction) {
                resolved_node.onGenClass(OnGenClass { gc: EvaClass ->
                    val gc1: EvaNode = resolved_node.genClass
                    if (gc is EvaContainerNC) // and not another function
                    {
                        this.code = gc.living?.code!!

                        if (this.code == 0) {
                            val living = gc.living!!
                            val compilation0 = gc.element.context.module().compilation
                            val compilation = compilation0 as Compilation
                            living.code = (compilation.world() as DefaultLivingRepo).nextClassCode()
                            //							gc.setCode();
                            this.code = gc.code
                        }

                        cr.provide(gc,
                                "_act_FunctionDef:getResolved-instanceof-BaseEvaFunction:genClass-instanceof-EvaContainerNC")

                        assert(this.code > 0)
                    } else {
                        this.code = -2

                        cr.anti_provide(-2,
                                "_act_FunctionDef:getResolved-instanceof-BaseEvaFunction:genClass-NOT-instanceof-EvaContainerNC")
                    }
                })

                // TODO 09/06 maybe remove?
                if (resolved_node.genClass is EvaNamespace) {
                    // FIXME sometimes genClass is not called so above wont work,
                    // so check if a code was set and use it here
                    val generatedNamespace = resolved_node.genClass as EvaNamespace
                    val cc: Int = generatedNamespace.code1234567
                    if (cc > 0) {
                        this.code = cc

                        assert(this.code == cc)
                    }
                }
            } else if (resolved_node is EvaClass) {
                this.code = resolved_node.code

                cr.provide(resolved_node, "_act_DefFunctionDef:getResolved-instanceof-EvaClass")
            }
        }
        assert(i == getsSize() - 1 // Make sure we are ending with a ProcedureCall
        )
        sl.clear()

        if (!cr.isSet) {
            // README happens because onGenClass isn't resolved
            our_code = -1
        } else {
            assert(cr.isSet)
            assert(!cr.isAnti)
            assert(this.code == cr.code)
            our_code = cr.code // this.code;
        }

        // TODO CodeProviderTarget/EG_Statement
        val text2 = String.format("z%d%s", our_code, (resolved_element as FunctionDef?)!!.name())
        aCReference.addRef(text2, CReference.Ref.FUNCTION)
    }

    private fun _act_PropertyStatement(aCReference: CReference) {
        sl.clear() // don't we want all the text including from sl?

        val ps = resolved_element as PropertyStatement?

        val propertyGet = GCS_Property_Get(ps)
        val text2: String = propertyGet.text

        aCReference.addRef(text2, CReference.Ref.PROPERTY_GET)

        aCReference.__cheat_ret = text2
    }

    private fun _act_VariableStatement(aCReference: CReference) {
        val variableStatement = resolved_element as VariableStatementImpl?

        val givs = aCReference._repo()!!.itemFor(variableStatement) as GI_VariableStatement?
        givs!!._createReferenceForVariableStatement(aCReference, generatedFunction, value)
    }

    fun action(aCRI_ident: CRI_Ident?, aCReference: CReference): Boolean {
        var b = false
        val resolvedElement = resolved_element

        if (resolvedElement is ClassStatement) {
            b = _act_ClassStatement(aCReference, b)
        } else if (resolvedElement is ConstructorDef) {
            _act_ConstructorDef(aCReference)
        } else if (resolvedElement is FunctionDef) {
            _act_FunctionDef(aCReference)
        } else if (resolvedElement is DefFunctionDef) {
            _act_DefFunctionDef(aCReference)
        } else if (resolvedElement is VariableStatementImpl) {
            _act_VariableStatement(aCReference)
        } else if (resolvedElement is PropertyStatement) {
            _act_PropertyStatement(aCReference)
        } else if (resolvedElement is AliasStatementImpl) {
            _act_AliasStatement()
        } else if (resolvedElement is FormalArgListItem) {
            _act_FormalArgListItem(aCReference, resolvedElement)
        } else {
            // text = idte.getIdent().getText();
            SimplePrintLoggerToRemoveSoon.println_out("1008 " + resolvedElement!!.javaClass.name)
            throw NotImplementedException()
        }
        return b
    }

    @Contract(pure = true)
    fun getsSize(): Int {
        return sSize
    }

    companion object {
        @Contract(pure = true)
        private fun _act_AliasStatement() {
            val y = 2
            NotImplementedException.raise()
            // text = Emit.emit("/*167*/")+((AliasStatementImpl)resolved_element).name();
            // return _getIdentIAPath_IdentIAHelper(text, sl, i, sSize, _res)
        }
    }
}

internal interface ICodeResolver {
    fun getCode(): Int
}
