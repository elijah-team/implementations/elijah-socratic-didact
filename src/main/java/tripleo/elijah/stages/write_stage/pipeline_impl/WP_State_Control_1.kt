package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.elijah.comp.nextgen.pn.SC_Suc
import tripleo.elijah.nextgen.pn.SC_Fai
import tripleo.elijah.stages.write_stage.pipeline_impl.WP_Flow.FlowStatus
import tripleo.elijah.stages.write_stage.pipeline_impl.WP_Flow.OPS
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Purpose: to hold an exception for each [WP_Individual_Step]
 */
class WP_State_Control_1 : WP_State_Control {
    private var e: Exception? = null
    private var __cur: WP_Individual_Step? = null
    private var __cuo: OPS? = null

    override fun clear() {
        e = null
    }

    override fun exception(ee: Exception) {
        e = ee
    }

    // TODO DiagnosticException == ExceptionDiagnostic
    override fun getException(): Exception {
        return e!!
    }

    override fun hasException(): Boolean {
        return e != null
    }

    override fun markSuccess(aSuc: SC_Suc) {
        if (__cur != null) {
            val step: WP_Individual_Step = __cur!!
            val ops = __cuo
            ops!!.put(step, FlowStatus.TRIED, Operation.success(Ok.instance()))
        }

        // FIXME 10/19 just mark for now
        SimplePrintLoggerToRemoveSoon.println_err_4("[%s] markSuccess (%s) (%s)".formatted("Default", this.javaClass.name, aSuc.asString()))
        //NotImplementedException.raise_stop();
    }

    override fun markFailure(aFai: SC_Fai) {
        if (__cur != null) {
            val step: WP_Individual_Step = __cur!!
            val ops = __cuo
            ops!!.put(step, FlowStatus.FAILED, Operation.failure(this.exception))
        }

        // FIXME 10/19 just mark for now
        SimplePrintLoggerToRemoveSoon.println_err_4("[%s] markSuccess (%s) (%s)".formatted("Default", this.javaClass.name, aFai.sc_fai_asString()))
        //NotImplementedException.raise_stop();
    }

    override fun cur(step: WP_Individual_Step,
                     aWritePipelineSharedState: WritePipelineSharedState,
                     ops: OPS) {
        // FIXME 10/19 crossover in ops
        this.clear()

        try {
            __cur = step
            __cuo = ops
            step.act(aWritePipelineSharedState, this)
        } finally {
            __cur = null
            __cuo = null
        }
    }
}
