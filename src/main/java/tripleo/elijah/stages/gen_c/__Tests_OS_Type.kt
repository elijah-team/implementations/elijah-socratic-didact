package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.NormalTypeName
import tripleo.elijah.lang.i.OS_Type

internal enum class __Tests_OS_Type {
    ;

    companion object {
        fun boundedClassName_NormalTypeName(ty: OS_Type): String {
            val el = ty.classOf
            val name = if (ty is NormalTypeName) {
                (ty as NormalTypeName).name
            } else {
                el.name
            }
            return name
        }
    }
}
