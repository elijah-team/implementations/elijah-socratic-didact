package tripleo.elijah.comp;

import org.apache.commons.lang3.tuple.Pair;
import org.jdeferred2.DoneCallback;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ElijahCli;
import tripleo.elijah.comp.i.ErrSink;
import tripleo.elijah.comp.inputs.CompilerInput;
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure;
import tripleo.elijah.util2.UnintendedUseException;
import tripleo.elijah.world.i.LivingRepo;
import tripleo.wrap.File;

import java.util.List;

public class ElijahTestCli {
	public /*final*/ ElijahCli cli;

	public static ElijahTestCli createDefault() {
		final ElijahTestCli instance = new ElijahTestCli();
		instance.cli = ElijahCli.createDefault();
		return instance;
	}

	public static ElijahTestCli createDefaultWithArgs(final List<String> aStringList, final String aS) {
		final ElijahTestCli instance = new ElijahTestCli();
		instance.cli = ElijahCli.createDefaultWithArgs(aStringList, aS);
		return instance;
	}

	public void main(final String[] args) throws Exception {
		cli.main(args);
	}

	public void feedCmdLine(final List<String> args) {
		cli.feedCmdLine(args);
	}

	public @NotNull List<CompilerInput> stringListToInputList(final @NotNull List<String> args) {
		return cli.stringListToInputList(args);
	}

	public int obtainErrorCount() {
		return cli.obtainErrorCount();
	}

	public boolean _calledFeedCmdLine() {
		return cli._calledFeedCmdLine();
	}

	public int errorCount() {
		return cli.errorCount();
	}

	public LivingRepo world() {
		return cli.getComp().world();
	}

	public IO getIO() {
		return cli.getComp().getIO();
	}

	public Finally reports() {
		return cli.getComp().reports();
	}

	public boolean isPackage(final String aPackageName) {
		return cli.getComp().world().isPackage(aPackageName);
	}

	public boolean outputTree_isEmpty() {
		return cli.getComp().getOutputTree().getList().isEmpty();
	}

	public CompilationEnclosure getCompilationEnclosure() {
		return cli.getComp().getCompilationEnclosure();
	}

	public List<Pair<ErrSink.Errors, Object>> errSinkList() {
		return cli.getComp().getErrSink().list();
	}

    public int instructionCount() {
		return 4;
    }

	public ET_SourceRoot addSourceRoot(String test) {
		ET_SourceRoot sourceRoot = new ET_SourceRoot() {
			@Override
			public String getDirectoryName() {
				return test;
			}

			@Override
			public File getDirectory() {
				return ftest;
			}

			private final File ftest = new File(test);
		};

		this.cli.getComp().addSourceRoot(sourceRoot);
		return sourceRoot;
	}

	public void addSourceDirectory(String aFilePath, ET_SourceRoot aSourceRoot) {
		throw new UnintendedUseException("placemarker");
	}

	public void addStageAssertion(String string) {
		this.cli.getComp().addStageAssertion(string);
	}

	public void addResultSink(DoneCallback<CT_ResultSink> resultSink, CT_ResultFilter rfltr) {
		throw new UnintendedUseException("placemarker");
	}

	public void triggerResults() {
		this.cli.getComp().triggerResults();
	}
}
