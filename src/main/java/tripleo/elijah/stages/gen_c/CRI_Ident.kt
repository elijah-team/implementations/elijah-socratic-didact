package tripleo.elijah.stages.gen_c

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.function.Consumer

internal class CRI_Ident(private val ite: IdentTableEntry, private val generatedFunction: BaseEvaFunction) {
    fun _getIdentIAPath_IdentIAHelper(ia_next: InstructionArgument?, sl: MutableList<String>, i: Int,
                                      sSize: Int, resolved_element: OS_Element?, generatedFunction: BaseEvaFunction,
                                      aResolved: EvaNode?, aValue: String?, aCReference: CReference): Boolean {
        return CReference_getIdentIAPath_IdentIAHelper(ia_next, sl, i, sSize, resolved_element, generatedFunction,
                aResolved, aValue).action(this, aCReference)
    }

    private fun _re_is_PropertyStatement(addRef: Consumer<CReference.Reference>,
                                         aog: AOG, sSize: Int, i: Int, aValue: String?,
                                         skip: Consumer<Void?>, text: Consumer<String?>): EvaNode? {
        NotImplementedException.raise()
        val resolved1 = ite.type?.resolved()
        val code = if (resolved1 != null) (resolved1 as EvaContainerNC).code
        else -1
        var state/*: Short*/ = 0
        state = if (i < sSize - 1) {
            1
        } else {
            when (aog) {
                AOG.GET -> 1
                AOG.ASSIGN -> 2
            }
        }
        when (state) {
            1 -> {
                addRef.accept(CReference.Reference(String.format("ZP%d_get%s(", code, ite.ident.text),
                        CReference.Ref.PROPERTY_GET))
                skip.accept(null)
                text.accept(null)
            }

            2 -> {
                addRef.accept(CReference.Reference(String.format("ZP%d_set%s(", code, ite.ident.text),
                        CReference.Ref.PROPERTY_SET, aValue))
                skip.accept(null)
                text.accept(null)
            }

            else -> throw IllegalStateException("Unexpected value: $state")
        }
        return resolved1
    }

    fun getIdentIAPath(i: Int, sSize: Int, aog: AOG,
                       sl: MutableList<String>, aValue: String?, addRef: Consumer<CReference.Reference>,
                       s: List<InstructionArgument>, ia2: IdentIA?, aCReference: CReference,
                       item: CR_ReferenceItem): String? {
        var i = i
        val skip = booleanArrayOf(false)
        val resolved_element = ite.resolvedElement
        val text = arrayOf<String?>(null)

        val _cheat = aCReference._cheat

        val repo_element = aCReference._repo()!!.itemFor(resolved_element)

        // item.setInstructionArgument(s.get(i));
        item.generateCItem = repo_element

        if (resolved_element != null) {
            val resolved2: EvaNode?

            // assert repo_element != null;
            if (repo_element == null) {
                val y = 2
                // throw new AssertionError();
            } else {
                if (resolved_element is ClassStatement) {
                    (repo_element as GI_ClassStatement).setITE(ite)
                } else if (resolved_element is FunctionDef) {
                    val pte = ite.callablePTE
                    resolved2 = (repo_element as GI_FunctionDef)._re_is_FunctionDef(pte, _cheat, ite)

                    repo_element.setEvaNode(resolved2)
                } else if (resolved_element is PropertyStatement) {
                    resolved2 = _re_is_PropertyStatement(addRef, aog, sSize, i, aValue, { x: Void? -> skip[0] = true },
                            { x: String? -> text[0] = x })

                    repo_element.evaNode = resolved2

                    skip[0] = false
                } else if (resolved_element is VariableStatementImpl) {
                    // repo_element.set
                    // addRef.accept(new CReference.Reference(resolvedElement.getName(),
                    // CReference.Ref.MEMBER/*??*/));
                    item.text = resolved_element.name
                    if (aCReference.refs!!.size > 0) item.reference = aCReference.refs!![aCReference.refs!!.size - 1]
                }
            }
        }

        if (repo_element != null) {
            val resolved0 = repo_element.evaNode
            val resolved: EvaNode?

            // if (resolved0 == null) {
            // if (item.getTableEntry() instanceof IdentTableEntry ite) {
            // if (ite.hasResolvedElement()) {
            // resolved = ite.getResolvedElement();
            // }
            // }
            // } else
            run {
                resolved = resolved0
            }

            if (!skip[0]) {
                var state/*: Short*/ = 1
                if (ite.externalRef() != null) {
                    state = 2
                }
                when (state) {
                    1 -> {
                        if (resolved == null) {
                            SimplePrintLoggerToRemoveSoon.println_err("***88*** resolved is null for $ite")
                        }
                        if (sSize >= i + 1) {
                            _getIdentIAPath_IdentIAHelper(null, sl, i, sSize, resolved_element, generatedFunction, resolved,
                                    aValue, aCReference)
                            var x = aCReference.__cheat_ret
                            if (x == null && repo_element is GI_VariableStatement) {
                                repo_element.setItem(item)
                                x = repo_element.text
                            }
                            if (x == null && resolved_element is VariableStatement) {
                                x = resolved_element.name
                            }
                            text[0] = x
                        } else {
                            val b = _getIdentIAPath_IdentIAHelper(s[i + 1], sl, i, sSize, resolved_element,
                                    generatedFunction, resolved, aValue, aCReference)
                            if (b) i++
                        }
                    }

                    2 -> if ((resolved_element is VariableStatementImpl)) {
                        val text2 = resolved_element.name

                        ite.onExternalRef { externalRef: EvaNode? ->
                            if (externalRef is EvaNamespace) {
                                val text3 = String.format("zN%d_instance",
                                        externalRef.code1234567)
                                addRef.accept(CReference.Reference(text3, CReference.Ref.LITERAL, null))
                            } else if (externalRef is EvaClass) {
                                assert(false)
                                val text3 = String.format("zN%d_instance", externalRef.code)
                                addRef.accept(CReference.Reference(text3, CReference.Ref.LITERAL, null))
                            } else {
                                throw IllegalStateException()
                            }
                        }
                        addRef.accept(CReference.Reference(text2, CReference.Ref.MEMBER, aValue))
                    } else {
                        throw NotImplementedException()
                    }
                }
            }
        } else {
            when (ite.status) {
                BaseTableEntry.Status.KNOWN -> text[0] = Emit.emit("/*194*/") + ite.ident.text
                BaseTableEntry.Status.UNCHECKED -> {
                    val path2 = generatedFunction.getIdentIAPathNormal(ia2!!)
                    val text3 = String.format("<<UNCHECKED ia2: %s>>", path2 /* idte.getIdent().getText() */)
                    text[0] = text3
                }

                BaseTableEntry.Status.UNKNOWN -> {
                    val path = generatedFunction.getIdentIAPathNormal(ia2!!)
                    val text1 = ite.ident.text
                    //						assert false;
                    // TODO make tests pass but I dont like this (should emit a dummy function or
                    // placeholder)
                    if (sl.size == 0) {
                        text[0] = Emit.emit("/*149*/") + text1 // TODO check if it belongs somewhere else (what does this
                        // mean?)
                    } else {
                        text[0] = Emit.emit("/*152*/") + "vm" + text1
                    }
                    SimplePrintLoggerToRemoveSoon.println_err("119 " + ite.ident + " " + ite.status)
                    val text2 = (Emit.emit("/*114*/") + String.format("%s is UNKNOWN", text1))
                    addRef.accept(CReference.Reference(text2, CReference.Ref.MEMBER))
                }

                else -> throw IllegalStateException("Unexpected value: " + ite.status)
            }
        }

        return text[0]
    }

    companion object {
        @Contract(value = "_, _ -> new", pure = true)
        fun of(aIdte: IdentTableEntry, aGf: BaseEvaFunction): CRI_Ident {
            return CRI_Ident(aIdte, aGf)
        }
    }
}
