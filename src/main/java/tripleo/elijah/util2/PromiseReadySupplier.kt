package tripleo.elijah.util2

import org.jdeferred2.DoneCallback
import org.jdeferred2.Promise
import java.lang.UnsupportedOperationException

class PromiseReadySupplier<T>(private val p: Promise<T, Void, Void>) : ReadySupplier<T> {
    override fun get(): T {
        throw UnsupportedOperationException()
    }

    override fun ready(): Boolean {
        return p.isResolved
    }

    fun then(aO: DoneCallback<T>?) {
        p.then(aO)
    }
}
