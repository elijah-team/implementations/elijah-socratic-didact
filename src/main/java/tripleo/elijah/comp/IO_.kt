/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp

import tripleo.elijah.comp.IO._IO_ReadFile
import tripleo.elijah.util.io.CharSource
import tripleo.elijah.util.io.DisposableCharSink
import tripleo.elijah.util.io.FileCharSink
import tripleo.wrap.File
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Path

class IO_ : IO { //
    // exists, delete, isType ....
    val recordedwrites: MutableList<File> = ArrayList()
    private val recordedreads: MutableList<_IO_ReadFile> = ArrayList()

    override fun openRead(p: Path): CharSource? {
        record(FileOption.READ, p)
        return null
    }

    @Throws(IOException::class)
    override fun openWrite(p: Path): DisposableCharSink {
        record(FileOption.WRITE, p)
        return FileCharSink(Files.newOutputStream(p))
    }

    @Throws(FileNotFoundException::class)
    override fun readFile(f: File): InputStream {
        record(FileOption.READ, f)
        return f.fileInputStream
    }

    @Throws(FileNotFoundException::class)
    override fun readFile2(f: File): _IO_ReadFile {
        val readFile = _IO_ReadFile(f)

        record(readFile)

        val inputStream = f.fileInputStream
        readFile.inputStream = inputStream

        return readFile
    }

    override fun record(read: FileOption, file: File) {
        when (read) {
            FileOption.WRITE -> recordedwrites.add(file)
            FileOption.READ -> record(_IO_ReadFile(file))
            else -> throw IllegalStateException("Cant be here")
        }
    }

    override fun recordedRead(file: File): Boolean {
        return recordedreads.stream().anyMatch { x: _IO_ReadFile -> x.file.absolutePath == file.absolutePath }
    }

    override fun recordedWrite(file: File): Boolean {
        return recordedwrites.contains(file)
    }

    override fun recordedreads_io(): List<_IO_ReadFile> {
        return recordedreads
    }

    override fun recordedreads(): kotlin.collections.List<String> {
        return recordedreads.stream().map { it.file.betterName }.toList()
    }

    override fun record(aReadFile: _IO_ReadFile) {
        recordedreads.add((aReadFile))
    }

    override fun recordedwrites(): List<File> {
        return this.recordedwrites
    }
}
//
//

