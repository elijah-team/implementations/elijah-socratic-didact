package tripleo.elijah.comp

import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputtree.EOT_OutputFile
import tripleo.elijah.nextgen.outputtree.EOT_OutputFileImpl
import tripleo.elijah.nextgen.outputtree.EOT_OutputType
import tripleo.elijah.stages.gen_generic.Old_GenerateResult
import tripleo.elijah.util.*
import java.util.function.Consumer
import java.util.function.Supplier
import java.util.stream.Collectors

class WriteMakefilePipeline(pa0: GPipelineAccess) : PipelineMember(), Consumer<Supplier<Old_GenerateResult?>?> {
    private val pa = pa0 as IPipelineAccess

    override fun accept(t: Supplier<Old_GenerateResult?>?) {
        // TODO Auto-generated method stub
    }

    @Throws(Exception::class)
    override fun run(st: CR_State, aOutput: CB_Output?) {
        val c = st.ca().compilation as Compilation
        val cot = c.outputTree

        cot.recompute()

        val list1 = cot.list
        val eof = testable_part(list1)

        cot.add(eof)
    }

    override fun finishPipeline_asString(): String {
        return this.javaClass.toString()
    }

    fun testable_part(list1: List<EOT_OutputFile>): EOT_OutputFile {
        val list = list1.stream().filter { off: EOT_OutputFile -> off.type == EOT_OutputType.SOURCES }.collect(Collectors.toList())

        val sb = StringBuilder()

        sb.append("CC=cc -g\n")
        sb.append("CODE=code2/\n")
        sb.append("\n")
        sb.append("all:\n")

        //		sb.append("all:\n\tfalse");

        // sb.append(Helpers.String_join("\n",
        // list.stream().map(x->x.getFilename()).collect(Collectors.toList())));
        val prelc: MutableList<EOT_OutputFile> = ArrayList()
        val stdlibc: MutableList<EOT_OutputFile> = ArrayList()
        val sourcec: MutableList<EOT_OutputFile> = ArrayList()
        val otherc: List<EOT_OutputFile> = ArrayList()

        for (off in list) {
            val fn = off.filename!!

            if (fn.startsWith("/Prelude/")) {
                prelc.add(off)
            } else if (fn.startsWith("/stdlib/")) {
                stdlibc.add(off)
            } else { // if (fn.startsWith("/stdlib/")) {
                sourcec.add(off)
            }
        }

        val g = G()
        process(g, prelc)
        process(g, stdlibc)
        process(g, sourcec)
        process1(g)

        sb.append('\n')
        sb.append(g.text)
        sb.append('\n')

        val seq = EG_Statement.of(sb.toString(), EX_Explanation.withMessage("the Makefile"))

        /*
		 * var input_list = list1.stream() .map(off -> off.getFilename()) .map(s -> new
		 * EIT_ModuleInput(string_to_module(s), null)) .collect(Collectors.toList());
		 */
        val eof: EOT_OutputFile = EOT_OutputFileImpl3(Helpers.List_of(), "Makefile", EOT_OutputType.BUILD, seq)
        return eof
    }


    private fun process(aG: G, aL: List<EOT_OutputFile>) {
        val sb = StringBuilder()

        for (off in aL) {
            val fn = off.filename!!

            if (fn.endsWith(".c")) {
                val fn2a = fn.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val fn2 = java.util.List.of(*fn2a)

                SimplePrintLoggerToRemoveSoon.println_out_4(115, "" + fn2)

                val fn3: List<String> = fn2.subList(1, fn2.size - 1)
                val fn4 = Helpers.String_join("/", fn3)

                sb.append("\t-mkdir -p \"B/%s\"\n".formatted(fn4))

                val fn_dot_o = fn.substring(0, fn.length - 2) + ".o"
                aG.add_object(fn_dot_o)
                sb.append("\t$(CC) -c $(CODE)/%s -o B/%s -I$(CODE)\n".formatted(fn, fn_dot_o))
            }
        }

        aG.append(sb.toString())
    }

    private fun process1(aG: G) {
        aG.append_string("\tcc \\\n")
        for (`object` in aG.objects) {
            aG.append_string("\t\tB/%s \\\n".formatted(`object`))
        }
        aG.append_string("\t\t-o B/output.exe -l gc\n")
        aG.append_string("\n")
    }

    internal inner class G {
        val sb: StringBuilder = StringBuilder()
        val objects: MutableList<String> = ArrayList()

        fun add_object(obj: String) {
            objects.add(obj)
        }

        fun append(aString: String?) {
            sb.append(aString)
        }

        fun append_string(s: String?) {
            sb.append(s)
        }

        val text: String
            get() = sb.toString()
    }
}

fun EOT_OutputFileImpl3(
    listOf: List<EIT_Input>,
    s: String,
    build: EOT_OutputType,
    seq: EG_Statement,
): EOT_OutputFile {
    val ss = `__`.EOT_FileNameProvider2(s)
    return EOT_OutputFileImpl(listOf , ss, build, seq)
}
