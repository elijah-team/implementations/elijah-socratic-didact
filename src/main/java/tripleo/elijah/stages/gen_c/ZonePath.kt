package tripleo.elijah.stages.gen_c

import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument

class ZonePath(private val identIA: IdentIA, private val s: List<InstructionArgument>) : ZoneMember
