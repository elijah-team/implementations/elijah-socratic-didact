/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp

import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.stages.deduce.pipeline_impl.DeducePipelineImpl
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Created 8/21/21 10:10 PM
 */
class DeducePipeline(pa0: GPipelineAccess) : PipelineMember() {
    private val impl: DeducePipelineImpl

    init {
        // logProgress("***** Hit DeducePipeline constructor");
        val pa = pa0 as IPipelineAccess

        impl = DeducePipelineImpl(pa)
    }

    protected fun logProgress(g: String?) {
        SimplePrintLoggerToRemoveSoon.println_err_2(g)
    }

    override fun run(aSt: CR_State, aOutput: CB_Output?) {
        // logProgress("***** Hit DeducePipeline #run");
        impl.run()
    }

    override fun finishPipeline_asString(): String {
        return this.javaClass.toString()
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

