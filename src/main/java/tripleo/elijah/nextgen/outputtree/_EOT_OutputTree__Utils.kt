package tripleo.elijah.nextgen.outputtree

import tripleo.elijah.nextgen.outputstatement.EG_SequenceStatement
import tripleo.elijah.nextgen.outputstatement.EG_Statement

internal object _EOT_OutputTree__Utils {
    fun _extractStatementSequenceFromAllOutputFiles(
            tt: Collection<EOT_OutputFile>): List<EG_Statement> {
        val list2: MutableList<EG_Statement> = ArrayList()
        for (of1 in tt) {
            list2.addAll(_extractStatementSequenceFromOutputFile(of1))
        }
        return list2
    }

    private fun _extractStatementSequenceFromOutputFile
        (
            of1: EOT_OutputFile
        ): List<EG_Statement>
    {
        val sequence = of1.statementSequence
        val result : MutableList<EG_Statement> = listOf<EG_Statement>().toMutableList()

        if (sequence is EG_SequenceStatement) {
            for (egStatement in result) {
                result.add(egStatement)
            }
        } else {
            result.add(sequence)
        }

        return result
    }
}
