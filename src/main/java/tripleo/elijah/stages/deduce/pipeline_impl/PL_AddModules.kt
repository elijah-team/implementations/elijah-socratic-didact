package tripleo.elijah.stages.deduce.pipeline_impl

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.util.CompletableProcess
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.Eventual
import tripleo.elijah.world.i.WorldModule

internal class PL_AddModules @Contract(pure = true) constructor(aPipelineAccess: IPipelineAccess) : PipelineLogicRunnable {
	private val plp = Eventual<PipelineLogic>()

	init {
		val w = aPipelineAccess.compilation.world()

		w.addModuleProcess(object : CompletableProcess<WorldModule> {
			override fun add(item: WorldModule) {
//				plp.then(pipelineLogic -> pipelineLogic.addModule(item));
				SimplePrintLoggerToRemoveSoon.println_err_4("[PL_AddModules::ModuleCompletableProcess] add " + item.module().fileName)
			}

			override fun complete() {
//				NotImplementedException.raise_stop();
			}

			override fun error(d: Diagnostic) {
				//	throw new UnintendedUseException();
			}

			override fun preComplete() {
				//throw new UnintendedUseException();
			}

			override fun start() {
				SimplePrintLoggerToRemoveSoon.println_out_4("[PL_AddModules::ModuleCompletableProcess] start")
			}
		})
	}

	override fun run(pipelineLogic: PipelineLogic) {
		plp.resolve(pipelineLogic)
	}
}
