package tripleo.elijah.stages.gen_c

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.nextgen.outputstatement.*
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.ProcTableEntry
import tripleo.elijah.stages.gen_fn.ProcTableEntry.ECT
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

class GCX_FunctionCall @Contract(pure = true) constructor(private val gf: WhyNotGarish_BaseFunction, private val gc: GenerateC,
                                                          private val instruction: Instruction) : EG_Statement {
    private val pte: ProcTableEntry

    init {
        val x = instruction.getArg(0)

        assert(x is ProcIA)
        pte = gf.getProcTableEntry((x as ProcIA).index())
    }

    override val explanation: EX_Explanation
        get() = withMessage("GCX_FunctionCall >> action_CALL")

    override val text: String
        get() {
            val sb = StringBuilder()

            val ec = pte.expressionConfession()

            when (ec.type) {
                ECT.exp_num -> {
                    // FIME 07/20 why are we using expression in exp_num
                    val ptex = pte.__debug_expression as IdentExpression
                    val text = ptex.text
                    val xx = gf.vte_lookup(text)!!

                    val realTargetName = gc.getRealTargetName(gf, (xx as IntegerIA), AOG.GET)

                    sb.append(Emit.emit("/*424*/") + realTargetName)
                    sb.append('(')
                    val sl3 = gc.getArgumentStrings(gf, instruction)
                    sb.append(Helpers.String_join(", ", sl3))
                    sb.append(");")

                    val beg = EG_SingleStatement("(", EX_Explanation_withMessage("GCX_FunctionCall beg"))
                    val mid = EG_SingleStatement(Helpers.String_join(", ", sl3), EX_Explanation_withMessage("GCX_FunctionCall mid"))
                    val end = EG_SingleStatement(");", EX_Explanation_withMessage("GCX_FunctionCall end"))
                    val ind = false
                    val exp = EX_Explanation_withMessage("GCX_FunctionCall exp_num")

                    val est = EG_CompoundStatement(beg, mid, end, ind, exp)

                    val ss = est.text

                    SimplePrintLoggerToRemoveSoon.println_out_4(ss)
                }

                ECT.exp -> {
                    val reference = CReference(gc.repo(), gc.ce)
                    val ia2 = pte.expression_num as IdentIA
                    reference.getIdentIAPath(ia2, AOG.GET, null)
                    val sl3 = gc.getArgumentStrings(gf, instruction)
                    reference.args(sl3)
                    val path = reference.build()

                    reference.debugPath(ia2, path)

                    sb.append(Emit.emit("/*427-2*/") + path + ";")
                }

                else -> throw IllegalStateException("Unexpected value: " + ec.type)
            }
            if (false) {
                if (pte.expression_num == null) {
                    val ptex = pte.__debug_expression as IdentExpression
                    val text = ptex.text
                    val xx = gf.vte_lookup(text)!!
                    val realTargetName = gc.getRealTargetName(gf, (xx as IntegerIA),
                            AOG.GET)
                    sb.append(Emit.emit("/*424*/") + realTargetName)
                    sb.append('(')
                    val sl3 = gc.getArgumentStrings(gf, instruction)
                    sb.append(Helpers.String_join(", ", sl3))
                    sb.append(");")
                } else {
                    val reference = CReference(gc.repo(), gc.ce)
                    val ia2 = pte.expression_num as IdentIA
                    reference.getIdentIAPath(ia2, AOG.GET, null)
                    val sl3 = gc.getArgumentStrings(gf, instruction)
                    reference.args(sl3)
                    val path = reference.build()

                    reference.debugPath(ia2, path)

                    sb.append(Emit.emit("/*427-3*/") + path + ";")
                }
            }

            return sb.toString()
        }
}
