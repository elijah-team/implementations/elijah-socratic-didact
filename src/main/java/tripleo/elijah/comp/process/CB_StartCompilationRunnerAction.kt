package tripleo.elijah.comp.process

import lombok.Getter
import org.jetbrains.annotations.Contract
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.i.CB_Action
import tripleo.elijah.comp.i.CB_Monitor
import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.CB_Process
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.*
import tripleo.elijah.util.*

class CB_StartCompilationRunnerAction @Contract(pure = true) constructor(private val compilationRunner: CompilationRunner,
                                                                         private val pa: IPipelineAccess,
                                                                         private val rootCI: CompilerInstructions) : CB_Action, CB_Process {
    @Getter
    internal val o: CB_Output = pa.compilationEnclosure.cB_Output // new CB_Output();

    @Contract(value = " -> new", pure = true)
    fun cb_Process(): CB_Process {
        return this
    }

    override fun execute(monitor: CB_Monitor) {
        val compilationDriver = pa.compilationEnclosure.compilationDriver
        val ocrsd = compilationDriver[CompilationImpl.CompilationAlways.Tokens.COMPILATION_RUNNER_START]

        when (ocrsd.mode()) {
            Mode.SUCCESS -> {
                val compilationRunnerStart = ocrsd.success() as CD_CompilationRunnerStart
                val crState = compilationRunner.crState

//                assert(!CB_StartCompilationRunnerAction.started)

                // flux, flow, mono, eventual, lcm, ... ??
                if (CB_StartCompilationRunnerAction.started) {
                    //throw new AssertionError();
                    SimplePrintLoggerToRemoveSoon.println_err_4("twice for $this")
                } else {
                    startManager()!!.enjoin(compilationRunnerStart, rootCI, crState, o)
                }

                monitor.reportSuccess(this, o)
            }

            Mode.FAILURE, Mode.NOTHING -> {
                monitor.reportFailure(this, o)
                throw IllegalStateException("Error")
            }
        }
    }

    internal class _StartManager {
        fun enjoin(aCompilationRunnerStart: CD_CompilationRunnerStart, aRootCI: CompilerInstructions?, aCrState: CR_State?, aCBOutput: CB_Output?) {
            aCompilationRunnerStart.start(aRootCI!!, aCrState!!, aCBOutput!!)
            setStarted()
        }
    }

    @Contract(pure = true)
    override fun name(): String {
        return "StartCompilationRunnerAction"
    }

    override fun steps(): List<CB_Action> {
        return Helpers.List_of<CB_Action>(this@CB_StartCompilationRunnerAction)
    }

    companion object {
        internal var started = false
        private var _startManager: _StartManager? = null

        fun setStarted() {
            started = true
        }

        fun enjoin(aCompilationRunnerStart: CD_CompilationRunnerStart, aRootCI: CompilerInstructions?, aCRState: CR_State?, aCbOutput: CB_Output?) {
            startManager()!!.enjoin(aCompilationRunnerStart, aRootCI, aCRState, aCbOutput)
        }

        private fun startManager(): _StartManager? {
            if (_startManager == null) {
                _startManager = _StartManager()
            }
            return _startManager
        }
    }
}
