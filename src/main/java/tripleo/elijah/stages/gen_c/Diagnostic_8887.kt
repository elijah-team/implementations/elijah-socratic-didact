package tripleo.elijah.stages.gen_c

import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Locatable
import tripleo.elijah.lang.i.TypeName
import tripleo.elijah.stages.deduce.post_bytecode.GCFM_Diagnostic
import tripleo.elijah.util2.UnintendedUseException
import java.io.PrintStream

internal class Diagnostic_8887(private val y: TypeName) : GCFM_Diagnostic {
    val _code: Int = 8887

    override fun _message(): String {
        return String.format("%d VARIABLE WASN'T FULLY DEDUCED YET: %s", _code, y.javaClass.name)
    }

    override fun code(): String {
        return "" + _code
    }

    override fun primary(): Locatable {
        throw UnintendedUseException("just make it compile")
    }

    override fun report(stream: PrintStream) {
        stream.println(_message())
    }

    override fun secondary(): List<Locatable?> {
        throw UnintendedUseException("just make it compile")
    }

    override fun severity(): Diagnostic.Severity {
        return Diagnostic.Severity.ERROR
    }
}
