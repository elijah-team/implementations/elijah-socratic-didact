package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.FunctionDef.Species
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.util.Helpers0

internal object __Tests_BaseEvaFunction {
    fun testIsValue(gf: BaseEvaFunction?, name: String): Boolean {
        if (name != "Value") return false
        //
        val fd = gf!!.fd
        when (fd.species) {
            Species.REG_FUN, Species.DEF_FUN -> {
                if (fd.parent !is ClassStatement) return false
                for (anno in (fd.parent as ClassStatement?)!!.annotationIterable()) {
                    if (anno.annoClass() == Helpers0.string_to_qualident("Primitive")) {
                        return true
                    }
                }
                return false
            }

            Species.PROP_GET, Species.PROP_SET -> return true
            else -> throw IllegalStateException("Unexpected value: " + fd.species)
        }
    }

    fun testIsValue(yf: WhyNotGarish_Function, name: String): Boolean {
        return testIsValue(yf.gf, name)
    }
}
