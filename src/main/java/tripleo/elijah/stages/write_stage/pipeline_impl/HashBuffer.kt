package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.elijah.stages.gen_generic.DoubleLatch
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.Mode
import tripleo.util.buffer.DefaultBuffer
import java.util.concurrent.Executor

/*
* intent: HashBuffer
*  - contains 3 sub-buffers: hash, space, and filename
*  - has all logic to update and present hash
*    - codec: MTL sha2 here
*    - encoding: reg or multihash (hint hint...)
*/
class HashBuffer : DefaultBuffer {
    val dl: DoubleLatch<String> = DoubleLatch { aFilename: String? ->
        val outputBuffer = this
        val hh: String
        val hh2 = Helpers.getHashForFilename(aFilename!!)
        if (hh2.mode() == Mode.SUCCESS) {
            hh = hh2.success()

            if (hh != null) {
                outputBuffer.append(hh)
                outputBuffer.append(" ")
                outputBuffer.append_ln(aFilename)
            }
        } else {
            throw RuntimeException(hh2.failure())
        }
    }

    private val parent: HashBufferList?

    constructor(string: String) : super(string) {
        parent = null

        dl.notifyData(string)
    }

    // public String getText() {
    // dl.notifyLatch(true);
    //
    // return dl.
    // }
    constructor(aFileName: String, aHashBufferList: HashBufferList?, aExecutor: Executor?) : super("") {
        parent = aHashBufferList

        // parent.setNext(this);
        dl.notifyData(aFileName)
    }
}
