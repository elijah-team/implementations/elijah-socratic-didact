/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import org.jdeferred2.Deferred
import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.BaseFunctionDef
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.lang.impl.MatchConditionalImpl.MatchArm_TypeMatch__
import tripleo.elijah.lang.nextgen.names.impl.ENU_ClassName
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceClient3
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_fn.BaseTableEntry.StatusListener
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.util.Holder
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.ReadySupplier_1
import java.text.MessageFormat

/**
 * Created 7/8/21 2:31 AM
 */
class Resolve_Ident_IA {
    /**
     * Created 11/22/21 8:23 PM
     */
    class DeduceElementIdent(val identTableEntry: IdentTableEntry) : IDeduceElement_old {
        private val _resolvedElementPromise: Deferred<OS_Element?, ResolveError, Void> = DeferredObject()
        private var context: Context? = null
        private var generatedFunction: BaseEvaFunction? = null
        private var deduceTypes2: DeduceTypes2? = null

        init {
            // _resolvedElementPromise = identTableEntry.resolvedElementPromise;
            if (false) {
                if (identTableEntry.isResolved) {
                    resolveElement(identTableEntry.resolvedElement) // README hmm 06/19
                }
            }

            identTableEntry.addStatusListener { eh, newStatus ->
                if (newStatus == BaseTableEntry.Status.KNOWN) {
                    resolveElement(eh.element)
                }
            }
        }

        val resolvedElement: OS_Element?
            get() {
                if (deduceTypes2 == null) { // TODO remove this ASAP. Should never happen
                    SimplePrintLoggerToRemoveSoon.println_err_2("5454 Should never happen. gf is not deduced.")
                    return null

                    // throw new IllegalStateException("5454 Should never happen. gf is not
                    // deduced.");

                    // var x = identTableEntry.get_ident().identTableEntry().
                }

                val holder = Holder<OS_Element?>()

                var rp = false

                if (deduceTypes2!!.hasResolvePending(identTableEntry)) {
                    identTableEntry.elementPromise({ el: OS_Element? -> holder.set(el) }, null)
                    val dp = identTableEntry.buildDeducePath(generatedFunction)
                    val de3 = identTableEntry.getDeduceElement3(deduceTypes2,
                            generatedFunction)

                    de3.sneakResolve()

                    rp = true
                } else {
                    deduceTypes2!!.addResolvePending(identTableEntry, this, holder)

                    val de3 = identTableEntry.getDeduceElement3(deduceTypes2,
                            generatedFunction)
                    de3.sneakResolve()
                }

                val is_set = booleanArrayOf(false)

                if (!rp) {
                    val pe1 = deduceTypes2!!.promiseExpectation<OS_Element>(identTableEntry,
                            "DeduceElementIdent getResolvedElement")

                    //				assert _resolvedElementPromise.isResolved();
                    _resolvedElementPromise.then { e: OS_Element? ->
                        is_set[0] = true
                        holder.set(e)

                        pe1.satisfy(e!!)
                        deduceTypes2!!.LOG.info(
                                MessageFormat.format("DeduceElementIdent: found element for {0} {1}", identTableEntry, e))
                        deduceTypes2!!.removeResolvePending(identTableEntry)
                    }

                    // TODO when you get bored, remove this 07/20
                    /*
				 * deduceTypes2.resolveIdentIA_(context, this, generatedFunction,
				 * _inj().new_FoundElement(deduceTypes2.phase) {
				 * 
				 * @Override public void foundElement(final OS_Element e) { is_set[0] = true;
				 * holder.set(e);
				 * 
				 * pe1.satisfy(e); deduceTypes2.LOG.info(MessageFormat.
				 * format("DeduceElementIdent: found element for {0} {1}", identTableEntry, e));
				 * deduceTypes2.removeResolvePending(identTableEntry); }
				 * 
				 * @Override public void noFoundElement() { pe1.fail();
				 * deduceTypes2.LOG.err("DeduceElementIdent: can't resolve element for " +
				 * identTableEntry); deduceTypes2.removeResolvePending(identTableEntry); } });
				 */
                }

                //			assert is_set[0] == true;
                val R = holder.get()
                return R
            }

        fun resolvedElementPromise(): Promise<OS_Element?, ResolveError, Void> {
            return _resolvedElementPromise.promise()
        }

        fun resolveElement(aElement: OS_Element?) {
            if (!_resolvedElementPromise.isResolved) {
                _resolvedElementPromise.resolve(aElement)
                identTableEntry._p_resolvedElementPromise.resolve(aElement)
            }
        }

        fun setDeduceTypes2(aDeduceTypes2: DeduceTypes2?, aContext: Context?,
                            aGeneratedFunction: BaseEvaFunction) {
            deduceTypes2 = aDeduceTypes2
            context = aContext
            generatedFunction = aGeneratedFunction
        }
    }

    internal inner class GenericElementHolderWithDC(private val element: OS_Element, val dC: DeduceClient3) : IElementHolder {
        override fun getElement(): OS_Element {
            return element
        }
    }

    internal enum class RIA_STATE {
        CONTINUE, NEXT, RETURN
    }

    internal inner class StatusListener__RIA__176(private val y: IdentTableEntry, private val foundElement: FoundElement) : StatusListener {
        private val normal_path = generatedFunction.getIdentIAPathNormal(identIA)
        private val normal_path1: List<DT_Resolvable> = _getIdentIAResolvableList(identIA)
        var _called: Boolean = false

        override fun onChange(eh: IElementHolder, newStatus: BaseTableEntry.Status) {
            if (_called) return

            if (newStatus == BaseTableEntry.Status.KNOWN) {
                _called = true

                y.resolveExpectation?.satisfy(normal_path)
                //				dc.found_element_for_ite(generatedFunction, y, eh.getElement(), null); // No context
//				LOG.info("1424 Found for " + normal_path);
                foundElement.doFoundElement(eh.element)
            }
        }
    }

    private val context: Context
    private val dei: DeduceElementIdent
    private val errSink: ErrSink
    private val dc: DeduceClient3
    private val foundElement: FoundElement
    private val generatedFunction: BaseEvaFunction
    private val identIA: IdentIA
    private val LOG: ElLog

    private val phase: DeducePhase

    private val dt2: DeduceTypes2

    var ectx: Context? = null

    var el: OS_Element? = null

    constructor(aDeduceClient3: DeduceClient3, aContext: Context,
                aDei: DeduceElementIdent, aGeneratedFunction: BaseEvaFunction,
                aFoundElement: FoundElement, aErrSink: ErrSink) {
        dc = aDeduceClient3
        this.dt2 = dc.deduceTypes2

        phase = dc.phase
        context = aContext
        dei = aDei
        identIA = dt2._inj().new_IdentIA(dei.identTableEntry.index, aGeneratedFunction)
        generatedFunction = aGeneratedFunction
        foundElement = aFoundElement
        errSink = aErrSink
        //
        LOG = dc.log
    }

    @Contract(pure = true)
    constructor(aDeduceClient3: DeduceClient3, aContext: Context,
                aIdentIA: IdentIA, aGeneratedFunction: BaseEvaFunction,
                aFoundElement: FoundElement, aErrSink: ErrSink) {
        dc = aDeduceClient3
        dt2 = dc.deduceTypes2
        phase = dc.phase
        generatedFunction = aGeneratedFunction
        //
        errSink = aErrSink
        context = aContext
        //
        identIA = aIdentIA
        foundElement = aFoundElement
        //
        dei = identIA.entry.deduceElement
        dei.setDeduceTypes2(dc.deduceTypes2, context, generatedFunction)
        //
        LOG = dc.log
    }

    private fun _procIA_constructor_helper(pte: ProcTableEntry) {
        check(pte.classInvocation == null)

        if (pte.functionInvocation == null) {
            _procIA_constructor_helper_create_invocations(pte)
        } else {
            val fi = pte.functionInvocation
            val ci = fi?.classInvocation
            if (fi?.function is ConstructorDef) {
                val genType = dt2._inj().new_GenTypeImpl(ci!!.klass)
                genType.ci = ci
                ci.resolvePromise().then { result -> genType.node = result }
                val wl = dt2._inj().new_WorkList()
                val module = ci.klass.context.module()
                val generateFunctions = dc.getGenerateFunctions(module)
                if (pte.functionInvocation?.function === LangGlobals.defaultVirtualCtor) {
                    wl.addJob(dt2._inj().new_WlGenerateDefaultCtor(generateFunctions, fi,
                            dc.deduceTypes2.creationContext(), phase.codeRegistrar))
                } else {
                    wl.addJob(dt2._inj().new_WlGenerateCtor(generateFunctions, fi, null,
                            dc.deduceTypes2.phase.codeRegistrar))
                }
                dc.addJobs(wl)
                //				generatedFunction.addDependentType(genType);
//				generatedFunction.addDependentFunction(fi);
            }
        }
    }

    private fun _procIA_constructor_helper_create_invocations(pte: ProcTableEntry) {
        val dt2 = dc.deduceTypes2

        var ci = dt2._inj().new_ClassInvocation(el as ClassStatement?, null, ReadySupplier_1(dt2))

        ci = phase.registerClassInvocation(ci!!)
        //		prte.setClassInvocation(ci);
        val cs = ((el as ClassStatement?)!!.constructors)
        var selected_constructor: ConstructorDef? = null
        if (pte.args?.isEmpty()!! && cs.isEmpty()) {
            // TODO use a virtual default ctor
            LOG.info("2262 use a virtual default ctor for " + pte.__debug_expression)
            selected_constructor = LangGlobals.defaultVirtualCtor
        } else {
            // TODO find a ctor that matches prte.getArgs()
            val x = pte.args
            val yy = 2
        }
        assert((el as ClassStatement?)!!.genericPart.size == 0)
        val fi = phase.newFunctionInvocation(selected_constructor, pte, ci)
        //		fi.setClassInvocation(ci);
        pte.functionInvocation = fi
        if (fi.function is ConstructorDef) {
            val genType = dt2._inj().new_GenTypeImpl(ci.klass)
            genType.ci = ci
            ci.resolvePromise().then { result: EvaClass? -> genType.node = result }
            generatedFunction.addDependentType(genType)
            generatedFunction.addDependentFunction(fi)
        } else generatedFunction.addDependentFunction(fi)
    }

    fun action() {
        dei.resolvedElementPromise().then { el2: OS_Element? ->

            // final OS_Element el2 = dei.getResolvedElement();
            SimplePrintLoggerToRemoveSoon.println_out_2("  70 $el2")

            val s = BaseEvaFunction._getIdentIAPathList(identIA)

            ectx = context
            el = null

            if (!process(s[0], s)) return@then

            preUpdateStatus(s)
            updateStatus(s)
        }
    }

    private fun action_001(aAttached: OS_Type) {
        val dt2 = dc.deduceTypes2

        when (aAttached.type) {
            OS_Type.Type.USER_CLASS -> {
                val x = aAttached.classOf
                ectx = x.context
            }

            OS_Type.Type.FUNCTION -> {
                val yy = 2
                LOG.err("1005")
                val x = aAttached.element as FunctionDef
                ectx = x.context
            }

            OS_Type.Type.USER -> if (el is MatchArm_TypeMatch__) {
                // for example from match conditional
                val tn = (el as MatchArm_TypeMatch).typeName
                try {
                    val ty = dc.resolve_type(dt2._inj().new_OS_UserType(tn), tn.context)
                    ectx = ty.resolved.element.context
                } catch (resolveError: ResolveError) {
                    resolveError.printStackTrace()
                    LOG.err("1182 Can't resolve $tn")
                    throw IllegalStateException("ResolveError.")
                }
                //						ectx = el.getContext();
            } else ectx = aAttached.typeName.context // TODO is this right?

            OS_Type.Type.FUNC_EXPR -> {
                val funcExpr = aAttached.element as FuncExpr
                ectx = funcExpr.context
            }

            else -> {
                LOG.err("1010 " + aAttached.type)
                throw IllegalStateException("Don't know what you're doing here.")
            }
        }
    }

    private fun action_002(tte: TypeTableEntry) {
        // >ENTRY
        // assert vte.potentailTypes().size() == 1;
        assert(tte.attached == null)
        // <ENTRY
        if (tte.tableEntry is ProcTableEntry) {
            val pte = tte.tableEntry as ProcTableEntry
            val x = pte.expression_num as IdentIA
            val y = x.entry
            if (!y.hasResolvedElement()) {
                action_002_no_resolved_element(pte, y)
            } else {
                val res = y.resolvedElement
                val ite = identIA.entry
                action_002_1(pte, y, true)
            }
        } else throw IllegalStateException("tableEntry must be ProcTableEntry")
    }

    private fun action_002_1(pte: ProcTableEntry, ite: IdentTableEntry, setClassInvocation: Boolean = false) {
        val resolvedElement = ite.resolvedElement!!

        var ci: ClassInvocation? = null

        if (pte.functionInvocation == null) {
            val fi: FunctionInvocation

            if (resolvedElement is ClassStatement) {
                // assuming no constructor name or generic parameters based on function syntax
                ci = dt2._inj().new_ClassInvocation(resolvedElement, null, ReadySupplier_1(dt2))
                ci = phase.registerClassInvocation(ci)
                fi = phase.newFunctionInvocation(null, pte, ci)
            } else if (resolvedElement is FunctionDef) {
                val invocation = dc.getInvocation((generatedFunction as EvaFunction))
                fi = phase.newFunctionInvocation(resolvedElement, pte, invocation)
                if (fi.function!!.parent is ClassStatement) {
                    val classStatement = fi.function.parent as ClassStatement

                    ci = dt2._inj().new_ClassInvocation(classStatement, null, ReadySupplier_1<DeduceTypes2>(dt2)) // TODO
                    // generics
                    ci = phase.registerClassInvocation(ci)
                }
            } else {
                throw IllegalStateException()
            }

            if (setClassInvocation) {
                if (ci != null) {
                    pte.classInvocation = ci
                } else SimplePrintLoggerToRemoveSoon.println_err_2("542 Null ClassInvocation")
            }

            pte.functionInvocation = fi
        }

        el = resolvedElement
        ectx = el!!.context
    }

    @Throws(ResolveError::class)
    private fun action_002_2(pte: ProcTableEntry, ite: IdentTableEntry,
                             aAContext: Context) {
        val dt2 = dc.deduceTypes2

        val lrl2 = dc.lookupExpression(ite.ident.get(), aAContext)
        val best = lrl2.chooseBest(null)!!
        ite.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(best))
        action_002_1(pte, ite)
    }

    private fun action_002_no_resolved_element(pte: ProcTableEntry, ite: IdentTableEntry) {
        val dt2 = dc.deduceTypes2

        if (ite.backlink is ProcIA) {
            val backlink_ = ite.backlink as ProcIA
            val backlink = generatedFunction.getProcTableEntry(backlink_.index())
            val resolvedElement = backlink.resolvedElement!!
            try {
                val lrl2 = dc.lookupExpression(ite.ident.get(), resolvedElement.context)
                val best = lrl2.chooseBest(null)!!
                ite.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(best))
            } catch (aResolveError: ResolveError) {
                errSink.reportDiagnostic(aResolveError)
                assert(false)
            }
            action_002_1(pte, ite)
        } else if (ite.backlink is IntegerIA) {
            val backlink_ = ite.backlink as IntegerIA
            val backlink = backlink_.entry
            val resolvedElement = backlink.resolvedElement!!
            if (resolvedElement is IdentExpression) {
                backlink.typePromise().then { result: GenType ->
                    try {
                        val context1: Context = result.resolved.classOf.context
                        action_002_2(pte, ite, context1)
                    } catch (aResolveError: ResolveError) {
                        errSink.reportDiagnostic(aResolveError)
                    }
                }
            } else {
                try {
                    val context1 = resolvedElement.context
                    action_002_2(pte, ite, context1)
                } catch (aResolveError: ResolveError) {
                    errSink.reportDiagnostic(aResolveError)
                    assert(false)
                }
            }
        } else assert(false)
    }

    private fun action_IdentIA(ia: IdentIA): RIA_STATE {
        val dt2 = dc.deduceTypes2

        val idte = ia.entry
        if (idte.status == BaseTableEntry.Status.UNKNOWN) {
            LOG.info("1257 Not found for " + generatedFunction.getIdentIAPathNormal(ia))
            // No need checking more than once
            idte.resolveExpectation?.fail()
            foundElement.doNoFoundElement()
            return RIA_STATE.RETURN
        }

        // assert idte.backlink == null;
        if (idte.status == BaseTableEntry.Status.UNCHECKED) {
            val drIdent = generatedFunction.getIdent(idte)
            val ident_name = idte.ident.get().name

            if (ident_name.hasUnderstanding(ENU_ClassName::class.java)) {
                val y = 2
            }

            if (idte.backlink == null) {
                val text = idte.ident.text
                if (idte.resolvedElement == null) {
                    val lrl = ectx!!.lookup(text!!)
                    el = lrl.chooseBest(null)
                } else {
                    assert(false)
                    el = idte.resolvedElement
                }
                run {
                    if (el is FunctionDef) {
                        val parent = el?.getParent()
                        var genType: GenType? = null
                        var invocation: IInvocation? = null
                        when (DecideElObjectType.getElObjectType(parent!!)) {
                            ElObjectType.UNKNOWN -> {}
                            ElObjectType.CLASS -> {
                                genType = dt2._inj().new_GenTypeImpl(parent as ClassStatement?)
                                val ci = dt2._inj().new_ClassInvocation(parent as ClassStatement?, null,
                                        ReadySupplier_1(dt2))
                                invocation = phase.registerClassInvocation(ci!!)
                            }

                            ElObjectType.NAMESPACE -> {
                                genType = dt2._inj().new_GenTypeImpl(parent as NamespaceStatement?)
                                invocation = phase.registerNamespaceInvocation((parent as NamespaceStatement?)!!)
                            }

                            else -> {}
                        }
                        if (genType != null) {
                            generatedFunction.addDependentType(genType)

                            // TODO might not be needed
                            if (invocation != null) {
                                val fi = phase.newFunctionInvocation(el as BaseFunctionDef?, null,
                                        invocation)
                                //								generatedFunction.addDependentFunction(fi); // README program fails if this is included
                            }
                        }
                        val callablePTE = idte.callablePTE!!
                        val fi = dc.newFunctionInvocation(el as BaseFunctionDef?,
                                callablePTE, invocation!!)
                        if (invocation is ClassInvocation) {
                            callablePTE.classInvocation = invocation
                        }
                        callablePTE.functionInvocation = fi
                        generatedFunction.addDependentFunction(fi)
                    } else if (el is ClassStatement) {
                        val genType = dt2._inj().new_GenTypeImpl(el as ClassStatement?)
                        generatedFunction.addDependentType(genType)
                    }
                }
                if (el != null) {
                    idte.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(el))

                    checkNotNull(el!!.context) { "2468 null context" }

                    ectx = el!!.context
                } else {
                    errSink.reportError("1179 Can't resolve $text")
                    idte.setStatus(BaseTableEntry.Status.UNKNOWN, null)
                    foundElement.doNoFoundElement()
                    return RIA_STATE.RETURN
                }
            } else  /* if (false) */ {
                dc.resolveIdentIA2_(ectx!!,  /* context */ia, null, generatedFunction, object : FoundElement(phase) {
                    val z: String = generatedFunction.getIdentIAPathNormal(ia)

                    override fun foundElement(e: OS_Element?) {
                        if (e==null) return

                        idte.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(e))
                        foundElement.doFoundElement(e)
                        dc.found_element_for_ite(generatedFunction, idte, e, ectx)
                    }

                    override fun noFoundElement() {
                        foundElement.noFoundElement()
                        LOG.info("2002 Cant resolve $z")
                        idte.setStatus(BaseTableEntry.Status.UNKNOWN, null)
                    }
                })
            }
            //				assert idte.getStatus() != BaseTableEntry.Status.UNCHECKED;
            val normal_path = generatedFunction.getIdentIAPathNormal(identIA)
            if (idte.resolveExpectation == null) {
                SimplePrintLoggerToRemoveSoon.println_err_2("385 idte.resolveExpectation is null for $idte")
            } else idte.resolveExpectation?.satisfy(normal_path)
        } else if (idte.status == BaseTableEntry.Status.KNOWN) {
            val normal_path = generatedFunction.getIdentIAPathNormal(identIA)
            // assert idte.resolveExpectation.isSatisfied();
            if (!(idte.resolveExpectation?.isSatisfied!!)) idte.resolveExpectation?.satisfy(normal_path)

            el = idte.resolvedElement
            ectx = el!!.context
        }
        return RIA_STATE.NEXT
    }

    private fun action_IntegerIA(ia: InstructionArgument): RIA_STATE {
        val vte = (ia as IntegerIA).entry
        val text = vte.name
        val lrl = ectx!!.lookup(text)
        el = lrl.chooseBest(null)
        if (el != null) {
            //
            // TYPE INFORMATION IS CONTAINED IN VARIABLE DECLARATION
            //
            if (el is VariableStatement) {
                val el1 = el as VariableStatement
                if (!(el1.typeName().isNull)) {
                    ectx = el1.typeName().context
                    return RIA_STATE.CONTINUE
                }
            }
            //
            // OTHERWISE TYPE INFORMATION MAY BE IN POTENTIAL_TYPES
            //
            val pot = dc.getPotentialTypesVte(vte)
            if (pot.size == 1) {
                val attached = pot[0].attached
                if (attached != null) {
                    action_001(attached)
                } else {
                    action_002(pot[0])
                }
            }
        } else {
            errSink.reportError("1001 Can't resolve $text")
            foundElement.doNoFoundElement()
            return RIA_STATE.RETURN
        }
        return RIA_STATE.NEXT
    }

    private fun action_ProcIA(ia: InstructionArgument) {
        val dt2 = dc.deduceTypes2

        val prte = (ia as ProcIA).entry
        if (prte.resolvedElement == null) {
            var exp = prte.__debug_expression
            if (exp is ProcedureCallExpression) {
                exp = exp.getLeft() // TODO might be another pce??!!
                check(exp !is ProcedureCallExpression) { "double pce!" }
            } else throw IllegalStateException("prte resolvedElement not ProcCallExpression")
            try {
                val lrl = dc.lookupExpression(exp, ectx!!)
                el = lrl.chooseBest(null)
                assert(el != null)
                ectx = el!!.context
                //					prte.setResolvedElement(el);
                prte.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(el))
                // handle constructor calls
                if (el is ClassStatement) {
                    _procIA_constructor_helper(prte)
                }
            } catch (aResolveError: ResolveError) {
                aResolveError.printStackTrace()
                throw NotImplementedException()
            }
        } else {
            el = prte.resolvedElement
            ectx = el!!.context
        }
    }

    private fun preUpdateStatus(s: List<InstructionArgument>) {
        val dt2 = dc.deduceTypes2

        val normal_path1 = _getIdentIAResolvableList(identIA)
        val normal_path = generatedFunction.getIdentIAPathNormal(identIA)
        if (s.size > 1) {
            val x = s[s.size - 1]
            if (x is IntegerIA) {
                assert(false)
                val y = x.entry
                y.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(el))
            } else if (x is IdentIA) {
                val y = x.entry
                if (!y.preUpdateStatusListenerAdded) {
                    y.addStatusListener(dt2._inj().new_StatusListener__RIA__176(y, foundElement, this))
                    y.preUpdateStatusListenerAdded = true
                }
            }
        } else {
//			LOG.info("1431 Found for " + normal_path);
            foundElement.doFoundElement(el!!)
        }
    }

    @Contract("null, _ -> fail")
    private fun process(ia: InstructionArgument, aS: List<InstructionArgument>): Boolean {
        if (ia is IntegerIA) {
            val state = action_IntegerIA(ia)
            if (state == RIA_STATE.RETURN) {
                return false
            } else if (state == RIA_STATE.NEXT) {
                val identIA2 = identIA // (IdentIA) aS.get(1);
                val idte = identIA2.entry

                val dt2 = dc.deduceTypes2

                dc.resolveIdentIA2_(context, identIA2, aS, generatedFunction, object : FoundElement(phase) {
                    val z: String = generatedFunction.getIdentIAPathNormal(identIA2)
                    val zz: DT_Resolvabley = _getIdentIAResolvable(identIA2)

                    override fun foundElement(e: OS_Element?) {
                        idte.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(e))
                        foundElement.doFoundElement(e!!)
                    }

                    override fun noFoundElement() {
                        foundElement.noFoundElement()
                        LOG.info("2002 Cant resolve " + zz.getNormalPath(generatedFunction, identIA))
                        idte.setStatus(BaseTableEntry.Status.UNKNOWN, null)
                    }
                })
            }
        } else if (ia is IdentIA) {
            val state = action_IdentIA(ia)
            return state != RIA_STATE.RETURN
        } else if (ia is ProcIA) {
            action_ProcIA(ia)
        } else throw IllegalStateException("Really cant be here")
        return true
    }

    private fun updateStatus(aS: List<InstructionArgument>) {
        val dt2 = dc.deduceTypes2

        val x = aS[0]
        if (x is IntegerIA) {
            val y = x.entry
            if (el is VariableStatement) {
                val el1 = el as VariableStatement
                y.setStatus(BaseTableEntry.Status.KNOWN, dc.newGenericElementHolderWithType(el1, el1.typeName()))
            }
            y.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolderWithDC(el, dc, this))
        } else if (x is IdentIA) {
            val y = x.entry
            assert(y.status == BaseTableEntry.Status.KNOWN)//				y.setStatus(BaseTableEntry.Status.KNOWN, el);
        } else if (x is ProcIA) {
            val y = x.entry
            assert(y.status == BaseTableEntry.Status.KNOWN)
            y.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(el))
        } else throw NotImplementedException()
    }
}
