package tripleo.elijah.nextgen.inputtree

import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.util.Operation
import tripleo.elijah_elevated.comp.model.CM_Resource
import tripleo.elijah_elevated.comp.model.CM_ResourceCompute2

interface EIT_InputTree {
    fun addNode(i: CompilerInput?)

    fun setNodeOperation(input: CompilerInput, operation: Operation<*>?)

    fun addResourceNode(aResource: CM_Resource?, aCompute2: CM_ResourceCompute2?)
}
