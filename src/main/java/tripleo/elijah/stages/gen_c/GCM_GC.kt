package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.stages.gen_fn.EvaContainerNC
import tripleo.elijah.stages.gen_fn.IEvaConstructor
import tripleo.elijah.stages.gen_fn.TypeTableEntry
import tripleo.elijah.stages.gen_fn.VariableTableEntry
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.stages.logging.ElLog

internal class GCM_GC(private val gf: IEvaConstructor, private val LOG: ElLog, private val gc: GenerateC) : GCM_D {
    override fun find_return_type(aGenerate_method_header__: Generate_Method_Header): String {
        val type: OS_Type
        val tte: TypeTableEntry
        var returnType: String? = null

        val result_index = gf.vte_lookup("self")
        if (result_index is IntegerIA) {
            val vte: VariableTableEntry = result_index.entry
            assert(vte.vtt == VariableTableType.SELF)
            // Get it from resolved
            tte = gf.getTypeTableEntry(result_index.index)
            val res = tte.resolved()
            if (res is EvaContainerNC) {
                val code: Int = res.code
                return String.format("Z%d*", code)
            }

            // Get it from type.attached
            type = tte.attached!!

            LOG.info("228-1 $type")
            if (type.isUnitType) {
                assert(false)
            } else if (type != null) {
                returnType = String.format("/*267*/%s*", gc.getTypeName(type))
            } else {
                LOG.err("655 Shouldn't be here (type is null)")
                returnType = "void/*2*/"
            }

            return returnType!!
        } else {
            return "<<cant find self for ctor:57>>"
        }
    }
}
