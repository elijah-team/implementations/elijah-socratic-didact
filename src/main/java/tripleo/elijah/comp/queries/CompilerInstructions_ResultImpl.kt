package tripleo.elijah.comp.queries

import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.util.Operation2
import java.util.stream.Collectors

class CompilerInstructions_ResultImpl : CompilerInstructions_Result {
    private val directoryResult: MutableList<Pair<Operation2<CompilerInstructions>, QSEZ_Reasoning>> = ArrayList()

    override fun getDirectoryResult(): List<Operation2<CompilerInstructions>> {
        return directoryResult.stream()
                .map { it: Pair<Operation2<CompilerInstructions>, QSEZ_Reasoning> -> it.key }
                .collect(Collectors.toList())
    }

    override fun getDirectoryResult2(): List<Pair<Operation2<CompilerInstructions>, QSEZ_Reasoning>> {
        return directoryResult
    }

    override fun add(aCio: Operation2<CompilerInstructions>, aReasoning: QSEZ_Reasoning) {
        directoryResult.add(Pair.of(aCio, aReasoning))
    }
}
