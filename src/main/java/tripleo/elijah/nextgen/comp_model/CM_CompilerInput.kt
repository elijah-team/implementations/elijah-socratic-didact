package tripleo.elijah.nextgen.comp_model

import com.google.common.base.MoreObjects
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.Finally.Out2
import tripleo.elijah.comp.Finally_.FinallyInput_
import tripleo.elijah.comp.Finally_Input
import tripleo.elijah.comp.i.ILazyCompilerInstructions
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.nextgen.outputtree.EOT_Nameable
import tripleo.elijah.util.Maybe
import tripleo.wrap.File

class CM_CompilerInput(private val carrier: CompilerInput, private val comp: Compilation) : EOT_Nameable {
    var ty: CompilerInput.Ty? = null
        private set
    private var dir_carrier: File? = null
    private var hash: String? = null
    private var accept_ci: Maybe<ILazyCompilerInstructions>? = null

    fun inpSameAs(aS: String): Boolean {
        return aS == carrier.inp
    }

    //public void setSourceRoot() {
    //	ty = CompilerInput.Ty.SOURCE_ROOT;
    //}
    fun setDirectory(aF: File?) {
        ty = CompilerInput.Ty.SOURCE_ROOT
        dir_carrier = aF
    }

    //public void setArg() {
    //	ty = CompilerInput.Ty.ARG;
    //}
    fun accept_hash(aHash: String?) {
        this.hash = aHash
    }

    fun printableString(): String {
        val inp11 = this.nameableString
        return MoreObjects.toStringHelper(this)
                .add("ty", ty)
                .add("inp", inp11)
                .add("accept_ci", accept_ci.toString())
                .add("dir_carrier", dir_carrier)
                .add("hash", hash)
                .toString()
    }

    fun accept_ci(mci: Maybe<ILazyCompilerInstructions>?) {
        accept_ci = mci
    }

    fun acceptance_ci(): Maybe<ILazyCompilerInstructions>? {
        return accept_ci
    }

    val inp: String
        get() = this.nameableString

    fun fileOf(): File {
        val inp1 = this.nameableString
        return File(inp1)
    }

    fun onIsEz() {
        val ilci = comp.con().createLazyCompilerInstructions(carrier)

        val m4 = Maybe(ilci, null)
        carrier.accept_ci(m4)
    }

    fun createInput(aTy: Out2?): Finally_Input {
        return FinallyInput_(this, aTy!!)
    }

    override val nameableString: String
        get() = carrier.inp
}
