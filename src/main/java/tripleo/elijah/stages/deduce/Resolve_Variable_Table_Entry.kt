/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.contexts.IFunctionContext
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Diagnostic.withMessage
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.IdentExpressionImpl
import tripleo.elijah.lang.nextgen.names.i.EN_Name
import tripleo.elijah.lang.types.OS_FuncExprType
import tripleo.elijah.nextgen.outputstatement.EX_Explanation_withMessage
import tripleo.elijah.stages.deduce.DeduceTypes2.IVariableConnector
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action_Arena
import tripleo.elijah.stages.deduce.tastic.DT_External_2
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.util.Helpers0
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.Operation2
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Created 9/5/21 2:54 AM
 */
internal class Resolve_Variable_Table_Entry(private val generatedFunction: BaseEvaFunction, private val ctx: Context, private val deduceTypes2: DeduceTypes2) {
    private val errSink: ErrSink = deduceTypes2._errSink()

    //
    private val LOG = deduceTypes2._LOG()
    private val phase = deduceTypes2._phase()
    private val wm = deduceTypes2.wm

    fun action(vte: VariableTableEntry, aConnector: IVariableConnector) {
        when (vte.vtt) {
            VariableTableType.ARG -> action_ARG(vte)
            VariableTableType.VAR -> action_VAR(vte)
            else -> {
                throw Error()
            }
        }
        aConnector.connect(vte, vte.name)
    }

    private fun action_ARG(vte: VariableTableEntry) {
        val tte = vte.typeTableEntry
        val attached = tte.attached
        if (attached != null) {
            val genType = tte.genType
            when (attached.type) {
                OS_Type.Type.USER -> {
                    if (genType.typeName == null) genType.typeName = attached
                    try {
                        if (attached.typeName is RegularTypeName) {
                            val rtn = attached.typeName as RegularTypeName
                            if (rtn.getGenericPart() != null) {
                                // README Can't resolve generic typenames, need a classInvocation ...
                                // 08/13 tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("===== future 8181");
                                return
                            }
                        }
                        if (attached.typeName is FuncTypeName) {
                            val ftn = attached.typeName as FuncTypeName
                            if (ftn.argListIsGeneric() || ftn.hasParts()) {
                                // README Can't resolve generic typenames, need a classInvocation ...
                                // 08/13 tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("===== future 8181");
                                return
                            }
                        }

                        genType.copy(deduceTypes2.resolve_type(attached, ctx))
                        tte.attached = genType.resolved // TODO probably not necessary, but let's leave it for now
                    } catch (aResolveError: ResolveError) {
                        errSink.reportDiagnostic(aResolveError)
                        LOG.err("Can't resolve argument type $attached")
                        return
                    }
                    if (generatedFunction.fi?.classInvocation != null) genNodeForGenType(genType, generatedFunction.fi?.classInvocation)
                    else genCIForGenType(genType)
                    vte.resolveType(genType)
                }

                OS_Type.Type.USER_CLASS -> {
                    if (genType.resolved == null) genType.resolved = attached
                    // TODO genCI and all that -- Incremental?? (.increment())
                    vte.resolveType(genType)
                    genCIForGenType2(genType)
                }

                else -> {
                    throw Error()
                }
            }
        } else {
            val y = 2
        }
    }

    private fun action_VAR(vte: VariableTableEntry) {
        if (vte.typeTableEntry.attached == null && vte.potentialTypes.size == 1) {
            val pot = deduceTypes2._inj().new_ArrayList__TypeTableEntry(vte.potentialTypes())[0]
            if (pot.attached is OS_FuncExprType) {
                action_VAR_potsize_1_and_FuncExprType(vte, (pot.attached as OS_FuncExprType?)!!, pot.genType,
                    pot.__debug_expression)
            } else if (pot.attached != null && pot.attached!!.type == OS_Type.Type.USER_CLASS) {
                val y = 1
                vte.setType(pot)
                vte.resolveType(pot.genType)
            } else {
                action_VAR_potsize_1_other(vte, pot)
            }
        }
    }

    private fun action_VAR_potsize_1_and_FuncExprType(vte: VariableTableEntry,
                                                      funcExprType: OS_FuncExprType, aGenType: GenType, aPotentialExpression: IExpression) {
        aGenType.typeName = funcExprType

        val fe = funcExprType.element as FuncExpr

        // add namespace
        val mod1 = fe.context.module()
        val nso = lookup_module_namespace(mod1)
        val mod_ns = nso.success()

        var callable_pte: ProcTableEntry? = null

        if (mod_ns != null) {
            // add func_expr to namespace
            val fd1 = deduceTypes2._inj().new_FunctionDefImpl(mod_ns, mod_ns.context)
            fd1.setFal(fe.fal())
            fd1.setContext(fe.context as IFunctionContext)
            fd1.scope(fe.scope)
            fd1.species = FunctionDef.Species.FUNC_EXPR
            //			tripleo.elijah.util.Stupidity.println_out_2("1630 "+mod_ns.getItems()); // element 0 is ctor$0
            fd1.setName(IdentExpression_forString(String.format("$%d", mod_ns.items.size + 1)))

            val wl = deduceTypes2._inj().new_WorkList()
            val gen = phase.generatePhase.getGenerateFunctions(mod1)
            val modi = deduceTypes2._inj().new_NamespaceInvocation(mod_ns)
            val pte = findProcTableEntry(generatedFunction, aPotentialExpression)!!
            callable_pte = pte
            val fi = phase.newFunctionInvocation(fd1, pte, modi)
            wl.addJob(deduceTypes2._inj().new_WlGenerateNamespace(gen, modi, phase.generatedClasses,
                phase.codeRegistrar)) // TODO hope this works (for more than one)
            val wlgf = deduceTypes2._inj().new_WlGenerateFunction(gen, fi,
                phase.codeRegistrar)
            wl.addJob(wlgf)
            wm.addJobs(wl)
            wm.drain() // TODO here?

            aGenType.ci = modi
            aGenType.node = wlgf!!.result

            val pe = deduceTypes2
                .promiseExpectation<GenType>( /* pot.genType.node */{
                    "FuncType..." // TODO
                }, "FuncType Result")
            (aGenType.node as EvaFunction).typePromise().then { result ->
                pe.satisfy(result)
                vte.resolveType(result)
            }

            // vte.typeDeferred().resolve(pot.genType); // this is wrong
        }

        if (callable_pte != null) vte.callablePTE = (callable_pte)
    }

    private fun action_VAR_potsize_1_other(vte: VariableTableEntry, aPot: TypeTableEntry) {
        try {
            if (aPot.tableEntry is ProcTableEntry) {
                val pte1 = aPot.tableEntry
                val debugExpression: IExpression = pte1.__debug_expression!!

                var llext: DT_External_2? = null

                if (debugExpression is DotExpression) {
                    val left = debugExpression.getLeft()
                    if (left is ProcedureCallExpression) {
                        val leftlfeft = left.getLeft()
                        if (leftlfeft is IdentExpression) {
                            val llname: EN_Name = leftlfeft.name

                            llext = deduceTypes2._inj().new_DT_External_2(
                                pte1.evaExpression?.entry as IdentTableEntry,
                                leftlfeft.context.module(),
                                pte1,
                                null,
                                LOG,
                                ctx,
                                generatedFunction,
                                -1,
                                null,
                                vte)

                            deduceTypes2.addExternal(llext)
                        }
                    }
                }

                val e = DeduceLookupUtils.lookup(debugExpression, ctx, deduceTypes2)

                // 05/10
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                // assert e != null;
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                //
                if (e != null) {
                    val de3_vte = deduceTypes2.zeroGet(vte, generatedFunction)
                    de3_vte.__action_vp1o(vte, aPot, pte1, e)
                } else {
                    llext?.onResolve { el: OS_Element? ->
                        val de3_vte = deduceTypes2.zeroGet(vte,
                            generatedFunction)
                        de3_vte.__action_vp1o(vte, aPot, pte1, el!!)
                    }

                    SimplePrintLoggerToRemoveSoon.println_err_4("118118 Making external for $debugExpression")
                }
            } else if (aPot.tableEntry == null) {
                val el = vte.resolvedElement
                if (el is VariableStatement) {
                    val de3_vte = deduceTypes2.zeroGet(vte, generatedFunction)
                    de3_vte.__action_VAR_pot_1_tableEntry_null(el)
                }
            } else throw NotImplementedException()
        } catch (aResolveError: ResolveError) {
            errSink.reportDiagnostic(aResolveError)
        }
    }

    private fun findProcTableEntry(aGeneratedFunction: BaseEvaFunction,
                                   aExpression: IExpression): ProcTableEntry? {
        for (procTableEntry in aGeneratedFunction.prte_list) {
            if (procTableEntry.__debug_expression === aExpression) return procTableEntry
        }
        return null
    }

    /**
     * Sets the invocation (`genType#ci`) and the node for a GenType
     *
     * @param aGenType the GenType to modify. must be set to a nonGenericTypeName
     * that is non-null and generic
     */
    private fun genCIForGenType(aGenType: GenType) {
        // assert aGenType.getNonGenericTypeName() != null;//&& ((NormalTypeName)
        // aGenType.nonGenericTypeName).getGenericPart().size() > 0;

        if (aGenType.nonGenericTypeName == null) return

        aGenType.genCI(aGenType.nonGenericTypeName, deduceTypes2, deduceTypes2._errSink(), deduceTypes2.phase)
        val invocation = aGenType.ci
        if (invocation is NamespaceInvocation) {
            invocation.resolveDeferred().then { result: EvaNamespace? -> aGenType.node = result }
        } else if (invocation is ClassInvocation) {
            invocation.resolvePromise().then { result: EvaClass? -> aGenType.node = result }
        } else throw IllegalStateException("invalid invocation")
    }

    /**
     * Sets the invocation (`genType#ci`) and the node for a GenType
     *
     * @param aGenType the GenType to modify. doesn;t care about nonGenericTypeName
     */
    private fun genCIForGenType2(aGenType: GenType) {
        val list: List<setup_GenType_Action> = ArrayList()
        val arena = setup_GenType_Action_Arena()

        aGenType.genCI(aGenType.nonGenericTypeName, deduceTypes2, deduceTypes2._errSink(), deduceTypes2.phase)
        val invocation = aGenType.ci
        if (invocation is NamespaceInvocation) {
            invocation.resolveDeferred().then { result: EvaNamespace? -> aGenType.node = result }
        } else if (invocation is ClassInvocation) {
            invocation.resolvePromise().then { result: EvaClass? -> aGenType.node = result }
        } else throw IllegalStateException("invalid invocation")

        for (action in list) {
            action.run(aGenType, arena)
        }
    }

    /**
     * Sets the node for a GenType, given an invocation
     *
     * @param aGenType the GenType to modify. must be set to a nonGenericTypeName
     * that is non-null and generic
     */
    private fun genNodeForGenType(aGenType: GenType, invocation: IInvocation?) {
        // assert aGenType.getNonGenericTypeName() != null;

        assert(aGenType.ci == null || aGenType.ci === invocation)
        aGenType.ci = invocation
        if (invocation is NamespaceInvocation) {
            invocation.resolveDeferred().then { aResult: EvaNamespace? -> aGenType.node = aResult }
        } else if (invocation is ClassInvocation) {
            invocation.resolvePromise().then { aResult: EvaClass? -> aGenType.node = aResult }
        } else throw IllegalStateException("invalid invocation")
    }

    private fun lookup_module_namespace(aModule: OS_Module): Operation2<NamespaceStatement> {
        try {
            val module_ident = IdentExpressionImpl.forString("__MODULE__")
            val e = DeduceLookupUtils.lookup(module_ident, aModule.context, deduceTypes2)
            if (e != null) {
                if (e is NamespaceStatement) {
                    return Operation2.success(e)
                } else {
                    LOG.err("__MODULE__ should be namespace")
                    return Operation2.failure(withMessage("9328", "__MODULE__ should be namespace",
                        Diagnostic.Severity.ERROR))
                }
            } else {
                // not found, so add. this is where AST would come in handy
                // TODO 08/13 this looks wrong
                val ns = deduceTypes2._inj().new_NamespaceStatementImpl(aModule, aModule.context)
                ns.setName(module_ident)
                return Operation2.success(ns)
            }
        } catch (aResolveError: ResolveError) {
            errSink.reportDiagnostic(aResolveError)
            return Operation2.failure(aResolveError)
        }
    }
}

private fun IdentExpression_forString(string: String): IdentExpression {
//        return IdentExpressionImpl.forString(string)
    return IdentExpressionImpl(Helpers0.makeToken(string), "<inline-absent2>")
}
