package tripleo.elijah.stages.deduce.tastic

import org.jdeferred2.Promise
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.IntegerIA

class FT_FCA_VariableStatement(private val vs: VariableStatementImpl,
                               private val generatedFunction: BaseEvaFunction) {
    fun _FunctionCall_Args_doLogic0(vte: VariableTableEntry,
                                    vte1: VariableTableEntry, e_text: String,
                                    p: Promise<GenType?, Void?, Void?>) {
        assert(vs.name == e_text)
        val vte2_ia = generatedFunction.vte_lookup(vs.name)!!

        val vte2 = (vte2_ia as IntegerIA).entry
        if (p.isResolved) System.out.printf("915 Already resolved type: vte2.type = %s, gf = %s %n", vte1.typeTableEntry,
                generatedFunction)
        else {
            val gt = vte1.genType
            gt.resolved = vte2.typeTableEntry.attached
            vte1.resolveType(gt)
        }

        //			vte.type = vte2.type;
//			tte.attached = vte.type.attached;
        vte.setStatus(BaseTableEntry.Status.KNOWN, GenericElementHolder(vs))
        vte2.setStatus(BaseTableEntry.Status.KNOWN, GenericElementHolder(vs)) // TODO ??
    }
}
