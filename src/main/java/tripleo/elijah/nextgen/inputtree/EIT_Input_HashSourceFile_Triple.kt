package tripleo.elijah.nextgen.inputtree

import tripleo.elijah.comp.IO._IO_ReadFile
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.Mode
import java.util.*

class EIT_Input_HashSourceFile_Triple
(private val hash: String?, private val source: EIT_SourceOrigin, private val filename: String) : EIT_Input {
    override val type: EIT_InputType
        get() {
            // builder?? memtc st pat
            if (filename.endsWith(".elijah")) {
                return EIT_InputType.ELIJAH_SOURCE
            }
            if (filename.endsWith(".ez")) {
                return EIT_InputType.EZ_FILE
            }
            throw IllegalStateException("Unexpected value $filename")
        }

    fun hash(): String? {
        return hash
    }

    fun source(): EIT_SourceOrigin {
        return source
    }

    fun filename(): String {
        return filename
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as EIT_Input_HashSourceFile_Triple
        return this.hash == that.hash && (this.source == that.source) && (this.filename == that.filename)
    }

    override fun hashCode(): Int {
        return Objects.hash(hash, source, filename)
    }

    override fun toString(): String {
        return "EIT_Input_HashSourceFile_Triple[" +
                "hash=" + hash + ", " +
                "source=" + source + ", " +
                "filename=" + filename + ']'
    }

    companion object {
        fun decode(fn: String): EIT_Input_HashSourceFile_Triple {
            // move to Builder...Operation...
            // also CP_Filename hashPromise products
            val op2 = Helpers.getHashForFilename(fn)

            if (op2.mode() == Mode.SUCCESS) {
                val hh = op2.success()!!

                // TODO EG_Statement here
                val x = if (fn == "lib_elijjah/lib-c/Prelude.elijjah") {
                    EIT_SourceOrigin.PREL
                } else if (fn.startsWith("lib_elijjah/")) {
                    EIT_SourceOrigin.LIB
                } else if (fn.startsWith("test/")) {
                    EIT_SourceOrigin.SRC
                } else {
                    throw IllegalStateException("Error") // Operation??
                }

                val yy2 = EIT_Input_HashSourceFile_Triple(hh, x, fn)
                return yy2
            }
            throw IllegalStateException("hash failure") // Operation??
        }

        @JvmStatic
        fun decode(aFile: _IO_ReadFile): EIT_Input_HashSourceFile_Triple {
            val fn = aFile.fileName
            val op2 = aFile.hash()

            if (op2.mode() == Mode.SUCCESS) {
                val hh = op2.success()!!
                val x = aFile.sourceOrigin

                // TODO EG_Statement here
                val yy2 = EIT_Input_HashSourceFile_Triple(hh, x, fn)
                return yy2
            }
            throw IllegalStateException("hash failure") // Operation??
        }
    }
}
