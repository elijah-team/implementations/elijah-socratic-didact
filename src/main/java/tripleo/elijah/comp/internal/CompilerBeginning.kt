package tripleo.elijah.comp.internal

import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.Compilation.CompilationConfig
import tripleo.elijah.comp.i.IProgressSink
import tripleo.elijah.comp.inputs.CompilerInput
import java.util.*

class CompilerBeginning(private val compilation: Compilation, private val compilerInstructions: CompilerInstructions,
                        private val compilerInput: List<CompilerInput>, private val progressSink: IProgressSink, private val cfg: CompilationConfig) {
    fun compilation(): Compilation {
        return compilation
    }

    fun compilerInstructions(): CompilerInstructions {
        return compilerInstructions
    }

    fun compilerInput(): List<CompilerInput> {
        return compilerInput
    }

    fun progressSink(): IProgressSink {
        return progressSink
    }

    fun cfg(): CompilationConfig {
        return cfg
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as CompilerBeginning
        return this.compilation == that.compilation && (this.compilerInstructions == that.compilerInstructions) && (this.compilerInput == that.compilerInput) && (this.progressSink == that.progressSink) && (this.cfg == that.cfg)
    }

    override fun hashCode(): Int {
        return Objects.hash(compilation, compilerInstructions, compilerInput, progressSink, cfg)
    }

    override fun toString(): String {
        return "CompilerBeginning[" +
                "compilation=" + compilation + ", " +
                "compilerInstructions=" + compilerInstructions + ", " +
                "compilerInput=" + compilerInput + ", " +
                "progressSink=" + progressSink + ", " +
                "cfg=" + cfg + ']'
    }
}
