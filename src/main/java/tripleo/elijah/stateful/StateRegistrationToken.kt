package tripleo.elijah.stateful

import org.jetbrains.annotations.Contract
import java.util.*

class StateRegistrationToken internal constructor(private val token: Int) {
    fun token(): Int {
        return token
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as StateRegistrationToken
        return this.token == that.token
    }

    override fun hashCode(): Int {
        return Objects.hash(token)
    }

    @Contract(pure = true)
    override fun toString(): String {
        return "StateRegistrationToken[" +
                "token=" + token + ']'
    }
}
