package tripleo.elijah.stages.deduce

import tripleo.elijah.lang.i.NormalTypeName
import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.lang.i.VariableStatement
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_IdentTableEntry
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_IdentTableEntry.DE3_EH_GroundedVariableStatement
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.*

class DTR_VariableStatement(private val deduceTypeResolve: DeduceTypeResolve,
                            private val variableStatement: VariableStatement) {
    internal class DTR_VS_Ctx(private val eh: IElementHolder, private val genType: GenType, private val normalTypeName: NormalTypeName) {
        fun eh(): IElementHolder {
            return eh
        }

        fun genType(): GenType {
            return genType
        }

        fun normalTypeName(): NormalTypeName {
            return normalTypeName
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as DTR_VS_Ctx
            return this.eh == that.eh && (this.genType == that.genType) && (this.normalTypeName == that.normalTypeName)
        }

        override fun hashCode(): Int {
            return Objects.hash(eh, genType, normalTypeName)
        }

        override fun toString(): String {
            return "DTR_VS_Ctx[" +
                    "eh=" + eh + ", " +
                    "genType=" + genType + ", " +
                    "normalTypeName=" + normalTypeName + ']'
        }
    }

    private fun normalTypeName_generic_butNotNull(ctx: DTR_VS_Ctx) {
        val eh = ctx.eh()
        val genType = ctx.genType()
        val normalTypeName = ctx.normalTypeName()

        var dt2: DeduceTypes2? = null
        if (eh is GenericElementHolderWithType) {
            dt2 = eh.deduceTypes2
            val type: OS_Type = eh.type

            genType.typeName = dt2._inj().new_OS_UserType(normalTypeName)
            try {
                val resolved = dt2.resolve_type(genType.typeName,
                        variableStatement.context)
                if (resolved.resolved.type == OS_Type.Type.GENERIC_TYPENAME) {
                    val backlink = deduceTypeResolve.backlink

                    normalTypeName_generic_butNotNull_resolveToGeneric(genType, resolved, backlink!!)
                } else {
                    normalTypeName_generic_butNotNull_resolveToNonGeneric(genType, resolved)
                }
            } catch (aResolveError: ResolveError) {
                aResolveError.printStackTrace()
                assert(false)
            }
        } else if (eh is DeduceElement3Holder) {
            NotImplementedException.raise()
        } else {
            assert(dt2 != null // README 12/29 born to fail
            )
            genType.typeName = dt2!!._inj().new_OS_UserType(normalTypeName)
        }
    }

    private /* static */ fun normalTypeName_generic_butNotNull_resolveToGeneric(genType: GenType,
                                                                                resolved: GenType, backlink: BaseTableEntry) {
        backlink.typeResolvePromise().then { result_gt: GenType? ->
            (backlink as Constructable).constructablePromise()
                    .then { result_pte: ProcTableEntry ->
                        val ci = result_pte.classInvocation!!
                        val gp = ci.genericPart().map
                        val sch = resolved.typeName.typeName

                        assert(gp != null)
                        for ((key, value) in gp!!) {
                            if (key == sch) {
                                genType.resolved = value
                                break
                            }
                        }
                    }
        }
    }

    private /* static */ fun normalTypeName_generic_butNotNull_resolveToNonGeneric(genType: GenType,
                                                                                   resolved: GenType) {
        genType.resolved = resolved.resolved
    }

    private fun normalTypeName_notGeneric(ctx: DTR_VS_Ctx) {
        val eh = ctx.eh()
        val genType = ctx.genType()
        val normalTypeName = ctx.normalTypeName()

        val genericPart = normalTypeName.genericPart
        if (eh is GenericElementHolderWithType) {
            normalTypeName_notGeneric_typeProvided(ctx)
        } else normalTypeName_notGeneric_typeNotProvided(ctx)
    }

    private /* static */ fun normalTypeName_notGeneric_typeNotProvided(ctx: DTR_VS_Ctx) {
        val genType = ctx.genType()
        val normalTypeName = ctx.normalTypeName()

        genType.nonGenericTypeName = normalTypeName
    }

    private fun normalTypeName_notGeneric_typeProvided(ctx: DTR_VS_Ctx) {
        val genType = ctx.genType()
        val normalTypeName = ctx.normalTypeName()

        val eh1 = ctx.eh() as GenericElementHolderWithType
        val dt2 = eh1.deduceTypes2
        val type = eh1.type

        genType.nonGenericTypeName = normalTypeName

        assert(normalTypeName === type.typeName)
        val typeName: OS_Type = dt2._inj().new_OS_UserType(normalTypeName)
        try {
            val resolved = dt2.resolve_type(typeName, variableStatement.context)
            genType.resolved = resolved.resolved
        } catch (aResolveError: ResolveError) {
            aResolveError.printStackTrace()
            assert(false)
        }
    }

    fun run(dt2: DeduceTypes2, eh: IElementHolder, genType: GenType) {
        val typeName1 = variableStatement.typeName()

        check(typeName1 is NormalTypeName)

        var state = 0

        if (typeName1.genericPart != null) {
            state = 1
        } else {
            if (!typeName1.isNull()) {
                state = 2
            }
        }

        // DTR_VS_Ctx ctx = _inj().new_DTR_VS_Ctx(eh, genType, normalTypeName);
        val ctx = DTR_VS_Ctx(eh, genType, typeName1)

        when (state) {
            1 -> normalTypeName_notGeneric(ctx)
            2 -> normalTypeName_generic_butNotNull(ctx)
            else -> {
                if (eh is DE3_EH_GroundedVariableStatement) {
                    val ground: DeduceElement3_IdentTableEntry = eh.ground

                    val bl = ground.principal.backlink
                    if (bl is ProcIA) {
                        val pte_bl: ProcTableEntry = bl.entry

                        assert(pte_bl.status == BaseTableEntry.Status.KNOWN)
                        pte_bl.typeResolvePromise().then { gt: GenType ->
                            assert(dt2 === ground.principal._deduceTypes2())
                            gt.genCIForGenType2(ground.principal._deduceTypes2()) // README any will do

                            assert(gt.ci != null)
                            assert(gt.node != null)
                            for (entry in (gt.node as EvaContainerNC).varTable) {
                                if (entry.nameToken.text == variableStatement.name) {
                                    entry.resolve_varType_cb { result: OS_Type ->
                                        val y = 2
                                        SimplePrintLoggerToRemoveSoon.println_err_4("7676 DTR_VariableStatement >> $result")
                                    }
                                    break
                                }
                            }
                        }

                        val re1 = pte_bl.resolvedElement

                        val lrl = re1!!.context.lookup(variableStatement.name)
                        val e2 = lrl.chooseBest(null)

                        if (e2 == null) {
                            val y = 2
                        } else {
                            val y = 2
                        }
                    }
                } else {
                    SimplePrintLoggerToRemoveSoon
                            .println_err("Unexpected value: " + state + "for " + variableStatement.name)
                }
            }
        }
    }
}
