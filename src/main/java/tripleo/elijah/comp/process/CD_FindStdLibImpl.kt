package tripleo.elijah.comp.process

import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.i.CD_FindStdLib
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.comp.nextgen.wonka.CK_SourceFileFactory
import tripleo.elijah.g.GCR_State
import tripleo.elijah.util.Mode
import tripleo.elijah.util.Operation2
import java.util.function.Consumer

class CD_FindStdLibImpl : CD_FindStdLib {
    private var foundResult: Operation2<CompilerInstructions>? = null

    override fun findStdLib(crState: GCR_State, aPreludeName: String, coci: Consumer<Operation2<CompilerInstructions>>) {
        findStdLib(crState as CR_State, aPreludeName, coci)
    }

    //@Override
    fun findStdLib(crState: CR_State,
                   aPreludeName: String,
                   coci: Consumer<Operation2<CompilerInstructions>>) {
        val compilationRunner = crState.runner()
        val cc = compilationRunner!!._accessCompilation().compilationClosure
        val slr = cc.compilation.paths().stdlibRoot()
        val pl = slr.child("lib-$aPreludeName")
        val sle = pl.child("stdlib.ez")

        var result: Operation2<CompilerInstructions>? = null
        try {
            val local_stdlib = sle.toFile()
            cc.compilation.compilationEnclosure.logProgress(CompProgress.DriverPhase, Pair.of(3939, "" + local_stdlib))

            if (local_stdlib.exists()) {
                try {
                    //final CK_SourceFile<CompilerInstructions> sourceFile2 = sle.getSourceFile();
                    val sourceFile2 = CK_SourceFileFactory.get(sle, CK_SourceFileFactory.K.SpecifiedPathEzFile)
                    sourceFile2.associate(cc)

                    result = sourceFile2.process_query()

                    assert(result != null)
                    assert(result?.mode() == Mode.SUCCESS)
                } catch (e: Exception) {
                    result = Operation2.failure_exc(e)
                }
            }
/*
            if (result == null) {
                throw NeverReached()

                //				result = Operation.failure(new Exception("No stdlib found"));
            }
*/
        } catch (aE: Exception) {
            result = Operation2.failure_exc(aE)
        }

        foundResult = result
        coci.accept(result!!)
    }

    override fun maybeFoundResult(): CompilerInstructions? {
        if (foundResult!!.mode() == Mode.SUCCESS) return foundResult!!.success()
        return null
    }
}

internal class NeverReached : RuntimeException()
