package tripleo.elijah.stages.gen_c

import tripleo.elijah.nextgen.reactive.Reactivable
import tripleo.elijah.nextgen.reactive.ReactiveDimension
import tripleo.elijah.stages.gen_c.GenerateC.GenerateResultProgressive
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaFunction
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.util.buffer.Buffer

class GCFM(aRs: List<C2C_Result?>, private val gf: DeducedBaseEvaFunction, private val gr: GenerateResult?) : Reactivable {
    private var buf: Buffer? = null
    private var bufHdr: Buffer? = null

    init {
        for (r in aRs) {
            // TODO store a Map<TY, Buffer/*GRI??*/> in rs
            when (r!!.ty()) {
                TY.HEADER -> buf = r.buffer
                TY.IMPL -> bufHdr = r.buffer
                else -> throw IllegalStateException()
            }
        }
    }

    override fun respondTo(aDimension: ReactiveDimension?) {
        if (aDimension is GenerateC) {
            val lsp = gf.module__.lsp

            val gf1 = gf.carrier as BaseEvaFunction

            gr!!.addFunction(gf1, buf, TY.IMPL, lsp)
            gr.addFunction(gf1, bufHdr, TY.HEADER, lsp)

            val gr2: GenerateResultProgressive = aDimension.generateResultProgressive()
            gr2.addFunction(gf1 as EvaFunction, buf, TY.IMPL)
            gr2.addFunction(gf1, bufHdr, TY.HEADER)
        }
    }
}
