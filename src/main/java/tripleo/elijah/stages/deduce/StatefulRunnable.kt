package tripleo.elijah.stages.deduce

import tripleo.elijah.stateful.DefaultStateful

internal class StatefulRunnable(private val runnable: Runnable) : DefaultStateful(), IStateRunnable {
    override fun run() {
        runnable.run()
    }
}
