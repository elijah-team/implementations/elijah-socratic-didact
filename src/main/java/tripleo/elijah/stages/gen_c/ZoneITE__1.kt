package tripleo.elijah.stages.gen_c

import com.google.common.base.Preconditions
import tripleo.elijah.lang.i.ConstructorDef
import tripleo.elijah.lang.i.PropertyStatement
import tripleo.elijah.lang.i.VariableStatement
import tripleo.elijah.nextgen.outputstatement.IReasonedString
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaContainerNC
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_fn.IdentTableEntry
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.util.Helpers
import tripleo.elijah.util2.UnintendedUseException
import java.util.*

class ZoneITE__1(private val identTableEntry: IdentTableEntry, private val gf: BaseEvaFunction, private val generateC: GenerateC) : ZoneITE {
    override fun getRealTargetName(): String {
        //return null;
        throw UnintendedUseException()
    }

    override fun isPropertyStatement(): Boolean {
        val resolvedElement = identTableEntry.resolvedElement
        return resolvedElement is PropertyStatement
    }

    override fun isConstructorDef(): Boolean {
        val resolvedElement = identTableEntry.resolvedElement
        return resolvedElement is ConstructorDef
    }

    override fun isVariableStatement(): Boolean {
        val resolvedElement = identTableEntry.resolvedElement
        return resolvedElement is VariableStatement
    }

    override fun getVariableStatement(): ZoneVariableStatement {
        Preconditions.checkArgument(isVariableStatement)
        return ZoneVariableStatement(identTableEntry.resolvedElement as VariableStatement?)
    }

    override fun getRealTargetName2(aog: AOG, value: String): String {
        var state = 0
        var code = -1

        val ls = LinkedList<String>()
        // TODO in Deduce set property lookupType to denote what type of lookup it is: MEMBER, LOCAL, or CLOSURE
        var backlink = identTableEntry.backlink
        val text = identTableEntry.ident.text
        if (backlink == null) {
            val zi = generateC._zone[identTableEntry, gf]
            if (zi!!.isVariableStatement) {
                val zvs = zi.variableStatement
                val parent = zvs.containerParent

                if (parent !== gf.fd) {
                    // FIXME 10/19 find out what this here means and implement it vv
                    // we want identTableEntry.resolved which will be a EvaMember
                    // which will have a container which will be either be a function,
                    // statement (semantic block, loop, match, etc) or a EvaContainerNC
                    // FIXME 10/19 ^^ ^^
                    val y = 2
                    val er = identTableEntry.externalRef() // FIXME move to onExternalRef
                    if (er is EvaContainerNC) {
                        if (er !is EvaNamespace) {
                            throw AssertionError()
                        } else {
                            //if (ns.isInstance()) {}
                            state = 1
                            code = er.code
                        }
                    }
                }
            }
            when (state) {
                0 -> ls.add(Emit.emit("/*912*/") + "vsc->vm" + text)
                1 -> ls.add(Emit.emit("/*845*/") + String.format("zNZ%d_instance->vm%s", code, text))
                else -> throw IllegalStateException("Can't be here")
            }
        } else {
            ls.add(Emit.emit("/*872*/") + "vm" + text) // TODO blindly adding "vm" might not always work, also put in loop
        }

        while (backlink != null) {
            if (backlink is IntegerIA) {
                val realTargetName: String = generateC.getRealTargetName(gf, backlink, AOG.ASSIGN)
                ls.addFirst(Emit.emit("/*892*/") + realTargetName)
                backlink = null
            } else if (backlink is IdentIA) {
                val identIAIndex: Int = backlink.index
                val identTableEntry1 = gf.getIdentTableEntry(identIAIndex)
                val identTableEntryName = identTableEntry1.ident.text
                ls.addFirst(Emit.emit("/*885*/") + "vm" + identTableEntryName) // TODO blindly adding "vm" might not always be right
                backlink = identTableEntry1.backlink
            } else throw IllegalStateException("Invalid InstructionArgument for backlink")
        }

        val reference = CReference(generateC._repo, generateC.ce)

        val index = gf.findIdentTableIndex(identTableEntry)
        assert(index != -1)
        val target = IdentIA(index, gf)

        reference.getIdentIAPath(target, aog, value)
        val path = reference.build()
        generateC.LOG.info("932 $path")
        val s = Helpers.String_join("->", ls)
        generateC.LOG.info("933 $s")

        val zi = generateC._zone[identTableEntry, gf]

        return if (zi!!.isConstructorDef || zi.isPropertyStatement /* || value != null*/) {
            path
        } else {
            s
        }
    }

    override fun getRealTargetName3(aAssignmentValue: String): Garish_TargetName {
        return object : Garish_TargetName {
            override fun forAOG(aAOG: AOG): String {
                return getRealTargetName2(aAOG, aAssignmentValue)
            }

            override fun reasonedForAOG(aAOG: AOG): IReasonedString {
                return object : IReasonedString {
                    override fun text(): String? {
                        return forAOG(aAOG)
                    }

                    override fun reason(): String {
                        return "<<151: ??>>"
                    }
                }
            }
        }
    }
}
