package tripleo.elijah.nextgen.pn

interface SC_Fai {
    fun signal()

    fun sc_fai_asString(): String?
}
