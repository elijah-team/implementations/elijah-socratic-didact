package tripleo.elijah.stages.gen_fn

import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action_Arena

class SGTA_SetResolvedType(private val osType: OS_Type) : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        gt.resolved = osType
    }
}
