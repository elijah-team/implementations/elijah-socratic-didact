package tripleo.elijah.comp.notation

import org.jetbrains.annotations.Contract
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.gen_generic.OutputFileFactoryParams
import tripleo.elijah.world.i.WorldModule
import java.util.*
import java.util.function.Supplier

class GM_GenerateModuleRequest(private val generateNodesIntoSink: GN_GenerateNodesIntoSink,
                               private val mod: WorldModule, private val env: GN_GenerateNodesIntoSinkEnv) : GN_Env {
    @Contract("_ -> new")
    fun getGenerateFiles(fgs: Supplier<GenerateResultEnv?>): GenerateFiles {
        val params = params()
        return GN_GenerateNodesIntoSinkEnv.Companion.getGenerateFiles(params, params.worldMod, fgs)
    }

    fun params(): OutputFileFactoryParams {
        return env.getParams(mod, generateNodesIntoSink)
    }

    fun generateNodesIntoSink(): GN_GenerateNodesIntoSink {
        return generateNodesIntoSink
    }

    fun mod(): WorldModule {
        return mod
    }

    fun env(): GN_GenerateNodesIntoSinkEnv {
        return env
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as GM_GenerateModuleRequest
        return this.generateNodesIntoSink == that.generateNodesIntoSink && (this.mod == that.mod) && (this.env == that.env)
    }

    override fun hashCode(): Int {
        return Objects.hash(generateNodesIntoSink, mod, env)
    }

    override fun toString(): String {
        return "GM_GenerateModuleRequest[" +
                "generateNodesIntoSink=" + generateNodesIntoSink + ", " +
                "mod=" + mod + ", " +
                "env=" + env + ']'
    }
}
