package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang2.SpecialVariables
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.VariableTableEntry
import tripleo.elijah.stages.instructions.VariableTableType

internal class ZoneVTE__1(private val _g_varTableEntry: VariableTableEntry, private val _g_gf: BaseEvaFunction, private val _g_gc: GenerateC) : ZoneVTE {
    private var _realTargetName: String? = null

    override fun getRealTargetName(): String {
        if (_realTargetName == null) {
            _realTargetName = calculate()
        }

        return Emit.emit("/*879*/") + _realTargetName // TODO 10/19 EmittedString/Z whatever/EG_Stmt(ZoneVTE::getRealTargetName)
    }

    private fun calculate(): String {
        val vte_name = _g_varTableEntry.name
        return when (_g_varTableEntry.vtt) {
            VariableTableType.TEMP -> {
                if (_g_varTableEntry.name == null) {
                    "vt" + _g_varTableEntry.tempNum
                } else {
                    "vt" + _g_varTableEntry.name
                }
            }

            VariableTableType.ARG -> {
                "va$vte_name"
            }

            VariableTableType.RESULT -> {
                "vsr"
            }

            else -> {
                if (SpecialVariables.contains(vte_name)) {
                    SpecialVariables.get(vte_name)
                } else if (isValue(_g_gc.a_lookup(_g_gf)!!, vte_name)) {
                    "vsc->vsv"
                } else {
                    "vv$vte_name"
                }
            }
        }
    }
}
