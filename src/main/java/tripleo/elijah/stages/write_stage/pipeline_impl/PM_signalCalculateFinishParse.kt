package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.write_stage.pipeline_impl.WPIS_GenerateOutputs.OutputItems

class PM_signalCalculateFinishParse(val result: GenerateResult, val cs: List<EvaClass>, val ns: List<EvaNamespace>, val fs: List<BaseEvaFunction>, val itms: OutputItems)
