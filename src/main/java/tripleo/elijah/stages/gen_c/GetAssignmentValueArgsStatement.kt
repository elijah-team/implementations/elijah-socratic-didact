package tripleo.elijah.stages.gen_c

import tripleo.elijah.nextgen.outputstatement.*
import tripleo.elijah.nextgen.outputstatement.EG_Statement.Companion.of
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.util2.UnintendedUseException
import java.util.stream.Collectors

class GetAssignmentValueArgsStatement(private val inst: Instruction) : EG_Statement {
    private val sll: MutableList<String> = ArrayList()

    fun add_string(aS: String) {
        sll.add(aS)
    }
    override val explanation: EX_Explanation
        get() {
            throw UnintendedUseException()
            // return null;
        }

    override val text: String
        get() {
            val getAssignmentValueArgsStatement = mk_EG_SequenceStatement(
                    EG_Naming("GetAssignmentValueArgsStatement"),
                    sll.stream()
                            .map { x: String? -> EG_Statement.of(x!!, EX_Explanation_withMessage("GetAssignmentValueArgsStatement *27*")) }
                            .collect(Collectors.toList()
                            )
            )
            return getAssignmentValueArgsStatement.text
        }

    fun stringList(): List<String> {
        return sll
    }
}
