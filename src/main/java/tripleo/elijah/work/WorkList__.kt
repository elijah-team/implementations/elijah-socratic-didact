/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.work

import com.google.common.collect.ImmutableList

/**
 * Created 4/26/21 4:24 AM
 */
class WorkList__ : WorkList {
    private var _done = false
    private val jobs: MutableList<WorkJob> = ArrayList()

    override fun addJob(aJob: WorkJob) {
        jobs.add(aJob)
    }

    override fun getJobs(): ImmutableList<WorkJob> {
        return ImmutableList.copyOf(jobs)
    }

    override fun isDone(): Boolean {
        return _done
    }

    override fun isEmpty(): Boolean {
        return jobs.size == 0
    }

    override fun setDone() {
        _done = true
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

