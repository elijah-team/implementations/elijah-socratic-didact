package tripleo.elijah.comp.nextgen.wonka

import com.google.common.base.Preconditions
import com.google.common.base.Supplier
import tripleo.elijah.comp.IO
import tripleo.elijah.comp.nextgen.CK_GlobalRef
import tripleo.elijah.comp.specs.ElijahCache
import tripleo.elijah.comp.specs.ElijahSpec
import tripleo.elijah.comp.specs.ElijahSpec_
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.util.Operation2
import tripleo.wrap.File
import java.io.FileNotFoundException

class CK_SourceFile__ElaboratedElijahFile(protected val directory: File, protected val file_name: String) : __CK_SourceFile__AbstractElijahFile() {
    override val file: File = File(directory, file_name)

    override fun process_query(): Operation2<Any?> {
        assert(false)
        val ezCache: ElijahCache? = null // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
        //compilation.getCompilationEnclosure().getCompilationRunner().ezCache();
        val om = process_query(compilation!!.io, ezCache!!)

        //super.asserverate(); // FIXME 12/03 what is this? eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
        return om as Operation2<Any?>
    }

    private fun process_query(io: IO, elijahCache: ElijahCache): Operation2<OS_Module> {
        val fileName = file_name()
        Preconditions.checkArgument(__CK_SourceFile__AbstractElijahFile.Companion.isElijjahFile(fileName))

        val stream = Supplier {
            try {
                return@Supplier io.readFile(file)
            } catch (aE: FileNotFoundException) {
                throw RuntimeException(aE)
            }
        }.get()

        val elijahSpec: ElijahSpec = ElijahSpec_(fileName, file, stream)

        return __CK_SourceFile__AbstractElijahFile.Companion.realParseElijahFile(elijahSpec, elijahCache, compilation!!)
    }

    private fun file_name(): String {
        return file.name
    }

    override val fileName: String
        get() = file_name()

    override fun compilation00(): CK_GlobalRef {
        return this.compilation!!
    }
}
