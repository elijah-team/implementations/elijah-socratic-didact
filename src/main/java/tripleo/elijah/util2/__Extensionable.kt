package tripleo.elijah.util2

abstract class __Extensionable : _Extensionable {
    private val exts: MutableMap<Any, Any> = HashMap()

    override fun getExt(aClass: Class<*>): Any? {
        if (exts.containsKey(aClass)) {
            return exts[aClass]!!
        }
        return null
    }

    override fun putExt(aClass: Class<*>, o: Any) {
        exts[aClass] = o
    }
}
