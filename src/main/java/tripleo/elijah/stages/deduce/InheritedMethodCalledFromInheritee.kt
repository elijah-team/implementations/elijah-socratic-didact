package tripleo.elijah.stages.deduce

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import java.util.*

class InheritedMethodCalledFromInheritee(private val activeFunction: FunctionDef, private val definedIn: ClassStatement,
                                         private val calledIn: BaseEvaFunction) : CI_Hint {
    fun activeFunction(): FunctionDef {
        return activeFunction
    }

    fun definedIn(): ClassStatement {
        return definedIn
    }

    fun calledIn(): BaseEvaFunction {
        return calledIn
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as InheritedMethodCalledFromInheritee
        return this.activeFunction == that.activeFunction && (this.definedIn == that.definedIn) && (this.calledIn == that.calledIn)
    }

    override fun hashCode(): Int {
        return Objects.hash(activeFunction, definedIn, calledIn)
    }

    override fun toString(): String {
        return "InheritedMethodCalledFromInheritee[" +
                "activeFunction=" + activeFunction + ", " +
                "definedIn=" + definedIn + ", " +
                "calledIn=" + calledIn + ']'
    } // the function referenced by pte.expression is an inherited method
    // defined in genType.resolved and called in generatedFunction
    // for whatever reason we don't have a CodePoint (gf, instruction...)
}
