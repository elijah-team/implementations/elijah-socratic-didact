package tripleo.elijah.stages.gen_c;

import org.jetbrains.annotations.Nullable;
import tripleo.elijah.stages.gen_fn.EvaNode;

public interface ConstructorPathOp { // internal?? package?? module??
	@Nullable
	String getCtorName();

	@Nullable
	EvaNode getResolved();
}
