package tripleo.elijah.comp.process

import lombok.Getter
import tripleo.elijah.comp.graph.i.CK_AbstractStepsContext
import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.internal.CR_State

//void addDiagnostic(Diagnostic d);
@Getter
internal class CD_CRS_StepsContext(// 24/01/04 back and forth
        val state: CR_State, // 24/01/04 back and forth
        //void addOutputString(CB_OutputString os);
        val output: CB_Output) : CK_AbstractStepsContext()
