package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.nextgen.DR_PossibleType

class DR_PossibleTypeCI(private val ci: ClassInvocation, private val fi: FunctionInvocation) : DR_PossibleType
