package tripleo.elijah.comp.specs

import tripleo.elijah.comp.graph.i.CM_Module
import tripleo.elijah.comp.local.CW.CX_ParseElijahFile.ElijahSpecReader
import tripleo.wrap.File
import java.io.InputStream
import java.util.*

class ElijahSpec_ : ElijahSpec {
    private val file_name: String
    private val file: File
    private val r: ElijahSpecReader?
    private val s: InputStream?
    private var cmModule: CM_Module? = null

    constructor(file_name: String, file: File, s: InputStream?) {
        this.file_name = file_name
        this.file = file
        this.s = s
        this.r = null
    }

    constructor(aFileName: String, aFile: File, aR: ElijahSpecReader?) {
        file_name = aFileName
        file = aFile
        r = aR
        s = null
    }

    override fun getLongPath2(): String {
        // [T920907]
        // TODO 10/13 why new File(file_name) and not file??
        val absolutePath = File(file_name).absolutePath // !!
        return absolutePath
    }

    override fun file_name(): String {
        return file_name
    }

    override fun file(): File {
        return file
    }

    override fun s(): InputStream {
        return s!!
    }

    override fun getModule(): CM_Module {
        return cmModule!!
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as ElijahSpec
        return this.file_name == that.file_name() && (this.file == that.file()) && (this.s == that.s())
    }

    override fun hashCode(): Int {
        return Objects.hash(file_name, file, s)
    }

    override fun toString(): String {
        return "ElijahSpec[" +
                "file_name=" + file_name + ", " +
                "file=" + file + ", " +
                "s=" + s + ']'
    }

    fun advise(aCmModule: CM_Module?) {
        cmModule = aCmModule
    }
}
