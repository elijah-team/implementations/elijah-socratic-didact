package tripleo.elijah.stages.garish

import org.jetbrains.annotations.Contract
import tripleo.elijah.g.GGarishClass
import tripleo.elijah.lang.types.OS_UnknownType
import tripleo.elijah.lang.types.OS_UserClassType
import tripleo.elijah.stages.gen_c.CClassDecl
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.util.BufferTabbedOutputStream
import tripleo.elijah.world.i.LivingClass

class GarishClass // _lc.setGarish(this);
@Contract(pure = true) constructor(val living: LivingClass) : GGarishClass {
    fun finalizedGenericPrintable(evaClass: EvaClass): String {
        val klass = evaClass.klass
        val x = evaClass.ci?.genericPart()

        val name = klass.name
        val sb = StringBuilder()
        sb.append(name)

        sb.append('[')

        if (x != null) {
            for ((_, y) in x.entrySet()) {
                if (y is OS_UnknownType) {
                } else {
                    if (y is OS_UserClassType) {
                        sb.append("%s,".formatted(y.classOf!!.name))
                    }
                }
            }
        }

        sb.append(']')

        return sb.toString()
    }

    fun garish(aGenerateC: GenerateC?, gr: GenerateResult?, aResultSink: GenerateResultSink) {
        living.generateWith(aResultSink, this, gr, aGenerateC)
    }

    fun getImplBuffer(x: EvaClass, decl: CClassDecl, class_name: String?, class_code: Int): BufferTabbedOutputStream {
        val tos = BufferTabbedOutputStream()

        // TODO remove this block when constructors are added in dependent functions,
        // etc in Deduce

        // TODO what about named constructors and ctor$0 and "the debug stack"
        tos.put_string_ln(String.format("%sZC%d() {", class_name, class_code))
        tos.incr_tabs()
        tos.put_string_ln(String.format("%sR = GC_malloc(sizeof(%s));", class_name, class_name))
        tos.put_string_ln(String.format("R->_tag = %d;", class_code))
        if (decl.prim) {
            // TODO consider NULL, and floats and longs, etc
            if (decl.prim_decl != "bool") tos.put_string_ln("R->vsv = 0;")
            else tos.put_string_ln("R->vsv = false;")
        } else {
            for (o in x.varTable) {
//					final String typeName = getTypeNameForVarTableEntry(o);
                // TODO this should be the result of getDefaultValue for each type
                tos.put_string_ln(String.format("R->vm%s = 0;", o.nameToken))
            }
        }
        tos.put_string_ln("return R;")
        tos.dec_tabs()
        tos.put_string_ln(String.format("} // class %s%s", if (decl.prim) "box " else "", x.name))
        tos.put_string_ln("")

        tos.flush()

        tos.close()
        return tos
    }

    fun getImplBuffer(aGenerateC: GenerateC): BufferTabbedOutputStream {
        val evaClass = living.evaNode() as EvaClass

        val decl = CClassDecl(evaClass)
        decl.evaluatePrimitive()

        val class_name = aGenerateC.getTypeName(evaClass)
        val class_code = living.code

        return getImplBuffer(evaClass, decl, class_name, class_code)
    }

    fun getHeaderBuffer(aGenerateC: GenerateC): BufferTabbedOutputStream {
        val evaClass = living.evaNode() as EvaClass

        val decl = CClassDecl(evaClass)
        decl.evaluatePrimitive()

        val class_name = aGenerateC.getTypeName(evaClass)
        val class_code = living.code

        return getHeaderBuffer(aGenerateC, evaClass, decl, class_name)
    }

    fun getHeaderBuffer(aGenerateC: GenerateC, x: EvaClass, decl: CClassDecl, class_name: String?): BufferTabbedOutputStream {
        val tosHdr = BufferTabbedOutputStream()

        tosHdr.put_string_ln("typedef struct {")
        tosHdr.incr_tabs()
        tosHdr.put_string_ln("int _tag;")
        if (!decl.prim) {
            for (o in x.varTable) {
                val typeName = aGenerateC.getTypeNameGNCForVarTableEntry(o)
                tosHdr.put_string_ln(String.format("%s vm%s;", typeName, o.nameToken))
            }
        } else {
            tosHdr.put_string_ln(String.format("%s vsv;", decl.prim_decl))
        }

        tosHdr.dec_tabs()
        tosHdr.put_string_ln("")

        val xx = finalizedGenericPrintable(x)
        tosHdr.put_string_ln("} %s;  // class %s%s".formatted(class_name, if (decl.prim) "box " else "", xx))
        tosHdr.put_string_ln("")

        tosHdr.flush()
        tosHdr.close()
        return tosHdr
    }
}
