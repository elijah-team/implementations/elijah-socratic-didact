/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Iterables
import com.google.common.collect.Multimap
import lombok.Getter
import org.apache.commons.lang3.tuple.Pair
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.i.ICompilationAccess
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CompilationImpl.Companion.gitlabCIVerbosity
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.types.OS_UnknownType
import tripleo.elijah.nextgen.ClassDefinition
import tripleo.elijah.nextgen.ClassDefinition__
import tripleo.elijah.nextgen.diagnostic.CouldntGenerateClass
import tripleo.elijah.nextgen.reactive.ReactiveDimension
import tripleo.elijah.stages.deduce.DeduceLocalVariable.MemberInvocation
import tripleo.elijah.stages.deduce.DeduceTypes2.OS_SpecialVariable
import tripleo.elijah.stages.deduce.declarations.DeferredMember
import tripleo.elijah.stages.deduce.declarations.DeferredMemberFunction
import tripleo.elijah.stages.deduce.nextgen.DR_Ident
import tripleo.elijah.stages.deduce.nextgen.DR_Item
import tripleo.elijah.stages.deduce.nextgen.DR_ProcCall
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_IdentTableEntry.ST.Companion.register
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_fn.EvaContainer.VarTableEntry
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.logging.ElLog.Verbosity
import tripleo.elijah.stages.logging.ElLog_
import tripleo.elijah.stages.post_deduce.DefaultCodeRegistrar
import tripleo.elijah.stateful.State
import tripleo.elijah.stateful._RegistrationTargetMixin
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.Maybe
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.Eventual
import tripleo.elijah.work.*
import tripleo.elijah.world.i.WorldModule
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.function.Consumer
import java.util.function.Function
import java.util.function.Supplier

/**
 * Created 12/24/20 3:59 AM
 */
class DeducePhase(private val ca: ICompilationAccess, @JvmField var pa: IPipelineAccess,
				  private val pipelineLogic: PipelineLogic) : _RegistrationTargetMixin(), ReactiveDimension {
    @JvmField
	val generatedClasses: GeneratedClasses
    // given

    // transitive
	@JvmField
	val generatePhase: GeneratePhase = pipelineLogic.generatePhase
    val iWantModules: Multimap<OS_Module, Consumer<DeduceTypes2>> = ArrayListMultimap.create()
    private val PHASE = "DeducePhase"
    private val __inj = DeducePhaseInjector()
    val functionMapHooks: MutableList<IFunctionMapHook> = _inj().new_ArrayList__IFunctionMapHook()

    // 24/01/04 back and forth
    @JvmField
	@Getter
    val codeRegistrar: ICodeRegistrar
    private val namespaceInvocationMap = _inj()
            .new_HashMap__NamespaceInvocationMap()
    private val classGenerator: ExecutorService = Executors.newCachedThreadPool()
    private val country = _inj().new_Country1(this)
    private val deferredMemberFunctions = _inj()
            .new_ArrayList__DeferredaMemberFunction()
    private val foundElements = _inj().new_ArrayList__FoundElement()
    private val functionMap: Multimap<FunctionDef, EvaFunction> = ArrayListMultimap.create()
    private val idte_type_callbacks = _inj().new_HashMap__IdentTableEntry()
    private val LOG: ElLog
    private val _actives = _inj().new_ArrayList__DE3_Active()
    private val classInvocationMultimap: Multimap<ClassStatement, ClassInvocation> = ArrayListMultimap
            .create()
    private val deferredMembers = _inj().new_ArrayList__DeferredMember()
    private val onclasses: Multimap<ClassStatement, OnClass> = ArrayListMultimap.create()
    private val resolved_variables: Multimap<OS_Element, ResolvedVariables> = ArrayListMultimap.create()
    private val drs = _inj().new_DRS()
    private val waits = _inj().new_WAITS()

    constructor(ace: CompilationEnclosure) : this(ace.compilationAccess, ace.pipelineAccess, ace.pipelineLogic)

    init {
        // created
        codeRegistrar = _inj().new_DefaultCodeRegistrar(ca.compilation as Compilation)
        LOG = _inj().new_ElLog("(DEDUCE_PHASE)", pipelineLogic.verbosity, PHASE)

        // using
        pa.compilationEnclosure.pipelineLogic.addLog(LOG)
        pa.compilationEnclosure.addReactiveDimension(this)

        // create more
        generatedClasses = _inj().new_GeneratedClasses(this)

        //
        register(this)
    }

    fun _compilation(): Compilation {
        return ca.compilation as Compilation
    }

    fun _functionMap(): Multimap<FunctionDef, EvaFunction> {
        return functionMap
    }

    fun _inj(): DeducePhaseInjector {
        return this.__inj
    }

    fun addActives(activesList: List<DE3_Active>) {
        _actives.addAll(activesList)
    }

    fun addDeferredMember(aDeferredMember: DeferredMember) {
        deferredMembers.add(aDeferredMember)
    }

    fun addDeferredMember(aDeferredMemberFunction: DeferredMemberFunction) {
        deferredMemberFunctions.add(aDeferredMemberFunction)
    }

    private fun addDr(drp: Pair<BaseEvaFunction?, DR_Item>) {
        drs.add(drp)
    }

    fun addDrs(aGeneratedFunction: BaseEvaFunction?, aDrs: List<DR_Item>) {
        for (dr in aDrs) {
            addDr(Pair.of(aGeneratedFunction, dr))
        }
    }

    fun addFunction(generatedFunction: EvaFunction, fd: FunctionDef) {
        functionMap.put(fd, generatedFunction)
    }

    fun addFunctionMapHook(aFunctionMapHook: IFunctionMapHook) {
        functionMapHooks.add(aFunctionMapHook)
    }

    fun addLog(aLog: ElLog?) {
        // deduceLogs.add(aLog);
        pipelineLogic.addLog(aLog)
    }

    fun ca(): ICompilationAccess {
        return ca
    }

    fun country(): Country {
        return country
    }

    fun deduceModule(aMod: WorldModule): DeduceTypes2 {
        return deduceModule(aMod, this.generatedClasses, gitlabCIVerbosity())
    }

    fun deduceModule(wm: WorldModule, lgf: Iterable<EvaNode>,
                     verbosity: Verbosity?): DeduceTypes2 {
        val mod = wm.module()

        val deduceTypes2 = _inj().new_DeduceTypes2(mod, this, verbosity)
        //		LOG.err("196 DeduceTypes "+deduceTypes2.getFileName());
        run {
            val p: List<EvaNode> = _inj().new_ArrayList__EvaNode()
            Iterables.addAll(p, lgf)
            LOG.info("197 lgf.size " + p.size)
        }
        deduceTypes2.deduceFunctions(lgf)

        //		deduceTypes2.deduceClasses(generatedClasses.copy().stream()
//				.filter(c -> c.module() == m)
//				.collect(Collectors.toList()));
        for (evaNode in generatedClasses.copy()) {
            if (evaNode.module() !== mod) continue

            if (evaNode is EvaClass) {
                evaNode.fixupUserClasses(deduceTypes2, evaNode.klass.context)
                deduceTypes2.deduceOneClass(evaNode)
            }
        }

        for (evaNode in lgf) {
            val bef: BaseEvaFunction

            if (evaNode is BaseEvaFunction) {
                bef = evaNode
            } else continue
            for (hook in functionMapHooks) {
                if (hook.matches(bef.fd)) {
                    hook.apply(Helpers.List_of(bef as EvaFunction))
                }
            }
        }

        return deduceTypes2
    }

    fun doneWait(aDeduceTypes2: DeduceTypes2?, aGeneratedFunction: BaseEvaFunction?) {
        NotImplementedException.raise()
    }

    fun equivalentGenericPart(first: ClassInvocation, second: ClassInvocation): Boolean {
        val secondGenericPart1 = second.genericPart()
        val firstGenericPart1 = first.genericPart()

        if (second.klass === first.klass /* && secondGenericPart1 == null */) return true

        val secondGenericPart = secondGenericPart1.map
        val firstGenericPart = firstGenericPart1.map

        var i = secondGenericPart!!.entries.size
        for ((key, value) in secondGenericPart) {
            val entry_type = firstGenericPart!![key]

            // assert !(entry_type instanceof OS_UnknownType);
            if (entry_type is OS_UnknownType) continue

            if (entry_type == value) i--
            //				else
//					return aClassInvocation;
        }
        return i == 0
    }

    fun finish() {
        setGeneratedClassParents()
        /*
		 * for (GeneratedNode generatedNode : generatedClasses) { if (generatedNode
		 * instanceof EvaClass) { final EvaClass generatedClass = (EvaClass)
		 * generatedNode; final ClassStatement cs = generatedClass.getKlass();
		 * Collection<ClassInvocation> cis = classInvocationMultimap.get(cs); for
		 * (ClassInvocation ci : cis) { if (equivalentGenericPart(generatedClass.ci,
		 * ci)) { final DeferredObject<EvaClass, Void, Void> deferredObject =
		 * (DeferredObject<EvaClass, Void, Void>) ci.promise(); deferredObject.then(new
		 * DoneCallback<EvaClass>() {
		 *
		 * @Override public void onDone(EvaClass result) { assert result ==
		 * generatedClass; } }); // deferredObject.resolve(generatedClass); } } } }
		 */
        handleOnClassCallbacks()
        handleIdteTypeCallbacks()
        /*
		 * for (Map.Entry<EvaFunction, OS_Type> entry : typeDecideds.entrySet()) { for
		 * (Triplet triplet : forFunctions) { if (triplet.gf.getGenerated() ==
		 * entry.getKey()) { synchronized (triplet.deduceTypes2) {
		 * triplet.forFunction.typeDecided(entry.getValue()); } } } }
		 */
        /*
		 * for (Map.Entry<FunctionDef, EvaFunction> entry : functionMap.entries()) {
		 * FunctionInvocation fi = _inj().new_FunctionInvocation(entry.getKey(), null);
		 * for (Triplet triplet : forFunctions) { // Collection<EvaFunction> x =
		 * functionMap.get(fi); triplet.forFunction.finish(); } }
		 */
        handleFoundElements()
        handleResolvedVariables()
        resolveAllVariableTableEntries()
        handleDeferredMemberFunctions()
        handleDeferredMembers()
        sanityChecks()
        handleFunctionMapHooks()

        for (de3_Active in _actives) {
            pa.compilationEnclosure.addReactive(de3_Active)
        }

        for (pair in drs.iterator()) {
            val ef = pair.left
            val dr = pair.right

            // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("611a " + ef);
            // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("611b " + dr);
            if (dr is DR_ProcCall) {
                val fi: FunctionInvocation = dr.functionInvocation
                if (fi != null) {
                    val ef1 = arrayOfNulls<BaseEvaFunction>(1)
                    fi.generatePromise().then { x: BaseEvaFunction? -> ef1[0] = x }

                    if (ef1[0] == null) {
                        // throw new AssertionError();
                        SimplePrintLoggerToRemoveSoon.println_err_4("****************************** no function generated")
                    } else {
                        pa.activeFunction(ef1[0])
                    }
                }
            } else if (dr is DR_Ident) {
                // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4(String.format("***** 623623 -- %s %b", drid.name(),
                // drid.isResolved()));

                for (understanding in dr.u) {
                    // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4(String.format("**** 623626 -- %s",
                    // understanding.asString()));
                }
            }
        }

        for (wait in waits.iterator()) {
            for ((key, value) in iWantModules.asMap()) {
                if (key === wait.module) {
                    for (deduceTypes2Consumer in value) {
                        // README I like this, but by the time we get here, everything is already
                        // done...
                        // and I mean the callback to DT_External2::actualise
                        // - everything being resolution and setStatus, etc

                        deduceTypes2Consumer.accept(wait)
                    }
                }
            }
        }

        waits.clear()
    }

    fun forFunction(deduceTypes2: DeduceTypes2,
                    fi: FunctionInvocation,
                    forFunction: ForFunction) {
//		LOG.err("272 forFunction\n\t"+fi.getFunction()+"\n\t"+fi.pte);
        fi.generateDeferred().then { result: BaseEvaFunction -> result.typePromise().then { aType: GenType? -> forFunction.typeDecided(aType) } }
    }

    fun generateClass2(gf: GenerateFunctions?,
                       ci: ClassInvocation,
                       wm: WorkManager?): Eventual<ClassDefinition> {
        val ret = Eventual<ClassDefinition>()

        classGenerator.submit {
            val gen = _inj().new_WlGenerateClass(gf, ci, generatedClasses, codeRegistrar)
            val genclass1 = arrayOfNulls<EvaClass>(1)

            gen.setConsumer { x: EvaClass? ->
                genclass1[0] = x
            }

            gen.run(wm!!)

            val cd = _inj().new_ClassDefinition(ci)
            if (genclass1[0] != null) {
                val genclass = genclass1[0]

                cd.node = genclass
                ret.resolve(cd)
            } else {
                ret.reject(_inj().new_CouldntGenerateClass(gen, this@DeducePhase))
            }
        }

        return ret
    }

    fun handleDeferredMemberFunctions() {
        for (deferredMemberFunction in deferredMemberFunctions) {
            val y = 2
            val parent = deferredMemberFunction.parent // .getParent().getParent();

            if (parent is ClassStatement) {
                val invocation = deferredMemberFunction.getInvocation()

                val dmfpic = _inj()
                        .new_DeferredMemberFunctionParentIsClassStatement(deferredMemberFunction, invocation, this)
                dmfpic.action()
            } else if (parent is NamespaceStatement) {
//				final ClassStatement classStatement = (ClassStatement) deferredMemberFunction.getParent();
                val invocation = deferredMemberFunction.getInvocation()
                val namespaceInvocation = if (invocation is ClassInvocation) {
                    _inj().new_NamespaceInvocation(parent)
                } else {
                    invocation as NamespaceInvocation?
                }

                namespaceInvocation!!.resolvePromise().then { result: EvaNamespace ->
                    val x = namespaceInvocation
                    val z = deferredMemberFunction
                    val yy = 2
                }
            }
        }

        for (evaNode in generatedClasses) {
            if (evaNode is EvaContainerNC) {
                evaNode.noteDependencies(evaNode.dependency) // TODO is this right?

                for (generatedFunction in evaNode.functionMap.values) {
                    generatedFunction.noteDependencies(evaNode.dependency)
                }
                if (evaNode is EvaClass) {
                    for (evaConstructor in evaNode.constructors.values) {
                        evaConstructor.noteDependencies(evaNode.dependency)
                    }
                }
            }
        }
    }

    fun handleDeferredMembers() {
        for (deferredMember in deferredMembers) {
            if (deferredMember.parent.isNamespaceStatement) {
                val parent = deferredMember.parent.element() as NamespaceStatement
                val nsi = registerNamespaceInvocation(parent)
                nsi!!.resolveDeferred().done { result: EvaNamespace ->
                    val v_m = result
                            .getVariable(deferredMember.getVariableStatement().name)
                    assert(!v_m.isException)
                    val v = v_m.o!!
                    v.resolve_varType_cb { varType: OS_Type? ->
                        val genType = _inj().new_GenTypeImpl()
                        genType.set(varType!!)

                        //								if (deferredMember.getInvocation() instanceof NamespaceInvocation) {
//									((NamespaceInvocation) deferredMember.getInvocation()).resolveDeferred().done(new DoneCallback<EvaNamespace>() {
//										@Override
//										public void onDone(EvaNamespace result) {
//											result;
//										}
//									});
//								}
                        deferredMember.externalRefDeferred().resolve(result)
                    }
                }
            } else if (deferredMember.parent.element() is ClassStatement) {
                // TODO do something
                val parent = deferredMember.parent.element() as ClassStatement
                val name = deferredMember.getVariableStatement().name

                // because deferredMember.invocation is null, we must create one here
                val ci = registerClassInvocation(parent, null, NULL_DeduceTypes2())!!
                ci.resolvePromise().then { result: EvaClass ->
                    val vt = result.varTable
                    for (gc_vte in vt) {
                        if (gc_vte.nameToken.text == name) {
                            // check connections
                            // unify pot. types (prol. shuld be done already -- we don't want to be
                            // reporting errors here)
                            // call typePromises and externalRefPromisess

                            // TODO just getting first element here (without processing of any kind); HACK

                            val connectionPairs = gc_vte.connectionPairs
                            if (!connectionPairs.isEmpty()) {
                                val ty = connectionPairs[0].vte.typeTableEntry.genType
                                assert(ty.resolved != null)
                                gc_vte.varType = ty.resolved // TODO make sure this is right in all cases
                                if (deferredMember.typeResolved().isPending) deferredMember.typeResolved().resolve(ty)
                                break
                            } else {
                                NotImplementedException.raise()
                            }
                        }
                    }
                }
            } else throw NotImplementedException()
        }
    }

    fun handleFoundElements() {
        for (foundElement in foundElements) {
            // TODO As we are using this, didntFind will never fail because
            // we call doFoundElement manually in resolveIdentIA
            // As the code matures, maybe this will change and the interface
            // will be improved, namely calling doFoundElement from here as well
            if (foundElement.didntFind()) {
                foundElement.doNoFoundElement()
            }
        }
    }

    fun handleFunctionMapHooks() {
        for ((key, value) in functionMap.asMap()) {
            for (functionMapHook1 in ca.functionMapHooks()) {
                val functionMapHook = functionMapHook1 as IFunctionMapHook

                if (functionMapHook.matches(key)) {
                    functionMapHook.apply(value)
                }
            }
        }
    }

    fun handleIdteTypeCallbacks() {
        for ((idte, value) in idte_type_callbacks) {
            if (idte.type != null &&  // TODO make a stage where this gets set (resolvePotentialTypes)
                    idte.type?.attached != null) {
                value.typeDeduced(idte.type?.attached!!) // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
            } else {
                value.noTypeFound()
            }
        }
    }

    fun handleOnClassCallbacks() {
        // TODO rewrite with classInvocationMultimap
        for (classStatement in onclasses.keySet()) {
            for (evaNode in generatedClasses) {
                if (evaNode is EvaClass) {
                    if (evaNode.klass === classStatement) {
                        val ks = onclasses[classStatement]
                        for (k in ks) {
                            k.classFound(evaNode)
                        }
                    } else {
                        val cmv: Collection<EvaClass> = evaNode.classMap.values
                        for (aClass in cmv) {
                            if (aClass.klass === classStatement) {
                                val ks = onclasses[classStatement]
                                for (k in ks) {
                                    k.classFound(evaNode)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun handleResolvedVariables() {
        for (evaNode in generatedClasses.copy()) {
            if (evaNode is EvaContainer) {
                val x = resolved_variables[evaNode.element]
                for (resolvedVariables in x) {
                    val variable_m: Maybe<VarTableEntry> = evaNode
                            .getVariable(resolvedVariables.varName)

                    assert(!variable_m.isException)
                    val variable = variable_m.o!!

                    val type = resolvedVariables.identTableEntry.type
                    if (type != null) variable.addPotentialTypes(Helpers.List_of(type))
                    variable.addPotentialTypes(resolvedVariables.identTableEntry.potentialTypes())
                    variable.updatePotentialTypes(evaNode)
                }
            }
        }
    }

    fun modulePromise(aModule: OS_Module, con: Consumer<DeduceTypes2>) {
        iWantModules.put(aModule, con)
    }

    fun newFunctionInvocation(
		    f: FunctionDef?, aO: ProcTableEntry?,
		    ci: IInvocation?
    ): FunctionInvocation {
        return _inj().new_FunctionInvocation(f, aO, ci, this.generatePhase)
    }

    fun onClass(aClassStatement: ClassStatement, callback: OnClass) {
        onclasses.put(aClassStatement, callback)
    }

    fun onType(entry: IdentTableEntry, callback: OnType) {
        idte_type_callbacks[entry] = callback
    }

    fun registerClassInvocation(aClassInvocation: ClassInvocation): ClassInvocation {
        val rci = _inj().new_RegisterClassInvocation(this)
        return rci.registerClassInvocation(aClassInvocation)
    }

    fun registerClassInvocation(aParent: ClassStatement): ClassInvocation {
        // return registerClassInvocation(_inj().new_ClassInvocation(aParent, null,
        // aDeduceTypes2));
        return registerClassInvocation(_inj().new_ClassInvocation(aParent, null, NULL_DeduceTypes2())) // !! 08/28
    }

    // helper function. no generics!
    fun registerClassInvocation(aParent: ClassStatement,
                                aConstructorName: String?,
                                aDeduceTypes2: Supplier<DeduceTypes2?>): ClassInvocation? {
        // @Nullable ClassInvocation classInvocation = _inj().new_ClassInvocation(aParent, aConstructorName, aDeduceTypes2);
        var ci = _inj().new_ClassInvocation(aParent, aConstructorName, aDeduceTypes2) // !! 08/28; 09/24 ??
        ci = registerClassInvocation(ci)
        return ci
    }

    fun registerClassInvocation(env: RegisterClassInvocation_env): ClassInvocation {
        val rci = env.deducePhaseSupplier().get().RegisterClassInvocation()
        return rci.registerClassInvocation(env)
    }

    fun registerFound(foundElement: FoundElement) {
        foundElements.add(foundElement)
    }

    fun registerNamespaceInvocation(aNamespaceStatement: NamespaceStatement): NamespaceInvocation? {
        if (namespaceInvocationMap.containsKey(aNamespaceStatement)) return namespaceInvocationMap[aNamespaceStatement]

        val nsi = _inj().new_NamespaceInvocation(aNamespaceStatement)
        namespaceInvocationMap[aNamespaceStatement] = nsi
        return nsi
    }

    fun registerResolvedVariable(identTableEntry: IdentTableEntry, parent: OS_Element, varName: String?) {
        resolved_variables.put(parent, _inj().new_ResolvedVariables(identTableEntry, parent, varName))
    }

    fun resolveAllVariableTableEntries() {
        val gcs = _inj().new_ArrayList__EvaClass()
        var all_resolve_var_table_entries = false
        while (!all_resolve_var_table_entries) {
            if (generatedClasses.size() == 0) break
            for (evaNode in generatedClasses.copy()) {
                if (evaNode is EvaClass) {
                    all_resolve_var_table_entries = evaNode.resolve_var_table_entries(this) // TODO use a while loop
                    // to get all classes
                }
            }
        }
    }

    private fun sanityChecks() {
        for (evaNode in generatedClasses) {
            if (evaNode is EvaClass) {
                sanityChecks(evaNode.functionMap.values)
                //				sanityChecks(generatedClass.constructors.values()); // TODO reenable
            } else if (evaNode is EvaNamespace) {
                sanityChecks(evaNode.functionMap.values)
                //				sanityChecks(generatedNamespace.constructors.values());
            }
        }
    }

    private fun sanityChecks(aGeneratedFunctions: Collection<EvaFunction>) {
        for (generatedFunction in aGeneratedFunctions) {
            for (identTableEntry in generatedFunction.idte_list) {
                when (identTableEntry.status) {
                    BaseTableEntry.Status.UNKNOWN -> {
                        assert(!identTableEntry.hasResolvedElement())
                        LOG.err(String.format("250 UNKNOWN idte %s in %s", identTableEntry, generatedFunction))
                    }

                    BaseTableEntry.Status.KNOWN -> {
                        assert(identTableEntry.hasResolvedElement())
                        if (identTableEntry.type == null) {
                            LOG.err(String.format("258 null type in KNOWN idte %s in %s", identTableEntry,
                                    generatedFunction
                            ))
                        }
                    }

                    BaseTableEntry.Status.UNCHECKED -> {
                        LOG.err(String.format("255 UNCHECKED idte %s in %s", identTableEntry, generatedFunction))
                    }
                }
                for (pot_tte in identTableEntry.potentialTypes()) {
                    if (pot_tte.attached == null) {
                        LOG.err(String.format("267 null potential attached in %s in %s in %s", pot_tte, identTableEntry,
                                generatedFunction
                        ))
                    }
                }
            }
        }
    }

    fun setGeneratedClassParents() {
        // TODO all EvaFunction nodes have a genClass member
        for (evaNode in generatedClasses) {
            if (evaNode is EvaClass) {
                val functions: Collection<EvaFunction> = evaNode.functionMap.values
                for (generatedFunction in functions) {
                    generatedFunction.parent = evaNode
                }
            } else if (evaNode is EvaNamespace) {
                val functions: Collection<EvaFunction> = evaNode.functionMap.values
                for (generatedFunction in functions) {
                    generatedFunction.parent = evaNode
                }
            }
        }
    }

    fun waitOn(aDeduceTypes2: DeduceTypes2) {
        waits.add(aDeduceTypes2)
    }

    interface Country {
        fun sendClasses(ces: Consumer<List<EvaNode>?>)
    }

    class DRS {
        private val drs: MutableList<Pair<BaseEvaFunction?, DR_Item>> = ArrayList()

        fun add(aDrp: Pair<BaseEvaFunction?, DR_Item>) {
            drs.add(aDrp)
        }

        fun iterator(): Iterable<Pair<BaseEvaFunction?, DR_Item>> {
            return drs
        }
    }

    class ResolvedVariables(aIdentTableEntry: IdentTableEntry, aParent: OS_Element, aVarName: String?) {
        val identTableEntry: IdentTableEntry
        val parent: OS_Element // README tripleo.elijah.lang._CommonNC, but that's package-private
        val varName: String?

        init {
            assert(aParent is ClassStatement || aParent is NamespaceStatement)
            identTableEntry = aIdentTableEntry
            parent = aParent
            varName = aVarName
        }
    }

    class WAITS {
        private val waits: MutableSet<DeduceTypes2> = HashSet()

        fun add(aDeduceTypes2: DeduceTypes2) {
            waits.add(aDeduceTypes2)
        }

        fun iterator(): Iterable<DeduceTypes2> {
            return waits
        }

        fun clear() {
            waits.clear()
        }
    }

    inner class Country1 : Country {
        override fun sendClasses(ces: Consumer<List<EvaNode>?>) {
            ces.accept(generatedClasses.copy())
        }
    }

    inner class DeducePhaseInjector {
        fun new_ArrayList__DE3_Active(): MutableList<DE3_Active> {
            return ArrayList()
        }

        fun new_ArrayList__DeferredaMemberFunction(): MutableList<DeferredMemberFunction> {
            return ArrayList()
        }

        fun new_ArrayList__DeferredMember(): MutableList<DeferredMember> {
            return ArrayList()
        }

        fun new_ArrayList__EvaClass(): List<EvaClass> {
            return ArrayList()
        }

        fun new_ArrayList__EvaNode(): MutableList<EvaNode> {
            return ArrayList()
        }

        fun new_ArrayList__EvaNode(aGeneratedClasses: List<EvaNode>?): List<EvaNode> {
            return ArrayList(aGeneratedClasses)
        }

        fun new_ArrayList__FoundElement(): MutableList<FoundElement> {
            return ArrayList()
        }

        fun new_ArrayList__IFunctionMapHook(): MutableList<IFunctionMapHook> {
            return ArrayList()
        }

        fun new_ArrayList__State(): List<State> {
            return ArrayList()
        }

        fun new_ClassDefinition(aCi: ClassInvocation?): ClassDefinition {
            return ClassDefinition__(aCi!!)
        }

        fun new_ClassInvocation(aParent: ClassStatement?, aConstructorName: String?,
                                aDeduceTypes2Supplier: Supplier<DeduceTypes2?>): ClassInvocation {
            return ClassInvocation(aParent!!, aConstructorName, aDeduceTypes2Supplier)
        }

        fun new_CouldntGenerateClass(aCd: ClassDefinition?, aGf: GenerateFunctions?,
                                     aCi: ClassInvocation?): Diagnostic {
            return CouldntGenerateClass(aCd, aGf, aCi)
        }

        fun new_CouldntGenerateClass(gen: WlGenerateClass?, deducePhase: DeducePhase?): Diagnostic {
            return CouldntGenerateClass(gen, deducePhase)
        }

        fun new_Country1(aDeducePhase: DeducePhase): Country1 {
            return aDeducePhase.Country1()
        }

        fun new_DeduceTypes2(aM: OS_Module?, aDeducePhase: DeducePhase?,
                             aVerbosity: Verbosity?): DeduceTypes2 {
            return DeduceTypes2(aM!!, aDeducePhase!!, aVerbosity)
        }

        fun new_DefaultCodeRegistrar(aCompilation: Compilation?): ICodeRegistrar {
            return DefaultCodeRegistrar(aCompilation!!)
        }

        fun new_DeferredMemberFunctionParentIsClassStatement(
                aDeferredMemberFunction: DeferredMemberFunction, aInvocation: IInvocation?,
                aDeducePhase: DeducePhase): DeferredMemberFunctionParentIsClassStatement {
            return aDeducePhase.DeferredMemberFunctionParentIsClassStatement(aDeferredMemberFunction, aInvocation)
        }

        fun new_DRS(): DRS {
            return DRS()
        }

        fun new_ElLog(aS: String?, aVerbosity: Verbosity?, aDeducePhase: String?): ElLog {
            return ElLog_(aS!!, aVerbosity!!, aDeducePhase!!)
        }

        fun new_FunctionInvocation(aF: FunctionDef?,
                                   aProcTableEntry: ProcTableEntry?,
                                   aCi: IInvocation?,
                                   aGeneratePhase: GeneratePhase?): FunctionInvocation {
            return FunctionInvocation(aF, aProcTableEntry, aCi!!, aGeneratePhase)
        }

        fun new_GeneratedClasses(aDeducePhase: DeducePhase): GeneratedClasses {
            return aDeducePhase.GeneratedClasses()
        }

        fun new_GenTypeImpl(): GenType {
            return GenTypeImpl()
        }

        fun new_GetFunctionMapClass(): Function<EvaNode, Map<FunctionDef, EvaFunction>> {
            return GetFunctionMapClass()
        }

        fun new_GetFunctionMapNamespace(): Function<EvaNode, Map<FunctionDef, EvaFunction>> {
            return GetFunctionMapNamespace()
        }

        fun new_HashMap__IdentTableEntry(): MutableMap<IdentTableEntry, OnType> {
            return HashMap()
        }

        fun new_HashMap__NamespaceInvocationMap(): MutableMap<NamespaceStatement, NamespaceInvocation> {
            return HashMap()
        }

        fun new_NamespaceInvocation(aParent: NamespaceStatement?): NamespaceInvocation {
            return NamespaceInvocation(aParent!!)
        }

        fun new_RegisterClassInvocation(aDeducePhase: DeducePhase): RegisterClassInvocation {
            return aDeducePhase.RegisterClassInvocation()
        }

        fun new_ResolvedVariables(aIdentTableEntry: IdentTableEntry, aParent: OS_Element,
                                  aVarName: String?): ResolvedVariables {
            return ResolvedVariables(aIdentTableEntry, aParent, aVarName)
        }

        fun new_WAITS(): WAITS {
            return WAITS()
        }

        fun new_WlGenerateClass(aGenerateFunctions: GenerateFunctions?,
                                aClassInvocation: ClassInvocation?, aGeneratedClasses: GeneratedClasses?,
                                aCodeRegistrar: ICodeRegistrar?): WlGenerateClass {
            return WlGenerateClass(aGenerateFunctions!!, aClassInvocation!!, aGeneratedClasses, aCodeRegistrar!!)
        }

        fun new_WlGenerateClass(aGenerateFunctions: GenerateFunctions?,
                                aClassInvocation: ClassInvocation?,
                                aGeneratedClasses: GeneratedClasses?,
                                aCodeRegistrar: ICodeRegistrar?,
                                aEnv: RegisterClassInvocation_env?): WorkJob {
            return WlGenerateClass(aGenerateFunctions!!, aClassInvocation!!, aGeneratedClasses, aCodeRegistrar!!, aEnv!!)
        }

        fun new_WorkList(): WorkList {
            return WorkList__()
        }

        fun new_WorkManager(): WorkManager {
            return WorkManager__()
        }
    }

    /* static */
    inner class DeferredMemberFunctionParentIsClassStatement(private val deferredMemberFunction: DeferredMemberFunction,
                                                             private val invocation: IInvocation?) {
        private val parent = deferredMemberFunction.parent // .getParent().getParent();

        fun action() {
            if (invocation is ClassInvocation) invocation.resolvePromise().then { result -> defaultAction(result) }
            else if (invocation is NamespaceInvocation) invocation.resolvePromise().then { result -> defaultAction(result) }
        }

        fun <T : EvaNode?> defaultAction(result: T) {
            val p = deferredMemberFunction.parent

            if (p is OS_SpecialVariable) {
                onSpecialVariable(p)
                val y = 2
            } else if (p is ClassStatement) {
                val x = getFunctionMap(result)

                // once again we need EvaFunction, not FunctionDef
                // we seem to have it below, but there can be multiple
                // specializations of each function
                val gf = x.apply(result!!)[deferredMemberFunction.functionDef]
                if (gf != null) {
                    deferredMemberFunction.externalRefDeferred().resolve(gf)
                    gf.typePromise().then { result -> deferredMemberFunction.typeResolved().resolve(result) }
                }
            } else throw IllegalStateException("unknown parent")
        }

        private fun <T : EvaNode?> getFunctionMap(result: T): Function<EvaNode, Map<FunctionDef, EvaFunction>> {
            val x = if (result is EvaNamespace) _inj().new_GetFunctionMapNamespace()
            else if (result is EvaClass) _inj().new_GetFunctionMapClass()
            else throw NotImplementedException()
            return x
        }

        fun onSpecialVariable(aSpecialVariable: OS_SpecialVariable) {
            val mi = aSpecialVariable.memberInvocation

            when (mi.role) {
                MemberInvocation.Role.INHERITED -> {
                    val functionInvocation = deferredMemberFunction.functionInvocation()
                    functionInvocation.generatePromise().then { gf ->
                        deferredMemberFunction.externalRefDeferred().resolve(gf)
                        gf.typePromise().then { result -> deferredMemberFunction.typeResolved().resolve(result) }
                    }
                }

                MemberInvocation.Role.DIRECT -> if (invocation is NamespaceInvocation) assert(false) else {
                    val classInvocation = invocation as ClassInvocation?
                    classInvocation!!.resolvePromise().then { element_generated -> // once again we need EvaFunction, not FunctionDef
                        // we seem to have it below, but there can be multiple
                        // specializations of each function
                        val gf = element_generated.functionMap[deferredMemberFunction.functionDef]
                        deferredMemberFunction.externalRefDeferred().resolve(gf)
                        gf!!.typePromise().then { result -> deferredMemberFunction.typeResolved().resolve(result) }
                    }
                }

                else -> throw IllegalStateException("Unexpected value: " + mi.role)
            }
        }
    }

    inner class GeneratedClasses : Iterable<EvaNode> {
        var generatedClasses: MutableList<EvaNode> = _inj().new_ArrayList__EvaNode()
        private var generation = 0

        fun add(aClass: EvaNode) {
            pa._send_GeneratedClass(aClass)

            generatedClasses.add(aClass)
        }

        fun addAll(lgc: List<EvaNode>) {
            // TODO is this method really needed
            generatedClasses.addAll(lgc)
        }

        fun copy(): List<EvaNode> {
            ++generation
            return ArrayList(generatedClasses)
        }

        override fun iterator(): MutableIterator<EvaNode> {
            return generatedClasses.iterator()
        }

        fun size(): Int {
            return generatedClasses.size
        }

        override fun toString(): String {
            val size = generatedClasses.size
            return "GeneratedClasses{size=$size, generation=$generation}"
        }
    }

    inner class RegisterClassInvocation {
        // TODO this class is a mess
        fun getClassInvocationPromise(aClassInvocation: ClassInvocation,
                                      mod: OS_Module?,
                                      wl: WorkList?,
                                      aEnv: RegisterClassInvocation_env): Eventual<ClassDefinition> {
            var mod = mod
            if (mod == null) {
                mod = aClassInvocation.klass.context.module()
            }

            val finalMod: OS_Module = mod

            val req2 = RegisterClassInvocation2_env(aEnv, generatePhase.wm, wl!!) { generatePhase.getGenerateFunctions(finalMod) }

            val prom = generateClass(req2)
            return prom
        }

        private fun generateClass(aReq2: RegisterClassInvocation2_env): Eventual<ClassDefinition> {
            // par { return promise ; wm.drain() ; }

            val gf = aReq2.generateFunctions.get()
            val ci = aReq2.env1().classInvocation()
            val wm = aReq2.workManager()

            val x = generateClass2(gf, ci, wm)

            wm.drain()
            return x
        }

        private fun getClassInvocation(aClassInvocation: ClassInvocation,
                                       mod: OS_Module?, wl: WorkList, aEnv: RegisterClassInvocation_env): ClassInvocation {
            var mod = mod
            if (mod == null) mod = aClassInvocation.klass.context.module()

            val prom = DeferredObject<ClassDefinition, Diagnostic, Void>()

            val generateFunctions = generatePhase.getGenerateFunctions(mod)
            wl.addJob(_inj().new_WlGenerateClass(generateFunctions, aClassInvocation, generatedClasses,
                    codeRegistrar, aEnv
            )) // TODO why add now?
            generatePhase.wm.addJobs(wl)
            generatePhase.wm.drain() // TODO find a better place to put this

            prom.resolve(ClassDefinition__(aClassInvocation))

            // return prom;
            return aClassInvocation
        }

        private fun part2(aClassInvocation: ClassInvocation,
                          put: Boolean,
                          aEnv: RegisterClassInvocation_env): ClassInvocation {
            // 2. Check and see if already done
            val cls = classInvocationMultimap[aClassInvocation.klass]
            for (ci in cls) {
                if (equivalentGenericPart(ci, aClassInvocation)) {
                    return ci
                }
            }

            if (put) {
                classInvocationMultimap.put(aClassInvocation.klass, aClassInvocation)
            }

            // 3. Generate new EvaClass
            val wl = _inj().new_WorkList()

            val x = getClassInvocation(aClassInvocation, null, wl, aEnv)

            // 4. Return it
            // final ClassDefinition[] yy = new ClassDefinition[1];
            // x.then(y -> yy[0] =y);
            // return yy[0];
            return x
        }

        fun registerClassInvocation(aClassInvocation: ClassInvocation): ClassInvocation {
            val env = RegisterClassInvocation_env(aClassInvocation, NULL_DeduceTypes2(), null)

            return registerClassInvocation(env)
        }

        fun registerClassInvocation(env: RegisterClassInvocation_env): ClassInvocation {
            val aClassInvocation = env.classInvocation()

            // 1. select which to return
            val c = aClassInvocation.klass
            val cis = classInvocationMultimap[c]

            // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
            for (ci in cis) {
                // don't lose information
                if (ci.constructorName != null) if (ci.constructorName != aClassInvocation.constructorName) continue

                val i = equivalentGenericPart(aClassInvocation, ci)
                if (i) {
                    if (aClassInvocation is DerivedClassInvocation) {
                        if (ci is DerivedClassInvocation) continue

                        /* if (classInvocation.resolvePromise().isResolved()) */
                        run { // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
                            ci.resolvePromise().then { aClassInvocation.resolveDeferred().resolve(it) }
                            return aClassInvocation
                        }
                    } else return ci
                }
            }

            return part2(aClassInvocation, true, env)
        }
    }
}
internal class GetFunctionMapClass : Function<EvaNode, Map<FunctionDef, EvaFunction>> {
    override fun apply(aClass: EvaNode): Map<FunctionDef, EvaFunction> {
        return (aClass as EvaClass).functionMap
    }
}

internal class GetFunctionMapNamespace : Function<EvaNode, Map<FunctionDef, EvaFunction>> {
    override fun apply(aNamespace: EvaNode): Map<FunctionDef, EvaFunction> {
        return (aNamespace as EvaNamespace).functionMap
    }
}
