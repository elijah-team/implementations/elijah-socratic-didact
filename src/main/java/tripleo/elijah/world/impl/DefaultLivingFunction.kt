package tripleo.elijah.world.impl

import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.impl.BaseFunctionDef
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.world.i.LivingFunction

class DefaultLivingFunction : LivingFunction {
    private val _element: FunctionDef
    private val _gf: BaseEvaFunction?

    constructor(aFunction: BaseEvaFunction) {
        _element = aFunction.fd
        _gf = aFunction
    }

    constructor(aElement: BaseFunctionDef) {
        _element = aElement
        _gf = null
    }

    override fun getCode(): Int {
        return _gf!!.code
    }

    override fun getElement(): FunctionDef {
        return _element
    }
}
