/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.instructions

import tripleo.elijah.lang.i.Context
import tripleo.elijah.stages.deduce.DeduceElement

/**
 * Created 9/10/20 3:16 PM
 */
class Instruction {
    var deduceElement: DeduceElement? = null
    var args__: List<InstructionArgument?>? = null
    var context: Context? = null

    @JvmField
    var index: Int = -1

    @JvmField
    var name: InstructionName? = null

    constructor()

    constructor(aInstructionName: InstructionName?) {
        name = aInstructionName
    }

    constructor(aInstructionName: InstructionName?, aLi: List<InstructionArgument?>?) {
        name = aInstructionName
        setArgs(aLi)
    }

    fun getArg(i: Int): InstructionArgument? {
        return args__!![i]
    }

    val argsSize: Int
        get() = args__!!.size

    fun setArgs(args_: List<InstructionArgument?>?) {
        args__ = args_
    }

    override fun toString(): String {
        return "Instruction{name=$name, index=$index, args=$args__}"
    }
}
