package tripleo.elijah.comp.nextgen.wonka

import tripleo.elijah.comp.graph.i.CK_SourceFile
import tripleo.elijah.comp.i.CompilationClosure
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.local.CW
import tripleo.elijah.comp.nextgen.CK_GlobalRef
import tripleo.elijah.comp.specs.ElijahCache
import tripleo.elijah.comp.specs.ElijahSpec
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.outputtree.EOT_OutputFile
import tripleo.elijah.util.Operation2
import tripleo.elijah.util2.UnintendedUseException
import tripleo.wrap.File
import java.util.regex.Pattern

abstract class __CK_SourceFile__AbstractElijahFile : CK_SourceFile<Any?> {
    //	private final __CK_SourceFile__AbstractElijjahFile CKSourceFile__abstractElijahFile;
    protected var compilation: CK_GlobalRef? = null
    protected var input: CompilerInput? = null

    override fun associate(aCc: CompilationClosure) {
        compilation = aCc.compilation as CK_GlobalRef
    }

    override fun associate(aInput: CompilerInput, aCc: CompilationClosure) {
        input = aInput
        compilation = aCc.compilation as CK_GlobalRef
    }

    override fun compilerInput(): CompilerInput {
        return input!!
    }

    override fun input(): EIT_Input {
        throw UnintendedUseException("TODO 12/?? implement me")
    }

    override fun output(): EOT_OutputFile {
        throw UnintendedUseException("TODO 12/?? implement me")
    }

    protected abstract val file: File
        //protected void asserverate() {
        get

    abstract val fileName: String

    abstract fun compilation00(): CK_GlobalRef

    companion object {
        //	public __CK_SourceFile__AbstractElijjahFile(final __CK_SourceFile__AbstractElijjahFile aCKSourceFile__abstractElijahFile) {
        //		CKSourceFile__abstractElijahFile = aCKSourceFile__abstractElijahFile;
        //	}

        /**
         * - I don't remember what absolutePath is for - Cache doesn't add to QueryDB
         *
         *
         * STEPS ------
         *
         *
         * 1. Get absolutePath<br></br>
         * 2. Check cache, return early<br></br>
         * 3. Parse (Query is incorrect I think)<br></br>
         * 4. Cache new result<br></br>
         *
         *
         * @param spec
         * @param cache
         * @return
         */
        @JvmOverloads
        fun realParseElijahFile(spec: ElijahSpec, cache: ElijahCache, compilation: CK_GlobalRef): Operation2<OS_Module> {
            val absolutePath = spec.longPath2

            val early = cache[absolutePath]

            if (early.isPresent) {
                return Operation2.success(early.get())
            }

            val om = CW.CX_ParseElijahFile.parseAndCache(spec, cache, absolutePath, compilation.asCompilation())
            return om
        }

        fun isElijjahFile(aFileName: String?): Boolean {
            return Pattern.matches(".+\\.elijjah$", aFileName) || Pattern.matches(".+\\.elijah$", aFileName)
        }
    }
}
