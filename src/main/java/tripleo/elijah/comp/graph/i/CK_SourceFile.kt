package tripleo.elijah.comp.graph.i

import tripleo.elijah.comp.i.CompilationClosure
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.outputtree.EOT_OutputFile
import tripleo.elijah.util.Operation2

interface CK_SourceFile<T> {
	fun compilerInput(): CompilerInput?

	fun input(): EIT_Input? // s ??

	fun output(): EOT_OutputFile? // s ??

	fun process_query(): Operation2<T>?

	fun associate(aCc: CompilationClosure)

	fun associate(aInput: CompilerInput, aCc: CompilationClosure)
}
