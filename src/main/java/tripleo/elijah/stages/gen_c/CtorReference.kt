/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_c

import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.gen_c.c_ast1.C_Assignment
import tripleo.elijah.stages.gen_c.c_ast1.C_ProcedureCall
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaContainerNC
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.util.*

/**
 * Created 3/7/21 1:22 AM
 */
class CtorReference {
    var refs: MutableList<CReference.Reference> = ArrayList()
    private var _resolved: EvaNode? = null
    private var args: List<String?>? = null
    private var ctorName: String? = ""

    fun addRef(text: String, type: CReference.Ref) {
        refs.add(CReference.Reference(text, type))
    }

    /**
     * Call before you call build
     *
     * @param sl3
     */
    fun args(sl3: List<String?>?) {
        args = sl3
    }

    fun build(aClsinv: ClassInvocation): String {
        var sb = StringBuilder()
        var open = false
        var needs_comma = false
        //		List<String> sl = new ArrayList<String>();
        var text = ""
        for (ref in refs) {
            when (ref.type) {
                CReference.Ref.LOCAL -> {
                    text = "vv" + ref.text
                    sb.append(text)
                }

                CReference.Ref.MEMBER -> {
                    text = "->vm" + ref.text
                    sb.append(text)
                }

                CReference.Ref.INLINE_MEMBER -> {
                    text = Emit.emit("/*2190*/") + ".vm" + ref.text
                    sb.append(text)
                }

                CReference.Ref.DIRECT_MEMBER -> {
                    text = Emit.emit("/*1240*/") + "vsc->vm" + ref.text
                    sb.append(text)
                }

                CReference.Ref.FUNCTION -> {
                    val s = sb.toString()
                    text = String.format("%s(%s", ref.text, s)
                    sb = StringBuilder()
                    open = true
                    if (s != "") needs_comma = true
                    sb.append(text)
                }

                CReference.Ref.CONSTRUCTOR -> {
                    val s = sb.toString()
                    text = String.format("%s(%s", ref.text, s)
                    sb = StringBuilder()
                    open = true
                    if (s != "") needs_comma = true
                    sb.append(text)
                }

                CReference.Ref.PROPERTY_GET -> {
                    val s = sb.toString()
                    text = String.format("%s(%s", ref.text, s)
                    sb = StringBuilder()
                    open = true
                    if (s != "") needs_comma = true
                    sb.append(text)
                }

                else -> throw IllegalStateException("Unexpected value: " + ref.type)
            }//			sl.add(text);
        }
        run {
            // Assuming constructor call
            val code = if (_resolved != null) {
                (_resolved as EvaContainerNC).code
            } else {
                -3
            }
            if (code == 0) {
                SimplePrintLoggerToRemoveSoon
                        .println_err_2("** 32135 ClassStatement with 0 code " + aClsinv.klass)
            }

            val n = sb.toString()

            // TODO Garish(?)Constructor.calculateCtorName(?)/Code
            val text2 = String.format("ZC%d%s", code, ctorName) // TODO what about named constructors
            sb.append(" = ")
            sb.append(text2)
            sb.append("(")
            assert(!open)
            open = true

            val pc = C_ProcedureCall()
            pc.setTargetName(text2)
            pc.setArgs(args)
            val cas = C_Assignment()
            cas.setLeft(n)
            cas.setRight(pc)
            return cas.string
        }

        /*
		 * if (needs_comma && args != null && args.size() > 0) sb.append(", "); if
		 * (open) { if (args != null) { sb.append(Helpers.String_join(", ", args)); }
		 * sb.append(")"); } return sb.toString();
		 */
    }

    fun getConstructorPath(ia2: InstructionArgument, gf: BaseEvaFunction) {
        val s: List<InstructionArgument> = _getIdentIAPathList(ia2)

        var i = 0
        val sSize = s.size
        while (i < sSize) {
            val ia = s[i]
            if (ia is IntegerIA) {
                // should only be the first element if at all
                assert(i == 0)
                val vte = gf.getVarTableEntry(DeduceTypes2.to_int(ia))

                val op: ConstructorPathOp = IntegerIA_Ops.Companion.get(ia, sSize).getConstructorPath()
                _resolved = op.resolved
                ctorName = op.ctorName

                addRef(vte.name, CReference.Ref.LOCAL)
            } else if (ia is IdentIA) {
                val op: ConstructorPathOp = IdentIA_Ops.Companion.get(ia).getConstructorPath()
                _resolved = op.resolved
                ctorName = op.ctorName

                addRef(ia.entry.ident.get().text, CReference.Ref.LOCAL) // TDOO check correctness
            } else if (ia is ProcIA) {
//				final ProcTableEntry prte = generatedFunction.getProcTableEntry(to_int(ia));
//				text = (prte.expression.getLeft()).toString();
////				assert i == sSize-1;
//				addRef(text, Ref.FUNCTION); // TODO needs to use name of resolved function
                throw NotImplementedException()
            } else {
                throw NotImplementedException()
            }
            i++
        }
    }
}

private fun IdentIA_Ops.getConstructorPath(): ConstructorPathOp {
    TODO("Not yet implemented")
}

private fun IntegerIA_Ops.getConstructorPath(): ConstructorPathOp {
    TODO("Not yet implemented")
}
