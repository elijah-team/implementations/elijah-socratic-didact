package tripleo.elijah.comp.internal

import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.ci.LibraryStatementPart
import tripleo.elijah.comp.graph.i.CM_Module
import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.CD_FindStdLib
import tripleo.elijah.comp.i.USE_Reasoning
import tripleo.elijah.comp.process.CK_ProcessInitialAction
import tripleo.elijah.util2.UnintendedUseException
import tripleo.wrap.File

enum class USE_Reasonings {
    ;

    private class USE_Reasoning_initial(private val processInitialAction: CK_ProcessInitialAction, private val compilationRunner: CompilationRunner, private val cbOutput: CB_Output) : USE_Reasoning {
        override fun parent(): Boolean {
            return false
        }

        override fun module(): CM_Module {
            throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
//            return null // idk
        }

        override fun instruction_dir(): File {
            throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
        }

        override fun compilerInstructions(): CompilerInstructions {
            return processInitialAction.maybeFoundResult()
        }

        override fun ty(): USE_Reasoning.Type {
            return USE_Reasoning.Type.USE_Reasoning__initial
        }
    }

    private class USE_Reasoning_findStdLib(private val findStdLib: CD_FindStdLib) : USE_Reasoning {
        override fun parent(): Boolean {
            return false
        }

        override fun module(): CM_Module {
            //return null;
            throw UnintendedUseException()
        }

        override fun instruction_dir(): File {
            throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
        }

        override fun compilerInstructions(): CompilerInstructions {
            return findStdLib.maybeFoundResult()
        }

        override fun ty(): USE_Reasoning.Type {
            return USE_Reasoning.Type.USE_Reasoning__findStdLib
        }
    }

    private class USE_Reasoning_instruction_doer_addon(private val item: CompilerInstructions) : USE_Reasoning {
        override fun parent(): Boolean {
            return false
        }

        override fun module(): CM_Module {
            //return ProgramIsLikelyWrong;
            //throw new UnintendedUseException("");
//            return null // ci not mod
            throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode") // back and forth
        }

        override fun instruction_dir(): File {
            throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
        }

        override fun compilerInstructions(): CompilerInstructions {
            return item
        }

        override fun ty(): USE_Reasoning.Type {
            return USE_Reasoning.Type.USE_Reasoning__instruction_doer_addon
        }
    }

    private class USE_Reasoning_default_(private val instructionDir: File, private val compilerInstructions: CompilerInstructions) : USE_Reasoning {
        override fun parent(): Boolean {
            return false
        }

        override fun module(): CM_Module {
//            return null // this one is just confusing
            throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
        }

        override fun instruction_dir(): File {
            return instructionDir
        }

        override fun compilerInstructions(): CompilerInstructions {
            return compilerInstructions
        }

        override fun ty(): USE_Reasoning.Type {
            return USE_Reasoning.Type.USE_Reasoning___default
        }
    }

    private class USE_Reasoning_child(private val dirName: String, private val instructionDir: File, private val parent: Boolean, private val dir: File, private val compilerInstructions: CompilerInstructions) : USE_Reasoning {
        fun top(): File {
            return File(dirName)
        }

        fun child(): File {
            return instructionDir
        }

        override fun parent(): Boolean {
            return parent
        }

        override fun module(): CM_Module {
//            return null // idk
            throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
        }

        override fun instruction_dir(): File {
            return dir
        }

        override fun compilerInstructions(): CompilerInstructions {
            return compilerInstructions
        }

        override fun ty(): USE_Reasoning.Type {
            return USE_Reasoning.Type.USE_Reasoning__child
        }
    }

    private class USE_Reasoning_parent(private val parent: Boolean, private val instructionDir: File, private val compilerInstructions: CompilerInstructions) : USE_Reasoning {
        override fun parent(): Boolean {
            return parent
        }

        override fun module(): CM_Module {
//            return null // idk
            throw UnintendedUseException("implement me when you are feeling unfulfilled and or failure mode")
        }

        override fun instruction_dir(): File {
            return instructionDir
        }

        override fun compilerInstructions(): CompilerInstructions {
            return compilerInstructions
        }

        override fun ty(): USE_Reasoning.Type {
            return USE_Reasoning.Type.USE_Reasoning__parent
        }
    }

    companion object {
        fun parent(aCompilerInstructions: CompilerInstructions, parent: Boolean, aInstructionDir: File, aLsp: LibraryStatementPart?): USE_Reasoning {
            return USE_Reasoning_parent(parent, aInstructionDir, aCompilerInstructions)
        }

        fun child(aCompilerInstructions: CompilerInstructions, parent: Boolean, aInstructionDir: File, aDirName: String, aDir: File, aLsp: LibraryStatementPart?): USE_Reasoning {
            return USE_Reasoning_child(aDirName, aInstructionDir, parent, aDir, aCompilerInstructions)
        }

        fun default_(aCompilerInstructions: CompilerInstructions, parent: Boolean, aInstructionDir: File, aLsp: LibraryStatementPart?): USE_Reasoning {
            return USE_Reasoning_default_(aInstructionDir, aCompilerInstructions)
        }

        @JvmStatic
        fun instruction_doer_addon(item: CompilerInstructions): USE_Reasoning {
            return USE_Reasoning_instruction_doer_addon(item)
        }

        @JvmStatic
        fun findStdLib(aFindStdLib: CD_FindStdLib): USE_Reasoning {
            return USE_Reasoning_findStdLib(aFindStdLib)
        }

        @JvmStatic
        fun initial(aCKProcessInitialAction: CK_ProcessInitialAction, aCompilationRunner: CompilationRunner, aOutput: CB_Output): USE_Reasoning {
            return USE_Reasoning_initial(aCKProcessInitialAction, aCompilationRunner, aOutput)
        }
    }
}
