package tripleo.elijah.stages.hooligan.pipeline_impl

import antlr.Token
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.i.SmallWriter.*
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.world.i.WorldModule
import java.util.*

internal class Lawabidingcitizen {
    internal inner class SmallWriter1 {
        val objMap1: MutableMap<OS_Element, SW_Ref> = HashMap()
        private val writers: MutableMap<OS_Element, SmallWriter> = LinkedHashMap()

        val text: String
            get() {
                val sb3 = StringBuilder()

                for ((key, smallWriter) in writers) {
                    val module = key as OS_Module

                    sb3.append("(MODULE \"%s\"\n\n".formatted(module.fileName))
                    sb3.append("%s\n\n".formatted(smallWriter.string))

                    val values: Collection<SW_Ref> = (smallWriter as SmallWriter2).objMap.values
                    for (ref in ArrayList<SW_Ref>(values)) {
                        sb3.append("(REFERENCE \"%s\" \"%s\"\n".formatted(ref.name(), ref.get().javaClass.name))

                        val writer2 = SmallWriter2(objMap1)
                        ref.get().serializeTo(writer2)

                        for (ref2 in writer2.insides) {
                            ref2.get().serializeTo(writer2)
                        }

                        sb3.append(writer2.sb)
                        sb3.append(")\n")
                    }
                    sb3.append(")\n")
                }

                return sb3.toString()
            }

        fun newWriter(aModule: OS_Module): SmallWriter {
            val sw = SmallWriter2(objMap1)

            writers[aModule] = sw

            return sw
        }
    }

    internal inner class SmallWriter2(// = new HashMap<>();
            val objMap: MutableMap<OS_Element, SW_Ref>) : SmallWriter {
        val sb: StringBuilder = StringBuilder()
        var insides: Set<SW_Ref> = HashSet()

        override fun createList(): SW_List? {
            NotImplementedException.raise()
            return null
        }

        override fun createRef(aFieldValue: OS_Element): SW_Ref {
            if (objMap.containsKey(aFieldValue)) {
                return objMap[aFieldValue]!!
            }

            // aFieldValue.serializeTo(this);
            //
            // sb.append("(field 'string \"%s\" \"%s\")\n".formatted("--ref--",
            // aFieldValue));
            // NotImplementedException.raise();
            val swRef: SW_Ref = object : SW_Ref {
                val uuid: UUID = UUID.randomUUID()

                override fun get(): OS_Element {
                    return aFieldValue
                }

                override fun name(): String {
                    return uuid.toString()
                }
            }

            objMap[aFieldValue] = swRef

            return swRef
        }

        override fun createTypeNameList(): SW_TypenameList {
            NotImplementedException.raise()
            return object : SW_TypenameList {
                private val typenames: MutableList<TypeName> = LinkedList()

                override fun add(el: TypeName) {
                    typenames.add(el)
                }

                override fun items(): List<TypeName> {
                    return typenames
                }
            }
        }

        override fun fieldElement(aFieldName: String, aFieldValue: OS_Element) {
            val w5 = SmallWriter2(objMap)
            aFieldValue.serializeTo(w5)
            sb.append("(field 'element \"%s\" %s)\n".formatted(aFieldName, w5.sb))
        }

        override fun fieldExpression(aFieldName: String, aFieldValue: IExpression) {
            sb.append("(field 'expression \"%s\" \"%s\")\n".formatted(aFieldName, aFieldValue.toString()))
        }

        override fun fieldIdent(aFieldName: String, aFieldValue: IdentExpression) {
            val w4 = SmallWriter2(objMap)

            aFieldValue.serializeTo(w4)

            sb.append("(field 'ident \"%s\" %s)\n".formatted(aFieldName, w4.sb)) // !!
        }

        override fun fieldInteger(aFieldName: String, aFieldValue: Int) {
            sb.append("(field 'integer \"%s\" \"%d\")\n".formatted(aFieldName, aFieldValue))
        }

        override fun <E> fieldList(aFieldName: String, aFieldValue: List<E>) {
            val sb2 = StringBuilder()

            var i = 1
            for (e in aFieldValue) {
                if (!(OS_Element::class.java.isInstance(e))) {
                    sb2.append("(item %d \"%s\")\n".formatted(i++, e.toString()))
                } else {
                    val r = this.createRef(e as OS_Element)
                    //					this.fieldRef("item%d".formatted(i++), r);
                    sb2.append("(item %d \"%s\")\n".formatted(i++, r.name()))
                }
            }

            sb.append("(field 'list \"%s\" %s)\n".formatted(aFieldName, sb2.toString()))

            val rs = ArrayList(objMap.values)
            val w3 = SmallWriter2(objMap)
            for (r in rs) {
                if (!Companion.insides.contains(r)) {
                    w3.sb.append("(REF \"%s\" \"%s\"\n".formatted(r.name(), r.get().javaClass.name))

                    Companion.insides.add(r)
                    r.get().serializeTo(w3)
                    w3.sb.append(")\n")
                }
            }
            sb.append(w3.sb)
        }

        override fun fieldRef(aParent: String, aRef: SW_Ref) {
            sb.append("(field 'ref \"%s\" \"%s\")\n".formatted(aParent, aRef.name()))
        }

        override fun fieldString(aFieldName: String, aFieldValue: String) {
            sb.append("(field 'string \"%s\" \"%s\")\n".formatted(aFieldName, aFieldValue))
        }

        override fun fieldToken(aFieldName: String, aFieldValue: Token) {
            sb.append("(field 'token \"%s\" \"%s\")\n".formatted(aFieldName, aFieldValue))
        }

        override fun fieldTypenameList(aInheritance: String, aInh: SW_TypenameList) {
            sb.append("(field 'typeNameList \"%s\" ".formatted(aInheritance))
            for (item in aInh.items()) {
                sb.append("(field 'typeName \"%s\")\n".formatted(item))
            }
            sb.append(")\n")
        }

        override fun getString(): String {
            return sb.toString()
        }
    }

    fun __modules2(aModuleList: Collection<WorldModule>): SmallWriter1 {
        val sw = SmallWriter1()

        for (module in aModuleList) {
            module.module().serializeTo(sw.newWriter(module.module()))
        }

        return sw
    }

    companion object {
        private val insides: MutableSet<SW_Ref> = HashSet()
    }
}
