package tripleo.elijah.stages.deduce.tastic

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.i.NamespaceStatement
import tripleo.elijah.lang.i.TypeName
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.ForFunction
import tripleo.elijah.stages.deduce.IInvocation
import tripleo.elijah.stages.deduce.tastic.FT_FnCallArgs.DoAssignCall
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util2.ReadySupplier_1

class FT_FCA_FunctionDef(private val fd: FunctionDef, private val _dt2: DeduceTypes2) {
    private fun _inj(): DeduceTypes2Injector {
        return _dt2._inj()
    }

    fun loop2_i(aDoAssignCall: DoAssignCall, pte: ProcTableEntry,
                vte: VariableTableEntry, instructionIndex: Int) {
        val invocation: IInvocation?
        if (fd.parent === aDoAssignCall.generatedFunction().fd.parent) {
            invocation = aDoAssignCall.dc().getInvocation((aDoAssignCall.generatedFunction() as EvaFunction))
        } else {
            if (fd.parent is NamespaceStatement) {
                val ni = aDoAssignCall.dc()
                        .registerNamespaceInvocation(fd.parent as NamespaceStatement?)
                invocation = ni
            } else if (fd.parent is ClassStatement) {
                val classStatement = fd.parent as ClassStatement
                var ci = _inj().new_ClassInvocation(classStatement, null, ReadySupplier_1<DeduceTypes2>(_dt2))
                val genericPart: List<TypeName> = classStatement.getGenericPart()
                if (genericPart.size > 0) {
                    // TODO handle generic parameters somehow (getInvocationFromBacklink?)
                }
                ci = aDoAssignCall.dc().registerClassInvocation(ci!!)
                invocation = ci
            } else throw NotImplementedException()
        }
        aDoAssignCall.dc().forFunction(aDoAssignCall.dc().newFunctionInvocation(fd, pte, invocation!!), object : ForFunction() {
            override fun typeDecided(aType: GenType?) {
                if (!vte.typeDeferred_isPending()) {
                    if (vte.resolvedType() == null) {
                        val ci = aDoAssignCall.dc().genCI(aType!!, null)
                        vte.typeTableEntry.genTypeCI(ci)
                        ci!!.resolvePromise().then { result -> vte.resolveTypeToClass(result) }
                    }
                    aDoAssignCall.log.err("2041 type already found $vte")
                    return  // type already found
                }
                // I'm not sure if below is ever called
                val tte = aDoAssignCall.generatedFunction().newTypeTableEntry(TypeTableEntry.Type.TRANSIENT,
                        aDoAssignCall.dc().gt(aType!!), pte.__debug_expression, pte)
                vte.addPotentialType(instructionIndex, tte)
            }
        })
    }
}
