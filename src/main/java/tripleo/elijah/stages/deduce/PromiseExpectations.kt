package tripleo.elijah.stages.deduce

internal class PromiseExpectations {
    var counter: Long = 0

    var exp: MutableList<PromiseExpectation<*>> = ArrayList()

    fun add(aExpectation: PromiseExpectation<*>) {
        counter++
        aExpectation.setCounter(counter)
        exp.add(aExpectation)
    }

    fun check() {
        for (promiseExpectation in exp) {
            if (!promiseExpectation.isSatisfied) promiseExpectation.fail()
        }
    }
}
