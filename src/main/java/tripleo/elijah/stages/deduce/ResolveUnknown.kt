/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Locatable
import tripleo.elijah.util2.UnintendedUseException
import java.io.PrintStream

/**
 * Created 9/9/21 6:25 AM
 */
class ResolveUnknown : Diagnostic {
    override fun code(): String {
        return "E1003"
    }

    private fun message(): String {
        return "Can't resolve variable"
    }

    override fun primary(): Locatable {
        throw UnintendedUseException("ndjsaklfdsajlkfdasjkl")
    }

    override fun report(stream: PrintStream) {
        stream.printf("---[%s]---: %s%n", code(), message())
        // linecache.print(primary);
        for (sec in secondary()) {
            // linecache.print(sec)
        }
        stream.flush()
    }

    override fun secondary(): List<Locatable?> {
        throw UnintendedUseException("ndjsaklfdsajlkfdasjkl")
    }

    override fun severity(): Diagnostic.Severity {
        return Diagnostic.Severity.ERROR
    }
}
