/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce.declarations

import lombok.Getter
import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.VariableStatement
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.deduce.DeduceElementWrapper
import tripleo.elijah.stages.deduce.IInvocation
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_fn.GenType

/**
 * Created 6/27/21 1:41 AM
 */
class DeferredMember(// 24/01/04 back and forth
        @JvmField @field:Getter val parent: DeduceElementWrapper, // 24/01/04 back and forth
        @JvmField @field:Getter val invocation: IInvocation,
        @field:Getter private val variableStatement: VariableStatementImpl) {
    class DeferredMemberInjector {
        fun new_DeferredObject__EvaNode(): DeferredObject<EvaNode, Void, Void> {
            return DeferredObject()
        }

        fun new_DeferredObject__GenType(): DeferredObject<GenType, Diagnostic, Void> {
            return DeferredObject()
        }
    }

    private val typePromise = DeferredObject<GenType, Diagnostic, Void>()

    private val externalRef = DeferredObject<EvaNode, Void, Void>()

    private val __inj = DeferredMemberInjector()

    fun _inj(): DeferredMemberInjector {
        return __inj
    }

    fun externalRef(): Promise<EvaNode, Void, Void> {
        return externalRef.promise()
    }

    fun externalRefDeferred(): DeferredObject<EvaNode, Void, Void> {
        return externalRef
    }

    override fun toString(): String {
        return "DeferredMember{" + "parent=" + parent + ", variableName=" + variableStatement.name + '}'
    }

    fun typePromise(): Promise<GenType, Diagnostic, Void> {
        return typePromise
    }

    // for DeducePhase
    fun typeResolved(): DeferredObject<GenType, Diagnostic, Void> {
        return typePromise
    }

    fun getVariableStatement(): VariableStatement {
        // 24/01/04 back and forth
        return this.variableStatement
    }
} //
//
//

