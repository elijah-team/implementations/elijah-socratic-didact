package tripleo.elijah.stages.gen_fn

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.NamespaceStatement
import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.lang.i.TypeName
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.nextgen.DR_Type
import tripleo.elijah.stages.deduce.nextgen.SGTA_SetDrType
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action_Arena

class ForwardingGenType(private val base: GenType, aB: Boolean) : GenType {
	enum class Mode {
		GENERATIONAL, NORMAL
	}

	var mode: Mode
	private var g: setup_GenType_Action_Arena? = null

	private val list: MutableList<setup_GenType_Action> = ArrayList()

	init {
		if (aB) {
			mode = Mode.GENERATIONAL
			g = setup_GenType_Action_Arena()
		} else {
			mode = Mode.NORMAL
			g = null
		}
	}

	override fun asString(): String {
		return base.asString()
	}

	override fun copy(aGenType: GenType) {
		if (mode == Mode.GENERATIONAL) {
			list.add(SGTA_CopyGenType(aGenType))
		} else {
			base.copy(aGenType)
		}
	}

//	override
//	fun genCI(
//			aGenericTypeName: TypeName, deduceTypes2: DeduceTypes2, errSink: ErrSink, phase: DeducePhase,
//	) ClassInvocation? {
////		return base.genCI(aGenericTypeName, deduceTypes2, errSink, phase)
//	}

	override fun genCIForGenType2(deduceTypes2: DeduceTypes2?) {
		if (deduceTypes2 == null) return

		// if (base.getCi() == null) return;
		this.unsparkled()

		base.genCIForGenType2(deduceTypes2)
	}

	override fun genCIForGenType2__(aDeduceTypes2: DeduceTypes2) {
		base.genCIForGenType2__(aDeduceTypes2)
	}

	override fun getCi(): IInvocation {
		return base.ci
	}

	override fun getFunctionInvocation(): FunctionInvocation {
		return base.functionInvocation
	}

	override fun getNode(): EvaNode {
		return base.node
	}

	override fun getNonGenericTypeName(): TypeName {
		return base.nonGenericTypeName
	}

	override fun getResolved(): OS_Type {
		return base.resolved
	}

	override fun getResolvedn(): NamespaceStatement {
		return base.resolvedn
	}

	override fun getTypeName(): OS_Type {
		return base.typeName
	}

	override fun isNull(): Boolean {
		return base.isNull
	}

	override fun set(aType: OS_Type) {
		if (mode == Mode.GENERATIONAL) {
			list.add(SGTA_Set(aType))
		} else {
			base.set(aType)
		}
	}

	override fun setCi(aInvocation: IInvocation) {
		if (mode == Mode.GENERATIONAL) {
			list.add(SGTA_SetCi(aInvocation))
		} else {
			base.ci = aInvocation
		}
	}

	override fun setDrType(aDrType: DR_Type) {
		list.add(SGTA_SetDrType(aDrType))
	}

	override fun setFunctionInvocation(aFi: FunctionInvocation) {
		if (mode == Mode.GENERATIONAL) {
			list.add(SGTA_SetFunctionInvocation(aFi))
		} else {
			base.functionInvocation = aFi
		}
	}

	override fun setNode(aResult: EvaNode) {
		if (mode == Mode.GENERATIONAL) {
			list.add(SGTA_SetNode(aResult))
		} else {
			base.node = aResult
		}
	}

	override fun setNonGenericTypeName(typeName: TypeName) {
		if (mode == Mode.GENERATIONAL) {
			list.add(SGTA_SetNonGenericTypeName(typeName))
		} else {
			base.nonGenericTypeName = typeName
		}
	}

	override fun setResolved(aOSType: OS_Type) {
		if (mode == Mode.GENERATIONAL) {
			list.add(SGTA_SetResolvedType(aOSType))
		} else {
			base.resolved = aOSType
		}
	}

	override fun setResolvedn(parent: NamespaceStatement) {
		if (mode == Mode.GENERATIONAL) {
			list.add(SGTA_RestReolvedn(parent))
		} else {
			base.resolvedn = parent
		}
	}

	override fun setTypeName(aType: OS_Type) {
		if (mode == Mode.GENERATIONAL) {
			list.add(SGTA_SetTypeNaeme(aType))
		} else {
			base.typeName = aType
		}
	}

	override fun genCI(classStatement: ClassStatement, deduceTypes2: DeduceTypes2?, errSink: ErrSink, phase: DeducePhase): ClassInvocation {
		TODO("Not yet implemented")
	}

	override fun genCI(nonGenericTypeName: TypeName?, deduceTypes2: DeduceTypes2?, errSink: ErrSink?, phase: DeducePhase?): ClassInvocation {
		TODO("Not yet implemented")
	}

//	override fun genCI(classStatement: ClassStatement, deduceTypes2: DeduceTypes2?, errSink: ErrSink, phase: DeducePhase) {
//		TODO("Not yet implemented")
//	}

	fun unsparkled(): GenType {
		if (g != null) {
			for (setupGenTypeAction in list) {
				setupGenTypeAction.run(base, g!!)
			}
			list.clear()
			g!!.clear()
		}
		return this
	}
}
