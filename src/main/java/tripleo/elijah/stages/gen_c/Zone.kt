package tripleo.elijah.stages.gen_c

import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.IdentTableEntry
import tripleo.elijah.stages.gen_fn.VariableTableEntry
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument

class Zone(private val generateC: GenerateC) {
    private val members: MutableMap<Any, ZoneMember> = HashMap()

    operator fun get(aVarTableEntry: VariableTableEntry, aGf: BaseEvaFunction): ZoneVTE? {
        if (members.containsKey(aVarTableEntry)) return members[aVarTableEntry] as ZoneVTE?

        val r: ZoneVTE = ZoneVTE__1(aVarTableEntry, aGf, this.generateC)
        members[aVarTableEntry] = r
        return r
    }

    fun getPath(aIdentIA: IdentIA): ZonePath? {
        val s: List<InstructionArgument> = _getIdentIAPathList(aIdentIA)

        if (members.containsKey(aIdentIA)) return members[aIdentIA] as ZonePath?

        val r = ZonePath(aIdentIA, s)
        members[aIdentIA] = r
        return r
    }

    operator fun get(aIdentTableEntry: IdentTableEntry, aGf: BaseEvaFunction): ZoneITE? {
        if (members.containsKey(aIdentTableEntry)) return members[aIdentTableEntry] as ZoneITE?

        val r: ZoneITE = ZoneITE__1(aIdentTableEntry, aGf, this.generateC)
        members[aIdentTableEntry] = r
        return r
    }

    operator fun get(target: IdentIA): ZoneITE? {
        val gf = target.gf
        val identTableEntry = gf.getIdentTableEntry(target.index)
        val zi = this.get(identTableEntry, gf)
        return zi
    } // public GI_Item get(final EvaNode aGeneratedNode) {
    // }
}
