/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jdeferred2.DoneCallback
import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.lang.i.*
import tripleo.elijah.nextgen.reactive.DefaultReactive
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.DeduceTypes2.ExpectationBase
import tripleo.elijah.stages.deduce.Resolve_Ident_IA.DeduceElementIdent
import tripleo.elijah.stages.deduce.nextgen.DR_Ident
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_IdentTableEntry
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.Eventual
import java.util.*
import java.util.function.Consumer

/**
 * Created 9/12/20 10:27 PM
 */
class IdentTableEntry(
    val index: Int,
    ident: IdentExpression,
    context: Context,
    aBaseEvaFunction: BaseEvaFunction,
) : BaseTableEntry1(), Constructable, TableEntryIV, ExpectationBase, IDeduceResolvable {
    private inner class __StatusListener__ITE_SetResolvedElement() : StatusListener {
        override fun onChange(eh: IElementHolder, newStatus: Status) {
            if (newStatus == Status.KNOWN) {
                resolvedElement = (eh.getElement())
            }
        }
    }

    inner class _Reactive_IDTE() : DefaultReactive() {
        override fun <IdentTableEntry> addListener(t: Consumer<IdentTableEntry>?) {
            throw IllegalStateException("Error")
        }

        fun <IdentTableEntry> addResolveListener(t: Consumer<IdentTableEntry>) {
            _p_resolveListenersPromise
                    .then { t::accept }
        }
    }

    class ITE_Resolver_Result(private val element: OS_Element) {
        fun element(): OS_Element {
            return element
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that: ITE_Resolver_Result = obj as ITE_Resolver_Result
            return (this.element == that.element)
        }

        override fun hashCode(): Int {
            return Objects.hash(element)
        }

        override fun toString(): String {
            return ("ITE_Resolver_Result[" +
                    "element=" + element + ']')
        }
    }

    val _p_resolvedElementPromise: DeferredObject<OS_Element, ResolveError, Void> = DeferredObject()
    private val _p_backlinkSet: DeferredObject<InstructionArgument?, Void, Void> = DeferredObject()
    private val _p_constructableDeferred: DeferredObject<ProcTableEntry, Void, Void> = DeferredObject()
    private val _p_fefiDone: DeferredObject<GenType, Void, Void> = DeferredObject()
    private val _p_resolveListenersPromise: DeferredObject<IdentTableEntry, Void, Void> = DeferredObject()
    val deduceElement: DeduceElementIdent = DeduceElementIdent(this)
    internal val ident: EvaExpression<IdentExpression>
    val pc: Context
    private val _definedFunction: BaseEvaFunction
    var externalRef__: EvaNode? = null
    private var resolvedType: EvaNode? = null
    var constructable_pte: ProcTableEntry? = null
    var fefi: Boolean = false
    var potentialTypes: MutableMap<Int, TypeTableEntry> = HashMap()
    var preUpdateStatusListenerAdded: Boolean = false
    @JvmField
    var resolveExpectation: PromiseExpectation<String>? = null
    @JvmField
    var type: TypeTableEntry? = null
        var _ident: DR_Ident? = null
//             get() = field
    var backlink: InstructionArgument? = null
//        get() = field
        set(value) {
            field = value
            _p_backlinkSet.resolve(value)
        }
    var insideGetResolvedElement: Boolean = false

    private var _de3: DeduceElement3_IdentTableEntry? = null

    private var _reactive: _Reactive_IDTE? = null

    private var _resolver_result: ITE_Resolver_Result? = null

    val reactiveEventual: Eventual<_Reactive_IDTE?> = Eventual()

    private val _onExternalRef: Eventual<EvaNode> = Eventual()

    private val resolvers: MutableList<ITE_Resolver> = ArrayList()

    @JvmField
    var _cheat_variableStatement: VariableStatement? = null // DTR_VariableStatement 07/30

    init {
        this.ident = EvaExpression(ident, this)
        this.pc = context

        this._definedFunction = aBaseEvaFunction

        addStatusListener(__StatusListener__ITE_SetResolvedElement())
        setupResolve()

        _p_resolvedElementPromise.then(DoneCallback({ element: OS_Element -> this.resolveLanguageLevelConstruct(element) }))
        typeResolve!!.typeResolution().then(DoneCallback({ gt: GenType? ->
            if (type != null && type!!.genType != null) // !! 07/30
                type!!.genType.copy(gt)
        }))
    }

    fun _generatedFunction(): BaseEvaFunction {
        return (__gf)!!
    }

    fun addPotentialType(instructionIndex: Int, tte: TypeTableEntry) {
        potentialTypes.put(instructionIndex, tte)
    }

    fun addResolver(aResolver: ITE_Resolver) {
        resolvers.add(aResolver)
    }

    fun backlinkSet(): Promise<InstructionArgument?, Void, Void> {
        return _p_backlinkSet.promise()
    }

    fun buildDeducePath(generatedFunction: BaseEvaFunction?): DeducePath {
        val x: List<InstructionArgument> = BaseEvaFunction.Companion._getIdentIAPathList(IdentIA(index, generatedFunction!!))
        return DeducePath(this, x)
    }

    fun calculateResolvedElement() {
        val resolved_element: OS_Element? = deduceElement.resolvedElement
        if (resolved_element != null) {
            if (!_p_resolvedElementPromise.isResolved()) {
                _p_resolvedElementPromise.resolve(resolved_element)
            }
        } else {
            // 08/13 tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("283283 resolution failure for " +
            // dei.getIdentTableEntry().getIdent().getText());
        }
    }

    override fun constructablePromise(): Promise<ProcTableEntry, Void, Void> {
        return _p_constructableDeferred.promise()
    }

    fun evaExpression(): EvaExpression<IdentExpression> {
        return ident
    }

    override fun expectationString(): String {
        return "IdentTableEntry{" + "index=" + index + ", ident=" + ident.get() + ", backlink=" + backlink + "}"
    }

    fun externalRef(): EvaNode? {
        return externalRef__
    }

    fun fefiDone(aGenType: GenType) {
        if (_p_fefiDone.isPending()) _p_fefiDone.resolve(aGenType)
    }

    val deduceElement3: DeduceElement3_IdentTableEntry
        get() {
            return getDeduceElement3(this._deduceTypes2(), __gf)
        }

    fun getDeduceElement3(aDeduceTypes2: DeduceTypes2?,
                          aGeneratedFunction: BaseEvaFunction?): DeduceElement3_IdentTableEntry {
        if (_de3 == null) {
            _de3 = DeduceElement3_IdentTableEntry(this)
            _de3!!.deduceTypes2 = aDeduceTypes2
            _de3!!.generatedFunction = aGeneratedFunction
        }
        return _de3!!
    }

    val definedIdent: DR_Ident
        get() {
            return _definedFunction.getIdent(this)
        }

    fun getIdent(): IdentExpression? {
        return ident.get()
    }

    override var resolvedElement: OS_Element?
        get() {
            val resolved_element: OS_Element?

            if (_p_elementPromise.isResolved()) {
                val r: Array<OS_Element?> = arrayOfNulls(1)
                _p_elementPromise.then(DoneCallback({ x: OS_Element? -> x.also { r[0] = it } }))
                return r.get(0)
            }

            if (insideGetResolvedElement) return null
            insideGetResolvedElement = true
            resolved_element = deduceElement.resolvedElement
            if (resolved_element != null) {
                if (_p_elementPromise.isResolved()) {
                    NotImplementedException.raise()
                } else {
                    _p_elementPromise.resolve(resolved_element)
                }
            }
            insideGetResolvedElement = false
            return resolved_element
        }
        set(resolvedElement) {
            super.resolvedElement = resolvedElement
        }

    fun hasResolvedElement(): Boolean {
        return !_p_elementPromise.isPending()
    }

    val isResolved: Boolean
        get() {
            return resolvedType != null
        }

    fun makeType(aGeneratedFunction: BaseEvaFunction, aType: TypeTableEntry.Type,
                 aExpression: IExpression) {
        type = aGeneratedFunction.newTypeTableEntry(aType, null, aExpression, this)
    }

    fun makeType(aGeneratedFunction: BaseEvaFunction, aType: TypeTableEntry.Type,
                 aOS_Type: OS_Type) {
        type = aGeneratedFunction.newTypeTableEntry(aType, aOS_Type, (getIdent())!!, this)
    }

    fun onExternalRef(cb: DoneCallback<EvaNode>?) {
        _onExternalRef.then(cb)
    }

    fun onFefiDone(aCallback: DoneCallback<GenType>?) {
        _p_fefiDone.then(aCallback)
    }

    fun onResolvedElement(cb: DoneCallback<OS_Element>?) {
        _p_resolvedElementPromise.then((cb))
    }

    fun onType(phase: DeducePhase, callback: OnType?) {
        phase.onType(this, (callback)!!)
    }

    // @SuppressFBWarnings("NP_NONNULL_RETURN_VIOLATION")
    fun potentialTypes(): Collection<TypeTableEntry> {
        return potentialTypes.values
    }

    fun reactive(): _Reactive_IDTE {
        if (_reactive == null) {
            _reactive = _Reactive_IDTE()
        }
        if (_de3 != null) {
            val ce: CompilationEnclosure = _de3!!.deduceTypes2()._ce()
            ce.reactiveJoin(_reactive)
        }

        if (!reactiveEventual.isResolved) {
            // TODO !!
            reactiveEventual.resolve(_reactive)
        }

        return _reactive!!
    }

    fun resolvedType(): EvaNode? {
        return resolvedType
    }

    private fun resolveLanguageLevelConstruct(element: OS_Element) {
        assert(__gf != null)
        assert(this._deduceTypes2() != null)
        if (element is FunctionDef) {
            NotImplementedException.raise()
        }

        _p_elementPromise.then(DoneCallback({ x: OS_Element? ->
            NotImplementedException.raise()
        }))
    }

    fun resolvers_round() {
        if (_resolver_result != null) return

        for (resolver: ITE_Resolver in resolvers) {
            if (!resolver.isDone()) {
                resolver.check()
            }

            if (resolver.isDone()) {
                _resolver_result = resolver.getResult() // TODO resolve here?? 07/20
                break
            }

            // ... ??
        }
    }

    override fun resolveTypeToClass(gn: EvaNode) {
        resolvedType = gn
        assert(type != null)
        if (type != null) // TODO maybe find a more robust solution to this, like another Promise? or just
        // setType? or onPossiblesResolve?
            type!!.resolve(gn) // TODO maybe this obviates the above?

        if (!_p_resolveListenersPromise.isResolved()) // FIXME 06/16
            _p_resolveListenersPromise.resolve(this)
    }

    override fun setConstructable(aPte: ProcTableEntry) {
        constructable_pte = aPte
        if (_p_constructableDeferred.isPending()) _p_constructableDeferred.resolve(constructable_pte)
        else {
            // final Holder<ProcTableEntry> holder = new Holder<ProcTableEntry>();
            _p_constructableDeferred.then(DoneCallback({ el: ProcTableEntry? ->
                SimplePrintLoggerToRemoveSoon
                        .println_err_2(String.format("Setting constructable_pte twice 1) %s and 2) %s", el, aPte))
            }))
        }
    }

    fun setDeduceTypes2(aDeduceTypes2: DeduceTypes2, aContext: Context?,
                        aGeneratedFunction: BaseEvaFunction) {
        deduceElement.setDeduceTypes2(aDeduceTypes2, aContext, aGeneratedFunction)
    }

    fun setExternalRef_(aResult: EvaNode) {
        externalRef__ = aResult
        _onExternalRef.resolve(aResult)
    }

    override fun setGenType(aGenType: GenType) {
        if (type != null) {
            type!!.genType.copy(aGenType)
        } else {
            throw IllegalStateException("idte-102 Attempting to set a null type")
            //			tripleo.elijah.util.Stupidity.println_err_2("idte-102 Attempting to set a null type");
        }
    }

    override fun toString(): String {
        return ("IdentTableEntry{" + "index=" + index + ", ident=" + ident.get() + ", backlink=" + backlink + ", status="
                + status + ", resolved=" + resolvedType + ", potentialTypes=" + potentialTypes + ", type=" + type + '}')
    }
} //

private fun <D, F, P> DeferredObject<D, F, P>.then(doneCallback: DoneCallback<D>) {
    this.then(doneCallback)
}
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
