package tripleo.elijah.stages.deduce

import java.util.function.Supplier

data class RegisterClassInvocation_env(val classInvocation: ClassInvocation,
                                       val deduceTypes2Supplier: Supplier<DeduceTypes2>,
                                       val deducePhaseSupplier: Supplier<DeducePhase>?
) {
	fun classInvocation(): ClassInvocation {
		return classInvocation
	}

	fun deduceTypes2Supplier(): Supplier<DeduceTypes2> {
		return deduceTypes2Supplier
	}

	fun deducePhaseSupplier(): Supplier<DeducePhase> {
		return deducePhaseSupplier!!
	}
}
