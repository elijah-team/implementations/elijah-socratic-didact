package tripleo.elijah.nextgen.inputtree

enum class EIT_InputType {
    ELIJAH_SOURCE, EZ_FILE
}
