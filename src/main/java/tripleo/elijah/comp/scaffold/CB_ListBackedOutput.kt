package tripleo.elijah.comp.scaffold

import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.CB_OutputString
import tripleo.elijah.diagnostic.CodedOperationDiagnostic
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.util2.UnintendedUseException

class CB_ListBackedOutput : CB_Output {
    private val _listBacking: MutableList<CB_OutputString> = ArrayList()

    override fun get(): List<CB_OutputString> {
        return _listBacking
    }

    override fun logProgress(number: Int, text: String) {
        if (number == 130) return

        //		tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4
        print(String.format("%d %s", number, text))
    }

    override fun print(s: String) {
        _listBacking.add(CB_OutputString { s })
    }

    override fun logProgress(aDiagnostic: Diagnostic) {
        if (aDiagnostic is CodedOperationDiagnostic<*>) {
            logProgress(aDiagnostic.intCode(), aDiagnostic.message())
        } else {
            // FIXME 10/20 dont worry about this yet
//			logProgress(aDiagnostic.code(), aDiagnostic.message());
            throw UnintendedUseException("see what this is")
        }
    }
}
