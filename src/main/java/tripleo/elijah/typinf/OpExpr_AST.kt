package tripleo.elijah.typinf

import tripleo.elijah.util.Helpers

/**
 * Binary operation between expressions.
 */
class OpExpr_AST(val op: String, val left: AstNode, val right: AstNode) : AstNode() {
    init {
        this._children = Helpers.List_of(this.left, this.right)
    }

    override fun toString(): String {
        return String.format("(%s %s %s)", left, op, right)
    }
}
