package tripleo.elijah.stages.gen_c

import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.util.Mode

internal class GCX_Construct(private val gi_proc: GI_ProcIA?, private val instruction1: Instruction, private val gc1: GenerateC) : EG_Statement {
    override val explanation: EX_Explanation
        get() = withMessage("GCX_Construct")

    override val text: String
        get() {
            val s = gi_proc!!.action_CONSTRUCT(instruction1, gc1)
            if (s.mode() == Mode.SUCCESS) {
                return s.success()!!.text
            }
            return "-------just make it compile---------------"
        }
}
