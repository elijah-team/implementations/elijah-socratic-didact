package tripleo.elijah.comp;

import tripleo.wrap.File;

public interface ET_SourceRoot {
    String getDirectoryName();
    File getDirectory();
}
