package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.nextgen.outputstatement.ReasonedStringListStatement
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.stages.instructions.InstructionFixedList
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.util.Helpers
import java.util.function.Supplier

internal class __Pte_Dispatch_IExpression_Statement(private val expression: IExpression,
                                                    private val instruction: Instruction,
                                                    private val gf: BaseEvaFunction,
                                                    private val gc: GenerateC) : ReasonedStringListStatement() {
    init {
        val z = this

        val ptex = expression as IdentExpression
        val text = ptex.text

        val xx = gf.vte_lookup(text)!!
        val sl3 = gc.getArgumentStrings { InstructionFixedList(instruction) }

        z.append(Emit.emit("/*424*/"), "emit-code")
        z.append(Supplier { gc.getRealTargetName((xx as IntegerIA), AOG.GET) }, "real-target-name")
        z.append("(", "open-brace")
        z.append(Helpers.String_join(", ", sl3), "arguments")
        z.append(");", "close-brace")
    }
}
