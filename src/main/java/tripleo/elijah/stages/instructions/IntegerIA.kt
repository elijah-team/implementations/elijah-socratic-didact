/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.instructions

import org.jdeferred2.Promise
import tripleo.elijah.stages.gen_fn.*

/**
 * Created 9/10/20 3:35 PM
 */
class IntegerIA(@JvmField val index: Int, @JvmField val gf: BaseEvaFunction) : InstructionArgument, Constructable {
    override fun constructablePromise(): Promise<ProcTableEntry, Void, Void> {
        return entry.constructablePromise()
    }

    val entry: VariableTableEntry
        get() = gf.getVarTableEntry(index)

    override fun resolveTypeToClass(aNode: EvaNode) {
        entry.resolveTypeToClass(aNode)
    }

    override fun setConstructable(aPte: ProcTableEntry) {
        entry.setConstructable(aPte)
    }

    override fun setGenType(aGenType: GenType) {
        entry.setGenType(aGenType)
    }

    override fun toString(): String {
        return "IntegerIA{index=$index}"
    }
} //
//
//

