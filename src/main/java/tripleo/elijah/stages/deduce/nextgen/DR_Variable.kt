package tripleo.elijah.stages.deduce.nextgen

import tripleo.elijah.lang.i.VariableStatement
import tripleo.elijah.stages.gen_fn.BaseEvaFunction

class DR_Variable(private val element: VariableStatement, private val baseEvaFunction: BaseEvaFunction) : DR_Item {
    fun declaredTypeIsEmpty(): Boolean {
        return element.typeName().isNull
    }
}
