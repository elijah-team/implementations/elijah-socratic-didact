/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_c

import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.comp.AccessBus
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.types.OS_FuncExprType
import tripleo.elijah.lang2.BuiltInTypes
import tripleo.elijah.nextgen.outputstatement.ReasonedStringListStatement
import tripleo.elijah.nextgen.reactive.ReactiveDimension
import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.garish.GarishClass
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_fn.EvaContainer.VarTableEntry
import tripleo.elijah.stages.gen_generic.*
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.logging.ElLog_
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.IFixedList
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.work.WorkJob
import tripleo.elijah.work.WorkList
import tripleo.elijah.work.WorkList__
import tripleo.elijah.work.WorkManager
import tripleo.util.buffer.Buffer
import java.util.*
import java.util.function.Supplier
import java.util.stream.Collectors

/**
 * Created 10/8/20 7:13 AM
 */
class GenerateC(
		val params: OutputFileFactoryParams,
		val fileGen__: GenerateResultEnv,
)

	: CodeGenerator, GenerateFiles, ReactiveDimension {

	val _repo: GI_Repo = GI_Repo(this)
	val _zone: Zone = Zone(this)
	val ce: CompilationEnclosure
	val errSink: ErrSink = params.errSink
	val LOG: ElLog
	private val a_directory: MutableMap<EvaNode, WhyNotGarish_Item> = HashMap()
	private val generateResultProgressive = GenerateResultProgressive()

	@JvmField
	var resultSink: GenerateResultSink? = null

	init {
		val mod = params.mod
		val verbosity = params.verbosity

		LOG = ElLog_(mod.fileName, verbosity, PHASE)

		ce = params.compilationEnclosure
		ce.accessBusPromise.then { ab: AccessBus ->
			ab.subscribePipelineLogic { pl: PipelineLogic? -> pl!!.addLog(LOG) }
		}

		ce.addReactiveDimension(this)

		ce.pipelineAccess.resolveWaitGenC(mod, this)
	}

	fun a_lookup(aGf: BaseEvaFunction): WhyNotGarish_Function? {
		if (a_directory.containsKey(aGf)) {
			return a_directory[aGf] as WhyNotGarish_Function?
		}

		val ncf = WhyNotGarish_Function(aGf, this)
		a_directory[aGf] = ncf
		return ncf
	}

	fun a_lookup(aGf: EvaConstructor): WhyNotGarish_Constructor? {
		if (a_directory.containsKey(aGf)) {
			return a_directory[aGf] as WhyNotGarish_Constructor?
		}

		val ncc1907 = WhyNotGarish_Constructor(aGf, this)
		a_directory[aGf] = ncc1907
		return ncc1907
	}

	fun a_lookup(en: EvaNamespace): WhyNotGarish_Namespace? {
		if (a_directory.containsKey(en)) {
			return a_directory[en] as WhyNotGarish_Namespace?
		}

		val ncn = WhyNotGarish_Namespace(en, this)
		a_directory[en] = ncn
		return ncn
	}

	override fun elLog(): ElLog {
		return this.LOG
	}

	override fun finishUp(aGenerateResult: GenerateResult, wm: WorkManager, aWorkList: WorkList) {
		for (value in ArrayList(a_directory.values)) {
			if (!value.hasFileGen()) {
				value.provideFileGen(fileGen__)
			}
		}
	}

	override fun generate_class(aFileGen: GenerateResultEnv, x: EvaClass) {
		val gr = aFileGen.gr()
		val aResultSink = aFileGen.resultSink()

		val lc = aResultSink.getLivingClassForEva(x)!! // TODO could also add _living property

		(lc.garish as GarishClass).garish(this, gr, aResultSink)
	}

	override fun generate_constructor(
			aEvaConstructor: EvaConstructor,
			gr: GenerateResult,
			wl: WorkList,
			__aResultSink: GenerateResultSink,
			aWorkManager: WorkManager,
			aFileGen: GenerateResultEnv,
	) {
		// TODO 10/16 argument ordering
		generateCodeForConstructor(aFileGen, aEvaConstructor)
		postGenerateCodeForConstructor(aEvaConstructor, wl, aFileGen)
	}

	override fun generate_function(
			aEvaFunction: EvaFunction,
			gr: GenerateResult,
			wl: WorkList,
			aResultSink: GenerateResultSink,
	) {
		generateCodeForMethod(fileGen__, aEvaFunction)
		postGenerateCodeForFunction(aEvaFunction, wl, fileGen__)
	}

	override fun generate_namespace(
			x: EvaNamespace,
			gr: GenerateResult,
			aResultSink: GenerateResultSink,
	) {
		val ln = aResultSink.getLivingNamespaceForEva(x) // TODO could also add _living property
		ln!!.garish.garish(this, gr, aResultSink)

		val yf = a_lookup(x)

		//		yf.
	}

	override fun generateCode(
			lgn: Collection<EvaNode>,
			aFileGen: GenerateResultEnv,
	): GenerateResult {
		val gr: GenerateResult = Old_GenerateResult()
		val wl: WorkList = WorkList__()

		val wm = aFileGen.wm()
		val aResultSink = aFileGen.resultSink()

		for (evaNode in lgn) {
			if (evaNode is EvaFunction) {
				generate_function(evaNode, gr, wl, aResultSink)
				if (!wl.isEmpty) wm.addJobs(wl)
			} else if (evaNode is EvaContainerNC) {
				evaNode.generateCode(fileGen__, this)
			} else if (evaNode is EvaConstructor) {
				generate_constructor(evaNode, gr, wl, aResultSink, wm, aFileGen)
				if (!wl.isEmpty) wm.addJobs(wl)
			}
		}

		return gr
	}

	override fun <T : Any?> getFileGen(): GenerateResultEnv {
		return this.fileGen__
	}

	//private void generateCodeForConstructor(final @NotNull EvaConstructor aEvaConstructor,
	//										final @NotNull WorkList aWorkList,
	//                                        final @NotNull GenerateResultEnv aFileGen) {
	//	if (aEvaConstructor.getFD() != null) {
	//		Generate_Code_For_Method gcfm = new Generate_Code_For_Method(this, LOG);
	//		gcfm.generateCodeForConstructor(aEvaConstructor, aFileGen);
	//	}
	//}
	//public void generateCodeForConstructor(final GenerateResultEnv aFileGen, final EvaConstructor gf) {
	//	final WhyNotGarish_Constructor cc = this.a_lookup(gf);
	//
	//	cc.resolveFileGenPromise(aFileGen);
	//}
	//public void generateCodeForMethod(final GenerateResultEnv aFileGen, final BaseEvaFunction aEvaFunction) {
	//	final WhyNotGarish_Function cf = this.a_lookup(aEvaFunction);
	//
	//	cf.resolveFileGenPromise(aFileGen);
	//}
	private fun generateIdent(identTableEntry: IdentTableEntry) {
		assert(identTableEntry.isResolved)
		val x = identTableEntry.resolvedType()

		val gr = fileGen__.gr()
		val resultSink1 = fileGen__.resultSink()
		val wl = fileGen__.wl()

		if (x is EvaClass) {
			generate_class(fileGen__, x)
		} else if (x is EvaFunction) {
			wl.addJob(WlGenerateFunctionC(fileGen__, x, this))
		} else {
			LOG.err(x.toString())
			throw NotImplementedException()
		}
	}

	//public GenerateResultProgressive generateResultProgressive() {
	//	return generateResultProgressive;
	//}
	//	@NotNull
	//	List<String> getArgumentStrings(final @NotNull BaseEvaFunction gf, final @NotNull Instruction instruction) {
	//		final List<String> sl3       = new ArrayList<String>();
	//		final int          args_size = instruction.getArgsSize();
	//		for (int i = 1; i < args_size; i++) {
	//			final InstructionArgument ia = instruction.getArg(i);
	//			if (ia instanceof IntegerIA) {
	////				VariableTableEntry vte = gf.getVarTableEntry(DeduceTypes2.to_int(ia));
	//				final String realTargetName = getRealTargetName(gf, (IntegerIA) ia, Generate_Code_For_Method.AOG.GET);
	//				sl3.add(Emit.emit("/*669*/") + realTargetName);
	//			} else if (ia instanceof IdentIA) {
	//				final CReference reference = new CReference(_repo, ce);
	//				reference.getIdentIAPath((IdentIA) ia, Generate_Code_For_Method.AOG.GET, null);
	//				String text = reference.build();
	//				sl3.add(Emit.emit("/*673*/") + text);
	//			} else if (ia instanceof final @NotNull ConstTableIA c) {
	//				ConstantTableEntry cte = gf.getConstTableEntry(c.getIndex());
	//				String             s   = new GetAssignmentValue().const_to_string(cte.initialValue);
	//				sl3.add(s);
	//				int y = 2;
	//			} else if (ia instanceof ProcIA) {
	//				LOG.err("740 ProcIA");
	//				throw new NotImplementedException();
	//			} else {
	//				LOG.err(ia.getClass().getName());
	//				throw new IllegalStateException("Invalid InstructionArgument");
	//			}
	//		}
	//		return sl3;
	//	}
	//	@NotNull
	//	List<String> getArgumentStrings(final @NotNull Supplier<IFixedList<InstructionArgument>> instructionSupplier) {
	//		final @NotNull List<String> sl3       = new ArrayList<String>();
	//		final int                   args_size = instructionSupplier.get().size();
	//		for (int i = 1; i < args_size; i++) {
	//			final InstructionArgument ia = instructionSupplier.get().get(i);
	//			if (ia instanceof IntegerIA) {
	////				VariableTableEntry vte = gf.getVarTableEntry(DeduceTypes2.to_int(ia));
	//				final String realTargetName = getRealTargetName((IntegerIA) ia, Generate_Code_For_Method.AOG.GET);
	//				sl3.add(Emit.emit("/*669*/") + realTargetName);
	//			} else if (ia instanceof IdentIA) {
	//				final CReference reference = new CReference(_repo, ce);
	//				reference.getIdentIAPath((IdentIA) ia, Generate_Code_For_Method.AOG.GET, null);
	//				final String text = reference.build();
	//				sl3.add(Emit.emit("/*673*/") + text);
	//			} else if (ia instanceof final @NotNull ConstTableIA c) {
	//				final ConstantTableEntry cte = c.getEntry();
	//				final String             s   = new GetAssignmentValue().const_to_string(cte.initialValue);
	//				sl3.add(s);
	//				final int y = 2;
	//			} else if (ia instanceof ProcIA) {
	//				LOG.err("740 ProcIA");
	//				throw new NotImplementedException();
	//			} else {
	//				LOG.err(ia.getClass().getName());
	//				throw new IllegalStateException("Invalid InstructionArgument");
	//			}
	//		}
	//		return sl3;
	//	}
	//public @NotNull List<String> getArgumentStrings(final @NotNull WhyNotGarish_BaseFunction aGf,
	//                                                final @NotNull Instruction aInstruction) {
	//	return getArgumentStrings(aGf.cheat(), aInstruction);
	//}
	fun getAssignmentValue(
			value_of_this: VariableTableEntry?,
			value: InstructionArgument?,
			gf: BaseEvaFunction,
	): String {
		val gav = GetAssignmentValue()
		if (value is FnCallArgs) {
			return gav.FnCallArgs(value, gf, LOG)
		}

		if (value is ConstTableIA) {
			return gav.ConstTableIA(value, gf)
		}

		if (value is IntegerIA) {
			return gav.IntegerIA(value, gf)
		}

		if (value is IdentIA) {
			return gav.IdentIA(value, gf)
		}

		LOG.err(String.format("783 %s %s", value!!.javaClass.name, value))
		return value.toString()
	}

	fun getAssignmentValue(
			aSelf: VariableTableEntry?,
			aRhs: InstructionArgument?,
			aGf: WhyNotGarish_BaseFunction,
	): String {
		return getAssignmentValue(aSelf, aRhs, aGf.cheat())
	}

	//@Override
	//public GenerateResultEnv getFileGen() {
	//	return _fileGen;
	//}
	fun getRealTargetName(
			gf: BaseEvaFunction,
			target: IdentIA,
			aog: AOG,
			value: String?,
	): String {
		var state = 0
		var code = -1
		val identTableEntry = gf.getIdentTableEntry(target.index)
		val ls = LinkedList<String>()
		// TODO in Deduce set property lookupType to denote what type of lookup it is: MEMBER, LOCAL, or CLOSURE
		var backlink = identTableEntry.backlink
		val text = identTableEntry.ident.get()
		if (backlink == null) {
			if (identTableEntry.resolvedElement is VariableStatement) {
				var vs = identTableEntry.resolvedElement as VariableStatement
				val parent: OS_Element? = vs.getParent()!!.getParent()
				if (parent !== gf.fd) {
					// we want identTableEntry.resolved which will be a EvaMember
					// which will have a container which will be either be a function,
					// statement (semantic block, loop, match, etc) or a EvaContainerNC
					val y = 2
					val er = identTableEntry.externalRef() // FIXME move to onExternalRef
					if (er is EvaContainerNC) {
						if (er !is EvaNamespace) {
							throw AssertionError()
						} else {
							//if (ns.isInstance()) {}
							state = 1
							code = er.code
						}
					}
				}
			}
			when (state) {
				0 -> ls.add(Emit.emit("/*912*/") + "vsc->vm" + text) // TODO blindly adding "vm" might not always work, also put in loop
				1 -> ls.add(Emit.emit("/*845*/") + String.format("zNZ%d_instance->vm%s", code, text))
				else -> throw IllegalStateException("Can't be here")
			}
		} else ls.add(Emit.emit("/*872*/") + "vm" + text) // TODO blindly adding "vm" might not always work, also put in loop

		while (backlink != null) {
			if (backlink is IntegerIA) {
				val realTargetName: String = getRealTargetName(gf, backlink, AOG.ASSIGN)
				ls.addFirst(Emit.emit("/*892*/") + realTargetName)
				backlink = null
			} else if (backlink is IdentIA) {
				val identIAIndex: Int = backlink.index
				val identTableEntry1 = gf.getIdentTableEntry(identIAIndex)
				val identTableEntryName = identTableEntry1.ident.get()
				ls.addFirst(Emit.emit("/*885*/") + "vm" + identTableEntryName) // TODO blindly adding "vm" might not always be right
				backlink = identTableEntry1.backlink
			} else throw IllegalStateException("Invalid InstructionArgument for backlink")
		}
		val reference = CReference(_repo, ce)
		reference.getIdentIAPath(target, aog, value)
		val path = reference.build()
		LOG.info("932 $path")
		val s = Helpers.String_join("->", ls)
		LOG.info("933 $s")
		return if (identTableEntry.resolvedElement is ConstructorDef || identTableEntry.resolvedElement is PropertyStatement /* || value != null*/) path
		else s
	}

	fun getRealTargetName(gf: BaseEvaFunction, target: IntegerIA, aog: AOG?): String {
		val varTableEntry = gf.getVarTableEntry(target.index)
		return getRealTargetName(gf, varTableEntry)
	}

	fun getRealTargetName(gf: BaseEvaFunction, varTableEntry: VariableTableEntry): String {
		val zone_vte = _zone[varTableEntry, gf]

		return zone_vte!!.realTargetName
	}

	fun _ce(): CompilationEnclosure {
		return ce
	}

	//@Override
	//public ElLog elLog() {
	//	return this.LOG;
	//}
	fun generateResultProgressive(): GenerateResultProgressive {
		return generateResultProgressive
	}

	fun getArgumentStrings(instructionSupplier: Supplier<IFixedList<InstructionArgument>>): List<String> {
		val sl3: MutableList<String> = ArrayList()
		val args_size = instructionSupplier.get().size()
		for (i in 1 until args_size) {
			val ia = instructionSupplier.get()[i]
			if (ia is IntegerIA) {
//				VariableTableEntry vte = gf.getVarTableEntry(DeduceTypes2.to_int(ia));
				val realTargetName = getRealTargetName(ia, AOG.GET)
				sl3.add(Emit.emit("/*669*/") + realTargetName)
			} else if (ia is IdentIA) {
				val reference = CReference(_repo, ce)
				reference.getIdentIAPath(ia, AOG.GET, null)
				val text = reference.build()
				sl3.add(Emit.emit("/*673*/") + text)
				// action {
				//   text: text
				//   , source: ia
				//   , sourceType: ES_Symbol()
				//      for convenience
				//      TaggedSymbol?? (sym, tag [, tagresolver??])
				//  , text { emit(...) text(...) ... } // reason(...)??!!
				// } // dsl
			} else if (ia is ConstTableIA) {
				val cte: ConstantTableEntry = ia.entry
				val s = GetAssignmentValue().const_to_string(cte.initialValue)
				sl3.add(s)
				val y = 2
			} else if (ia is ProcIA) {
				LOG.err("740 ProcIA")
				throw NotImplementedException()
			} else {
				LOG.err(ia.javaClass.name)
				throw IllegalStateException("Invalid InstructionArgument")
			}
		}
		return sl3
	}

	fun getRealTargetName(target: IntegerIA, aog: AOG?): String {
		val gf = target.gf
		val varTableEntry = gf.getVarTableEntry(target.index)

		val zone_vte = _zone[varTableEntry, gf]

		return zone_vte!!.realTargetName
	}

	fun getTypeName(tte: TypeTableEntry): String {
		return GetTypeName.forTypeTableEntry(tte)
	}

	@Deprecated("")
	fun getRealTargetName(aGf: WhyNotGarish_BaseFunction, aTarget: IntegerIA, aAOG: AOG?): String {
		return getRealTargetName(aGf.cheat(), aTarget, aAOG)
	}

	fun repo(): GI_Repo {
		return _repo
	}

	fun getArgumentStrings(aGf: WhyNotGarish_BaseFunction, aInstruction: Instruction): List<String> {
		return getArgumentStrings(aGf.cheat(), aInstruction)
	}

	fun getArgumentStrings(gf: BaseEvaFunction, instruction: Instruction): List<String> {
		val sl3: MutableList<String> = ArrayList()
		val args_size = instruction.argsSize
		for (i in 1 until args_size) {
			val ia = instruction.getArg(i)
			if (ia is IntegerIA) {
//				VariableTableEntry vte = gf.getVarTableEntry(DeduceTypes2.to_int(ia));
				val realTargetName = getRealTargetName(gf, ia, AOG.GET)
				sl3.add(Emit.emit("/*669*/") + realTargetName)
			} else if (ia is IdentIA) {
				val reference = CReference(_repo, ce)
				reference.getIdentIAPath(ia, AOG.GET, null)
				val text = reference.build()
				sl3.add(Emit.emit("/*673*/") + text)
			} else if (ia is ConstTableIA) {
				val cte = gf.getConstTableEntry(ia.index)
				val s = GetAssignmentValue().const_to_string(cte.initialValue)
				sl3.add(s)
				val y = 2
			} else if (ia is ProcIA) {
				LOG.err("740 ProcIA")
				throw NotImplementedException()
			} else {
				LOG.err(ia!!.javaClass.name)
				throw IllegalStateException("Invalid InstructionArgument")
			}
		}
		return sl3
	}

	fun getTypeName(aNode: EvaNode?): String {
		if (aNode is EvaClass) {
			return a_lookup(aNode)!!.typeNameString
		}
		if (aNode is EvaNamespace) {
			return a_lookup(aNode)!!.typeNameString
		}
		throw IllegalStateException("Must be class or namespace.")
	}

	fun a_lookup(aGc: EvaClass): WhyNotGarish_Class? {
		if (a_directory.containsKey(aGc)) {
			return a_directory[aGc] as WhyNotGarish_Class?
		}

		val ncc = WhyNotGarish_Class(aGc, this)
		a_directory[aGc] = ncc
		return ncc
	}

	@Deprecated("")
	fun getTypeName(typeName: TypeName): String {
		return GetTypeName.forTypeName(typeName, errSink)
	}

	fun getTypeNameForGenClass(aGenClass: EvaNode): String {
		return GetTypeName.getTypeNameForEvaNode(aGenClass)
	}

	fun getTypeNameGNCForVarTableEntry(o: VarTableEntry): String {
		val typeName: String
		if (o.resolvedType() != null) {
			val xx = o.resolvedType()
			typeName = if (xx is EvaClass) {
				GetTypeName.forGenClass(xx)
			} else if (xx is EvaNamespace) {
				GetTypeName.forGenNamespace(xx)
			} else throw NotImplementedException()
		} else {
			typeName = if (o.varType != null) getTypeName(o.varType)
			else "void*/*null*/"
		}
		return typeName
	}

	@Deprecated("")
	fun getTypeName(ty: OS_Type): String {
		return GetTypeName.forOSType(ty, LOG)
	}

	fun _LOG(): ElLog {
		return LOG
	}

	internal enum class GetTypeName {
		;

		companion object {
			@Deprecated("")
			fun forOSType(ty: OS_Type, LOG: ElLog): String {
				requireNotNull(ty) { "ty is null" }
				//
				val z: String
				when (ty.type) {
					OS_Type.Type.USER_CLASS -> {
						val name: String = __Tests_OS_Type.Companion.boundedClassName_NormalTypeName(ty)
						z = Emit.emit("/*443*/") + String.format("Z%d/*%s*/", -4,  /*el._a.getCode()*/name)
					}

					OS_Type.Type.FUNCTION -> z = "<function>"
					OS_Type.Type.FUNC_EXPR -> {
						z = "<function>"
						val fe = ty as OS_FuncExprType
						val y = 2
					}

					OS_Type.Type.USER -> {
						val typeName = ty.typeName
						LOG.err("Warning: USER TypeName in GenerateC $typeName")
						val s = typeName.toString()
						z = if (s == "Unit") "void"
						else String.format("Z<Unknown_USER_Type /*%s*/>", s)
					}

					OS_Type.Type.BUILT_IN -> {
						LOG.err("Warning: BUILT_IN TypeName in GenerateC")
						z = "Z" + ty.bType.code // README should not even be here, but look at .name() for other code gen schemes
					}

					OS_Type.Type.UNIT_TYPE -> z = "void"
					else -> throw IllegalStateException("Unexpected value: " + ty.type)
				}
				return z
			}

			@Deprecated("")
			fun forTypeName(typeName: TypeName, errSink: ErrSink): String {
				if (typeName is RegularTypeName) {
					val name = typeName.name // TODO convert to Z-name

					return String.format("Z<%s>/*kklkl*/", name)
					//			return getTypeName(new OS_UserType(typeName));
				}
				errSink.reportError("Type is not fully deduced $typeName")
				return typeName.toString() // TODO type is not fully deduced
			}

			fun forTypeTableEntry(tte: TypeTableEntry): String {
				val res = tte.resolved()
				if (res is EvaContainerNC) {
					val code: Int = res.code
					return "Z$code"
				} else return "Z<-1>"
			}

			fun forVTE(input: VariableTableEntry): String {
				val attached = input.typeTableEntry.attached ?: return Emit.emit("/*390*/") + "Z__Unresolved*"
				// TODO remove this ASAP

				//
				// special case
				//
				if (input.typeTableEntry.genType.node != null) return Emit.emit("/*395*/") + getTypeNameForEvaNode(input.typeTableEntry.genType.node) + "*"
				//
				if (input.status == BaseTableEntry.Status.UNCHECKED) return "Error_UNCHECKED_Type"
				if (attached.type == OS_Type.Type.USER_CLASS) {
					return attached.classOf.name()
				} else if (attached.type == OS_Type.Type.USER) {
					val typeName = attached.typeName
					val name = if (typeName is NormalTypeName) typeName.name
					else typeName.toString()
					return String.format(Emit.emit("/*543*/") + "Z<%s>*", name)
				} else throw NotImplementedException()
			}

			fun getTypeNameForEvaNode(aEvaNode: EvaNode): String {
				val ty = if (aEvaNode is EvaClass) forGenClass(aEvaNode)
				else if (aEvaNode is EvaNamespace) forGenNamespace(aEvaNode)
				else "Error_Unknown_GenClass"
				return ty
			}

			fun forGenClass(aEvaClass: EvaClass): String {
				val z = String.format("Z%d", aEvaClass.code)
				return z
			}

			fun forGenNamespace(aEvaNamespace: EvaNamespace): String {
				val z = String.format("Z%d", aEvaNamespace.code1234567)
				return z
			}
		}
	}

	internal class WlGenerateFunctionC(private val fileGen: GenerateResultEnv, private val gf: BaseEvaFunction, private val generateC: GenerateFiles) : WorkJob {
		val resultSink: GenerateResultSink = fileGen.resultSink()
		private val gr: GenerateResult = fileGen.gr()
		private val wl: WorkList = fileGen.wl()
		private var _isDone = false

		override fun isDone(): Boolean {
			return _isDone
		}

		override fun run(aWorkManager: WorkManager?) {
			if (gf is EvaFunction) {
				generateC.generate_function(gf, gr, wl, resultSink)
			} else {
				generateC.generate_constructor(gf as EvaConstructor, gr, wl, resultSink, aWorkManager, fileGen)
			}
			_isDone = true
		}
	}

	inner class GenerateResultProgressive {
		var _gr: GenerateResult = Old_GenerateResult()

		fun addConstructor(aGf: EvaConstructor, aBuf: Buffer?, aTY: TY?) {
			val lsp = aGf.module().lsp

			_gr.addConstructor(aGf, aBuf, aTY, lsp)
		}

		fun addFunction(aGf: EvaFunction, aBufHdr: Buffer?, aTY: TY?) {
			val lsp = aGf.module().lsp

			_gr.addFunction(aGf, aBufHdr, aTY, lsp)
		}
	}

	/*static*/
	inner class GetAssignmentValue {
		fun ConstTableIA(constTableIA: ConstTableIA, gf: BaseEvaFunction): String {
			val cte = gf.getConstTableEntry(constTableIA.index)
			return when (cte.initialValue.kind) {
				ExpressionKind.NUMERIC -> const_to_string(cte.initialValue)
				ExpressionKind.STRING_LITERAL -> const_to_string(cte.initialValue)
				ExpressionKind.IDENT -> {
					val text = (cte.initialValue as IdentExpression).text
					if (BuiltInTypes.isBooleanText(text)) text
					else throw NotImplementedException()
				}

				else -> throw NotImplementedException()
			}
		}

		fun const_to_string(expression: IExpression): String {
			val cs = GCX_ConstantString(this@GenerateC,
					this@GetAssignmentValue,
					expression)

			return cs.text
		}

		fun FnCallArgs(fca: FnCallArgs, gf: BaseEvaFunction, LOG: ElLog): String {
			val sb = StringBuilder()
			val inst = fca.expression
			//			LOG.err("9000 "+inst.getName());
			val x = inst.getArg(0)
			assert(x is ProcIA)
			val pte = gf.getProcTableEntry(DeduceTypes2.to_int(x!!))
			when (inst.name) {
				InstructionName.CALL -> {
					if (pte.expression_num == null) {
//					assert false; // TODO synthetic methods
						val statement = FnCallArgs_Statement(this@GenerateC, this, pte, inst, gf)

						sb.append(statement.text)
					} else {
						val statement = FnCallArgs_Statement2(this@GenerateC, gf, LOG, inst, pte, this)

						sb.append(statement.text)
					}
					return sb.toString()
				}

				InstructionName.CALLS -> {
					var reference: CReference? = null
					if (pte.expression_num == null) {
						val y = 2
						val ptex = pte.__debug_expression as IdentExpression
						sb.append(Emit.emit("/*684*/"))
						sb.append(ptex.text)

						val s = pte.getDeduceElement3(pte._deduceTypes2(), pte.__gf!!).toString()

						//final DR_Ident id = pte.__gf.getIdent((IdentExpression) pte.expression);
						//id.resolve();
						val yy = 2
					} else {
						// TODO Why not expression_num?
						reference = CReference(_repo, ce)
						val ia2 = pte.expression_num as IdentIA
						reference.getIdentIAPath(ia2, AOG.GET, null)
						val ava = getAssignmentValueArgs(inst, gf, LOG)
						val sll = ava.stringList()
						reference.args(sll)
						val path = reference.build()
						sb.append(Emit.emit("/*807*/") + path)

						val ptex = pte.__debug_expression
						if (ptex is IdentExpression) {
							val z = ReasonedStringListStatement()

							z.append(Emit.emit("/*803*/"), "emit-code")
							z.append(ptex.text, "ptex")

							sb.append(z.text)
						} else if (ptex is ProcedureCallExpression) {
							val z = ReasonedStringListStatement()

							z.append(Emit.emit("/*806*/"), "emit-code")
							z.append(ptex.getLeft().toString(),  /*FIXME 09/07*/"ptex") // TODO Qualident, IdentExpression, DotExpression

							sb.append(z.text)
						}
					}
					if (true /*reference == null*/) {
						val z = ReasonedStringListStatement()

						val ava = getAssignmentValueArgs(inst, gf, LOG)
						val sll = ava.stringList()

						z.append(Emit.emit("/*810*/"), "emit-code")
						z.append("(", "open-brace")
						z.append(ava, "GetAssignmentValueArgsStatement")
						z.append(");", "close-brace")

						sb.append(z.text)
					}
					return sb.toString()
				}

				else -> throw IllegalStateException("Unexpected value: " + inst.name)
			}
		}

		fun getAssignmentValueArgs(inst: Instruction, gf: BaseEvaFunction, LOG: ElLog): GetAssignmentValueArgsStatement {
			val gavas = GetAssignmentValueArgsStatement(inst)

			val args_size = inst.argsSize

			for (i in 1 until args_size) {
				val ia = inst.getArg(i)
				val y = 2

				//LOG.err("7777 " + ia);
				if (ia is ConstTableIA) {
					val constTableEntry = gf.getConstTableEntry(ia.index)
					gavas.add_string(const_to_string(constTableEntry.initialValue))
				} else if (ia is IntegerIA) {
					val variableTableEntry = gf.getVarTableEntry(ia.index)
					gavas.add_string(Emit.emit("/*853*/") + _zone[variableTableEntry, gf]!!.realTargetName)
				} else if (ia is IdentIA) {
					val path = gf.getIdentIAPathNormal(ia) // return x.y.z
					val ite: IdentTableEntry = ia.entry

					if (ite.status == BaseTableEntry.Status.UNKNOWN) {
						gavas.add_string(String.format("%s is UNKNOWN", path))
					} else {
						val reference = CReference(_repo, ce)
						reference.getIdentIAPath(ia, AOG.GET, null)
						val path2 = reference.build() // return ZP105get_z(vvx.vmy)

						if (path == path2) {
							// should always fail
							//throw new AssertionError();
							LOG.err(String.format("864 should always fail but didn't %s %s", path, path2))
						}

						//assert ident != null;
						//IdentTableEntry ite = gf.getIdentTableEntry(((IdentIA) ia).getIndex());
						//sll.add(Emit.emit("/*748*/")+""+ite.getIdent().getText());
						gavas.add_string(Emit.emit("/*748*/") + path2)
						LOG.info("743 $path2 $path")
					}
				} else if (ia is ProcIA) {
					LOG.err("863 ProcIA")
					throw NotImplementedException()
				} else {
					throw IllegalStateException("Cant be here: Invalid InstructionArgument")
				}
			}

			return gavas
		}

		// TODO look at me
		fun forClassInvocation(aInstruction: Instruction, aClsinv: ClassInvocation, gf: BaseEvaFunction, LOG: ElLog): String {
			val _arg0 = aInstruction.getArg(0)
			val pte = gf.getProcTableEntry((_arg0 as ProcIA).index())
			val reference = CtorReference()
			reference.getConstructorPath(pte.expression_num!!, gf)
			val ava = getAssignmentValueArgs(aInstruction, gf, LOG)
			val x = ava.stringList()
			reference.args(x)
			return reference.build(aClsinv)
		}

		fun IdentIA(identIA: IdentIA, gf: BaseEvaFunction): String {
			assert(gf === identIA.gf // yup
			)
			val reference = CReference(_repo, ce)
			reference.getIdentIAPath(identIA, AOG.GET, null)
			return reference.build()
		}

		fun IntegerIA(integerIA: IntegerIA, gf: BaseEvaFunction): String {
			val vte = gf.getVarTableEntry(integerIA.index)
			val x = getRealTargetName(gf, vte)
			return x
		}
	}

	private fun postGenerateCodeForFunction(aEvaFunction: EvaFunction, wl: WorkList, fileGen: GenerateResultEnv) {
		for (identTableEntry in aEvaFunction.idte_list) {
			if (identTableEntry.isResolved) {
				val x = identTableEntry.resolvedType()

				if (x is EvaClass) {
					generate_class(fileGen, x)
				} else if (x is EvaFunction) {
					wl.addJob(WlGenerateFunctionC(fileGen, x, this))
				} else {
					LOG.err(x.toString())
					throw NotImplementedException()
				}
			}
		}
		for (pte in aEvaFunction.prte_list) {
			val fi = pte.functionInvocation
			if (fi == null) {
				// TODO constructor
			} else {
				fi.generateDeferred().then { evaFunction: BaseEvaFunction -> wl.addJob(WlGenerateFunctionC(fileGen, evaFunction, this)) }
				//BaseEvaFunction gf = fi.getEva();
				//if (gf != null) {
				//	wl.addJob(new WlGenerateFunctionC(fileGen, gf, this));
				//}
			}
		}
	}


	//@Override
	//public void finishUp(final GenerateResult aGenerateResult, final WorkManager wm, final WorkList aWorkList) {
	//	assert _fileGen != null;
	//
	//	for (WhyNotGarish_Item value : new ArrayList<>(a_directory.values())) {
	//		if (!value.hasFileGen())
	//			value.provideFileGen(_fileGen);
	//	}
	//}
	private fun postGenerateCodeForConstructor(aEvaConstructor: EvaConstructor, wl: WorkList, aFileGen: GenerateResultEnv) {
		for (identTableEntry in aEvaConstructor.idte_list) {
			identTableEntry.reactive().addResolveListener { x: IdentTableEntry ->
				generateIdent(x)
			}

			if (identTableEntry.isResolved) {
				generateIdent(identTableEntry)
			}
		}
		for (pte in aEvaConstructor.prte_list) {
//			ClassInvocation ci = pte.getClassInvocation();
			val fi = pte.functionInvocation
			if (fi == null) {
				// TODO constructor
				val y = 2
			} else {
				fi.generateDeferred().then { evaFunction: BaseEvaFunction -> wl.addJob(WlGenerateFunctionC(aFileGen, evaFunction, this)) }

				//BaseEvaFunction gf = fi.getEva();
				//if (gf != null) {
				//	wl.addJob(new WlGenerateFunctionC(aFileGen, gf, this));
				//}
			}
		}
	}

	private fun generateCodeForConstructor(
			aGenerateResultEnv: GenerateResultEnv,
			aEvaConstructor: EvaConstructor,
	) {
		val yf = a_lookup(aEvaConstructor)

		if (true) {
			if (aEvaConstructor.fd == null) return

			//var inj = _inj // TODO this virus hasn't spread this far?
			val gcfm = Generate_Code_For_Method(this, LOG)
			val dgf = yf!!.deduced()

			gcfm.generateCodeForConstructor(dgf, aGenerateResultEnv)
		} else {
			yf!!.resolveFileGenPromise(aGenerateResultEnv)
		}
	}

	fun generateCodeForConstructor_1(aEvaConstructor: EvaConstructor, aGenerateResultEnv: GenerateResultEnv) {
		val yf = a_lookup(aEvaConstructor)
		yf!!.resolveFileGenPromise(aGenerateResultEnv)
	}

	fun generateCodeForMethod(aGenerateResultEnv: GenerateResultEnv, aEvaFunction: BaseEvaFunction) {
		val yf = this.a_lookup(aEvaFunction)
		yf!!.resolveFileGenPromise(aGenerateResultEnv)
	}

	override fun resultsFromNodes(
			aNodes: List<EvaNode>,
			wm: WorkManager,
			grs: GenerateResultSink,
			fg: GenerateResultEnv,
	): GenerateResult {
		val gr2 = fg.gr()

		if (fg.resultSink() !== grs) {
			// LOOK 09/28 dsfjklafhdjsklfhjdlfdhjlf
			_ce().logProgress(CompProgress.GenerateC, Pair.of(9997, "fg.resultSink() != grs"))
		}

		for (generatedNode in aNodes) {
			if (generatedNode is EvaContainerNC) {
				generatedNode.generateCode(fileGen__, this)
				val gn1: Collection<EvaNode> = generatedNode.functionMap.values.stream().map { x: EvaFunction -> x }.collect(Collectors.toList())
				val gr3 = this.generateCode(gn1, fg)
				grs.additional(gr3)
				val gn2: Collection<EvaNode> = generatedNode.classMap.values.stream().map { x: EvaClass -> x }.collect(Collectors.toList())
				val gr4 = this.generateCode(gn2, fg)
				grs.additional(gr4)
			} else {
				LOG.info("2009 " + generatedNode.javaClass.name)
			}
		}

		return gr2
	}

	fun _zone_get(ident_ia: IdentIA): ZoneITE {
		return _zone.get(ident_ia)!!
	}
}

	val PHASE = "GenerateC"
	fun isValue(gf: BaseEvaFunction, name: String): Boolean {
		return __Tests_BaseEvaFunction.testIsValue(gf, name)
	}

	fun isValue(yf: WhyNotGarish_Function, name: String): Boolean {
		return __Tests_BaseEvaFunction.testIsValue(yf, name)
	}
