package tripleo.elijah.world.impl

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.Compilation0
import tripleo.elijah.compiler_model.CM_Filename
import tripleo.elijah.entrypoints.MainClassEntryPoint
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.BaseFunctionDef
import tripleo.elijah.lang.impl.OS_PackageImpl
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.util.CompletableProcess
import tripleo.elijah.util.ObservableCompletableProcess
import tripleo.elijah.world.i.*
import tripleo.elijah.world.i.LivingRepo.Add
import java.util.*
import java.util.function.Consumer
import java.util.stream.Collectors

class DefaultLivingRepo : LivingRepo {
    private val wmo = ObservableCompletableProcess<WorldModule>()

    private val _packages: MutableMap<String, OS_Package> = HashMap()
    private val _modules: MutableSet<WorldModule> = HashSet()
    private val repo: MutableList<LivingNode> = ArrayList()
    private val functionMap: Multimap<BaseEvaFunction, DefaultLivingFunction> = ArrayListMultimap.create()

    private var _classCode = 101
    private var _functionCode = 1001
    private var _packageCode = 1

    override fun getMods__(): Collection<WorldModule> {
        return _modules
    }

    override fun _completeModules() {
        wmo.onComplete()
    }

    override fun addClass(cs: ClassStatement): LivingClass? {
        return null
    }

    override fun addClass(aClass: EvaClass, addFlag: Add): DefaultLivingClass {
        var living: DefaultLivingClass? = null
        var set = false

        when (addFlag) {
            Add.NONE -> {
                var livingClass = aClass.living

                if (livingClass == null) {
                    livingClass = DefaultLivingClass(aClass)
                    aClass.living = livingClass
                    set = true

                    living = livingClass
                }

                if (livingClass.code == 0) {
                    livingClass.code = nextClassCode()
                } else {
                    if (2 == 3) {
                        assert(true)
                    }
                }
            }

            Add.MAIN_FUNCTION -> {
                throw IllegalArgumentException("not a function")
            }

            Add.MAIN_CLASS -> {
                val isMainClass = MainClassEntryPoint.isMainClass(aClass.klass)
                require(isMainClass) { "not a main class" }
                aClass.code = 100
            }
        }
        if (!set) {
            living = DefaultLivingClass(aClass)
            aClass.living = living
        }

        repo.add(living!!)

        return living
    }

    override fun addFunction(aFunction: BaseEvaFunction,
                             addFlag: Add): DefaultLivingFunction {
        when (addFlag) {
            Add.NONE -> {
                aFunction.code = nextFunctionCode()
            }

            Add.MAIN_FUNCTION -> {
                if (aFunction.fd is FunctionDef
                        && MainClassEntryPoint.is_main_function_with_no_args(aFunction.fd)) {
                    aFunction.code = 1000
                    // compilation.notifyFunction(code, aFunction);
                } else {
                    throw IllegalArgumentException("not a main function")
                }
            }

            Add.MAIN_CLASS -> {
                throw IllegalArgumentException("not a class")
            }
        }
        val living = DefaultLivingFunction(aFunction)
        aFunction._living = living

        functionMap.put(aFunction, living)

        return living
    }

    override fun addFunction(fd: BaseFunctionDef): LivingFunction? {
        return null
    }

    override fun addModule(mod: OS_Module, aFilename: String, aC: Compilation0) {
        addModule(mod, aFilename, aC as Compilation)
    }

    //@Override
    fun addModule(mod: OS_Module,
                  aFilename: String,
                  aC: Compilation) {
//		tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4("LivingRepo::addModule >> " + aFilename);

//		var t = aC.getCompilerInputListener();
//		t.change(new EIT_ModuleInput(mod, aC), CompilerInput.CompilerInputField.DIRECTORY_RESULTS);

//		aC._cis().onNext();
    }

    override fun addModule2(aWorldModule: WorldModule) {
        _modules.add(aWorldModule)

        wmo.onNext(aWorldModule)
    }

    override fun addModuleProcess(wmcp: CompletableProcess<WorldModule>) {
        wmo.subscribe(wmcp)
    }

    override fun addNamespace(aNamespace: EvaNamespace,
                              addFlag: Add): DefaultLivingNamespace {
        when (addFlag) {
            Add.NONE -> {
                aNamespace.code1234567 = nextClassCode()
            }

            Add.MAIN_FUNCTION -> {
                throw IllegalArgumentException("not a function")
            }

            Add.MAIN_CLASS -> {
                throw IllegalArgumentException("not a main class")
            }
        }
        val living = DefaultLivingNamespace(aNamespace)
        aNamespace.living = living

        repo.add(living)

        return living
    }

    override fun addPackage(pk: OS_Package): LivingPackage? {
        return null
    }

    override fun eachModule(`object`: Consumer<WorldModule>) {
        _modules.forEach(`object`)
    }

    override fun findClass(aClassName: String): List<ClassStatement> {
        val l: MutableList<ClassStatement> = ArrayList()
        val modules1 = modules().stream()
                .map { obj: WorldModule -> obj.module() }
                .collect(Collectors.toList())

        // TODO idk why I can never figure this out
//		var ll = modules1.stream()
//				.filter(m -> m.hasClass(aClassName))
//				.map(m -> m.findClassesNamed(aClassName))
//				.collect(Collectors.toList());
        for (module in modules1) {
            if (module.hasClass(aClassName)) {
                l.addAll(module.findClassesNamed(aClassName))
            }
        }

        //		assert Objects.equals(l,ll);
        return l
    }

    override fun findModule(mod: OS_Module): WorldModule? {
        // noinspection UnnecessaryLocalVariable
        val result = _modules.stream()
                .filter { wm: WorldModule -> wm.module() === mod }
                .findFirst()
                .orElse(null)

        return result
    }

    override fun getClass(aEvaClass: EvaClass): LivingClass {
        for (livingNode in repo) {
            if (livingNode is LivingClass) {
                if (livingNode.evaNode() == aEvaClass) return livingNode
            }
        }

        val living = DefaultLivingClass(aEvaClass)

        // klass._living = living;
        repo.add(living)

        return living
    }

    override fun getClassesForClassNamed(className: String): List<LivingClass> {
        val lcs: MutableList<LivingClass> = LinkedList()

        for (livingNode in repo) {
            if (livingNode is LivingClass) {
                if (livingNode.element.name() == className) lcs.add(livingNode)
            }
        }

        return lcs
    }

    override fun getClassesForClassStatement(cls: ClassStatement): List<LivingClass> {
        val lcs: MutableList<LivingClass> = LinkedList()

        for (livingNode in repo) {
            if (livingNode is LivingClass) {
                if (livingNode.element == cls) lcs.add(livingNode)
            }
        }

        return lcs
    }

    override fun getFunction(aBaseEvaFunction: BaseEvaFunction): LivingFunction? {
        val c = functionMap[aBaseEvaFunction]

        if (!c.isEmpty()) return c.iterator().next()

        return null
    }

    override fun getNamespace(aEvaNamespace: EvaNamespace): LivingNamespace {
        for (livingNode in repo) {
            if (livingNode is LivingNamespace) {
                if (livingNode.evaNode() == aEvaNamespace) return livingNode
            }
        }

        val living = DefaultLivingNamespace(aEvaNamespace)

        // klass._living = living;
        repo.add(living)

        return living
    }

    override fun getPackage(aPackageName: String): OS_Package {
        return _packages[aPackageName]!!
    }

    override fun hasPackage(aPackageName: String): Boolean {
        if (aPackageName == "C") {
            val y = 2
        }
        return _packages.containsKey(aPackageName)
    }

    override fun isPackage(pkg: String): Boolean {
        return _packages.containsKey(pkg)
    }

    override fun addModule(aMod: OS_Module, aFn: CM_Filename, aCompilation: Compilation) {
        addModule(aMod, aFn.string, aCompilation)
    }

    override fun makePackage(pkg_name: Qualident): OS_Package {
        val pkg_name_s = pkg_name.toString()
        if (isPackage(pkg_name_s)) {
            return _packages[pkg_name_s]!!
        }

        val newPackage: OS_Package = OS_PackageImpl(pkg_name, nextPackageCode())
        _packages[pkg_name_s] = newPackage
        return newPackage
    }

    override fun modules(): Collection<WorldModule> {
        return _modules
    }

    fun nextClassCode(): Int {
        val i = _classCode
        _classCode++
        return i
    }

    fun nextFunctionCode(): Int {
        val i = _functionCode
        _functionCode++
        return i
    }

    @Contract(mutates = "this")
    private fun nextPackageCode(): Int {
        val i = _packageCode
        _packageCode++
        return i
    }
}
