package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.ProcTableEntry
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.util.Helpers

class GCX_FunctionCall_Special(private val pte: ProcTableEntry, aGf: WhyNotGarish_BaseFunction,
                               private val gc: GenerateC, private val instruction: Instruction) {
    private val gf: BaseEvaFunction? = aGf.cheat()

    val text: String
        get() {
            val sb = StringBuilder()

            val e = gf!!.elements()

            var reference: CReference? = null
            if (pte.expression_num == null) {
                val y = 2
                val ptex = pte.__debug_expression as IdentExpression
                val text = ptex.text
                val xx = gf.vte_lookup(text)
                val xxx: String?
                if (xx != null) {
                    xxx = gc.getRealTargetName(gf, xx as IntegerIA, AOG.GET)
                } else {
                    xxx = text
                    gc.LOG.err("xxx is null $text")
                }
                sb.append(Emit.emit("/*460*/") + xxx)
            } else {
                val ia2 = pte.expression_num as IdentIA
                reference = CReference(gc.repo(), gc.ce)
                reference.getIdentIAPath(ia2, AOG.GET, null)
                val sl3 = gc.getArgumentStrings(gf, instruction)
                reference.args(sl3)
                val path = reference.build()
                sb.append(Emit.emit("/*463*/") + path)
            }
            if (reference == null) {
                sb.append('(')
                val sl3 = gc.getArgumentStrings(gf, instruction)
                sb.append(Helpers.String_join(", ", sl3))
                sb.append(");")
            }

            return sb.toString()
        }
}
