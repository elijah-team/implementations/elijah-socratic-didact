package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.elijah.nextgen.output.NG_OutputItem
import tripleo.elijah.nextgen.output.NG_OutputStatement
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import java.util.*

// TODO 09/04 Duplication madness
class NG_OutputRequest(private val fileName: EOT_FileNameProvider,
                       private val statement: EG_Statement,
                       private val outputStatement: NG_OutputStatement,
                       private val outputItem: NG_OutputItem
) {
    fun fileName(): EOT_FileNameProvider {
        return fileName
    }

    fun statement(): EG_Statement {
        return statement
    }

    fun outputStatement(): NG_OutputStatement {
        return outputStatement
    }

    fun outputItem(): NG_OutputItem {
        return outputItem
    }

    override fun hashCode(): Int {
        return Objects.hash(fileName, statement, outputStatement, outputItem)
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as NG_OutputRequest
        return this.fileName == that.fileName && (this.statement == that.statement) && (this.outputStatement == that.outputStatement) && (this.outputItem == that.outputItem)
    }

    override fun toString(): String {
        return "NG_OutputRequest[" +
                "fileName=" + fileName + ", " +
                "statement=" + statement + ", " +
                "outputStatement=" + outputStatement + ", " +
                "outputItem=" + outputItem + ']'
    }
}
