/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.stages.deduce.nextgen.DR_Item
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.stages.instructions.VariableTableType

/**
 * Created 6/27/21 9:40 AM
 */
class EvaFunction(private val fd: FunctionDef?) : BaseEvaFunction(), GNCoded { //
	@JvmField
	var _idents: MutableSet<DR_Item> = HashSet()

	override fun getFD(): FunctionDef {
		checkNotNull(fd) { "No function" }
		return fd
	}

	override fun getRole(): GNCoded.Role = GNCoded.Role.FUNCTION

	override fun getSelf(): VariableTableEntry? {
		return if (getFD().parent is ClassStatement) {
			getVarTableEntry(0)
		} else {
			null
		}
	}

	override fun identityString(): String = fd.toString()

	override fun module(): OS_Module = getFD().context.module()

	fun name(): String = getFD().name()

	override fun register(aCr: ICodeRegistrar) = aCr.registerFunction1(this)

	override fun addVariableTableEntry(name: String?, vtt: VariableTableType?, type: TypeTableEntry?, el: OS_Element?): Int {
		TODO("Not yet implemented")
	}

	override fun getCode(): Int = code

	override fun toString(): String {
		if (fd == null) {
			return "<EvaFunction {{unattached}}>"
		} else {
			val args = fd.args
			val parent = fd.parent
			val name = fd.name()

			val pparent = parent?.toString() ?: "{{null parent}}"

			val pte_string = args.toString() // TODO wanted PTE.getLoggingString

			return String.format("<EvaFunction %s %s %s>", pparent, name, pte_string)
		}
	}
}
