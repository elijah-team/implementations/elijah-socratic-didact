package tripleo.elijah.nextgen.inputtree

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.EvaPipeline
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CompilationImpl.CompilationAlways.Companion.defaultPrelude
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.nextgen.inputtree.EIT_ModuleInput
import tripleo.elijah.comp.notation.GM_GenerateModule
import tripleo.elijah.comp.notation.GM_GenerateModuleRequest
import tripleo.elijah.comp.notation.GN_GenerateNodesIntoSink
import tripleo.elijah.comp.notation.GN_GenerateNodesIntoSinkEnv
import tripleo.elijah.g.GModuleGenerationRequest
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.model.SM_Module
import tripleo.elijah.nextgen.model.SM_ModuleItem
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_generic.*
import tripleo.elijah.stages.gen_generic.pipeline_impl.DefaultGenerateResultSink
import tripleo.elijah.stages.gen_generic.pipeline_impl.ProcessedNode
import tripleo.elijah.work.WorkList__
import tripleo.elijah.work.WorkManager
import java.util.function.Consumer

class EIT_ModuleInputImpl

@Contract(pure = true)
constructor(
    private val module: OS_Module?,
    private val c: Compilation,
    override val type: EIT_InputType = EIT_InputType.ELIJAH_SOURCE,
) : EIT_ModuleInput {
    override fun computeSourceModel(): SM_Module {
        val sm = SM_Module {
            val items: MutableList<SM_ModuleItem> = ArrayList()
            for (item in module!!.items) {
                items.add { item }
            }
            items
        }
        return sm
    }

    override fun doGenerate(r0: GModuleGenerationRequest) {
        val r = r0 as ModuleGenerationRequest
        doGenerate(r.evaNodeList, r.work(), r.generateResultConsumer!!, r.compilationEnclosure!!)
    }

    private fun doGenerate(nodes: List<EvaNode?>?,
                           wm: WorkManager?,
                           resultConsumer: Consumer<GenerateResult?>,
                           ce: CompilationEnclosure) {
        // 0. get lang
        val lang = langOfModule()

        // 1. find Generator (GenerateFiles) eg. GenerateC final
        val mod = ce.compilation.world().findModule(module!!)
        val p = OutputFileFactoryParams(mod!!, ce)

        ce.pipelineAccessPromise.then {
            val resultSink = DefaultGenerateResultSink(it!!)
            val gr = Old_GenerateResult()
            val wl = WorkList__()
            val nodes1: List<ProcessedNode> = EvaPipeline.processLgc(nodes!!.stream().map { it!! }?.toList()!!)
            val gnis_env = GN_GenerateNodesIntoSinkEnv(nodes1,
                resultSink,
                ce.pipelineLogic.verbosity,
                gr,
                ce)
            val gnis = GN_GenerateNodesIntoSink(gnis_env)
            val gmr = GM_GenerateModuleRequest(gnis, mod, gnis_env)
            val gmgm = GM_GenerateModule(gmr)
            val env = GenerateResultEnv(resultSink, gr, wm!!, wl, gmgm)

            val generateFiles = OutputFileFactory.create(lang, p, env)

            // 2. query results
            val gr2 = generateFiles.resultsFromNodes(nodes, wm, (generateFiles as GenerateC).resultSink, env)

            // 3. #drain workManager -> README part of workflow. may change later as appropriate
            wm.drain()

            // 4. tail process results
            resultConsumer.accept(gr2)
        }
    }

    private fun langOfModule(): String {
        val lsp = module!!.lsp
        val ci = lsp.instructions
        val lang = if (ci.genLang() == null) defaultPrelude() else ci.genLang()
        // DEFAULT(compiler-default), SPECIFIED(gen-clause: codePoint), INHERITED(cp) // CodePoint??
        return lang
    }
}
