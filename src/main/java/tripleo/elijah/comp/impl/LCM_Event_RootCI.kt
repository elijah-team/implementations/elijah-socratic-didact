package tripleo.elijah.comp.impl

import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.i.LCM_Event
import tripleo.elijah.comp.i.LCM_HandleEvent
import tripleo.elijah.comp.i.extra.IPipelineAccess

class LCM_Event_RootCI private constructor() : LCM_Event {
    override fun handle(aHandleEvent: LCM_HandleEvent) {
        val c = aHandleEvent.compilation
        val rootCI = aHandleEvent.obj as CompilerInstructions

        try {
//			c.c().setRootCI(rootCI);
            val compilationEnclosure = aHandleEvent.compilation.c().compilationEnclosure

            compilationEnclosure.pipelineAccessPromise.then { apa: IPipelineAccess? -> c.cr().start(rootCI, apa!!) }
        } catch (aE: Exception) {
            aHandleEvent.lcm.exception(aHandleEvent, aE)
        }
    }

    companion object {
        @JvmField
        val INSTANCE: LCM_Event_RootCI = LCM_Event_RootCI()

        fun instance(): LCM_Event_RootCI {
            return INSTANCE
        }
    }
}
