package tripleo.elijah.nextgen.model

import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

enum class SM_Module__babyPrint {
    ;

    companion object {
        fun babyPrint(sm: SM_Module) {
            for (item in sm.items()) {
                SimplePrintLoggerToRemoveSoon.println_out_4(item.toString())
            }
        }
    }
}
