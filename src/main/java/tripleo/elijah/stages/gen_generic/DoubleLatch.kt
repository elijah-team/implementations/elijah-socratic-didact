package tripleo.elijah.stages.gen_generic

import org.jetbrains.annotations.Contract
import java.util.function.Consumer

class DoubleLatch<T> // private IincInsnNode action;
@Contract(pure = true) constructor(private val action: Consumer<T>) {
    private var simple = false
    private var tt: T? = null

    fun notifyData(att: T) {
        tt = att
        if (simple && tt != null) {
            action.accept(tt!!)
        }
    }

    fun notifyLatch(ass: Boolean) {
        simple = ass
        if (simple && tt != null) {
            action.accept(tt!!)
        }
    }
}
