package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.NULL_DeduceTypes2
import tripleo.elijah.stages.gen_fn.*

// DeduceGrand
class DG_ClassStatement(private val classStatement: ClassStatement) : DG_Item {
    var genericElementHolder: GenericElementHolder? = null
    private var _evaNode: EvaClass? = null
    private var classInvocation: ClassInvocation? = null
    private var fi: FunctionInvocation? = null
    private var pte: ProcTableEntry? = null

    fun attach(aFi: FunctionInvocation?, aPte: ProcTableEntry?) {
        fi = aFi
        pte = aPte
    }

    fun attachClass(aResult: EvaClass?) {
        _evaNode = aResult
    }

    fun classInvocation(): ClassInvocation {
        if (classInvocation == null) {
            classInvocation = ClassInvocation((classStatement), null, NULL_DeduceTypes2()) // 08/28 !!
            // classInvocation = _inj().new_ClassInvocation((classStatement), null);
        }
        return classInvocation!!
    }

    fun ConstructableElementHolder(aE: OS_Element?, aVte: VariableTableEntry?): IElementHolder {
        // return _inj().new_ConstructableElementHolder(classStatement, aVte);
        return ConstructableElementHolder(classStatement, aVte)
    }

    fun evaClass(): EvaClass? {
        return _evaNode
    }

    fun functionInvocation(): FunctionInvocation? {
        return fi
    }

    fun GenericElementHolder(): GenericElementHolder {
        if (genericElementHolder == null) {
            // genericElementHolder = _inj().new_GenericElementHolder(classStatement);
            genericElementHolder = GenericElementHolder(classStatement)
        }
        return genericElementHolder!!
    }
}
