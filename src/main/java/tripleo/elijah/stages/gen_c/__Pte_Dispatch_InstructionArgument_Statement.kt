package tripleo.elijah.stages.gen_c

import tripleo.elijah.nextgen.outputstatement.ReasonedStringListStatement
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.InstructionFixedList

internal class __Pte_Dispatch_InstructionArgument_Statement(private val expression_num: InstructionArgument,
                                                            private val instruction: Instruction,
                                                            private val gf: BaseEvaFunction,
                                                            private val gc: GenerateC) : ReasonedStringListStatement() {
    private val xx: Array<SpecialText?>

    init {
        val z = this
        xx = arrayOfNulls(1)

        z.append(Emit.emit("/*427-1*/"), "emit-code")
        z.append({
            val identIA = expression_num as IdentIA
            val reference = CReference(gc.repo(), gc.ce)
            xx[0] = reference.getIdentIAPath(identIA, AOG.GET, null)
            val sl3 = gc.getArgumentStrings { InstructionFixedList(instruction) }
            reference.args(sl3)
            val path = reference.build()
            path
        }, "path")
        z.append(";", "close-semi")
    }
}
