package tripleo.elijah.util2;

/**
 * Used to mark public access that should not be called from outside
 */
public @interface ElijahInternal {
}
