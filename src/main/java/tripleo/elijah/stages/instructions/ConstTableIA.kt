/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.instructions

import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.ConstantTableEntry

/**
 * Created 9/10/20 3:35 PM
 */
class ConstTableIA(@JvmField val index: Int, private val gf: BaseEvaFunction) : InstructionArgument {
    val entry: ConstantTableEntry
        get() = gf.getConstTableEntry(index)

    override fun toString(): String {
        val constantTableEntry = gf.cte_list[index]
        val name = constantTableEntry.name
        return if (name != null) String.format("(ct %d) [%s=%s]", index, name, constantTableEntry.initialValue)
        else String.format("(ct %d) [%s]", index, constantTableEntry.initialValue)
    }
} //
//
//

