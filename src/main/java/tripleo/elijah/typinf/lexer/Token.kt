/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.typinf.lexer

/**
 * A simple Token structure.
 * Contains the token type, value and position.
 */
class Token(@JvmField val type: String?, @JvmField val `val`: String?, @JvmField val pos: Int) {
    override fun toString(): String {
        return String.format("%s(%s) at %s", this.type, this.`val`, this.pos)
    }
} //
//
//

