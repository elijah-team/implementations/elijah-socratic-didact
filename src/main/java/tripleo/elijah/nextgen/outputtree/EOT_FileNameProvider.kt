@file:Suppress("ClassName")

package tripleo.elijah.nextgen.outputtree

interface EOT_FileNameProvider {
    // abstract??
//    val filename : String
//        get() = getFilename()

    fun getFilename(): String
}
