package tripleo.elijah.nextgen.model

fun interface SM_Module : SM_Node {
    fun items(): List<SM_ModuleItem>
}
