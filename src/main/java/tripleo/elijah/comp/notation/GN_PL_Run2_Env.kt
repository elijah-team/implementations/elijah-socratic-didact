package tripleo.elijah.comp.notation

import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.world.i.WorldModule
import java.util.function.Consumer

class GN_PL_Run2_Env(val pipelineLogic: PipelineLogic,
                     val mod: WorldModule,
                     val ce: CompilationEnclosure,
                     val worldConsumer: Consumer<WorldModule>) : GN_Env {
//    fun pipelineLogic(): PipelineLogic = pipelineLogic

//    fun mod(): WorldModule = mod

//    fun ce(): CompilationEnclosure = ce

//    fun worldConsumer(): Consumer<WorldModule> = worldConsumer
}
