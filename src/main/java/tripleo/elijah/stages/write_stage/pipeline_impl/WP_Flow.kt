package tripleo.elijah.stages.write_stage.pipeline_impl

import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.comp.WritePipeline
import tripleo.elijah.comp.graph.i.CK_AbstractStepsContext
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation

class WP_Flow(private val writePipeline: WritePipeline, aPa: IPipelineAccess, s: Collection<WP_Individual_Step>) : CK_AbstractStepsContext() {
    private val steps: MutableList<WP_Individual_Step> = ArrayList()
    private val myOps: OPS

    fun act(): OPS {
        sc = WP_State_Control_1()

        myOps.init()

        for (step in steps) {
            sc!!.cur(step, writePipeline.st, ops())
            if (sc!!.hasException()) {
                break
            }
        }

        return myOps
    }

    private fun ops(): OPS {
        return myOps
    }

    fun context_st(): WritePipelineSharedState {
        return writePipeline.st
    }

    var sc: WP_State_Control_1? = null

    init {
        steps.addAll(s)
        myOps = OPS(writePipeline, this)
    }

    fun context_sc(): WP_State_Control? {
        return sc
    }

    fun context_ops(): OPS {
        return this.myOps
    }

    enum class FlowStatus {
        FAILED, NOT_TRIED, TRIED
    }

    inner class OPS //_writePipeline.st.pa
    (private val _writePipeline: WritePipeline, private val flow: WP_Flow) {
        private val ops = HashMap<WP_Individual_Step, Pair<FlowStatus, Operation<Ok>?>>()

        //public void put(final WP_Indiviual_Step aStep, final Pair<FlowStatus, Operation<Ok>> aOf) {
        //	ops.put(aStep, aOf);
        //}
        fun put(aStep: WP_Individual_Step, fs: FlowStatus, ob: Operation<Ok>?) {
            ops[aStep] = Pair.of(fs, ob) // TODO 10/18 time (maybe somewhere else) chain
        }

        fun init() {
            for (step in steps) {
                ops[step] = Pair.of(FlowStatus.NOT_TRIED, null) // TODO 10/18 initailize (my) chain
            }
        }
    }

    override fun begin() {
        //throw new UnintendedUseException();
    }
}
