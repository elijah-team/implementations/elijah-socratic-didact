package tripleo.elijah.stages.gen_c

import org.apache.commons.lang3.tuple.Pair
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.nextgen.reactive.Reactivable
import tripleo.elijah.nextgen.reactive.ReactiveDimension
import tripleo.elijah.stages.garish.GarishClass
import tripleo.elijah.stages.garish.GarishClass__addClass_1
import tripleo.elijah.stages.gen_c.GenerateC.GetTypeName
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.util.*
import tripleo.elijah.util2.DebugFlags
import tripleo.elijah.world.i.LivingClass

class WhyNotGarish_Class(private val gc: EvaClass, private val generateC: GenerateC) : WhyNotGarish_Item {
    private val fileGenPromise = DeferredObject<GenerateResultEnv, Void, Void>()
    private val gcfc: GCFC = GCFC()

    init {
        gc.reactive().add(gcfc)

        fileGenPromise.then { aFileGen: GenerateResultEnv -> this.onFileGen(aFileGen) }
    }

    private fun onFileGen(aFileGen: GenerateResultEnv) {
        //assert false;
        NotImplementedException.raise()

        if (__Oct_13) {
            //final Compilation compilation = aFileGen.gmgm().gmr().mod().module().getContext().compilation();
            val compilation = generateC._ce().compilation

            // FIXME 10/13 which result sink?
            val resultSink2 = generateC.resultSink
            val resultSink = aFileGen.resultSink()
            val generateResult = aFileGen.gr()
            val livingClass = compilation.world().getClass(gc)
            val garishClass = livingClass.garish as GarishClass

            assert(resultSink === resultSink2)
            gc.generator().provide(resultSink, garishClass, generateResult, generateC)
        } else {
            if (!DebugFlags.MANUAL_DISABLED) {
                gcfc.respondTo(this.generateC)
            }

            val generateResult = aFileGen.gr()

            val compilation = generateC._ce().compilation
            val world = compilation.world()
            val garishClass = world.getClass(gc).garish as GarishClass

            val sink = aFileGen.resultSink()
            if (sink == null) {
                logProgress(9991, "sink failed")
                return
            }

            sink.add(GarishClass__addClass_1(garishClass, generateResult, generateC))
        }
    }

    private fun logProgress(code: Int, message: String) {
        generateC._ce().logProgress(CompProgress.GenerateC, Pair.of(code, message))
    }

    val typeNameString: String
        get() = GetTypeName.Companion.forGenClass(gc)

    override fun hasFileGen(): Boolean {
        return fileGenPromise.isResolved
    }

    override fun provideFileGen(fg: GenerateResultEnv) {
        fileGenPromise.resolve(fg)
    }

    inner class GCFC : Reactivable {
        override fun respondTo(aDimension: ReactiveDimension?) {
            if (aDimension is GenerateC) {
                fileGenPromise.then { fileGen: GenerateResultEnv ->
                    val livingClass: LivingClass = aDimension._ce().compilation.world().getClass(gc)
                    livingClass.generateWith(fileGen.resultSink(), livingClass.garish, fileGen.gr(), aDimension)
                }
            }
        }
    }

    companion object {
        private const val __Oct_13 = false
    }
}
