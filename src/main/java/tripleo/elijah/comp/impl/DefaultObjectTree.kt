package tripleo.elijah.comp.impl

import org.apache.commons.lang3.tuple.Pair
import org.apache.commons.lang3.tuple.Triple
import tripleo.elijah.comp.graph.i.Asseverate
import tripleo.elijah.comp.graph.i.CK_ObjectTree
import tripleo.elijah.comp.graph.i.CK_SourceFile
import tripleo.elijah.comp.graph.i.CM_Module
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.comp.internal.CompilationImpl
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.nextgen.i.Asseveration
import tripleo.elijah.comp.specs.EzSpec
import tripleo.elijah.nextgen.inputtree.EIT_InputTree
import tripleo.elijah.nextgen.inputtree.EIT_ModuleList
import tripleo.elijah.nextgen.inputtree.EIT_ModuleList_
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.Operation
import tripleo.elijah.util2.DebugFlags

class DefaultObjectTree(private val compilation: CompilationImpl) : CK_ObjectTree {
    private val moduleList: EIT_ModuleList = EIT_ModuleList_()

    override fun asseverate(o: Any?, asseveration: Asseverate) {
        when (asseveration) {
            Asseverate.CI_PARSED -> {
                val y = 2
                //			throw new UnintendedUseException();
            }

            Asseverate.ELIJAH_PARSED -> {
                val x = o as CM_Module

                if (DebugFlags.MakeSense) {
                    x.adviseCreator(compilation)
                    x.adviseWorld(compilation.world())
                }
            }

            Asseverate.CI_HASHED -> {
                val t = o as Triple<EzSpec, CK_SourceFile<*>, Operation<String>>

                val spec = t.left
                val hash = t.right
                val p = t.middle

                compilation.compilationEnclosure.logProgress(CompProgress.Ez__HasHash, Pair.of(spec, hash.success()))

                if (p.compilerInput() != null) {
                    p.compilerInput()!!.accept_hash(hash.success())
                } else {
                    NotImplementedException.raise_stop()
                }
            }

            Asseverate.CI_CACHED -> throw UnsupportedOperationException("Unimplemented case: $asseveration")
            Asseverate.CI_SPECCED -> {}
            Asseverate.EZ_PARSED -> throw UnsupportedOperationException("Unimplemented case: $asseveration")
            else -> throw IllegalArgumentException("Unexpected value: $asseveration")
        }//			NotImplementedException.raise_stop();
    }

    private val compilationEnclosure: CompilationEnclosure
        get() = compilation.compilationEnclosure

    override fun asseverate(aAsseveration: Asseveration) {
        aAsseveration.onLogProgress(compilation.compilationEnclosure)
    }

    override fun getInputTree(): EIT_InputTree {
        return compilation.inputTree
    }

    override fun getModuleList(): EIT_ModuleList {
        return moduleList
    }
}
