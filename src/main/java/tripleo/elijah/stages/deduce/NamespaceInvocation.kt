/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.lang.i.NamespaceStatement
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaNamespace

/**
 * Created 5/31/21 12:00 PM
 */
class NamespaceInvocation(val namespace: NamespaceStatement) : IInvocation { //
    private val resolveDeferred = DeferredObject<EvaNamespace, Void, Void>()

    fun resolveDeferred(): DeferredObject<EvaNamespace, Void, Void> {
        return resolveDeferred
    }

    fun resolvePromise(): Promise<EvaNamespace, Void, Void> {
        return resolveDeferred.promise()
    }

    override fun setForFunctionInvocation(aFunctionInvocation: FunctionInvocation) {
        aFunctionInvocation.namespaceInvocation = this
    }

    override fun onResolve(function: (EvaClass) -> Unit) {
        TODO("Not yet implemented")
    }
}
