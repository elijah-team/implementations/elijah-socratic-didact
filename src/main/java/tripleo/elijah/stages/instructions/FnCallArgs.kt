/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.instructions

import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.TypeTableEntry
import tripleo.elijah.util.Helpers

/**
 * Created 9/10/20 3:36 PM
 */
class FnCallArgs(val expression: Instruction, private val gf: BaseEvaFunction) : InstructionArgument {
    private var _type: TypeTableEntry? = null // the return type of the function call

    fun getArg(i: Int): InstructionArgument? {
        return expression.getArg(i)
    }

    private val args: List<InstructionArgument?>?
        get() = expression.args__

    val instructionArguments: List<InstructionArgument?>
        get() {
            val args = this.args
            return args!!.subList(1, args.size)
        }

    fun setType(tte2: TypeTableEntry?) {
        _type = tte2
    }

    override fun toString(): String {
        val index = DeduceTypes2.to_int(expression.args__!![0]!!)
        val instructionArguments = instructionArguments

        val collect2: Collection<String> = Helpers.mapCollectionElementsToString(instructionArguments)
        val commaed_strings = Helpers.String_join(" ", collect2)

        val procTableEntry = gf.prte_list[index]

        return String.format("(call %d [%s(%s)] %s)", index, procTableEntry.__debug_expression, procTableEntry.args,
                commaed_strings)
    }
} //
//
//

