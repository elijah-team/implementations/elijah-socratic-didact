package tripleo.elijah.stages.deduce

import tripleo.elijah.stages.gen_fn.GenerateFunctions
import tripleo.elijah.work.WorkList
import tripleo.elijah.work.WorkManager
import java.util.*
import java.util.function.Supplier

class RegisterClassInvocation2_env(private val env1: RegisterClassInvocation_env,
                                   private val workManager: WorkManager,
                                   private val workList: WorkList,
                                   val generateFunctions: Supplier<GenerateFunctions>
) {
    fun env1(): RegisterClassInvocation_env {
        return env1
    }

    fun workManager(): WorkManager {
        return workManager
    }

    fun workList(): WorkList {
        return workList
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as RegisterClassInvocation2_env
        return this.env1 == that.env1 && (this.workManager == that.workManager) && (this.workList == that.workList) && (this.generateFunctions == that.generateFunctions)
    }

    override fun hashCode(): Int {
        return Objects.hash(env1, workManager, workList, generateFunctions)
    }

    override fun toString(): String {
        return "RegisterClassInvocation2_env[" +
                "env1=" + env1 + ", " +
                "workManager=" + workManager + ", " +
                "workList=" + workList + ", " +
                "getGenerateFunctions=" + generateFunctions + ']'
    }
}
