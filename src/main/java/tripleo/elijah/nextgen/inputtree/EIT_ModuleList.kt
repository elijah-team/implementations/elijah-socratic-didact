package tripleo.elijah.nextgen.inputtree

import tripleo.elijah.g.GWorldModule
import java.util.stream.Stream

interface EIT_ModuleList {
    fun add(m: GWorldModule?)

    val mods: List<GWorldModule?>

    fun stream(): Stream<GWorldModule?>?
}
