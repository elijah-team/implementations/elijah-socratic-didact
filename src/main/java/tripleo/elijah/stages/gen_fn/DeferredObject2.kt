/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jdeferred2.Deferred
import org.jdeferred2.Promise
import org.jdeferred2.impl.AbstractPromise
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.OS_Element

/**
 * Created 9/2/21 12:09 AM
 */
open class DeferredObject2<D, F, P> : AbstractPromise<D, F, P>(), Deferred<D, F, P> {
    override fun notify(progress: P): Deferred<D, F, P> {
        synchronized(this) {
            check(isPending) { "Deferred object already finished, cannot notify progress" }
            triggerProgress(progress)
        }
        return this
    }

    override fun promise(): Promise<D, F, P> {
        return this
    }

    override fun reject(reject: F): Deferred<D, F, P> {
        synchronized(this) {
            check(isPending) { "Deferred object already finished, cannot reject again" }
            this.state = Promise.State.REJECTED
            this.rejectResult = reject
            try {
                triggerFail(reject)
            } finally {
                triggerAlways(state, null, reject)
            }
        }
        return this
    }

    fun reset() {
        state = Promise.State.PENDING
        rejectResult = null
        resolveResult = null
    }

    override fun resolve(resolve: D): Deferred<D, F, P> {
        synchronized(this) {
            check(isPending) { "Deferred object already finished, cannot resolve again" }
            this.state = Promise.State.RESOLVED
            this.resolveResult = resolve
            try {
                triggerDone(resolve)
            } finally {
                triggerAlways(state, resolve, null)
            }
        }
        return this
    }

    open fun resolve(resolve: OS_Element?): Deferred<OS_Element, Diagnostic, Void> {
        TODO("Not yet implemented")
    }
} //
//
//

