package tripleo.elijah.nextgen.outputstatement

import org.jetbrains.annotations.Contract

/**
 * @author Tripleo Nova
 */
interface EG_Statement {
    val text: String

    val explanation: EX_Explanation

    companion object {
        @JvmStatic
        @Contract(value = "_, _ -> new", pure = true)
        fun of(aText: String, aExplanation: EX_Explanation): EG_Statement {
            // FIXME 11/29
//             ElIntrinsics.checkNotNull(aText);
//             ElIntrinsics.checkNotNull(aExplanation);

            return object : EG_Statement {
                override val explanation: EX_Explanation
                    get() = aExplanation

                override val text: String
                    get() = aText
            }
        }
    }
}
