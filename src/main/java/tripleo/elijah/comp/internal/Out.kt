/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp.internal

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings
import tripleo.elijah.comp.Compilation
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.lang.i.ParserClosure
import tripleo.elijah.lang.impl.ParserClosureImpl

class Out(fn: String?, aCompilation: Compilation) {
    private val pc: ParserClosure = ParserClosureImpl(fn, aCompilation)

    fun closure(): ParserClosure {
        return pc
    }

    @SuppressFBWarnings("NM_METHOD_NAMING_CONVENTION")
    fun FinishModule() {
        pc.module().finish()
    }

    fun module(): OS_Module {
        return pc.module()
    }
} //
//
//

