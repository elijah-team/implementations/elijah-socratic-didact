package tripleo.elijah.stages.deduce

import tripleo.elijah.stages.deduce.tastic.ITastic

internal class TasticMap_1 : ITasticMap {
    private val _map_tasticMap: MutableMap<Any, ITastic> = HashMap()

    override fun containsKey(aO: Any): Boolean {
        return _map_tasticMap.containsKey(aO)
    }

    override fun get(aO: Any): ITastic {
        return _map_tasticMap[aO]!!
    }

    override fun put(aO: Any, aR: ITastic) {
        _map_tasticMap[aO] = aR
    }
}
