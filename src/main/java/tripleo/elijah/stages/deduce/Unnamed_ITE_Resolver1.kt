package tripleo.elijah.stages.deduce

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.BaseFunctionDef
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.lang.nextgen.names.i.EN_Name
import tripleo.elijah.lang.types.OS_FuncTypeImpl
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_fn.BaseTableEntry.StatusListener
import tripleo.elijah.stages.gen_fn.IdentTableEntry.ITE_Resolver_Result
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.util.Mode
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.Operation

internal class Unnamed_ITE_Resolver1(private val dt2: DeduceTypes2, private val ite: IdentTableEntry, private val generatedFunction: BaseEvaFunction,
                                     private val ctx: Context) : ITE_Resolver {
    private var _done = false
    private var _resolve_result: ITE_Resolver_Result? = null

    override fun check() {
        resolve_ident_table_entry2()
        ite.ident.get().name.addUsage(_inj().new_EN_DeduceUsage(ite.backlink, ite.__gf, ite))
    }

    override fun getResult(): ITE_Resolver_Result {
        return _resolve_result!!
    }

    override fun isDone(): Boolean {
        return _done
    }

    fun resolve_ident_table_entry2() {
        var instructionArgument: InstructionArgument? = IdentIA(ite.index, generatedFunction)

        run {
            // FIXME begging for recursion
            while (instructionArgument != null && instructionArgument is IdentIA) {
                val runningEntry: IdentTableEntry = (instructionArgument!! as IdentIA).entry

                var x: BaseTableEntry? = null
                val runningEntryBacklink = runningEntry.backlink
                if (runningEntryBacklink is IntegerIA) {
                    x = runningEntryBacklink.entry
                    //					if (vte.constructable_pte != null)
                    instructionArgument = null
                } else if (runningEntryBacklink is IdentIA) {
                    x = runningEntryBacklink.entry
                    instructionArgument = x.backlink
                } else if (runningEntryBacklink is ProcIA) {
                    x = runningEntryBacklink.entry
                    //					if (runningEntry.getCallablePTE() == null)
//						// turned out to be wrong (by double calling), so let's wrap it
//						runningEntry.setCallablePTE((ProcTableEntry) x);
                    // TODO Proc cannot have backlink??
                    instructionArgument = null // ((ProcTableEntry) x).backlink;
                } else if (runningEntryBacklink == null) {
                    instructionArgument = null
                    x = null
                }

                x?.addStatusListener(
                        FoundParent(x, runningEntry, runningEntry.ident.get().context, generatedFunction))
            }
        }

        if (ite.hasResolvedElement()) {
            _done = true
            val e = ite.resolvedElement
            _resolve_result = ITE_Resolver_Result(e!!)
            return
        }

        ite.calculateResolvedElement()

        val re = ite.resolvedElement
        if (re != null) {
            // ite.resolveExpectation.satisfy(re);

            val de3_ite = ite.getDeduceElement3(ite._deduceTypes2(), ite.__gf)

            if (re is VariableStatement) {
                val vs_name: EN_Name = re.nameToken.name
                vs_name.addUsage(_inj().new_EN_NameUsage(ite.ident.get().name, de3_ite))
                vs_name.addUsage(_inj().new_EN_DeduceUsage(ite.backlink, ite.__gf, ite))
            } else if (re is FunctionDef) {
                val fd_name: EN_Name = re.nameNode.name
                fd_name.addUsage(_inj().new_EN_NameUsage(ite.ident.get().name, de3_ite))
                fd_name.addUsage(_inj().new_EN_DeduceUsage(ite.backlink, ite.__gf, ite))
            } else {
                assert(false)
            }

            _done = true
            _resolve_result = _inj().new_ITE_Resolver_Result(re)
            return
        }
        if (true) {
            ite.addStatusListener(StatusListener { eh, newStatus ->
                if (newStatus != BaseTableEntry.Status.KNOWN) return@StatusListener
                val e = eh.element
                dt2.found_element_for_ite(generatedFunction, ite, e, ctx, dt2.central())
            })
        }
    }

    private fun _inj(): DeduceTypes2Injector {
        return dt2._inj()
    }

    inner class FoundParent @Contract(pure = true) constructor(private val bte: BaseTableEntry, private val ite: IdentTableEntry, private val ctx: Context,
                                                               private val generatedFunction: BaseEvaFunction) : StatusListener {
        override fun onChange(eh: IElementHolder, newStatus: BaseTableEntry.Status) {
            if (newStatus == BaseTableEntry.Status.KNOWN) {
                if (bte is VariableTableEntry) {
                    onChangeVTE(bte)
                } else if (bte is ProcTableEntry) {
                    onChangePTE(bte)
                } else if (bte is IdentTableEntry) {
                    onChangeITE(bte)
                }
                postOnChange(eh)
            }
        }

        private fun onChangeVTE(vte: VariableTableEntry) {
            val pot = dt2.getPotentialTypesVte(vte)
            if (vte.status == BaseTableEntry.Status.KNOWN && vte.typeTableEntry.attached != null && vte.resolvedElement != null) {
                val ty = vte.typeTableEntry.attached

                var ele2: OS_Element? = null

                try {
                    if (ty!!.type == OS_Type.Type.USER) {
                        val ty2 = dt2.resolve_type(ty, ty.typeName.context)
                        if (vte.typeTableEntry.genType.resolved == null) {
                            if (ty2.resolved.type == OS_Type.Type.USER_CLASS) {
                                vte.typeTableEntry.genType.copy(ty2)
                            }
                        }
                        val ele = ty2.resolved.element
                        val lrl = DeduceLookupUtils.lookupExpression(ite.ident.get(), ele.context, dt2)
                        ele2 = lrl.chooseBest(null)
                    } else ele2 = ty.element

                    if (ty is OS_FuncTypeImpl) {
                        vte.typePromise().then { result ->
                            val ele3: OS_Element = result.resolved.classOf
                            var lrl: LookupResultList? = null
                            try {
                                lrl = DeduceLookupUtils.lookupExpression(ite.ident.get(), ele3.context, dt2)

                                val best = lrl.chooseBest(null)
                                // README commented out because only firing for dir.listFiles, and we always use
                                // `best'
                                // if (best != ele2) LOG.err(String.format("2824 Divergent for %s, %s and %s",
                                // ite, best, ele2));;
                                ite.setStatus(BaseTableEntry.Status.KNOWN,
                                        _inj().new_GenericElementHolderWithType(best, ty, dt2))
                            } catch (aResolveError: ResolveError) {
                                aResolveError.printStackTrace()
                                dt2._errSink().reportDiagnostic(aResolveError)
                            }
                        }
                    } else {
                        val lrl = DeduceLookupUtils.lookupExpression(ite.ident.get(), ele2!!.context,
                                dt2)
                        val best = lrl!!.chooseBest(null)
                        // README commented out because only firing for dir.listFiles, and we always use `best'
                        // if (best != ele2) LOG.err(String.format("2824 Divergent for %s, %s and %s", ite, best, ele2));;
                        ite.setStatus(BaseTableEntry.Status.KNOWN, _inj().new_GenericElementHolderWithType(best, ty, dt2))
                    }
                } catch (aResolveError: ResolveError) {
                    aResolveError.printStackTrace()
                    dt2._errSink().reportDiagnostic(aResolveError)
                }
            } else if (pot.size == 1) {
                val tte = pot[0]
                val ty = tte.attached
                if (ty != null) {
                    when (ty.type) {
                        OS_Type.Type.USER -> vte_pot_size_is_1_USER_TYPE(vte, ty)
                        OS_Type.Type.USER_CLASS -> vte_pot_size_is_1_USER_CLASS_TYPE(vte, ty)
                        else -> throw IllegalStateException("Error")
                    }
                }
            }
        }

        private fun onChangePTE(aPte: ProcTableEntry) {
            if (aPte.status == BaseTableEntry.Status.KNOWN) { // TODO might be obvious
                if (aPte.functionInvocation != null) {
                    val fi = aPte.functionInvocation
                    val fd = fi?.function
                    if (fd is ConstructorDef) {
                        fi.generateDeferred().then { result ->
                            val constructorDef = result as IEvaConstructor
                            val ele = constructorDef.fd
                            try {
                                val lrl = DeduceLookupUtils.lookupExpression(ite.ident.get(),
                                        ele.context, dt2)
                                val best = lrl.chooseBest(null)!!
                                ite.setStatus(BaseTableEntry.Status.KNOWN, _inj().new_GenericElementHolder(best))
                            } catch (aResolveError: ResolveError) {
                                aResolveError.printStackTrace()
                                dt2._errSink().reportDiagnostic(aResolveError)
                            }
                        }
                    }
                } else throw NotImplementedException()
            } else {
                dt2.LOG.info("1621")
                var lrl: LookupResultList? = null
                try {
                    lrl = DeduceLookupUtils.lookupExpression(ite.ident.get(), ctx, dt2)
                    val best = lrl.chooseBest(null)!!
                    ite.resolvedElement = best
                    dt2.found_element_for_ite(null, ite, best, ctx, dt2.central())
                    //						ite.setStatus(BaseTableEntry.Status.KNOWN, best);
                } catch (aResolveError: ResolveError) {
                    aResolveError.printStackTrace()
                }
            }
        }

        private fun onChangeITE(identTableEntry: IdentTableEntry) {
            val env = _inj().new_DT_Env(dt2.LOG, dt2._errSink(), dt2.central())
            val ite_type = identTableEntry.type

            if (ite_type != null) {
                val ty = ite_type.attached

                var ele2: OS_Element? = null

                try {
                    if (ty!!.type == OS_Type.Type.USER) {
                        val ty2 = dt2.resolve_type(ty, ty.typeName.context)
                        if (ite_type.genType.resolved == null) {
                            if (ty2.resolved.type == OS_Type.Type.USER_CLASS) {
                                ite_type.genType.copy(ty2)
                            }
                        }
                        val ele = ty2.resolved.element
                        val lrl = DeduceLookupUtils.lookupExpression(this.ite.ident.get(), ele.context,
                                dt2)
                        ele2 = lrl.chooseBest(null)
                    } else ele2 = ty.classOf // TODO might fail later (use getElement?)


                    var lrl: LookupResultList? = null

                    lrl = DeduceLookupUtils.lookupExpression(this.ite.ident.get(), ele2!!.context, dt2)
                    val best = lrl.chooseBest(null)
                    // README commented out because only firing for dir.listFiles, and we always use
                    // `best'
//					if (best != ele2) LOG.err(String.format("2824 Divergent for %s, %s and %s", identTableEntry, best, ele2));;
                    this.ite.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(best))
                } catch (aResolveError: ResolveError) {
                    aResolveError.printStackTrace()
                    dt2._errSink().reportDiagnostic(aResolveError)
                }
            } else {
                if (!identTableEntry.fefi) {
                    val fefi = dt2._inj().new_Found_Element_For_ITE(generatedFunction, ctx, env,
                            dt2._inj().new_DeduceClient1(dt2))
                    fefi.action(identTableEntry)
                    identTableEntry.fefi = true
                    identTableEntry.onFefiDone { result ->
                        var lrl: LookupResultList? = null
                        val ele2: OS_Element?
                        try {
                            lrl = DeduceLookupUtils.lookupExpression(ite.ident.get(),
                                    result.resolved.classOf.context, dt2)
                            ele2 = lrl?.chooseBest(null)

                            if (ele2 != null) {
                                ite.setStatus(BaseTableEntry.Status.KNOWN,
                                        dt2._inj().new_GenericElementHolder(ele2))
                                ite.resolveTypeToClass(result.node)

                                if (ite.callablePTE != null) {
                                    val pte = ite.callablePTE
                                    val invocation = result.ci
                                    val fi = dt2.newFunctionInvocation(
                                            ele2 as BaseFunctionDef?, pte, invocation, dt2.phase)

                                    generatedFunction.addDependentFunction(fi)
                                }
                            }
                        } catch (aResolveError: ResolveError) {
                            aResolveError.printStackTrace()
                        }
                    }
                }
                // TODO we want to setStatus but have no USER or USER_CLASS to perform lookup
                // with
            }
        }

        /* @ensures ite.type != null; */
        private fun postOnChange(eh: IElementHolder) {
            if (ite.type == null && eh.element is VariableStatement) {
                val variableStatement = eh.element as VariableStatement
                val typ: TypeName = variableStatement.typeName()
                val ty: OS_Type = dt2._inj().new_OS_UserType(typ)

                try {
                    val ty2 = getTY2(variableStatement, typ, ty)

                    // no expression or TableEntryIV below
                    if (ty2 != null) {
                        val tte = generatedFunction
                                .newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null)

                        // trying to keep genType up to date
                        if (!ty.typeName.isNull) tte.attached = ty
                        tte.setAttached(ty2)

                        ite.type = tte
                        if ( /* !ty.getTypeName().isNull() && */!ty2.isNull) {
                            var skip = false

                            if (!ty.typeName.isNull) {
                                val gp = (ty.typeName as NormalTypeName).genericPart
                                if (gp != null) {
                                    if (gp.size() > 0 && ite.type?.genType?.nonGenericTypeName == null) {
                                        skip = true
                                    }
                                }
                            }
                            if (!skip) ite.type?.genType?.genCIForGenType2(dt2)
                        }
                    }
                } catch (aResolveError: ResolveError) {
                    dt2._errSink().reportDiagnostic(aResolveError)
                }
            }
        }

        private fun vte_pot_size_is_1_USER_TYPE(vte: VariableTableEntry, aTy: OS_Type?) {
            try {
                val ty2 = dt2.resolve_type(aTy!!, aTy.typeName.context)
                // TODO ite.setAttached(ty2) ??
                val ele = ty2.resolved.element
                val lrl = DeduceLookupUtils.lookupExpression(ite.ident.get(), ele.context, dt2)
                val best = lrl.chooseBest(null)
                ite.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(best))

                //									ite.setResolvedElement(best);
                val klass = ele as ClassStatement

                dt2.register_and_resolve(vte, klass)
            } catch (resolveError: ResolveError) {
                dt2._errSink().reportDiagnostic(resolveError)
            }
        }

        private fun vte_pot_size_is_1_USER_CLASS_TYPE(vte: VariableTableEntry, aTy: OS_Type?) {
            val klass = aTy!!.classOf
            var lrl: LookupResultList? = null
            try {
                lrl = DeduceLookupUtils.lookupExpression(ite.ident.get(), klass.context, dt2)
                val best = lrl.chooseBest(null)!!
                ite.resolvedElement = best

                val genType = dt2._inj().new_GenTypeImpl(klass)
                val typeName = vte.typeTableEntry.genType.nonGenericTypeName
                val ci: ClassInvocation = genType.genCI(typeName, dt2, dt2._errSink(), dt2.phase)
                //							resolve_vte_for_class(vte, klass);
                ci!!.onResolve { vte.resolveTypeToClass(it) }
            } catch (aResolveError: ResolveError) {
                dt2._errSink().reportDiagnostic(aResolveError)
            }
        }

        @Throws(ResolveError::class)
        private fun getTY2(aVariableStatement: VariableStatement,
                           aTypeName: TypeName,
                           aTy: OS_Type): GenType? {
            if (aTy.type != OS_Type.Type.USER) {
                assert(false)
                val genType = _inj().new_GenTypeImpl()
                genType.set(aTy)
                return genType
            }

            if (!aTypeName.isNull) {
                val r = UIR1_Rule(aTy, aVariableStatement)
                assert(r.product().mode() == Mode.SUCCESS)
                return r.product().success()
            }

            var ty2: GenType? = null
            if (bte is VariableTableEntry) {
                val vte_tte: TypeTableEntry = bte.typeTableEntry
                ty2 = _inj().new_GenTypeImpl()
                ty2.copy(vte_tte.genType)

                // TODO why not just return this ^^? instead of copying

                /*
				 * final OS_Type attached = vte_tte.getAttached(); if (attached == null) {
				 * type_is_null_and_attached_is_null_vte(); // ty2 will probably be null here }
				 * else { ty2 = _inj().new_GenTypeImpl(); ty2.set(attached); }
				 */
            } else if (bte is IdentTableEntry) {
                val tte = bte.type
                if (tte != null) {
                    val attached = tte.attached

                    if (attached == null) {
                        type_is_null_and_attached_is_null_ite(bte)
                        // ty2 will be null here
                    } else {
                        ty2 = _inj().new_GenTypeImpl()
                        ty2.set(attached)
                    }
                }
            }

            return ty2
        }

        private fun type_is_null_and_attached_is_null_ite(ite: IdentTableEntry) {
            val y = 2
            val pe = dt2.promiseExpectation<GenType>(ite, "Null USER type attached resolved")
            //			ite.onType(phase, _inj().new_OnType() {
//
//				@Override
//				public void typeDeduced(@NotNull OS_Type aType) {
//					// TODO Auto-generated method stub
//					pe.satisfy(aType);
//				}
//
//				@Override
//				public void noTypeFound() {
//					// TODO Auto-generated method stub
//
//				}
//			})
            // ;.done(new DoneCallback<GenType>() {
//				@Override
//				public void onDone(GenType result) {
//					pe.satisfy(result);
//					final OS_Type attached1 = result.resolved != null ? result.resolved : result.typeName;
//					if (attached1 != null) {
//						switch (attached1.getType()) {
//						case USER_CLASS:
//							FoundParent.this.ite.type = generatedFunction.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, attached1);
//							break;
//						case USER:
//							try {
//								OS_Type ty3 = resolve_type(attached1, attached1.getTypeName().getContext());
//								// no expression or TableEntryIV below
//								@NotNull TypeTableEntry tte4 = generatedFunction.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null);
//								// README trying to keep genType up to date
//								tte4.setAttached(attached1);
//								tte4.setAttached(ty3);
//								FoundParent.this.ite.type = tte4; // or ty2?
//							} catch (ResolveError aResolveError) {
//								aResolveError.printStackTrace();
//							}
//							break;
//						}
//					}
//				}
//			});
        }

        private fun type_is_null_and_attached_is_null_vte() {
            // LOG.err("2842 attached == null for "+((VariableTableEntry) bte).type);
            val pe = dt2.promiseExpectation<GenType>(bte as VariableTableEntry,
                    "Null USER type attached resolved")
            VTE_TypePromises.found_parent(pe, generatedFunction, (bte), ite, dt2)
        }


        internal inner class UIR1_Rule(aATy: OS_Type, vs: VariableStatement) : DT_Rule {
            private val aTy: OS_Type

            init {
                val typ = vs.typeName()

                val aTypeName = vs.typeName()
                assert(!aTypeName.isNull)
                aTy = aATy
            }

            fun product(): Operation<GenType> {
                val ty2: GenType
                assert(aTy.typeName != null)
                try {
                    ty2 = dt2.resolve_type(aTy, aTy.typeName.context)
                    return Operation.success(ty2)
                } catch (aE: ResolveError) {
                    return Operation.failure(aE)
                }
            }

            override fun ruleName(): String {
                return "Unnamed_ITE_Resolver1::getTY2"
            }
        }
    }
}

internal interface DT_Rule {
    fun ruleName(): String
}

internal /*data*/ class UIR1_Env() {
//    override fun hashCode(): Int {
//        return 1
//    }
//
//    override fun equals(obj: Any?): Boolean {
//        return obj === this || obj != null && obj.javaClass == this.javaClass
//    }
//
//    override fun toString(): String {
//        return "UIR1_Env[]"
//    }
}
