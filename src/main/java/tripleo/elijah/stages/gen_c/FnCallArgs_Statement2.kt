package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.stages.gen_c.GenerateC.GetAssignmentValue
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.BaseTableEntry
import tripleo.elijah.stages.gen_fn.IdentTableEntry
import tripleo.elijah.stages.gen_fn.ProcTableEntry
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.stages.logging.ElLog

internal class FnCallArgs_Statement2(private val generateC: GenerateC, private val gf: BaseEvaFunction, private val LOG: ElLog,
                                     private val inst: Instruction, private val pte: ProcTableEntry,
                                     private val getAssignmentValue: GetAssignmentValue) : EG_Statement {
    override val explanation: EX_Explanation
        get() = withMessage("FnCallArgs_Statement2")

    override val text: String
        get() {
            val sb = StringBuilder()

            val aGenerateC = generateC

            if (pte.expression_num is IntegerIA) {
            } else if (pte.expression_num is IdentIA) {
                var ia2=pte.expression_num
                val idte: IdentTableEntry = ia2.entry
                if (idte.status == BaseTableEntry.Status.UNCHECKED) {
                    // final DeduceTypes2 deduceTypes2 = pte.getDeduceElement3().deduceTypes2();
                    // final BaseEvaFunction gf1 = ia2.gf;

                    val deduceTypes2 = idte._deduceTypes2()
                    val gf1 = idte._generatedFunction()

                    val de3_idte = idte.getDeduceElement3(deduceTypes2, gf1)
                    de3_idte.sneakResolve()
                }

                if (idte.status == BaseTableEntry.Status.KNOWN) {
                    val reference = CReference(aGenerateC._repo, aGenerateC.ce)
                    val functionInvocation = pte.functionInvocation
                    if (functionInvocation == null || functionInvocation.function === LangGlobals.defaultVirtualCtor) {
                        reference.getIdentIAPath(ia2, AOG.GET, null)
                        val ava = getAssignmentValue.getAssignmentValueArgs(inst, gf,
                                generateC.LOG)
                        val sll = ava!!.stringList()
                        reference.args(sll)
                        val path = reference.build()
                        sb.append(Emit.emit("/*829*/") + path)
                    } else {
                        val pte_generated = functionInvocation.eva
                        if (idte.resolvedType() == null && pte_generated != null) idte.resolveTypeToClass(pte_generated)
                        reference.getIdentIAPath(ia2, AOG.GET, null)
                        val ava = getAssignmentValue.getAssignmentValueArgs(inst, gf,
                                generateC.LOG)
                        val sll = ava!!.stringList()
                        reference.args(sll)
                        val path = reference.build()
                        sb.append(Emit.emit("/*827*/") + path)
                    }
                } else {
                    val zone_path = aGenerateC._zone.getPath(ia2)

                    // 08/13 tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4("763 " + zone_path);
                    val path = gf.getIdentIAPathNormal(ia2)
                    sb.append(Emit.emit("/*828*/") + String.format("%s is UNKNOWN", path))
                }
            }

            return sb.toString()
        }
}
