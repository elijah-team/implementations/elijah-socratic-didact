package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Diagnostic.withMessage
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.BaseFunctionDef
import tripleo.elijah.lang.impl.IdentExpressionImpl
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.lang.types.OS_FuncType
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceClient3
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.post_bytecode.DED.DED_PTE
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3.DeduceElement3_Kind
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.Eventual
import tripleo.elijah.util2.ReadySupplier_1
import tripleo.elijah.work.WorkList
import tripleo.elijah.work.WorkList__
import java.util.*
import java.util.function.Consumer

class DeduceElement3_ProcTableEntry(val tablePrincipal: ProcTableEntry,
                                    private val deduceTypes2: DeduceTypes2?,
                                    @JvmField val generatedFunction: BaseEvaFunction) : IDeduceElement3 {
    @JvmField
    var instruction: Instruction? = null

    fun _action_002_no_resolved_element(_backlink: InstructionArgument?,
                                        backlink: ProcTableEntry,
                                        dc: DeduceClient3,
                                        ite: IdentTableEntry,
                                        errSink: ErrSink,
                                        phase: DeducePhase) {
        val resolvedElement = backlink.resolvedElement ?: return

        // throw new AssertionError(); // TODO feb 20


        try {
            val lrl2 = dc.lookupExpression(ite.ident.get(), resolvedElement.context)
            val best = lrl2.chooseBest(null)!!
            ite.setStatus(BaseTableEntry.Status.KNOWN, dc._deduceTypes2()._inj().new_GenericElementHolder(best))
        } catch (aResolveError: ResolveError) {
            errSink.reportDiagnostic(aResolveError)
            assert(false)
        }

        action_002_1(tablePrincipal, ite, false, phase, dc)
    }

    fun _inj() = _inj

    val _inj: DeduceTypes2Injector
        get() = (deduceTypes2()!!)._inj()

    private fun action_002_1(pte: ProcTableEntry, ite: IdentTableEntry,
                             setClassInvocation: Boolean, phase: DeducePhase,
                             dc: DeduceClient3) {
        val resolvedElement = ite.resolvedElement!!

        var ci: ClassInvocation? = null

        val dt2 = dc._deduceTypes2()

        if (pte.functionInvocation == null) {
            val fi: FunctionInvocation

            if (resolvedElement is ClassStatement) {
                // assuming no constructor name or generic parameters based on function syntax
                ci = _inj().new_ClassInvocation(resolvedElement, null, ReadySupplier_1(dt2))
                ci = phase.registerClassInvocation(ci)
                fi = phase.newFunctionInvocation(null, pte, ci)
            } else if (resolvedElement is FunctionDef) {
                val invocation = dc.getInvocation((generatedFunction as EvaFunction))
                fi = phase.newFunctionInvocation(resolvedElement as BaseFunctionDef, pte, invocation)
                if (resolvedElement.getParent() is ClassStatement) {
                    val classStatement = fi.function?.parent as ClassStatement?
                    ci = _inj().new_ClassInvocation(classStatement, null, ReadySupplier_1(dt2)) // TODO generics
                    ci = phase.registerClassInvocation(ci)
                }
            } else {
                throw IllegalStateException()
            }

            if (setClassInvocation) {
                if (ci != null) {
                    pte.classInvocation = ci
                } else SimplePrintLoggerToRemoveSoon.println_err2("542 Null ClassInvocation")
            }

            pte.functionInvocation = fi
        }

        //        el   = resolvedElement;
//        ectx = el.getContext();
    }

    override fun deduceTypes2(): DeduceTypes2? {
        return deduceTypes2
    }

    override fun elementDiscriminator(): DED {
        return DED_PTE(tablePrincipal)
    }

    override fun generatedFunction(): BaseEvaFunction {
        return generatedFunction
    }

    override fun genType(): GenType {
        throw UnsupportedOperationException("no type for PTE")
    } // TODO check correctness

    override fun getPrincipal(): OS_Element {
        // return principal.getDeduceElement3(deduceTypes2,
        // generatedFunction).getPrincipal(); // README infinite loop

        return tablePrincipal.resolvedElement!! // getDeduceElement3(deduceTypes2, generatedFunction).getPrincipal();
    }

    override fun kind(): DeduceElement3_Kind {
        return DeduceElement3_Kind.GEN_FN__PTE
    }

    override fun resolve(aContext: Context, dt2: DeduceTypes2) {
        throw UnsupportedOperationException()
    }

    override fun resolve(aIdentIA: IdentIA, aContext: Context, aFoundElement: FoundElement) {
        throw UnsupportedOperationException()
    }

    fun doFunctionInvocation() {
        val fi = tablePrincipal.functionInvocation

        if (fi == null) {
            if (tablePrincipal.__debug_expression is ProcedureCallExpression) {
                val exp = tablePrincipal.__debug_expression as ProcedureCallExpression
                val left: IExpression = exp.getLeft()

                if (left is DotExpression) {
                    if (left.getLeft() is IdentExpression
                            && left.right is IdentExpression) {
                        val rl = left.right as IdentExpression
                        if (rl.text == "a1") {
                            val gc = arrayOfNulls<EvaClass>(1)

                            val vrl = generatedFunction.vte_lookup(rl.getText())!!

                            val vte = (vrl as IntegerIA).entry

                            vte.typePromise().then { left_type: GenType ->
                                val cs = left_type.resolved.classOf // TODO we want a
                                // DeduceClass here.
                                // EvaClass may suffice
                                val ci = deduceTypes2!!._phase().registerClassInvocation(cs)
                                ci.resolvePromise().then { gc2: EvaClass? ->
                                    gc[0] = gc2
                                }

                                val rr: IdentExpression? = null // exp.right.right // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
                                val lrl = cs.context.lookup(rr!!.text)
                                val best = lrl.chooseBest(null)
                                if (best != null) {
                                    val `fun` = best as FunctionDef

                                    val fi2 = deduceTypes2._phase().newFunctionInvocation(`fun`,
                                            null, ci) // TODO pte??

                                    tablePrincipal.functionInvocation = fi2 // TODO pte above

                                    val j = fi2.generateFunction(deduceTypes2, best)
                                    j.run(null)

                                    val ite = (tablePrincipal.expression_num as IdentIA)
                                            .entry
                                    val attached = ite.type?.attached

                                    fi2.generatePromise().then { gf: BaseEvaFunction? ->
                                        val y4 = 4
                                    }

                                    if (attached is OS_FuncType) {
                                        val x = gc[0]

                                        fi2.generatePromise().then { gf: BaseEvaFunction? ->
                                            val y4 = 4
                                        }

                                        val y = 2
                                    }
                                    val yy = 2
                                }
                            }
                            val y = 2
                        }
                    }
                }
            }
        }
    }

    class LFOE_Action_Results(private val aDeduceTypes2: DeduceTypes2,  // input/not really needed
                              private val wl: WorkList,  // input/not really needed
                              private val addJobs: Consumer<WorkList>,  // input/not really needed
                              private val actualResults: List<Any>,
                              private val createdFunctionInvocation: Eventual<FunctionInvocation>
    ) {
        fun aDeduceTypes2(): DeduceTypes2 {
            return aDeduceTypes2
        }

        fun wl(): WorkList {
            return wl
        }

        fun addJobs(): Consumer<WorkList> {
            return addJobs
        }

        fun actualResults(): List<Any> {
            return actualResults
        }

        fun createdFunctionInvocation(): Eventual<FunctionInvocation> {
            return createdFunctionInvocation
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as LFOE_Action_Results
            return this.aDeduceTypes2 == that.aDeduceTypes2 && (this.wl == that.wl) && (this.addJobs == that.addJobs) && (this.actualResults == that.actualResults) && (this.createdFunctionInvocation == that.createdFunctionInvocation)
        }

        override fun hashCode(): Int {
            return Objects.hash(aDeduceTypes2, wl, addJobs, actualResults, createdFunctionInvocation)
        }

        override fun toString(): String {
            return "LFOE_Action_Results[" +
                    "aDeduceTypes2=" + aDeduceTypes2 + ", " +
                    "wl=" + wl + ", " +
                    "addJobs=" + addJobs + ", " +
                    "actualResults=" + actualResults + ", " +
                    "createdFunctionInvocation=" + createdFunctionInvocation + ']'
        }
    }

    fun lfoe_action(aDeduceTypes2: DeduceTypes2,
                    wl: WorkList,
                    addJobs: Consumer<WorkList>,
                    resultconsumer: Consumer<LFOE_Action_Results?>) {
        //assert aDeduceTypes2 == deduceTypes2;
        if (aDeduceTypes2 !== deduceTypes2) {
            SimplePrintLoggerToRemoveSoon.println_err_4("503262 deduceTypes divergence")
            //throw new AssertionError();
        }

        val q = __LFOE_Q(aDeduceTypes2.wm, wl, aDeduceTypes2)
        val efi = Eventual<FunctionInvocation>()
        efi.then(lfoe_action__FunctionInvocationDoneCallback(this, aDeduceTypes2, addJobs, q, wl))

        val actualResultList: List<Any> = ArrayList()
        val virtualResult = LFOE_Action_Results(aDeduceTypes2, wl, addJobs, actualResultList, efi)
        val fi2 = tablePrincipal.functionInvocation

        try {
            if (fi2 == null) {
                __lfoe_action__getFunctionInvocation(tablePrincipal, aDeduceTypes2).then { p: FunctionInvocation -> efi.resolve(p) }

                //if (fi == null)
                //	return;
            } else {
                efi.resolve(fi2)
            }
        } finally {
            resultconsumer.accept(virtualResult)
        }
    }


    class _1(private val functionInvocation: FunctionInvocation,
             private val parentNamespace: NamespaceStatement,
             private val workList: WorkList,
             private val deducePhase: DeducePhase,
             private val addJobs: Consumer<WorkList>,
             private val q: __LFOE_Q
    ) {
        fun functionInvocation(): FunctionInvocation {
            return functionInvocation
        }

        fun parentNamespace(): NamespaceStatement {
            return parentNamespace
        }

        fun workList(): WorkList {
            return workList
        }

        fun deducePhase(): DeducePhase {
            return deducePhase
        }

        fun addJobs(): Consumer<WorkList> {
            return addJobs
        }

        fun q(): __LFOE_Q {
            return q
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as _1
            return this.functionInvocation == that.functionInvocation && (this.parentNamespace == that.parentNamespace) && (this.workList == that.workList) && (this.deducePhase == that.deducePhase) && (this.addJobs == that.addJobs) && (this.q == that.q)
        }

        override fun hashCode(): Int {
            return Objects.hash(functionInvocation, parentNamespace, workList, deducePhase, addJobs, q)
        }

        override fun toString(): String {
            return "_1[" +
                    "functionInvocation=" + functionInvocation + ", " +
                    "parentNamespace=" + parentNamespace + ", " +
                    "workList=" + workList + ", " +
                    "deducePhase=" + deducePhase + ", " +
                    "addJobs=" + addJobs + ", " +
                    "q=" + q + ']'
        }
    }

    data class _0(
// TODO 10/18 $env.process($this)
        val functionInvocation: FunctionInvocation,
        val classInvocation: ClassInvocation,
        val parentClass: ClassStatement,
        val deducePhase: DeducePhase,
        val addJobs: Consumer<WorkList>,
        internal val q: _LFOE_Q,
    ) {
        fun functionInvocation(): FunctionInvocation = functionInvocation
        fun classInvocation(): ClassInvocation = classInvocation
        fun parentClass(): ClassStatement = parentClass
        fun deducePhase(): DeducePhase = deducePhase
        fun addJobs(): Consumer<WorkList> = addJobs
    }

    fun __lfoe_action__proceed(o0: _0) {
        val fi = o0.functionInvocation()
        var ci = o0.classInvocation()
        val aParent = o0.parentClass()
        val addJobs = o0.addJobs()
        val q = o0.q
        val phase = o0.deducePhase()

        ci = phase.registerClassInvocation(ci)

        val kl = ci.klass // TODO Don't you see aParent??
        val fd2 = fi.function
        var state = 0

        state = if (fd2 === LangGlobals.defaultVirtualCtor) {
            if (fi.pte?.args?.size == 0) 1
            else 2
        } else if (fd2 is ConstructorDef) {
            if (fi.classInvocation?.constructorName != null) 3
            else 2
        } else {
            if (fi.function == null && fi.classInvocation != null) 3
            else 4
        }

        val generateFunctions: GenerateFunctions? = null

        when (state) {
            1 -> {
                assert(fi.pte?.args?.size == 0)
                // default ctor
                q.enqueue_default_ctor(generateFunctions, fi, null)
            }

            2 -> q.enqueue_ctor(generateFunctions, fi, fd2?.nameNode)
            3 -> {
                // README this is a special case to generate constructor
                // TODO should it be GenerateDefaultCtor? (check args size and ctor-name)
                val constructorName = fi.classInvocation?.constructorName
                val constructorName1: IdentExpression? = if (constructorName != null
                ) IdentExpressionImpl.forString(constructorName)
                else null
                q.enqueue_ctor(generateFunctions, fi, constructorName1)
            }

            4 -> q.enqueue_function(generateFunctions, fi, phase.codeRegistrar)
            else -> throw NotImplementedException()
        }        // addJobs.accept(wl);
    }

    fun __lfoe_action__proceed(o1: _1) {
        val fi = o1.functionInvocation()
        val aParent = o1.parentNamespace()
        val wl = o1.workList()
        val phase = o1.deducePhase()
        val addJobs = o1.addJobs()
        val q: _LFOE_Q = o1.q()

        // ci = phase.registerClassInvocation(ci);
        val module1 = aParent.context.module()

        val nsi = phase.registerNamespaceInvocation(aParent)

        val cr = phase.codeRegistrar

        q.enqueue_namespace({ phase.generatePhase.getGenerateFunctions(module1) }, nsi, phase.generatedClasses, cr)
        q.enqueue_function({ phase.generatePhase.getGenerateFunctions(module1) }, fi, cr)

        // addJobs.accept(wl); // TODO 10/17 should we remove this or uncomment it?
    }

    fun sneakResolve_IDTE(el: OS_Element,
                          aDeduceElement3IdentTableEntry: DeduceElement3_IdentTableEntry): Boolean {
        var b = false

        val left = tablePrincipal.__debug_expression?.left
        if (left === aDeduceElement3IdentTableEntry.principal.ident) {
            // TODO is this duplication ok??
            val de3_ite_holder = _inj().new_DE3_ITE_Holder(el,
                    aDeduceElement3IdentTableEntry)
            if (tablePrincipal.status == BaseTableEntry.Status.UNCHECKED) {
                tablePrincipal.setStatus(BaseTableEntry.Status.KNOWN, de3_ite_holder)
                de3_ite_holder.commitGenTypeActions()
            }
            b = true // TODO include this in block above??
        }

        // if (principal.expression_num instanceof IdentIA identIA) {
        // var ite = identIA.getEntry();
        if (tablePrincipal.evaExpression?.entry is IdentTableEntry) {
            val ite = tablePrincipal.evaExpression!!.entry as IdentTableEntry
            val ident1: IdentExpression = ite.getIdent()!!

            assert(ident1 is IdentExpression // README null check
            )
            val ident_name = ident1.name

            val u = ident_name.understandings

            if (u.size == 0 || u.size == 2) {
            } else {
                NotImplementedException.raise()
            }

            // ident_name.addUnderstanding(_inj().new_ENU_LookupResult(...));
            if (el is ClassStatement) {
                ident_name.addUnderstanding(_inj().new_ENU_ClassName())
                ident_name.addUnderstanding(_inj().new_ENU_ConstructorCallTarget()) // FIXME 07/20 look here later
            } else if (el is FunctionDef) {
                ident_name.addUnderstanding(_inj().new_ENU_FunctionName())
            }
        }

        // principal.getIdent().getName().addUnderstanding(_inj().new_ENU_ConstructorCallTarget());
        return b
    }

    override fun toString(): String {
        return "DeduceElement3_ProcTableEntry{" + "principal=" + tablePrincipal + '}'
    }

    companion object {
        // TODO class Action<FunctionInvocation>
        private fun __lfoe_action__getFunctionInvocation(pte: ProcTableEntry,
                                                         aDeduceTypes2: DeduceTypes2): Eventual<FunctionInvocation> {
            val efi = Eventual<FunctionInvocation>()

            // Action<FunctionInvocation> action = new ...
            // action.provide(ClassStatement.class, (left-is-class ...|left-is-function), ...
            // action.fail(e-is-null)
            // action.fail(e-is-not-class-or-function)
            var fi: FunctionInvocation
            if (pte.__debug_expression != null && pte.expression_num != null) {
                if (pte.__debug_expression is ProcedureCallExpression) {
                    val exp = pte.__debug_expression as ProcedureCallExpression

                    if (exp.getLeft() is IdentExpression) {
                        val expLeft = exp.getLeft() as IdentExpression

                        val left: String = expLeft.getText()
                        val lrl: LookupResultList = expLeft.getContext().lookup(left)
                        val e = lrl.chooseBest(null)
                        if (e != null) {
                            if (e is ClassStatement) {
                                val ci: ClassInvocation = aDeduceTypes2.phase.registerClassInvocation(e)
                                pte.classInvocation = ci
                            } else if (e is FunctionDef) {
                                val parent = e.getParent()

                                if (parent is ClassStatement) {
                                    val ci: ClassInvocation = aDeduceTypes2.phase.registerClassInvocation(parent)
                                    pte.classInvocation = ci
                                }
                            } else {
                                throw NotImplementedException()
                            }
                        }
                    }
                }
            }

            var invocation = pte.classInvocation

            if (invocation == null && pte
                            .functionInvocation != null /* never true if we are in this function (check only use guard)! */) {
                invocation = pte.functionInvocation!!.classInvocation
            }
            if (invocation == null) {
                val diagnostic = withMessage("523523",
                        "can't find invocation in __lfoe_action__getFunctionInvocation",
                        Diagnostic.Severity.WARN) // "WARN" !!
                efi.fail(diagnostic)
                return efi
            }

            val de3_pte = pte.getDeduceElement3(aDeduceTypes2, pte.__gf!!) // !! pte.__gf

            //de3_pte.();
            val fi2 = aDeduceTypes2._phase().newFunctionInvocation(LangGlobals.defaultVirtualCtor, pte,
                    invocation)

            val q: _LFOE_Q = __LFOE_Q(aDeduceTypes2.wm, WorkList__(), aDeduceTypes2)

            val generateFunctions = aDeduceTypes2.getGenerateFunctions(invocation.klass.context.module())

            val finalInvocation = invocation
            q.enqueue_default_ctor(generateFunctions, fi2) { aBaseEvaFunctionEventual: Eventual<BaseEvaFunction> ->
                aBaseEvaFunctionEventual.then { ef2: BaseEvaFunction ->
                    val zp = aDeduceTypes2.zeroGet(pte, ef2)
                    val fi3 = aDeduceTypes2.newFunctionInvocation(ef2.fd, pte, finalInvocation, aDeduceTypes2.phase)
                    efi.resolve(fi3)
                }
            }

            return efi
        }
    }
}
