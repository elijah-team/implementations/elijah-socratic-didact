package tripleo.elijah.nextgen.reactive

/**
 * A callback
 */
interface Reactivable {
    fun respondTo(aDimension: ReactiveDimension?)
}