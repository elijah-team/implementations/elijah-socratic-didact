package tripleo.elijah.stages.gen_c

import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.stages.instructions.IntegerIA

internal class GCFM_Inst_AGNK(private val generateCodeForMethod: Generate_Code_For_Method, private val gc: GenerateC,
                              private val gf: WhyNotGarish_BaseFunction, private val instruction: Instruction) {
    val text: String
        get() {
            val target = instruction.getArg(0)
            val value = instruction.getArg(1)

            val realTarget = gc.getRealTargetName(gf, target as IntegerIA, AOG.ASSIGN)
            val assignmentValue = gc.getAssignmentValue(gf.self, value, gf.cheat())
            val s = String.format(Emit.emit("/*278*/") + "%s = %s;", realTarget, assignmentValue)

            return s
        }
}
