/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.ci_impl

import antlr.Token
import com.google.common.base.Preconditions
import lombok.Getter
import tripleo.elijah.ci.GenerateStatement
import tripleo.elijah.g.GDirective
import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.xlang.LocatableString
import java.util.*
import java.util.stream.Stream
import kotlin.collections.ArrayList

/**
 * Created 9/6/20 12:04 PM
 */
class GenerateStatementImpl : GenerateStatement {
    private val dirs: MutableList<Directive> = ArrayList()

    // FIXME 24/01/10 TokenExtensions.addDirective[To](...)
    override fun addDirective(token: Token, expression: IExpression) {
        dirs.add(Directive(LocatableString.of(token), expression))
    }

    override fun dirStream(): Stream<GDirective> {
        return dirs.stream()
                .map { d: Directive -> d }
    }

//    @Getter
    class Directive(private val name: LocatableString, internal val expression: IExpression) : GDirective {
        override fun sameName(aName: String): Boolean {
            Preconditions.checkNotNull(aName)
            return aName == this.name.string
        }
    }
}
