package tripleo.elijah.stages.deduce.nextgen

import tripleo.elijah.stages.deduce.DeducePhase
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.GeneratePhase
import tripleo.elijah.util2.Eventual

/**
 * Pivot off of a [DeduceTypes2] instance to provide "Context" useful when creating things in the Deduce realm
 *
 * Chatty explanation:
 *
 * 1. Pivot means only one variable is retained
 * a. the others could be stored as members for debugging (design decision)
 * b. there is no reason for computation, as they should all be created (so JIT can inline ;)
 * 2. #makeGenerated_fi__Eventual is used in one place as of now
 *
 */
class DefaultDeduceCreationContext(private val deduceTypes2: DeduceTypes2) : DeduceCreationContext {
    override fun getDeducePhase(): DeducePhase {
        return deduceTypes2.phase
    }

    override fun getDeduceTypes2(): DeduceTypes2 {
        return deduceTypes2
    }

    override fun getGeneratePhase(): GeneratePhase {
        return deducePhase.generatePhase
    }

    override fun makeGenerated_fi__Eventual(aFunctionInvocation: FunctionInvocation): Eventual<BaseEvaFunction> {
        return aFunctionInvocation.makeGenerated__Eventual(this, null)
    }
}
