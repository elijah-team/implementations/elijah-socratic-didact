package tripleo.elijah.comp

import com.google.common.collect.Multimap
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.nextgen.i.CP_Path
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.util.Helpers
import java.util.stream.Collectors

internal class MesonFile(
		private val writeMesonPipeline: WriteMesonPipeline,
		private val subDir: String,
		private val lsp_outputs: Multimap<CompilerInstructions, String>,
		private val compilerInstructions: CompilerInstructions,
		val path: CP_Path,
) : EG_Statement {
	override val explanation: EX_Explanation
		get() = EX_Explanation.withMessage("MesonFile")
	val pathString: String
		get() = path.path.toString()

	override val text: String
		get() {
			val files_ = lsp_outputs[compilerInstructions]
			val files = files_.stream().filter { x: String -> x.endsWith(".c") }
					.map { x: String -> String.format("\t'%s',", writeMesonPipeline.pullFileName(x)) }
					.collect(Collectors.toUnmodifiableSet())

			val sb = StringBuilder()
			sb.append(String.format("%s_sources = files(\n%s\n)", subDir, Helpers.String_join("\n", files)))
			sb.append("\n")
			sb.append(
					String.format("%s = static_library('%s', %s_sources, install: false,)", subDir, subDir, subDir)) // include_directories,
			// dependencies:
			// [],
			sb.append("\n")
			sb.append("\n")
			sb.append(String.format("%s_dep = declare_dependency( link_with: %s )", subDir, subDir)) // include_directories
			sb.append("\n")

			val s: String = sb.toString()
			return s
		}
}
