/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 */
package tripleo.elijah.stages.deduce;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.stages.gen_fn.EvaClass;

/**
 * Marker interface to show intent
 * <p>
 * Created 6/8/21 2:29 AM
 */
public interface IInvocation {
	void setForFunctionInvocation(FunctionInvocation aFunctionInvocation);

	void onResolve(@NotNull Function1<EvaClass, Unit> function);
}
