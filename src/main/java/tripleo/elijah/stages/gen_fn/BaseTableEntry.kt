/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jdeferred2.Deferred
import org.jdeferred2.DoneCallback
import org.jdeferred2.FailCallback
import org.jdeferred2.Promise
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.util.NotImplementedException

/**
 * Created 2/4/21 10:11 PM
 */
abstract class BaseTableEntry {
    enum class Status {
        KNOWN, UNCHECKED, UNKNOWN
    }

    fun interface StatusListener {
        fun onChange(eh: IElementHolder, newStatus: Status)
    }

    val _p_elementPromise: DeferredObject2<OS_Element, Diagnostic, Void> = object : DeferredObject2<OS_Element, Diagnostic, Void>() {
        override fun resolve(resolve: OS_Element?): Deferred<OS_Element, Diagnostic, Void> {
            if (resolve == null) {
                if (this@BaseTableEntry is VariableTableEntry) {
                    val vte = this@BaseTableEntry as VariableTableEntry
                    when (vte.vtt) {
                        VariableTableType.SELF, VariableTableType.TEMP, VariableTableType.RESULT -> {
                            return super.resolve(resolve!!)
                        }

                        else -> {/*passthru*/}
                    }
                }
                NotImplementedException.raise()
            }
            return super.resolve(resolve!!)
        }
    }
    private val statusListenerList: MutableList<StatusListener> = ArrayList()
    @JvmField
    protected var __dt2: DeduceTypes2? = null
    @JvmField
    var __gf: BaseEvaFunction? = null

    // endregion resolved_element
    // region resolved_element
    // region status
    @JvmField
    var status: Status = Status.UNCHECKED

    @JvmField
    var typeResolve: DeduceTypeResolve? = null

    fun _fix_table(aDeduceTypes2: DeduceTypes2?, aEvaFunction: BaseEvaFunction) {
        __dt2 = aDeduceTypes2
        __gf = aEvaFunction
    }

    fun addStatusListener(sl: StatusListener) {
        statusListenerList.add(sl)
    }

    fun elementPromise(dc: DoneCallback<OS_Element>?, fc: FailCallback<Diagnostic>?) {
        if (dc != null) _p_elementPromise.then(dc)
        if (fc != null) _p_elementPromise.fail(fc)
    }

    open var resolvedElement: OS_Element?
        get() {
            if (_p_elementPromise.isResolved) {
                val xx = arrayOf<OS_Element?>(null)

                _p_elementPromise.then { x: OS_Element? -> xx[0] = x }

                return xx[0]
            }

            return null
        }
        set(aResolved_element) {
            if (_p_elementPromise.isResolved) {
                NotImplementedException.raise()
            } else _p_elementPromise.resolve(aResolved_element!!)
        }

    fun setStatus(newStatus: Status,  /* @NotNull */
                  eh: IElementHolder?) {
        status = newStatus
        assert(newStatus != Status.KNOWN || eh?.element != null)
        for (i in statusListenerList.indices) {
            val statusListener = statusListenerList[i]
            statusListener.onChange(eh!!, newStatus)
        }
        if (newStatus == Status.UNKNOWN) if (!_p_elementPromise.isRejected) _p_elementPromise.reject(ResolveUnknown())
    }

    protected open fun setupResolve() {
        typeResolve = DeduceTypeResolve(this, NULL_DeduceTypes2())
    }

    fun typeResolve(aGt: GenType?) {
        typeResolve!!.typeResolve(aGt)
    }

    fun typeResolvePromise(): Promise<GenType, ResolveError, Void> {
        return typeResolve!!.typeResolution()
    }
} //
//
//

