package tripleo.elijah.comp.impl

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.Pipeline
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.PipelineMember
import tripleo.elijah.comp.i.ICompilationAccess
import tripleo.elijah.g.*
import tripleo.elijah.stages.deduce.IFunctionMapHook
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.logging.ElLog.Verbosity
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.*

class DefaultCompilationAccess @Contract(pure = true) constructor(val compilation_: Compilation) : ICompilationAccess {
    private val pipelines = Pipeline()

    override fun addFunctionMapHook(aFunctionMapHook1: GFunctionMapHook) {
        val aFunctionMapHook = aFunctionMapHook1 as IFunctionMapHook
        compilation_.compilationEnclosure.pipelineLogic.dp.addFunctionMapHook(aFunctionMapHook)
    }

    override fun addPipeline(pl: GPipelineMember) {
        pipelines.add(pl as PipelineMember)
    }

    override fun functionMapHooks(): List<GFunctionMapHook> {
        return Collections.unmodifiableList<GFunctionMapHook>(compilation_.compilationEnclosure.pipelineLogic.dp.functionMapHooks)
    }

    override fun getCompilation(): Compilation = compilation_

    override fun internal_pipelines(): GPipeline {
        return pipelines
    }

    override fun setPipelineLogic(pl: GPipelineLogic) {
        setPipelineLogic_((pl as PipelineLogic)!!)
    }

    //@Override
    fun setPipelineLogic_(pl: PipelineLogic?) {
        //assert compilation.getCompilationEnclosure().getPipelineLogic() == null;
        if (compilation_.compilationEnclosure.pipelineLogic != null) {
            //throw new AssertionError();
            SimplePrintLoggerToRemoveSoon.println_err_4("903056 pipelineLogic already set")
        } else {
            compilation_.compilationEnclosure.pipelineLogic = pl
        }
    }

    override fun testSilence(): Verbosity {
        return if (compilation_.cfg().silent) ElLog.Verbosity.SILENT else ElLog.Verbosity.VERBOSE
    }

    override fun getCompilationEnclosure(): GCompilationEnclosure {
        return getCompilation().compilationEnclosure
    }
}
