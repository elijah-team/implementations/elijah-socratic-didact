package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.output.NG_OutputClass
import tripleo.elijah.stages.garish.GarishClass
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.write_stage.pipeline_impl.WPIS_GenerateOutputs.OutputItems

internal class AmazingClass(private val c: EvaClass,
                            private val itms: OutputItems,
                            aPa: IPipelineAccess?) : Amazing {
    private val mod = c.module()
    private val compilation = mod.compilation as Compilation

    fun mod(): OS_Module {
        return mod
    }

    fun waitGenC(ggc: GenerateC?) {
        val oc = NG_OutputClass()
        oc.setClass(compilation.world().getClass(c).garish as GarishClass, ggc)
        itms.addItem(oc)
    }
}
