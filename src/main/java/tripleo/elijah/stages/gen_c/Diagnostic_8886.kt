package tripleo.elijah.stages.gen_c

import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Locatable
import tripleo.elijah.stages.deduce.post_bytecode.GCFM_Diagnostic
import tripleo.elijah.util2.UnintendedUseException
import java.io.PrintStream

internal class Diagnostic_8886 : GCFM_Diagnostic {
    val _code: Int = 8886

    override fun _message(): String {
        return String.format("%d y is null (No typename specified)", _code)
    }

    override fun code(): String {
        return "" + _code
    }

    override fun primary(): Locatable {
        throw UnintendedUseException("just make it compile")
    }

    override fun report(stream: PrintStream) {
        stream.println(_message())
    }

    override fun secondary(): List<Locatable?> {
        throw UnintendedUseException("just make it compile")
    }

    override fun severity(): Diagnostic.Severity {
        return Diagnostic.Severity.ERROR
    }
}
