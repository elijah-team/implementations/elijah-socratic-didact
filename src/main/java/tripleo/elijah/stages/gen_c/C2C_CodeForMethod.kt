package tripleo.elijah.stages.gen_c

import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.util.Helpers

class C2C_CodeForMethod(private val generateCodeForMethod: Generate_Code_For_Method,
        //	private final GenerateResultEnv        fileGen;
                        private val whyNotGarishFunction: WhyNotGarish_Function,
                        aFileGen: GenerateResultEnv) : C2C_Results {
    //		fileGen               = aFileGen;
    val generateResult: GenerateResult = aFileGen.gr()
    private var _calculated = false
    private var buf: C2C_Result? = null
    private var bufHdr: C2C_Result? = null

    private fun calculate() {
        if (!_calculated) {
            val tos = generateCodeForMethod.tos
            val tosHdr = generateCodeForMethod.tosHdr

            val gmh = Generate_Method_Header(whyNotGarishFunction, generateCodeForMethod._gc(), generateCodeForMethod.LOG)

            tos!!.put_string_ln(String.format("%s {", gmh.header_string))
            tosHdr!!.put_string_ln(String.format("%s;", gmh.header_string))

            generateCodeForMethod.action_invariant(whyNotGarishFunction, gmh)

            tos.flush()
            tos.close()
            generateCodeForMethod.tosHdr.flush()
            generateCodeForMethod.tosHdr.close()
            val buf1 = tos.buffer
            val bufHdr1 = generateCodeForMethod.tosHdr.buffer

            buf = Default_C2C_Result(buf1, TY.IMPL, "C2C_CodeForMethod IMPL", whyNotGarishFunction)
            bufHdr = Default_C2C_Result(bufHdr1, TY.HEADER, "C2C_CodeForMethod HEADER", whyNotGarishFunction)

            _calculated = true
        }
    }

    override val results: List<C2C_Result>
        get() {
            calculate()
            return Helpers.List_of(buf!!, bufHdr!!)
        }
}
