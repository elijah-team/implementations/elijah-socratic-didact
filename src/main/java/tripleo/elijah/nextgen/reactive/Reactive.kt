package tripleo.elijah.nextgen.reactive

import java.util.function.Consumer

/**
 * The base element of **reactive**.
 *
 * Derive from this and add to `CompilationEnclosure#addReactive`
 *
 * Wastefully, [.join] will be called for all
 * [Reactives] times all [ReactiveDimension]s
 */
interface Reactive {
    fun add(aReactivable: Reactivable)

    fun <T> addListener(t: Consumer<T>?)

    fun join(aDimension: ReactiveDimension?)
}
