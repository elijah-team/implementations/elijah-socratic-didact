/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import lombok.Getter
import org.jdeferred2.DoneCallback
import tripleo.elijah.comp.i.ICompilationAccess
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.Provenance
import tripleo.elijah.comp.notation.GN_PL_Run2
import tripleo.elijah.comp.notation.GN_PL_Run2.GenerateFunctionsRequest
import tripleo.elijah.comp.notation.GN_PL_Run2_Env
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.g.GPipelineLogic
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.stages.deduce.DeducePhase
import tripleo.elijah.stages.deduce.DeducePhase.GeneratedClasses
import tripleo.elijah.stages.gen_fn.GenerateFunctions
import tripleo.elijah.stages.gen_fn.GeneratePhase
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.logging.ElLog.Verbosity
import tripleo.elijah.util.CompletableProcess
import tripleo.elijah.util2.Eventual
import tripleo.elijah.util3._AbstractEventualRegister
import tripleo.elijah.world.i.WorldModule
import java.util.function.Consumer

/**
 * Created 12/30/20 2:14 AM
 */
class PipelineLogic(private val pa: IPipelineAccess, ca: ICompilationAccess) : _AbstractEventualRegister(), GPipelineLogic {
    @JvmField
	val dp: DeducePhase
    @JvmField
	val generatePhase: GeneratePhase

    //	private final @NonNull EIT_ModuleList           mods   = new EIT_ModuleList();
    private val mcp = ModuleCompletableProcess()
    val modMap: ModMap = ModMap()

    // 24/01/04 back and forth
    @JvmField
	@Getter
    val verbosity: Verbosity

    init {
        // TODO annotation time, or use clj
        pa.install_notate(Provenance.PipelineLogic__nextModule, GN_PL_Run2::class.java, GN_PL_Run2_Env::class.java)

        ca.setPipelineLogic(this)
        verbosity = ca.testSilence()
        generatePhase = GeneratePhase(verbosity, pa, this)
        dp = DeducePhase(ca, pa, this)

        pa.compilationEnclosure.addModuleListener(PL_ModuleListener(this, pa))
    }

    fun _mcp(): ModuleCompletableProcess {
        return mcp
    }

    fun _pa(): IPipelineAccess {
        return pa
    }

    fun addLog(aLog: ElLog?) {
        _pa().addLog(aLog)
    }

    fun getGenerateFunctions(mod: OS_Module): GenerateFunctions {
        return generatePhase.getGenerateFunctions(mod)
    }

    fun handle(rq: GenerateFunctionsRequest): Eventual<GeneratedClasses> {
        val mod = rq.mod
        @Suppress("UNUSED_VARIABLE")
        val wm = rq.worldModule

        val p = Eventual<GeneratedClasses>()
        p.register(this)
        modMap.put(mod, p)

        return p
    }

    inner class ModMap {
        private val modMap: MutableMap<OS_Module, Eventual<GeneratedClasses>> = HashMap()
        private val mmme: Multimap<OS_Module, DoneCallback<Eventual<GeneratedClasses>>> = ArrayListMultimap
                .create()

        fun put(mod: OS_Module, p: Eventual<GeneratedClasses>) {
            modMap[mod] = p
            val x = mmme[mod]
            for (callback in x) {
                callback.onDone(p)
            }
        }

        fun then(mod: OS_Module, p: DoneCallback<Eventual<GeneratedClasses>>) {
            mmme.put(mod, p)

            val x = modMap[mod]
            if (x != null) {
                p.onDone(x)
            } else {
                val e = Eventual<GeneratedClasses>()
                e.then { lgc: GeneratedClasses? -> p.onDone(e) }
                modMap[mod] = e
            }
        }
    }

    inner class ModuleCompletableProcess : CompletableProcess<WorldModule> {
        override fun add(aWorldModule: WorldModule) {
            // System.err.printf("7070 %s %d%n", mod.getFileName(), mod.entryPoints.size());

            val ce = pa.compilationEnclosure
            val worldConsumer = Consumer { bWorldModule: WorldModule -> ce.noteAccept(bWorldModule) } // FIXME not data...
            val pl_run2 = GN_PL_Run2_Env(this@PipelineLogic, aWorldModule, ce, worldConsumer)

            pa.notate(Provenance.PipelineLogic__nextModule, pl_run2)
        }

        override fun complete() {
            dp.finish()
        }

        override fun error(d: Diagnostic) {
//			throw new UnintendedUseException();
        }

        override fun preComplete() {
        }

        override fun start() {
        }
    }
} //
//
//

