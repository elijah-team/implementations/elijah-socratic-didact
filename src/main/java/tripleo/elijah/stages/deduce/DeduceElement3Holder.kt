package tripleo.elijah.stages.deduce

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3
import tripleo.elijah.stages.gen_fn.IElementHolder

class DeduceElement3Holder @Contract(pure = true) constructor(private val element: IDeduceElement3) : IElementHolder {
    override fun getElement(): OS_Element {
        return element.principal
    }
}
