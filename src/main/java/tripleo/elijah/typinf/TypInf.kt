/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.typinf

import lombok.Data
import lombok.Getter
import tripleo.elijah.util.Helpers
import java.util.*
import java.util.function.Consumer
import java.util.stream.Collectors

//
// Eli Bendersky [http://eli.thegreenplace.net]
// This code is in the public domain.

//import javax.annotation.Nullable;
/**
 * Created 9/3/21 12:40 AM
 */
class TypInf {
    /**
     * Show a type assignment for the given subtree, as a table.
     *
     * @param node the given subtree
     * @return Returns a string that shows the assigmnent.
     */
    fun show_type_assignment(node: AstNode): String {
        val lines: MutableList<String> = LinkedList()

        show_rec(node, lines)
        return Helpers.String_join("\n", lines)
    }

    fun show_rec(node: AstNode, lines: MutableList<String>) {
        lines.add(String.format("%60s %s", node, node.get_type()))
        node.visit_children { node1: AstNode -> show_rec(node1, lines) }
    }

    fun get_expression_type(expr: AstNode, subst: HashMap<String?, Type?>?): Type? {
        return get_expression_type(expr, subst, false)
    }

    class Counter {
        var i: Int = 0

        fun next(): Int {
            val j = i
            i++
            return j
        }
    }

    /**
     * A type equation between two types: left and right.
     *
     *
     * orig_node is the original AST node from which this equation was derived, for
     * debugging.
     */
    class TypeEquation
    /**
     *
     */(val left: Type?, val right: Type?, private val orig_node: AstNode) {
        override fun toString(): String {
            return String.format("%s :: %s [from %s]", left, right, orig_node)
        }

        fun left(): Type? {
            return left
        }

        fun right(): Type? {
            return right
        }

        fun orig_node(): AstNode {
            return orig_node
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as TypeEquation
            return this.left == that.left && (this.right == that.right) && (this.orig_node == that.orig_node)
        }

        override fun hashCode(): Int {
            return Objects.hash(left, right, orig_node)
        }
    }

    /**
     * Function (n-ary) type.
     *
     *
     * Encapsulates a sequence of argument types and a single return type.
     */
    class FuncType
    /**
     *
     */(val argtypes: List<Type?>, val rettype: Type?) : Type {
        override fun toString(): String {
            return if (argtypes.size == 1) {
                String.format("(%s -> %s)", argtypes[0], rettype)
            } else {
                String.format("((%s) -> %s)", Helpers.String_join(", ", argtypes.stream().map { t: Type? -> t.toString() }.collect(Collectors.toList())), rettype)
            }
        }

        //@Override
        fun equals__(aO: Any?): Boolean {
            /*
					def __eq__(self, other):
						return (type(self) == type(other) and
							self.rettype == other.rettype and
									all(self.argtypes[i] == other.argtypes[i]
									for i in range(len(self.argtypes))))

				*/
            if (this === aO) return true
            if (aO == null || javaClass != aO.javaClass) return false
            val funcType = aO as FuncType
            return argtypes == funcType.argtypes && rettype == funcType.rettype
        }

        fun argtypes(): List<Type?> {
            return argtypes
        }

        fun rettype(): Type? {
            return rettype
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as FuncType
            return this.argtypes == that.argtypes && (this.rettype == that.rettype)
        }

        override fun hashCode(): Int {
            return Objects.hash(argtypes, rettype)
        }
    }

    @Data
    class TypeVar(// "back and forth"
            @field:Getter var name: String) : Type {
        override fun toString(): String {
            return name
        }
    }

    class TypingError(aS: String?) : RuntimeException(aS)

    companion object {
        // Global counter to produce unique type names.
        var _typecounter: Counter = Counter()

        /**
         * Unifies variable v with type typ, using subst.
         *
         *
         * Returns updated subst or None on failure.
         *
         * @param v
         * @param typ
         * @param subst
         * @return
         */
        fun unify_variable(v: TypeVar, typ: Type?, subst: HashMap<String?, Type?>): HashMap<String?, Type?>? {
            if (subst.containsKey(v.name)) {
                return unify(subst[v.name], typ, subst)
            } else if ((typ is TypeVar) && subst.containsKey(typ.name)) {
                return unify(v, subst[typ.name], subst)
            } else if (occurs_check(v, typ, subst)) {
                return null
            } else {
                // v is not yet in subst and can't simplify x. Extend subst.
                val name = v.name
                return dict_combine(subst, mapping(name, typ))
            }
        }

        private fun dict_combine(a: HashMap<String?, Type?>, b: HashMap<String?, Type?>): HashMap<String?, Type?> {
            val r = HashMap<String?, Type?>()
            for ((key, value) in a) {
                r[key] = value
            }
            for ((key, value) in b) {
                r[key] = value
            }
            return r
        }

        fun <K, V> mapping(k: K, v: V): HashMap<K, V> {
            val r = HashMap<K, V>()
            r[k] = v
            return r
        }

        /**
         * Unifies all type equations in the sequence eqs.
         *
         *
         * Returns a substitution (most general unifier).
         *
         * @param eqs
         * @return
         */
        fun unify_all_equations(eqs: List<TypeEquation>): HashMap<String?, Type?>? {
            var subst: HashMap<String?, Type?>? = HashMap()
            for (eq in eqs) {
                subst = unify(eq.left, eq.right, subst)
                if (subst == null) {
                    break
                }
            }
            return subst
        }

        /**
         * Finds the type of the expression given a substitution.
         *
         *
         * If rename_types is True, renames all the type vars to be sequential
         * characters starting from 'a', so that 't5 -> t3' will be renamed to
         * 'a -> b'. These names are less cluttery and also facilitate testing.
         *
         *
         * Note: expr should already be annotated with assign_typenames.
         *
         * @param expr
         * @param subst
         * @param rename_types
         * @return
         */
        fun get_expression_type(expr: AstNode, subst: HashMap<String?, Type?>?, rename_types: Boolean): Type? {
            val typ = apply_unifier(expr.get_type(), subst)
            if (rename_types) {
                val namecounter = Counter()
                val namemap = HashMap<String, String>()
                rename_type(typ, namemap, namecounter)
            }
            return typ
        }

        // A symbol table is used to map symbols to types throughout the inference
        // process. Example:
        //
        //  > eight = 8
        //  > nine = 9
        //  > foo a = if a == 0 then eight else nine
        //
        // When inferring the type for 'foo', we already have 'eight' and 'nine' assigned
        // to IntType in the symbol table. Also, inside the definition of 'foo' we have
        // 'a' assigned a TypeVar type (since the type of 'a' is initially unknown).
        // Stages:
        //
        // 1. Visit the AST and assign types to all nodes: known types to constant nodes
        //    and fresh typevars to all other nodes. The types are placed in the _type
        //    attribute of each node.
        // 2. Visit the AST again, this time applying type inference rules to generate
        //    equations between the types. The result is a list of equations, all of
        //    which have to be satisfied.
        // 3. Find the most general unifier (solution) for these equations by using
        //    the classical unification algorithm.
        fun rename_type(typ: Type?, namemap: HashMap<String, String>, namecounter: Counter) {
            if (typ is TypeVar) {
                if (namemap.containsKey(typ.name)) {
                    typ.name = namemap[typ.name]!!
                } else {
                    val name = String.format("a%d", namecounter.next())

                    namemap[typ.name] = name
                    namemap[name] = name
                    typ.name = namemap[typ.name]!!
                }
            } else if (typ is FuncType) {
                rename_type(typ.rettype, namemap, namecounter)
                for (argtyp in typ.argtypes) {
                    rename_type(argtyp, namemap, namecounter)
                }
            }
        }

        /**
         * Creates a fresh typename that will be unique throughout the program.
         *
         * @return
         */
        fun _get_fresh_typename(): String {
            return String.format("t%d", _typecounter.next())
        }

        /**
         * This function is useful for determinism in tests.
         */
        fun reset_type_counter() {
            _typecounter = Counter()
        }

        fun assign_typenames(node: AstNode?) {
            assign_typenames(node, HashMap())
        }

        /**
         * Assign typenames to the given AST subtree and all its children.
         *
         *
         * Symtab is the initial symbol table we can query for identifiers found
         * throughout the subtree. All identifiers in the subtree must be bound either
         * in symtab or in lambdas contained in the subtree.
         *
         *
         * This function doesn't return anything, but it updates the _type property
         * on the AST nodes it visits.
         */
        @Throws(TypingError::class)
        fun assign_typenames(node: AstNode?, symtab: HashMap<String?, Type?>) {
            if (node is Identifier_AST) {
                // Identifier nodes are treated specially, as they have to refer to
                // previously defined identifiers in the symbol table.
                val identifier_name: String = node.name
                if (symtab.containsKey(identifier_name)) {
                    node.set_type(symtab[identifier_name])
                } else {
                    throw TypingError(String.format("unbound name \"%s\"", identifier_name))
                }
            } else if (node is LambdaExpr_AST) {
                node.set_type(TypeVar(_get_fresh_typename()))
                val local_symtab = HashMap<String?, Type?>()
                for (argname in node.argnames) {
                    val typename = _get_fresh_typename()
                    local_symtab[argname] = TypeVar(typename)
                }
                node._arg_types = local_symtab
                assign_typenames(node.expr, dict_combine(symtab, local_symtab))
            } else if ((node is OpExpr_AST)) {
                node.set_type(TypeVar(_get_fresh_typename()))
                node.visit_children(Consumer { c: AstNode? -> assign_typenames(c, symtab) })
            } else if ((node is IfExpr_AST)) {
                node.set_type(TypeVar(_get_fresh_typename()))
                node.visit_children(Consumer { c: AstNode? -> assign_typenames(c, symtab) })
            } else if ((node is AppExpr_AST)) {
                node.set_type(TypeVar(_get_fresh_typename()))
                node.visit_children(Consumer { c: AstNode? -> assign_typenames(c, symtab) })
            } else if ((node is IntConstant_AST)) {
                node.set_type(IntType())
            } else if ((node is BoolConstant_AST)) {
                node.set_type(BoolType())
            } else {
                throw TypingError(String.format("unknown node %s", node!!.javaClass.name))
            }
        }

        /**
         * Generate type equations from node and place them in type_equations.
         *
         *
         * Prior to calling this functions, node and its children already have to
         * be annotated with _type, by a prior call to assign_typenames.
         *
         * @param node
         */
        fun generate_equations(node: AstNode, type_equations: MutableList<TypeEquation?>) {
            if ((node is IntConstant_AST)) {
                type_equations.add(TypeEquation(node.get_type(), IntType(), node))
            } else if ((node is BoolConstant_AST)) {
                type_equations.add(TypeEquation(node.get_type(), BoolType(), node))
            } else if ((node is Identifier_AST)) {
                // Identifier references add no equations.
                //pass
            } else if ((node is OpExpr_AST)) {
                node.visit_children { c: AstNode -> generate_equations(c, type_equations) }
                // All op arguments are integers.
                type_equations.add(TypeEquation(node.left._type, IntType(), node))
                type_equations.add(TypeEquation(node.right._type, IntType(), node))
                // Some ops return boolean, and some return integer.
                if (Helpers.List_of("!=", "==", ">=", "<=", ">", "<").contains(node.op)) {
                    type_equations.add(TypeEquation(node.get_type(), BoolType(), node))
                } else {
                    type_equations.add(TypeEquation(node.get_type(), IntType(), node))
                }
            } else if (node is AppExpr_AST) {
                node.visit_children { c: AstNode -> generate_equations(c, type_equations) }
                val argtypes: List<Type?> = node.args.stream().map { name: AstNode? -> node.get_type() }.collect(Collectors.toList())

                // An application forces its function's type.
                type_equations.add(TypeEquation(node.func._type, FuncType(argtypes, node.get_type()), node))
            } else if (node is IfExpr_AST) {
                node.visit_children { c: AstNode -> generate_equations(c, type_equations) }
                type_equations.add(TypeEquation(node.ifexpr._type, BoolType(), node))
                type_equations.add(TypeEquation(node.get_type(), node.thenexpr._type, node))
                type_equations.add(TypeEquation(node.get_type(), node.elseexpr._type, node))
            } else if (node is LambdaExpr_AST) {
                node.visit_children { c: AstNode -> generate_equations(c, type_equations) }
                val argtypes: List<Type?> = node.argnames.stream().map { name: String? -> node._arg_types!![name] }.collect(Collectors.toList())
                type_equations.add(TypeEquation(node.get_type(), FuncType(argtypes, node.expr._type), node))
            } else {
                throw TypingError(String.format("unknown node %s", node.javaClass.name))
            }
        }

        /**
         * Unify two types typ_x and typ_y, with initial subst.
         *
         *
         * Returns a subst (map of name->Type) that unifies typ_x and typ_y, or None if
         * they can't be unified. Pass subst={} if no subst are initially
         * known. Note that {} means valid (but empty) subst.
         *
         * @param typ_x
         * @param typ_y
         * @param subst
         * @return
         */
        fun unify(typ_x: Type?, typ_y: Type?, subst: HashMap<String?, Type?>?): HashMap<String?, Type?>? {
            var subst = subst
            if (subst == null) {
                return null
            } else if (typ_x == typ_y) {
                return subst
            } else if ((typ_x is TypeVar)) {
                return unify_variable(typ_x, typ_y, subst)
            } else if ((typ_y is TypeVar)) {
                return unify_variable(typ_y, typ_x, subst)
            } else if ((typ_x is FuncType) && (typ_y is FuncType)) {
                if (typ_x.argtypes.size != (typ_y.argtypes.size)) {
                    return null
                } else {
                    subst = unify(typ_x.rettype, typ_y.rettype, subst)
                    for (i in typ_x.argtypes.indices) {
                        subst = unify(typ_x.argtypes[i], typ_y.argtypes[i], subst)
                    }
                    return subst
                }
            } else {
                return null
            }
        }

        /**
         * Does the variable v occur anywhere inside typ?
         *
         *
         * Variables in typ are looked up in subst and the check is applied
         * recursively.
         *
         * @param v
         * @param subst
         * @return
         */
        fun occurs_check(v: TypeVar, typ: Any?, subst: HashMap<String?, Type?>): Boolean {
            if (v == typ) {
                return true
            } else if (typ is TypeVar && subst.containsKey(typ.name)) {
                return occurs_check(v, subst[typ.name], subst)
            } else if (typ is FuncType) {
                if (occurs_check(v, typ.rettype, subst)) return true

                for (arg in typ.argtypes) {
                    if (occurs_check(v, arg, subst)) return true
                }
            } else {
                return false
            }
            return false
        }

        /**
         * Applies the unifier subst to typ.
         *
         *
         * Returns a type where all occurrences of variables bound in subst
         * were replaced (recursively); on failure returns None.
         *
         * @param typ
         * @param subst
         * @return
         */
        fun apply_unifier(typ: Type?, subst: HashMap<String?, Type?>?): Type? {
            if (subst == null) {
                return null
            } else if ((subst.size) == 0) {
                return typ
            } else if (typ is BoolType || typ is IntType) {
                return typ
            } else if (typ is TypeVar) {
                return if (subst.containsKey(typ.name)) apply_unifier(subst[typ.name], subst)
                else typ
            } else if ((typ is FuncType)) {
                val newargtypes = typ.argtypes.stream().map { arg: Type? -> apply_unifier(arg, subst) }.collect(Collectors.toList())
                return FuncType(newargtypes, apply_unifier(typ.rettype, subst))
            } else {
                return null
            }
        }
    }
} //
//
//

