/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.typinf.lexer

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created 9/4/21 5:18 PM
 */
class Regex {
    private var pat: Pattern? = null

    fun search(aBuf: String?, aPos: Int): MatchObject {
        val m = pat!!.matcher(aBuf)
        if (!m.find(aPos));
        return MatchObject(m)
    }

    fun match(aBuf: String?, aPos: Int): MatchObject? {
        val substring = aBuf!!.substring(aPos)
        val m = pat!!.matcher(substring)
        if (!m.lookingAt()) return null
        return MatchObject(m)
    }

    class MatchObject(private val matcher: Matcher) {
        fun start(): Int {
            return matcher.start()
        }

        fun end(): Int {
            return matcher.end()
        }

        val string: String
            get() = matcher.group(0)
    }

    companion object {
        fun compile(aS: String?): Regex {
//			Object x = org.python.modules._sre();
            val r = Regex()
            r.pat = Pattern.compile(aS)
            return r
        }
    }
} //
//
//

