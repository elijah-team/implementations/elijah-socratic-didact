package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.lang.i.VariableStatement

class ZoneVariableStatement(private val variableStatement: VariableStatement?) {
    val containerParent: OS_Element?
        get() = variableStatement!!.parent!!.parent
}
