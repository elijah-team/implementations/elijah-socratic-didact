package tripleo.elijah.stages.write_stage.pipeline_impl

class SPrintStream : XPrintStream {
    private val sb = StringBuilder()

    val string: String
        get() = sb.toString()

    override fun println(aS: String) {
        sb.append(aS)
        sb.append('\n')
    }
}
