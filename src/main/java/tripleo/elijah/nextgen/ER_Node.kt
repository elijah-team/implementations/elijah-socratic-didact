package tripleo.elijah.nextgen

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.nextgen.i.CP_Path
import tripleo.elijah.nextgen.outputstatement.EG_Statement

/**
 * See
 * [CompOutput.writeToPath]
 */
// TODO 09/04 Duplication madness
interface ER_Node {
//    @JvmField
    val path: CP_Path

//    @JvmField
    val statement: EG_Statement

    companion object {
        @Contract(value = "_, _ -> new", pure = true)
        fun of(p: CP_Path, seq: EG_Statement): ER_Node {
            return object : ER_Node {
                override val path: CP_Path
                    get() =//				Path pp = p.getPath();
                        p
                //				throw new NotImplementedException();

                override val statement: EG_Statement
                    get() = seq

                override fun toString(): String {
                    return "17 ER_Node $p"
                }
            }
        }
    }
}
