package tripleo.elijah.comp.process

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.i.*
import tripleo.elijah.comp.i.extra.ICompilationRunner
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.internal.CompilationImpl
import tripleo.elijah.comp.internal.CompilationRunner
import tripleo.elijah.comp.local.CW
import tripleo.elijah.comp.percy.CN_CompilerInputWatcher.e
import tripleo.elijah.nextgen.comp_model.CM_CompilerInput
import tripleo.elijah.util.Maybe
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.nio.file.NotDirectoryException

class CB_FindCIs @Contract(pure = true) constructor(private val compilationRunner: ICompilationRunner, aInputs: List<CompilerInput?>?) : CB_Action {
    //	private final List<CompilerInput> _inputs;
    //		_inputs           = aInputs;
    private val o: CB_Output = (compilationRunner as CompilationRunner).compilationEnclosure.cB_Output

    override fun execute(aMonitor: CB_Monitor) {
        val st = compilationRunner.crState!!
        val c = st.ca().compilation as Compilation
        val errSink = c.errSink


        //		final CK_StepsContext  context   = new CD_CRS_StepsContext(st, o);
        for (input in c.compilationEnclosure.compilerInput) {
            _processInput(c.compilationClosure, errSink, input)
        }

        logProgress_Stating("outputString.size", "" + o.get().size)

        SimplePrintLoggerToRemoveSoon.println_out_3("** CB_FindCIs :: outputString.size :: " + o.get().size)

        for (outputString in o.get()) {
            // 08/13
            SimplePrintLoggerToRemoveSoon.println_out_3("** CB_FindCIs :: outputString :: " + outputString.text)
        }

        // TODO capture action outputs
        //  09/27 is that not being done above??
        aMonitor.reportSuccess(this, o)

        //final CK_AlmostComplete almostComplete = new CK_AlmostComplete();
        //almostComplete.execute(context, monitor);
    }

    @Contract(pure = true)
    override fun name(): String {
        return "FindCIs"
    }

    private fun _processInput(c: CompilationClosure,
                              aErrSink: ErrSink,
                              input: CompilerInput) {
        // FIXME 24/01/09 oop
        val ty = input.ty()
        if (ty != null) {
            when (ty) {
                CompilerInput.Ty.NULL, CompilerInput.Ty.SOURCE_ROOT -> {}
                else -> {
                    return
                }
            }
        }

        val cm = (c.compilation as CompilationImpl)[input] // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
        val f = cm.fileOf()
        val compilationClosure = c.compilation.compilationClosure

        if (input.isEzFile) {
            if (input.isNull) {
                input.certifyRoot()
            }

            CW.CW_inputIsEzFile_(input, compilationClosure)
        } else {
            // aErrSink.reportError("9996 Not an .ez file "+file_name);
            if (f.isDirectory) {
                val compilation = compilationClosure.compilation as CompilationImpl

                // FIXME 24/01/09 Duplication alert??
                compilation.addCompilerInputWatcher { aEvent: e, aCompilerInput: CompilerInput, aObject: Any -> __CN_CompilerInputWatcher__event(aEvent, aCompilerInput, aObject) }
                CW.CW_inputIsDirectory_(input, compilationClosure, f)
            } else {
                val d = NotDirectoryException(f.toString())
                aErrSink.reportError("9995 Not a directory " + f.absolutePath)
            }
        }
    }

    private fun logProgress_Stating(aSection: String, aStatement: String) {
        SimplePrintLoggerToRemoveSoon.println_out_3("** CB_FindCIs :: %s :: %s".formatted(aSection, aStatement))
    }

    companion object {
        private fun __CN_CompilerInputWatcher__event(aEvent: e, aCompilerInput: CompilerInput, aObject: Any) {
            when (aEvent) {
                e.ACCEPT_CI -> {
                    val mci = aObject as Maybe<ILazyCompilerInstructions>
                    aCompilerInput.accept_ci(mci)
                }

                e.IS_EZ -> {
                    val cm = aObject as CM_CompilerInput
                    cm.onIsEz()
                }

                else -> {
                    System.err.println("~~ [11/24 111] $aEvent $aCompilerInput")
                }
            }
        }
    }
}
