package tripleo.elijah.stages.gen_c

import org.jetbrains.annotations.Contract
import tripleo.elijah.util.Helpers

/**
 * (Unrealized) Intent: provide annotations in output code to show what
 * generated text is generated from
 */
class SpecialText(val text: String) {
    companion object {
        @Contract("_ -> new")
        fun compose(aStringList: List<String>?): SpecialText {
            return SpecialText(Helpers.String_join(".", aStringList!!))
        }
    }
}
