package tripleo.elijah.comp;

import kotlin.jvm.functions.Function0;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider;

public class __ {
    @NotNull
    public static EOT_FileNameProvider EOT_FileNameProvider2(@NotNull String x) {
        return new EOT_FileNameProvider() {
            @NotNull
            @Override
            public String getFilename() {
                return x;
            }
        };
    }

    @NotNull
    public static EOT_FileNameProvider EOT_FileNameProvider2(@NotNull Function0<String> x) {
        return new EOT_FileNameProvider() {
            @NotNull
            @Override
            public String getFilename() {
                return x.invoke();
            }
        };
    }
}
