/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import com.google.common.collect.Collections2
import org.jdeferred2.DoneCallback
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.FunctionStatement
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.notation.GN_PL_Run2.GenerateFunctionsRequest
import tripleo.elijah.entrypoints.ArbitraryFunctionEntryPoint
import tripleo.elijah.entrypoints.EntryPoint
import tripleo.elijah.entrypoints.MainClassEntryPoint
import tripleo.elijah.g.GGenerateFunctions
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.IdentExpressionImpl
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.lang.impl.MatchConditionalImpl.MatchConditionalPart3__
import tripleo.elijah.lang.impl.NumericExpressionImpl
import tripleo.elijah.lang.types.OS_BuiltinType
import tripleo.elijah.lang.types.OS_FuncExprType
import tripleo.elijah.lang.types.OS_UnitType
import tripleo.elijah.lang.types.OS_UserType
import tripleo.elijah.lang2.BuiltInTypes
import tripleo.elijah.lang2.SpecialFunctions
import tripleo.elijah.nextgen.reactive.ReactiveDimension
import tripleo.elijah.pre_world.Mirror_ArbitraryFunctionEntryPoint
import tripleo.elijah.pre_world.Mirror_EntryPoint
import tripleo.elijah.pre_world.Mirror_MainClassEntryPoint
import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.RegisterClassInvocation_env
import tripleo.elijah.stages.gen_fn.IdentTableEntry._Reactive_IDTE
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.stages.inter.ModuleThing
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.logging.ElLog_
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.Helpers0
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.util.range.Range
import java.util.function.Consumer

/**
 * Created 9/10/20 2:28 PM
 */
class GenerateFunctions(val module: OS_Module, aPipelineLogic: PipelineLogic,
                        private val pa: IPipelineAccess) : ReactiveDimension, GGenerateFunctions {
    internal class __GenerateClass {
        private val LOG: ElLog
        private val passthruEnv: RegisterClassInvocation_env?

        @Contract(pure = true)
        constructor(aLOG: ElLog) {
            LOG = aLOG
            passthruEnv = null
        }

        constructor(aLOG: ElLog, aPassthruEnv: RegisterClassInvocation_env?) {
            LOG = aLOG
            passthruEnv = aPassthruEnv
        }

        fun processItem(klass: ClassStatement, item: ClassItem, gc: EvaClass) {
            var an: AccessNotation? = null

            if (item is AliasStatement) {
                LOG.info("Skip alias statement for now")
                //				throw new NotImplementedException();
            } else if (item is ClassStatement) {
//				final ClassStatement classStatement = (ClassStatement) item;
//				@NotNull EvaClass gen_c = generateClass(classStatement);
//				gc.addClass(classStatement, gen_c);
            } else if (item is ConstructorDef) {
//				final ConstructorDef constructorDef = (ConstructorDef) item;
//				@NotNull GeneratedConstructor f = generateConstructor(constructorDef, klass, null); // TODO remove this null
//				gc.addConstructor(constructorDef, f);
            } else if (item is DestructorDef) {
                throw NotImplementedException()
            } else if (item is DefFunctionDef) {
//				@NotNull EvaFunction f = generateFunction((DefFunctionDef) item, klass);
//				gc.addFunction((DefFunctionDef) item, f);
            } else if (item is FunctionDef) {
                // README handled in WlGenerateFunction
//				@NotNull EvaFunction f = generateFunction((FunctionDef) item, klass);
//				gc.addFunction((FunctionDef) item, f);
            } else if (item is NamespaceStatement) {
                throw NotImplementedException()
            } else if (item is VariableSequence) {
                for (vs in item.items()) {
//					LOG.info("6999 "+vs);
                    gc.addVarTableEntry(an, vs!!, passthruEnv)
                }
            } else if (item is AccessNotation) {
                //
                // TODO two AccessNotationImpl's can be active at once, for example if the first
                // one defined only classes and the second one defined only a category
                //
                an = item
                //				gc.addAccessNotation(an);
            } else if (item is PropertyStatement) {
                LOG.err("307 Skipping property for now")
            } else {
                LOG.err("305 " + item.javaClass.name)
                throw NotImplementedException()
            }

            gc.createCtor0()

            //			klass._a.setCode(nextClassCode());
        }
    }

    internal inner class Generate_Item {
        fun generate_alias_statement(`as`: AliasStatement?) {
            throw NotImplementedException()
        }

        fun generate_case_conditional(cc: CaseConditional) {
            val y = 2
            LOG.err("Skip CaseConditional for now")
            //			throw new NotImplementedException();
        }

        fun generate_construct_statement(aConstructStatement: ConstructStatement,
                                         gf: BaseEvaFunction, cctx: Context) {
            val left = aConstructStatement.expr // TODO need type of this expr, not expr!!
            val args = aConstructStatement.args
            //
            var expression_num = simplify_expression(left, gf, cctx)
            if (expression_num == null) {
                expression_num = gf.get_assignment_path(left, this@GenerateFunctions, cctx)
            }
            val i = addProcTableEntry(left, expression_num, get_args_types(args, gf, cctx), gf)
            val l: MutableList<InstructionArgument> = ArrayList()
            val procIA = ProcIA(i, gf)
            l.add(procIA)
            val args1 = simplify_args(args, gf, cctx)
            l.addAll(args1)
            val instruction_number = add_i(gf, InstructionName.CONSTRUCT, l, cctx)
        }

        fun generate_if(ifc: IfConditional, gf: BaseEvaFunction) {
            val cctx = ifc.context
            val Boolean_true = Helpers0.string_to_ident("true")
            val label_next = gf.addLabel()
            val label_end = gf.addLabel()
            run {
                val begin0 = add_i(gf, InstructionName.ES, null, cctx)
                val expr = ifc.expr
                val i = simplify_expression(expr, gf, cctx)
                //				LOG.info("711 " + i);
                val const_true = addConstantTableEntry("true", Boolean_true,
                        OS_BuiltinType(BuiltInTypes.Boolean), gf)
                add_i(gf, InstructionName.JNE, Helpers.List_of(i, ConstTableIA(const_true, gf), label_next), cctx)
                val begin_1st = add_i(gf, InstructionName.ES, null, cctx)
                val begin_2nd = add_i(gf, InstructionName.ES, null, cctx)
                for (item: OS_Element in ifc.items) {
                    generate_item(item, gf, cctx)
                }
                add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin_2nd, gf)), cctx)
                if (ifc.parts.size == 0) {
                    gf.place(label_next)
                    add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin_1st, gf)), cctx)
                    //					gf.place(label_end);
                } else {
                    add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_end), cctx)
                    val parts = ifc.parts
                    for (part: IfConditional in parts) {
                        gf.place(label_next)
                        //						label_next = gf.addLabel();
                        if (part.expr != null) {
                            val ii = simplify_expression(part.expr, gf, cctx)
                            LOG.info("712 $ii")
                            add_i(gf, InstructionName.JNE, Helpers.List_of(ii, ConstTableIA(const_true, gf), label_next),
                                    cctx)
                        }
                        val begin_next = add_i(gf, InstructionName.ES, null, cctx)
                        for (partItem: OS_Element in part.items) {
                            LOG.info("709 $part $partItem")
                            generate_item(partItem, gf, cctx)
                        }
                        add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin_next, gf)), cctx)
                        gf.place(label_next)
                    }
                    gf.place(label_end)
                }
                add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin0, gf)), cctx)
            }
        }

        fun generate_loop(loop: Loop, gf: BaseEvaFunction) {
            val cctx = loop.context
            val e2 = add_i(gf, InstructionName.ES, null, cctx)
            when (loop.type) {
                LoopTypes.FROM_TO_TYPE -> generate_loop_FROM_TO_TYPE(loop, gf, cctx)
                LoopTypes.TO_TYPE -> {}
                LoopTypes.EXPR_TYPE -> generate_loop_EXPR_TYPE(loop, gf, cctx)
                LoopTypes.ITER_TYPE -> {}
                LoopTypes.WHILE -> {}
                LoopTypes.DO_WHILE -> {}
            }
            val x2 = add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(e2, gf)), cctx)
            val r = Range(e2, x2)
            gf.addContext(loop.context, r)
        }

        private fun generate_loop_EXPR_TYPE(loop: Loop, gf: BaseEvaFunction, cctx: Context) {
            val loop_iterator = addTempTableEntry(null, gf) // TODO deduce later
            add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(loop_iterator, gf)), cctx)
            val i2 = addConstantTableEntry("", NumericExpressionImpl(0),
                    OS_BuiltinType(BuiltInTypes.SystemInteger), gf)
            val ia1: InstructionArgument = ConstTableIA(i2, gf)
            //			if (ia1 instanceof ConstTableIA)
            add_i(gf, InstructionName.AGNK, Helpers.List_of(IntegerIA(loop_iterator, gf), ia1), cctx)
            //			else
//				add_i(gf, InstructionName.AGN, List_of(new IntegerIA(loop_iterator), ia1), cctx);
            val label_top = gf.addLabel("top", true)
            gf.place(label_top)
            val label_bottom = gf.addLabel("bottom" + label_top.index, false)
            add_i(gf, InstructionName.JE, Helpers.List_of(IntegerIA(loop_iterator, gf),
                    simplify_expression(loop.toPart, gf, cctx), label_bottom), cctx)
            for (statementItem in loop.items) {
                LOG.info("707 $statementItem")
                generate_item(statementItem as OS_Element, gf, cctx)
            }
            val txt = SpecialFunctions.of(ExpressionKind.INCREMENT)
            val pre_inc_name = Helpers0.string_to_ident(txt)
            val tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, pre_inc_name)
            val pre_inc = addProcTableEntry(pre_inc_name, null, Helpers.List_of(tte), gf)
            add_i(gf, InstructionName.CALLS, Helpers.List_of(ProcIA(pre_inc, gf), IntegerIA(loop_iterator, gf)), cctx)
            add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_top), cctx)
            gf.place(label_bottom)
        }

        private fun generate_loop_FROM_TO_TYPE(loop: Loop, gf: BaseEvaFunction,
                                               cctx: Context) {
            val iterNameToken = loop.iterNameToken
            val iterName = iterNameToken.text
            val iter_temp = addTempTableEntry(null, iterNameToken, gf, iterNameToken) // TODO deduce later
            add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(iter_temp, gf)), cctx)
            val ia1 = simplify_expression(loop.fromPart, gf, cctx)
            if (ia1 is ConstTableIA) add_i(gf, InstructionName.AGNK, Helpers.List_of(IntegerIA(iter_temp, gf), ia1), cctx)
            else add_i(gf, InstructionName.AGN, Helpers.List_of(IntegerIA(iter_temp, gf), ia1), cctx)
            val label_top = gf.addLabel("top", true)
            gf.place(label_top)
            val label_bottom = gf.addLabel("bottom" + label_top.index, false)
            add_i(gf, InstructionName.JE, Helpers.List_of(IntegerIA(iter_temp, gf),
                    simplify_expression(loop.toPart, gf, cctx), label_bottom), cctx)
            for (statementItem in loop.items) {
                LOG.info("705 $statementItem")
                generate_item(statementItem as OS_Element, gf, cctx)
            }
            val pre_inc_name = Helpers0.string_to_ident("__preinc__")
            val tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, pre_inc_name)
            val pre_inc = addProcTableEntry(pre_inc_name, null, Helpers.List_of(tte /* getType(left), getType(right) */),
                    gf)
            add_i(gf, InstructionName.CALLS, Helpers.List_of(ProcIA(pre_inc, gf), IntegerIA(iter_temp, gf)), cctx)
            add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_top), cctx)
            gf.place(label_bottom)
        }

        fun generate_match_conditional(mc: MatchConditional, gf: BaseEvaFunction) {
            val y = 2
            val cctx = mc.parent!!.context // TODO MatchConditional.getContext returns NULL!!!
            run {
                val expr = mc.expr
                val i = simplify_expression(expr, gf, cctx)

                //				LOG.info("710 " + i);
                var label_next = gf.addLabel()
                val label_end = gf.addLabel()
                run {
                    for (part: MC1 in mc.parts) {
                        if (part is MatchArm_TypeMatch) {
                            val tn: TypeName = part.typeName
                            val id: IdentExpression = part.ident

                            val begin0 = add_i(gf, InstructionName.ES, null, cctx)

                            val tmp = addTempTableEntry(OS_UserType(tn), id, gf, id) // TODO no context!
                            val vte_tmp = gf.getVarTableEntry(tmp)
                            val t = vte_tmp.typeTableEntry
                            add_i(gf, InstructionName.IS_A,
                                    Helpers.List_of(i, IntegerIA(t.getIndex(), gf),  /* TODO not */LabelIA(label_next)),
                                    cctx)
                            val context = part.context

                            add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(tmp, gf)),
                                    context)
                            val cast_inst = add_i(gf, InstructionName.CAST_TO,
                                    Helpers.List_of(IntegerIA(tmp, gf), IntegerIA(t.getIndex(), gf), (i)), context)
                            vte_tmp.addPotentialType(cast_inst, t) // TODO in the future instructionIndex may be

                            // unsigned
                            for (item: FunctionItem in part.items) {
                                generate_item(item, gf, context)
                            }

                            add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_end), context)
                            add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin0, gf)), cctx)
                            gf.place(label_next)
                            label_next = gf.addLabel()
                        } else if (part is IMatchConditionalPart2) {
                            val id: IExpression = part.matchingExpression

                            val begin0 = add_i(gf, InstructionName.ES, null, cctx)

                            val i2 = simplify_expression(id, gf, cctx)
                            add_i(gf, InstructionName.JNE, Helpers.List_of(i, i2, label_next), cctx)
                            val context = part.context

                            for (item: FunctionItem in part.items) {
                                generate_item(item, gf, context)
                            }

                            add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_end), context)
                            add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin0, gf)), cctx)
                            gf.place(label_next)
                            //							label_next = gf.addLabel();
                        } else if (part is MatchConditionalPart3__) {
                            LOG.err("Don't know what this is")
                        }
                    }
                    gf.place(label_next)
                    add_i(gf, InstructionName.NOP, Helpers.List_of(), cctx)
                    gf.place(label_end)
                }
            }
        }

        fun generate_statement_wrapper(aStatementWrapper: StatementWrapper, x: IExpression,
                                       expressionKind: ExpressionKind, gf: BaseEvaFunction, cctx: Context) {
//			LOG.err("106-1 "+x.getKind()+" "+x);
            if (x.is_simple) {
//				int i = addTempTableEntry(x.getType(), gf);
                when (expressionKind) {
                    ExpressionKind.ASSIGNMENT -> //					LOG.err(String.format("703.2 %s %s", x.getLeft(), ((BasicBinaryExpressionImpl)x).getRight()));
                        generate_item_assignment(aStatementWrapper, x, gf, cctx)

                    ExpressionKind.AUG_MULT -> {
                        LOG.info(String.format("801.1 %s %s %s", expressionKind, x.left,
                                (x as BasicBinaryExpression).right))
                        //						BasicBinaryExpressionImpl bbe = (BasicBinaryExpressionImpl) x;
//						final IExpression right1 = bbe.getRight();
                        val left = simplify_expression(x.getLeft(), gf, cctx)
                        val right = simplify_expression(x.right, gf,
                                cctx)
                        val fn_aug_name = Helpers0
                                .string_to_ident(SpecialFunctions.of(expressionKind))
                        val argument_types = Helpers.List_of(
                                gf.getVarTableEntry(DeduceTypes2.to_int(left)).typeTableEntry, gf.getVarTableEntry(DeduceTypes2.to_int(right)).typeTableEntry)
                        //						LOG.info("801.2 "+argument_types);
                        val fn_aug = addProcTableEntry(fn_aug_name, null, argument_types, gf)
                        val i = add_i(gf, InstructionName.CALLS, Helpers.List_of(ProcIA(fn_aug, gf), left, right), cctx)
                        //
                        // SEE IF CALL SHOULD BE DEFERRED
                        //
                        for (argument_type in argument_types) {
                            if (argument_type.attached == null) {
                                // still dont know the argument types at this point, which creates a problem
                                // for resolving functions, so wait until later when more information is
                                // available
                                if (!gf.deferred_calls.contains(i)) gf.deferred_calls.add(i)
                                break
                            }
                        }
                    }

                    else -> throw NotImplementedException()
                }
            } else {
                when (expressionKind) {
                    ExpressionKind.ASSIGNMENT -> //					LOG.err(String.format("803.2 %s %s", x.getLeft(), ((BasicBinaryExpressionImpl)x).getRight()));
                        generate_item_assignment(aStatementWrapper, x, gf, cctx)

                    ExpressionKind.PROCEDURE_CALL -> {
                        val pce = x as ProcedureCallExpression
                        simplify_procedure_call(pce, gf, cctx)
                    }

                    ExpressionKind.DOT_EXP -> {
                        val de = x as DotExpression
                        generate_item_dot_expression(null, de.left, de.right, gf, cctx)
                    }

                    else -> throw IllegalStateException("Unexpected value: $expressionKind")
                }
            }
        }

        fun generate_variable_sequence(item: VariableSequence, gf: BaseEvaFunction,
                                       cctx: Context) {
            for (vs in item.items()) {
                var state = 0
                //				LOG.info("8004 " + vs);
                val variable_name = vs.name
                val initialValue = vs.initialValue()
                //
                state = if (vs.typeModifiers == TypeModifiers.CONST) {
                    if (initialValue.is_simple) {
                        if (initialValue is IdentExpression) {
                            4
                        } else {
                            1
                        }
                    } else {
                        2
                    }
                } else {
                    3
                }
                when (state) {
                    1 -> {
                        val ci = addConstantTableEntry(variable_name, initialValue, null, gf)
                        val vte_num = addVariableTableEntry(variable_name,
                                gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null, vs.nameToken),
                                gf, vs.nameToken)
                        val iv = initialValue
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("const"), IntegerIA(vte_num, gf)), cctx)
                        add_i(gf, InstructionName.AGNK, Helpers.List_of(IntegerIA(vte_num, gf), ConstTableIA(ci, gf)),
                                cctx)
                    }

                    2 -> {
                        val vte_num = addVariableTableEntry(variable_name, gf.newTypeTableEntry(
                                TypeTableEntry.Type.SPECIFIED, null, vs.nameToken), gf, vs)
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("val"), IntegerIA(vte_num, gf)), cctx)
                        val iv = initialValue
                        assign_variable(gf, vte_num, iv, cctx)
                    }

                    3 -> {
                        val tte: TypeTableEntry
                        tte = if (initialValue === LangGlobals.UNASSIGNED && vs.typeName() != null) {
                            gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, OS_UserType(vs.typeName()),
                                    vs.nameToken)
                        } else {
                            gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null,
                                    vs.nameToken)
                        }
                        val vte_num = addVariableTableEntry(variable_name, tte, gf, vs) // TODO why not
                        // vs.initialValue ??
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("var"), IntegerIA(vte_num, gf)), cctx)
                        val iv = initialValue
                        assign_variable(gf, vte_num, iv, cctx)
                    }

                    4 -> {
                        val vte_num = addVariableTableEntry(variable_name,
                                gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, (null),
                                        vs.nameToken),
                                gf, vs.nameToken)
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("const"), IntegerIA(vte_num, gf)), cctx)
                        assign_variable(gf, vte_num, initialValue, cctx)
                    }

                    else -> throw IllegalStateException()
                }
            }
        }
    }

    internal inner class Generate_item_assignment {
        fun dot(gf: BaseEvaFunction, left: DotExpression, right: IExpression,
                cctx: Context) {
            val simple_left = simplify_expression(left, gf, cctx)
            val simple_right = simplify_expression(right, gf, cctx)

            /*
			 * IExpression left_dot_left_ = left.getLeft(); assert left_dot_left_ instanceof
			 * IdentExpression; IdentExpression left_dot_left = (IdentExpression)
			 * left_dot_left_;
			 * 
			 * final InstructionArgument vte_left = gf.vte_lookup(left_dot_left.getText());
			 * 
			 * IExpression left_dot_right_ = left.getRight(); assert left_dot_right_
			 * instanceof IdentExpression; IdentExpression left_dot_right =
			 * (IdentExpression) left_dot_right_;
			 * 
			 * final int ident_left; final int ident_lright;
			 * 
			 * if (vte_left != null) { ident_lright = gf.addIdentTableEntry(left_dot_right,
			 * cctx);
			 * 
			 * gf.getIdentTableEntry(ident_lright).setBacklink(vte_left); } else {
			 * ident_left = gf.addIdentTableEntry(left_dot_left, cctx); ident_lright =
			 * gf.addIdentTableEntry(left_dot_right, cctx);
			 * 
			 * gf.getIdentTableEntry(ident_lright).setBacklink(new IdentIA(ident_left, gf));
			 * }
			 * 
			 * final int ident_right = gf.addIdentTableEntry(right, cctx); final int inst =
			 * add_i(gf, InstructionName.AGN, List_of(new IdentIA(ident_lright, gf), new
			 * IdentIA(ident_right, gf)), cctx);
			 */
            val inst2 = add_i(gf, InstructionName.AGN, Helpers.List_of(simple_left, simple_right), cctx)

            val y = 2
        }

        fun ident(gf: BaseEvaFunction, left: IdentExpression, right: IdentExpression,
                  cctx: Context) {
            val vte_left = gf.vte_lookup(left.text)
            val ident_left: Int
            val ident_right: Int
            val some_left: InstructionArgument
            if (vte_left == null) {
                ident_left = gf.addIdentTableEntry(left, cctx)
                some_left = IdentIA(ident_left, gf)
            } else some_left = vte_left
            val vte_right = gf.vte_lookup(right.text)
            val inst: Int
            if (vte_right == null) {
                ident_right = gf.addIdentTableEntry(right, cctx)
                inst = add_i(gf, InstructionName.AGN, Helpers.List_of(some_left, IdentIA(ident_right, gf)), cctx)
            } else {
                inst = add_i(gf, InstructionName.AGN, Helpers.List_of(some_left, vte_right), cctx)

                if (vte_left != null) {
                    val vte = gf.getVarTableEntry(DeduceTypes2.to_int(vte_left))
                    // ^^
                    vte.addPotentialType(inst, gf.getVarTableEntry(DeduceTypes2.to_int(vte_right)).typeTableEntry)
                } else if (some_left is IdentIA) {
//					((IdentIA) some_left).getEntry().addPotentialType(inst, unknown_type);
                }
            }
        }

        fun mathematical(gf: BaseEvaFunction, left: IExpression, kind: ExpressionKind?,
                         right1: IExpression, cctx: Context) {
            // TODO doesn't use kind
            val tte = gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null, right1)

            val left_ia = simplify_expression(left, gf, cctx)
            val right_ia = simplify_expression(right1, gf, cctx)

            val instruction_number = add_i(gf, InstructionName.AGN, Helpers.List_of(left_ia, right_ia), cctx)
            val instruction = gf.getInstruction(instruction_number)
            if (left_ia is IntegerIA) {
                // Assuming this points to a variable and not ie a function
                val vte = gf.getVarTableEntry(DeduceTypes2.to_int(left_ia))
                //				vte.type = tte;
                vte.addPotentialType(instruction!!.index, tte)
            } else if (left_ia is IdentIA) {
                val idte = gf.getIdentTableEntry(DeduceTypes2.to_int(left_ia))
                //				idte.type = tte;
                idte.addPotentialType(instruction!!.index, tte)
            }
        }

        fun neg(gf: BaseEvaFunction, left: IExpression, aKind: ExpressionKind?,
                right1: IExpression, cctx: Context) {
            // TODO doesn't use kind
            val tte = gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null, right1)

            val left_ia = simplify_expression(left, gf, cctx)
            val right_ia = simplify_expression(right1, gf, cctx)

            val instruction_number = add_i(gf, InstructionName.AGN, Helpers.List_of(left_ia, right_ia), cctx)
            val instruction = gf.getInstruction(instruction_number)
            if (left_ia is IntegerIA) {
                // Assuming this points to a variable and not ie a function
                val vte = gf.getVarTableEntry(DeduceTypes2.to_int(left_ia))
                //				vte.type = tte;
                vte.addPotentialType(instruction!!.index, tte)
            } else if (left_ia is IdentIA) {
                val idte = gf.getIdentTableEntry(DeduceTypes2.to_int(left_ia))
                //				idte.type = tte;
                idte.addPotentialType(instruction!!.index, tte)
            }
        }

        fun numeric(gf: BaseEvaFunction, left: IExpression, ne: NumericExpression,
                    cctx: Context) {
            val agn_path = gf.get_assignment_path(left, this@GenerateFunctions, cctx)
            val cte = addConstantTableEntry("", ne, null, gf)

            val agn_inst = add_i(gf, InstructionName.AGN, Helpers.List_of(agn_path, ConstTableIA(cte, gf)), cctx)
            // TODO what now??
        }

        fun procedure_call(aStatementWrapper: StatementWrapper?, gf: BaseEvaFunction,
                           bbe: BasicBinaryExpression, pce: ProcedureCallExpression, cctx: Context) {
            /*
			 * final IExpression left = bbe.getLeft();
			 * 
			 * final InstructionArgument lookup = simplify_expression(left, gf, cctx);
			 * final @NotNull TypeTableEntry tte =
			 * gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, bbe.getType(), left);
			 * 
			 * final Instruction expression_to_call = expression_to_call(pce, gf, cctx);
			 * final List<InstructionArgument> list_of_fn_call = List_of(lookup, new
			 * FnCallArgs(expression_to_call, gf));
			 */

            GIA__procedure_call__one(bbe, gf, cctx, this, pce, this@GenerateFunctions).action(aStatementWrapper)

            /*
			 * if (lookup instanceof IntegerIA) { // TODO should be AGNC final int
			 * instruction_number = add_i(gf, InstructionName.AGN, list_of_fn_call, cctx);
			 * final Instruction instruction = gf.getInstruction(instruction_number);
			 * final @NotNull VariableTableEntry vte = gf.getVarTableEntry(((IntegerIA)
			 * lookup).getIndex()); vte.addPotentialType(instruction.getIndex(), tte); }
			 * else { if (left instanceof IdentExpression) { final IdentExpression
			 * identExpression = (IdentExpression) left; final String text =
			 * identExpression.getText(); final int vte_num; if (aStatementWrapper
			 * instanceof WrappedStatementWrapper) { vte_num = addVariableTableEntry(text,
			 * tte, gf, ((WrappedStatementWrapper)
			 * aStatementWrapper).getVariableStatement()); } else { vte_num =
			 * addVariableTableEntry(text, tte, gf, identExpression); } add_i(gf,
			 * InstructionName.DECL, List_of(new SymbolIA("tmp"), new IntegerIA(vte_num,
			 * gf)), cctx); // TODO should be AGNC final List<InstructionArgument>
			 * list_of_fn_call2 = List_of(new IntegerIA(vte_num, gf), new
			 * FnCallArgs(expression_to_call, gf)); final int instruction_number = add_i(gf,
			 * InstructionName.AGN, list_of_fn_call2, cctx); final Instruction instruction =
			 * gf.getInstruction(instruction_number); final @NotNull VariableTableEntry vte
			 * = gf.getVarTableEntry(vte_num); vte.addPotentialType(instruction.getIndex(),
			 * tte); } else { assert lookup instanceof IdentIA;
			 * 
			 * // TODO should be AGNC final int instruction_number = add_i(gf,
			 * InstructionName.AGN, list_of_fn_call, cctx); final Instruction instruction =
			 * gf.getInstruction(instruction_number);
			 * 
			 * @NotNull IdentTableEntry ite = ((IdentIA) lookup).getEntry();
			 * ite.addPotentialType(instruction.getIndex(), tte); } }
			 */
        }

        fun string_literal(gf: BaseEvaFunction, left: IExpression, right: StringExpression,
                           aContext: Context) {
            val agn_path = gf.get_assignment_path(left, this@GenerateFunctions, aContext)
            val cte = addConstantTableEntry("", right,
                    OS_BuiltinType(BuiltInTypes.String_),  /* right.getType() */gf)

            val agn_inst = add_i(gf, InstructionName.AGN, Helpers.List_of(agn_path, ConstTableIA(cte, gf)), aContext)
            // TODO what now??
        }
    }

    internal inner class Generate_Item11 {
        fun generate_alias_statement(`as`: AliasStatement?) {
            throw NotImplementedException()
        }

        private fun generate_case_conditional(cc: CaseConditional) {
            val y = 2
            LOG.err("Skip CaseConditional for now")
            //			throw new NotImplementedException();
        }

        fun generate_construct_statement(aConstructStatement: ConstructStatement,
                                         gf: BaseEvaFunction, cctx: Context) {
            val left = aConstructStatement.expr // TODO need type of this expr, not expr!!
            val args = aConstructStatement.args
            //
            var expression_num = simplify_expression(left, gf, cctx)
            if (expression_num == null) {
                expression_num = gf.get_assignment_path(left, this@GenerateFunctions, cctx)
            }
            val i = addProcTableEntry(left, expression_num, get_args_types(args, gf, cctx), gf)
            val l: MutableList<InstructionArgument> = ArrayList()
            val procIA = ProcIA(i, gf)
            l.add(procIA)
            val args1 = simplify_args(args, gf, cctx)
            l.addAll(args1)
            val instruction_number = add_i(gf, InstructionName.CONSTRUCT, l, cctx)
        }

        private fun generate_if(ifc: IfConditional, gf: EvaFunction) {
            val cctx = ifc.context
            val Boolean_true = Helpers0.string_to_ident("true")
            val label_next = gf.addLabel()
            val label_end = gf.addLabel()
            run {
                val begin0 = add_i(gf, InstructionName.ES, null, cctx)
                val expr = ifc.expr
                val i = simplify_expression(expr, gf, cctx)
                //				LOG.info("711 " + i);
                val const_true = addConstantTableEntry("true", Boolean_true,
                        OS_BuiltinType(BuiltInTypes.Boolean), gf)
                add_i(gf, InstructionName.JNE, Helpers.List_of(i, ConstTableIA(const_true, gf), label_next), cctx)
                val begin_1st = add_i(gf, InstructionName.ES, null, cctx)
                val begin_2nd = add_i(gf, InstructionName.ES, null, cctx)
                for (item: OS_Element in ifc.items) {
                    generate_item(item, gf, cctx)
                }
                add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin_2nd, gf)), cctx)
                if (ifc.parts.size == 0) {
                    gf.place(label_next)
                    add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin_1st, gf)), cctx)
                    //					gf.place(label_end);
                } else {
                    add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_end), cctx)
                    val parts = ifc.parts
                    for (part: IfConditional in parts) {
                        gf.place(label_next)
                        //						label_next = gf.addLabel();
                        if (part.expr != null) {
                            val ii = simplify_expression(part.expr, gf, cctx)
                            LOG.info("712 $ii")
                            add_i(gf, InstructionName.JNE, Helpers.List_of(ii, ConstTableIA(const_true, gf), label_next),
                                    cctx)
                        }
                        val begin_next = add_i(gf, InstructionName.ES, null, cctx)
                        for (partItem: OS_Element in part.items) {
                            LOG.info("709 $part $partItem")
                            generate_item(partItem, gf, cctx)
                        }
                        add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin_next, gf)), cctx)
                        gf.place(label_next)
                    }
                    gf.place(label_end)
                }
                add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin0, gf)), cctx)
            }
        }

        private fun generate_loop(loop: Loop, gf: EvaFunction) {
            val cctx = loop.context
            val e2 = add_i(gf, InstructionName.ES, null, cctx)
            when (loop.type) {
                LoopTypes.FROM_TO_TYPE -> generate_loop_FROM_TO_TYPE(loop, gf, cctx)
                LoopTypes.TO_TYPE -> {}
                LoopTypes.EXPR_TYPE -> generate_loop_EXPR_TYPE(loop, gf, cctx)
                LoopTypes.ITER_TYPE -> {}
                LoopTypes.WHILE -> {}
                LoopTypes.DO_WHILE -> {}
            }
            val x2 = add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(e2, gf)), cctx)
            val r = Range(e2, x2)
            gf.addContext(loop.context, r)
        }

        private fun generate_loop_EXPR_TYPE(loop: Loop, gf: EvaFunction, cctx: Context) {
            val loop_iterator = addTempTableEntry(null, gf) // TODO deduce later
            add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(loop_iterator, gf)), cctx)
            val i2 = addConstantTableEntry("", NumericExpressionImpl(0),
                    OS_BuiltinType(BuiltInTypes.SystemInteger), gf)
            val ia1: InstructionArgument = ConstTableIA(i2, gf)
            //			if (ia1 instanceof ConstTableIA)
            add_i(gf, InstructionName.AGNK, Helpers.List_of(IntegerIA(loop_iterator, gf), ia1), cctx)
            //			else
//				add_i(gf, InstructionName.AGN, List_of(new IntegerIA(loop_iterator), ia1), cctx);
            val label_top = gf.addLabel("top", true)
            gf.place(label_top)
            val label_bottom = gf.addLabel("bottom" + label_top.index, false)
            add_i(gf, InstructionName.JE, Helpers.List_of(IntegerIA(loop_iterator, gf),
                    simplify_expression(loop.toPart, gf, cctx), label_bottom), cctx)
            for (statementItem in loop.items) {
                LOG.info("707 $statementItem")
                generate_item(statementItem as OS_Element, gf, cctx)
            }
            val txt = SpecialFunctions.of(ExpressionKind.INCREMENT)
            val pre_inc_name = Helpers0.string_to_ident(txt)
            val tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, pre_inc_name)
            val pre_inc = addProcTableEntry(pre_inc_name, null, Helpers.List_of(tte), gf)
            add_i(gf, InstructionName.CALLS, Helpers.List_of(ProcIA(pre_inc, gf), IntegerIA(loop_iterator, gf)), cctx)
            add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_top), cctx)
            gf.place(label_bottom)
        }

        private fun generate_loop_FROM_TO_TYPE(loop: Loop, gf: EvaFunction, cctx: Context) {
            val iterNameToken = loop.iterNameToken
            val iterName = iterNameToken.text
            val iter_temp = addTempTableEntry(null, iterNameToken, gf, iterNameToken) // TODO deduce later
            add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(iter_temp, gf)), cctx)
            val ia1 = simplify_expression(loop.fromPart, gf, cctx)
            if (ia1 is ConstTableIA) add_i(gf, InstructionName.AGNK, Helpers.List_of(IntegerIA(iter_temp, gf), ia1), cctx)
            else add_i(gf, InstructionName.AGN, Helpers.List_of(IntegerIA(iter_temp, gf), ia1), cctx)
            val label_top = gf.addLabel("top", true)
            gf.place(label_top)
            val label_bottom = gf.addLabel("bottom" + label_top.index, false)
            add_i(gf, InstructionName.JE, Helpers.List_of(IntegerIA(iter_temp, gf),
                    simplify_expression(loop.toPart, gf, cctx), label_bottom), cctx)
            for (statementItem in loop.items) {
                LOG.info("705 $statementItem")
                generate_item(statementItem as OS_Element, gf, cctx)
            }
            val pre_inc_name = Helpers0.string_to_ident("__preinc__")
            val tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, pre_inc_name)
            val pre_inc = addProcTableEntry(pre_inc_name, null, Helpers.List_of(tte /* getType(left), getType(right) */),
                    gf)
            add_i(gf, InstructionName.CALLS, Helpers.List_of(ProcIA(pre_inc, gf), IntegerIA(iter_temp, gf)), cctx)
            add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_top), cctx)
            gf.place(label_bottom)
        }

        private fun generate_match_conditional(mc: MatchConditional, gf: EvaFunction) {
            val y = 2
            val cctx = mc.parent!!.context // TODO MatchConditional.getContext returns NULL!!!
            run {
                val expr = mc.expr
                val i = simplify_expression(expr, gf, cctx)

                //				LOG.info("710 " + i);
                var label_next = gf.addLabel()
                val label_end = gf.addLabel()
                run {
                    for (part: MC1 in mc.parts) {
                        if (part is MatchArm_TypeMatch) {
                            val tn: TypeName = part.typeName
                            val id: IdentExpression = part.ident

                            val begin0 = add_i(gf, InstructionName.ES, null, cctx)

                            val tmp = addTempTableEntry(OS_UserType(tn), id, gf, id) // TODO no context!
                            val vte_tmp = gf.getVarTableEntry(tmp)
                            val t = vte_tmp.typeTableEntry
                            add_i(gf, InstructionName.IS_A,
                                    Helpers.List_of(i, IntegerIA(t.getIndex(), gf),  /* TODO not */LabelIA(label_next)),
                                    cctx)
                            val context = part.context

                            add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(tmp, gf)),
                                    context)
                            val cast_inst = add_i(gf, InstructionName.CAST_TO,
                                    Helpers.List_of(IntegerIA(tmp, gf), IntegerIA(t.getIndex(), gf), (i)), context)
                            vte_tmp.addPotentialType(cast_inst, t) // TODO in the future instructionIndex may be

                            // unsigned
                            for (item: FunctionItem in part.items) {
                                generate_item(item, gf, context)
                            }

                            add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_end), context)
                            add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin0, gf)), cctx)
                            gf.place(label_next)
                            label_next = gf.addLabel()
                        } else if (part is IMatchConditionalPart2) {
                            val id: IExpression = part.matchingExpression

                            val begin0 = add_i(gf, InstructionName.ES, null, cctx)

                            val i2 = simplify_expression(id, gf, cctx)
                            add_i(gf, InstructionName.JNE, Helpers.List_of(i, i2, label_next), cctx)
                            val context = part.context

                            for (item: FunctionItem in part.items) {
                                generate_item(item, gf, context)
                            }

                            add_i(gf, InstructionName.JMP, Helpers.List_of<InstructionArgument>(label_end), context)
                            add_i(gf, InstructionName.XS, Helpers.List_of<InstructionArgument>(IntegerIA(begin0, gf)), cctx)
                            gf.place(label_next)
                            //							label_next = gf.addLabel();
                        } else if (part is MatchConditionalPart3__) {
                            LOG.err("Don't know what this is")
                        }
                    }
                    gf.place(label_next)
                    add_i(gf, InstructionName.NOP, Helpers.List_of(), cctx)
                    gf.place(label_end)
                }
            }
        }

        private fun generate_statement_wrapper(aStatementWrapper: StatementWrapper, x: IExpression,
                                               expressionKind: ExpressionKind, gf: BaseEvaFunction, cctx: Context) {
//			LOG.err("106-1 "+x.getKind()+" "+x);
            if (x.is_simple) {
//				int i = addTempTableEntry(x.getType(), gf);
                when (expressionKind) {
                    ExpressionKind.ASSIGNMENT -> //					LOG.err(String.format("703.2 %s %s", x.getLeft(), ((BasicBinaryExpressionImpl)x).getRight()));
                        generate_item_assignment(aStatementWrapper, x, gf, cctx)

                    ExpressionKind.AUG_MULT -> {
                        LOG.info(String.format("801.1 %s %s %s", expressionKind, x.left,
                                (x as BasicBinaryExpression).right))
                        //						BasicBinaryExpressionImpl bbe = (BasicBinaryExpressionImpl) x;
//						final IExpression right1 = bbe.getRight();
                        val left = simplify_expression(x.getLeft(), gf, cctx)
                        val right = simplify_expression(x.right, gf,
                                cctx)
                        val fn_aug_name = Helpers0
                                .string_to_ident(SpecialFunctions.of(expressionKind))
                        val argument_types = Helpers.List_of(
                                gf.getVarTableEntry(DeduceTypes2.to_int(left)).typeTableEntry, gf.getVarTableEntry(DeduceTypes2.to_int(right)).typeTableEntry)
                        //						LOG.info("801.2 "+argument_types);
                        val fn_aug = addProcTableEntry(fn_aug_name, null, argument_types, gf)
                        val i = add_i(gf, InstructionName.CALLS, Helpers.List_of(ProcIA(fn_aug, gf), left, right), cctx)
                        //
                        // SEE IF CALL SHOULD BE DEFERRED
                        //
                        for (argument_type in argument_types) {
                            if (argument_type.attached == null) {
                                // still dont know the argument types at this point, which creates a problem
                                // for resolving functions, so wait until later when more information is
                                // available
                                if (!gf.deferred_calls.contains(i)) gf.deferred_calls.add(i)
                                break
                            }
                        }
                    }

                    else -> throw NotImplementedException()
                }
            } else {
                when (expressionKind) {
                    ExpressionKind.ASSIGNMENT -> //					LOG.err(String.format("803.2 %s %s", x.getLeft(), ((BasicBinaryExpressionImpl)x).getRight()));
                        generate_item_assignment(aStatementWrapper, x, gf, cctx)

                    ExpressionKind.PROCEDURE_CALL -> {
                        val pce = x as ProcedureCallExpression
                        simplify_procedure_call(pce, gf, cctx)
                    }

                    ExpressionKind.DOT_EXP -> {
                        val de = x as DotExpression
                        generate_item_dot_expression(null, de.left, de.right, gf, cctx)
                    }

                    else -> throw IllegalStateException("Unexpected value: $expressionKind")
                }
            }
        }

        private fun generate_variable_sequence(item: VariableSequence, gf: EvaFunction,
                                               cctx: Context) {
            for (vs in item.items()) {
                var state = 0
                //				LOG.info("8004 " + vs);
                val variable_name = vs.name
                val initialValue = vs.initialValue()
                //
                state = if (vs.typeModifiers == TypeModifiers.CONST) {
                    if (initialValue.is_simple) {
                        if (initialValue is IdentExpression) {
                            4
                        } else {
                            1
                        }
                    } else {
                        2
                    }
                } else {
                    3
                }
                when (state) {
                    1 -> {
                        val ci = addConstantTableEntry(variable_name, initialValue, null, gf)
                        val vte_num = addVariableTableEntry(variable_name,
                                gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null, vs.nameToken),
                                gf, vs.nameToken)
                        val iv = initialValue
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("const"), IntegerIA(vte_num, gf)), cctx)
                        add_i(gf, InstructionName.AGNK, Helpers.List_of(IntegerIA(vte_num, gf), ConstTableIA(ci, gf)),
                                cctx)
                    }

                    2 -> {
                        val vte_num = addVariableTableEntry(variable_name,
                                gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null, vs.nameToken),
                                gf, vs.nameToken)
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("val"), IntegerIA(vte_num, gf)), cctx)
                        val iv = initialValue
                        assign_variable(gf, vte_num, iv, cctx)
                    }

                    3 -> {
                        val tte: TypeTableEntry
                        tte = if (initialValue === LangGlobals.UNASSIGNED && vs.typeName() != null) {
                            gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, OS_UserType(vs.typeName()),
                                    vs.nameToken)
                        } else {
                            gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null, vs.nameToken)
                        }
                        val vte_num = addVariableTableEntry(variable_name, tte, gf, vs) // TODO why not
                        // vs.initialValue ??
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("var"), IntegerIA(vte_num, gf)), cctx)
                        val iv = initialValue
                        assign_variable(gf, vte_num, iv, cctx)
                    }

                    4 -> {
                        val vte_num = addVariableTableEntry(variable_name,
                                gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null, vs.nameToken), gf, vs.nameToken)
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("const"), IntegerIA(vte_num, gf)), cctx)
                        assign_variable(gf, vte_num, initialValue, cctx)
                    }

                    else -> throw IllegalStateException()
                }
            }
        }
    }

    internal class GIA__procedure_call__one(bbe: BasicBinaryExpression,
                                            private val gf: BaseEvaFunction,
                                            private val cctx: Context,
                                            private val generate_item_assign: Generate_item_assignment,
                                            pce: ProcedureCallExpression,
                                            private val gfs: GenerateFunctions) {
        val expression_to_call: Instruction
        val left: IExpression = bbe.left
        val list_of_fn_call: List<InstructionArgument>
        val lookup: InstructionArgument
        val tte: TypeTableEntry

        init {
            lookup = gfs.simplify_expression(left, gf, cctx)
            tte = gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null, left)

            expression_to_call = gfs.expression_to_call(pce, gf, cctx)
            list_of_fn_call = Helpers.List_of(lookup, FnCallArgs(expression_to_call, gf))
        }

        fun action(aStatementWrapper: StatementWrapper?) {
            if (lookup is IntegerIA) {
                val index: Int = lookup.index
                action__IntegerIA(list_of_fn_call, index)
            } else {
                if (left is IdentExpression) {
                    action__IdentExpression(aStatementWrapper)
                } else {
                    action__IdentIA()
                }
            }
        }

        private fun action__IdentExpression(aStatementWrapper: StatementWrapper?) {
            val identExpression = left as IdentExpression
            val text = identExpression.text
            val vte_num: Int
            vte_num = if (aStatementWrapper is WrappedStatementWrapper) {
                gfs.addVariableTableEntry(text, tte, gf,
                        aStatementWrapper.variableStatement)
            } else {
                addVariableTableEntry(text, tte, gf, identExpression)
            }
            add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(vte_num, gf)), cctx)
            // TODO should be AGNC
            val list_of_fn_call2 = Helpers.List_of(IntegerIA(vte_num, gf),
                    FnCallArgs(expression_to_call, gf))
            val instruction_number = add_i(gf, InstructionName.AGN, list_of_fn_call2, cctx)
            val instruction = gf.getInstruction(instruction_number)
            val vte = gf.getVarTableEntry(vte_num)
            vte.addPotentialType(instruction!!.index, tte)
        }

        private fun action__IdentIA() {
            assert(lookup is IdentIA)
            // TODO should be AGNC
            val instruction_number = add_i(gf, InstructionName.AGN, list_of_fn_call, cctx)
            val instruction = gf.getInstruction(instruction_number)
            val ite = (lookup as IdentIA).entry
            ite.addPotentialType(instruction!!.index, tte)
        }

        private fun action__IntegerIA(list_of_fn_call: List<InstructionArgument>, lookup: Int) {
            // TODO should be AGNC
            val instruction_number = add_i(gf, InstructionName.AGN, list_of_fn_call, cctx)
            val instruction = gf.getInstruction(instruction_number)
            val vte = gf.getVarTableEntry(lookup)
            vte.addPotentialType(instruction!!.index, tte)
        }

        //
        fun add_i(aGf: BaseEvaFunction, aAgn: InstructionName,
                  aList_of_fn_call: List<InstructionArgument>?, aCctx: Context): Int {
            return gfs.add_i(aGf, aAgn, aList_of_fn_call, aCctx)
        }

        fun addVariableTableEntry(aText: String, aTte: TypeTableEntry,
                                  aGf: BaseEvaFunction, aIdentExpression: IdentExpression): Int {
            return gfs.addVariableTableEntry(aText, aTte, aGf, aIdentExpression)
        }
    }

    val phase: GeneratePhase = aPipelineLogic.generatePhase

    private val LOG: ElLog

    init {
        //
        LOG = ElLog_(module.fileName, phase.verbosity, PHASE)
        pa.addLog(LOG)
    }

    private fun add_i(gf: BaseEvaFunction, x: InstructionName,
                      list_of: List<InstructionArgument>?, ctx: Context): Int {
        val i = gf.add(x, list_of!!, ctx)
        return i
    }

    /**
     * Add a Constant Table Entry of type with Type Table Entry type
     * [TypeTableEntry.Type.SPECIFIED]
     *
     * @param name
     * @param initialValue
     * @param type
     * @param gf
     * @return the cte table index
     */
    private fun addConstantTableEntry(name: String?, initialValue: IExpression, type: OS_Type?,
                                      gf: BaseEvaFunction): Int {
        val tte = gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, type!!, initialValue)
        val cte = ConstantTableEntry(gf.cte_list.size, name, initialValue, tte)
        gf.cte_list.add(cte)
        return cte.index
    }

    /**
     * Add a Constant Table Entry of type with Type Table Entry type
     * [TypeTableEntry.Type.TRANSIENT]
     *
     * @param name
     * @param initialValue
     * @param type
     * @param gf
     * @return the cte table index
     */
    private fun addConstantTableEntry2(name: String?,
                                       initialValue: IExpression,
                                       type: OS_Type?,
                                       gf: BaseEvaFunction): Int {
        val tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, type!!, initialValue)
        val cte = ConstantTableEntry(gf.cte_list.size, name, initialValue, tte)

        /*
		 * gf.b(new BuildaBear() {
		 * 
		 * @Override public void baby(final BuildaBearJavaThing result) { assert type ==
		 * null;
		 * 
		 * result.const_table_entry(name, initialValue, type); } });
		 */
        gf.cte_list.add(cte)
        return cte.index
    }

    fun addProcTableEntry(expression: IExpression?,
                          expression_num: InstructionArgument?,
                          args: List<TypeTableEntry>?,
                          gf: BaseEvaFunction): Int {
        val pte = ProcTableEntry(gf.prte_list.size, expression, expression_num!!, args!!)
        gf.prte_list.add(pte)
        if (expression_num is IdentIA) {
            if (expression_num.entry.callablePTE == null) expression_num.entry.callablePTE = pte
        }
        return pte.index
    }

    private fun addTempTableEntry(type: OS_Type?, gf: BaseEvaFunction): Int {
        return addTempTableEntry(type, null, gf, null)
    }

    private fun addTempTableEntry(type: OS_Type?,
                                  name: IdentExpression?,
                                  gf: BaseEvaFunction,
                                  el: OS_Element?): Int {
        val theName: String?
        val num: Int
        val tte: TypeTableEntry

        if (name != null) {
            tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, type!!, name)
            theName = name.text
            // README Don't set tempNum because we have a name
            num = -1
        } else {
            tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, type!!)
            theName = null
            num = gf.nextTemp()
        }
        val vte_index = gf.addVariableTableEntry(theName!!, VariableTableType.TEMP, tte, el!!)
        val vte = gf.getVarTableEntry(vte_index)
        vte.tempNum = num
        gf.vte_list.add(vte)
        return vte.index
    }

    private fun addVariableTableEntry(name: String, type: TypeTableEntry, gf: BaseEvaFunction,
                                      el: OS_Element): Int {
        return gf.addVariableTableEntry(name, VariableTableType.VAR, type, el)
    }

    private fun assign_variable(gf: BaseEvaFunction, vte: Int, value: IExpression,
                                cctx: Context) {
        if (value === LangGlobals.UNASSIGNED) return  // default_expression

        when (value.kind) {
            ExpressionKind.PROCEDURE_CALL -> {
                val pce = value as ProcedureCallExpression
                val fnCallArgs = FnCallArgs(expression_to_call(pce, gf, cctx), gf)
                val ii2 = add_i(gf, InstructionName.AGN, Helpers.List_of(IntegerIA(vte, gf), fnCallArgs), cctx)
                val vte_proccall = gf.getVarTableEntry(vte)
                val gg = fnCallArgs.expression.getArg(0)
                val g: TableEntryIV
                g = if (gg is IntegerIA) {
                    gf.getVarTableEntry(gg.index)
                } else if (gg is IdentIA) {
                    gf.getIdentTableEntry(gg.index)
                } else if (gg is ProcIA) {
                    gf.getProcTableEntry(gg.index())
                } else throw NotImplementedException()
                val tte_proccall = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null,
                        value, g)
                fnCallArgs.setType(tte_proccall)
                vte_proccall.addPotentialType(ii2, tte_proccall)
            }

            ExpressionKind.NUMERIC -> {
                val ci = addConstantTableEntry(null, value, null, gf)
                val ii = add_i(gf, InstructionName.AGNK, Helpers.List_of(IntegerIA(vte, gf), ConstTableIA(ci, gf)),
                        cctx)
                val vte_numeric = gf.getVarTableEntry(vte)
                vte_numeric.addPotentialType(ii, gf.getConstTableEntry(ci).type)
            }

            ExpressionKind.IDENT -> {
                val ia1 = simplify_expression(value, gf, cctx)
                val ii3 = add_i(gf, InstructionName.AGN, Helpers.List_of(IntegerIA(vte, gf), ia1), cctx)
                val vte3_ident = gf.getVarTableEntry(vte)
                val tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, value)
                vte3_ident.addPotentialType(ii3, tte)
            }

            ExpressionKind.FUNC_EXPR -> {
                val fe = value as FuncExpr
                val pte_index = addProcTableEntry(fe, null, get_args_types(fe.args, gf), gf)
                val ii4 = add_i(gf, InstructionName.AGNF,
                        Helpers.List_of<InstructionArgument>(IntegerIA(vte, gf), IntegerIA(pte_index, gf)), cctx)
                val vte3_func = gf.getVarTableEntry(vte)
                val tte_func = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT,
                        OS_FuncExprType(fe), value)
                vte3_func.addPotentialType(ii4, tte_func)
            }

            else -> throw IllegalStateException("Unexpected value: " + value.kind)
        }
    }

    private fun expression_to_call(pce: ProcedureCallExpression,
                                   gf: BaseEvaFunction, cctx: Context): Instruction {
        val left = pce.left
        when (left.kind) {
            ExpressionKind.IDENT -> return expression_to_call_add_entry(gf, pce, left, cctx)
            ExpressionKind.QIDENT -> {
                val xx = Helpers0.qualidentToDotExpression2((left as Qualident))
                /*
			 * IExpression xx = pce.getLeft();
			 */
//			simplify_qident((Qualident) pce.getLeft(), gf); // TODO ??
                return expression_to_call_add_entry(gf, pce, xx!!, cctx)
            }

            ExpressionKind.DOT_EXP -> {
                val x = simplify_dot_expression(left as DotExpression, gf, cctx) // TODO ??
                return expression_to_call_add_entry(gf, pce, left, x, cctx)
            }

            ExpressionKind.PROCEDURE_CALL -> {
                val pce1 = left as ProcedureCallExpression
                if (true) {
                    val x1 = simplify_expression(left, gf, cctx)
                    return expression_to_call_add_entry(gf, pce1, left, x1, cctx)
                } else {
                    val y = 2
                     throw IllegalStateException("Error");
//                    return null // java lets you be lazy
                }
            }

            else -> throw IllegalStateException("Unexpected value: " + left.kind)
        }
    }

    private fun expression_to_call_add_entry(gf: BaseEvaFunction,
                                             pce: ProcedureCallExpression, left: IExpression, cctx: Context): Instruction {
        val i = Instruction()
        i.name = InstructionName.CALL // TODO see line 686
        val li: MutableList<InstructionArgument> = ArrayList()
        val assignment_path = gf.get_assignment_path(left, this, cctx)
        val args_types = get_args_types(pce.args, gf, cctx)
        val pte_num = addProcTableEntry(left, assignment_path, args_types, gf)
        li.add(ProcIA(pte_num, gf))
        val args_ = simplify_args(pce.args, gf, cctx)
        li.addAll(args_)
        i.setArgs(li)
        return i
    }

    private fun expression_to_call_add_entry(gf: BaseEvaFunction,
                                             pce: ProcedureCallExpression, left: IExpression, left1: InstructionArgument,
                                             cctx: Context): Instruction {
        /*
		 * pte_num = addProcTableEntry(left...) return Instruction { name: CALL, args: [
		 * ProcIA(pte_num) ] ++ simplify_args(...) }
		 */

        val args = pce.args
        val argsTypes = get_args_types(args, gf, cctx)
        val pte_num = addProcTableEntry(left, left1, argsTypes, gf)
        val args_ = simplify_args(args, gf, cctx)

        val li: MutableList<InstructionArgument> = ArrayList()
        li.add(ProcIA(pte_num, gf))
        li.addAll(args_)

        val i = Instruction(InstructionName.CALL, li)
        return i
    }

    private fun generate_item(item: OS_Element, gf: BaseEvaFunction, cctx: Context) {
        val gi = Generate_Item()
        if (item is AliasStatement) {
            gi.generate_alias_statement(item)
        } else if (item is CaseConditional) {
            gi.generate_case_conditional(item)
        } else if (item is ClassStatement) {
            // TODO this still has no ClassInvocation
            val gc = generateClass(item)
            val ite_index = gf.addIdentTableEntry(item.nameNode, cctx)
            val ite = gf.getIdentTableEntry(ite_index)
            ite.resolveTypeToClass(gc)
        } else if (item is StatementWrapper) {
            val x: IExpression = item.expr
            val expressionKind = x.kind
            gi.generate_statement_wrapper(item, x, expressionKind, gf, cctx)
        } else if (item is IfConditional) {
            gi.generate_if(item, gf)
        } else if (item is Loop) {
            LOG.err("800 -- generateLoop")
            gi.generate_loop(item, gf)
        } else if (item is MatchConditional) {
            gi.generate_match_conditional(item, gf)
        } else if (item is NamespaceStatement) {
//			LOG.info("Skip namespace for now "+((NamespaceStatement) item).name());
            throw NotImplementedException()
        } else if (item is VariableSequence) {
            gi.generate_variable_sequence(item, gf, cctx)
        } else if (item is WithStatement) {
            throw NotImplementedException()
        } else if (item is SyntacticBlock) {
            throw NotImplementedException()
        } else if (item is ConstructStatement) {
            gi.generate_construct_statement(item, gf, cctx)
        } else {
            throw IllegalStateException("cant be here")
        }
    }

    private fun generate_item_assignment(aStatementWrapper: StatementWrapper, x: IExpression,
                                         gf: BaseEvaFunction, cctx: Context) {
//		LOG.err(String.format("801 %s %s", x.getLeft(), ((BasicBinaryExpressionImpl) x).getRight()));
        val bbe = x as BasicBinaryExpression
        val right1 = bbe.right
        val gia = Generate_item_assignment()
        when (right1.kind) {
            ExpressionKind.PROCEDURE_CALL -> gia.procedure_call(aStatementWrapper, gf, bbe, right1 as ProcedureCallExpression, cctx)
            ExpressionKind.IDENT -> if (bbe.left is IdentExpression) {
                gia.ident(gf, bbe.left as IdentExpression, right1 as IdentExpression, cctx)
            } else if (bbe.left is DotExpression) {
                gia.dot(gf, bbe.left as DotExpression, right1, cctx)
            }

            ExpressionKind.NUMERIC -> gia.numeric(gf, bbe.left, right1 as NumericExpression, cctx)
            ExpressionKind.STRING_LITERAL -> gia.string_literal(gf, bbe.left, right1 as StringExpression, cctx)
            ExpressionKind.ADDITION, ExpressionKind.MULTIPLY, ExpressionKind.GE, ExpressionKind.GT -> gia.mathematical(gf, bbe.left, right1.kind, right1, cctx)
            ExpressionKind.NEG -> gia.neg(gf, bbe.left, right1.kind, right1, cctx)
            else -> {
                LOG.err("right1.getKind(): " + right1.kind)
                throw NotImplementedException()
            }
        }
    }

    private fun generate_item_dot_expression(backlink: InstructionArgument?,
                                             left: IExpression, right: IExpression, gf: BaseEvaFunction,
                                             cctx: Context) {
        val y = 2
        val x = gf.addIdentTableEntry(left as IdentExpression, cctx)

        val identIA = IdentIA(x, gf)

        val entry = identIA.entry

        entry.reactiveEventual.then { xxx: _Reactive_IDTE? ->
            xxx!!.join(this)
        }

        if (backlink != null) {
            identIA.setPrev(backlink)
            //			gf.getIdentTableEntry(x).addStatusListener(new DeduceTypes2.FoundParent());
//			gf.getIdentTableEntry(x).backlink = backlink;
        }
        if (right.left === right) return
        //
        if (right is IdentExpression) generate_item_dot_expression(IdentIA(x, gf), right.getLeft(), right, gf, cctx)
        else generate_item_dot_expression(IdentIA(x, gf), right.left,
                (right as BasicBinaryExpression).right, gf, cctx)
    }

    fun generateAllTopLevelClasses(lgc: MutableList<EvaNode?>) {
        for (item in module.items) {
            if (item is NamespaceStatement) {
                val ns = generateNamespace(item)
                lgc.add(ns)
            } else if (item is ClassStatement) {
                val kl = generateClass(item)
                lgc.add(kl)
            }
            // TODO enums, datatypes, (type)aliases
        }
    }

    fun generateClass(klass: ClassStatement): EvaClass {
        val gc = EvaClass(klass, module)
        val gcgc = __GenerateClass(LOG)

        for (item in ArrayList<ClassItem>(klass.items)) {
            gcgc.processItem(klass, item, gc)
        }

        return gc
    }

    /**
     * See [WlGenerateClass.run]
     *
     * @param aClassStatement
     * @param aClassInvocation
     * @return
     */
    fun generateClass(aClassStatement: ClassStatement, aClassInvocation: ClassInvocation?): EvaClass {
        val Result = generateClass(aClassStatement)
        Result.ci = aClassInvocation
        return Result
    }

    fun generateClass(aClassStatement: ClassStatement, aClassInvocation: ClassInvocation?,
                      aPassthruEnv: RegisterClassInvocation_env?): EvaClass {
        val gc = EvaClass(aClassStatement, module)
        val gcgc = __GenerateClass(LOG, aPassthruEnv)

        for (item in ArrayList<ClassItem>(aClassStatement.items)) {
            gcgc.processItem(aClassStatement, item, gc)
        }

        val Result = gc
        Result.ci = aClassInvocation
        return Result
    }

    fun generateConstructor(aConstructorDef: ConstructorDef,
                            parent: ClassStatement,  // TODO Namespace constructors
                            aFunctionInvocation: FunctionInvocation): EvaConstructor {
        val gf = EvaConstructor(aConstructorDef)
        gf.setFunctionInvocation(aFunctionInvocation)
        if (parent is ClassStatement) {
            val parentType = parent.oS_Type
            val selfIdent = IdentExpressionImpl.forString("self")
            val tte = gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, parentType,
                    selfIdent)
            gf.addVariableTableEntry("self", VariableTableType.SELF, tte, null)
        }

        run {
            val fali_args = aConstructorDef.fal().falis()
            val fi_args = aFunctionInvocation.args
            for (i in fali_args.indices) {
                val fali = fali_args[i]

                val tte1 = fi_args[i]
                val attached = tte1.attached

                // TODO for reference now...
                val genType: GenType = GenTypeImpl()
                val typeName = fali.typeName()
                if (typeName != null) genType.typeName = OS_UserType(typeName)
                genType.resolved = attached

                val attached1: OS_Type?
                if (attached == null && typeName != null) attached1 = genType.typeName
                else attached1 = attached

                val tte = gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, (attached1)!!,
                        fali.nameToken)

                //				assert attached != null; // TODO this fails
                gf.addVariableTableEntry(fali.name(), VariableTableType.ARG, tte, fali)
            }
        }

        val cctx = aConstructorDef.context
        val e1 = add_i(gf, InstructionName.E, null, cctx)
        for (item in aConstructorDef.items) {
//			LOG.err("7056 aConstructorDef.getItem = "+item);
            generate_item(item, gf, cctx)
        }
        val x1 = add_i(gf, InstructionName.X, Helpers.List_of<InstructionArgument>(IntegerIA(e1, gf)), cctx)
        gf.addContext(aConstructorDef.context, Range(e1, x1)) // TODO remove interior contexts
        //		LOG.info(String.format("602.1 %s", aConstructorDef.name()));
//		for (Instruction instruction : gf.instructionsList) {
//			LOG.info(instruction);
//		}
//		EvaFunction.printTables(gf);
        gf.fi = aFunctionInvocation
        return gf
    }

    fun generateFromEntryPoints(rq: GenerateFunctionsRequest) {
        val epl = rq.mod.entryPoints()
        val dcg = rq.classGenerator

        if (epl.isEmpty()) {
            return
        }

        epl.forEach(Consumer { entryPoint: EntryPoint -> pa.compilationEnclosure.addEntryPoint(getMirrorEntryPoint(entryPoint, rq.mt), dcg) })

        // FIXME looking too hard into phase...
        val wm = phase.wm

        // README Work is created (in dcg namely, by ...) when epl is not empty
        val wl = dcg.wl()

        if (!wl.isEmpty) {
            logProgress(1070, "WorkList not empty")
            wm!!.addJobs(wl)
            wm.drain()
        }
    }

    fun generateFunction(fd: FunctionDef, parent: OS_Element?,
                         aFunctionInvocation: FunctionInvocation): EvaFunction {
//		LOG.err("601.1 fn "+fd.name() + " " + parent);
        val gf = EvaFunction(fd)
        if (parent is ClassStatement) gf.addVariableTableEntry("self", VariableTableType.SELF, gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED,
                parent.oS_Type, IdentExpressionImpl.forString("self")), null)
        val returnType: OS_Type
        val returnType1 = fd.returnType()
        returnType = if (returnType1 == null) OS_UnitType()
        else OS_UserType(returnType1)
        gf.addVariableTableEntry("Result", VariableTableType.RESULT,
                gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, returnType, IdentExpressionImpl.forString("Result")),
                null) // TODO what about Unit returns?

        run {
            val fali_args = fd.fal().falis()
            val fi_args = aFunctionInvocation.args
            for (i in fali_args.indices) {
                val fali = fali_args[i]

                val tte1 = fi_args[i]
                val attached = tte1.attached

                // TODO for reference now...
                val genType: GenType = GenTypeImpl()
                val typeName = fali.typeName()
                if (typeName != null) genType.typeName = OS_UserType(typeName)
                genType.resolved = attached

                val attached1: OS_Type?
                if (attached == null && typeName != null) attached1 = genType.typeName
                else attached1 = attached

                val tte = gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, (attached1)!!,
                        fali.nameToken)

                //				assert attached != null; // TODO this fails
                gf.addVariableTableEntry(fali.name(), VariableTableType.ARG, tte, fali)
            }
        }

        // TODO Exception !!??

        //
        val cctx = fd.context
        val e1 = add_i(gf, InstructionName.E, null, cctx)
        for (item in fd.items) {
//			LOG.err("7001 fd.getItem = "+item);
            generate_item(item, gf, cctx)
        }
        val x1 = add_i(gf, InstructionName.X, Helpers.List_of<InstructionArgument>(IntegerIA(e1, gf)), cctx)
        gf.addContext(fd.context, Range(e1, x1)) // TODO remove interior contexts

        //		LOG.info(String.format("602.1 %s", fd.name()));
//		for (Instruction instruction : gf.instructionsList) {
//			LOG.info(instruction);
//		}
//		EvaFunction.printTables(gf);
        gf.fi = aFunctionInvocation

        pa.addFunctionStatement(FunctionStatement(gf)) // NOTE 10/19

        return gf
    }

    fun generateNamespace(namespace1: NamespaceStatement): EvaNamespace {
        val gn = EvaNamespace(namespace1, module)
        var an: AccessNotation? = null

        for (item in namespace1.items) {
            if (item is AliasStatement) {
                LOG.err("328 Skip AliasStatementImpl for now")
                //				throw new NotImplementedException();
            } else if (item is ClassStatement) {
                throw NotImplementedException()
            } else if (item is ConstructorDef) {
                throw NotImplementedException()
            } else if (item is DestructorDef) {
                throw NotImplementedException()
            } else if (item is FunctionDef) {
                // throw new NotImplementedException();
//				@NotNull EvaFunction f = generateFunction((FunctionDef) item, namespace1);
//				gn.addFunction((FunctionDef) item, f);
            } else if (item is DefFunctionDef) {
                throw NotImplementedException()
            } else if (item is NamespaceStatement) {
                throw NotImplementedException()
            } else if (item is VariableSequence) {
                for (vs in item.items()) {
//					LOG.info("6999 "+vs);
                    gn.addVarTableEntry(an, vs!!, null /* , passthruEnv */)
                }
            } else if (item is AccessNotation) {
                //
                // TODO two AccessNotationImpl's can be active at once, for example if the first
                // one defined only classes and the second one defined only a category
                //
                an = item
                //				gn.addAccessNotation(an);
            } else throw NotImplementedException()
        }

        gn.createCtor0()

        return gn
    }

    //
    // region add-table-entries
    //
    fun get_args_types(args: ExpressionList?,
                       gf: BaseEvaFunction,
                       aContext: Context): List<TypeTableEntry> {
        val R: MutableList<TypeTableEntry> = ArrayList()
        if (args != null) {
            for (arg in args) {
                val type: OS_Type? = null // arg.getType(); // README 10/14 this always returns null (remnant of DT1)
                //			LOG.err(String.format("108 %s %s", arg, type));
                if (arg is IdentExpression) {
                    R.add(craftTypeForIdentExpression(gf, aContext, arg, arg)!!)
                } else {
                    R.add(getType(arg, gf))
                }
            }
            assert(R.size == args.size())
        }
        return R
    }

    private fun get_args_types(args: List<FormalArgListItem>,
                               gf: BaseEvaFunction): List<TypeTableEntry> {
        val R: MutableList<TypeTableEntry> = ArrayList()
        //
        for (arg in args) {
            val tte: TypeTableEntry
            var ty: OS_Type?
            ty = if (arg.typeName() == null || arg.typeName().isNull) null
            else OS_UserType(arg.typeName())

            tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, ty!!, arg.nameToken)

            R.add(tte)
        }
        assert(R.size == args.size)
        return R
    }

    private fun getMirrorEntryPoint(entryPoint: EntryPoint, mt: ModuleThing): Mirror_EntryPoint {
        val m: Mirror_EntryPoint
        m = if (entryPoint is MainClassEntryPoint) {
            Mirror_MainClassEntryPoint(entryPoint, mt, this)
        } else if (entryPoint is ArbitraryFunctionEntryPoint) {
            Mirror_ArbitraryFunctionEntryPoint(entryPoint, mt, this)
        } else {
            throw IllegalStateException("unhandled")
        }
        return m
    }

    private fun getType(arg: IExpression, gf: BaseEvaFunction): TypeTableEntry {
        val tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, arg)
        return tte
    }

    private fun simplify_args(
            args: ExpressionList?, gf: BaseEvaFunction,
            cctx: Context): List<InstructionArgument> {
        val R: MutableList<InstructionArgument> = ArrayList()
        if (args == null) return R
        //
        for (expression in args) {
            val ia = simplify_expression(expression, gf, cctx)
            if (ia != null) {
//				LOG.err("109 "+expression);
                R.add(ia)
            } else {
                LOG.err("109-0 error expr not found $expression")
            }
        }
        return R
    }

    private fun simplify_args2(
            args: ExpressionList?, gf: EvaFunction,
            cctx: Context): Collection<InstructionArgument?> {
        var R: Collection<InstructionArgument?> = ArrayList()
        if (args == null) return R
        //
        R = Collections2.transform(args.expressions()) { input: IExpression ->
            assert(input != null)
            val expression: IExpression = input
            val ia: InstructionArgument? = simplify_expression(expression, gf, cctx)
            if (ia != null) {
                LOG.err("109-1 " + expression)
            } else {
                LOG.err("109-01 error expr not found " + expression)
            }
            ia
        }
        return R
    }

    private fun simplify_dot_expression(dotExpression: DotExpression,
                                        gf: BaseEvaFunction,
                                        cctx: Context): InstructionArgument {
        val x = gf.get_assignment_path(dotExpression, this, cctx)
        LOG.info("1117 $x")
        return x
    }

    fun simplify_expression(expression: IExpression, gf: BaseEvaFunction,
                            cctx: Context): InstructionArgument {
        val expressionKind = expression.kind
        when (expressionKind) {
            ExpressionKind.PROCEDURE_CALL -> return simplify_expression_procedure_call(expression, gf, cctx)
            ExpressionKind.CAST_TO -> {
                run {
                    val tce = expression as TypeCastExpression
                    val simp = simplify_expression(tce.left, gf, cctx)
                    val tte_index = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT,
                            OS_UserType(tce.typeName))
                    val x = add_i(gf, InstructionName.CAST_TO, Helpers.List_of(simp, IntegerIA(tte_index.getIndex(), gf)),
                            cctx)
                }
                run {
                    val tce = expression as TypeCastExpression
                    val simp = simplify_expression(tce.left, gf, cctx)
                    val tte_index = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT,
                            OS_UserType(tce.typeName))
                    val x = add_i(gf, InstructionName.AS_CAST, Helpers.List_of(simp, IntegerIA(tte_index.getIndex(), gf)),
                            cctx)
                }
                run {
                    val de = expression as DotExpression
                    return gf.get_assignment_path(de, this, cctx)
                }
            }

            ExpressionKind.AS_CAST -> {
                run {
                    val tce = expression as TypeCastExpression
                    val simp = simplify_expression(tce.left, gf, cctx)
                    val tte_index = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT,
                            OS_UserType(tce.typeName))
                    val x = add_i(gf, InstructionName.AS_CAST, Helpers.List_of(simp, IntegerIA(tte_index.getIndex(), gf)),
                            cctx)
                }
                run {
                    val de = expression as DotExpression
                    return gf.get_assignment_path(de, this, cctx)
                }
            }

            ExpressionKind.DOT_EXP -> {
                val de = expression as DotExpression
                return gf.get_assignment_path(de, this, cctx)
            }

            ExpressionKind.QIDENT -> {
                val q = expression as Qualident
                val de = Helpers0.qualidentToDotExpression2(q)
                return gf.get_assignment_path(de!!, this, cctx)
            }

            ExpressionKind.IDENT -> {
                val text = (expression as IdentExpression).text
                var i = gf.vte_lookup(text)
                if (i == null) {
                    val x = gf.getIdentTableEntryFor(expression)
                    if (x == null) {
                        val ii = gf.addIdentTableEntry(expression, cctx)
                        i = IdentIA(ii, gf)
                    } else {
                        i = IdentIA(x.index, gf)
                    }
                }
                return i
            }

            ExpressionKind.NUMERIC -> {
                val ne = expression as NumericExpression
                val ii = addConstantTableEntry2(null, ne, null, gf)
                return ConstTableIA(ii, gf)
            }

            ExpressionKind.STRING_LITERAL -> {
                val se = expression as StringExpression
                val ii = addConstantTableEntry2(null, se, null, gf)
                return ConstTableIA(ii, gf)
            }

            ExpressionKind.CHAR_LITERAL -> {
                val cle = expression as CharLitExpression
                val ii = addConstantTableEntry2(null, cle, null, gf)
                return ConstTableIA(ii, gf)
            }

            ExpressionKind.GET_ITEM -> {
                run {
                    val gie = expression as GetItemExpression
                    val left = gie.left
                    val right = gie.index()
                    val left_instruction: InstructionArgument
                    val right_instruction: InstructionArgument
                    if (left.is_simple) {
                        if (left is IdentExpression) {
                            left_instruction = simplify_expression(left, gf, cctx)
                        } else {
                            // a constant
                            assert(LangGlobals.isConstant(right))
                            val left_constant_num = addConstantTableEntry2(null, left, null, gf)
                            left_instruction = ConstTableIA(left_constant_num, gf)
                        }
                    } else {
                        // create a tmp var
                        left_instruction = simplify_expression(left, gf, cctx)
                    }
                    if (right.is_simple) {
                        if (right is IdentExpression) {
                            right_instruction = simplify_expression(right, gf, cctx)
                        } else {
                            // a constant
                            assert(LangGlobals.isConstant(right))
                            val right_constant_num = addConstantTableEntry2(null, right, null, gf)
                            right_instruction = ConstTableIA(right_constant_num, gf)
                        }
                    } else {
                        // create a tmp var
                        right_instruction = simplify_expression(right, gf, cctx)
                    }
                    run {
                        // create a call
                        val expr_kind_name = Helpers0
                                .string_to_ident(SpecialFunctions.of(expressionKind))
                        // TypeTableEntry tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT,
                        // null, expr_kind_name);
                        val tte_left = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null,
                                left)
                        val tte_right = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null,
                                right)
                        val pte = addProcTableEntry(expr_kind_name, null, Helpers.List_of(tte_left, tte_right), gf)
                        val tmp = addTempTableEntry(OS_BuiltinType(BuiltInTypes.Boolean), gf) // README should be
                        // Boolean
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(tmp, gf)), cctx)
                        val inst = Instruction()
                        inst.name = InstructionName.CALLS
                        inst.setArgs(Helpers.List_of(ProcIA(pte, gf), left_instruction, right_instruction))
                        val fca = FnCallArgs(inst, gf)
                        val x = add_i(gf, InstructionName.AGN, Helpers.List_of(IntegerIA(tmp, gf), fca), cctx)
                        return IntegerIA(tmp, gf)
                    }
                }
                run {
                    val subexpression = expression as SubExpression
                    val exp = subexpression.expression

                    val ia = simplify_expression(exp, gf, cctx)
                    return ia
                }
            }

            ExpressionKind.SUBEXPRESSION -> {
                val subexpression = expression as SubExpression
                val exp = subexpression.expression

                val ia = simplify_expression(exp, gf, cctx)
                return ia
            }

            ExpressionKind.LT_, ExpressionKind.GT, ExpressionKind.GE, ExpressionKind.ADDITION, ExpressionKind.MULTIPLY, ExpressionKind.SUBTRACTION, ExpressionKind.DIVIDE, ExpressionKind.NOT_EQUAL, ExpressionKind.EQUAL, ExpressionKind.MODULO -> {
                run {
                    val bbe = expression as BasicBinaryExpression
                    val left = bbe.left
                    val right = bbe.right
                    val left_instruction: InstructionArgument
                    val right_instruction: InstructionArgument
                    if (left.is_simple) {
                        if (left is IdentExpression) {
                            left_instruction = simplify_expression(left, gf, cctx)
                        } else {
                            // a constant
                            if (LangGlobals.isConstant(left)) {
                                val left_constant_num = addConstantTableEntry2(null, left, null, gf)
                                left_instruction = ConstTableIA(left_constant_num, gf)
                            } else {
                                left_instruction = simplify_expression(left, gf, cctx)
                            }
                        }
                    } else {
                        // create a tmp var
                        left_instruction = simplify_expression(left, gf, cctx)
                    }
                    if (right.is_simple) {
                        if (right is IdentExpression) {
                            right_instruction = simplify_expression(right, gf, cctx)
                        } else {
                            // a constant
                            if (LangGlobals.isConstant(right)) {
                                val right_constant_num = addConstantTableEntry2(null, right, null, gf)
                                right_instruction = ConstTableIA(right_constant_num, gf)
                            } else {
                                right_instruction = simplify_expression(right, gf, cctx)
                            }
                        }
                    } else {
                        // create a tmp var
                        right_instruction = simplify_expression(right, gf, cctx)
                    }
                    run {
                        // create a call
                        val expr_kind_name = Helpers0
                                .string_to_ident(SpecialFunctions.of(expressionKind))
                        //					TypeTableEntry tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, expr_kind_name);
                        val tte_left = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null,
                                left)
                        val tte_right = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null,
                                right)
                        val pte = addProcTableEntry(expr_kind_name, null, Helpers.List_of(tte_left, tte_right), gf)
                        val tmp = addTempTableEntry(OS_BuiltinType(BuiltInTypes.Boolean), gf)
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(tmp, gf)), cctx)
                        val inst = Instruction()
                        inst.name = InstructionName.CALLS
                        inst.setArgs(Helpers.List_of(ProcIA(pte, gf), left_instruction, right_instruction))
                        val fca = FnCallArgs(inst, gf)
                        // TODO should be AGNC
                        val x = add_i(gf, InstructionName.AGN, Helpers.List_of(IntegerIA(tmp, gf), fca), cctx)
                        return IntegerIA(tmp, gf) // TODO is this right?? we want to return the variable, not proc calls,
                    }
                }
                run {
                    val bbe = expression as UnaryExpression
                    val left = bbe.left
                    val left_instruction: InstructionArgument
                    if (left.is_simple) {
                        if (left is IdentExpression) {
                            left_instruction = simplify_expression(left, gf, cctx)
                        } else {
                            // a constant
                            val left_constant_num = addConstantTableEntry2(null, left, null, gf)
                            left_instruction = ConstTableIA(left_constant_num, gf)
                        }
                    } else {
                        // create a tmp var
                        left_instruction = simplify_expression(left, gf, cctx)
                    }
                    run {
                        // create a call
                        val expr_kind_name = Helpers0
                                .string_to_ident(SpecialFunctions.of(expressionKind))
                        //					TypeTableEntry tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, expr_kind_name);
                        val tte_left = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null,
                                left)
                        val pte = addProcTableEntry(expr_kind_name, null, Helpers.List_of(tte_left), gf)
                        val tmp = addTempTableEntry(OS_BuiltinType(BuiltInTypes.Boolean), gf)
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(tmp, gf)), cctx)
                        val inst = Instruction()
                        inst.name = InstructionName.CALLS
                        inst.setArgs(Helpers.List_of(ProcIA(pte, gf), left_instruction))
                        val fca = FnCallArgs(inst, gf)
                        // TODO should be AGNC
                        val x = add_i(gf, InstructionName.AGN, Helpers.List_of(IntegerIA(tmp, gf), fca), cctx)
                        return IntegerIA(tmp, gf)
                    }
                }
                run {
                    val s1 = simplify_expression(expression.left, gf, cctx)
                    val s2 = simplify_expression((expression as BasicBinaryExpression).right, gf, cctx)
                    val x = add_i(gf, InstructionName.AGN, Helpers.List_of(s1, s2), cctx)
                    return s1 // TODO is this right?
                }
            }

            ExpressionKind.NEG -> {
                run {
                    val bbe = expression as UnaryExpression
                    val left = bbe.left
                    val left_instruction: InstructionArgument
                    if (left.is_simple) {
                        if (left is IdentExpression) {
                            left_instruction = simplify_expression(left, gf, cctx)
                        } else {
                            val left_constant_num = addConstantTableEntry2(null, left, null, gf)
                            left_instruction = ConstTableIA(left_constant_num, gf)
                        }
                    } else {
                        left_instruction = simplify_expression(left, gf, cctx)
                    }
                    run {
                        val expr_kind_name = Helpers0
                                .string_to_ident(SpecialFunctions.of(expressionKind))
                        val tte_left = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null,
                                left)
                        val pte = addProcTableEntry(expr_kind_name, null, Helpers.List_of(tte_left), gf)
                        val tmp = addTempTableEntry(OS_BuiltinType(BuiltInTypes.Boolean), gf)
                        add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(tmp, gf)), cctx)
                        val inst = Instruction()
                        inst.name = InstructionName.CALLS
                        inst.setArgs(Helpers.List_of(ProcIA(pte, gf), left_instruction))
                        val fca = FnCallArgs(inst, gf)
                        val x = add_i(gf, InstructionName.AGN, Helpers.List_of(IntegerIA(tmp, gf), fca), cctx)
                        return IntegerIA(tmp, gf)
                    }
                }
                run {
                    val s1 = simplify_expression(expression.left, gf, cctx)
                    val s2 = simplify_expression((expression as BasicBinaryExpression).right, gf, cctx)
                    val x = add_i(gf, InstructionName.AGN, Helpers.List_of(s1, s2), cctx)
                    return s1
                }
            }

            ExpressionKind.ASSIGNMENT -> {
                val s1 = simplify_expression(expression.left, gf, cctx)
                val s2 = simplify_expression((expression as BasicBinaryExpression).right, gf, cctx)
                val x = add_i(gf, InstructionName.AGN, Helpers.List_of(s1, s2), cctx)
                return s1
            }

            else -> throw IllegalStateException("Unexpected value: $expressionKind")
        }//		return null;
    }

    // endregion
    private fun simplify_expression_procedure_call(expression: IExpression,
                                                   gf: BaseEvaFunction, cctx: Context): InstructionArgument {
        val pce = expression as ProcedureCallExpression
        val left = pce.left
        val args = pce.args
        val left_ia: InstructionArgument
        val right_ia: MutableList<InstructionArgument>
        //
        val initialCapacity = if (args != null) args.size() + 1 else 1
        right_ia = ArrayList(initialCapacity)
        //
        if (left.is_simple) {
            if (left is IdentExpression) {
                // for ident(xyz...)
                val x = gf.addIdentTableEntry(left, cctx)
                // TODO attach to var/const or lookup later in deduce
                left_ia = IdentIA(x, gf)
            } else if (left is SubExpression) {
                // for (1).toString() etc
                val ia = simplify_expression(left.expression, gf, cctx)
                // return ia; // TODO is this correct?
                left_ia = ia
            } else {
                // for "".strip() etc
                assert(LangGlobals.isConstant(left))
                val x = addConstantTableEntry(null, left, null,  /*left.getType()*/gf)
                left_ia = ConstTableIA(x, gf)
                //						throw new IllegalStateException("Cant be here");
            }
        } else {
            val x = simplify_expression(left, gf, cctx)
            left_ia = x
        }
        val args1: MutableList<TypeTableEntry> = ArrayList()
        if (args != null) {
            for (arg in args) {
                val ia: InstructionArgument
                val iat: TypeTableEntry?
                if (arg.is_simple) {
                    val y = 2
                    if (arg is IdentExpression) {
                        val x = gf.addIdentTableEntry(arg, cctx)
                        // TODO attach to var/const or lookup later in deduce
                        ia = IdentIA(x, gf)
                        iat = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, arg)
                    } else if (arg is SubExpression) {
                        ia = simplify_expression(arg.expression, gf, cctx)
                        iat = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null, arg)
                    } else {
                        assert(LangGlobals.isConstant(arg))
                        val x = addConstantTableEntry(null, arg, null,  /*arg.getType()*/gf)
                        ia = ConstTableIA(x, gf)
                        iat = gf.getConstTableEntry(x).typeTableEntry
                    }
                } else {
                    ia = simplify_expression(arg, gf, cctx)
                    iat = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null,  /* README wait to be deduced */arg)
                }
                right_ia.add(ia)
                args1.add(iat)
            }
        }
        val pte = addProcTableEntry(expression, left_ia, args1, gf)
        right_ia.add(0, ProcIA(pte, gf))
        run {
            val tmp_var = addTempTableEntry(null, gf) // line 686 is here
            add_i(gf, InstructionName.DECL, Helpers.List_of(SymbolIA("tmp"), IntegerIA(tmp_var, gf)), cctx)
            val i = Instruction()
            i.name = InstructionName.CALL
            i.setArgs(right_ia)
            // TODO should be AGNC
            val x = add_i(gf, InstructionName.AGN, Helpers.List_of(IntegerIA(tmp_var, gf), FnCallArgs(i, gf)),
                    cctx)
            return IntegerIA(tmp_var, gf) // return tmp_var instead of expression assigning it
        }
    }

    private fun simplify_procedure_call(pce: ProcedureCallExpression, gf: BaseEvaFunction,
                                        cctx: Context) {
        val left = pce.left
        val args = pce.args
        //
        var expression_num = simplify_expression(left, gf, cctx)
        if (expression_num == null) {
            expression_num = gf.get_assignment_path(left, this, cctx)
        }
        val i = addProcTableEntry(left, expression_num, get_args_types(args, gf, cctx), gf)
        val l: MutableList<InstructionArgument> = ArrayList()
        l.add(ProcIA(i, gf))
        l.addAll(simplify_args(args, gf, cctx))
        val instructionIndex = add_i(gf, InstructionName.CALL, l, cctx)
        run {
            val pte = gf.getProcTableEntry(i)
            if (expression_num is IdentIA) {
                val idte = expression_num.entry
                idte.callablePTE = pte
                pte.typePromise().then(DoneCallback { result ->

                    // TODO should this be done here?
                    val tte = gf.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, result.resolved)
                    tte.genType.copy(result)
                    idte.addPotentialType(instructionIndex, tte)
                })
            }
        }
    }

    private fun simplify_qident(left: Qualident, gf: EvaFunction) {
        throw NotImplementedException()
    }

    companion object {
        private const val PHASE = "GenerateFunctions"

        private fun logProgress(code: Int, message: String) {
            // TODO LOG
            SimplePrintLoggerToRemoveSoon.println_err_4("** $code $message")
        }

        private fun craftTypeForIdentExpression(gf: BaseEvaFunction,
                                                aContext: Context,
                                                arg: IExpression,
                                                identExpression: IdentExpression): TypeTableEntry? {
            val x = gf.vte_lookup(identExpression.text)
            val tte: TypeTableEntry
            if (x is ConstTableIA) {
                val cte = gf.getConstTableEntry(x.index)
                tte = cte.typeTableEntry
            } else if (x is IntegerIA) {
                val vte = gf.getVarTableEntry(x.index)
                tte = vte.typeTableEntry
            } else {
                //
                // WHEN VTE_LOOKUP FAILS, IE WHEN A MEMBER VARIABLE
                //

                // TODO 10/13 assert above

                val idte_index = gf.addIdentTableEntry(identExpression, aContext)
                val identTableEntry = gf.getIdentTableEntry(idte_index)

                tte = gf.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, null, arg)
                identTableEntry.type = tte
            }

            return tte
        }
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

