package tripleo.elijah.comp.internal

import com.google.common.eventbus.DeadEvent
import com.google.common.eventbus.EventBus
import com.google.common.eventbus.Subscribe
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.i.LCM_Event
import tripleo.elijah.comp.i.LCM_HandleEvent
import tripleo.elijah.nextgen.comp_model.CM_UleLog

class LCM(private val _compilation: Compilation) {
	private val eventBus = EventBus()
	private val listener: LCM_EventListener

	init {
		listener = LCM_EventListener()
		listener._setLog(_compilation)

		eventBus.register(listener)
	}

	fun asv(obj: Any?, event: LCM_Event?) {
		eventBus.post(LCM_HandleEvent(_compilation.lcmAccess, this, obj, event))
	}

	fun exception(aHandleEvent: LCM_HandleEvent?, aE: Exception?) {
		throw RuntimeException(aE) // FIXME 11/24 rethrow
	}

	inner class LCM_EventListener {
		private var LOG: CM_UleLog? = null

		@Subscribe
		fun handleDeadEvent(deadEvent: DeadEvent) {
			LOG!!.info("unhandled event [" + deadEvent.event + "]")
			Companion.eventsHandled++
		}

		fun _setLog(_compilation: Compilation) {
			LOG = _compilation.con().uLog
		}

		@Subscribe
		fun someLCM_Event(aHandleEvent: LCM_HandleEvent) {
			LOG!!.info("do event [$aHandleEvent]")
			aHandleEvent.event.handle(aHandleEvent)
			Companion.eventsHandled++
		}

		fun resetEventsHandled() {
			Companion.eventsHandled = 0
		}

		val eventsHandled: Int
			get() = eventsHandled
	}

	companion object {
		//		private static final Logger LOG = LoggerFactory.getLogger(LCM_EventListener.class);
		private var eventsHandled = 0
	}

}
