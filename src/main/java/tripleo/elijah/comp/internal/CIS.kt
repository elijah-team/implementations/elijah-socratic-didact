package tripleo.elijah.comp.internal

import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.subjects.ReplaySubject
import io.reactivex.rxjava3.subjects.Subject
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.CompilerInstructionsObserver
import tripleo.elijah.comp.i.IProgressSink
import tripleo.elijah.util.ObservableCompletableProcess
import tripleo.elijah.util2.DebugFlags
import tripleo.elijah.util2.UnintendedUseException

class CIS : Observer<CompilerInstructions> {
    private val ocp_ci = ObservableCompletableProcess<CompilerInstructions>()
    private val compilerInstructionsSubject: Subject<CompilerInstructions> = ReplaySubject.create()
    var progressSink: IProgressSink? = null
    private var _cio: CompilerInstructionsObserver? = null

    fun almostComplete() {
        _cio!!.almostComplete()
    }

    override fun onSubscribe(d: Disposable) {
        if (DebugFlags.CIS_ocp_ci__FeatureGate) {
            compilerInstructionsSubject.onSubscribe(d)
        } else {
            ocp_ci.onSubscribe(d)
        }
    }

    override fun onNext(aCompilerInstructions: CompilerInstructions) {
        if (DebugFlags.CIS_ocp_ci__FeatureGate) { // l.add
            compilerInstructionsSubject.onNext(aCompilerInstructions)
        } else {
            ocp_ci.onNext(aCompilerInstructions)
        }
    }

    override fun onError(e: Throwable) {
        if (DebugFlags.CIS_ocp_ci__FeatureGate) {
            compilerInstructionsSubject.onError(e)
        } else {
            ocp_ci.onError(e)
        }
    }

    override fun onComplete() {
        throw UnintendedUseException()
    }

    fun set_cio(a_cio: CompilerInstructionsObserver?) {
        _cio = a_cio
    }

    fun subscribe(aCio: Observer<CompilerInstructions>) {
        if (DebugFlags.CIS_ocp_ci__FeatureGate) {
            compilerInstructionsSubject.subscribe(aCio)
        } else {
            ocp_ci.subscribe(aCio)
        }
    }

    fun subscribeTo(aC: Compilation) {
        aC.subscribeCI(_cio as Observer<CompilerInstructions>)
    }
}
