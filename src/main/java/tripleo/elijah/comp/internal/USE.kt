package tripleo.elijah.comp.internal

import org.jetbrains.annotations.Contract
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.ci.LibraryStatementPart
import tripleo.elijah.ci_impl.LibraryStatementPartImpl
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.caches.DefaultElijahCache
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.comp.i.CompilationClosure
import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.comp.i.USE_Reasoning
import tripleo.elijah.comp.internal.CompilationImpl.CompilationAlways
import tripleo.elijah.comp.local.CW
import tripleo.elijah.comp.local.CW.CX_ParseElijahFile.ElijahSpecReader
import tripleo.elijah.comp.local.CW_sourceDirRequest
import tripleo.elijah.comp.local.CY_FindPrelude
import tripleo.elijah.comp.specs.ElijahCache
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.ExceptionDiagnostic
import tripleo.elijah.diagnostic.FileNotFoundDiagnostic
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.util.*
import tripleo.wrap.File
import java.io.FileNotFoundException
import java.io.FilenameFilter
import java.io.InputStream
import java.util.regex.Pattern

class USE @Contract(pure = true) constructor(cc: CompilationClosure) {
    private val c = cc.compilation as Compilation
    private val errSink: ErrSink = cc.errSink()

    val elijahCache: ElijahCache = DefaultElijahCache()

    fun findPrelude(prelude_name: String?): Operation2<OS_Module?> {
        val cyFindPrelude = CY_FindPrelude(
                { c },
                { elijahCache }
        )
        return cyFindPrelude.findPrelude(prelude_name)
    }

    private fun parseElijjahFile(f: File,
                                 file_name: String,
                                 lsp: LibraryStatementPart): Operation2<OS_Module> {
        logProgress(CompProgress.USE__parseElijjahFile, f.absolutePath)

        if (!f.exists()) {
            val e: Diagnostic = FileNotFoundDiagnostic(f.wrapped()) // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

            return Operation2.failure(e)
        }

        val om: Operation2<OS_Module>

        try {
            val rdr = ElijahSpecReader {
                try {
                    val readFile = c.io.readFile(f)
                    return@ElijahSpecReader Operation.success<InputStream>(readFile)
                } catch (aE: FileNotFoundException) {
                    return@ElijahSpecReader Operation.failure<InputStream>(aE)
                }
            }
            om = CW.CX_ParseElijahFile.__parseEzFile(
                    file_name,
                    f,
                    rdr,  //c.getIO(),
                    c.con().defaultElijahSpecParser(elijahCache)
            )

            when (om.mode()) {
                Mode.SUCCESS -> {
                    val mm = om.success()
                    val cm = c.megaGrande(mm)

                    //assert mm.getLsp() == null;
                    //assert mm.prelude() == null;
                    cm.advise(lsp)
                    cm.advise { findPrelude(CompilationAlways.Companion.defaultPrelude()) }

                    return om
                }

                else -> {
                    return om
                }
            }
        } catch (aE: Exception) {
            return Operation2.failure(ExceptionDiagnostic(aE))
        }
    }

    private fun logProgress(aCompProgress: CompProgress, aAbsolutePath: String) {
        c.compilationEnclosure.logProgress(aCompProgress, aAbsolutePath)
    }

    fun use(compilerInstructions: CompilerInstructions) {
        // TODO

        if (compilerInstructions.filename == null) return

        val file = compilerInstructions.makeFile()
        val instruction_dir = file.parentFile

        if (instruction_dir == null) {
            SimplePrintLoggerToRemoveSoon.println_err_4("106106 ************************************** $file")
            // Prelude.elijjah is a special case
            // instruction_dir = file;
            return
        }

        for (lsp in compilerInstructions.libraryStatementParts) {
            val dir_name = Helpers.remove_single_quotes_from_string(lsp.dirName)
            val dir: File // = new File(dir_name);
            var reasoning: USE_Reasoning? = null
            if (dir_name == "..") {
                dir = instruction_dir /* .getAbsoluteFile() */.parentFile // FIXME 09/26 this has always been questionable
                reasoning = USE_Reasonings.Companion.parent(compilerInstructions, true, instruction_dir, lsp)
            } else {
                dir = File(instruction_dir, dir_name)
                reasoning = USE_Reasonings.Companion.child(compilerInstructions, false, instruction_dir, dir_name, dir, lsp)
            }
            use_internal(dir, lsp, reasoning)
        }

        val lsp: LibraryStatementPart = LibraryStatementPartImpl()
        lsp.setName(Helpers0.makeToken("default")) // TODO: make sure this doesn't conflict
        lsp.setDirName(Helpers0.makeToken(String.format("\"%s\"", instruction_dir)))
        lsp.instructions = compilerInstructions
        val reasoning: USE_Reasoning = USE_Reasonings.Companion.default_(compilerInstructions, false, instruction_dir, lsp)
        use_internal(instruction_dir, lsp, reasoning)
    }

    private fun use_internal(dir: File, lsp: LibraryStatementPart, aReasoning: USE_Reasoning?) {
        if (dir.wrapped() == null) {
            val y = 2
        }
        if (!dir.isDirectory) {
            errSink.reportError("9997 Not a directory $dir")
            return
        }
        //
        val files = dir.listFiles(accept_source_files)
        if (files != null) {
            CW_sourceDirRequest.apply(files, dir, lsp, { file: File ->
                val file_name = file.toString()
                parseElijjahFile(file, file_name, lsp)
            }, c, aReasoning)
        }
    }

    companion object {
        private val accept_source_files = FilenameFilter { directory: java.io.File?, file_name: String? ->
            val matches = (Pattern.matches(".+\\.elijah$", file_name)
                    || Pattern.matches(".+\\.elijjah$", file_name))
            matches
        }
    }
}
