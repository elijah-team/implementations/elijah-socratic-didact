package tripleo.elijah.nextgen.model

interface SM_ClassBody : SM_Node {
    fun children(): List<SM_Node?>?
}
