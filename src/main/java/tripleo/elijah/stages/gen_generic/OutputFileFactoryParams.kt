package tripleo.elijah.stages.gen_generic

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.stages.logging.ElLog.Verbosity

import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure

import tripleo.elijah.comp.PipelineLogic

import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.world.i.WorldModule

data class OutputFileFactoryParams // if (mod.ce != null) //!!
    (
            val worldMod: WorldModule
            , val compilationEnclosure: CompilationEnclosure
   ) {

    val errSink: ErrSink
        get() = compilationEnclosure.compilationClosure.errSink()

    val mod: OS_Module
        get () = worldMod.module()

    val pipelineLogic: PipelineLogic
        get() = compilationEnclosure.pipelineLogic

    val verbosity: Verbosity
        get() = compilationEnclosure.compilationAccess.testSilence()
}
