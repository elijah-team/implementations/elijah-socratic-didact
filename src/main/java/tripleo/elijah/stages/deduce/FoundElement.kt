/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import tripleo.elijah.lang.i.OS_Element

/**
 * Created 1/12/21 2:10 AM
 */
abstract class FoundElement(aPhase: DeducePhase) {
    private var _called = false
    private var _didntFind = false

    init {
        aPhase.registerFound(this)
    }

    fun didntFind(): Boolean {
        return _didntFind
    }

    fun doFoundElement(e: OS_Element) {
        if (_called) return

        _didntFind = false
        _called = true
        foundElement(e)
    }

    fun doNoFoundElement() {
        if (_called) return

        _didntFind = true
        _called = true
        noFoundElement()
    }

    abstract fun foundElement(e: OS_Element?)

    abstract fun noFoundElement()
}
