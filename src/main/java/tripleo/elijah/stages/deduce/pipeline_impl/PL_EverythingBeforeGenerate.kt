package tripleo.elijah.stages.deduce.pipeline_impl

import tripleo.elijah.comp.PipelineLogic

internal class PL_EverythingBeforeGenerate : PipelineLogicRunnable {
    override fun run(pipelineLogic: PipelineLogic) {
        val mcp = pipelineLogic._mcp()

        val ce = pipelineLogic._pa().compilationEnclosure
        ce.compilation.world().addModuleProcess(mcp)
    }
}
