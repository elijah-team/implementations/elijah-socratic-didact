package tripleo.elijah.stages.deduce.pipeline_impl

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.stages.gen_fn.EvaNode

internal class PL_SaveGeneratedClasses @Contract(pure = true) constructor(private val pa: IPipelineAccess) : PipelineLogicRunnable {
    override fun run(pipelineLogic: PipelineLogic) {
        val deducePhase = pipelineLogic.dp

        deducePhase.country().sendClasses { aEvaNodeList: List<EvaNode?>? -> pa.setNodeList(aEvaNodeList) }
    }
}
