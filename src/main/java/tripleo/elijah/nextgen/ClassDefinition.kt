package tripleo.elijah.nextgen

import tripleo.elijah.g.GClassInvocation
import tripleo.elijah.g.GEvaClass
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.nextgen.composable.IComposable

interface ClassDefinition {
    var composable: IComposable?

    val extended: Set<ClassStatement>

    var invocation: GClassInvocation

    //@JvmField
    var node: GEvaClass?

    val primary: ClassStatement
}
