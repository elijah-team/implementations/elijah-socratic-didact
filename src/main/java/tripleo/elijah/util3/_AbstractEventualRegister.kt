package tripleo.elijah.util3

import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.Eventual
import tripleo.elijah.util2.EventualRegister

abstract class _AbstractEventualRegister : EventualRegister {
    private val _eventuals: MutableList<Eventual<*>> = ArrayList()

    override fun checkFinishEventuals() {
        _eventuals.stream()
                .filter { eventual: Eventual<*> -> !eventual.isResolved }
                .map { eventual: Eventual<*> -> "[PipelineLogic::checkEventual] failed for " + eventual.description() }
                .forEach { aS: String? -> SimplePrintLoggerToRemoveSoon.println_err_4(aS) }
    }

    override fun <P> register(e: Eventual<P>) {
        _eventuals.add(e)
    }
}
