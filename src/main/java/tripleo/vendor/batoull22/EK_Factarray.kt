package tripleo.vendor.batoull22

/**
 * @author tripleo
 */
internal class EK_Factarray(private val st: String, private val ch: Array<EK_Fact?>) {
    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as EK_Factarray
        return this.st == that.st && (this.ch == that.ch)
    }

    override fun toString(): String {
        return "EK_Factarray[st=$st, ch=$ch]"
    }

    fun st(): String {
        return this.st
    }

    fun ch(): Array<EK_Fact?> {
        return this.ch
    }
}
