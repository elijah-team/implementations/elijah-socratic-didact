package tripleo.elijah.stages.gen_generic.pipeline_impl

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.g.GEvaClass
import tripleo.elijah.g.GEvaNamespace
import tripleo.elijah.nextgen.output.NG_OutputFunction
import tripleo.elijah.stages.gen_c.C2C_Result
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_generic.GRS_Addable
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.util2.ElijahInternal
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah.world.i.LivingClass
import tripleo.elijah.world.i.LivingNamespace

class DefaultGenerateResultSink @Contract(pure = true) constructor(private val pa: IPipelineAccess) : GenerateResultSink {
    override fun add(aAddable: GRS_Addable) {
        assert(aAddable is EvaNode)
        if (aAddable is EvaNode) {
            // README 11/30 Preserve original behavior by throwing
            //throw new IllegalStateException("Error");
            throw UnintendedUseException()
        } else {
            aAddable.action(this)
        }
    }

    override fun addFunction(aGf: BaseEvaFunction, aRs: List<C2C_Result>, aGenerateFiles: GenerateFiles) {
        val o = NG_OutputFunction()
        o.setFunction(aGf, aGenerateFiles, aRs)
        pa.addOutput(o)
    }

    override fun additional(aGenerateResult: GenerateResult) {
        // throw new IllegalStateException("Error");
    }

    override fun getLivingClassForEva(aEvaClass: GEvaClass): LivingClass? {
        return pa.compilation.world().getClass(aEvaClass as EvaClass)
    }

    override fun getLivingNamespaceForEva(aEvaNamespace: GEvaNamespace): LivingNamespace? {
        return pa.compilation.world().getNamespace(aEvaNamespace as EvaNamespace)
    }

    @ElijahInternal
    override fun pa(): IPipelineAccess {
        return pa
    }
}
