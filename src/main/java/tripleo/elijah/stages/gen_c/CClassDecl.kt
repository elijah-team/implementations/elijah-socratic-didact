/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.StringExpression
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.util.Helpers0
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Created 12/24/20 7:42 AM
 */
class CClassDecl(private val evaClass: EvaClass) {
    var prim: Boolean = false
    var prim_decl: String? = null

    fun evaluatePrimitive() {
        val xx = evaClass.klass
        xx.walkAnnotations { anno ->
            if (anno.annoClass() == Helpers0.string_to_qualident("C.repr")) {
                if (anno.exprs != null) {
                    val expressions = ArrayList(
                            anno.exprs.expressions())
                    val str0 = expressions[0]
                    if (str0 is StringExpression) {
                        val str = str0.text
                        setDecl(str)
                    } else {
                        SimplePrintLoggerToRemoveSoon.println_out_2("Illegal C.repr")
                    }
                }
            }
            if (anno.annoClass() == Helpers0.string_to_qualident("Primitive")) setPrimitive()
        }
    }

    fun setDecl(str: String?) {
        prim_decl = str
    }

    fun setPrimitive() {
        prim = true
    }
} //
//
//

