package tripleo.elijah.comp.process

import tripleo.elijah.comp.Pipeline.RP_Context_1
import tripleo.elijah.comp.graph.i.CK_Action
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.graph.i.CK_StepsContext
import tripleo.elijah.comp.i.RuntimeProcess
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.OStageProcess
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.util.Mode
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation

class CK_RunBetterAction : CK_Action {
    override fun execute(context1: CK_StepsContext, aMonitor: CK_Monitor): Operation<Ok> {
        val context = context1 as CD_CRS_StepsContext
        val crState = context.state
        val output = context.output

        try {
            val ca = crState!!.ca()
            val compilation = ca.compilation
            val ce = ca.compilationEnclosure as CompilationEnclosure

            ce.pipelineAccessPromise.then { pa: IPipelineAccess ->
                //final GPipelineAccess      pa             = compilation.pa();
                val processRecord = pa.processRecord
                val process: RuntimeProcess = OStageProcess(processRecord.ca())

                try {
                    process.prepare()
                } catch (aE: Exception) {
                    //throw new RuntimeException(aE);
                }

                val res = process.run(compilation, RP_Context_1(crState, output))

                if (res.mode() == Mode.FAILURE) {
                    //Logger.getLogger(OStageProcess.class.getName()).log(Level.SEVERE, null, ex);

                    val ex = res.failure()
                    ex.printStackTrace()

                    //return Operation.failure(ex); eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
                }

                process.postProcess()
                ce.writeLogs()
            }
            return Operation.success(Ok.instance())
        } catch (aE: Exception) {
            aE.printStackTrace() // TODO debug 07/26; 10/20 do we still want this?? also steps
            return Operation.failure(aE)
        }
    }
}
