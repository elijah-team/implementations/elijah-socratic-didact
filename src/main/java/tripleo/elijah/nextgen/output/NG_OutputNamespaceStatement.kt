package tripleo.elijah.nextgen.output

import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.nextgen.inputtree.EIT_ModuleInput
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.inputtree.EIT_InputType
import tripleo.elijah.nextgen.inputtree.EIT_ModuleInputImpl
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.util.BufferTabbedOutputStream
import tripleo.util.buffer.Buffer

data class NG_OutputNamespaceStatement(val bufferTabbedOutputStream: BufferTabbedOutputStream,
                                       val module: OS_Module,
                                       override val ty: TY) : NG_OutputStatement {
	private val moduleDependency = NG_OutDep(module)
	private val buf: Buffer = bufferTabbedOutputStream.buffer


	override val explanation: EX_Explanation
		get() = EX_Explanation.withMessage("NG_OutputNamespaceStatement")

	override val moduleInput: EIT_ModuleInput
		get() {
			val m = moduleDependency().module()

			val c = m.compilation as Compilation
			val moduleInput: EIT_ModuleInput = EIT_ModuleInputImpl(m, c, EIT_InputType.ELIJAH_SOURCE)
			return moduleInput
		}

	override val text: String
		get() = buf.text

	/*private*/ fun moduleDependency(): NG_OutDep = moduleDependency
}
