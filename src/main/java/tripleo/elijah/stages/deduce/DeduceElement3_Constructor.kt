package tripleo.elijah.stages.deduce

import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.lang.i.*
//import tripleo.elijah.lang.i.*
import tripleo.elijah.stages.deduce.post_bytecode.DED
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3.DeduceElement3_Kind
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.stages.inter.ModuleThing
import tripleo.elijah.util2.ReadySupplier_1
import tripleo.elijah.util2.UnintendedUseException

class DeduceElement3_Constructor(private val evaConstructor: EvaConstructor, private val deduceTypes2: DeduceTypes2, val compilerEnclosure: CompilationEnclosure) : IDeduceElement3 {
	fun __post_deduce_generated_function_base(aDeducePhase: DeducePhase) {
		for (identTableEntry in evaConstructor.idte_list) {
			if (identTableEntry.resolvedElement is VariableStatement) {
				val vs = identTableEntry.resolvedElement as VariableStatement
				val el: OS_Element = vs.getParent()?.getParent()!!
				val el2 = evaConstructor.fd.parent

				if (el !== el2) {
					when {
						el !is ClassStatement && el !is NamespaceStatement -> {
							continue
						}
						else -> {
							System.err.println("9998-3434 "+el.javaClass.name)
						}
					}

					// NOTE there is no concept of gf here
					aDeducePhase.registerResolvedVariable(identTableEntry, el, vs.getName())
				}
			}
		}
		run {
			val gf: IEvaConstructor = evaConstructor
			var result_index = gf.vte_lookup("Result")
			if (result_index == null) {
				// if there is no Result, there should be Value
				result_index = gf.vte_lookup("Value")
				// but Value might be passed in. If it is, discard value
				if (result_index != null) {
					val vte = (result_index as IntegerIA).entry
					if (vte.vtt != VariableTableType.RESULT) {
						result_index = null
					}
				}
			}
			if (result_index != null) {
				val vte = (result_index as IntegerIA).entry
				if (vte.resolvedType() == null) {
					val b = vte.genType
					val a = vte.typeTableEntry.attached
					if (a != null) {
						// see resolve_function_return_type
						when (a.type) {
							OS_Type.Type.USER_CLASS -> dof_uc(vte, a)
							OS_Type.Type.USER -> dof_uu(b, a, vte)
							else -> {                            // TODO do nothing for now
								var y3 = 2
							}
						}
					}
					/*
					 * else throw new NotImplementedException();
					 */
				}
			}
		}
		//		aDeducePhase.addFunction(aGeneratedConstructor, (FunctionDef) aGeneratedConstructor.getFD()); // TODO do we need this?
	}

	private fun dof_uu(b: GenType, a: OS_Type, vte: VariableTableEntry) {
		b.typeName = a
		try {
			val rt = deduceTypes2.resolve_type(a, a.typeName.context)
			if (rt.resolved != null && rt.resolved.type == OS_Type.Type.USER_CLASS) {
				if (rt.resolved.classOf.genericPart.size > 0) b.setNonGenericTypeName(a.typeName) // TODO might be wrong

				dof_uc(vte, rt.resolved)
			}
		} catch (aResolveError: ResolveError) {
			deduceTypes2._errSink().reportDiagnostic(aResolveError)
		}
	}

	override fun deduceTypes2(): DeduceTypes2 {
		return deduceTypes2
	}

	private fun dof_uc(aVte: VariableTableEntry, aOSType: OS_Type) {
		// we really want a ci from somewhere
		assert(aOSType.classOf.genericPart.size == 0)
		var ci = ClassInvocation(aOSType.classOf, null, ReadySupplier_1(deduceTypes2))
		ci = deduceTypes2.phase.registerClassInvocation(ci)

		aVte.genType.resolved = aOSType // README assuming OS_Type cannot represent namespaces
		aVte.genType.ci = ci

		ci.resolvePromise().done { aNode: EvaClass? -> aVte.resolveTypeToClass(aNode!!) }
	}

	override fun elementDiscriminator(): DED {
		throw UnintendedUseException("TODO fulfill or error")
	}

	override fun generatedFunction(): BaseEvaFunction {
		return evaConstructor
	}

	override fun genType(): GenType {
		throw UnintendedUseException("TODO fulfill or error")
	}

	override fun getPrincipal(): OS_Element {
		return evaConstructor.fd
	}

	override fun kind(): DeduceElement3_Kind {
		throw UnintendedUseException("TODO fulfill or error")
	}

	override fun resolve(aContext: Context, dt2: DeduceTypes2) {
	}

	override fun resolve(aIdentIA: IdentIA, aContext: Context, aFoundElement: FoundElement) {
		throw UnintendedUseException()
	}

	fun deduceOneConstructor(aMt: ModuleThing) {
		val mt = aMt

		//		var ccc = this;
		deduceTypes2.deduce_generated_function_base(evaConstructor, evaConstructor.fd, mt)

		//		evaConstructor.de3_Promise()
//				.then((DeduceElement3_Constructor c) -> {
//					assert c == ccc;
//					c.
		__post_deduce_generated_function_base(deduceTypes2._phase())
		//				});
	}
}
