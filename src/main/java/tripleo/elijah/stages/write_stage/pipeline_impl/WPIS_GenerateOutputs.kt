package tripleo.elijah.stages.write_stage.pipeline_impl

import com.google.common.base.Preconditions
import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.nextgen.pn.PN_Ping
import tripleo.elijah.comp.nextgen.pn.PN_signalCalculateFinishParse
import tripleo.elijah.comp.nextgen.pw.PW_signalCalculateFinishParse
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.output.NG_OutputItem
import tripleo.elijah.nextgen.outputstatement.EG_Naming
import tripleo.elijah.nextgen.outputstatement.EG_SequenceStatement
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EG_Statement.Companion.of
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.nextgen.outputstatement.mk_EG_SequenceStatement
import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.nextgen.outputtree.EOT_OutputFileImpl
import tripleo.elijah.nextgen.outputtree.EOT_OutputType
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.generate.OutputStrategy
import tripleo.elijah.stages.generate.OutputStrategyC
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.*
import java.util.function.Consumer

class WPIS_GenerateOutputs : WP_Individual_Step, PN_signalCalculateFinishParse {
    private val ors: List<NG_OutputRequest> = ArrayList()
    private var st: WritePipelineSharedState? = null
    private lateinit var amazings: MutableList<Amazing>

    override fun act(st: WritePipelineSharedState, sc: WP_State_Control) {
        Preconditions.checkState(st.gr != null)
        Preconditions.checkState(st.sys != null)

        val result = st.gr

        //final SPrintStream sps = new SPrintStream();
        //DebugBuffersLogic.debug_buffers_logic(result, sps);

        //final Default_WPIS_GenerateOutputs_Behavior_PrintDBLString printDBLString = new Default_WPIS_GenerateOutputs_Behavior_PrintDBLString();
        //printDBLString.print(sps.getString());
        this.st = st

        val itms = OutputItems()

        st.c.pushWork(PW_signalCalculateFinishParse.instance(), object : PN_Ping<Any> {
            override fun ping(t: Any) {
//				final OutputItems itms = new OutputItems(); // maybe belongs here?


                SimplePrintLoggerToRemoveSoon.println_err_4("999053 " + t.javaClass.name)


                val cs = st.pa.activeClasses
                val ns = st.pa.activeNamespaces
                val fs = st.pa.activeFunctions

                pmPN_signalCalculateFinishParse(result!!, cs, ns, fs, itms)
            }
        })
    }

    private fun pmPN_signalCalculateFinishParse(result: GenerateResult,
                                                cs: List<EvaClass>,
                                                ns: List<EvaNamespace>,
                                                fs: List<BaseEvaFunction>,
                                                itms: OutputItems) {
        val p = PM_signalCalculateFinishParse(result, cs, ns, fs, itms)
        ping(p)
    }

    override fun ping(signal: PM_signalCalculateFinishParse) {
        val cs = signal.cs
        val ns = signal.ns
        val fs = signal.fs
        val itms = signal.itms
        val result = signal.result

        val totalCount = cs!!.size + ns!!.size + fs!!.size
        itms!!.readyCount(totalCount) // looks like it should work, but also looks like it won't

        amazings = ArrayList(totalCount)

        for (c in cs) {
            val amazingClass = AmazingClass(c!!, itms, st!!.pa)
            waitGenC(amazingClass.mod()) { amazingClass.waitGenC(it) }
            amazings.add(amazingClass)
        }
        for (f in fs) {
            val amazingFunction = AmazingFunction(f!!, itms, result!!, st!!.pa)
            waitGenC(amazingFunction.mod()) { amazingFunction.waitGenC(it) }
            amazings.add(amazingFunction)
        }
        for (n in ns) {
            val amazingNamespace = AmazingNamespace(n!!, itms, st!!.pa)
            waitGenC(amazingNamespace.mod()) { amazingNamespace.waitGenC(it) }
            amazings.add(amazingNamespace)
        }

        for (amazing in amazings) {
            amazing.run()
        }
    }

    fun waitGenC(mod: OS_Module, cb: Consumer<GenerateC>) {
        st!!.pa.waitGenC(mod, cb)
    }

    fun pnPN_signalCalculateFinishParse() {
    }

    //@Override
    fun execute(aMonitor: CK_Monitor?): Operation<Ok>? {
        return null
    }

    fun interface WPIS_GenerateOutputs_Behavior_PrintDBLString {
        fun print(sps: String?)
    }

    internal interface Writable {
        fun filename(): EOT_FileNameProvider?

        fun inputs(): List<EIT_Input?>

        fun statement(): EG_Statement
    }

    internal class Default_WPIS_GenerateOutputs_Behavior_PrintDBLString : WPIS_GenerateOutputs_Behavior_PrintDBLString {
        override fun print(sps: String?) {
            SimplePrintLoggerToRemoveSoon.println_err_4(sps)
        }
    }

    // TODO 09/04 Duplication madness
    private class MyWritable(aEntry: MutableMap.MutableEntry<NG_OutputRequest, MutableCollection<EG_Statement>>) : Writable {
        val value: Collection<EG_Statement>
        val filename: EOT_FileNameProvider?
        val list: List<EG_Statement?>
        val statement: EG_SequenceStatement
        private val outputRequest: NG_OutputRequest?

        init {
            this.outputRequest = aEntry.key
            filename = outputRequest?.fileName()
            value = aEntry.value

            list = value.stream().toList()

            statement = mk_EG_SequenceStatement(EG_Naming("writable-combined-file"), list)
        }

        override fun filename(): EOT_FileNameProvider? {
            return filename
        }

        override fun inputs(): List<EIT_Input?> {
            checkNotNull(outputRequest) { "shouldn't be here" }

            val moduleInput = outputRequest.outputStatement().moduleInput

            return Helpers.List_of<EIT_Input?>(moduleInput)
        }

        override fun statement(): EG_Statement {
            return statement
        }
    }

    inner class OutputItems {
        val osg: OutputStrategy = st!!.sys!!.outputStrategyCreator.get()
        val outputStrategyC: OutputStrategyC = OutputStrategyC(osg)
        val ors1: MutableList<NG_OutputRequest> = ArrayList()
        val itms: MutableList<NG_OutputItem> = ArrayList()
        private var _readyCount = 0
        private var _addTally = 0

        fun addItem(aOutputItem: NG_OutputItem) {
            itms.add(aOutputItem)

            ++_addTally
            if (_addTally == _readyCount) {
                MyRunnable(this, this@WPIS_GenerateOutputs).run()
            }
        }

        fun readyCount(aI: Int) {
            this._readyCount = aI
        }
    }

    private class MyRunnable(private val outputItems: OutputItems,
                             aWPISGenerateOutputs: WPIS_GenerateOutputs) : Runnable {
        private val c: Compilation = aWPISGenerateOutputs.st!!.c

        override fun run() {
            for (o in outputItems.itms) {
                val oxs = o.outputs
                for (ox in oxs) {
                    val oxt = ox.ty
                    val oxb = ox.text
                    val s = o.outName(outputItems.outputStrategyC, oxt)
                    val or = NG_OutputRequest(s, ox, ox, o)

                    outputItems.ors1.add(or)
                }
            }

            val mfss: Multimap<NG_OutputRequest, EG_Statement> = ArrayListMultimap.create()
            val cot = c.outputTree
            val ce = c.compilationEnclosure

            for (or in outputItems.ors1) {
                ce.AssertOutFile(or)
            }

            // README combine output requests into file requests
            for (or in outputItems.ors1) {
                mfss.put(or, or.statement())
            }

            val writables: MutableList<Writable> = ArrayList()

            for (entry in mfss.asMap().entries) {
                writables.add(MyWritable(entry))
            }

            for (writable in writables) {
                val filename = writable.filename()!!.getFilename()
                val statement0 = writable.statement()
                val list2 = relist3_flatten(statement0)

                val statement = if (filename.endsWith(".h")) {
                    __relist3(list2)
                } else {
                    statement0
                }

                val off = EOT_OutputFileImpl(writable.inputs(), EOT_FileNameProvider__ofString(filename), EOT_OutputType.SOURCES, statement)
                cot.add(off)
            }
        }
    }
}

fun relist3_flatten(sequence: EG_Statement): List<EG_Statement> {
    val llll = ArrayList<EG_Statement>()

    if (sequence is EG_SequenceStatement) {
        llll.addAll(sequence._list())
    } else {
        llll.add(sequence)
    }

    return llll
}

fun __relist3(list2: List<EG_Statement>): EG_Statement {
    val uuid = "elinc_%s".formatted(UUID.randomUUID().toString().replace('-', '_'))
    val b_s = "#ifndef %s\n#define %s 1\n\n".formatted(uuid, uuid)
    val b = tripleo.elijah.nextgen.outputstatement.EG_Statement.Companion.of(b_s, withMessage("Header file prefix"))
    val e = tripleo.elijah.nextgen.outputstatement.EG_Statement.Companion.of("\n#endif\n", withMessage("Header file postfix"))
    val list3 = Helpers.__combine_list_elements(b, list2, e)
    val statement: EG_Statement = mk_EG_SequenceStatement(EG_Naming("relist3"), list3)
    return statement
}