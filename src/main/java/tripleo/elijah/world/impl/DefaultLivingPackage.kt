package tripleo.elijah.world.impl

import tripleo.elijah.lang.i.OS_Package
import tripleo.elijah.world.i.LivingPackage

class DefaultLivingPackage(private val _element: OS_Package) : LivingPackage {
    override fun getCode(): Int {
        return 0
    }

    override fun getElement(): OS_Package {
        return _element
    }
}
