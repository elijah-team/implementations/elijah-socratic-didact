/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jdeferred2.DoneCallback
import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.lang.i.Context
import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_ProcTableEntry
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.Ok
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.*

/**
 * Created 9/12/20 10:07 PM
 */
class ProcTableEntry(val index: Int,
                     /**
                      * Either a hint to the programmer-- The compiler should be able to work without
                      * this. <br></br>
                      * Or for synthetic methods
                      */
                     @JvmField val __debug_expression: IExpression?,
                     @JvmField val expression_num: InstructionArgument,
                     args: List<TypeTableEntry>) : BaseTableEntry(), TableEntryIV {
    private inner class _StatusListener_PTE_67 : StatusListener {
        override fun onChange( /* @NotNull */
                               eh: IElementHolder, newStatus: Status) {
            if (newStatus == Status.KNOWN) {
                resolvedElement = eh.element
            }
        }
    }

    enum class ECT {
        exp, exp_num
    }

    @JvmField
    var args: List<TypeTableEntry>? = null

    var evaExpression: EvaExpression<IExpression>? = null

    private val _p_completeDeferred = DeferredObject<Ok, Void, Void>()
    private val _p_onFunctionInvocations = DeferredObject2<FunctionInvocation, Void, Void>()
    var dpc: DeduceProcCall = DeduceProcCall(this)
    var expressionConfession: ExpressionConfession = ExpressionConfession { ECT.exp }
    private var _de3: DeduceElement3_ProcTableEntry? = null

    @JvmField
    var classInvocation: ClassInvocation? = null

    var functionInvocation: FunctionInvocation? = null
        // have no idea what this is for
        set(aFunctionInvocation) {
            if (field != aFunctionInvocation) {
                field = aFunctionInvocation
                _p_onFunctionInvocations.reset()
                _p_onFunctionInvocations.resolve(field!!)
            }
        }

    init {
        //		expressionConfession = ExpressionConfession.from(expression, expression_num);
        this.args = if (args.size == 0) {
            emptyList()
        } else {
            args
        }

        addStatusListener(_StatusListener_PTE_67())

        setupResolve()
        if (expression_num != null) {
            if (expression_num is IdentIA) {
                evaExpression = EvaExpression(__debug_expression as IdentExpression, expression_num.entry)
            } else if (expression_num is IntegerIA) {
                evaExpression = EvaExpression(__debug_expression as IdentExpression, expression_num.entry)
            } else if (expression_num is ProcIA) {
                evaExpression = EvaExpression(__debug_expression as IdentExpression, expression_num.entry)
            } else throw Error()
        } else {
            evaExpression = EvaExpression(__debug_expression as IdentExpression, this)!! // FIXME justify this
        }
    }

    fun _deduceTypes2(): DeduceTypes2 {
        return __dt2!!
    }

    fun _inj(): DeduceTypes2Injector {
        return _deduceTypes2()._inj()
    }

    private fun completeDeferred(): DeferredObject<Ok, Void, Void> {
        return _p_completeDeferred
    }

    fun deduceProcCall(): DeduceProcCall {
        return dpc
    }

    fun expressionConfession(): ExpressionConfession {
        if (expressionConfession == null) {
            expressionConfession = if (expression_num == null) {
                ExpressionConfession { ECT.exp }
            } else {
                ExpressionConfession { ECT.exp_num }
            }
        }

        return expressionConfession
    }

    val deduceElement3: DeduceElement3_ProcTableEntry
        get() {
            // assert dpc._deduceTypes2() != null; // TODO setDeduce... called; Promise?
            //
            // return getDeduceElement3(dpc._deduceTypes2(), dpc._generatedFunction());

            assert(__gf != null)
            assert(_deduceTypes2() != null)
            return getDeduceElement3(_deduceTypes2(), __gf!!)
        }

    fun getDeduceElement3(aDeduceTypes2: DeduceTypes2,
                          aGeneratedFunction: BaseEvaFunction): DeduceElement3_ProcTableEntry {
        if (_de3 == null) {
            _de3 = DeduceElement3_ProcTableEntry(this, aDeduceTypes2, aGeneratedFunction)
        }
        return _de3!!
    }

    fun getLoggingString(aDeduceTypes2: DeduceTypes2?): String {
        val pte_string: String
        val l: MutableList<String> = ArrayList()

        for (typeTableEntry in args!!) {
            val attached = typeTableEntry.attached

            if (attached != null) l.add(attached.toString())
            else {
                aDeduceTypes2?.LOG?.err("267 attached == null for $typeTableEntry")

                if (typeTableEntry.__debug_expression != null) l.add(String.format("<Unknown expression: %s>", typeTableEntry.__debug_expression))
                else l.add("<Unknkown>")
            }
        }

        val sb2 = "[" + Helpers.String_join(", ", l) + "]"
        pte_string = sb2
        return pte_string
    }

    // have no idea what this is for
    fun onFunctionInvocation(callback: DoneCallback<FunctionInvocation>) {
        _p_onFunctionInvocations.then(callback)
    }

    fun onSetAttached() {
        var state = 0
        if ( /* args != null || */args !== Collections.EMPTY_LIST) {
            val ac = args?.size
            var acx = 0
            for (tte in args!!) {
                if (tte.attached != null) acx++
            }
            if (acx < ac!!) {
                state = 1
            } else if (acx > ac) {
                state = 2
            } else if (acx == ac) {
                state = 3
            }
        } else {
            state = 3
        }
        when (state) {
            0 -> throw IllegalStateException()
            1 -> SimplePrintLoggerToRemoveSoon.println_err_2("136 pte not finished resolving $this")
            2 -> SimplePrintLoggerToRemoveSoon.println_err_2("138 Internal compiler error")
            3 -> if (_p_completeDeferred.isPending) _p_completeDeferred.resolve(Ok.instance())
            else -> throw NotImplementedException()
        }
    }

    fun resolveType(aResult: GenType) {
        if (typeDeferred().isResolved) {
            typeDeferred().reset() // !! 07/20
        }
        typeDeferred().resolve(aResult)
    }

    fun setArgType(aIndex: Int, aType: OS_Type?) {
        args!![aIndex].attached = aType
    }

    fun setDeduceTypes2(aDeduceTypes2: DeduceTypes2?, aContext: Context?,
                        aGeneratedFunction: BaseEvaFunction?, aErrSink: ErrSink?) {
        dpc.setDeduceTypes2(aDeduceTypes2, aContext, aGeneratedFunction, aErrSink)
    }


    override fun toString(): String {
        return ("ProcTableEntry{" + "index=" + index + ", expression=" + __debug_expression + ", expression_num="
                + expression_num + ", status=" + status + ", args=" + args + '}')
    }

    fun typeDeferred(): DeferredObject2<GenType, ResolveError, Void> {
        return typeResolve!!.deferred
    }

    fun typePromise(): Promise<GenType, ResolveError, Void> {
        return typeResolvePromise()
    } // public PTE_Zero zero() {
    // if (_zero == null)
    // _zero = new PTE_Zero(this);
    //
    // return _zero;
    // }
} //
//
//

