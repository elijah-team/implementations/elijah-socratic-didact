/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tripleo.elijah.nextgen.outputstatement

/**
 * @author Tripleo Nova
 */
data class EG_SingleStatement(
    override val text: String,
    override val explanation: EX_Explanation
) : EG_Statement {
    constructor(void: String) : this(void, EX_Explanation_withMessage("just make it compile"))

    fun rule(aS: String?, aI: Int): EG_SingleStatement {
        return this
    }

    fun tag(aS: String?, aI: Int): EG_SingleStatement {
        return this
    }
}
