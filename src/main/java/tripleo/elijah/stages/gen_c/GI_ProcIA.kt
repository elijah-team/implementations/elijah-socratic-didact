package tripleo.elijah.stages.gen_c

import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.ExceptionDiagnostic
import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.nextgen.outputstatement.EG_SingleStatement
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation_withMessage
import tripleo.elijah.stages.deduce.OnGenClass
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_ProcTableEntry
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.util.Operation2
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.text.MessageFormat
import java.util.function.Consumer
import tripleo.elijah.diagnostic.Diagnostic.withMessage as Diagnostic_withMessage

class GI_ProcIA(private val carrier: ProcIA, private val gc: GenerateC) : GenerateC_Item {
    private val pte = carrier.entry
    private var _evaNode: EvaNode? = null

    fun action_CONSTRUCT(aInstruction: Instruction,
                         gc: GenerateC): Operation2<EG_Statement> {
        val pte = carrier.entry
        val x = pte.args
        val y = aInstruction.argsSize

        //		InstructionArgument z = instruction.getArg(1);
        val clsinv = pte.classInvocation
        if (pte.__debug_expression is IdentExpression) {
            val ie = pte.__debug_expression as IdentExpression
            if (ie.getText() == "f") return Operation2.failure(ExceptionDiagnostic(Exception("pte.expression is f")))
        }

        if (clsinv != null) {
            val target = pte.expression_num

            //			final InstructionArgument value  = instruction;
            if (target is IdentIA) {
                // how to tell between named ctors and just a path?
                val target2 = target.entry
                val str = target2.ident.text

                SimplePrintLoggerToRemoveSoon.println_out_4("130  $str")
            }

            val s = MessageFormat.format("{0}{1};", Emit.emit("/*500*/"),
                    getAssignmentValue(aInstruction, gc))

            return Operation2.success(EG_SingleStatement(s, EX_Explanation_withMessage("aaa")))
        }

        return Operation2.failure(Diagnostic_withMessage("12900", "no construct possible for GI_Proc", Diagnostic.Severity.INFO))
    }

    fun getAssignmentValue(aInstruction: Instruction, gc: GenerateC): String? {
        val gf = carrier.generatedFunction()
        val clsinv = carrier.entry.classInvocation

        // return gc.getAssignmentValue(gf.getSelf(), aInstruction, clsinv, gf);
        val gav = gc.GetAssignmentValue()

        //		return gav.forClassInvocation(aInstruction, clsinv, gf, gc.LOG);
        val _arg0 = aInstruction.getArg(0)
        val pte = carrier.entry

        val reference = CtorReference()
        reference.getConstructorPath(pte.expression_num, gf)
        val ava = gav.getAssignmentValueArgs(aInstruction, gf, gc.LOG)
        val x = ava.stringList()
        reference.args(x)
        val build = reference.build(clsinv!!)
        return build
    }

    override fun getEvaNode(): EvaNode {
        return _evaNode!!
    }

    fun getIdentIAPath(addRef: Consumer<Pair<String, CReference.Ref>>): String? {
        return getIdentIAPath_Proc(carrier.entry, addRef)
    }

    fun getIdentIAPath_Proc(pte: ProcTableEntry,
                            addRef: Consumer<Pair<String, CReference.Ref>>): String? {
        val text = arrayOfNulls<String>(1)
        val fi = pte.functionInvocation

        if (fi == null) {
            SimplePrintLoggerToRemoveSoon.println_err_2("7777777777777777 fi getIdentIAPath_Proc $pte")

            return null // throw new IllegalStateException();
        }

        /* final */
        var generated = fi.generated
        val de_pte = pte.deduceElement3 as DeduceElement3_ProcTableEntry

        if (generated == null) {
            logProgress(6464, "" + fi.pte)

            val wlgf = WlGenerateCtor(
                    de_pte.deduceTypes2()!!.getGenerateFunctions(de_pte.principal.context.module()), fi, null,
                    de_pte.deduceTypes2()!!.phase.codeRegistrar)
            wlgf.run(null)
            generated = wlgf.result

            for (identTableEntry in generated?.idte_list!!) {
                identTableEntry._fix_table(de_pte.deduceTypes2(), de_pte.generatedFunction())
            }

            // throw new IllegalStateException();
        }

        if (generated is EvaConstructor) {
            val ac: WhyNotGarish_Constructor? = gc.a_lookup(generated)

            val constructorNameText = ac!!.constructorNameText

            generated.onGenClass(OnGenClass { genClass: EvaClass ->
                text[0] = String.format("ZC%d%s", genClass.code, constructorNameText)
                addRef.accept(Pair.of(text[0], CReference.Ref.CONSTRUCTOR))
            })
        } else {
            val functionName = generated.fd.nameNode
            generated.onGenClass { genClass: EvaClass ->
                text[0] = String.format("z%d%s", genClass.code, functionName.text)
                addRef.accept(Pair.of(text[0], CReference.Ref.FUNCTION))
            }
        }

        return text[0]
    }

    private fun logProgress(code: Int, message: String) {
        gc.ce.logProgress(CompProgress.GenerateC, Pair.of(code, message))
    }

    override fun setEvaNode(evaNode: EvaNode) {
        _evaNode = evaNode
    }
}
