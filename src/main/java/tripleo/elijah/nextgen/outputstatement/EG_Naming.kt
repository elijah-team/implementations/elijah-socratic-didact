package tripleo.elijah.nextgen.outputstatement

class EG_Naming {
    val s: String
    val s1: String?

    constructor(aS: String) {
        s = aS
        s1 = null
    }

    constructor(aS: String, aS1: String?) {
        s = aS
        s1 = aS1
    }
}
