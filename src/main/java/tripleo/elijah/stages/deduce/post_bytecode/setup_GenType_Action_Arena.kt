package tripleo.elijah.stages.deduce.post_bytecode

class setup_GenType_Action_Arena {
    private val arenaVars: MutableMap<String, Any> = HashMap()

    fun clear() {
        arenaVars.clear()
    }

    fun <T> get(a: String): T? {
        if (arenaVars.containsKey(a)) {
            return arenaVars[a] as T?
        }
        return null
    }

    fun <T> put(k: String, v: T) {
        arenaVars[k] = v!!
    }
}
