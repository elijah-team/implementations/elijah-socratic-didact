package tripleo.vendor.batoull22

import org.jetbrains.annotations.Contract
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.*

/**
 * @author tripleo
 */
class EK_Production @Contract(pure = true) constructor(val string: String) {
    private val ch: Array<EK_Fact?>?

    init {
        ch = ekFactarray.ch()
    }

    private val ekFactarray: EK_Factarray
        get() { // FIXME 10/18 too complicated
            val st = this.string

            val ch1: MutableList<EK_Fact> = ArrayList()
            for (c in st.toCharArray()) {
                ch1.add(EK_Fact(c))
            }

            val ch2: Array<EK_Fact?> = ch1.toTypedArray()
            logProgress(3737, "" + ch2)

            val result = EK_Factarray(st, ch2)
            return result
        }

    val merge: EK_Merge
        get() = EK_Merge(ch!![0], ch[2], ch[4])

    val push: EK_Push
        get() {
            val predicating = ch!![0]
            val resultant = ch[3]
            return EK_Push(predicating, resultant)
        }

    fun isMerge(): Boolean {
        return ch!!.size >= 5 // example A.B-->C;
    }

    fun isPush(): Boolean {
        return ch!!.size == 4 // example A-->C;
    }

    override fun toString(): String {
        return string
    }

    private fun logProgress(aI: Int, aList: String) {
        SimplePrintLoggerToRemoveSoon.println_out_4("$aI $aList")
    }
}
