/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp;

import tripleo.elijah.lang.i.OS_Module;
import tripleo.elijah.world.i.WorldModule;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import static tripleo.elijah.util.Helpers.List_of;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static com.google.common.truth.Truth.assertThat;

import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;

/**
 * @author Tripleo(envy)
 */
//@Disabled
public class CompilationTest {

	@Disabled @Test
	public final void testEz() {
//		final List<String>  args = List_of(, "-sE"/* , "-out" */);
		final ElijahTestCli c    = ElijahTestCli.createDefault();

		final ET_SourceRoot sr1 = c.addSourceRoot("test");
		c.addSourceDirectory("comp_test/main3", sr1);
		//c.addSourceDirectory("comp_test/main3"); // basically we want just one, and or pick the first one that matches

		c.addStageAssertion("E");

		c.feedCmdLine(null);

		CT_ResultFilter rfltr = null; // send them all!

		c.addResultSink(rslts -> {
			System.err.println("9998-4646 "+rslts.xxx());
		}, rfltr);

		c.triggerResults(); // maybe should not be necessary

		assertThat(c.getIO().recordedreads())
				.containsAtLeast(
						"test/comp_test/main3/main3.ez"
						, "test/comp_test/main3/main3.ez"
//						, "test/comp_test/main3/main3.elijah"
						, "test/comp_test/fact1.elijah"
				);

		assertThat(c.reports().getCodeInputs())
				.containsAtLeast(
						"test/comp_test/main3/main3.elijah"
						, "test/comp_test/main3/main3.elijah"
				);

		assertThat(c.instructionCount())
				.isGreaterThan(0);

		final Collection<WorldModule> worldModules = c.world().modules();

        for (WorldModule wm : worldModules) {
            final OS_Module mod = wm.module();
            SimplePrintLoggerToRemoveSoon.println_out_2(String.format("**48** %s %s", mod, mod.getFileName()));
        }

		assertThat(worldModules.size())
				.isEqualTo((3/*7*//* 12 */));

		SimplePrintLoggerToRemoveSoon.println_err_4("CompilationTest -- 53 " + worldModules.size());
        assertThat(worldModules.size())
				.isGreaterThan(2);
	}
}

//
//
//
