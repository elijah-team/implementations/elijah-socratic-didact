package tripleo.elijah.typinf

class Identifier_AST(val name: String) : AstNode() {
    override fun toString(): String {
        return name
    }
}
