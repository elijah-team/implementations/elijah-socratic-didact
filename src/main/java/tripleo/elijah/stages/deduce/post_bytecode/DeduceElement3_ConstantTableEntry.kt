package tripleo.elijah.stages.deduce.post_bytecode

import org.jetbrains.annotations.Contract
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.Context
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.FoundElement
import tripleo.elijah.stages.deduce.post_bytecode.DED.DED_CTE
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3.DeduceElement3_Kind
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.ConstantTableEntry
import tripleo.elijah.stages.gen_fn.GenType
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.util.NotImplementedException

class DeduceElement3_ConstantTableEntry @Contract(pure = true) constructor(private val principal: ConstantTableEntry) : IDeduceElement3 {
    var deduceTypes2: DeduceTypes2? = null
    @JvmField
    var diagnostic: Diagnostic? = null
    @JvmField
    var deduceElement3: IDeduceElement3? = null
    private val genType: GenType? = null
    var generatedFunction: BaseEvaFunction? = null
    @JvmField
    var osType: OS_Type? = null

    private fun _inj(): DeduceTypes2Injector {
        return deduceTypes2()._inj()
    }

    override fun deduceTypes2(): DeduceTypes2 {
        return deduceTypes2!!
    }

    override fun elementDiscriminator(): DED {
        return DED_CTE(principal)
    }

    override fun generatedFunction(): BaseEvaFunction {
        return generatedFunction!!
    }

    override fun genType(): GenType {
        return genType!!
    }

    override fun getPrincipal(): OS_Element {
//        return principal.deduceElement3.principal
        throw Error()
    }

    override fun kind(): DeduceElement3_Kind {
        return DeduceElement3_Kind.GEN_FN__CTE
    }

    override fun resolve(aContext: Context, aDeduceTypes2: DeduceTypes2) {
        // deduceTypes2.resolveIdentIA_(aContext, aIdentIA, generatedFunction,
        // aFoundElement);
        throw NotImplementedException()
        // careful with this
        // throw new UnsupportedOperationException("Should not be reached");
    }

    override fun resolve(aIdentIA: IdentIA, aContext: Context,
                         aFoundElement: FoundElement) {
        // FoundElement is the "disease"
        deduceTypes2!!.resolveIdentIA_(aContext, aIdentIA, generatedFunction!!, aFoundElement)
    }
}
