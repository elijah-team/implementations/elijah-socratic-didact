/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import org.jdeferred2.Promise
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.deduce.DeclAnchor.AnchorType
import tripleo.elijah.stages.deduce.tastic.FCA_Stop
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.DeferredObject2
import tripleo.elijah.stages.gen_fn.ProcTableEntry
import tripleo.elijah.util.Mode
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.ReadySupplier_1

/**
 * Created 11/30/21 11:56 PM
 */
class DeduceProcCall @Contract(pure = true) constructor(private val procTableEntry: ProcTableEntry) {
    inner class DeclTarget(private val element: OS_Element, aDeclAnchor: OS_Element,
                           aAnchorType: AnchorType) : DeduceElement {
        private val anchor: DeclAnchor = _g_deduceTypes2!!._inj().new_DeclAnchor(aDeclAnchor, aAnchorType)

        /**
         * $element(FunctionDef Directory.listFiles) is a $anchorType(MEMBER) of
         * anchorElement(ClassSt std.io::Directory) and invocation just happens to be
         * around (invocation.pte is the call site (MainLogic::main))
         *
         *
         * [:///./Screenshot-from-2023-08-13 12-25-11.png][file]
         */
        init {
            val invocation: IInvocation?
            when (aAnchorType) {
                AnchorType.VAR -> {
                    val v = _g_generatedFunction!!.getVar(element as VariableStatement)
                    if (v.declaredTypeIsEmpty()) {
                        SimplePrintLoggerToRemoveSoon.println_err_4("8787 declaredTypeIsEmpty for " + element.name)
                        throw FCA_Stop()
                    } else {
                        val normalTypeName = (element as VariableStatementImpl).typeName() as NormalTypeName
                        val lrl = normalTypeName.context.lookup(normalTypeName.name)
                        val classStatement = lrl.chooseBest(null) as ClassStatement?
                        val oi = DeduceTypes2.ClassInvocationMake
                                .withGenericPart(classStatement!!, null, normalTypeName, _g_deduceTypes2!!)

                        assert(oi.mode() == Mode.SUCCESS)
                        invocation = oi.success()
                    }
                }

                else -> {
                    if (element is FunctionDef) {
                        invocation = _g_generatedFunction!!.fi
                    } else {
                        var declaredInvocation: IInvocation? = _g_generatedFunction?.fi?.classInvocation
                        if (declaredInvocation == null) {
                            declaredInvocation = _g_generatedFunction?.fi?.namespaceInvocation
                        }
                        if (aAnchorType == AnchorType.INHERITED) {
                            assert(declaredInvocation is ClassInvocation)
                            invocation = _g_deduceTypes2!!._inj().new_DerivedClassInvocation(aDeclAnchor as ClassStatement,
                                    declaredInvocation as ClassInvocation?, ReadySupplier_1(_g_deduceTypes2!!))
                        } else {
                            invocation = declaredInvocation
                        }
                    }
                }
            }
            anchor.invocation = invocation
        }

        @Contract(pure = true)
        override fun declAnchor(): DeclAnchor {
            return anchor
        }

        @Contract(pure = true)
        override fun element(): OS_Element {
            return element
        }
    }

    private val _pr_target = DT_Resolvable11<DeduceElement?>()
    private val _p_targetP2 = DeferredObject2<DeduceElement, FCA_Stop, Void>()
    private var _g_context: Context? = null
    private var _g_deduceTypes2: DeduceTypes2? = null
    private var _g_errSink: ErrSink? = null
    private var _g_generatedFunction: BaseEvaFunction? = null

    var target: DeduceElement? = null

    init {
        procTableEntry.onFunctionInvocation { functionInvocation: FunctionInvocation ->
            functionInvocation.generatePromise().then { evaFunction: BaseEvaFunction ->
                val best = evaFunction.fd
                val anchorType = AnchorType.MEMBER
                val declAnchor = best.parent
                try {
                    target = procTableEntry._inj().new_DeclTarget(best, declAnchor, anchorType, this)
                    _pr_target.resolve(target)
                    _p_targetP2.resolve(target!!)
                } catch (aE: FCA_Stop) {
                    _p_targetP2.reject(aE)
                }
            }
        }
    }

    fun _generatedFunction(): BaseEvaFunction? {
        return _g_generatedFunction
    }

    fun setDeduceTypes2(aDeduceTypes2: DeduceTypes2?, aContext: Context?,
                        aGeneratedFunction: BaseEvaFunction?, aErrSink: ErrSink?) {
        _g_deduceTypes2 = aDeduceTypes2
        _g_context = aContext
        _g_generatedFunction = aGeneratedFunction
        _g_errSink = aErrSink
    }

    fun targetP(): DT_Resolvable11<DeduceElement?>? {
        return _pr_target
    }

    fun targetP2(): Promise<DeduceElement, FCA_Stop, Void>? {
        return _p_targetP2
    }
}
