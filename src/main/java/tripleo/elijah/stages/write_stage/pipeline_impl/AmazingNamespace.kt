package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.output.NG_OutputNamespace
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.write_stage.pipeline_impl.WPIS_GenerateOutputs.OutputItems

internal class AmazingNamespace(private val n: EvaNamespace, private val itms: OutputItems?,
                                aPa: IPipelineAccess?) : Amazing {
    private val mod = n.module()
    private val compilation = mod.compilation as Compilation

    fun mod(): OS_Module {
        return mod
    }

    fun waitGenC(ggc: GenerateC?) {
        val on = NG_OutputNamespace()
        on.setNamespace(compilation.world().getNamespace(n).garish, ggc)
        itms!!.addItem(on)
    }
}
