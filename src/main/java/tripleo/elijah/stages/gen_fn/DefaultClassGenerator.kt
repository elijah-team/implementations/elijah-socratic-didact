package tripleo.elijah.stages.gen_fn

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.DeducePhase
import tripleo.elijah.stages.deduce.DeducePhase.GeneratedClasses
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.NULL_DeduceTypes2
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.work.WorkList
import tripleo.elijah.work.WorkList__

class DefaultClassGenerator(private val deducePhase: DeducePhase) : IClassGenerator {
    // given

    // transitive
    private val cr = deducePhase.codeRegistrar

    // creating
    private val wl: WorkList = WorkList__()

    override fun getCodeRegistrar(): ICodeRegistrar {
        return cr
    }

    override fun getGeneratedClasses(): GeneratedClasses {
        return deducePhase.generatedClasses
    }

    override fun newFunctionInvocation(fd: FunctionDef, pte: ProcTableEntry,
                                       ci: ClassInvocation): FunctionInvocation {
        val fi = deducePhase.newFunctionInvocation(fd, pte, ci)
        return fi
    }

    override fun registerClassInvocation(cs: ClassStatement, className: String): ClassInvocation? {
        val ci = deducePhase.registerClassInvocation(cs, className, NULL_DeduceTypes2())
        return ci
    }

    override fun submitGenerateClass(ci: ClassInvocation, gf: GenerateFunctions) {
        wl.addJob(WlGenerateClass(gf, ci, deducePhase.generatedClasses, cr))
    }

    override fun submitGenerateFunction(fi: FunctionInvocation, gf: GenerateFunctions) {
        wl.addJob(WlGenerateFunction(gf, fi, cr))
    }

    override fun wl(): WorkList {
        return wl
    }
}
