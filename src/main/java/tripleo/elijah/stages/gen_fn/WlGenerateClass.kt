/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import lombok.Getter
import org.jdeferred2.Promise
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.DeducePhase.GeneratedClasses
import tripleo.elijah.stages.deduce.RegisterClassInvocation_env
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.util.Holder
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.work.WorkJob
import tripleo.elijah.work.WorkManager
import java.util.function.Consumer

/**
 * Created 5/16/21 12:41 AM
 */
class WlGenerateClass : WorkJob { //
    @Getter
    private val classStatement: ClassStatement

    // 24/01/04 back and forth
    @Getter
    val classInvocation: ClassInvocation

    // 24/01/04 back and forth
    @Getter
    val generateFunctions: GenerateFunctions
    private val coll: GeneratedClasses?
    private val __passthru_env: RegisterClassInvocation_env?
    private var _isDone = false
    private val cr: ICodeRegistrar

    private var Result: EvaClass? = null

    private var consumer: Consumer<EvaClass>? = null

    constructor(aGenerateFunctions: GenerateFunctions, aClassInvocation: ClassInvocation,
                coll: GeneratedClasses?, aCodeRegistrar: ICodeRegistrar) {
        classStatement = aClassInvocation.klass
        generateFunctions = aGenerateFunctions
        classInvocation = aClassInvocation
        this.coll = coll

        cr = aCodeRegistrar

        __passthru_env = null
    }

    constructor(aGenerateFunctions: GenerateFunctions, aClassInvocation: ClassInvocation,
                coll: GeneratedClasses?, aCodeRegistrar: ICodeRegistrar,
                aEnv: RegisterClassInvocation_env) {
        classStatement = aClassInvocation.klass
        generateFunctions = aGenerateFunctions
        classInvocation = aClassInvocation
        this.coll = coll

        cr = aCodeRegistrar

        __passthru_env = aEnv
    }

    override fun isDone(): Boolean {
        return _isDone
    }

    override fun run(aWorkManager: WorkManager?) {
        val resolvePromise = classInvocation.resolveDeferred()

        resolvePromise.then { x: EvaClass ->
            if (consumer != null) {
                consumer!!.accept(x)
            }
        }

        when (resolvePromise.state()) {
            Promise.State.PENDING -> {
                val kl = generateFunctions.generateClass(classStatement, classInvocation, __passthru_env)

                // kl.setCode(generateFunctions.module.getCompilation().nextClassCode());
                cr.registerClass1(kl)

                coll?.add(kl)

                resolvePromise.resolve(kl)
                Result = kl
            }

            Promise.State.RESOLVED -> {
                val hgc = Holder<EvaClass>()
                resolvePromise.then { el: EvaClass -> hgc.set(el) }
                Result = hgc.get()
            }

            Promise.State.REJECTED -> throw NotImplementedException()
        }
        _isDone = true
    }

    fun setConsumer(consumer: Consumer<EvaClass>?) {
        this.consumer = consumer
    }
}
//
//

