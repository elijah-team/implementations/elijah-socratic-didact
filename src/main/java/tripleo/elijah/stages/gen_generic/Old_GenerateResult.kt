/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_generic

import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.subjects.ReplaySubject
import io.reactivex.rxjava3.subjects.Subject
import tripleo.elijah.ci.LibraryStatementPart
import tripleo.elijah.stages.gen_c.OutputFileC
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.util.buffer.Buffer
import java.util.function.Consumer

/**
 * Created 4/27/21 1:11 AM
 */
class Old_GenerateResult : GenerateResult {
    val _res: MutableList<Old_GenerateResultItem> = ArrayList()
    private val _watchers: MutableList<IGenerateResultWatcher> = ArrayList()
    private val completedItems: Subject<GenerateResultItem> = ReplaySubject.create()
    private var bufferCounter = 0

    private var outputFiles: Map<String, OutputFileC>? = null
    private var _closed = false

    override fun add(b: Buffer, n: EvaNode, ty: TY, aLsp: LibraryStatementPart?,
                     d: Dependency) {
        check(!_closed) { "closed GenerateResult" }

        if (aLsp == null) {
            SimplePrintLoggerToRemoveSoon.println_err_3("*************************** buffer --> " + b.text)
            return
        }

        ++bufferCounter
        val item = Old_GenerateResultItem(ty, b, n, aLsp, d, bufferCounter)
        _res.add(item)

        _watchers.forEach(Consumer { w: IGenerateResultWatcher -> w.item(item) })
    }

    override fun addClass(ty: TY, aClass: EvaClass, aBuf: Buffer, aLsp: LibraryStatementPart) {
        add(aBuf, aClass, ty, aLsp, aClass.dependency)
    }

    override fun addConstructor(aEvaConstructor: EvaConstructor, aBuffer: Buffer, aTY: TY,
                                aLsp: LibraryStatementPart) {
        addFunction(aEvaConstructor, aBuffer, aTY, aLsp)
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tripleo.elijah.stages.gen_generic.GenerateResult#addFunction(tripleo.elijah.
	 * stages.gen_fn.BaseEvaFunction, tripleo.util.buffer.Buffer,
	 * tripleo.elijah.stages.gen_generic.Old_GenerateResult.TY,
	 * tripleo.elijah.ci.LibraryStatementPart)
	 */
    override fun addFunction(aGeneratedFunction: BaseEvaFunction, aBuffer: Buffer, aTY: TY,
                             aLsp: LibraryStatementPart) {
        add(aBuffer, aGeneratedFunction, aTY, aLsp, aGeneratedFunction.dependency)
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tripleo.elijah.stages.gen_generic.GenerateResult#additional(tripleo.elijah.
	 * stages.gen_generic.GenerateResult)
	 */
    override fun additional(aGenerateResult: GenerateResult) {
        // TODO find something better
        // results()
        _res.addAll(aGenerateResult.results())
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tripleo.elijah.stages.gen_generic.GenerateResult#addNamespace(tripleo.elijah.
	 * stages.gen_generic.Old_GenerateResult.TY,
	 * tripleo.elijah.stages.gen_fn.EvaNamespace, tripleo.util.buffer.Buffer,
	 * tripleo.elijah.ci.LibraryStatementPart)
	 */
    override fun addNamespace(ty: TY, aNamespace: EvaNamespace, aBuf: Buffer,
                              aLsp: LibraryStatementPart) {
        add(aBuf, aNamespace, ty, aLsp, aNamespace.dependency)
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tripleo.elijah.stages.gen_generic.GenerateResult#addWatcher(tripleo.elijah.
	 * stages.gen_generic.IGenerateResultWatcher)
	 */
    override fun addWatcher(w: IGenerateResultWatcher) {
        _watchers.add(w)
    }

    override fun close() {
        this._closed = true
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tripleo.elijah.stages.gen_generic.GenerateResult#completeItem(tripleo.elijah.
	 * stages.gen_generic.GenerateResultItem)
	 */
    override fun completeItem(aGenerateResultItem: GenerateResultItem) {
        completedItems.onNext(aGenerateResultItem)
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tripleo.elijah.stages.gen_generic.GenerateResult#observe(io.reactivex.rxjava3
	 * .core.Observer)
	 */
    override fun observe(obs: Observer<GenerateResultItem>) {
        for (item in results()) {
            obs.onNext(item)
        }

        obs.onComplete()
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see tripleo.elijah.stages.gen_generic.GenerateResult#outputFiles(java.util.
	 * function.Consumer)
	 */
    override fun outputFiles(cmso: Consumer<Map<String, OutputFileC>>) {
        cmso.accept(outputFiles!!)
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see tripleo.elijah.stages.gen_generic.GenerateResult#results()
	 */
    override fun results(): List<Old_GenerateResultItem> {
        return _res
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see tripleo.elijah.stages.gen_generic.GenerateResult#signalDone()
	 */
    override fun signalDone() {
        completedItems.onComplete()

        for (w in _watchers) {
            w.complete()
        }
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tripleo.elijah.stages.gen_generic.GenerateResult#signalDone(java.util.Map)
	 */
    override fun signalDone(aOutputFiles: Map<String, OutputFileC>) {
        outputFiles = aOutputFiles

        signalDone()
    }

    // region REACTIVE
    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tripleo.elijah.stages.gen_generic.GenerateResult#subscribeCompletedItems(io.
	 * reactivex.rxjava3.core.Observer)
	 */
    override fun subscribeCompletedItems(aGenerateResultItemObserver: Observer<GenerateResultItem>) {
        completedItems.subscribe(aGenerateResultItemObserver)
    } // endregion
} //
//
//

