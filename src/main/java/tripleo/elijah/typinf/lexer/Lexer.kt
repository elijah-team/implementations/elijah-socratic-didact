/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.typinf.lexer

import tripleo.elijah.typinf.lexer.Regex.MatchObject
import tripleo.elijah.util.Helpers

/**
 * A simple regex-based lexer/tokenizer.
 *
 *
 * See below for an example of usage.
 *
 *
 * Created 9/3/21 10:05 PM
 */
class Lexer(rules: List<List<String>>, skip_whitespace: Boolean) {
    private val group_type = HashMap<String?, String>()

    //		private final Regex regex;
    private val skip_whitespace: Boolean
    private val re_ws_skip: Regex

    // All the regexes are concatenated into a single one
    // with named groups. Since the group names must be valid
    // Python identifiers, but the token types used by the
    // user are arbitrary strings, we auto-generate the group
    // names and map them to token types.
    //
    private val regex_parts: MutableList<RegexPart> = Helpers.List_of()
    private var idx = 1
    private var pos = 0
    private var buf: String? = null

    /**
     * Create a lexer.
     *
     * @param rules           A list of rules. Each rule is a `regex, type`
     * pair, where `regex` is the regular expression used
     * to recognize the token and `type` is the type
     * of the token to return when it's recognized.
     * @param skip_whitespace If True, whitespace (\s+) will be skipped and not
     * reported by the lexer. Otherwise, you have to
     * specify your rules for whitespace, or it will be
     * flagged as an error.
     */
    init {
        for (r in rules) {
            val regex = r[0]
            val type = r[1]

            val groupname = String.format("GROUP%s", idx)
            //				regex_parts.add(String.format("(?<%s>%s)", groupname, regex));
            regex_parts.add(RegexPart(groupname, Regex.Companion.compile(regex)))
            group_type[groupname] = type
            idx += 1
        }

        //			this.regex = Regex.compile(Helpers.String_join("|", regex_parts));
        this.skip_whitespace = skip_whitespace
        this.re_ws_skip = Regex.Companion.compile("\\S")
    }

    /**
     * Initialize the lexer with a buffer as input.
     */
    fun input(aBuf: String?) {
        buf = aBuf
        pos = 0
    }

    @Throws(LexerError::class)
    fun token(): Token? {
        if (this.pos >= buf!!.length) return null
        else {
            if (this.skip_whitespace) {
                val m = re_ws_skip.search(this.buf, this.pos)

                if (m != null) this.pos = m.start()
                else return null
            }

            var lastgroup: String? = null
            var m: MatchObject? = null //this.regex.match(this.buf, this.pos);

            for (regex_part in regex_parts) {
                lastgroup = regex_part.groupname
                m = regex_part.regex.match(buf, pos)
                if (m != null) break
            }

            if (m != null) {
                val groupname = lastgroup
                //					String groupname = m.matcher.;
                val tok_type = group_type[groupname]
                val tok = Token(tok_type, m.string, this.pos)

                this.pos += m.end()
                return tok
            }

            // if we're here, no rule matched
            throw LexerError(this.pos)
        }
    }

    /**
     * Returns an iterator to the tokens found in the buffer.
     *
     * @return
     */
    @Throws(LexerError::class)
    fun tokens(): List<Token> {
        val r: MutableList<Token> = ArrayList()

        while (true) {
            val tok = this.token() ?: break
            //	yield tok;
            r.add(tok)
        }

        return r
    }
} //
//
//

