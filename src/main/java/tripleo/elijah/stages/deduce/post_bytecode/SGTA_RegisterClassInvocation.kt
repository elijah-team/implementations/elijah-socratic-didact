package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.stages.deduce.ClassInvocation
import tripleo.elijah.stages.deduce.DeducePhase
import tripleo.elijah.stages.deduce.NULL_DeduceTypes2
import tripleo.elijah.stages.gen_fn.GenType

class SGTA_RegisterClassInvocation(private val classStatement: ClassStatement, private val phase: DeducePhase) : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        // @Nullable ClassInvocation ci = _inj().new_ClassInvocation(classStatement,
        // null);
        var ci = ClassInvocation(classStatement, null, NULL_DeduceTypes2()) // !! 08/28
        ci = phase.registerClassInvocation(ci)

        arena.put("ci", ci)
    }
}
