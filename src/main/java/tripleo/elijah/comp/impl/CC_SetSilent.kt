package tripleo.elijah.comp.impl

import tripleo.elijah.comp.Compilation0
import tripleo.elijah.comp.i.CompilationChange

class CC_SetSilent(private val flag: Boolean) : CompilationChange {
    override fun apply(c: Compilation0) {
        c.cfg().setSilent(flag)
    }
}
