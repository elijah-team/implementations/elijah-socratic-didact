package tripleo.elijah.nextgen.inputtree

import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.g.GCompilationEnclosure
import tripleo.elijah.g.GEvaNode
import tripleo.elijah.g.GGenerateResult
import tripleo.elijah.g.GModuleGenerationRequest
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.work.WorkManager
import java.util.function.Consumer

interface ModuleGenerationRequest : GModuleGenerationRequest {
    fun baseNodes(): List<GEvaNode?>?

    fun work(): WorkManager?

    fun baseGenerate(): Consumer<GGenerateResult?>?

    fun baseEnclosure(): GCompilationEnclosure?


    val evaNodeList: List<EvaNode?>?

    val generateResultConsumer: Consumer<GenerateResult?>?

    val compilationEnclosure: CompilationEnclosure?
}
