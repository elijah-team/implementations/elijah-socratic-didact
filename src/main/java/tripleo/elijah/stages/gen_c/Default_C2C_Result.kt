package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EG_Statement.Companion.of
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.util.buffer.Buffer

internal class Default_C2C_Result(private val buffer: Buffer,
                                  private val _ty: TY,
                                  private val explanation_message: String,
                                  private val whyNotGarishFunction: WhyNotGarish_BaseFunction) : C2C_Result {
    private val module: OS_Module = whyNotGarishFunction.gf.module()

    private var _calculated = false
    private var _my_statement: EG_Statement? = null

    override fun getBuffer(): Buffer {
        return buffer
    }

    override fun getDefinedModule(): OS_Module {
        return module
    }

    override fun getStatement(): EG_Statement {
        if (!_calculated) {
            _my_statement = of(buffer.text, withMessage(explanation_message))
            _calculated = true
        }
        return _my_statement!!
    }

    override fun getWhyNotGarishFunction(): WhyNotGarish_BaseFunction {
        return whyNotGarishFunction
    }

    override fun ty(): TY {
        return _ty
    }
}
