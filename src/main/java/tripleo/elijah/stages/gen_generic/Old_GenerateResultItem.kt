/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 */
package tripleo.elijah.stages.gen_generic

import org.jetbrains.annotations.Contract
import tripleo.elijah.ci.LibraryStatementPart
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.util.buffer.Buffer
import java.util.stream.Collectors

/**
 * Created 4/27/21 1:12 AM
 */
class Old_GenerateResultItem @Contract(pure = true) constructor(@JvmField val ty: TY, val buffer: Buffer,
																val node: EvaNode, val lsp: LibraryStatementPart,
																private val dependency: Dependency, @JvmField val counter: Int) : GenerateResultItem {
    @JvmField
	var output: String? = null
    var outputFile: IOutputFile? = null

    override fun __ty(): TY {
        return ty
    }

    override fun _counter(): Int {
        return counter
    }

    override fun buffer(): Buffer {
        return this.buffer
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see tripleo.elijah.stages.gen_generic.GenerateResultItem#dependencies()
	 */
    override fun dependencies(): List<DependencyRef> {
        val x = dependency.notedDeps.stream()
            .map { dep: Dependency? -> dep?.dref }
            .filter { it != null }
            .map { it!! }
            .collect(Collectors.toList())

        return x
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see tripleo.elijah.stages.gen_generic.GenerateResultItem#getDependency()
	 */
    override fun getDependency(): Dependency {
        val ds = dependencies()
        return dependency
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see tripleo.elijah.stages.gen_generic.GenerateResultItem#jsonString()
	 */
    override fun jsonString(): String {
        val sb = ("{\".class\": \"GenerateResultItem\", " + "counter: " + counter + ", " + "ty: " + ty + ", "
                + "output: " + output + ", " + "outputFile: " + outputFile + ", " + "lsp: " + lsp + ", " + "node: "
                + node + ", " + "buffer: \"\"\"" + buffer.text + "\"\"\", " + "dependency: "
                + dependency.jsonString() + ", " + "dependencies: " + dependencies() +  /* +", " */
                "}")
        return sb
    }

    override fun lsp(): LibraryStatementPart {
        return this.lsp
    }

    override fun node(): EvaNode {
        return this.node
    }

    override fun output(): String {
        return output!!
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see tripleo.elijah.stages.gen_generic.GenerateResultItem#toString()
	 */
    override fun toString(): String {
        return ("GenerateResultItem{" + "counter=" + counter + ", ty=" + ty + ", buffer=" + buffer.text + ", node="
                + node.identityString() + ", lsp=" + lsp.dirName + ", dependency=" + dependency.jsonString()
                + ", output='" + output + '\'' + ", outputFile=" + outputFile + '}')
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

