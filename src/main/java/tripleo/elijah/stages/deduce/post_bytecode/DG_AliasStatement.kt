package tripleo.elijah.stages.deduce.post_bytecode

import org.jdeferred2.Promise
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.impl.AliasStatementImpl
import tripleo.elijah.stages.deduce.DeduceLookupUtils
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.ResolveError
import tripleo.elijah.util.NotImplementedException

class DG_AliasStatement(private val aliasStatement: AliasStatementImpl, private val __dt2: DeduceTypes2) : DG_Item {
    private val _resolvePromise = DeferredObject<DG_Item, ResolveError, Void>()
    private var _resolveStarted = false

    fun resolvePromise(): Promise<DG_Item, ResolveError, Void> {
        if (!_resolveStarted) {
            _resolveStarted = true
            try {
                val ra = DeduceLookupUtils._resolveAlias2(aliasStatement, __dt2)

                if (ra is ClassStatement) {
                    _resolvePromise.resolve(__dt2.DG_ClassStatement(ra as ClassStatement?))
                } else if (ra is FunctionDef) {
                    _resolvePromise.resolve(__dt2.DG_FunctionDef(ra as FunctionDef?))
                } else {
                    throw NotImplementedException()
                }
            } catch (aE: ResolveError) {
                _resolvePromise.reject(aE)
            }
        }
        return _resolvePromise
    }
}
