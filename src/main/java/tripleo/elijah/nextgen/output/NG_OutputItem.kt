package tripleo.elijah.nextgen.output

import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.stages.generate.OutputStrategyC

interface NG_OutputItem {
//    @JvmField
    val outputs: List<NG_OutputStatement>

    fun outName(aOutputStrategyC: OutputStrategyC, ty: TY): EOT_FileNameProvider
}
