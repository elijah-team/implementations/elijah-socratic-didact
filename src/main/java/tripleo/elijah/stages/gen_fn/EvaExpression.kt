package tripleo.elijah.stages.gen_fn

import tripleo.elijah.lang.i.Context
import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.lang.i.OS_Element2
import tripleo.elijah.lang.nextgen.names.i.EN_Name

// T is either a OS_Element or IExpression
data class EvaExpression<T>(val _t: T,
                            val entry: BaseTableEntry) {
    val name: EN_Name?
        get() {
            val resolvedElement = entry.resolvedElement
            when (resolvedElement) {
//                is null -> {}
                is OS_Element2 -> {return resolvedElement.enName}
            }
            return null
        }

    val context: Context
        get() = entry.resolvedElement?.context!!

    // i think this is it
    val text: String?
        get() {
            return when {
                _t is IdentExpression -> _t.text
                else -> null
            }
        }

    fun get(): T = _t
}
