package tripleo.elijah.stateful

abstract class _RegistrationTargetMixin {
    fun registerState(aState: State): State {
        if (!(registeredStates.contains(aState))) {
            registeredStates.add(aState)

            val id = registeredStates.indexOf(aState)

            aState.setIdentity(StateRegistrationToken(id))
            return aState
        }

        return aState
    }

    private val registeredStates: MutableList<State> = ArrayList()
}
