package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.NamespaceStatement
import tripleo.elijah.lang.i.PropertyStatement
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage

class GCS_Property_Get(private val p: PropertyStatement?) : EG_Statement {
    override val explanation: EX_Explanation
        get() = withMessage("GCS_Property_Get")

    override val text: String
        get() {
            val parent = p!!.parent

            val code = if (parent is ClassStatement) {
                -3
            } else if (parent is NamespaceStatement) {
                -3
            } else {
//				code = -1;
                throw IllegalStateException(
                        "PropertyStatement can't have other parent than ns or cls. " + parent!!.javaClass.name)
            }

            // TODO Don't know if get or set!
            val text2 = String.format("ZP%dget_%s", code, p.name())

            return text2
        }
}
