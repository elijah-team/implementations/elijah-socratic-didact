package tripleo.elijah.comp.impl

import org.eclipse.emf.ecore.xml.type.util.XMLTypeResourceImpl
import tripleo.elijah.comp.*
import tripleo.elijah.comp.i.CompilerController
import tripleo.elijah.comp.i.CompilerController.Con
import tripleo.elijah.comp.i.ICompilationAccess
import tripleo.elijah.comp.i.ICompilationBus
import tripleo.elijah.comp.i.OptionsProcessor
import tripleo.elijah.comp.i.extra.ICompilationRunner
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.comp.internal.CompilationRunner
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.process.CB_FindCIs
import tripleo.elijah.comp.process.CB_FindStdLibProcess
import tripleo.elijah.g.GCompilationEnclosure
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.DebugFlags

class DefaultCompilerController(private val ca3: ICompilationAccess3) : CompilerController {
    private var cb: ICompilationBus? = null
    private var inputs: List<CompilerInput>? = null // NullMarked explicitly can be
    private var c: Compilation? = null

    val con: Con?
        get() {
            if (Companion.con == null) {
                Companion.con = _DefaultCon()
            }
            return Companion.con
        }

    fun _setInputs(aCompilation: Compilation0?, aInputs: List<CompilerInput>?) {
        c = aCompilation as Compilation?
        assert(c === ca3.comp)
        inputs = aInputs
    }

    override fun setEnclosure(aCompilationEnclosure: GCompilationEnclosure) {
        val ce = aCompilationEnclosure as CompilationEnclosure
        _setInputs(ce.compilation, ce.compilerInput)
    }

    override fun printUsage() {
        SimplePrintLoggerToRemoveSoon.println_out_2("Usage: eljc [--showtree] [-sE|O] <directory or .ez file names>")
    }

    override fun processOptions(): Operation<Ok> {
        val op: OptionsProcessor = ApacheOptionsProcessor()
        val cio = CompilerInstructionsObserver(c!!)
        val compilationEnclosure = c!!.compilationEnclosure

        compilationEnclosure.provideCompilationAccess(c!!.con().createCompilationAccess())
        cb = c!!.con().createCompilationBus()
        compilationEnclosure.compilationBus = cb

        c!!.provideCio(null)

        return op.process(c, inputs, cb) // TODO 09/08 Make this more complicated
    }

    override fun runner() {
        runner(this.con!!)
    }

    fun hook(aCr: ICompilationRunner?) {
    }

    override fun runner(con: Con) {
        var eclipse: XMLTypeResourceImpl.DataFrame
        if (DebugFlags.CLOJURE_FLAG) c!!.____m()

        c!!._cis().subscribeTo(c!!)

        val ce = c!!.compilationEnclosure

        val compilationAccess = ce.compilationAccess
        ce.provideCompilationRunner { con.createCompilationRunner(compilationAccess) }
        val cr = ce.compilationRunner

        hook(cr)

        cb!!.add(CB_FindCIs(cr, inputs))
        cb!!.add(CB_FindStdLibProcess(ce, cr))

        (cb as DefaultCompilationBus?)!!.runProcesses()

        c!!.fluffy.checkFinishEventuals()
    }

    class _DefaultCon : Con {
        override fun createCompilationRunner(compilationAccess: ICompilationAccess): CompilationRunner {
            val crState = CR_State(compilationAccess)
            val cr = CompilationRunner(compilationAccess, crState)

            crState.setRunner(cr)

            return cr
        }
    }

    companion object {
        private var con: Con? = null
    }
}
