package tripleo.elijah.comp.chewtoy;

public interface Startable {
	void start();

	Thread stealThread();
}
