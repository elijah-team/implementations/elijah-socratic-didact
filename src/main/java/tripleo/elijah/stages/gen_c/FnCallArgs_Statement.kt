package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.nextgen.outputstatement.ReasonedStringListStatement
import tripleo.elijah.stages.gen_c.GenerateC.GetAssignmentValue
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.ProcTableEntry
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.util.Helpers

internal class FnCallArgs_Statement(private val generateC: GenerateC, private val getAssignmentValue: GetAssignmentValue,
                                    private val pte: ProcTableEntry, private val inst: Instruction, private val gf: BaseEvaFunction) : EG_Statement {
    override val explanation: EX_Explanation
        get() = withMessage("FnCallArgs_Statement")

    override val text: String
        get() {
            val z = ReasonedStringListStatement()

            // VERIFY computed. immediate
            val ptex = pte.__debug_expression as IdentExpression

            // VERIFY template usage
            z.append(ptex.text, "pte-expression")

            // VERIFY template push
            z.append(Emit.emit("/*671*/"), "emit-code")
            z.append("(", "open-brace")

            // VERIFY alias evaluation
            val ava = getAssignmentValue.getAssignmentValueArgs(inst, gf, generateC.LOG)
            val sll = ava!!.stringList()
            // VERIFY template usage
            z.append(Helpers.String_join(", ", sll!!), "get-assignment-value-args")

            // VERIFY template push
            z.append(")", "close-brace")

            // VERIFY EG_St: <here> && getText() -> <~>
            return z.text
        }
}
