package tripleo.elijah.nextgen.model

import tripleo.elijah.lang.i.ModuleItem

// namespace
// class
// alias
// import
fun interface SM_ModuleItem : SM_Node {
    fun _carrier(): ModuleItem
}
