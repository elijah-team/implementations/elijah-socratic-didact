package tripleo.elijah.stages.deduce.post_bytecode

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.NamespaceStatement
import tripleo.elijah.stages.deduce.DeducePhase
import tripleo.elijah.stages.gen_fn.GenType

class SGTA_RegisterNamespaceInvocation @Contract(pure = true) constructor(private val namespaceStatement: NamespaceStatement, private val phase: DeducePhase) : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        val nsi = phase.registerNamespaceInvocation(namespaceStatement)
        arena.put("nsi", nsi)
    }
}
