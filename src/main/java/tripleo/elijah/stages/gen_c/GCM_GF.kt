package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.RegularTypeName
import tripleo.elijah.lang.i.TypeName
import tripleo.elijah.lang.types.OS_GenericTypeNameType
import tripleo.elijah.stages.gen_fn.EvaContainerNC
import tripleo.elijah.stages.gen_fn.EvaFunction
import tripleo.elijah.stages.gen_fn.TypeTableEntry
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.stages.logging.ElLog

internal class GCM_GF(private val gf: EvaFunction, private val LOG: ElLog, private val gc: GenerateC) : GCM_D {
    override fun find_return_type(aGenerate_method_header__: Generate_Method_Header): String {
        val fd = gf.fd

        if (fd.returnType() is RegularTypeName) {
            val rtn = fd.returnType() as RegularTypeName
            val n: String = rtn.getName()
            if ("Unit" == n) {
                return "void" // why not??
            } else if ("SystemInteger" == n) {
                return "int" // FIXME doesn't seem like much to do, but
            }
        }

        var returnType: String? = null
        val tte: TypeTableEntry

        var result_index = gf.vte_lookup("Result")
        if (result_index == null) {
            // if there is no Result, there should be Value
            result_index = gf.vte_lookup("Value")
            // but Value might be passed in. If it is, discard value
            val vte = (result_index as IntegerIA?)!!.entry
            if (vte.vtt != VariableTableType.RESULT) result_index = null
            if (result_index == null) return "void" // README Assuming Unit
        }

        // Get it from resolved
        tte = gf.getTypeTableEntry((result_index as IntegerIA).index)
        val res = tte.resolved()
        if (res is EvaContainerNC) {
            val code: Int = res.code
            return String.format("Z%d*", code)
        }

        // Get it from type.attached
        val type = tte.attached

        LOG.info("228 $type")
        if (type == null) {
            // FIXME request.operation.fail(655) 06/16
            // as opposed to current-operation
            LOG.err("655 Shouldn't be here (type is null)")
            returnType = "ERR_type_attached_is_null/*2*/"
        } else if (type.isUnitType) {
            // returnType = "void/*Unit-74*/";
            returnType = "void"
        } else if (type != null) {
            if (type is OS_GenericTypeNameType) {
                val tn: TypeName = type.realTypeName

                val genericPart = gf.fi?.classInvocation!!.genericPart()

                val realType = genericPart.valueForKey(tn)!!

                returnType = String.format("/*267*/%s*", gc.getTypeName(realType))
            } else returnType = String.format("/*267-1*/%s*", gc.getTypeName(type))
        } else {
            throw IllegalStateException()
            //					LOG.err("656 Shouldn't be here (can't reason about type)");
//					returnType = "void/*656*/";
        }

        return returnType
    }
}
