package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.util.buffer.DefaultBuffer

class HashBufferList(string: String?) : DefaultBuffer(string)
