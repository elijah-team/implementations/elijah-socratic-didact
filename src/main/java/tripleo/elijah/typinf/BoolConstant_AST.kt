package tripleo.elijah.typinf

class BoolConstant_AST : AstNode {
    private val value: Boolean

    internal constructor(aValue: Boolean) {
        value = aValue
    }

    constructor(aValue: String) {
        value = aValue.toBoolean()
    }

    override fun toString(): String {
        return "" + value
    }
}
