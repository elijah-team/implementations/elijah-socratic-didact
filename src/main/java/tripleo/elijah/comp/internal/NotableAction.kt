package tripleo.elijah.comp.internal

import tripleo.elijah.comp.i.CB_Action
import tripleo.elijah.comp.i.CB_Monitor
import tripleo.elijah.comp.i.CB_OutputString
import tripleo.elijah.comp.notation.GN_Notable
import tripleo.elijah.util2.UnintendedUseException

internal class NotableAction(private val notable: GN_Notable) : CB_Action {
	val o: List<CB_OutputString> = ArrayList()

	fun _actual_run() {
		notable.run()
	}

	override fun execute(aMonitor: CB_Monitor) {
		if (false) throw UnintendedUseException("breakpoint")
		notable.run()
	}

	override fun name(): String {
		return "Notable wrapper"
	}
}
