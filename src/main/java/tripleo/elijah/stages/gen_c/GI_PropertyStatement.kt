package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.PropertyStatement
import tripleo.elijah.stages.gen_fn.EvaNode

internal class GI_PropertyStatement(private val _e: PropertyStatement, private val _epo: GI_Repo) : GenerateC_Item {
    private var _evaNaode: EvaNode? = null

    override fun getEvaNode(): EvaNode {
        return _evaNaode!!
    }

    override fun setEvaNode(aEvaNode: EvaNode) {
        _evaNaode = aEvaNode
    }
}
