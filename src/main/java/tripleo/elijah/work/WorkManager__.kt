/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.work

/**
 * Created 4/26/21 4:22 AM
 */
class WorkManager__ : WorkManager {
    var doneWork: MutableList<WorkList> = ArrayList()
    var jobs: MutableList<WorkList> = ArrayList()

    override fun addJobs(aList: WorkList) {
        jobs.add(aList)
    }

    override fun drain() {
        while (true) {
            val w = next() ?: break
            w.run(this)
        }
    }

    override fun next(): WorkJob? {
        val workListIterator = jobs.iterator()
        while (true) {
            if (workListIterator.hasNext()) {
                val workList = workListIterator.next()
                //			for (WorkList workList :jobs) {
                if (!workList.isDone) {
                    for (w in workList.jobs) {
                        if (!w.isDone) return w
                    }
                    workList.setDone()
                } else {
                    workListIterator.remove()
                    doneWork.add(workList)
                    return next()
                }
            } else return null
        }
        //		return null;
    }

    override fun totalSize(): Int {
//		final Integer x = jobs.stream().collect(new Collector<WorkList, List<Integer>, Integer>() {
//			final List<Integer> li = new ArrayList<Integer>();
//
//			@Override
//			public Supplier<List<Integer>> supplier() {
//				return () -> li;
//			}
//
//			@Override
//			public BiConsumer<List<Integer>, WorkList> accumulator() {
//				return (a, b) -> a.add(b.getJobs().size());
//			}
//
//			@Override
//			public BinaryOperator<List<Integer>> combiner() {
//				return null;
//			}
//
//			@Override
//			public Function<List<Integer>, Integer> finisher() {
//				return null;
//			}
//
//			@Override
//			public Set<Characteristics> characteristics() {
//				return Set.of(Characteristics.UNORDERED);
//			}
//		});

//		final int reduce = jobs.stream()
//				.reduce(0, (Integer a, WorkList b) -> {
//					return a + b.getJobs().size();
//				});

        var totalSize = 0
        for (job in jobs) {
            totalSize += job.jobs.size
        }
        return totalSize
    }
} //
//
//

