package tripleo.elijah.nextgen.outputstatement

data class EG_DottedStatement(private val separator: String,
                              private val stringList: List<String>,
                              override val explanation: EX_Explanation) : EG_Statement {
	override val text: String
		get() = String_join(separator, stringList)
}
