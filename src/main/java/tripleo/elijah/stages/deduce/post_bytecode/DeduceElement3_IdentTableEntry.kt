package tripleo.elijah.stages.deduce.post_bytecode

import org.jdeferred2.DoneCallback
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Finally.Outs
import tripleo.elijah.contexts.IClassContext
import tripleo.elijah.contexts.ModuleContext
import tripleo.elijah.contexts.ModuleContext__
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.BaseFunctionDef
import tripleo.elijah.lang.imports.NormalImportStatement
import tripleo.elijah.lang.nextgen.names.i.EN_Name
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.nextgen.DR_Ident
import tripleo.elijah.stages.deduce.nextgen.DR_PossibleType
import tripleo.elijah.stages.deduce.post_bytecode.DED.DED_ITE
import tripleo.elijah.stages.deduce.post_bytecode.IDeduceElement3.DeduceElement3_Kind
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.stateful.DefaultStateful
import tripleo.elijah.stateful.State
import tripleo.elijah.stateful.StateRegistrationToken
import tripleo.elijah.stateful._RegistrationTargetMixin
import tripleo.elijah.util.Mode
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.Operation2
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

class DeduceElement3_IdentTableEntry @Contract(pure = true) constructor(@JvmField val principal: IdentTableEntry) : DefaultStateful(), IDeduceElement3 {
    internal class CheckEvaClassVarTable : State {
        private var identity: StateRegistrationToken? = null

        override fun apply(element: DefaultStateful) {
            val ite_de = (element as DeduceElement3_IdentTableEntry)
            val identTableEntry = ite_de.principal

            identTableEntry.backlinkSet().then(object : DoneCallback<InstructionArgument?> {
                override fun onDone(backlink0: InstructionArgument?) {
                    val backlink: BaseTableEntry?

                    if (backlink0 is IdentIA) {
                        backlink = backlink0.entry
                        setBacklinkCallback(backlink)
                    } else if (backlink0 is IntegerIA) {
                        backlink = backlink0.entry
                        setBacklinkCallback(backlink)
                    } else if (backlink0 is ProcIA) {
                        backlink = backlink0.entry
                        setBacklinkCallback(backlink)
                    } else backlink = null
                }

                fun setBacklinkCallback(backlink: BaseTableEntry) {
                    if (backlink is ProcTableEntry) {
                        backlink.typeResolvePromise().done { result: GenType ->
                            val de3_ite = identTableEntry.deduceElement3
                            if (result.ci == null && result.node == null) result.genCIForGenType2(de3_ite.deduceTypes2())
                            for (entry in (result.node as EvaContainerNC).varTable) {
                                if (!entry.isResolved) {
                                    SimplePrintLoggerToRemoveSoon.println_err_4("629 entry not resolved " + entry.nameToken)
                                }
                            }
                        }
                    }
                }
            })
        }

        override fun checkState(aElement3: DefaultStateful): Boolean {
            return true
        }

        override fun setIdentity(aId: StateRegistrationToken) {
            identity = aId
        }
    }

    inner class DE3_EH_GroundedVariableStatement(private val element: VariableStatement,
                                                 @JvmField val ground: DeduceElement3_IdentTableEntry) : IElementHolder {
        private val actions: MutableList<setup_GenType_Action> = ArrayList()

        fun commitGenTypeActions() {
            val arena = setup_GenType_Action_Arena()

            if (principal.type == null) {
                principal.type = generatedFunction!!.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null)
            }
            if (genType == null) {
                genType = principal.type!!.genType
            }

            for (action in actions) {
                action.run(principal.type!!.genType, arena)
            }
        }

        fun genTypeAction(aSGTASetResolvedClass: SGTA_SetResolvedClass) {
            actions.add(aSGTASetResolvedClass)
        }

        override fun getElement(): VariableStatement {
            return element
        }
    }

    inner class DE3_ITE_Holder(private val element: OS_Element) : IElementHolder {
        private val actions: MutableList<setup_GenType_Action> = ArrayList()

        fun commitGenTypeActions() {
            val arena = setup_GenType_Action_Arena()

            if (principal.type == null) {
                principal.type = generatedFunction!!.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, null)
            }
            if (genType == null) {
                genType = principal.type!!.genType
            }

            for (action in actions) {
                action.run(principal.type!!.genType, arena)
            }
        }

        fun genTypeAction(aSGTASetResolvedClass: SGTA_SetResolvedClass) {
            actions.add(aSGTASetResolvedClass)
        }

        override fun getElement(): OS_Element {
            return element
        }
    }

    enum class ST {
        ;

        companion object {
            @JvmField
            var EXIT_GET_TYPE: State? = null

            @JvmField
            var CHECK_EVA_CLASS_VAR_TABLE: State? = null

            @JvmStatic
            fun register(aRegistrationTarget: _RegistrationTargetMixin) {
                EXIT_GET_TYPE = aRegistrationTarget.registerState(ExitGetType())
                CHECK_EVA_CLASS_VAR_TABLE = aRegistrationTarget.registerState(CheckEvaClassVarTable())
            }
        }
    }

    @JvmField
    var deduceTypes2: DeduceTypes2? = null

    @JvmField
    var generatedFunction: BaseEvaFunction? = null
    private var _resolved: GenType? = null

    private val _type: DeduceElement3_Type = object : DeduceElement3_Type {
        @Contract(pure = true)
        override fun genType(): GenType {
            return typeTableEntry()?.genType!!
        }

        override fun resolved(ectx: Context): Operation2<GenType> {
            try {
                if (_resolved == null) {
                    _resolved = deduceTypes2!!.resolve_type(genType().typeName, ectx)

                    typeTableEntry()?.setAttached(_resolved)
                }

                return Operation2.success(_resolved)
            } catch (aResolveError: ResolveError) {
                return Operation2.failure(aResolveError)
            }
        }

        @Contract(pure = true)
        override fun typeTableEntry(): TypeTableEntry? {
            return principal.type
        }
    }

    @JvmField
    var context: Context? = null

    @JvmField
    var fdCtx: Context? = null

    private var genType: GenType? = null

    fun _ctxts(aFdCtx: Context?, aContext: Context?) {
        fdCtx = aFdCtx
        context = aContext
    }

    fun assign_type_to_idte(aFunctionContext: Context, aContext: Context) {
        ExitGetType().assign_type_to_idte(principal, generatedFunction!!, aFunctionContext, aContext, deduceTypes2!!,
            deduceTypes2!!._phase())
    }

    fun backlinkPte(classStatement: ClassStatement, ignoredPte: ProcTableEntry?,
                    __eh: IElementHolder) {
        // README classStatement [T310-231]

        // README setStause on callablePTE and principal

        val principal_ident = principal.ident
        val principal_ident_name = principal_ident.get().name
        val text = principal_ident.text

        val dt2 = principal._deduceTypes2()
        val lrl = classStatement.context.lookup(text!!)
        val e = lrl?.chooseBest(null)

        if (e != null) {
            if (e is FunctionDef) {
                principal_ident_name.addUnderstanding(dt2._inj().new_ENU_FunctionName())
                principal_ident_name.addUnderstanding(dt2._inj().new_ENU_ResolveToFunction(e))

                // FIXME move this to constructor i guess
                val ename: EN_Name = e.nameNode.name
                ename.addUnderstanding(dt2._inj().new_ENU_FunctionName())
                ename.addUsage(dt2._inj().new_EN_NameUsage(principal_ident_name, this))
            } else if (e is VariableStatement) {
                principal_ident_name.addUnderstanding(dt2._inj().new_ENU_VariableName())

                // principal_ident_name.addUnderstanding(_inj().new_ENU_ResolveToFunction(fd));
                val ename: EN_Name = e.nameToken.name
                // ename.addUnderstanding(_inj().new_ENU_FunctionName());
                ename.addUsage(dt2._inj().new_EN_NameUsage(principal_ident_name, this))
            } else if (e is PropertyStatement) {
                NotImplementedException.raise()
            } else {
                assert(false)
            }
        }

        val callablePTE = principal.callablePTE
        if (callablePTE != null && e != null) {
            assert(e is BaseFunctionDef // sholud fail for constructor and destructor
            )
            callablePTE.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(e))
        }

        if (principal.status == BaseTableEntry.Status.UNCHECKED) {
            val e2 = __eh.element!!
            // assert e != null;
            if (e is VariableStatement) {
                principal.setStatus(BaseTableEntry.Status.KNOWN,
                    dt2._inj().new_DE3_EH_GroundedVariableStatement(e as VariableStatement?, this))
            } else if (e is FunctionDef) {
                principal.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(e))
            }
        }

        if (principal.status == BaseTableEntry.Status.KNOWN) {
            principal.__gf?.getIdent(principal)?.resolve()
        }
    }

    fun dan(generatedFunction: BaseEvaFunction, instruction: Instruction,
            aContext: Context, vte: VariableTableEntry, identIA: IdentIA,
            idte: IdentTableEntry, aDeduceTypes2: DeduceTypes2) {
        assert(idte === principal)
        if (idte.type == null) {
            aDeduceTypes2.resolveIdentIA_(aContext, identIA, generatedFunction, object : FoundElement(aDeduceTypes2.phase) {
                override fun foundElement(e: OS_Element?) {
                    aDeduceTypes2.found_element_for_ite(generatedFunction, idte, e, aContext, aDeduceTypes2.central())
                    assert(idte.hasResolvedElement())
                    vte.addPotentialType(instruction.index, idte.type!!)
                }

                override fun noFoundElement() {
                    // TODO: log error
                    val y = 2
                }
            })
        }
    }

    override fun deduceTypes2(): DeduceTypes2 {
        return deduceTypes2!!
    }

    override fun elementDiscriminator(): DED {
        return DED_ITE(principal)
    }

    override fun generatedFunction(): BaseEvaFunction {
        return generatedFunction!!
    }

    override fun genType(): GenType {
        if (genType == null) {
            genType = GenTypeImpl() // _inj().new_GenTypeImpl();
        }
        return genType!!
        // return principal.type.genType;
    }

    override fun getPrincipal(): OS_Element {
        return principal.getDeduceElement3(deduceTypes2, generatedFunction).getPrincipal()
    }

    val resolved: EvaNode?
        get() {
            var R: EvaClass? = null
            val ectx = principal.ident.get().context

            if (_type.typeTableEntry().genType.typeName == null) {
                // README we don't actually care about the typeName, we just
                // wanted to use it to recreate a GenType, where we can then
                // extract .node

                // _type.typeTableEntry().genType.typeName = null;
                // throw new IllegalStateException("Error");
            }

            if (principal.resolvedElement is ClassStatement) {
                // README but skip this and get the evaClass saved from earlier to
                // Grande [T168-089] when all these objects are being created and
                // manipulated (dern video yttv)
                val dcs = principal._deduceTypes2()
                    .DG_ClassStatement(principal.resolvedElement as ClassStatement?)

                // README fixup GenType
                // Still ignoring TypeName and nonGenericTypeName
                // b/c only client is in gen_c, not deduce
                val gt1 = _type.typeTableEntry().genType
                // gt1.typeName
                gt1.ci = dcs.classInvocation()
                gt1.node = dcs.evaClass()
                gt1.functionInvocation = dcs.functionInvocation()

                return dcs.evaClass()
            }

            val cc = deduceTypes2!!._phase()._compilation()
            if (cc.reports().outputOn(Outs.OutDeduce_369)) { // damn your fine
                // README to "prosecute" this we need a ContextImpl. But where to get
                // it from? And can we #resolveTypeToClass with `dcs' above?
                // Technically, there is one (more) above, but this line does not
                // produce results.
                val or = _type.resolved(ectx)
                if (or.mode() == Mode.SUCCESS) {
                    R = or.success().resolved as EvaClass
                }

                assert(R != null)
                principal.resolveTypeToClass(R!!)
            }
            return R
        }

    override fun kind(): DeduceElement3_Kind {
        return DeduceElement3_Kind.GEN_FN__ITE
    }

    // @NotNull final GenType xx = // TODO xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    override fun resolve(aContext: Context, aDeduceTypes2: DeduceTypes2) {
        // deduceTypes2.resolveIdentIA_(aContext, aIdentIA, generatedFunction,
        // aFoundElement);
        throw NotImplementedException()
        // careful with this
        // throw new UnsupportedOperationException("Should not be reached");
    }

    override fun resolve(aIdentIA: IdentIA, aContext: Context,
                         aFoundElement: FoundElement) {
        // FoundElement is the "disease"
        deduceTypes2!!.resolveIdentIA_(aContext, aIdentIA, generatedFunction!!, aFoundElement)
    }

    fun resolve1(ite: IdentTableEntry, aContext: Context): Operation2<GenType> {
        // FoundElement is the "disease"
        return try {
            Operation2.success(deduceTypes2!!.resolve_type(ite.type?.attached!!, aContext))
        } catch (aE: ResolveError) {
            Operation2.failure(aE)
        }
    }

    fun setDeduceTypes(aDeduceTypes2: DeduceTypes2?, aGeneratedFunction: BaseEvaFunction?) {
        assert(principal.__gf != null)
        assert(principal._deduceTypes2() != null)
        deduceTypes2 = aDeduceTypes2
        generatedFunction = aGeneratedFunction
    }

    private fun sneak_el_null__bl1_not_null(ident: IdentExpression, elx: Array<OS_Element?>,
                                            el: OS_Element?, bl1: InstructionArgument): OS_Element? {
        var el = el
        if (bl1 is IntegerIA) {
            val vte_bl1: VariableTableEntry = bl1.entry

            // get DR
            val x = generatedFunction!!.getIdent(ident, vte_bl1)
            x.foo()

            // get backlink
            val b = generatedFunction!!.getIdent(vte_bl1)
            b.foo()

            // set up rel
            b.onPossibleType { aPt: DR_PossibleType? -> x.proposeType(aPt) }

            run {
                val dt2 = principal._deduceTypes2()
                //assert dt2 != null;
                __sneak_el_null__bl1_not_null__possible_type_part(vte_bl1, dt2, b)
            }

            val tr = vte_bl1.typeResolve()

            el = __sneak_el_null__bl1_not_null__resolve_part(ident, elx, el, vte_bl1, tr)
            val y = 2
        } else if (bl1 is ProcIA) {
            val pte1: ProcTableEntry = bl1.entry

            val yyy = 2
            val yyyy = yyy + 1

            val de3: IDeduceElement3 = pte1.deduceElement3
        }
        return el
    }

    private fun __sneak_el_null__bl1_not_null__possible_type_part(vte_bl1: VariableTableEntry,
                                                                  dt2: DeduceTypes2?,
                                                                  b: DR_Ident) {
        for (potentialType in vte_bl1.potentialTypes()) {
            val y = potentialType.tableEntry
            if (y is ProcTableEntry) {
                // var z = pte.__debug_expression;
                // var zz = generatedFunction.getProcCall(z, pte);

                if (y.status == BaseTableEntry.Status.KNOWN) {
                    val ci: ClassInvocation = y.classInvocation!!
                    val fi: FunctionInvocation = y.functionInvocation!!

                    // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("322 " + ci);
                    // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("323 " + fi);
                    if (dt2 != null) {
                        val pt = dt2._inj().new_DR_PossibleTypeCI(ci, fi)
                        b.addPossibleType(pt)
                    } else {
                        SimplePrintLoggerToRemoveSoon.println_out_4("460460 Can't create PossibleType because dt2 == null")
                    }

                    // deduceTypes2.deducePhase.reg

                    // README taking a chance here
                    val eef = deduceTypes2!!.creationContext().makeGenerated_fi__Eventual(fi)

                    eef.then(object : DoneCallback<BaseEvaFunction> {
                        private fun printString(code: Int, txt: String) {
                            //if (code == 330)
                            //	return;
                            System.err.printf("130522 %d %s%n", code, txt)
                            if (code == 336) printString(336, "********************")
                        }

                        override fun onDone(gf: BaseEvaFunction) {
                            printString(330, "" + gf)

                            val ret = (gf.vte_lookup("Result"))
                            if (ret is IntegerIA) {
                                val retvte: VariableTableEntry = ret.entry
                                retvte.typeResolvePromise().then { gt: GenType ->
                                    printString(336, "" + gt)
                                    System.exit(336)
                                }
                                val retvtept = retvte.potentialTypes()
                                for (typeTableEntry in retvtept) {
                                }

                                val retvtety = retvte.typeTableEntry
                                if (retvtety.attached != null) {
                                    val att = retvtety.attached
                                    val resl = att!!.resolve(principal.ident.get().context)

                                    if (resl != null) {
                                        val ci11 = deduceTypes2!!.phase
                                            .registerClassInvocation(resl.classOf)

                                        printString(350, "" + resl)
                                        if (dt2 != null) {
                                            val pt2 = dt2._inj().new_DR_PossibleTypeCI(ci11, null)
                                            b.addPossibleType(pt2)
                                        } else {
                                            SimplePrintLoggerToRemoveSoon.println_out_4("460507 Can't create PossibleType because dt2 == null")
                                        }
                                    } else {
                                        // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("364 " + principal.getIdent().getText());
                                    }
                                }
                            }
                        }
                    })
                }
            }
        }
    }

    private fun sneak_el_null__bl1_null(ident: IdentExpression, el: OS_Element?): OS_Element? {
        var el = el
        val dt2 = principal._deduceTypes2()!!
        val ctxs: MutableList<Context?> = ArrayList()

        var ctx2 = principal.ident.get().context
        var f = true
        while (f) {
            if (ctxs.contains(ctx2)) {
                f = false
                continue
            }
            if (ctx2 == null) {
                f = false
                continue
            }

            if (ctx2 is ModuleContext__) {
                ctxs.add(ctx2)

                val itms = (ctx2 as ModuleContext).carrier.items
                for (moduleItem in itms) {
                    if (moduleItem is NormalImportStatement) {
                        ctx2 = moduleItem.myContext()

                        val lrl2 = ctx2.lookup(ident.text)
                        el = lrl2.chooseBest(null)

                        if (el != null) {
                            f = false
                            break
                        }
                    }
                }

                ctx2 = ctx2!!.parent
            } else {
                ctxs.add(ctx2)
                ctx2 = ctx2.parent
            }
        }
        return el
    }

    fun sneakResolve() {
        val ident = principal.ident
        val ctx = ident.context

        val lrl = ident.get().lookup(ctx)
        val elx = arrayOf<OS_Element?>(null)
        var el = lrl.chooseBest(null)

        if (el != null) {
            val dt2 = principal._deduceTypes2()!!
            ident.name?.addUnderstanding(dt2._inj().new_ENU_LookupResult(lrl)) // [T267665]
        }

        if (el == null) {
            val bl1 = principal.backlink
            el = if (bl1 != null) {
                sneak_el_null__bl1_not_null(ident.get(), elx, el, bl1)
            } else {
                sneak_el_null__bl1_null(ident.get(), el)
            }

            if (el == null) {
                //////////////
                //////////////
                //////////////
                ////////////// calculate
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                return  /// throw new IllegalStateException("Error");
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
                //////////////
            }
        }

        if (principal.callablePTE != null) {
            val callable = principal.callablePTE!!

            var dt2 = callable._deduceTypes2()
            var gf = callable.__gf

            if (dt2 == null) dt2 = principal._deduceTypes2()
            if (gf == null) gf = principal._generatedFunction()

            val b = callable.getDeduceElement3(dt2!!, gf).sneakResolve_IDTE(el, this)

            if (b) return
        }

        val dt2 = principal._deduceTypes2()!!
        if (el is IdentExpression) {
            if (el.text == ident.text) {
                val de3_ite_holder = dt2._inj().new_DE3_ITE_Holder(el, this)
                principal.setStatus(BaseTableEntry.Status.KNOWN, de3_ite_holder)
                de3_ite_holder.commitGenTypeActions()
            }
        } else if (el is VariableStatement) {
            if (el.name == ident.text) {
                val de3_ite_holder = dt2._inj().new_DE3_ITE_Holder(el,
                    this@DeduceElement3_IdentTableEntry)
                principal.setStatus(BaseTableEntry.Status.KNOWN, de3_ite_holder)
                de3_ite_holder.commitGenTypeActions()
            }
        } else {
            System.err.printf("DeduceElement3_IdentTableEntry >> cant sneakResolve %s based on %s%n", ident.text,
                "" + el /* ((IdentExpression)el).getText() */)
        }
    }

    fun type(): DeduceElement3_Type {
        return _type
    }

    companion object {
        private fun __sneak_el_null__bl1_not_null__resolve_part(ident: IdentExpression, elx: Array<OS_Element?>, el: OS_Element?, vte_bl1: VariableTableEntry, tr: DeduceTypeResolve): OS_Element? {
            var el = el
            if (vte_bl1.typeDeferred_isResolved()) {
                vte_bl1.typePromise().then { type1: GenType ->
                    if (tr.typeResolution().isPending) {
                        if (!type1.isNull) {
                            tr.typeResolve(type1)
                        }
                    }
                    if (type1.resolved != null) {
                        val ctx2: Context = type1.resolved.classOf.context
                        val lrl2 = ctx2.lookup(ident.text)
                        elx[0] = lrl2.chooseBest(null)
                    }
                }

                el = elx[0]
            } else {
                vte_bl1.dlv.resolve_var_table_entry_for_exit_function()

                // assert vte_bl1.typeDeferred_isResolved();
                if (vte_bl1.typeDeferred_isResolved()) {
                    el = vte_bl1.resolvedElement
                    assert(el != null)
                }
            }
            return el
        }
    }
}

private fun IClassContext.lookup(text: IdentExpression): LookupResultList? {
    return lookup(text.text)
}
