package tripleo.elijah.comp.internal

import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.InstructionDoer
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.comp.i.extra.CompilerInputListener
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.inputs.CompilerInput.CompilerInputField
import tripleo.elijah.comp.inputs.ILazyCompilerInstructions_.of
import tripleo.elijah.util.Mode
import tripleo.elijah.util2.DebugFlags
import tripleo.elijah.util2.UnintendedUseException

class CCI_Acceptor__CompilerInputListener(aCompilation: CompilationImpl) : CompilerInputListener {
    private val compilation: Compilation = aCompilation

    val id: InstructionDoer = InstructionDoer(aCompilation)

    //	private final ICompilationRunner cr;
    //	private       CCI           cci;
    //	private       IProgressSink _progressSink;
    init {
        /*
		cr = compilation.getCompilationEnclosure().getCompilationRunner();
*/
    }

    fun _root(): CompilerInstructions {
        return id.root!!
    }

    override fun change(i: CompilerInput, field: CompilerInputField) {
        if (compilation.compilerInputListener is CCI_Acceptor__CompilerInputListener) {
            if (DebugFlags.CCI_gate) {
                /*
				if (cci == null) {
					cci = new DefaultCCI(compilation, compilation._cis(), _progressSink);
				}
				if (_progressSink == null) {
					_progressSink = compilation.getCompilationEnclosure().getCompilationBus().defaultProgressSink();
				}
				cci_listener.set(cci, _progressSink);
*/
            }
        }

        val inputTree = compilation.inputTree

        compilation.compilationEnclosure.logProgress(CompProgress.__CCI_Acceptor__CompilerInputListener__change__logInput, i)

        when (field) {
            CompilerInputField.TY -> {
                when (i.ty()) {
                    CompilerInput.Ty.NULL -> {
                        val y2 = 2
                        // inputTree.addNode(i); README obviously skip nulls
                    }

                    CompilerInput.Ty.SOURCE_ROOT -> {
                        val y3 = 2
                        inputTree.addNode(i)
                    }

                    CompilerInput.Ty.ROOT -> {
                        inputTree.addNode(i)

                        val instructionsMaybe = i.acceptance_ci()
                        if (instructionsMaybe != null) {
                            val ci = instructionsMaybe.o!!.get()!!

                            /*
							cr.pushNextCompilerInsructions(ci);
*/
                            id.add(ci)
                        }
                    }

                    CompilerInput.Ty.ARG -> {
                        // inputTree.addNode(i); README skip ARGS

                        // FIXME processOption here (ie apply compiler change)

                        val yyy = 3
                    }

                    CompilerInput.Ty.STDLIB -> {
                        val y4 = 4
                    }
                }
            }

            CompilerInputField.ACCEPT_CI -> {
                if (i.ty() == CompilerInput.Ty.ROOT) {
//					final ICompilationRunner                cr                = compilation.getCompilationEnclosure().getCompilationRunner();
                    val instructionsMaybe = i.acceptance_ci()
                    if (instructionsMaybe != null) {
                        val ci = instructionsMaybe.o!!.get()!!

                        if (DebugFlags.FORCE /* || false */) {
                            //cr._cis().onNext(ci);
                            id.add(ci)
                            //								hasInstructions(List_of(i.acceptance_ci().o.get()));
                        }
                    }
                } else if (i.ty() == CompilerInput.Ty.SOURCE_ROOT) {
                    val instructionsMaybe = i.acceptance_ci()
                    if (instructionsMaybe != null) {
                        val ci = instructionsMaybe.o!!.get()
                        id.add(ci)
                    }
                } else {
                    throw UnintendedUseException()
                }
            }

            CompilerInputField.HASH -> {
                val yy = 2
                // FIXME latch all create/commit inputs.txt -> should be Buffer!!
            }

            CompilerInputField.DIRECTORY_RESULTS -> {
                val y = 2

                if (i.directoryResults != null) {
                    val directoryResults = i.directoryResults.directoryResult

                    for (directoryResult in directoryResults) {
                        if (directoryResult.mode() == Mode.SUCCESS) {
                            val iLazyCompilerInstructions = of(directoryResult.success()!!)

                            id.add(iLazyCompilerInstructions.get())

                            if (DebugFlags.CCI_gate) {
                                /*
								cci.accept(Maybe.of(iLazyCompilerInstructions), _progressSink);
*/
                                throw UnintendedUseException("failpoint")
                            }
                        }
                    }
                }
            }
        }
    }
}
