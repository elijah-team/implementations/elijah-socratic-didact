package tripleo.vendor.batoull22

import org.jetbrains.annotations.Contract
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.io.InputStream
import java.util.*

private val EK_ExpertSystem.listfacts: List<EK_Fact>
    get() = getListfacts()

/**
 * @author tripleo
 */
internal class EK_Reader1 @Contract(pure = true) constructor(private val _system: EK_ExpertSystem, aStream: InputStream?) : EK_Reader {
    private var input_: Scanner? = null

    init {
        input_ = if (aStream != null) {
            Scanner(Objects.requireNonNull(aStream))
        } else {
            null
        }
    }

    override fun closefile() {
        if (input_ != null) {
            input_!!.close()
        }
    }

    override fun print() {
        SimplePrintLoggerToRemoveSoon.println_out_4("factlist:" + _system.listfacts)
        SimplePrintLoggerToRemoveSoon.println_out_4("rulelist:" + _system.listrule)
        SimplePrintLoggerToRemoveSoon.println_out_4("goal:" + _system.goal)
        SimplePrintLoggerToRemoveSoon.println_out_4(" ")
        // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4( c);
        // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4( j);
    }

    override fun readfile() {
        // Read the line
        if (input_ != null) {
            while (input_!!.hasNext()) {
                val a = input_!!.nextLine()

                _system.proof(a)
            }
        }
        // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4("factlist:"+ Listfacts);
        // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4("goal:"+ goal);
        // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4( "rulelist:"+Listrule);
        // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_out_4( " ");
    }
}
