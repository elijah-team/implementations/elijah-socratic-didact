package tripleo.elijah.stages.deduce

import tripleo.elijah.stages.deduce.DeduceTypes2.ExpectationBase

class PromiseExpectation<B>(private val deduceTypes2: DeduceTypes2,
                            private val base: ExpectationBase,
                            private val desc: String) {
    private var _printed = false
    private var counter: Long = 0
    private var result: B? = null
    var isSatisfied: Boolean = false
        private set

    fun fail() {
        if (!_printed) {
            deduceTypes2.LOG.err(String.format("Expectation (%s, %d) not met: %s", deduceTypes2, counter, desc))
            _printed = true
        }
    }

    fun satisfy(aResult: B) {
        val satisfied_already = if (isSatisfied) " already" else ""
        // assert !satisfied;
        if (!isSatisfied) {
            result = aResult
            isSatisfied = true
            deduceTypes2.LOG.info(String.format("Expectation (%s, %d)%s met: %s %s", deduceTypes2, counter,
                    satisfied_already, desc, base.expectationString()))
        }
    }

    fun setCounter(aCounter: Long) {
        counter = aCounter

        ///////			LOG.info(String.format("Expectation (%s, %d) set: %s %s", DeduceTypes2.this, counter, desc, base.expectationString()));
    }
}
