package tripleo.elijah.nextgen.outputtree

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

interface EOT_OutputTree {
    fun add(aOff: EOT_OutputFile)

    fun addAll(aLeof: List<EOT_OutputFile>)

    val list: MutableList<EOT_OutputFile>

    fun recompute()

    fun set(aLeof: List<EOT_OutputFile>)

    class _OTModel {
        private val _counter = MutableStateFlow/*<EOT_OutputFile>*/(value = 0) // private mutable state flow
        val counter = _counter.asStateFlow() // publicly exposed as read-only state flow

        fun inc() {
            _counter.update { it + 1 } // atomic, safe for concurrent use
        }
    }
}
