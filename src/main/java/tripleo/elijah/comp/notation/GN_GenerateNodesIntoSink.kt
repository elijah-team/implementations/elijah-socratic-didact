/*  -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp.notation

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.ModuleListener
import tripleo.elijah.g.GWorldModule
import tripleo.elijah.work.WorkManager
import tripleo.elijah.work.WorkManager__
import tripleo.elijah.world.i.WorldModule

class GN_GenerateNodesIntoSink(private val env: GN_GenerateNodesIntoSinkEnv) : GN_Notable, ModuleListener {
    private val wm: WorkManager = WorkManager__()

    init {
        env.pa().compilationEnclosure.addModuleListener(this)
    }

    fun _env(): GN_GenerateNodesIntoSinkEnv {
        return env
    }

    override fun close() {
//		NotImplementedException.raise_stop();
        wm.drain()
    }

    override fun listen(module1: GWorldModule) {
        run_one_mod(module1 as WorldModule, wm)
    }

    private fun run_one_mod(wm: WorldModule, wmgr: WorkManager) {
        val gmr = GM_GenerateModuleRequest(this, wm, env)
        val gm = GM_GenerateModule(gmr)
        val ggmr = gm.getModuleResult(wmgr, env.resultSink1)
        ggmr.doResult(wmgr)
    }

    override fun run() {
        val wm: WorkManager = WorkManager__()
        val mods = env.pa().compilationEnclosure.compilation.world().mods__

        //				moduleList().getMods();
        mods.stream().forEach { mod: WorldModule ->
            run_one_mod(mod, wm)
        }

        wm.drain() // README drain the WorkManager that we created

        env.pa().accessBus.resolveGenerateResult(env.gr)
    }

    companion object {
        @Contract(value = "_ -> new", pure = true)
        @Suppress("unused")
        fun getFactoryEnv(aEnv: GN_Env): GN_Notable {
            return GN_GenerateNodesIntoSink(aEnv as GN_GenerateNodesIntoSinkEnv)
        }
    }
}
