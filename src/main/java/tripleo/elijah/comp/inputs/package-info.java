/**
 * Let's collect everything here:
 *
 * <ol>
 *     <li>Instead of writing docs...</li>
 *     <li>That being said, Javadoc sucks</li>
 * <dl>
 * <dt>{@code tripleo.elijah.comp.inputs.ILazyCompilerInstructions_}</dt><dd></dd></ul>
 * <dt>{@code CK_SourceFileFactory}</dt><dd>https://github.com/elijah-team/elevated-potential/blob/48105e480ad1a3d10a57106f93fedf009bef3662/src/main/java/tripleo/elijah/comp/nextgen/impl/CK_SourceFileFactory.java#L10</dd></ul>
 * <dt>{@code CompilerInstructions}</dt><dd>https://github.com/elijah-team/elevated-potential/blob/c89a62aa714a691801e73ddf6e96ff69ed2966ff/src/main/java/tripleo/elijah/ci/CompilerInstructions.java#L16</dd></ul>
 * <dt>{@code CompilerInput}</dt><dd></dd></ul>
 * <dt>{@code tripleo.wrap.File}</dt><dd></dd></ul>
 * <dt>{@code CompilerInstructions_Result}</dt><dd></dd></ul>
 * <dt>{@code CM_CompilerInput}</dt><dd>https://github.com/elijah-team/prolific-remnant/blob/1b0b2c4310d54a56927ed4c6e07b5d170310d561/src/main/java/tripleo/elijah/nextgen/comp_model/CM_CompilerInput.java#L21</dd></ul>
 * </dl>
 */
package tripleo.elijah.comp.inputs;