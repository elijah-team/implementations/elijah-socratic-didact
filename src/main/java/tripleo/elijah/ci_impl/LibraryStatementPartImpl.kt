/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.ci_impl

import antlr.Token
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.ci.LibraryStatementPart
import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.xlang.LocatableString

/**
 * Created 9/6/20 12:06 PM
 */
class LibraryStatementPartImpl : LibraryStatementPart {
    inner class Directive(token_: Token, private val expression: IExpression) {
        private val name: LocatableString = LocatableString.of(token_)
    }

    override fun toString(): String {
        // TODO string interpolation in kt
        return "<LSP `%s` in `%s`>".formatted(this.name, ci!!.name)
    }

    private var ci: CompilerInstructions? = null
    private var dirName: String? = null

    private var name: String? = null

    private var dirs: MutableList<Directive>? = null

    override fun addDirective(token: Token, iExpression: IExpression) {
        if (dirs == null) dirs = ArrayList()
        dirs!!.add(Directive(token, iExpression))
    }

    override fun getDirName(): String {
        return dirName!!
    }

    override fun getInstructions(): CompilerInstructions {
        return ci!!
    }

    override fun getName(): String {
        return name!!
    }

    override fun setDirName(dirName: Token) {
        this.dirName = dirName.text
    }

    override fun setInstructions(instructions: CompilerInstructions) {
        ci = instructions
    }

    override fun setName(i1: Token) {
        name = i1.text
    }
}
