package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Locatable
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util2.UnintendedUseException
import java.io.PrintStream

class ZeroPotentialDiagnostic : Diagnostic {
    override fun code(): String? {
//        NotImplementedException.raise()
//        return null
        throw UnintendedUseException("fix me later")
    }

    override fun primary(): Locatable {
//        NotImplementedException.raise()
//        return null
        throw UnintendedUseException("fix me later")
    }

    override fun report(stream: PrintStream) {
        val y = 2
//        NotImplementedException.raise()
        throw UnintendedUseException("fix me later")
    }

    override fun secondary(): List<Locatable?> {
//        NotImplementedException.raise()
//        return null
        throw UnintendedUseException("fix me later")
    }

    override fun severity(): Diagnostic.Severity? {
//        NotImplementedException.raise()
//        return null
        throw UnintendedUseException("fix me later")
    }
}
