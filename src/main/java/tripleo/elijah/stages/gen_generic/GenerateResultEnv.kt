package tripleo.elijah.stages.gen_generic

import tripleo.elijah.comp.notation.GM_GenerateModule
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.work.WorkList
import tripleo.elijah.work.WorkManager
import java.util.*

class GenerateResultEnv(private val resultSink: GenerateResultSink,
                        private val gr: GenerateResult,
                        private val wm: WorkManager,
                        private val wl: WorkList,
                        private val gmgm: GM_GenerateModule
) {
    fun resultSink(): GenerateResultSink {
        return resultSink
    }

    fun gr(): GenerateResult {
        return gr
    }

    fun wm(): WorkManager {
        return wm
    }

    fun wl(): WorkList {
        return wl
    }

    fun gmgm(): GM_GenerateModule {
        return gmgm
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as GenerateResultEnv
        return this.resultSink == that.resultSink && (this.gr == that.gr) && (this.wm == that.wm) && (this.wl == that.wl) && (this.gmgm == that.gmgm)
    }

    override fun hashCode(): Int {
        return Objects.hash(resultSink, gr, wm, wl, gmgm)
    }

    override fun toString(): String {
        return "GenerateResultEnv[" +
                "resultSink=" + resultSink + ", " +
                "gr=" + gr + ", " +
                "wm=" + wm + ", " +
                "wl=" + wl + ", " +
                "gmgm=" + gmgm + ']'
    }
}
