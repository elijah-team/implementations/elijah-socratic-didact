package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.world.i.LivingFunction

internal class GI_FunctionDef(private val _e: FunctionDef, private val _repo: GI_Repo) : GenerateC_Item {
    private var _living: LivingFunction? = null
    private var _evaNode: EvaNode? = null

    fun _re_is_FunctionDef(pte: ProcTableEntry?, a_cheat: EvaClass?,
                           ite: IdentTableEntry): EvaNode? {
        var resolved: EvaNode? = null
        if (pte != null) {
            val fi = pte.functionInvocation
            if (fi != null) {
                val gen = fi.generated
                if (gen != null) resolved = gen
            }
        }
        if (resolved == null) {
            val resolved1 = ite.resolvedType()
            if (resolved1 is EvaFunction) resolved = resolved1
            else if (resolved1 is EvaClass) {
                resolved = resolved1

                // FIXME Bar#quux is not being resolves as a BGF in Hier

//								FunctionInvocation fi = pte.getFunctionInvocation();
//								fi.setClassInvocation();
            }
        }

        if (resolved == null) {
            resolved = a_cheat
        }

        return resolved
    }

    override fun getEvaNode(): EvaNode {
        return _evaNode!!
    }

    override fun setEvaNode(aEvaNode: EvaNode) {
        _evaNode = aEvaNode
        _living = _repo.generateC.ce.compilation.world().getFunction(_evaNode as BaseEvaFunction?)
    }
}
