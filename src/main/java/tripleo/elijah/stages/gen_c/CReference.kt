/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_c

import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.nextgen.outputstatement.EG_Statement
//import tripleo.elijah.nextgen.reactive.DefaultReactive.add
//import tripleo.elijah.nextgen.reactive.Reactive.add
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.BaseTableEntry
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.util.*
import tripleo.elijah.util2.DebugFlags
import tripleo.elijah.util2.Eventual
import java.util.*
import java.util.function.Consumer
import java.util.function.Supplier
import java.util.stream.Collectors

/**
 * Created 1/9/21 7:12 AM
 */
class CReference(/* = new GI_Repo() */private val _repo: GI_Repo?, ce: CompilationEnclosure?) {
    class BuildState {
        var open: Boolean = false
        var needs_comma: Boolean = false
        var sb: StringBuilder = StringBuilder()

        fun appendText(text: String?, erase: Boolean) {
            if (erase) sb = StringBuilder()

            sb.append(text)
        }

        override fun toString(): String {
            return sb.toString()
        } // ABOVE 3a
    }

    internal enum class Connector {
        DBL_COLON, DOT, INVALID, POINTER, UNKNOWN
    }

    internal class CR_ReferenceItem1 : CR_ReferenceItem {
        private var arg: String? = null
        private var connector: Connector? = null
        private var generateCItem: GenerateC_Item? = null
        private var instructionArgument: InstructionArgument? = null
        private var previous: Eventual<GenerateC_Item>? = null

        private var reference: Reference? = null

        private var statement: Operation2<EG_Statement>? = null

        private var text: String? = null
        var type: InstructionArgument.t? = null
        private var tableEntry: BaseTableEntry? = null

        override fun getArg(): String {
            return arg!!
        }

        override fun getConnector(): Connector {
            return connector!!
        }

        override fun getGenerateCItem(): GenerateC_Item {
            return generateCItem!!
        }

        override fun getInstructionArgument(): InstructionArgument {
            return instructionArgument!!
        }

        override fun getPrevious(): Eventual<GenerateC_Item> {
            return previous!!
        }

        override fun getReference(): Reference {
            return reference!!
        }

        override fun getStatement(): Operation2<EG_Statement> {
            return statement!!
        }

        override fun getTableEntry(): BaseTableEntry {
            return tableEntry!!
        }

        override fun getText(): String {
            return text!!
        }

        override fun setArg(aArg: String) {
            arg = aArg
        }

        override fun setConnector(aConnector: Connector) {
            connector = aConnector
        }

        override fun setGenerateCItem(aGenerateCItem: GenerateC_Item) {
            generateCItem = aGenerateCItem
        }

        override fun setInstructionArgument(aInstructionArgument: InstructionArgument) {
            instructionArgument = aInstructionArgument
            if (instructionArgument is IdentIA) {
                tableEntry = (instructionArgument as IdentIA).entry
            }
        }

        override fun setPrevious(aPrevious: Eventual<GenerateC_Item>) {
            previous = aPrevious
        }

        override fun setReference(aReference: Reference) {
            reference = aReference
        }

        override fun setStatement(aStatement: Operation2<EG_Statement>) {
            statement = aStatement
        }

        override fun setText(aText: String) {
            text = aText
        }
    }

    enum class Ref {
        // was:
        // enum Ref {
        // LOCAL, MEMBER, PROPERTY_GET, PROPERTY_SET, INLINE_MEMBER, CONSTRUCTOR,
        // DIRECT_MEMBER, LITERAL, PROPERTY (removed), FUNCTION
        // }
        CONSTRUCTOR {
            override fun buildHelper(ref: Reference, sb: BuildState) {
                val text: String
                val s = sb.toString()
                text = String.format("%s(%s", ref.text, s)
                sb.open = false
                if (s != "") sb.needs_comma = true
                sb.appendText("$text)", true)
            }
        },
        DIRECT_MEMBER {
            override fun buildHelper(ref: Reference, sb: BuildState) {
                val text = Emit.emit("/*124*/") + "vsc->vm" + ref.text

                val sb1 = StringBuilder()

                sb1.append(text)
                if (ref.value != null) {
                    sb1.append(" = ")
                    sb1.append(ref.value)
                    sb1.append(";")
                }

                sb.appendText(sb1.toString(), false)
            }
        },
        FUNCTION {
            override fun buildHelper(ref: Reference, sb: BuildState) {
                val text: String
                val s = sb.toString()
                text = String.format("%s(%s", ref.text, s)
                sb.open = true
                if (s != "") sb.needs_comma = true
                sb.appendText(text, true)
            }
        },
        INLINE_MEMBER {
            override fun buildHelper(ref: Reference, sb: BuildState) {
                val text = Emit.emit("/*219*/") + ".vm" + ref.text
                sb.appendText(text, false)
            }
        },
        LITERAL {
            override fun buildHelper(ref: Reference, sb: BuildState) {
                val text = ref.text
                sb.appendText(text, false)
            }
        },

        // https://www.baeldung.com/a-guide-to-java-enums
        LOCAL {
            override fun buildHelper(ref: Reference, sb: BuildState) {
                val text = "vv" + ref.text
                sb.appendText(text, false)
            }
        },
        MEMBER {
            internal inner class Text(private val text: String, private val sb: Supplier<Boolean>, private val ss: Supplier<String?>) : GenerateC_Statement {
                override fun getText(): String {
                    val sb1 = StringBuilder()

                    sb1.append("->vm$text")

                    if (sb.get()) {
                        sb1.append(" = ")
                        sb1.append(ss.get())
                        sb1.append(";")
                    }

                    return text
                }

                override fun rule(): GCR_Rule {
                    return GCR_Rule { "Ref MEMBER Text" }
                }
            }

            override fun buildHelper(ref: Reference, sb: BuildState) {
                val t = Text(ref.text, { ref.value != null }, { ref.value })

                sb.appendText(t.text, false)
            }
        },
        PROPERTY_GET {
            override fun buildHelper(ref: Reference, sb: BuildState) {
                val text: String
                val s = sb.toString()
                text = String.format("%s%s)", ref.text, s)
                sb.open = false
                //				if (!s.equals(""))
                sb.needs_comma = false
                sb.appendText(text, true)
            }
        },
        PROPERTY_SET {
            override fun buildHelper(ref: Reference, sb: BuildState) {
                val text: String
                val s = sb.toString()
                text = String.format("%s%s, %s);", ref.text, s, ref.value)
                sb.open = false
                //				if (!s.equals(""))
                sb.needs_comma = false
                sb.appendText(text, true)
            }
        };

        abstract fun buildHelper(ref: Reference, sb: BuildState)
    }

    class Reference {
        val text: String
        val type: Ref
        val value: String?

        constructor(aText: String, aType: Ref) {
            text = aText
            type = aType
            value = null
        }

        constructor(aText: String, aType: Ref, aValue: String?) {
            text = aText
            type = aType
            value = aValue
        }

        fun buildHelper(st: BuildState) {
            type.buildHelper(this, st)
        }
    }

    //
    //
    var __cheat_ret: String? = null

    private var args: List<String?>? = null

    // void addRef(final String text, final Ref type, final String aValue) {
    // refs.add(new Reference(text, type, aValue));
    // }
    private var items: ArrayList<CR_ReferenceItem1>? = null

    //
    //
    var _cheat: EvaClass? = null

    private var rtext: SpecialText? = null

    var refs: MutableList<Reference?>? = null

    fun _repo(): GI_Repo? {
        return _repo
    }

    fun addRef(aR: Reference?) {
        refs!!.add(aR)
    }

    fun addRef(text: String, type: Ref) {
        refs!!.add(Reference(text, type))
    }

    /**
     * Call before you call build
     *
     * @param sl3
     */
    fun args(sl3: List<String?>?) {
        args = sl3
    }

    fun build(): String {
        val st = BuildState()

        for (ref in refs!!) {
            when (ref!!.type) {
                Ref.LITERAL, Ref.DIRECT_MEMBER, Ref.INLINE_MEMBER, Ref.MEMBER, Ref.LOCAL, Ref.FUNCTION, Ref.PROPERTY_GET, Ref.PROPERTY_SET, Ref.CONSTRUCTOR -> ref.buildHelper(st)
                else -> throw IllegalStateException("Unexpected value: " + ref.type)
            }//			sl.add(text);
        }

        //		return Helpers.String_join("->", sl);
        val sb = st.sb

        if (st.needs_comma && args != null && args!!.size > 0) sb.append(", ")

        if (st.open) {
            if (args != null) {
                sb.append(Helpers.String_join(", ", args!!))
            }
            sb.append(")")
        }

        return sb.toString()
    }

    fun debugPath(identIA: IdentIA, aPath: String) {
        val pl = _getIdentIAPathList(identIA)

        if (DebugFlags.FORCE_IGNORE) {
            SimplePrintLoggerToRemoveSoon.println_out_4("\\ 172-172-172-172-172 ---------------------------------------------")
            for (instructionArgument in pl) {
                if (instructionArgument is ProcIA) {
                    SimplePrintLoggerToRemoveSoon.println_out_4("" + instructionArgument.entry.__debug_expression)
                } else if (instructionArgument is IdentIA) {
                    SimplePrintLoggerToRemoveSoon.println_out_4(instructionArgument.entry.ident.text)
                } else if (instructionArgument is IntegerIA) {
                    SimplePrintLoggerToRemoveSoon.println_out_4(instructionArgument.entry.name)
                }
            }
            SimplePrintLoggerToRemoveSoon.println_out_4("- 172-172-172-172-172 ---------------------------------------------")
            System.out.printf("[%d][%s]%n", aPath.length, aPath)
            SimplePrintLoggerToRemoveSoon.println_out_4("/ 172-172-172-172-172 ---------------------------------------------")
        }
    }

    fun getIdentIAPath(ia2: IdentIA,
                       aog: AOG,
                       aValue: String?): SpecialText {
        val generatedFunction = ia2.gf
        val s = _getIdentIAPathList(ia2)
        refs = ArrayList(s.size)

        //
        //
        //
        //
        //
        //
        items = ArrayList(s.size)

        //
        //
        //
        //
        //
        //
        //

        //
        // TODO NOT LOOKING UP THINGS, IE PROPERTIES, MEMBERS
        //
        var text: String? = ""
        val sl: MutableList<String> = ArrayList()
        var i = 0
        val sSize = s.size
        while (i < sSize) {
            //
            //
            //
            //
            val item = CR_ReferenceItem1()

            //
            //
            //
            //
            val ia = s[i]

            item.instructionArgument = ia

            text = if (ia is IntegerIA) {
                getIdentIAPath__IntegerIA(generatedFunction, i, item, ia)
            } else if (ia is IdentIA) {
                getIdentIAPath__IdentIA(ia2, aog, aValue, s, sl, i, sSize, item, ia)
            } else if (ia is ProcIA) {
                getIdentIAPath__ProcIA(item, ia)
            } else {
                throw NotImplementedException()
            }
            if (text != null) sl.add(text)

            //
            //
            //
            //
            //
            //
            items!!.add(item)
            i++
        }
        if (DebugFlags.FORCE_IGNORE) { // || true || sl.size() < items.size()) {
            val itms = items!!.stream().map { itm: CR_ReferenceItem1 ->
                val bs = BuildState()
                itm.reference.type.buildHelper(itm.reference, bs)
                bs.sb.toString()
            }.collect(Collectors.toList())

            logProgress(219219, "" + items)

            rtext = SpecialText.Companion.compose(itms)
        } else {
            rtext = SpecialText.Companion.compose(sl)
        }
        return rtext!!
    }

    private fun getIdentIAPath__IdentIA(ia2: IdentIA, aog: AOG,
                                        aValue: String?, s: List<InstructionArgument>, sl: MutableList<String>,
                                        i: Int, sSize: Int, item: CR_ReferenceItem1, identIA: IdentIA): String {
        val text: String
        item.type = InstructionArgument.t.IDENT

        val idte = identIA.entry
        val gf = identIA.gf

        if (idte._deduceTypes2() == null) {
            logProgress(169169, "")
            // throw new AssertionError();
        }

        val referenceConsumer = Consumer { e: Reference ->
            // refs.add(e);
            item.reference = e
        }
        val criIdent: CRI_Ident = CRI_Ident.Companion.of(idte, gf)
        text = criIdent.getIdentIAPath(i, sSize, aog, sl, aValue, referenceConsumer, s, ia2, this, item)!!

        // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("181181 " + text + " " + item.getText());
        // assert text != null;
        if (text == null) return "<<null 181>>"
        return text
    }

    private fun getIdentIAPath__IntegerIA(generatedFunction: BaseEvaFunction, i: Int,
                                          item: CR_ReferenceItem1, ia: InstructionArgument): String {
        val text: String
        item.type = InstructionArgument.t.VARIABLE

        assert(i == 0)
        val vte = generatedFunction.getVarTableEntry(DeduceTypes2.to_int(ia))

        /*
		 * if (vte.getName().equals("a1")) { final GenType gt1 = vte.getGenType(); final
		 * GenType gt2 = vte.getType().genType; final EvaClass gc1 = (EvaClass)
		 * gt1.getNode();
		 * 
		 * _cheat = gc1;
		 * 
		 * 
		 * // only gt1.node is not null
		 * 
		 * //assert gc1.getCode() == 106 || gc1.getCode() == 105 || gc1.getCode() ==
		 * 103; assert gc1.getName().equals("ConstString");
		 * 
		 * // gt2
		 * 
		 * assert gt2.getResolvedn() == null; assert gt2.getTypeName() instanceof
		 * OS_UserType; assert gt2.getNonGenericTypeName() instanceof RegularTypeName;
		 * if (gc1.getCode() == 106) { assert gt2.getResolved() instanceof OS_FuncType;
		 * // wrong: should be usertype: EvaClass } else if (gc1.getCode() == 105) {
		 * assert gt2.getResolved() instanceof OS_UserClassType; // now good ?? :):) }
		 * assert ((ClassInvocation) gt2.getCi()).resolvePromise().isResolved();
		 * 
		 * ((ClassInvocation) gt2.getCi()).resolvePromise().then(gc -> { // wrong:
		 * should be ConstString if (gc1.getCode() == 106) { assert gc.getCode() == 102;
		 * assert gc.getKlass().getName().equals("Arguments"); } else if (gc1.getCode()
		 * == 105) { assert gc.getCode() == 105; assert
		 * gc.getKlass().getName().equals("ConstString"); } });
		 * 
		 * assert gt2.getFunctionInvocation() == null;
		 * 
		 * final int y = 2; }
		 */
        text = "vv" + vte.name
        addRef(vte.name, Ref.LOCAL)

        item.reference = Reference(vte.name, Ref.LOCAL)
        return text
    }

    private fun getIdentIAPath__ProcIA(item: CR_ReferenceItem1, ia: ProcIA): String? {
        val text: String
        item.type = InstructionArgument.t.PROC

        val pia = _repo!!.itemFor(ia)!!
        text = pia.getIdentIAPath { p: Pair<String, Ref> ->
            val e = p.left!!
            addRef(e, p.right)
            item.reference = Reference(e, p.right)
        }!!
        return text
    }

    fun getIdentIAPath2(
        ia2: IdentIA,
        aog: AOG?,
        aValue: String?,
    ): String {
        val generatedFunction = ia2.gf
        val s = _getIdentIAPathList(ia2)

        val texts: MutableList<String> = ArrayList()

        for (instructionArgument in s) {
            if (instructionArgument is IntegerIA) {
                val entry = instructionArgument.entry
                texts.add("vv" + entry.name)
            } else if (instructionArgument is IdentIA) {
                val entry = instructionArgument.entry
                texts.add("vm" + entry.ident.text)
            } else if (instructionArgument is ProcIA) {
                val entry = instructionArgument.entry
                texts.add("" + entry.resolvedElement)
            }
        }

        return Helpers.String_join("->", texts)
    }

    private fun logProgress(aI: Int, aS: String) {
        _repo()!!.generateC._ce().logProgress(CompProgress.GenerateC, Pair.of(aI, aS))
    }
}

fun _getIdentIAPathList(oox: InstructionArgument): List<InstructionArgument> {
    var oo: InstructionArgument? = oox
    val s: MutableList<InstructionArgument> = ArrayList()// LinkedList() // nobody likes you anyway
    while (oo != null) {
        if (oo is IntegerIA) {
            s.add(0, oo)
            oo = null
        } else if (oo is IdentIA) {
            val ite1 = oo.entry
            s.add(0, oo)
            oo = ite1.backlink
        } else if (oo is ProcIA) {
//				final ProcTableEntry prte = ((ProcIA)oo).getEntry();
            s.add(0, oo)
            oo = null
        } else throw IllegalStateException("Invalid InstructionArgument")
    }
    return s
}
