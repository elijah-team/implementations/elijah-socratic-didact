/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp

import com.google.common.collect.Multimap
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.comp.nextgen.i.CP_Path
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.g.GPipelineMember
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputtree.EOT_OutputFile
import tripleo.elijah.nextgen.outputtree.EOT_OutputFileImpl
import tripleo.elijah.nextgen.outputtree.EOT_OutputType
import tripleo.elijah.stages.gen_generic.DoubleLatch
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.write_stage.pipeline_impl.mkf11
import tripleo.elijah.stages.write_stage.pipeline_impl.mkf12
import tripleo.elijah.util.*
import tripleo.elijah.util.io.CharSink
import tripleo.elijah.util.io.FileCharSink
import java.io.IOException
import java.nio.file.Path
import java.util.*
import java.util.function.Consumer
import java.util.function.Supplier
import java.util.regex.Pattern
import java.util.stream.Collectors

/**
 * Created 9/13/21 11:58 PM
 */
class WriteMesonPipeline(pa0: GPipelineAccess) : PipelineMember(), Consumer<Supplier<GenerateResult>>, GPipelineMember {
    val pullPat: Pattern = Pattern.compile("/[^/]+/(.+)")
    private val c: Compilation
    private val pa = pa0 as IPipelineAccess
    private val writePipeline: WritePipeline
    var write_makefiles_latch: DoubleLatch<Multimap<CompilerInstructions, String>> = DoubleLatch { lsp_outputs: Multimap<CompilerInstructions, String> -> this.write_makefiles_action(lsp_outputs) }
    private var _wmc: Consumer<Multimap<CompilerInstructions, String>>? = null
    private var grs: Supplier<GenerateResult>? = null

    init {
        val ab = pa.accessBus
        val compilation = ab.compilation
        val WritePipeline1 = ab.pipelineAccess.witePipeline

        c = compilation
        writePipeline = WritePipeline1
    }

    override fun accept(aGenerateResultSupplier: Supplier<GenerateResult>) {
        val gr: GenerateResult? = aGenerateResultSupplier.get()
        // 08/13 tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("WMP66 "+gr);
        grs = aGenerateResultSupplier
        val y = 2
    }

    fun consumer(): Consumer<Supplier<GenerateResult>> {
        return Consumer<Supplier<GenerateResult>> { aGenerateResultSupplier ->
            if (grs != null) {
                SimplePrintLoggerToRemoveSoon.println_err_2("234 grs not null " + grs!!.javaClass.name)
                return@Consumer
            }
            assert(false)
            grs = aGenerateResultSupplier
            // final GenerateResult gr = aGenerateResultSupplier.get();
        }
    }

    fun pullFileName(aFilename: String): String? {
        // return aFilename.substring(aFilename.lastIndexOf('/')+1);
        val x = pullPat.matcher(aFilename)
        try {
            if (x.matches()) return x.group(1)
        } catch (ignored: IllegalStateException) {
        }
        return null
    }

    @Throws(Exception::class)
    override fun run(aSt: CR_State, aOutput: CB_Output?) {
        write_makefiles()
    }

    override fun finishPipeline_asString(): String {
        return this.javaClass.toString()
    }

    private fun write_makefiles() {
        val lsp_outputs = writePipeline.st.lsp_outputs // TODO move this
        write_makefiles_consumer().accept(lsp_outputs!!)

        // write_makefiles_latch.notify(lsp_outputs);
        write_makefiles_latch.notifyLatch(true)
    }

    // by lazy??
    fun write_makefiles_consumer(): Consumer<Multimap<CompilerInstructions, String>> {
        if (_wmc != null) return _wmc!!

        _wmc = Consumer { att: Multimap<CompilerInstructions, String> -> write_makefiles_latch.notifyData(att) }

        return _wmc!!
    }

    private fun write_makefiles_action(lsp_outputs: Multimap<CompilerInstructions, String>) {
        val dep_dirs: MutableList<String> = LinkedList()

        try {
            write_root(lsp_outputs, dep_dirs)

            for (compilerInstructions in lsp_outputs.keySet()) {
                val sub_dir = compilerInstructions.name

                // final Path dpath = getPath2(sub_dir);
                getPath2(sub_dir).pathPromise.then { dpath: Path ->
                    if (dpath.toFile().exists()) {
                        try {
                            write_lsp(lsp_outputs, compilerInstructions, sub_dir)
                        } catch (aE: IOException) {
                            throw RuntimeException(aE)
                        }
                    }
                }
            }

            write_prelude()
        } catch (aE: IOException) {
            throw RuntimeException(aE)
        }
    }

    @Throws(IOException::class)
    private fun write_root(
        lsp_outputs: Multimap<CompilerInstructions, String>,
        aDep_dirs: MutableList<String>
    ) {
        val path2_ = getPath2("meson.build")

        path2_.pathPromise.then { path2: Path? ->
            var root_file: CharSink? = null
            try {
                root_file = c.io.openWrite(path2!!)
            } catch (aE: IOException) {
                throw RuntimeException(aE)
            }
            try {
                val project_name = c.projectName
                val project_string = String.format("project('%s', 'c', version: '1.0.0', meson_version: '>= 0.48.0',)", project_name)
                root_file.accept(project_string)
                root_file.accept("\n")

                for (compilerInstructions in lsp_outputs.keySet()) {
                    val name = compilerInstructions.name

                    // final Path dpath = getPath2(name);
                    val finalRoot_file: CharSink = root_file
                    path2_.child(name).pathPromise.then { dpath: Path ->
                        if (dpath.toFile().exists()) {
                            val name_subdir_string = String.format("subdir('%s')\n", name)
                            finalRoot_file.accept(name_subdir_string)
                            aDep_dirs.add(name)
                        }
                    }
                }
                aDep_dirs.add("Prelude")
                //			String prelude_string = String.format("subdir(\"Prelude_%s\")\n", /*c.defaultGenLang()*/"c");
                val prelude_string = "subdir('Prelude')\n"
                root_file.accept(prelude_string)

                //			root_file.accept("\n");
                val deps_names = Helpers.String_join(", ", aDep_dirs.stream().map { x: String? -> String.format("%s", x) } // TODO _lib
                    // ??
                    .collect(Collectors.toList()))
                root_file.accept(String.format("%s_bin = executable('%s', link_with: [ %s ], install: true)",
                    project_name, project_name, deps_names)) // dependencies, include_directories
            } finally {
                (root_file as FileCharSink).close()
            }
        }
    }

    private fun getPath2(aName: String): CP_Path {
        val root = pa.compilation.paths().outputRoot()

        val child = root.child(aName)
        return child
    }

    @Throws(IOException::class)
    private fun write_lsp(
        lsp_outputs: Multimap<CompilerInstructions, String>,
        compilerInstructions: CompilerInstructions, aSub_dir: String
    ) {
        if (true || false) {
            val path = getPath2(aSub_dir, "meson.build")
            path.pathPromise.then { pp: Path? ->
                val mesonFile = MesonFile(this, aSub_dir, lsp_outputs, compilerInstructions, path)
                val stmt: EG_Statement = mesonFile
                mesonFile.path.pathPromise.then { ppp: Path ->

                    // final String pathString = mesonFile.getPathString();
                    val pathString2 = mkf12(ppp.toString())

                    val off: EOT_OutputFile = EOT_OutputFileImpl(Helpers.List_of(), pathString2, EOT_OutputType.BUILD, stmt)
                    c.outputTree.add(off)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun write_prelude() {
        if (true || false) {
            val ppath1 = c.paths().outputRoot().child("Prelude")
            val ppath = ppath1.child("meson.build")

            if (false) {
                ppath1.pathPromise.then { pp: Path ->
                    SimplePrintLoggerToRemoveSoon.println_err_4("mkdirs 215 " + ppath1.toFile())
                    SimplePrintLoggerToRemoveSoon.println_err_4("mkdirs 215b " + pp.toFile())
                    pp.toFile().mkdirs() // README just in case -- but should be unnecessary at this point
                }
            }

            val files = Helpers.List_of("'Prelude.c'")

            val sb = StringBuilder()

            sb.append(String.format("Prelude_sources = files(\n%s\n)", Helpers.String_join("\n", files)))
            sb.append("\n")
            sb.append("Prelude = static_library('Prelude', Prelude_sources, install: false,)") // include_directories,
            // dependencies: [],
            sb.append("\n")
            sb.append("\n")
            sb.append(String.format("%s_dep = declare_dependency( link_with: %s )", "Prelude", "Prelude")) // include_directories
            sb.append("\n")

            val stmt = EG_Statement.of(sb.toString(), EX_Explanation.withMessage("WriteMesonPipeline"))
            val s: String = ppath.toString()
            val off: EOT_OutputFile = EOT_OutputFileImpl3(Helpers.List_of(), s, EOT_OutputType.BUILD, stmt)
            c.outputTree.add(off)
        }
    }

    private fun getPath2(aName: String, aName2: String): CP_Path {
        val root = pa.compilation.paths().outputRoot()

        val child = root.child(aName).child(aName2)
        return child
    }
}
