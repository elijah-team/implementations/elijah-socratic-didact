package tripleo.elijah.stages.gen_fn

import tripleo.elijah.lang.i.*
import tripleo.elijah.stages.deduce.FunctionInvocation
//import tripleo.elijah.stages.deduce.FunctionInvocation.function
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.*

class DT_Resolvabley(private val x: List<DT_Resolvable>) {
    fun getNormalPath(generatedFunction: BaseEvaFunction, identIA: IdentIA): String {
        val rr: MutableList<String> = LinkedList()

        for (resolvable in x) {
            val element = resolvable.element()
            if (element == null && resolvable.deduceItem() is FunctionInvocation) {
                val fi = resolvable.deduceItem() as FunctionInvocation
                val fd: FunctionDef = fi.function!!
                rr.add("%s".formatted(fd.nameNode.text))
                // rr.add("%s()".formatted(fd.getNameNode().getText()));
                continue
            }

            if (element != null) {
                when (DecideElObjectType.getElObjectType(element)) {
                    ElObjectType.CLASS -> {
                        val cs = element as ClassStatement
                        if (resolvable.deduceItem() is FunctionInvocation) {
                            val fi = resolvable.deduceItem() as FunctionInvocation
                            val fd: FunctionDef = fi.function!!
                            if (fi.function is ConstructorDef) {
                                rr.add("%s()".formatted(cs.name))
                            }
                        }
                    }

                    ElObjectType.FUNCTION -> {
                        val fd = element as FunctionDef
                        if (resolvable.deduceItem() == null) {
                            // when ~ is folders.forEach, this is null (fi not set yet)
                            rr.add("%s".formatted(fd.nameNode.text))
                        } else if (resolvable.deduceItem() is FunctionInvocation) {
                            val fi = resolvable.deduceItem() as FunctionInvocation
                            if (fi.function === fd) {
                                rr.add("%s".formatted(fd.nameNode.text))
                                //	    					rr.add("%s(...)".formatted(fd.getNameNode().getText()));
                            }
                        }
                    }

                    ElObjectType.VAR -> {
                        val vs = element as VariableStatement
                        rr.add(vs.name)
                    }

                    ElObjectType.FORMAL_ARG_LIST_ITEM -> {
                        val fali = element as FormalArgListItem
                        rr.add(fali.name())
                    }

                    ElObjectType.ALIAS -> TODO()
                    ElObjectType.CONSTRUCTOR -> TODO()
                    ElObjectType.MODULE -> TODO()
                    ElObjectType.NAMESPACE -> TODO()
                    ElObjectType.TYPE_NAME_ELEMENT -> TODO()
                    ElObjectType.UNKNOWN -> TODO()
                    ElObjectType.VAR_SEQ -> TODO()
                }
            } else if (resolvable.instructionArgument() is IdentIA) {
                val identIA2 = resolvable.instructionArgument() as IdentIA
                val ite: IdentTableEntry = identIA2.entry

                if (ite._callable_pte() != null) {
                    val cpte = ite._callable_pte()

                    assert(cpte!!.status != BaseTableEntry.Status.KNOWN)
                    rr.add("%s".formatted(ite.ident!!.text))
                }
            }
        }

        val r = Helpers.String_join(".", rr)

        val z = generatedFunction.getIdentIAPathNormal(identIA)

        // assert r.equals(z);
        if (r != z) {
            // 08/13
            logProgress(67, z)
        }

        return r
    }

    companion object {
        private fun logProgress(code: Int, message: String) {
            if (code == 67) {
                SimplePrintLoggerToRemoveSoon.println_err_4("----- 67 Should be $message")
            } else throw Error()
        }
    }
}
