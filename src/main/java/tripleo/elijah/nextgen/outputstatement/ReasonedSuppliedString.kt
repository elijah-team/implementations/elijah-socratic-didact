package tripleo.elijah.nextgen.outputstatement

import java.util.function.Supplier

internal class ReasonedSuppliedString(var textSupplier: Supplier<String>, var reason: String) : IReasonedString {
    override fun text(): String? {
        return textSupplier.get()
    }

    override fun reason(): String {
        return reason
    }
}
