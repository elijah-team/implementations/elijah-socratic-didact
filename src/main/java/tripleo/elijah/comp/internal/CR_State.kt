/*  -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp.internal

import org.apache.commons.lang3.tuple.Pair
import org.jdeferred2.DoneCallback
import org.jdeferred2.impl.DeferredObject
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.*
import tripleo.elijah.comp.AccessBus.AB_LgcListener
import tripleo.elijah.comp.AccessBus.AB_PipelineLogicListener
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.graph.i.CK_Steps
import tripleo.elijah.comp.graph.i.CK_StepsContext
import tripleo.elijah.comp.i.CB_Action
import tripleo.elijah.comp.i.ICompilationAccess
import tripleo.elijah.comp.i.ProcessRecord
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.inputs.CompilerInput
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.nextgen.CK_DefaultStepRunner
import tripleo.elijah.comp.notation.GN_Env
import tripleo.elijah.comp.notation.GN_Notable
import tripleo.elijah.g.GCR_State
import tripleo.elijah.g.GPipelineMember
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.output.NG_OutputItem
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.stages.logging.ElLog
import tripleo.elijah.stages.write_stage.pipeline_impl.WP_Flow.OPS
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.lang.reflect.InvocationTargetException
import java.util.function.Consumer

class CR_State @Contract(pure = true) constructor(private val ca: ICompilationAccess) : GCR_State {
    val compilationEnclosure: CompilationEnclosure = ca.compilationEnclosure as CompilationEnclosure
    var cur: CB_Action? = null
    var pr: ProcessRecord = ProcessRecordImpl(ca)
    private var compilationRunner: CompilationRunner? = null

    init {
        compilationEnclosure._resolvePipelineAccessPromise(ProcessRecord_PipelineAccess())
    }

    fun ca(): ICompilationAccess {
        return ca
    }

    fun runner(): CompilationRunner? {
        return compilationRunner
    }

    fun setRunner(aCompilationRunner: CompilationRunner?) {
        compilationRunner = aCompilationRunner
    }

    private class ProcessRecordImpl(// private final DeducePipeline dpl;
            private val ca: ICompilationAccess) : ProcessRecord {
        private lateinit var pa: IPipelineAccess
        private lateinit var pipelineLogic: PipelineLogic

        init {
            val compilationEnclosure0 = ca.getCompilationEnclosure() as CompilationEnclosure
            compilationEnclosure0.pipelineAccessPromise.then { pa0: IPipelineAccess? ->
                pa = pa0!!
                pipelineLogic = PipelineLogic(pa, ca)
            }
        }

        @Contract(pure = true)
        override fun ca(): ICompilationAccess {
            return ca
        }

        @Contract(pure = true)
        override fun pa(): IPipelineAccess {
            return pa
        }

        @Contract(pure = true)
        override fun pipelineLogic(): PipelineLogic {
            return pipelineLogic
        }

        override fun writeLogs() {
            val ce = pa.compilationEnclosure
            ce.writeLogs()
        }
    }

    internal inner class ProcessRecord_PipelineAccess : IPipelineAccess {
        private val _l_classes: MutableList<EvaNode> = ArrayList()
        private val activeClasses: MutableList<EvaClass> = ArrayList()
        private val activeNamespaces: MutableList<EvaNamespace> = ArrayList()
        private val EvaPipelinePromise = DeferredObject<EvaPipeline, Void, Void>()
        private val gc2m_map: MutableMap<OS_Module, DeferredObject<GenerateC, Void, Void>> = HashMap()
        private val installs: MutableMap<Provenance, Pair<Class<*>, Class<*>>> = java.util.HashMap()
        private val nlp = DeferredObject<List<EvaNode>, Void, Void>()
        private val outputs: MutableList<NG_OutputItem> = ArrayList()
        private val ppl = DeferredObject<PipelineLogic, Void, Void>()
        var activeFunctions_: MutableList<BaseEvaFunction> = ArrayList()
        private var _ab: AccessBus? = null
        private var _wpl: WritePipeline? = null
        private var grs: GenerateResultSink? = null
        private val inp: List<CompilerInput>? = null

        override fun _send_GeneratedClass(aClass: EvaNode) {
            _l_classes.add(aClass)
        }

        override fun _setAccessBus(ab: AccessBus) {
            _ab = ab
        }

        override fun activeClass(aEvaClass: EvaClass) {
            activeClasses.add(aEvaClass)
        }

        override fun activeFunction(aEvaFunction: BaseEvaFunction) {
            activeFunctions_.add(aEvaFunction)
        }

        override fun activeNamespace(aEvaNamespace: EvaNamespace) {
            activeNamespaces.add(aEvaNamespace)
        }

        override fun addLog(aLOG: ElLog) {
            compilationEnclosure.addLog(aLOG)
        }

        override fun addOutput(aOutput: NG_OutputItem) {
            outputs.add(aOutput)
        }

        override fun getAccessBus(): AccessBus {
            return _ab!!
        }

        override fun getActiveClasses(): List<EvaClass> {
            return activeClasses
        }

        override fun getActiveFunctions(): List<BaseEvaFunction> = activeFunctions_

        override fun getActiveNamespaces(): List<EvaNamespace> {
            return activeNamespaces
        }

        override fun getCompilation(): Compilation {
            return ca.compilation as Compilation
        }

        override fun getCompilationEnclosure(): CompilationEnclosure {
            return compilation.compilationEnclosure
        }

        override fun getCompilerInput(): List<CompilerInput> {
            return compilationEnclosure.compilerInput
        }

        override fun getGenerateResultSink(): GenerateResultSink {
            return grs!!
        }

        override fun getPipelineLogicPromise(): DeferredObject<PipelineLogic, Void, Void> {
            return ppl
        }

        override fun getProcessRecord(): ProcessRecord {
            return pr
        }

        // @Override
        // public @NotNull List<NG_OutputItem> getOutputs() {
        // return outputs;
        // }
        override fun getWitePipeline(): WritePipeline {
            return _wpl!!
        }

        override fun install_notate(aProvenance: Provenance,
                                    aRunClass: Class<out GN_Notable?>,
                                    aEnvClass: Class<out GN_Env>) {
            installs[aProvenance] = Pair.of(aRunClass, aEnvClass)
        }

        override fun notate(aProvenance: Provenance, aGNEnv: GN_Env) {
            val y = installs[aProvenance]!!

            // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("210 "+y);
            val x = y.left

            // var z = y.getRight();
            try {
                val inst = x.getMethod("getFactoryEnv", GN_Env::class.java)

                val notable1 = inst.invoke(null, aGNEnv)

                if (notable1 is GN_Notable) {
                    val notableAction = NotableAction(notable1)

                    // cb.add(notableAction);
                    notableAction._actual_run() // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

                    // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("227 "+inst);
                }
            } catch (e: NoSuchMethodException) {
                e.printStackTrace()
                throw RuntimeException(e)
            } catch (e: SecurityException) {
                e.printStackTrace()
                throw RuntimeException(e)
            } catch (e: InvocationTargetException) {
                e.printStackTrace()
                throw RuntimeException(e)
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
                throw RuntimeException(e)
            }
        }

        override fun notate(provenance: Provenance, aNotable: GN_Notable) {
            val cb = compilationEnclosure.compilationBus

            // FIXME 07/01 this doesn't work, why??
            // also keep in mind `provenance'
            val notableAction = NotableAction(aNotable)

            cb.add(notableAction)

            // 09/01 aNotable.run();
            notableAction._actual_run()
        }

        override fun pipelineLogic(): PipelineLogic {
            return processRecord.pipelineLogic()
        }

        override fun registerNodeList(done: DoneCallback<List<EvaNode>>) {
            nlp.then(done)
        }

        override fun resolvePipelinePromise(aPipelineLogic: PipelineLogic) {
            _ab!!.resolvePipelineLogic(aPipelineLogic)
        }

        override fun resolveWaitGenC(mod: OS_Module, aGenerateFiles: GenerateFiles) {
            val gc = aGenerateFiles as GenerateC
            val gcp = DeferredObject<GenerateC, Void, Void>()
            gcp.resolve(gc)
            gc2m_map[mod] = gcp
        }

        override fun setGenerateResultSink(aGenerateResultSink: GenerateResultSink) {
            grs = aGenerateResultSink
        }

        override fun setNodeList(aEvaNodeList: List<EvaNode>) {
            nlp /* ;) */.resolve(ArrayList(aEvaNodeList))
        }

        override fun setWritePipeline(aWritePipeline: WritePipeline) {
            _wpl = aWritePipeline
        }

        override fun waitGenC(mod: OS_Module, cb: Consumer<GenerateC>) {
            val v = gc2m_map[mod]!!
            v.then { ggc: GenerateC -> cb.accept(ggc) }
        }

        override fun finishPipeline(aPM: GPipelineMember, aOps: OPS) {
            val formatted = "[FinishPipeline] %s %s".formatted(aPM.finishPipeline_asString(), aOps)
            SimplePrintLoggerToRemoveSoon.println_err_4(formatted)
        }

        override fun runStepsNow(aSteps: CK_Steps, aStepsContext: CK_StepsContext) {
            val monitor: CK_Monitor? = null
            CK_DefaultStepRunner.runStepsNow(aSteps, aStepsContext, monitor)
        }

        override fun addFunctionStatement(aStatement: EG_Statement) {
            when (aStatement) {
                is FunctionStatement -> {
                    addFunctionStatement(aStatement)
                }
            }
        }

        override fun subscribePipelineLogic(aListener: AB_PipelineLogicListener) {
            val ab = accessBus
            ab.subscribePipelineLogic { lgc: PipelineLogic? -> aListener.pl_slot(lgc) }
        }

        override fun subscribe_lgc(aListener: AB_LgcListener) {
            val ab = accessBus
            ab.subscribe_lgc(aListener)
        }

        fun addFunctionStatement(aFunctionStatement: FunctionStatement) {
            EvaPipelinePromise.then { gp: EvaPipeline ->
                gp.addFunctionStatement(aFunctionStatement)
            }
        }

        override fun setCompilerInput(aInputs: List<CompilerInput>) {
            compilationEnclosure.compilerInput = aInputs
        }

        override fun setEvaPipeline(agp: EvaPipeline) {
            if (EvaPipelinePromise.isResolved) {
                EvaPipelinePromise.then { ep: EvaPipeline -> assert(ep === agp) }
            } else {
                EvaPipelinePromise.resolve(agp)
            }
        }
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

