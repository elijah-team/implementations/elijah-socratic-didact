package tripleo.elijah.stages.gen_generic.pipeline_impl

import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaContainerNC
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.GenerateFiles_
import tripleo.elijah.stages.gen_generic.GenerateResultEnv

class ProcessedNode1(val evaNode: EvaNode) : ProcessedNode {
    override fun isContainerNode(): Boolean {
        return evaNode is EvaContainerNC
    }

    override fun matchModule(aMod: OS_Module): Boolean {
        return evaNode.module() === aMod
    }

    override fun processClassMap(ggc: GenerateFiles, aFileGen: GenerateResultEnv) {
        val nc = evaNode as EvaContainerNC

        val gn2 = GenerateFiles_.classes_to_list_of_generated_nodes(nc.classMap.values)
        val gr4 = ggc.generateCode(gn2, aFileGen)
        aFileGen.gr().additional(gr4)
        aFileGen.resultSink().additional(gr4)
    }

    override fun processConstructors(ggc: GenerateFiles, aFileGen: GenerateResultEnv) {
        val nc = evaNode as EvaContainerNC

        if (nc is EvaClass) {
            val gn2 = GenerateFiles_.constructors_to_list_of_generated_nodes(nc.constructors.values)
            val gr3 = ggc.generateCode(gn2, aFileGen)

            aFileGen.gr().additional(gr3)
            aFileGen.resultSink().additional(gr3)
        }
        /*
		 * if (nc instanceof final EvaNamespace evaNamespace) { final @NotNull
		 * Collection<EvaNode> gn2 =
		 * GenerateFiles.constructors_to_list_of_generated_nodes(evaNamespace.
		 * constructors.values()); GenerateResult gr3 = ggc.generateCode(gn2, wm,
		 * aResultSink); gr.additional(gr3); aResultSink.additional(gr3); }
		 */
    }

    override fun processContainer(ggc: GenerateFiles, aFileGen: GenerateResultEnv) {
        val nc = evaNode as EvaContainerNC

        nc.generateCode(aFileGen, ggc)
    }

    override fun processFunctions(ggc: GenerateFiles, aFileGen: GenerateResultEnv) {
        val nc = evaNode as EvaContainerNC

        val gn1 = GenerateFiles_.functions_to_list_of_generated_nodes(nc.functionMap.values)
        val gr2 = ggc.generateCode(gn1, aFileGen)
        aFileGen.gr().additional(gr2)
        aFileGen.resultSink().additional(gr2)
    }
}
