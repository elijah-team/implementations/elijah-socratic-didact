package tripleo.elijah.nextgen.output

import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.stages.garish.GarishNamespace
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.stages.generate.OutputStrategyC
import tripleo.elijah.util.Helpers

class NG_OutputNamespace : NG_OutputItem {
    private var garishNamespace: GarishNamespace? = null
    private var generateC: GenerateC? = null

    override val outputs: List<NG_OutputStatement>
        get() {
            val x = garishNamespace!!.living.evaNode()
            val m = x.module()

            val class_name = if (x.code1234567 != 0) {
                "ZN%d".formatted(x.code1234567)
            } else {
                x.name
            }

            val tos = garishNamespace!!.getImplBuffer(x, class_name, x.code1234567)
            val implText = NG_OutputNamespaceStatement(tos, m, TY.IMPL)

            val tosHdr = garishNamespace!!.getHeaderBuffer(generateC!!, x, class_name, x.code1234567)
            val headerText = NG_OutputNamespaceStatement(tosHdr, m, TY.HEADER)

            return Helpers.List_of<NG_OutputStatement>(implText, headerText)
        }

    override fun outName(aOutputStrategyC: OutputStrategyC,
                         ty: TY): EOT_FileNameProvider {
        val x = garishNamespace!!.living.evaNode()

        return aOutputStrategyC.nameForNamespace1(x, ty)
    }

    fun setNamespace(aGarishNamespace: GarishNamespace?, aGenerateC: GenerateC?) {
        garishNamespace = aGarishNamespace
        generateC = aGenerateC
    }
}
