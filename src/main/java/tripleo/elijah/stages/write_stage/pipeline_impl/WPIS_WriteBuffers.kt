package tripleo.elijah.stages.write_stage.pipeline_impl

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.WritePipeline
import tripleo.elijah.comp.nextgen.i.CP_Paths
import tripleo.elijah.comp.nextgen.pn.SC_Fai_
import tripleo.elijah.comp.nextgen.pn.SC_I
import tripleo.elijah.comp.nextgen.pn.SC_Suc_
import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.outputstatement.*
import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.nextgen.outputtree.EOT_OutputFile
import tripleo.elijah.nextgen.outputtree.EOT_OutputFileImpl
import tripleo.elijah.nextgen.outputtree.EOT_OutputType
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.write_stage.pipeline_impl.LSPrintStream.LSResult
import tripleo.elijah.util.Helpers
import tripleo.elijah.util2.Eventual
import tripleo.wrap.File
import java.io.FileNotFoundException
import java.nio.file.Path

class WPIS_WriteBuffers @Contract(pure = true) constructor() : WP_Individual_Step, SC_I {
	private val _p_spsp = Eventual<LSResult?>()
	private val compilationPromise = Eventual<Compilation>()
	private val _m_bus = Bus()

	private lateinit var writePipeline: WritePipeline

	init {
		compilationPromise.then { _m_bus.setCompilation(it) }

		// TODO see if clj has anything for csp
		_p_spsp.then { ls: LSResult? ->
			compilationPromise.then { c: Compilation ->
				val outputFile = createOutputFile(ls, c, c.paths())
				_m_bus.addOutputFile(outputFile)
			}
		}
	}

	override fun sc_i_asString(): String {
		return "WPIS_WriteBuffers :: <interesting stuff here>"
	}

//    fun execute(aMonitor: CK_Monitor): Operation<Ok>? {
//        return null
//    }

	override fun act(st: WritePipelineSharedState, sc: WP_State_Control) {
		writePipeline = st._up

		// 5. write buffers
		try {
			debug_buffers(st)
			sc.markSuccess(SC_Suc_.i(this))
		} catch (aE: FileNotFoundException) {
			sc.markFailure(SC_Fai_.f(sc, aE))
		}
	}

	@Throws(FileNotFoundException::class)
	private fun debug_buffers(st: WritePipelineSharedState) {
		// TODO can/should this fail??

		compilationPromise.resolve(st.c)

		//final List<Old_GenerateResultItem> generateResultItems1 = st.getGr().results();
		val or = st.c.paths().outputRoot()

		val child = or.child("buffers.txt")

		child.pathPromise.then { pp: Path? ->
			// final File file = pp.toFile();
			// final String s1 = file.toString();
			// Stupidity.println_err_3("8383 " + s1);

			// TODO nested promises is a latch
			writePipeline.generateResultPromise.then { result: GenerateResult ->
				val result1 = st.gr
				val sps = LSPrintStream()

				DebugBuffersLogic.debug_buffers_logic(result1!!, sps)

				val _s = sps.result
				_p_spsp.resolve(_s)
			}
		}
	}

	internal class Bus {
		private var c: Compilation? = null

		fun addOutputFile(off: EOT_OutputFile) {
			c!!.outputTree.add(off)
		}

		fun setCompilation(cc: Compilation) {
			c = cc
		}
	}

	companion object {
		private fun createOutputFile(
				ls: LSResult?,
				c: Compilation,
				paths: CP_Paths,
		): EOT_OutputFile {
			// ??!!
			/*
									c.asv<EOT_OutputFile> {
											path <- [ paths.outputRoot() , "buffers.txt" ]
											type <- EOT_OutputType.BUFFERS
											filename <- {
													// ()->filename1...
											}
											statement { // sequencebuilder

											}
									}
			*/

			val or = paths.outputRoot()
			val file1 = or.subFile("buffers.txt").toFile() // TODO subFile vs child

			val fs: List<EIT_Input>
			val sequence: EG_Statement

			if (ls == null) {
				sequence = EG_SingleStatement("<<>>", EX_Explanation_withMessage("dsnajkldnasjkldasnjk"))
				fs = Helpers.List_of()
			} else {
				sequence = mk_EG_SequenceStatement(EG_Naming("WriteBuffers"), ls.statement)
				fs = ls.fs2(c)
			}
			val file11 = mkf11(file1)
			// noinspection UnnecessaryLocalVariable
			val off1: EOT_OutputFile = EOT_OutputFileImpl(fs, file11, EOT_OutputType.BUFFERS, sequence)
			return off1
		}
	}
}

fun mkf11(file1: File): EOT_FileNameProvider {
	//   return {file1.toString()}
	return object : EOT_FileNameProvider {
		override fun getFilename(): String {
			return file1.betterName
		}
	}
}
fun EOT_FileNameProvider__ofString(file2: String): EOT_FileNameProvider {
	//   return {file1.toString()}
	return object : EOT_FileNameProvider {
		override fun getFilename(): String {
			return file2
		}
	}
}
fun mkf12(file2: String): EOT_FileNameProvider {
	//   return {file1.toString()}
	return object : EOT_FileNameProvider {
		override fun getFilename(): String {
			return file2
		}
	}
}
