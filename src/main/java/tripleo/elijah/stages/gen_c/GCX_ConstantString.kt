package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.CharLitExpression
import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.lang.i.NumericExpression
import tripleo.elijah.lang.i.StringExpression
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.stages.gen_c.GenerateC.GetAssignmentValue
import tripleo.elijah.util.NotImplementedException

class GCX_ConstantString(private val generateC: GenerateC, private val getAssignmentValue: GetAssignmentValue,
                         private val expression: IExpression) : EG_Statement {
    override val explanation: EX_Explanation
        get() = withMessage("GCX_ConstantString >> GetAssignmentValue.const_to_string")

    override val text: String
        get() {
            if (expression is NumericExpression) {
                return String.format("%d", expression.value)
            }
            if (expression is CharLitExpression) {
                return String.format("'%s'", expression)
            }
            if (expression is StringExpression) {
                // TODO triple quoted strings and other escaping concerns
                return String.format("\"%s\"", expression.text)
            }

            // FloatLitExpression
            // BooleanExpression
            throw NotImplementedException()
        }
}
