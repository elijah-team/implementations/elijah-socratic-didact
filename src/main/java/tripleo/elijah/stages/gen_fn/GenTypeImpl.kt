/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jdeferred2.DoneCallback
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.BaseFunctionDef
import tripleo.elijah.lang.types.OS_BuiltinType
import tripleo.elijah.lang.types.OS_FuncExprType
import tripleo.elijah.lang.types.OS_FuncType
import tripleo.elijah.lang.types.OS_UserClassType
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.deduce.nextgen.DR_Type
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action_Arena
import tripleo.elijah.util.Mode
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.Operation
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Created 5/31/21 1:32 PM
 */
class GenTypeImpl : GenType {
	internal class SetGenCI() {
		fun call(
				genType: GenType, aGenericTypeName: TypeName,
				deduceTypes2: DeduceTypes2, phase: DeducePhase,
		): ClassInvocation? {
			if (genType.getNonGenericTypeName() != null) {
				return nonGenericTypeName(genType, deduceTypes2, phase)
			}
			if (genType.getResolved() != null) {
				val resolvedType: OS_Type.Type = genType.getResolved().getType()

				when (resolvedType) {
					OS_Type.Type.USER_CLASS -> return resolvedUserClass(genType, aGenericTypeName, phase, deduceTypes2)
					OS_Type.Type.FUNCTION -> return resolvedFunction(genType, aGenericTypeName, deduceTypes2, phase)
					OS_Type.Type.FUNC_EXPR ->                    // TODO what to do here?
						NotImplementedException.raise()

					OS_Type.Type.BUILT_IN ->                    // README no invocation for a builtin type
						return null

					OS_Type.Type.ANY -> TODO()
					OS_Type.Type.GENERIC_TYPENAME -> TODO()
					OS_Type.Type.UNIT_TYPE -> TODO()
					OS_Type.Type.UNKNOWN -> TODO()
					OS_Type.Type.USER -> TODO()
					OS_Type.Type.USER_NAMESPACE -> TODO()
				}
			}
			return null
		}

		private fun nonGenericTypeName(
				genType: GenType,
				deduceTypes2: DeduceTypes2, phase: DeducePhase,
		): ClassInvocation {
			val aTyn1: NormalTypeName = genType.getNonGenericTypeName() as NormalTypeName
			val constructorName: String? = null // FIXME this comes from nowhere

			when (genType.getResolved().getType()) {
				OS_Type.Type.GENERIC_TYPENAME -> {
					assert(false)
					throw NotImplementedException()
				}

				OS_Type.Type.USER_CLASS -> {
					val best: ClassStatement = genType.getResolved().getClassOf()
					//
					val oi: Operation<ClassInvocation> = DeduceTypes2.ClassInvocationMake.withGenericPart(best,
							constructorName, aTyn1, deduceTypes2)
					assert(oi.mode() == Mode.SUCCESS)
					var clsinv2: ClassInvocation = oi.success()
					clsinv2 = phase.registerClassInvocation(clsinv2)
					genType.setCi(clsinv2)
					return clsinv2
				}

				else -> throw IllegalStateException("Unexpected value: " + genType.getResolved().getType())
			}
		}

		private fun resolvedFunction(
				genType: GenType,
				aGenericTypeName: TypeName, deduceTypes2: DeduceTypes2,
				phase: DeducePhase,
		): ClassInvocation? {
			// TODO what to do here?
			val ele: OS_Element = genType.getResolved().getElement()
			val best: ClassStatement? = ele.getParent() as ClassStatement? // genType.resolved.getClassOf();
			val constructorName: String? = null // TODO what to do about this, nothing I guess

			val gp: List<TypeName> = best!!.getGenericPart()
			var clsinv: ClassInvocation?
			if (genType.getCi() == null) {
				val oi: Operation<ClassInvocation> = DeduceTypes2.ClassInvocationMake.withGenericPart((best),
						constructorName, aGenericTypeName as NormalTypeName?, deduceTypes2)
				assert(oi.mode() == Mode.SUCCESS)
				clsinv = oi.success()
				if (clsinv == null) return null
				clsinv = phase.registerClassInvocation(clsinv)
				genType.setCi(clsinv)
			} else clsinv = genType.getCi() as ClassInvocation?

			assert(clsinv != null)
			if (ele is FunctionDef) {
				// deduceTypes2.newFunctionInvocation(fd, pte, clsinv, phase);
				val y: Int = 10000
			}
			return (clsinv)!!
		}

		private fun resolvedUserClass(
				genType: GenType,
				aGenericTypeName: TypeName, phase: DeducePhase,
				deduceTypes2: DeduceTypes2,
		): ClassInvocation? {
			val best: ClassStatement = genType.getResolved().getClassOf()
			val constructorName: String? = null // TODO what to do about this, nothing I guess

			val gp: List<TypeName> = best.getGenericPart()
			assert(gp.size == 0 || gp.size > 0)
			var clsinv: ClassInvocation?
			if (genType.getCi() == null) {
				val oi: Operation<ClassInvocation> = DeduceTypes2.ClassInvocationMake.withGenericPart(best,
						constructorName, aGenericTypeName as NormalTypeName?, deduceTypes2)
				assert(oi.mode() == Mode.SUCCESS)
				clsinv = oi.success()
				if (clsinv == null) return null
				clsinv = phase.registerClassInvocation(clsinv)
				genType.setCi(clsinv)
			} else clsinv = genType.getCi() as ClassInvocation?
			return (clsinv)!!
		}
	}

	private var ci: IInvocation? = null
	private var functionInvocation: FunctionInvocation? = null
	private var node: EvaNode? = null
	private var nonGenericTypeName: TypeName? = null
	private var resolved: OS_Type? = null
	private var resolvedn: NamespaceStatement? = null

	private var typeName: OS_Type? = null // TODO or just TypeName ??

	private var drType: DR_Type? = null

	@Contract(pure = true)
	constructor()

	constructor(aClassStatement: ClassStatement) {
		resolved = aClassStatement.getOS_Type()
	}

	@Contract(pure = true)
	constructor(aNamespaceStatement: NamespaceStatement?) {
		resolvedn =  /* new OS_Type */(aNamespaceStatement)
	}

	constructor(
			aAttached: OS_Type?, aOS_type: OS_Type?, aB: Boolean, aTypeName: TypeName,
			deduceTypes2: DeduceTypes2, errSink: ErrSink, phase: DeducePhase,
	) {
		typeName = aAttached
		resolved = aOS_type
		if (aB) {
			ci = genCI(aTypeName, deduceTypes2, errSink, phase)
		}
	}

	override fun asString(): String { // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
		val sb: String = ("GenType{" + "resolvedn=" + resolvedn + ", typeName=" + typeName + ", nonGenericTypeName="
				+ nonGenericTypeName + ", resolved=" + resolved + ", ci=" + ci + ", node=" + node
				+ ", functionInvocation=" + functionInvocation + '}')
		return sb
	}

	override fun copy(aGenType: GenType) {
		if (resolvedn == null) resolvedn = aGenType.getResolvedn()
		if (typeName == null) typeName = aGenType.getTypeName()
		if (nonGenericTypeName == null) nonGenericTypeName = aGenType.getNonGenericTypeName()
		if (resolved == null) resolved = aGenType.getResolved()
		if (ci == null) ci = aGenType.getCi()
		if (node == null) node = aGenType.getNode()
	}

//    @Contract(value = "null -> false", pure = true)
//    override fun equals(aO: Any?): Boolean {
//        if (this === aO) return true
//        if (aO == null || javaClass != aO.javaClass) return false
//
//        val genType: GenType = aO as GenTypeImpl
//
//        if (!(resolvedn == genType.getResolvedn())) return false
//        if (!(typeName == genType.getTypeName())) return false
//        if (!(nonGenericTypeName == genType.getNonGenericTypeName())) return false
//        if (!(resolved == genType.getResolved())) return false
//        if (!(ci == genType.getCi())) return false
//        if (!(node == genType.getNode())) return false
//        return (functionInvocation == genType.getFunctionInvocation())
//    }

	override fun genCI(
			nonGenericTypeName: TypeName, deduceTypes2: DeduceTypes2, errSink: ErrSink, phase: DeducePhase,
	): ClassInvocation {
		val sgci: SetGenCI = SetGenCI()
		val ci: ClassInvocation? = sgci.call(this, aGenericTypeName = nonGenericTypeName, deduceTypes2, phase)
		val ci1: ClassInvocation? = ci
		val ci11: ClassInvocation? = ci1
		return (ci11)!!
	}

	override fun genCIForGenType2(deduceTypes2: DeduceTypes2) {
		val list: List<setup_GenType_Action> = ArrayList()
		val arena: setup_GenType_Action_Arena = setup_GenType_Action_Arena()

		genCI((nonGenericTypeName)!!, deduceTypes2, deduceTypes2._errSink(), deduceTypes2.phase)
		val invocation: IInvocation? = ci
		if (invocation is NamespaceInvocation) {
			invocation.resolveDeferred().then(object : DoneCallback<EvaNamespace?> {
				override fun onDone(result: EvaNamespace?) {
					node = result
				}
			})
		} else if (invocation is ClassInvocation) {
			invocation.resolvePromise().then(object : DoneCallback<EvaClass?> {
				override fun onDone(result: EvaClass?) {
					node = result
				}
			})
		} else {
			if (resolved is OS_BuiltinType) {
				//
			} else throw IllegalStateException("invalid invocation")
		}

		for (action: setup_GenType_Action in list) {
			action.run(this, arena)
		}
	}

	/**
	 * Sets the invocation (`genType#ci`) and the node for a GenType
	 *
	 * @param aDeduceTypes2
	 */
	override fun genCIForGenType2__(aDeduceTypes2: DeduceTypes2) {
		genCI((nonGenericTypeName)!!, aDeduceTypes2, aDeduceTypes2._errSink(), aDeduceTypes2.phase)
		val invocation: IInvocation? = ci
		if (invocation is NamespaceInvocation) {
			invocation.resolveDeferred().then(object : DoneCallback<EvaNamespace?> {
				override fun onDone(result: EvaNamespace?) {
					node = result
				}
			})
		} else if (invocation is ClassInvocation) {
			invocation.resolvePromise().then(object : DoneCallback<EvaClass?> {
				override fun onDone(result: EvaClass?) {
					node = result
				}
			})
		} else {
			if (resolved is OS_FuncExprType) {
				val genf: GenerateFunctions = aDeduceTypes2
						.getGenerateFunctions((resolved as OS_FuncExprType).getElement().getContext().module())
				val fi: FunctionInvocation = aDeduceTypes2._phase()
						.newFunctionInvocation((resolved as OS_FuncExprType).getElement() as BaseFunctionDef?, null, null)
				val gen: WlGenerateFunction = WlGenerateFunction(genf, fi, aDeduceTypes2._phase().codeRegistrar)
				gen.run(null)
				node = gen.result
			} else if (resolved is OS_FuncType) {
				val y: Int = 2
			} else if (resolved is OS_BuiltinType) {
				// passthrough
			} else throw IllegalStateException("invalid invocation")
		}
	}

	override fun getCi(): IInvocation {
		return (ci)!!
	}

	fun getDrType(): DR_Type? {
		return drType
	}

	override fun getFunctionInvocation(): FunctionInvocation {
		return (functionInvocation)!!
	}

	override fun getNode(): EvaNode {
		return (node)!!
	}

	override fun getNonGenericTypeName(): TypeName {
		return (nonGenericTypeName)!!
	}

	override fun getResolved(): OS_Type {
		return (resolved)!!
	}

	override fun getResolvedn(): NamespaceStatement {
		return (resolvedn)!!
	}

	override fun getTypeName(): OS_Type {
		return (typeName)!!
	}

//    override fun hashCode(): Int {
//        var result: Int = if (resolvedn != null) resolvedn.hashCode() else 0
//        result = 31 * result + (if (typeName != null) typeName.hashCode() else 0)
//        result = 31 * result + (if (nonGenericTypeName != null) nonGenericTypeName.hashCode() else 0)
//        result = 31 * result + (if (resolved != null) resolved.hashCode() else 0)
//        result = 31 * result + (if (ci != null) ci.hashCode() else 0)
//        result = 31 * result + (if (node != null) node.hashCode() else 0)
//        result = 31 * result + (if (functionInvocation != null) functionInvocation.hashCode() else 0)
//        return result
//    }

	override fun isNull(): Boolean {
		if (resolvedn != null) return false
		if (typeName != null) return false
		if (nonGenericTypeName != null) return false
		if (resolved != null) return false
		if (ci != null) return false
		return node == null
	}

	override fun set(aType: OS_Type) {
		when (aType.getType()) {
			OS_Type.Type.USER -> typeName = aType
			OS_Type.Type.USER_CLASS -> {
				resolved = aType
				SimplePrintLoggerToRemoveSoon.println_err_2("48 Unknown in set: " + aType)
			}

			else -> SimplePrintLoggerToRemoveSoon.println_err_2("48 Unknown in set: " + aType)
		}
	}

	override fun setCi(aCi: IInvocation) {
		ci = aCi
	}

	override fun setDrType(aDrType: DR_Type) {
		drType = aDrType
	}

	override fun setFunctionInvocation(aFunctionInvocation: FunctionInvocation) {
		functionInvocation = aFunctionInvocation
	}

	override fun setNode(aNode: EvaNode) {
		node = aNode
	}

	override fun setNonGenericTypeName(aNonGenericTypeName: TypeName) {
		nonGenericTypeName = aNonGenericTypeName
	}

	override fun setResolved(aResolved: OS_Type) {
		resolved = aResolved
	}

	override fun setResolvedn(aResolvedn: NamespaceStatement) {
		resolvedn = aResolvedn
	}

	override fun setTypeName(aTypeName: OS_Type) {
		typeName = aTypeName
	}

	override fun genCI(classStatement: ClassStatement, deduceTypes2: DeduceTypes2?, errSink: ErrSink, phase: DeducePhase): ClassInvocation {
		return genCIFrom(classStatement, deduceTypes2!!).ci as ClassInvocation
	}
}

fun genCIFrom(best: ClassStatement, deduceTypes2: DeduceTypes2): GenType {
	val genType: GenTypeImpl = GenTypeImpl()
	genType.setResolved(OS_UserClassType(best))
	// ci, typeName, node
	// genType.
	genType.genCI(null as TypeName, deduceTypes2, deduceTypes2._errSink(), deduceTypes2._phase())
	return genType
}

