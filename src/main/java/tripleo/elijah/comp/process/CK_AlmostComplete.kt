package tripleo.elijah.comp.process

import tripleo.elijah.comp.graph.i.CK_Action
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.graph.i.CK_StepsContext
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation

class CK_AlmostComplete : CK_Action {
    override fun execute(context1: CK_StepsContext, aMonitor: CK_Monitor): Operation<Ok> {
        val context = context1 as CD_CRS_StepsContext
        val crState = context.state
        val compilationRunner = crState!!.runner()

        compilationRunner!!._cis().almostComplete()
        return Operation.success(Ok.instance())
    }
}
