/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.AliasStatementImpl
import tripleo.elijah.lang.impl.BaseFunctionDef
import tripleo.elijah.lang.types.OS_FuncType
import tripleo.elijah.stages.deduce.DeduceTypes2.OS_SpecialVariable
import tripleo.elijah.stages.deduce.declarations.DeferredMemberFunction
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_VariableTableEntry
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.VariableTableType
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.stream.Collectors

/**
 * Created 11/30/21 1:32 AM
 */
class DeduceLocalVariable(private val variableTableEntry: VariableTableEntry) {
	class MemberInvocation(val element: OS_Element, @JvmField val role: Role) {
		enum class Role {
			DIRECT, INHERITED
		}
	}

	var type: DeferredObject2<GenType, Void, Void> = DeferredObject2()
	private var context: Context? = null
	private var de3: DeduceElement3_VariableTableEntry? = null
	private var deduceTypes2: DeduceTypes2? = null

	private var generatedFunction: BaseEvaFunction? = null

	private fun ___pt1_work_001(generatedFunction: BaseEvaFunction, e: OS_Element,
	                            self_class: OS_Element?): OS_Element? {
		val Self: OS_Element?
		val e_parent = e.parent

		var state/*: Short*/ = 0
		var b: ClassStatement? = null

		if (e_parent === self_class) {
			state = 1
		} else {
			b = class_inherits(self_class as ClassStatement, e_parent)
			state = if (b != null) 3
			else 2
		}

		when (state) {
			1 -> {
				val self1 = generatedFunction.vte_lookup("self")
				assert(self1 is IntegerIA)
				Self = deduceTypes2!!._inj().new_DeduceTypes2_OS_SpecialVariable((self1 as IntegerIA?)!!.entry,
						VariableTableType.SELF, generatedFunction)
			}

			2 -> Self = e_parent
			3 -> {
				val self2 = generatedFunction.vte_lookup("self")
				assert(self2 is IntegerIA)
				Self = deduceTypes2!!._inj().new_DeduceTypes2_OS_SpecialVariable((self2 as IntegerIA?)!!.entry,
						VariableTableType.SELF, generatedFunction)
				(Self as OS_SpecialVariable?)!!.memberInvocation = deduceTypes2!!._inj().new_MemberInvocation(b,
						MemberInvocation.Role.INHERITED)
			}

			else -> throw IllegalStateException()
		}
		return Self
	}

	private fun ___pt1_work_001b(generatedFunction: BaseEvaFunction, dp: DeducePath,
	                             self_class: OS_Element, aO: Array<Any>): OS_Element? {
		val e: OS_Element? = null

		val Self: OS_Element?

		assert(e != null // README 12/29 born to fail
		)
		val e_parent = e!!.parent

		var state/*: Short*/ = 0
		var b: ClassStatement? = null

		if (e_parent === self_class) {
			state = 1
		} else {
			b = class_inherits(self_class as ClassStatement, e_parent)
			state = if (b != null) 3
			else 2
		}

		when (state) {
			1 -> {
				val self1 = generatedFunction.vte_lookup("self")
				assert(self1 is IntegerIA)
				Self = deduceTypes2!!._inj().new_DeduceTypes2_OS_SpecialVariable((self1 as IntegerIA?)!!.entry,
						VariableTableType.SELF, generatedFunction)
			}

			2 -> Self = e_parent
			3 -> {
				val self2 = generatedFunction.vte_lookup("self")
				assert(self2 is IntegerIA)
				Self = deduceTypes2!!._inj().new_DeduceTypes2_OS_SpecialVariable((self2 as IntegerIA?)!!.entry,
						VariableTableType.SELF, generatedFunction)
				(Self as OS_SpecialVariable?)!!.memberInvocation = deduceTypes2!!._inj().new_MemberInvocation(b,
						MemberInvocation.Role.INHERITED)
			}

			else -> throw IllegalStateException()
		}
		return Self
	}

	private fun __pt_work_002(vte: VariableTableEntry, procTableEntry: ProcTableEntry,
	                          Self: OS_Element?) {
		val dm: DeferredMemberFunction
		val pdm = DeferredObject<DeferredMemberFunction, Void, Void>()

		pdm.then { dm1: DeferredMemberFunction ->
			dm1.externalRef().then { NotImplementedException.raise() }
			dm1.typePromise().then { result -> procTableEntry.resolveType(result) }
			procTableEntry.typePromise().then { result: GenType ->
				vte.typeTableEntry.setAttached(result)
				vte.resolveType(result)

				var node = result.node

				if (node == null) {
					// result.genCI(null, deduceTypes2, deduceTypes2._errSink(),
					// deduceTypes2._phase());
					result.genCIForGenType2__(deduceTypes2)
					node = result.node
					assert(node != null)
				}
				vte.resolveTypeToClass(node!!)
			}
		}

		val resolvedElement = procTableEntry.resolvedElement
		val functionInvocation = procTableEntry.functionInvocation
		if (resolvedElement is BaseFunctionDef) {
			dm = deduceTypes2!!.deferred_member_function(Self, null, (resolvedElement as BaseFunctionDef?)!!, functionInvocation!!)
			pdm.resolve(dm)
		} else {
			procTableEntry.onFunctionInvocation { //fi: FunctionInvocation ->
				val fi: FunctionInvocation = it!!

				if (fi.function is ConstructorDef) {
					val cd = fi.function as ConstructorDef
					val dm2 = deduceTypes2!!.deferred_member_function(Self, null, cd, functionInvocation!!)

					pdm.resolve(dm2)
				} else {
					SimplePrintLoggerToRemoveSoon.println_err_4("********************************* not a Constructor")
				}
			}
		}
	}

	private fun __pt_work_002b(vte: VariableTableEntry, procTableEntry: ProcTableEntry,
	                           o: Array<Any?>) {
		val Self = o[0] as OS_Element?

		val resolvedElement1 = procTableEntry.resolvedElement
		var resolvedElement0 = resolvedElement1

		while (resolvedElement0 is AliasStatementImpl) {
			try {
				resolvedElement0 = DeduceLookupUtils._resolveAlias2((resolvedElement1 as AliasStatementImpl?)!!,
						deduceTypes2!!)
			} catch (aE: ResolveError) {
				// throw new RuntimeException(aE);
				return
			}
		}

		if (Self == null) {
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")
			SimplePrintLoggerToRemoveSoon.println_err_2("336 ======================================================")

			return
		}

		val dm = deduceTypes2!!.deferred_member_function(Self, null,
				(resolvedElement0 as BaseFunctionDef?)!!, procTableEntry.functionInvocation!!)
		dm.externalRef().then { NotImplementedException.raise() }
		dm.typePromise().then { result -> procTableEntry.typeDeferred().resolve(result) }
		procTableEntry.typePromise().then { result ->
			vte.typeTableEntry.setAttached(result)
			vte.resolveType(result)
			vte.resolveTypeToClass(result.node)
		}
	}

	fun resolve_var_table_entry_for_exit_function() {
		val vte = variableTableEntry
		val ctx = context

		if (vte.vtt == VariableTableType.TEMP) {
			val genType = vte.typeTableEntry.genType
			val pts = vte.potentialTypes().size
			if (genType.typeName != null && genType.typeName === genType.resolved) {
				try {
					genType.resolved = deduceTypes2
							?.resolve_type(genType.typeName, ctx /* genType.typeName.getTypeName().getContext() */)
							?.resolved

					if (genType is ForwardingGenType) {
						genType.unsparkled()
					}

					genType.genCIForGenType2(deduceTypes2)
					vte.resolveType(genType)
					vte.resolveTypeToClass(genType.node)
					val y = 2
				} catch (aResolveError: ResolveError) {
//					aResolveError.printStackTrace();
					deduceTypes2!!._errSink().reportDiagnostic(aResolveError)
				}
			}
		}

		if (vte.resolvedElement == null) return
		run {
			if (vte.typeTableEntry.attached == null && vte.constructable_pte != null) {
				val c = vte.constructable_pte?.functionInvocation?.classInvocation?.klass
				val attached = c?.oS_Type
				// TODO this should have been set somewhere already
				// typeName and nonGenericTypeName are not set
				// but at this point probably wont be needed
				vte.typeTableEntry.genType.resolved = attached
				vte.typeTableEntry.attached = attached
			}
			if (vte.typeTableEntry.attached == null && vte.potentialTypes().size > 0) {
				val attached_list = vte.potentialTypes().stream()
						.filter { x: TypeTableEntry -> x.attached != null }.collect(Collectors.toList())

				if (attached_list.size == 1) {
					val pot = attached_list[0]
					vte.typeTableEntry.attached = pot.attached
					vte.typeTableEntry.genType.genCI(null as ClassStatement, deduceTypes2, deduceTypes2!!._errSink(), deduceTypes2!!.phase)
					val classInvocation = vte.typeTableEntry.genType.ci as ClassInvocation
					classInvocation.resolvePromise().then { result ->
						vte.resolveTypeToClass(result)
						vte.genType.copy(vte.typeTableEntry.genType) // TODO who knows if this is necessary?
					}
				} else {
					resolve_var_table_entry_potential_types_1(vte, generatedFunction!!)
				}
			} else if (vte.typeTableEntry.attached == null && vte.potentialTypes().size == 0) {
				NotImplementedException.raise()
			}
			run {
				val genType = vte.typeTableEntry.genType
				val pts = vte.potentialTypes().size
				if (genType.typeName != null && genType.typeName === genType.resolved) {
					try {
						genType.resolved = deduceTypes2!!.resolve_type(genType.typeName,
								ctx /* genType.typeName.getTypeName().getContext() */).resolved
						genType.genCIForGenType2(deduceTypes2)
						vte.resolveType(genType)
						vte.resolveTypeToClass(genType.node)
					} catch (aResolveError: ResolveError) {
//						aResolveError.printStackTrace();
						deduceTypes2!!._errSink().reportDiagnostic(aResolveError)
					}
				}
			}
			vte.setStatus(BaseTableEntry.Status.KNOWN,
					deduceTypes2!!._inj().new_GenericElementHolder(vte.resolvedElement))
			run {
				val genType = vte.typeTableEntry.genType
				if (genType.resolved != null && genType.node == null) {
					if (genType.resolved.type != OS_Type.Type.USER_CLASS
							&& genType.resolved.type != OS_Type.Type.FUNCTION) {
						try {
							genType.resolved = deduceTypes2!!.resolve_type(genType.resolved, ctx).resolved
						} catch (aResolveError: ResolveError) {
							aResolveError.printStackTrace()
							assert(false)
						}
					}

					// genCI(genType, genType.nonGenericTypeName);

					//
					// registerClassInvocation does the job of makeNode, so results should be
					// immediately available
					//
					var state/*: Short*/ = 1
					if (vte.callablePTE != null) {
						val callable_pte = vte.callablePTE
						if (callable_pte!!.__debug_expression is FuncExpr) {
							state = 2
						}
					}

					when (state) {
						1 -> genType.genCIForGenType2(deduceTypes2) // TODO what is this doing here? huh?
						2 -> {
							val fe = vte.callablePTE!!.__debug_expression as FuncExpr
							val dpc = vte.callablePTE!!.dpc

							// final DeduceFuncExpr target = (DeduceFuncExpr) dpc.target;
							val target = dpc.target
							NotImplementedException.raise()
						}
					}
					if (genType.ci != null) { // TODO we may need this call...
						(genType.ci as ClassInvocation).resolvePromise().then { result ->
							genType.node = result
							if (!vte.typePromise().isResolved) { // HACK
								if (genType.resolved is OS_FuncType) {
									val resolved = genType.resolved
									result.functionMapDeferred((resolved.getElement() as FunctionDef)
									) { aGeneratedFunction: EvaFunction ->
										// TODO check args (hint functionInvocation.pte)
										// but against what? (vte *should* have callable_pte)
										// if not, then try potential types for a PCE
										aGeneratedFunction.typePromise().then { aGenType: GenType? -> vte.resolveType(aGenType!!) }
									}
								} else vte.resolveType(genType)
							}
						}
					}
				}
			}
		}
	}

	fun resolve_var_table_entry_potential_types_1(vte: VariableTableEntry,
	                                              generatedFunction: BaseEvaFunction) {
		// TODO 06/26 getIdent / reduce potential types...
		if (vte.potentialTypes().size == 1) {
			val tte1 = vte.potentialTypes().iterator().next()
			if (tte1.tableEntry is ProcTableEntry) {
				val procTableEntry = tte1.tableEntry
				val dpc: DeduceProcCall = procTableEntry.deduceProcCall()

				/*
 * DeduceElement t = null; try { t = dpc.target(); } catch (FCA_Stop aE) { throw
 * new RuntimeException(aE); }
 *
 * tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("410 " + t);
 */

				// TODO for argument, we need a DeduceExpression (DeduceProcCall) which is
				// bounud to self
				// (inherited), so we can extract the invocation
				val ia: InstructionArgument = procTableEntry.expression_num!!

				if (ia is IntegerIA) return

				val dp = (ia as IdentIA).entry.buildDeducePath(generatedFunction)

				var Self: OS_Element? = null

				var callbackFlag = false

				val o = arrayOfNulls<Any>(1)

				if (dp.size() == 1) { // ia.getEntry().backlink == null
					val e = dp.getElement(0)
					val self_class = generatedFunction.fd.parent

					if (e == null) {
						// ___pt1_work_001b(generatedFunction, dp, self_class, o);
						callbackFlag = true
					} else {
						Self = ___pt1_work_001(generatedFunction, e, self_class)
					}
				} else Self = dp.getElement(dp.size() - 2) // TODO fix this


				if (callbackFlag) {
					__pt_work_002b(vte, procTableEntry, o)
				} else {
					__pt_work_002(vte, procTableEntry, Self)
				}
			}
		} else {
			throw IllegalStateException("Unexpected value: " + vte.potentialTypes().size)
		}
	}

	fun setDeduceTypes2(aDeduceTypes2: DeduceTypes2?, aContext: Context?,
	                    aGeneratedFunction: BaseEvaFunction?) {
		deduceTypes2 = aDeduceTypes2
		context = aContext
		generatedFunction = aGeneratedFunction

		de3 = deduceTypes2!!._inj().new_DeduceElement3_VariableTableEntry(variableTableEntry, aDeduceTypes2,
				aGeneratedFunction)
	}

	companion object {
		fun class_inherits(aFirstClass: ClassStatement, aInherited: OS_Element?): ClassStatement? {
			if (aInherited !is ClassStatement) return null

			val inh1 = aFirstClass.context.inheritance()
			for ((_, value) in inh1) {
				if (value == aInherited) return aInherited
			}
			return null
		}
	}
}
