package tripleo.elijah.stages.deduce.calculate.rider

import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.ProcTableEntry
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.stages.instructions.InstructionArgument

class __Rider__Implement_construct(val generatedFunction: BaseEvaFunction, val instruction: Instruction,
                                   val expression: InstructionArgument, val pte: ProcTableEntry) 