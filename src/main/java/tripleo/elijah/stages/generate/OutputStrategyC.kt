/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.generate

import org.jetbrains.annotations.Contract
import tripleo.elijah.ci.LibraryStatementPart
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaFunction
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_fn.IEvaConstructor
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.stages.generate.OutputStrategy.Per
import tripleo.elijah.util2.UnintendedUseException
import java.io.File

/**
 * Created 1/13/21 5:54 AM
 */
class OutputStrategyC(private val outputStrategy: OutputStrategy) {
    inner class OSC_NFC(var lsp: String?, var dir: String?, var basename: String, var extension: String) : EOT_FileNameProvider {
        private val filename__: String
            get() {
                val sb = StringBuilder()
                //			sb.append("/");
                if (lsp != null && !lsp!!.isEmpty()) {
                    sb.append("/")
                    sb.append(lsp)
                }
                if (dir != null && !dir!!.isEmpty()) {
                    sb.append("/")
                    sb.append(dir)
                }
                if (!basename.isEmpty()) {
                    sb.append("/")
                    sb.append(basename)
                }
                sb.append(extension)
                return sb.toString()
            }

        override fun getFilename(): String {
            return this.filename__
        }

        override fun toString(): String {
            return ("OSC_NFC{" + "lsp='" + lsp + '\'' + ", dir='" + dir + '\'' + ", extension='" + extension + '\''
                + ", basename='" + basename + '\'' + '}')
        }
    }

    inner class OSC_NFCo(val filename__: String) : EOT_FileNameProvider {
        override fun getFilename(): String {
            throw UnintendedUseException("fix this when the fixing occurs")
        }
    }

    inner class OSC_NFF(val filename__: String) : EOT_FileNameProvider {
        override fun getFilename(): String {
            throw UnintendedUseException("fix this when the fixing occurs")
        }
    }

    inner class OSC_NFN(private val lsp0: String?,
                        private val name0: String?,
                        private val filename__: String,
                        private val extension0: String?) : EOT_FileNameProvider {
        private val s: String? = null

        override fun getFilename(): String {
            if (s == null) {
                val sb = "$lsp0/$name0/$filename__$extension0"
                return sb
            }
            return s
        }
    }

    inner class OSC_NFN_(aX: EvaNamespace, aTy: TY) : EOT_FileNameProvider {
        private val s: String = nameForNamespace(aX, aTy)

        val filename__: String
            get() = s // inline s and cache/lazy

        override fun getFilename(): String = filename__
    }

    private fun a_name(aEvaClass: EvaClass, lsp: LibraryStatementPart): String {
        val name0: String

        when (outputStrategy.per()) {
            Per.PER_CLASS -> {
                name0 = if (aEvaClass.isGeneric) aEvaClass.numberedName
                else aEvaClass.name
            }

            Per.PER_MODULE -> {
                val mod = aEvaClass.module()
                val f = File(mod.fileName)
                val ff = f.name
                name0 = strip_elijah_extension(ff)
            }

            Per.PER_PACKAGE -> {
                val pkg2 = aEvaClass.klass.packageName
                val pkgName = if (pkg2 !== LangGlobals.default_package) { // FIXME ??
                    "__default_package-224"
                } else {
                    pkg2.name
                }
                name0 = pkgName
            }

            Per.PER_PROGRAM -> {
                val xx = lsp.instructions
                val n = xx.name
                name0 = n
            }

            else -> throw IllegalStateException("Unexpected value: " + outputStrategy.per())
        }
        return name0
    }

    @Contract(pure = true)
    private fun a_pkg(aEvaClass: EvaClass): String {
        val pkg0 = aEvaClass.klass.packageName
        val pkg: OS_Package?

        val dir0: String
        if (pkg0 !== LangGlobals.default_package) {
            pkg = pkg0 ?: findPackage(aEvaClass.klass)

            dir0 = (pkg!!.name)
        } else {
            dir0 = "" // _default_package-255";
        }

        return dir0
    }

    @Contract(pure = true)
    fun Extension0(aTy: TY): String {
        return when (aTy) {
            TY.IMPL -> ".c"
            TY.PRIVATE_HEADER -> "_Priv.h"
            TY.HEADER -> ".h"
            else -> throw IllegalStateException("Unexpected value: $aTy")
        }
    }

    @Contract(pure = true)
    private fun findPackage(ee: OS_Element): OS_Package? {
        var e: OS_Element? = ee
        while (e != null) {
            e = e.parent
            if (e!!.context.parent === e!!.context) e = null
            else {
                val t = DecideElObjectType.getElObjectType(e!!)
                when (t) {
                    ElObjectType.NAMESPACE -> if ((e as NamespaceStatement?)!!.packageName != null) return (e as NamespaceStatement?)!!.packageName
                    ElObjectType.CLASS -> if ((e as ClassStatement?)!!.packageName != null) return (e as ClassStatement?)!!.packageName
                    ElObjectType.FUNCTION -> continue
                    else ->                    // datatype, enum, alias
                        continue
                }
            }
        }
        return null
    }

    private fun n_pkg(generatedNamespace: EvaNamespace, pkg: OS_Package?): String {
        var pkg = pkg
        val name0: String
        if (pkg !== LangGlobals.default_package) {
            if (pkg == null) pkg = findPackage(generatedNamespace.namespaceStatement)
            name0 = pkg!!.name
        } else {
            name0 = "#default_package-349"
        }
        return name0
    }

    fun nameForClass(aX: EvaClass, aTy: TY): String {
        return nameForClass1(aX, aTy).getFilename()
    }

    fun nameForClass1(aEvaClass: EvaClass, aTy: TY): OSC_NFC {
        if (aEvaClass.module().isPrelude) {
            // We are dealing with the Prelude
            return OSC_NFC("Prelude", "Prelude", "", Extension0(aTy))
        }

        val lsp0: String
        val name0: String

        val lsp = aEvaClass.module().lsp
        lsp0 = a_lsp(lsp)

        val dir0 = a_pkg(aEvaClass)
        name0 = a_name(aEvaClass, lsp)
        val extension0 = Extension0(aTy)

        return OSC_NFC(lsp0, dir0, name0, extension0)
    }

    @Contract(pure = true)
    fun nameForConstructor(aEvaConstructor: IEvaConstructor,
                           aTy: TY): String? {
        var c = aEvaConstructor.genClass
        if (c == null) c = aEvaConstructor.parent // TODO fixme

        if (c is EvaClass) return nameForClass(c, aTy)
        else if (c is EvaNamespace) return nameForNamespace(c, aTy)
        return null
    }

    fun nameForConstructor1(aGf: IEvaConstructor,
                            aTy: TY): OSC_NFCo {
        return OSC_NFCo(nameForConstructor(aGf, aTy)!!)
    }

    @Contract(pure = true)
    fun nameForFunction(generatedFunction: EvaFunction, aTy: TY): String? {
        var c = generatedFunction.genClass
        if (c == null) {
            assert(false)
            c = generatedFunction.parent // TODO fixme
        }
        if (c is EvaClass) return nameForClass(c, aTy)
        else if (c is EvaNamespace) return nameForNamespace(c, aTy)
        assert(false)
        return null
    }

    fun nameForFunction1(aGf: EvaFunction,
                         aTy: TY): EOT_FileNameProvider {
        return OSC_NFF(nameForFunction(aGf, aTy)!!)
    }

    fun nameForNamespace(generatedNamespace: EvaNamespace, aTy: TY): String {
        if (generatedNamespace.module().isPrelude) {
            // We are dealing with the Prelude
            val lsp0 = ("/Prelude/")
            val name0 = ("Prelude")
            val filename = ""
            val extension0 = Extension0(aTy)
            return OSC_NFN(lsp0, name0, filename, extension0).getFilename()
        }
        var filename: String
        if (generatedNamespace.namespaceStatement.kind == NamespaceTypes.MODULE) {
            val moduleFileName = generatedNamespace.module().fileName
            val moduleFile = File(moduleFileName)
            filename = moduleFile.name
            filename = strip_elijah_extension(filename)

            if (generatedNamespace.code != 0) {
                filename = "elns-%d-%s".formatted(generatedNamespace.code, filename) // hoohoo
            }
//            if (generatedNamespace.code1234567 != 0) {
//                filename = "elns-%d-%s".formatted(generatedNamespace.code1234567, filename) // hoohoo
//            }
        } else {
            filename = generatedNamespace.name
        }

        val lsp0 = n_lsp(generatedNamespace)

        val pkg = generatedNamespace.namespaceStatement.packageName
        val name0 = n_pkg(generatedNamespace, pkg)

        val extension0 = Extension0(aTy)

        return OSC_NFN(lsp0, name0, filename, extension0).getFilename()
    }

    fun nameForNamespace1(aX: EvaNamespace,
                          aTy: TY): EOT_FileNameProvider {
        return OSC_NFN_(aX, aTy)
    }

    fun strip_elijah_extension(aFilename: String): String {
        var aFilename = aFilename
        if (aFilename.endsWith(".elijah")) {
            aFilename = aFilename.substring(0, aFilename.length - 7)
        } else if (aFilename.endsWith(".elijjah")) {
            aFilename = aFilename.substring(0, aFilename.length - 8)
        }
        return aFilename
    }

    companion object {
        @Contract(pure = true)
        private fun a_lsp(lsp: LibraryStatementPart?): String {
            val lsp0 = if (lsp == null) {
                // sb.append("______________");
                ""
            } else {
//			sb.append(generatedClass.module.lsp.getName());
                lsp.instructions.name
            }
            return lsp0
        }

        private fun n_lsp(generatedNamespace: EvaNamespace): String {
            val lsp0: String
            val lsp = generatedNamespace.module().lsp
            lsp0 = if (lsp == null) {
                // sb.append("___________________");
                ""
            } else {
                lsp.instructions.name
            }
            return lsp0
        }
    }
}
