package tripleo.elijah.stages.gen_c

import tripleo.elijah.nextgen.outputstatement.ReasonedStringListStatement
import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.stages.instructions.IntegerIA
import java.util.function.Supplier

internal class GCFM_Inst_AGN //generateCodeForMethod = aGenerateCodeForMethod;
(aGenerateCodeForMethod: Generate_Code_For_Method?,
 private val gc: GenerateC,
        //private final Generate_Code_For_Method generateCodeForMethod;
 private val yf: WhyNotGarish_BaseFunction,
 private val instruction: Instruction) : GenerateC_Statement {
    private var _calculated = false

    private var _calculatedText: String? = null

    override fun getText(): String {
        if (!_calculated) {
            val target = instruction.getArg(0)
            val value = instruction.getArg(1)

            if (target is IntegerIA) {
                val realTarget = yf.getRealTargetName(gc, target, AOG.ASSIGN)
                val assignmentValue = yf.getAssignmentValue(gc, value)

                val z = ReasonedStringListStatement()
                z.append(Emit.emit("/*267*/"), "emit-code")
                z.append(realTarget!!, "real-target")
                z.append(" = ", "equals-sign")
                z.append(assignmentValue!!, "assignment-value")
                z.append(";", "closing-semi")

                _calculatedText = z.text
            } else {
                val assignmentValue = yf.getAssignmentValue(gc, value)

                val zz = ReasonedStringListStatement()
                zz.append(Emit.emit("/*501*/"), "emit-code")
                val reasonedString = yf.getRealTargetNameReasonedString(gc, target as IdentIA, assignmentValue, "real-target", AOG.ASSIGN)
                zz.append(reasonedString!!)

                val z = ReasonedStringListStatement()
                z.append(Emit.emit("/*249*/"), "emit-code")
                z.append(zz, "real-target")
                z.append(" = ", "equals-sign")
                z.append(assignmentValue!!, "assignment-value")
                z.append(";", "closing-semi")

                _calculatedText = z.text
            }
            _calculated = true
        }

        assert(_calculatedText != null)
        return _calculatedText!!
    }

    val text2: String?
        get() {
            if (!_calculated) {
                val z = ReasonedStringListStatement()

                val target = instruction.getArg(0)
                val value = instruction.getArg(1)

                if (target is IntegerIA) {
                    val realTargetSupplier = Supplier { yf.getRealTargetName(gc, target, AOG.ASSIGN) }
                    val assignmentValueSupplier = Supplier { yf.getAssignmentValue(gc, value) }

                    z.append(realTargetSupplier as Supplier<String>, "real-target-name")
                    z.append(assignmentValueSupplier, "assignment-value")

                    _calculatedText = String.format(Emit.emit("/*267*/") + "%s = %s;", realTargetSupplier.get(),
                            assignmentValueSupplier.get())
                } else {
                    val assignmentValueSupplier = Supplier { yf.getAssignmentValue(gc, value) }
                    val s = Supplier { yf.getRealTargetName(gc, target as IdentIA, assignmentValueSupplier.get()).forAOG(AOG.ASSIGN) }

                    val realTargetName = s.get()

                    val s2 = Emit.emit("/*501*/") + realTargetName

                    _calculatedText = String.format(Emit.emit("/*249*/") + "%s = %s;", s2, assignmentValueSupplier.get())
                }
                _calculated = true
            }

            return _calculatedText
        }

    override fun rule(): GCR_Rule {
        return GCR_Rule.withMessage("GCFM_Inst_AGN")
    }
}
