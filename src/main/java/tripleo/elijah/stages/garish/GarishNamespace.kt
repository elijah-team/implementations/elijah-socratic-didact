package tripleo.elijah.stages.garish

import org.jetbrains.annotations.Contract
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.util.BufferTabbedOutputStream
import tripleo.elijah.world.i.LivingNamespace

class GarishNamespace // _lc.setGarish(this);
@Contract(pure = true) constructor(val living: LivingNamespace) {
    fun garish(aGenerateC: GenerateC?,
               gr: GenerateResult?,
               aResultSink: GenerateResultSink) {
        living.generateWith(aResultSink, this, gr, aGenerateC)
    }

    fun getHeaderBuffer(aGenerateC: GenerateC,
                        x: EvaNamespace, class_name: String?, class_code: Int): BufferTabbedOutputStream {
        val tosHdr = BufferTabbedOutputStream()

        tosHdr.put_string_ln("typedef struct {")
        tosHdr.incr_tabs()
        // tosHdr.put_string_ln("int _tag;");
        for (o in x.varTable) {
            val typeName = aGenerateC.getTypeNameGNCForVarTableEntry(o)

            tosHdr.put_string_ln(String.format("%s* vm%s;", if (o.varType == null) "void " else typeName, o.nameToken))
        }

        tosHdr.dec_tabs()
        tosHdr.put_string_ln("")
        tosHdr.put_string_ln(
                String.format("} %s; // namespace `%s' in %s", class_name, x.name, x.module().fileName))
        // TODO "instance" namespaces
        tosHdr.put_string_ln("")
        tosHdr.put_string_ln(
                String.format("%s* zN%d_instance; // namespace `%s'", class_name, class_code, x.name))
        tosHdr.put_string_ln("")

        tosHdr.put_string_ln("")
        tosHdr.put_string_ln("")
        for (o in x.varTable) {
            // final String typeName = getTypeNameForVarTableEntry(o);
            tosHdr.put_string_ln(String.format("R->vm%s = 0;", o.nameToken))
        }

        tosHdr.close()
        return tosHdr
    }

    fun getImplBuffer(ignoredX: EvaNamespace,
                      class_name: String?, class_code: Int): BufferTabbedOutputStream {
        val tos = BufferTabbedOutputStream()

        tos.put_string_ln(String.format("%s* ZNC%d() {", class_name, class_code))
        tos.incr_tabs()

        // TODO multiple calls of namespace function (need if/else statement)
        tos.put_string_ln(String.format("%s* R = GC_malloc(sizeof(%s));", class_name, class_name))
        // tos.put_string_ln(String.format("R->_tag = %d;", class_code));
        tos.put_string_ln("")
        tos.put_string_ln(String.format("zN%d_instance = R;", class_code))
        tos.put_string_ln("return R;")
        tos.dec_tabs()
        tos.put_string_ln(String.format("} // namespace `%s'", class_name /* x.getName() */))
        tos.put_string_ln("")

        tos.flush()
        tos.close()
        return tos
    }
}
