package tripleo.elijah.comp

import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.util2.UnintendedUseException

class CompilerInstructionsObserver(private val compilation: Compilation) : Observer<CompilerInstructions>, ICompilerInstructionsObserver {
    private val l: MutableList<CompilerInstructions> = ArrayList()

    override fun almostComplete() {
        val pipelineAccessPromise = compilation.compilationEnclosure.pipelineAccessPromise
        pipelineAccessPromise.register(compilation.fluffy)

        pipelineAccessPromise.then { pa0: IPipelineAccess? ->
//            compilation.hasInstructions(l, pa0!!)
            System.err.println(""+18181818)
        }
    }

    override fun onComplete() {
        throw UnintendedUseException()
    }

    override fun onError(e: Throwable) {
        throw UnintendedUseException()
    }

    override fun onNext(aCompilerInstructions: CompilerInstructions) {
        l.add(aCompilerInstructions)
    }

    override fun onSubscribe(d: Disposable) {
        // Disposable x = d;
        // NotImplementedException.raise();
    }
}
