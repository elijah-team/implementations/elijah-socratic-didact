package tripleo.elijah.stages.deduce.fluffy.impl

import tripleo.elijah.diagnostic.Locatable
import tripleo.elijah.nextgen.composable.IComposable
import tripleo.elijah.stages.deduce.fluffy.i.FluffyVar
import tripleo.elijah.stages.deduce.fluffy.i.FluffyVarTarget

class FluffyVarImpl : FluffyVar {
    override fun name(): String? {
        return null
    }

    override fun nameComposable(): IComposable? {
        return null
    }

    override fun nameLocatable(): Locatable? {
        return null
    }

    override fun target(): FluffyVarTarget? {
        return null
    }
}
