package tripleo.elijah.stages.deduce.post_bytecode

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.*
import tripleo.elijah.stages.deduce.*
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.BaseTableEntry
import tripleo.elijah.stages.gen_fn.IdentTableEntry
import tripleo.elijah.stages.gen_fn.IdentTableEntry.ITE_Resolver_Result
import tripleo.elijah.stages.gen_fn.ProcTableEntry
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stateful.DefaultStateful
import tripleo.elijah.stateful.State
import tripleo.elijah.stateful.StateRegistrationToken
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

//import tripleo.elijah.stateful.annotation.processor.StatefulProperty;
//@StatefulProperty
class ExitGetType : State {
    private class __foundElement_hasIdteType__ITE_Resolver : ITE_Resolver {
        override fun check() {
            val y = 2
        }

        @Contract(pure = true)
        override fun getResult(): ITE_Resolver_Result? {
            return null
        }

        override fun isDone(): Boolean {
            return false
        }
    }

    private class __foundElement_noIdteType__ITE_Resolver : ITE_Resolver {
        override fun check() {
            val y = 2
        }

        @Contract(pure = true)
        override fun getResult(): ITE_Resolver_Result? {
            return null
        }

        override fun isDone(): Boolean {
            return false
        }
    }

    private var identity: StateRegistrationToken? = null

    override fun apply(element: DefaultStateful) {
        val ite_de = (element as DeduceElement3_IdentTableEntry)
        val ite = ite_de.principal
        val generatedFunction1 = ite_de.generatedFunction()
        val dt2 = ite_de.deduceTypes2
        val phase1 = ite_de.deduceTypes2!!._phase()
        val aFd_ctx = ite_de.fdCtx
        val aContext = ite_de.context

        assign_type_to_idte(ite, generatedFunction1, aFd_ctx!!, aContext!!, dt2!!, phase1)
    }

    fun assign_type_to_idte(ite: IdentTableEntry,
                            generatedFunction: BaseEvaFunction, aFunctionContext: Context,
                            aContext: Context, dt2: DeduceTypes2, phase: DeducePhase) {
        if (!ite.hasResolvedElement()) {
            val ident_a = IdentIA(ite.index, generatedFunction)
            dt2.resolveIdentIA_(aContext, ident_a, generatedFunction, object : FoundElement(phase) {
                val path: String = generatedFunction.getIdentIAPathNormal(ident_a)

                val resolver000: __foundElement_noIdteType__ITE_Resolver = __foundElement_noIdteType__ITE_Resolver()

                val resolver001: __foundElement_hasIdteType__ITE_Resolver = __foundElement_hasIdteType__ITE_Resolver()

                private fun __foundElement_hasIdteType() {
                    ite.addResolver(resolver001)

                    when (ite.type?.attached!!.type) {
                        OS_Type.Type.USER -> {
                            try {
                                val xx = dt2.resolve_type(ite.type?.attached!!, aFunctionContext)
                                ite.type!!.setAttached(xx)
                            } catch (resolveError: ResolveError) {
                                dt2._LOG().info("192 Can't attach type to $path")
                                dt2._errSink().reportDiagnostic(resolveError)
                            }
                            if (ite.type!!.attached!!.type == OS_Type.Type.USER_CLASS) {
                                use_user_class(ite.type!!.attached!!, ite)
                            }
                        }

                        OS_Type.Type.USER_CLASS -> use_user_class(ite.type!!.attached!!, ite)
                        OS_Type.Type.FUNCTION -> {
                            // TODO All this for nothing
                            // the ite points to a function, not a function call,
                            // so there is no point in resolving it
                            if (ite.type?.tableEntry is ProcTableEntry) {
                            } else if (ite.type?.tableEntry is IdentTableEntry) {
                                val identTableEntry = ite.type!!.tableEntry as IdentTableEntry
                                if (identTableEntry.callablePTE != null) {
                                    val cpte: ProcTableEntry = identTableEntry.callablePTE!!
                                    cpte.typePromise().then { result ->
                                        SimplePrintLoggerToRemoveSoon
                                                .println2("1483 " + result.resolved + " " + result.node)
                                    }
                                }
                            }
                        }

                        else -> throw IllegalStateException("Unexpected value: " + ite.type!!.attached!!.type)
                    }
                }

                private fun __foundElement_noIdteType(x: OS_Element) {
                    ite.addResolver(resolver000)

                    val yy = 2
                    if (!ite.hasResolvedElement()) {
                        var lrl: LookupResultList? = null
                        try {
                            lrl = DeduceLookupUtils.lookupExpression(ite.ident, aFunctionContext, dt2)!!
                            val best = lrl.chooseBest(null)
                            if (best != null) {
                                ite.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(x))
                                if (ite.type != null && ite.type!!.attached != null) {
                                    if (ite.type!!.attached!!.type == OS_Type.Type.USER) {
                                        try {
                                            val xx = dt2.resolve_type(ite.type!!.attached!!,
                                                    aFunctionContext)
                                            ite.type!!.setAttached(xx)
                                        } catch (resolveError: ResolveError) { // TODO double catch
                                            dt2._LOG().info("210 Can't attach type to " + ite.ident)
                                            dt2._errSink().reportDiagnostic(resolveError)
                                            // continue;
                                        }
                                    }
                                }
                            } else {
                                dt2._LOG().err("184 Couldn't resolve " + ite.ident)
                            }
                        } catch (aResolveError: ResolveError) {
                            dt2._LOG().err("184-506 Couldn't resolve " + ite.ident)
                            aResolveError.printStackTrace()
                            dt2._errSink().reportDiagnostic(aResolveError)
                        }
                        if (ite.type?.attached!!.type == OS_Type.Type.USER_CLASS) {
                            use_user_class(ite.type!!.attached!!, ite)
                        }
                    }
                }

                override fun foundElement(x: OS_Element?) {
                    SimplePrintLoggerToRemoveSoon.println_err_4("590-590 $x")

                    if (ite.resolvedElement !== x) ite.setStatus(BaseTableEntry.Status.KNOWN, dt2._inj().new_GenericElementHolder(x))
                    if (ite.type != null && ite.type!!.attached != null) {
                        __foundElement_hasIdteType()
                    } else {
                        __foundElement_noIdteType(x!!)
                    }
                }

                override fun noFoundElement() {
                    ite.setStatus(BaseTableEntry.Status.UNKNOWN, null)
                    dt2._errSink().reportError("165 Can't resolve $path")
                }

                private fun use_user_class(aType: OS_Type, aEntry: IdentTableEntry) {
                    val cs = aType.classOf
                    if (aEntry.constructable_pte != null) {
                        val yyy = 3
                        SimplePrintLoggerToRemoveSoon.println2("use_user_class: $cs")
                    }
                }
            })
        }
    }

    override fun checkState(aElement3: DefaultStateful): Boolean {
        return true
    }

    override fun setIdentity(aId: StateRegistrationToken) {
        identity = aId
    }
}
