package tripleo.elijah.compiler_model

import tripleo.wrap.File

object CM_Factory {
    @JvmStatic
	fun Filename__fromFileToString(file: File): CM_Filename {
        return CM_Filename { file.wrapped().toString() }
    }

    fun Filename__fromParams(aF: String): CM_Filename {
        return CM_Filename { aF }
    }

    @JvmStatic
	fun Filename__of(aFn: String): CM_Filename {
        return CM_Filename { aFn }
    } //static CM_Filename Filename__fromInputRequestCanonicalFileToString(CompFactory.InputRequest aInputRequest) {
    //	var r = fromInputRequestCanonicalFileToStringOperation(aInputRequest);
    //	if (r.mode() == Mode.SUCCESS) {
    //		return r.success();
    //	}
    //	throw new RuntimeException(r.failure());
    //}
    //static Operation<CM_Filename> Filename__fromInputRequestCanonicalFileToStringOperation(CompFactory.InputRequest aInputRequest) {
    //	var file = aInputRequest.file();
    //	var f = aInputRequest.file().toString();
    //	var do_out = aInputRequest.do_out();
    //
    //	final String absolutePath1;
    //	try {
    //		absolutePath1 = file.getCanonicalFile().toString();
    //	} catch (IOException aE) {
    //		return Operation.failure(aE);
    //	}
    //
    //	var r = new CM_Filename() {
    //		@Override
    //		public String getString() {
    //			return absolutePath1;
    //		}
    //	};
    //
    //	return Operation.success(r);
    //}
}
