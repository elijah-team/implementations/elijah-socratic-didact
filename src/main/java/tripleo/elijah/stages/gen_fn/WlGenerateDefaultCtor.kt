/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jetbrains.annotations.Contract
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.*
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.nextgen.DeduceCreationContext
import tripleo.elijah.stages.gen_fn.EvaContainer.VarTableEntry
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.util2.Eventual
import tripleo.elijah.work.WorkJob
import tripleo.elijah.work.WorkManager

/**
 * Created 5/31/21 2:26 AM
 */
class WlGenerateDefaultCtor @Contract(pure = true) constructor(private val generateFunctions: GenerateFunctions,
                                                               private val functionInvocation: FunctionInvocation,
                                                               private val dcc: DeduceCreationContext,
                                                               private val codeRegistrar: ICodeRegistrar) : WorkJob {
	private var _isDone = false
	private val Result: BaseEvaFunction? = null

	@JvmField
	val generated: Eventual<BaseEvaFunction> = Eventual()

	constructor(module: OS_Module?,
	            aFunctionInvocation: FunctionInvocation,
	            crcl: DeduceCreationContext) : this(crcl.generatePhase.getGenerateFunctions(module!!),
			aFunctionInvocation,
			crcl,
			crcl.deducePhase.codeRegistrar)

	private fun getPragma(aAuto_construct: String): Boolean {
		return false
	}

	override fun isDone(): Boolean {
		return _isDone
	}

	override fun run(aWorkManager: WorkManager?) {
		if (functionInvocation.generateDeferred().isPending) {
			val klass = functionInvocation.classInvocation!!.klass

			// TODO 10/17 PromiseExpectation/EventualRegister
			functionInvocation.classInvocation!!.resolvePromise().then { result: EvaClass ->
				val genClass = result
				val cd: ConstructorDef = ConstructorDefImpl(null, klass as _CommonNC, klass.context)
				val scope3 = Scope3Impl(cd)
				cd.setName(LangGlobals.emptyConstructorName)
				cd.scope(scope3)
				for (varTableEntry in genClass.varTable) {
					val element = __getElement(varTableEntry, cd)
					scope3.add(element)
				}

				val classStatement = cd.parent
				assert(classStatement is ClassStatement)
				val gf = generateFunctions.generateConstructor(cd, (classStatement as ClassStatement?)!!, functionInvocation)

				// lgf.add(gf);
				val ci = functionInvocation.classInvocation
				ci!!.resolvePromise().done { result2: EvaClass ->
					codeRegistrar.registerFunction1(gf)
					// gf.setCode(generateFunctions.module.getCompilation().nextFunctionCode());
					gf.setClass(result2)
					result2.constructors[cd] = gf
				}

				functionInvocation.generateDeferred().resolve(gf)
				functionInvocation.generated = gf
				generated.resolve(gf)
			}
		} else {
			functionInvocation.generatePromise().then { p: BaseEvaFunction -> generated.resolve(p) }
		}

		_isDone = true
	}

	companion object {
		private fun __getElement(varTableEntry: VarTableEntry, cd: ConstructorDef): OS_Element {
			val element: OS_Element
			if (varTableEntry.initialValue !== LangGlobals.UNASSIGNED) {
				val left: IExpression = varTableEntry.nameToken
				val right = varTableEntry.initialValue

				val wrapped: IExpression = ExpressionBuilder.build(left, ExpressionKind.ASSIGNMENT, right)

				val vs = varTableEntry.vs() as VariableStatementImpl
				val wrapper = WrappedStatementWrapper(wrapped, cd.context, cd, vs)

				element = wrapper
			} else {
				val element3 = ConstructStatementImpl(cd, cd.context, varTableEntry.nameToken, null, null)
				element = element3
			}
			return element
		}
	}
} //
//
//

