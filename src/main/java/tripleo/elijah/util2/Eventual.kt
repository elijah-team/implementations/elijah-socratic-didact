package tripleo.elijah.util2

import org.jdeferred2.DoneCallback
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.diagnostic.Diagnostic

class Eventual<P> {
    private val prom = DeferredObject<P, Diagnostic, Void>()

    /**
     * Please overload this
     */
    fun description(): String {
        return "GENERIC-DESCRIPTION"
    }

    fun fail(d: Diagnostic) {
        prom.reject(d)
    }

    val isResolved: Boolean
        get() = prom.isResolved

    fun register(ev: EventualRegister) {
        ev.register(this)
    }

    fun resolve(p: P) {
        prom.resolve(p)
    }

    fun then(cb: DoneCallback<in P>?) {
        prom.then(cb)
    }

    fun _prom_isRejected(): Boolean {
        return prom.isRejected
    }

    fun reject(aDiagnostic: Diagnostic) {
        prom.reject(aDiagnostic)
    }

    val isPending: Boolean
        get() = prom.isPending
}
