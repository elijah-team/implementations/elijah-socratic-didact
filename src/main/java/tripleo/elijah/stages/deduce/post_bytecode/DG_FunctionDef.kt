package tripleo.elijah.stages.deduce.post_bytecode

import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.i.OS_Element

class DG_FunctionDef(internal val functionDef: FunctionDef) : DG_Item {
    fun getFunctionDef(): OS_Element {
        return functionDef
    }
}
