package tripleo.elijah.stages.inter

import tripleo.elijah.entrypoints.EntryPoint
import tripleo.elijah.g.GModuleThing
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.stages.gen_fn.EvaFunction
import tripleo.small.ES_Symbol
import java.util.*

class ModuleThing(private val mod: OS_Module) : GModuleThing {
    class GeneralDescription(private val aSymbol: ES_Symbol, private val aObjects: List<Any>) {
        fun aSymbol(): ES_Symbol {
            return aSymbol
        }

        fun aObjects(): List<Any> {
            return aObjects
        }

        override fun equals(obj: Any?): Boolean {
            if (obj === this) return true
            if (obj == null || obj.javaClass != this.javaClass) return false
            val that = obj as GeneralDescription
            return this.aSymbol == that.aSymbol && (this.aObjects == that.aObjects)
        }

        override fun hashCode(): Int {
            return Objects.hash(aSymbol, aObjects)
        }

        override fun toString(): String {
            return "GeneralDescription[" +
                    "aSymbol=" + aSymbol + ", " +
                    "aObjects=" + aObjects + ']'
        }
    }

    private val entryPoints: List<EntryPoint> = mod.entryPoints()
    private val evaFunctions: MutableList<EvaFunction> = ArrayList()

    private var generalDescription: GeneralDescription? = null

    fun addFunction(aGeneratedFunction: EvaFunction) {
        evaFunctions.add(aGeneratedFunction)
    }

    fun describe(aGeneralDescription: GeneralDescription?) {
        generalDescription = aGeneralDescription
    }
}
