package tripleo.elijah.nextgen.outputtree

enum class EOT_OutputType {
    BUFFERS,  // code2/*
    BUILD,  // code/*
    DUMP,  // inputs.txt
    INPUTS,  // buffers.txt
    LOGS,  // logs/*, // ??: meson build files
    SWW, SOURCES
}
