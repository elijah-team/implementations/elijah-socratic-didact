package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.ClassTypes
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.stages.gen_c.GenerateC.GetTypeName
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaContainerNC
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.GenerateResult.TY
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.util.*

internal class C2C_CodeForConstructor(private val generateCodeForMethod: Generate_Code_For_Method,
                                      aFileGen: GenerateResultEnv,
        //	private final GenerateResultEnv             fileGen;
        //private final          EvaConstructor           gf;
                                      private val yf: WhyNotGarish_Constructor) : C2C_Results {
    //gf = aYf.cheat();
//		fileGen = aFileGen;
    val gr: GenerateResult = aFileGen.gr()
    var st: EG_Statement? = null
    private var _calculated = false
    private var buf: C2C_Result? = null
    private var bufHdr: C2C_Result? = null

    override val results: List<C2C_Result>
        get() {
            calculate()
            return Helpers.List_of(buf!!, bufHdr!!)
        }

    private fun calculate() {
        val tos = generateCodeForMethod.tos
        val tosHdr = generateCodeForMethod.tosHdr
        calculate(tos, tosHdr)
    }

    private fun calculate(tos: BufferTabbedOutputStream?, tosHdr: BufferTabbedOutputStream?) {
        if (_calculated == false) {
            // TODO this code is only correct for classes and not meant for namespaces
            assert(yf.declaringContext().isClassStatement)
            val genClass = yf.genClass

            if (genClass is EvaClass) {
                when (genClass.klass.type!!) {
                    ClassTypes.INTERFACE, ClassTypes.SIGNATURE, ClassTypes.ABSTRACT -> return
                    ClassTypes.ANNOTATION,
                    ClassTypes.EXCEPTION,
                    ClassTypes.NORMAL,
                    ClassTypes.STRUCTURE -> {/*passthru*/}
                }
                val decl = CClassDecl(genClass)
                decl.evaluatePrimitive()

                val class_name: String = GetTypeName.Companion.forGenClass(genClass)
                val class_code: Int = genClass.code

                assert(yf.cd() != null // TODO 10/16 can we remove this?
                )
                val constructorName = yf.constructorNameText
                val xx = C2C_CodeForConstructor_Statement(class_name, class_code, constructorName, decl, genClass)
                xx.getTextInto(tos!!) // README created because non-recursive interpreter
                this.st = xx

                val gmh = Generate_Method_Header(yf, generateCodeForMethod._gc(), generateCodeForMethod._gc().LOG)
                generateCodeForMethod.action_invariant(yf, gmh)

                tos.put_string_ln("return R;")
                tos.dec_tabs()

                assert(!decl.prim)
                tos.put_string_ln(String.format("} // class %s%s", if (decl.prim) "box " else "", genClass.name))
                tos.put_string_ln("")

                val header_string = getHeaderString(genClass, class_name, class_code, constructorName)

                tosHdr!!.put_string_ln(String.format("%s;", header_string))

                tos.flush()
                tos.close()
                tosHdr.flush()
                tosHdr.close()

                val buf1 = tos.buffer
                val bufHdr1 = tosHdr.buffer

                buf = Default_C2C_Result(buf1, TY.IMPL, "C2C_CodeForConstructor IMPL", yf)
                bufHdr = Default_C2C_Result(bufHdr1, TY.HEADER, "C2C_CodeForConstructor HEADER", yf)

                _calculated = true
            }
        }
    }

    private fun getHeaderString(x: EvaClass,
                                class_name: String,
                                class_code: Int,
                                constructorName: String): String {
        val header_string: String
        val gmh = Generate_Method_Header(yf, generateCodeForMethod._gc(), generateCodeForMethod.LOG)
        val args_string = gmh.args_string

        // NOTE getGenClass is always a class or namespace, getParent can be a function
        val parent = yf.genClass as EvaContainerNC

        assert(parent === x)
        if (parent is EvaClass) {
            val name = String.format("ZC%d%s", class_code, constructorName)
            //				LOG.info("138 class_name >> " + class_name);
            header_string = String.format("%s* %s(%s)", class_name, name, args_string)
        } else if (parent is EvaNamespace) {
            // TODO see note above
            val name = String.format("ZNC%d", class_code)
            //				EvaNamespace st = (EvaNamespace) parent;
//				LOG.info(String.format("143 (namespace) %s -> %s", st.getName(), class_name));
//				final String if_args = args_string.length() == 0 ? "" : ", ";
            // TODO vsi for namespace instance??
//				tos.put_string_ln(String.format("%s %s%s(%s* vsi%s%s) {", returnType, class_name, name, class_name, if_args, args));
            header_string = String.format("%s %s(%s)", class_name, name, args_string)
        } else {
            throw IllegalStateException("generating a constructor for something not a class.")
            //				final String name = String.format("ZC%d", class_code);
//				header_string = String.format("%s %s(%s)", class_name, name, args_string);
        }
        return header_string
    }
}
