/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.typinf.parser

import tripleo.elijah.typinf.*
import tripleo.elijah.typinf.lexer.Lexer
import tripleo.elijah.typinf.lexer.LexerError
import tripleo.elijah.typinf.lexer.Token
import tripleo.elijah.util.Helpers

/**
 * Parser for micro-ML.
 *
 *
 * The only public method here is parse_decl that parses a "decl" from a
 * string. Usage:
 *
 *
 * p = Parser()
 * decl = p.parse_decl(<some micro-ML code>)
 * # decl is now an ast.Decl node
 *
 *
 * parse_decl() can be called multiple times with the same parser to parse
 * multiple decls (state is wiped out between calls).
 *
 *
 * Created 9/3/21 9:50 PM
</some> */
class Parser {
    private val lexer: Lexer
    private var cur_token: Token?
    private val operators: List<String?>

    init {
        val lex_rules = Helpers.List_of(
                Helpers.List_of("if", "IF"),
                Helpers.List_of("then", "THEN"),
                Helpers.List_of("else", "ELSE"),
                Helpers.List_of("true", "TRUE"),
                Helpers.List_of("false", "FALSE"),
                Helpers.List_of("lambda", "LAMBDA"),
                Helpers.List_of("\\d+", "INT"),
                Helpers.List_of("->", "ARROW"),
                Helpers.List_of("!=", "!="),
                Helpers.List_of("==", "=="),
                Helpers.List_of(">=", ">="),
                Helpers.List_of("<=", "<="),
                Helpers.List_of("<", "<"),
                Helpers.List_of(">", ">"),
                Helpers.List_of("\\+", "+"),
                Helpers.List_of("\\-", "-"),
                Helpers.List_of("\\*", "*"),
                Helpers.List_of("\\(", "("),
                Helpers.List_of("\\)", ")"),
                Helpers.List_of("=", "="),
                Helpers.List_of(",", ","),
                Helpers.List_of("[a-zA-Z_]\\w*", "ID")
        )
        this.lexer = Lexer(lex_rules, true)
        cur_token = null
        operators =  /*Set*/Helpers.List_of("!=", "==", ">=", "<=", "<", ">", "+", "-", "*")
    }

    //		static <E> Set<E> Set_of(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9) {
    //			return new ImmutableCollections.SetN<>(e1, e2, e3, e4, e5,
    //					e6, e7, e8, e9);
    //		}
    /**
     * Parse declaration given in text and return an AST node for it.
     */
    @Throws(ParseError::class)
    fun parse_decl(text: String?): Decl_AST {
        lexer.input(text)
        this._get_next_token()
        val decl = this._decl()
        if (cur_token!!.type != null) this._error(String.format("Unexpected token \"%s\" (at #%d)",
                cur_token!!.`val`, cur_token!!.pos))
        return decl
    }

    @Throws(ParseError::class)
    private fun _error(msg: String) {
        throw ParseError(msg)
    }

    /**
     * Advances the parser's internal lexer to the next token.
     *
     *
     * This method doesn't return anything; it assigns self.cur_token to the
     * next token in the input stream.
     */
    @Throws(ParseError::class)
    private fun _get_next_token() {
        try {
            cur_token = lexer.token()

            if (cur_token == null) cur_token = Token(null, null, 0)
        } catch (e: LexerError) {
            _error(String.format("Lexer error at position %d: %s", e.pos, e))
        }
    }

    /**
     * The 'match' primitive of RD parsers.
     *
     *
     * * Verifies that the current token is of the given type
     * * Returns the value of the current token
     * * Reads in the next token
     *
     * @return
     */
    @Throws(ParseError::class)
    fun _match(type: String): String? {
        if (cur_token!!.type == type) {
            val `val` = cur_token!!.`val`
            this._get_next_token()
            return `val`
        } else {
            this._error(String.format("Unmatched %s (found %s)", type, cur_token!!.type))
        }
        // never reached
        return null
    }

    @Throws(ParseError::class)
    fun _decl(): Decl_AST {
        val name = this._match("ID")
        val argnames = Helpers.List_of<String?>()

        // If we have arguments, collect them. Only IDs allowed here.;
        while (cur_token!!.type == "ID") {
            argnames.add(cur_token!!.`val`)
            this._get_next_token()
        }

        this._match("=")
        val expr = this._expr()
        return if (len(argnames) > 0) {
            Decl_AST(name!!, LambdaExpr_AST(argnames, expr))
        } else {
            Decl_AST(name!!, expr)
        }
    }

    private fun len(aList: List<String?>): Int {
        return aList.size
    }

    /**
     * Parse an expr of the form{
     *
     *
     * expr op expr;
     *
     *
     * We only allow a single operator between expressions. Additional;
     * operators should be nested using parens, e.g. x + (y * z);
     *
     * @return
     */
    @Throws(ParseError::class)
    fun _expr(): AstNode {
        val node = this._expr_component()
        if (operators.contains(cur_token!!.type)) {
            val op = cur_token!!.type
            this._get_next_token()
            val rhs = this._expr_component()
            return OpExpr_AST(op!!, node, rhs)
        } else {
            return node
        }
    }

    /**
     * Parse an expr component (components can be separated by an operator).
     */
    @Throws(ParseError::class)
    fun _expr_component(): AstNode {
        val curtok = this.cur_token
        if (cur_token!!.type == "INT") {
            this._get_next_token()
            return IntConstant_AST(curtok!!.`val`!!)
        } else if (Helpers.List_of<String?>("FALSE", "TRUE").contains(cur_token!!.type)) {
            this._get_next_token()
            return BoolConstant_AST(curtok!!.`val`!!)
        } else if (cur_token!!.type == "ID") {
            this._get_next_token()
            return if (cur_token!!.type != null && cur_token!!.type == "(") {
                // ID followed by '(' is function application;
                _app(curtok!!.`val`)
            } else {
                Identifier_AST(curtok!!.`val`!!)
            }
        } else if (cur_token!!.type === "(") {
            _get_next_token()
            val expr = _expr()
            _match(")")
            return expr
        } else if (cur_token!!.type === "IF") {
            return this._ifexpr()
        } else if (cur_token!!.type === "LAMBDA") {
            return this._lambda()
        } else {
            this._error(String.format("Don't support %s yet", curtok!!.type))
        }
        throw IllegalStateException()
    }

    @Throws(ParseError::class)
    fun _ifexpr(): IfExpr_AST {
        this._match("IF")
        val ifexpr = this._expr()
        this._match("THEN")
        val thenexpr = this._expr()
        this._match("ELSE")
        val elseexpr = this._expr()
        return IfExpr_AST(ifexpr, thenexpr, elseexpr)
    }

    @Throws(ParseError::class)
    fun _lambda(): LambdaExpr_AST {
        this._match("LAMBDA")
        val argnames = Helpers.List_of<String?>()

        while (cur_token!!.type == "ID") {
            argnames.add(cur_token!!.`val`)
            this._get_next_token()
        }
        if (len(argnames) < 1) {
            this._error("Expected non-empty argument list for lambda")
        }
        this._match("ARROW")
        val expr = this._expr()
        return LambdaExpr_AST(argnames, expr)
    }

    @Throws(ParseError::class)
    fun _app(name: String?): AppExpr_AST {
        this._match("(")
        val args = Helpers.List_of<AstNode>()
        while (cur_token!!.type != ")") {
            args.add(this._expr())
            if (cur_token!!.type == ",") {
                this._get_next_token()
            } else if (cur_token!!.type == ")") {
                //pass // the loop will break;
            } else {
                this._error(String.format("Unexpected %s in application", cur_token!!.`val`))
            }
        }
        this._match(")")
        return AppExpr_AST(Identifier_AST(name!!), args)
    }
}
