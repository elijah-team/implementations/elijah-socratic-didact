package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.ConstructorDef
import tripleo.elijah.lang.i.DecideElObjectType
import tripleo.elijah.lang.i.ElObjectType
import tripleo.elijah.lang.i.ElObjectType.*
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.instructions.IdentIA

class IdentIA_Ops(private val identIA: IdentIA) {
    val constructorPath: ConstructorPathOp
        get() {
            val idte = identIA.entry
            val resolved_element = idte.resolvedElement

            if (idte.resolvedType() != null) {
                val _resolved = idte.resolvedType()
                var ctorName: String? = null

                if (resolved_element != null) { // FIXME stop accepting null here
                    when (DecideElObjectType.getElObjectType(resolved_element)) {
                        CONSTRUCTOR -> {
                            ctorName = (resolved_element as ConstructorDef).name()
                        }

                        CLASS -> {
                            val y = 2
                            ctorName = "" // ((ClassStatement) resolved_element).name();
                        }

                        ALIAS -> TODO()
                        FORMAL_ARG_LIST_ITEM -> TODO()
                        FUNCTION -> TODO()
                        MODULE -> TODO()
                        NAMESPACE -> TODO()
                        TYPE_NAME_ELEMENT -> TODO()
                        UNKNOWN -> TODO()
                        VAR -> TODO()
                        VAR_SEQ -> TODO()
                    }
                } else ctorName = ""

                val finalCtorName = ctorName
                return object : ConstructorPathOp {
                    override fun getCtorName(): String? {
                        return finalCtorName
                    }

                    override fun getResolved(): EvaNode? {
                        return _resolved
                    }
                }
            } /*
			 * else if (resolved_element != null) { assert false; if (resolved_element
			 * instanceof VariableStatementImpl) { addRef(((VariableStatementImpl)
			 * resolved_element).getName(), CReference.Ref.MEMBER); } else if
			 * (resolved_element instanceof ConstructorDef) { assert i == sSize - 1; // Make
			 * sure we are ending with a constructor call int code = ((ClassStatement)
			 * resolved_element.getParent())._a.getCode(); if (code == 0) {
			 * tripleo.elijah.util.Stupidity.
			 * println_err_2("** 31161 ClassStatement with 0 code " +
			 * resolved_element.getParent()); } // README Assuming this is for named
			 * constructors String text = ((ConstructorDef) resolved_element).name(); String
			 * text2 = String.format("ZC%d%s", code, text);
			 * 
			 * ctorName = text;
			 * 
			 * // addRef(text2, CReference.Ref.CONSTRUCTOR);
			 * 
			 * // addRef(((ConstructorDef) resolved_element).name(),
			 * CReference.Ref.CONSTRUCTOR); } }
			 */

            return object : ConstructorPathOp {
                override fun getCtorName(): String? {
                    return null
                }

                override fun getResolved(): EvaNode? {
                    return null
                }
            }
        }

    companion object {
        fun get(aIdentIA: IdentIA): IdentIA_Ops {
            return IdentIA_Ops(aIdentIA)
        }
    }
}
