package tripleo.elijah.nextgen.output

import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.Compilation0
import tripleo.elijah.comp.nextgen.inputtree.EIT_ModuleInput
import tripleo.elijah.nextgen.inputtree.EIT_InputType
import tripleo.elijah.nextgen.inputtree.EIT_ModuleInputImpl
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.stages.gen_c.C2C_Result
import tripleo.elijah.stages.gen_generic.GenerateResult.TY

class NG_OutputFunctionStatement
(
    __c2c: C2C_Result
) : NG_OutputStatement {

    override val ty: TY = __c2c.ty()

    private val x: EG_Statement = __c2c.statement
    private val moduleDependency = NG_OutDep(__c2c.definedModule)

    override val explanation: EX_Explanation
        get() = EX_Explanation.withMessage("NG_OutputFunctionStatement")

    override val moduleInput: EIT_ModuleInput
        get() {
            val m = moduleDependency/*()*/.module()

            val compilation0: Compilation0 = m/*!!*/.compilation
            val compilation: Compilation = compilation0 as Compilation

            val moduleInput: EIT_ModuleInput = EIT_ModuleInputImpl(m, compilation, EIT_InputType.ELIJAH_SOURCE)
            return moduleInput
        }

    override val text: String
        get() = x.text

//    fun moduleDependency(): NG_OutDep = moduleDependency
}
