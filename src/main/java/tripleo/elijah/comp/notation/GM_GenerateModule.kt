package tripleo.elijah.comp.notation

import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.gen_generic.Sub_GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.stages.gen_generic.pipeline_impl.ProcessedNode1
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.work.WorkList__
import tripleo.elijah.work.WorkManager
import java.util.function.Supplier

class GM_GenerateModule(private val gmr: GM_GenerateModuleRequest) {
    fun getModuleResult(
            wm: WorkManager,
            aResultSink: GenerateResultSink
    ): GM_GenerateModuleResult {
        val mod = gmr.params().mod
        val generateNodesIntoSink = gmr.generateNodesIntoSink()

        val gr1: GenerateResult = Sub_GenerateResult()
        val fgs =  //this::createGenerateResultEnv;
                Supplier { GenerateResultEnv(aResultSink, gr1, wm, WorkList__(),  /* tautology */this) }

        val ggc = gmr.getGenerateFiles { fgs.get() }
        val lgc = generateNodesIntoSink._env().lgc

        val fileGen = (ggc as GenerateC).fileGen__

        for (processedNode in lgc) {
            val evaNode = (processedNode as ProcessedNode1).evaNode

            if (!(processedNode.matchModule(mod))) continue  // README curious


            if (processedNode.isContainerNode) {
                processedNode.processContainer(ggc, fileGen)

                processedNode.processConstructors(ggc, fileGen)
                processedNode.processFunctions(ggc, fileGen)
                processedNode.processClassMap(ggc, fileGen)
            } else {
                logProgress(2009, evaNode.javaClass.name)
            }
        }

        return GM_GenerateModuleResult(gr1, generateNodesIntoSink, gmr) { fgs.get() }
    }

    fun gmr(): GM_GenerateModuleRequest {
        return gmr
    }

    // README 12/30 too slick	
    //	public void createGenerateResultEnv() {
    //		return new GenerateResultEnv(aResultSink, gr1, wm, new WorkList__() /* tautology */, this);
    //	}
    fun logProgress(code: Int, message: String) {
        SimplePrintLoggerToRemoveSoon.println_out_2("$code $message")
    }
}
