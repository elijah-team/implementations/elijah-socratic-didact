package tripleo.elijah.comp.notation

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.functionality.f202.F202
import tripleo.elijah.comp.i.ICompilationAccess
import tripleo.elijah.stages.logging.ElLog

class GN_WriteLogs // silent = aCa.testSilence() == ElLog.Verbosity.SILENT;
@Contract(pure = true) constructor(private val ca: ICompilationAccess, // private final boolean silent;
                                   private val logs: List<ElLog>) : GN_Notable {
    override fun run() {
        if (false) {
            val logMap: Multimap<String, ElLog> = ArrayListMultimap.create()

            for (deduceLog in logs) {
                logMap.put(deduceLog.fileName, deduceLog)
            }

            val f202 = F202(ca.compilation.errSink, ca.compilation as Compilation)

            for ((_, value) in logMap.asMap()) {
                f202.processLogs(value)
            }
        }
    }
}
