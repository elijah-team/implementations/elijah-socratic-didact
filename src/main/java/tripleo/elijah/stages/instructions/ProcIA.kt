/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.instructions

import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.ProcTableEntry

/**
 * Created 1/12/21 4:22 AM
 */
class ProcIA(private val index: Int, private val generatedFunction: BaseEvaFunction) : InstructionArgument {
    override fun toString(): String {
        return "ProcIA{" + "index=" + index + ", " + "func=" + entry + '}'
    }

    val entry: ProcTableEntry
        get() = generatedFunction.getProcTableEntry(index)

    fun index(): Int {
        return index
    }

    fun generatedFunction(): BaseEvaFunction {
        return generatedFunction
    }
} //
//
//

