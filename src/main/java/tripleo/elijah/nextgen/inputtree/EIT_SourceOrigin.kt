package tripleo.elijah.nextgen.inputtree

enum class EIT_SourceOrigin {
    NULL, LIB, PREL, SRC
}
