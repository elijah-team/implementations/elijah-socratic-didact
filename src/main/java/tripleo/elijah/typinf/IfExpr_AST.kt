package tripleo.elijah.typinf

import tripleo.elijah.util.Helpers

/**
 * if ... then ... else ... expression.
 */
class IfExpr_AST(val ifexpr: AstNode, val thenexpr: AstNode, val elseexpr: AstNode) : AstNode() {
    init {
        this._children = Helpers.List_of(this.ifexpr, this.thenexpr, this.elseexpr)
    }

    override fun toString(): String {
        return String.format("If(%s, %s, %s)", this.ifexpr, this.thenexpr, this.elseexpr)
    }
}
