package tripleo.elijah.stages.garish

import tripleo.elijah.nextgen.output.NG_OutputFunction
import tripleo.elijah.stages.gen_c.C2C_Result
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaConstructor
import tripleo.elijah.stages.gen_generic.GRS_Addable
import tripleo.elijah.stages.gen_generic.pipeline_impl.DefaultGenerateResultSink
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink

class GarishConstructor__addFunction(private val gf: EvaConstructor, private val rs: List<C2C_Result>, private val generateC: GenerateC) : GRS_Addable {
    override fun action(aResultSink: GenerateResultSink) {
        val o = NG_OutputFunction()
        o.setFunction(gf, generateC, rs)
        (aResultSink as DefaultGenerateResultSink).pa().addOutput(o)
    }
}
