package tripleo.elijah.stages.write_stage.pipeline_impl

import tripleo.elijah.comp.IO._IO_ReadFile
import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.comp.nextgen.i.CP_Path
import tripleo.elijah.nextgen.inputtree.EIT_Input_HashSourceFile_Triple
import tripleo.elijah.nextgen.inputtree.EIT_Input_HashSourceFile_Triple.Companion.decode
import tripleo.elijah.nextgen.outputstatement.*
import tripleo.elijah.nextgen.outputstatement.EG_Statement.Companion.of
import tripleo.elijah.nextgen.outputtree.EOT_OutputFileImpl
import tripleo.elijah.nextgen.outputtree.EOT_OutputFileImpl.DefaultFileNameProvider
import tripleo.elijah.nextgen.outputtree.EOT_OutputType
import tripleo.elijah.util.Helpers
import tripleo.elijah.util.Mode
import tripleo.elijah.util.Ok
import tripleo.elijah.util.Operation
import tripleo.util.buffer.DefaultBuffer
import tripleo.util.buffer.TextBuffer
import java.nio.file.Path

class WPIS_WriteInputs : WP_Individual_Step {
    private val ops: MutableMap<String, Operation<String?>> = HashMap()

    override fun act(st: WritePipelineSharedState, sc: WP_State_Control) {
        val root = st.c.paths().outputRoot()
        val fn1: CP_Path = root.child("inputs.txt")

        val buf = DefaultBuffer("")

        val recordedreads = st.c.io.recordedreads_io()

        //for (final IO._IO_ReadFile readFile : recordedreads) {
        //	final String fn = readFile.getFileName();
        //
        //	final Operation<String> op = append_hash(buf, readFile);
        //
        //	ops.put(fn, op);
        //
        //	if (op.mode() == Mode.FAILURE) {
        //		break;
        //	}
        //}
        val s = buf.text

        val ot = st.c.outputTree

        val yys: MutableList<EIT_Input_HashSourceFile_Triple> = ArrayList()

        run {
            for (file in recordedreads) {
                val decoded = decode(file!!)
                yys.add(decoded)

                ops[decoded.filename()] = Operation.success(decoded.hash()) // FIXME extract actual operation
            }
        }

        val seq = mk_EG_SequenceStatement(
                EG_Naming("<<WPIS_WriteInputs>>"),
                Helpers.List_of(of(s, EX_Explanation_withMessage("<<WPIS_WriteInputs>> >> statement" ))))

        fn1.pathPromise.then { pp: Path? ->
            val string = "inputs.txt" // pp.toFile().toString(); //fn1.getPath().toFile().toString();
            val off = EOT_OutputFileImpl(Helpers.List_of(),
                    DefaultFileNameProvider(string),
                    EOT_OutputType.INPUTS,
                    seq)

            off.x = yys
            ot.add(off)
        }
    }

    fun append_hash(outputBuffer: TextBuffer, aFilename: String): Operation<String> {
        val hh2 = Helpers.getHashForFilename(aFilename)

        if (hh2.mode() == Mode.SUCCESS) {
            val hh = hh2.success()!!

            // TODO EG_Statement here
            outputBuffer.append(hh)
            outputBuffer.append(" ")
            outputBuffer.append_ln(aFilename)
        }

        return hh2
    }

    fun append_hash(outputBuffer: TextBuffer,
                    aReadFile: _IO_ReadFile): Operation<String> {
        val hh2 = aReadFile.hash()

        if (hh2.mode() == Mode.SUCCESS) {
            val hh = hh2.success()!!

            // TODO EG_Statement here
            outputBuffer.append(hh)
            outputBuffer.append(" ")
            outputBuffer.append_ln(aReadFile.fileName)
        }

        return hh2
    }

    //@Override
    fun execute(aMonitor: CK_Monitor?): Operation<Ok>? {
        return null
    }
}
