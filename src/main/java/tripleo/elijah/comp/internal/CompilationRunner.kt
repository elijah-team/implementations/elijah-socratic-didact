package tripleo.elijah.comp.internal

import lombok.Getter
import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.caches.DefaultEzCache
import tripleo.elijah.comp.i.*
import tripleo.elijah.comp.i.extra.ICompilationRunner
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.impl.DefaultCompilationEnclosure
import tripleo.elijah.comp.internal.CompilationImpl.CompilationAlways
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.process.CB_StartCompilationRunnerAction
import tripleo.elijah.comp.specs.EzCache
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.stateful._RegistrationTargetMixin
import tripleo.elijah.util.*
import tripleo.elijah.util2.UnintendedUseException
import java.util.function.Supplier

class CompilationRunner(aca: ICompilationAccess,
                        aCrState: CR_State,
                        scb: Supplier<ICompilationBus>?) : _RegistrationTargetMixin(), ICompilationRunner {
    val ezCache: EzCache
    private val _compilation = aca.compilation as Compilation
    private val cb: ICompilationBus

    @Getter
    private val crState: CR_State

    @Getter
    private val progressSink: IProgressSink
    private /*@NotNull*/ var startAction: CB_StartCompilationRunnerAction? = null

    constructor(aca: ICompilationAccess, aCrState: CR_State) : this(
            aca,
            aCrState,
            Supplier<ICompilationBus> { (aca.compilation.compilationEnclosure as DefaultCompilationEnclosure).compilationBus }
    )

    init {
        val compilationEnclosure = _compilation.compilationEnclosure
        compilationEnclosure.provideCompilationAccess(aca)
        compilationEnclosure.provideCompilationBus(scb)

        cb = compilationEnclosure.compilationBus
        progressSink = cb.defaultProgressSink()
        crState = aCrState
        ezCache = DefaultEzCache(aca.compilation as Compilation)
    }

    fun _accessCompilation(): Compilation {
        return _compilation
    }

    fun _cis(): CIS {
        return _compilation._cis()
    }

    fun c(): Compilation {
        return _compilation
    }

    override fun ezCache(): EzCache {
        return ezCache
    }

    override fun pushNextCompilerInstructions(aCi: CompilerInstructions) {
        _cis().onNext(aCi)
    }

    override fun getCrState__pr(): ProcessRecord {
        return crState.pr
    }

    val compilationEnclosure: CompilationEnclosure
        get() = _accessCompilation().compilationEnclosure

    fun logProgress(number: Int, text: String?) {
        if (number == 130) return

        SimplePrintLoggerToRemoveSoon.println_err_3("%d %s".formatted(number, text))
    }

    override fun start(aRootCI: CompilerInstructions, pa: GPipelineAccess) {
        // FIXME only run once 06/16
        if (startAction == null) {
            startAction = CB_StartCompilationRunnerAction(this, (pa as IPipelineAccess), aRootCI)
            // FIXME CompilerDriven vs Process ('steps' matches "CK", so...)
            cb.add(startAction!!.cb_Process())

            // FIXME calling automatically for some reason?
            val monitor = cb.monitor
            val compilationDriver = pa.compilationEnclosure.compilationDriver
            val ocrsd = compilationDriver[CompilationAlways.Tokens.Companion.COMPILATION_RUNNER_START]

            val cbOutput = startAction!!.o

            when (ocrsd.mode()) {
                Mode.SUCCESS -> {
                    val compilationRunnerStart = ocrsd.success() as CD_CompilationRunnerStart
                    val crState1 = this.getCrState()

                    // assert !(started);
                    if (CB_StartCompilationRunnerAction.started) {
                        //throw new AssertionError();
                        SimplePrintLoggerToRemoveSoon.println_err_4("twice for $startAction")
                    } else {
                        CB_StartCompilationRunnerAction.enjoin(compilationRunnerStart, aRootCI, crState1, cbOutput)
                    }

                    monitor.reportSuccess(startAction, cbOutput)
                }

                Mode.FAILURE, Mode.NOTHING -> {
                    monitor.reportFailure(startAction, cbOutput)
                    throw IllegalStateException("Error")
                }
            }
        } else {
            assert(false)
        }
    }

    override fun getCrState(): CR_State? {
        return ::crState.get()
    }

    class __CompRunner_Monitor : CB_Monitor {
        override fun reportFailure(action: CB_Action, output: CB_Output) {
            SimplePrintLoggerToRemoveSoon.println_err_4("" + output.get())
        }

        override fun reportSuccess(action: CB_Action, output: CB_Output) {
            NotImplementedException.raise_stop()
            for (outputString in output.get()) {
                SimplePrintLoggerToRemoveSoon.println_out_3("** CompRunnerMonitor ::  " + action.name() + " :: outputString :: " + outputString.text)
            }
        }
    }
}
