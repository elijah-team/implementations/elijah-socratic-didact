package tripleo.elijah.nextgen.output

import tripleo.elijah.comp.nextgen.inputtree.EIT_ModuleInput
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.stages.gen_generic.GenerateResult.TY

interface NG_OutputStatement : EG_Statement {
//    @JvmField
    val moduleInput: EIT_ModuleInput

    // promise filename
//    @JvmField
    val ty: TY

    // promise EOT_OutputFile
}
