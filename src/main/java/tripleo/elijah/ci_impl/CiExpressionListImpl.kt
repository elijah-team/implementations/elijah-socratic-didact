package tripleo.elijah.ci_impl

import com.google.common.base.Preconditions
import tripleo.elijah.ci.CiExpressionList
import tripleo.elijah.lang.i.IExpression

class CiExpressionListImpl : CiExpressionList {
    private val exprs: MutableList<IExpression> = ArrayList()

    override fun add(aExpr: IExpression) {
        exprs.add(aExpr)
    }

    override fun expressions(): Collection<IExpression> {
        return exprs
    }

    override fun iterator(): Iterator<IExpression> {
        return exprs.iterator()
    }

    override fun next(aExpr: IExpression): IExpression {
        Preconditions.checkNotNull(aExpr)

        add(aExpr)
        return aExpr
    }

    override fun toString(): String {
        return exprs.toString()
    }

    fun size(): Int {
        return exprs.size
    }
}
