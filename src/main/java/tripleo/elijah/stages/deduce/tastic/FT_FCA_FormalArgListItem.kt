package tripleo.elijah.stages.deduce.tastic

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.lang.i.FormalArgListItem
import tripleo.elijah.lang.i.OS_Type
import tripleo.elijah.lang.types.OS_UserType
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.TypeTableEntry
import tripleo.elijah.stages.gen_fn.VariableTableEntry

class FT_FCA_FormalArgListItem(private val fali: FormalArgListItem, private val generatedFunction: BaseEvaFunction) {
    fun _FunctionCall_Args_doLogic0(vte: VariableTableEntry,
                                    vte1: VariableTableEntry, errSink: ErrSink) {
        val osType: OS_Type = OS_UserType(fali.typeName())
        if (osType != vte.typeTableEntry.attached) {
            val tte1 = generatedFunction.newTypeTableEntry(TypeTableEntry.Type.SPECIFIED, osType,
                    fali.nameToken, vte1)
            /*
			 * if (p.isResolved()) System.out.
			 * printf("890 Already resolved type: vte1.type = %s, gf = %s, tte1 = %s %n",
			 * vte1.type, generatedFunction, tte1); else
			 */
            run {
                val attached = tte1.attached
                when (attached!!.type) {
                    OS_Type.Type.USER -> vte1.typeTableEntry.attached = attached // !!
                    OS_Type.Type.USER_CLASS -> {
                        val gt = vte1.genType
                        gt.resolved = attached
                        vte1.resolveType(gt)
                    }

                    else -> errSink.reportWarning("2853 Unexpected value: " + attached.type)
                }
            }
        }

        // vte.type = tte1;
        // tte.attached = tte1.attached;
        // vte.setStatus(BaseTableEntry.Status.KNOWN, best);
    }
}
