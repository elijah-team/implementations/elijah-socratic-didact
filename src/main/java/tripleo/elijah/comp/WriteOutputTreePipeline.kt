package tripleo.elijah.comp

import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.comp.nextgen.i.CP_Path
import tripleo.elijah.comp.nextgen.i.CP_RootType
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.g.GPipelineMember
import tripleo.elijah.nextgen.ER_Node
import tripleo.elijah.nextgen.inputtree.EIT_Input
import tripleo.elijah.nextgen.outputstatement.*
import tripleo.elijah.nextgen.outputtree.EOT_OutputFile
import tripleo.elijah.nextgen.outputtree.EOT_OutputFileImpl
import tripleo.elijah.nextgen.outputtree.EOT_OutputType
import tripleo.elijah.stages.write_stage.pipeline_impl.EOT_FileNameProvider__ofString
import tripleo.elijah.util.*

class WriteOutputTreePipeline(pa0: GPipelineAccess) : PipelineMember(), GPipelineMember {
    private val pa = pa0 as IPipelineAccess

    @Throws(Exception::class)
    override fun run(st: CR_State, aOutput: CB_Output?) {
        val compilation = st.ca().compilation as Compilation
        val ot = compilation.outputTree
        val l = ot.list

        //
        //
        //
        //
        //
        //
        //
        // HACK should be done earlier in process
        //
        //
        //
        //
        //
        //
        //

        val paths = compilation.paths()
        val REPORTS = compilation.reports()!!

        st.compilationEnclosure.pipelineAccessPromise.then { apa: IPipelineAccess? ->
            addLogs(l, apa!!)

            paths.signalCalculateFinishParse() // TODO maybe move this 06/22


            val r = paths.outputRoot()

            for (outputFile in l) {
                val path0 = outputFile.filename
                val seq = outputFile.statementSequence

                var pp: CP_Path?

                when (outputFile.type) {
                    EOT_OutputType.SOURCES -> {
                        pp = r.child("code2").child(path0)

                        REPORTS.addCodeOutput(outputFile) { path0!! }
                    }

                    EOT_OutputType.LOGS -> pp = r.child("logs").child(path0)
                    EOT_OutputType.INPUTS, EOT_OutputType.BUFFERS -> pp = r.child(path0)
                    EOT_OutputType.DUMP -> pp = r.child("dump").child(path0)
                    EOT_OutputType.BUILD -> pp = r.child(path0)
                    EOT_OutputType.SWW -> pp = r.child("sww").child(path0)
                    else -> throw IllegalStateException("Unexpected value: " + outputFile.type)
                }
                // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("106 " + pp);
                paths.addNode(CP_RootType.OUTPUT, ER_Node.of(pp, seq))
            }

            paths.renderNodes()
        }
    }

    override fun finishPipeline_asString(): String {
        return this.javaClass.toString()
    }

    companion object {
        private fun addLogs(l: MutableList<EOT_OutputFile>, aPa: IPipelineAccess) {
            val logs = aPa.compilationEnclosure.logs
            val s1 = logs[0].fileName

            for (log in logs) {
                val stmts: MutableList<EG_Statement> = ArrayList()

                if (log.entries.size == 0) continue  // README Prelude.elijjah "fails" here


                for (entry in log.entries) {
                    val logentry = String.format("[%s] [%tD %tT] %s %s",
                            s1,
                            entry.time(),
                            entry.time(),
                            entry.level(),
                            entry.message())
                    stmts.add(EG_SingleStatement(logentry + "\n", EX_Explanation_withMessage("WriteOutputTreePipeline::addLog")))
                }

                val seq = mk_EG_SequenceStatement(EG_Naming("wot.log.seq"), stmts)
                val fileName = log.fileName.replace("/", "~~") // eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
                val off: EOT_OutputFile = EOT_OutputFileImpl(Helpers.List_of(), fileName, EOT_OutputType.LOGS, seq)
                l.add(off)
            }
        }
    }
}

internal fun Finally.addCodeOutput(outputFile: EOT_OutputFile, function: () -> String) {
    return addCodeOutput(outputFile, EOT_FileNameProvider2 { function.invoke() }) // prol did this wrong
}

internal fun EOT_OutputFileImpl(inputs: List<EIT_Input?>, filename__: String, type: EOT_OutputType, statementSequence: EG_SequenceStatement): EOT_OutputFileImpl {
    val x = EOT_FileNameProvider__ofString(filename__)
    return EOT_OutputFileImpl(inputs, x, type, statementSequence)
}
