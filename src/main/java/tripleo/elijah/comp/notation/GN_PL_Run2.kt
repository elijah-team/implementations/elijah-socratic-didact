package tripleo.elijah.comp.notation

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.stages.deduce.DeducePhase.GeneratedClasses
import tripleo.elijah.stages.gen_fn.DefaultClassGenerator
import tripleo.elijah.stages.gen_fn.IClassGenerator
import tripleo.elijah.stages.inter.ModuleThing
import tripleo.elijah.util2.EventualRegister
import tripleo.elijah.util3._AbstractEventualRegister
import tripleo.elijah.world.i.WorldModule
import tripleo.elijah.world.impl.DefaultWorldModule
import java.util.function.Consumer

class GN_PL_Run2
(
		val pipelineLogic: PipelineLogic,
		val mod: WorldModule,
		val ce: CompilationEnclosure,
		val worldConsumer: Consumer<WorldModule>
) : _AbstractEventualRegister(), GN_Notable, EventualRegister {

	@JvmRecord
	data class GenerateFunctionsRequest(@JvmField val classGenerator: IClassGenerator,
	                                    @JvmField val worldModule: DefaultWorldModule) {
		val mt: ModuleThing
			get() = (worldModule.thing())!!

		val mod: OS_Module
			get() = worldModule.module()


//		override fun toString(): String {
//			return "GenerateFunctionsRequest[" +
//					"classGenerator=" + classGenerator + ", " +
//					"worldModule=" + worldModule + ']'
//		}
	}

	private val dcg = DefaultClassGenerator(pipelineLogic.dp)

	private fun _finish() {
		pipelineLogic.checkFinishEventuals()
	}

	override fun run() {
		val worldModule = mod as DefaultWorldModule
		val rq = GenerateFunctionsRequest(dcg, worldModule)

		worldModule.setRq(rq)

		val plgc = pipelineLogic.handle(rq)
		plgc.register(pipelineLogic)

		plgc.then { lgc: GeneratedClasses ->
			val cr = dcg.codeRegistrar
			val resolved_nodes2 = ResolvedNodes(cr)

			resolved_nodes2.init(lgc)
			resolved_nodes2.part2()
			resolved_nodes2.part3(pipelineLogic, mod, lgc)

			worldConsumer.accept(worldModule)
		}

		_finish()
	}
}

@Contract("_ -> new")
@Suppress("unused")
fun getFactoryEnv(env1: GN_Env): GN_PL_Run2 {
	val env = env1 as GN_PL_Run2_Env
	return GN_PL_Run2(env.pipelineLogic, env.mod, env.ce, env.worldConsumer)
}