package tripleo.elijah.comp.nextgen.wonka

import com.google.common.base.Preconditions
import com.google.common.base.Supplier
import tripleo.elijah.comp.IO
import tripleo.elijah.comp.nextgen.CK_GlobalRef
import tripleo.elijah.comp.specs.ElijahCache
import tripleo.elijah.comp.specs.ElijahSpec
import tripleo.elijah.comp.specs.ElijahSpec_
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.util.Operation2
import tripleo.wrap.File
import java.io.FileNotFoundException

class CK_SourceFile__SpecifiedElijahFile(override val file: File) : __CK_SourceFile__AbstractElijahFile() {
    override fun process_query(): Operation2<Any?> {
        val elijahCache = compilation!!.asCompilation().use_elijahCache()
        val oci: Operation2<OS_Module> = process_query(compilation!!.io, elijahCache)

        //super.asserverate();
        return oci as Operation2<Any?>
    }

    private fun process_query(io: IO, aElijahCache: ElijahCache): Operation2<OS_Module> {
        val fileName = file_name()
        Preconditions.checkArgument(__CK_SourceFile__AbstractElijahFile.Companion.isElijjahFile(fileName))

        val stream = Supplier {
            try {
                return@Supplier io.readFile(file)
            } catch (aE: FileNotFoundException) {
                throw RuntimeException(aE)
            }
        }.get()

        val elijahSpec: ElijahSpec = ElijahSpec_(fileName, file, stream)
        return realParseElijahFile(elijahSpec, aElijahCache, compilation!!)
    }

    private fun file_name(): String {
        return file.name
    }

    override val fileName: String
        get() = file_name()

    override fun compilation00(): CK_GlobalRef = compilation!!
}
