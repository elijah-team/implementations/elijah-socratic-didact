package tripleo.elijah.stages.deduce

import com.google.common.base.Preconditions
import org.jdeferred2.DoneCallback
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.deduce.DeducePath.MemberContext
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.deduce.tastic.FCA_Stop
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.util.Mode
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.Operation2
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import java.util.*

class Implement_construct(private val deduceTypes2: DeduceTypes2,
                          private val generatedFunction: BaseEvaFunction,
                          instruction: Instruction) {
    inner class ICH(private val genType: GenType?) {
        fun getClassInvocation(constructorName: String?, aTyn1: NormalTypeName,
                               aGenType: GenType?, aBest: ClassStatement): ClassInvocation {
            val clsinv: ClassInvocation
            if (aGenType != null && aGenType.ci != null) {
                assert(aGenType.ci is ClassInvocation)
                clsinv = aGenType.ci as ClassInvocation
            } else {
                val oi = DeduceTypes2.ClassInvocationMake.withGenericPart(aBest,
                        constructorName, aTyn1, deduceTypes2)
                assert(oi.mode() == Mode.SUCCESS)
                val clsinv2 = oi.success()
                clsinv = deduceTypes2.phase.registerClassInvocation(clsinv2)
            }
            return clsinv
        }

        fun lookupTypeName(normalTypeName: NormalTypeName, typeName: String): ClassStatement {
            val best: OS_Element?
            if (genType != null && genType.resolved != null) {
                best = genType.resolved.classOf
            } else {
                val lrl = normalTypeName.context.lookup(typeName)
                best = lrl.chooseBest(null)
            }
            assert(best is ClassStatement)
            return best as ClassStatement
        }
    }

    private val expression: InstructionArgument

    private val pte: ProcTableEntry

    init {
        assert(instruction.name == InstructionName.CONSTRUCT)
        assert(instruction.getArg(0) is ProcIA)
        val pte_num = (instruction.getArg(0) as ProcIA).index()
        pte = generatedFunction.getProcTableEntry(pte_num)

        expression = pte.expression_num!!

        assert(expression is IntegerIA || expression is IdentIA)
    }

    private fun _implement_construct_type(co: Constructable?, constructorName: String?,
                                          aTyn1: NormalTypeName, aGenType: GenType?) {
        val s = aTyn1.name
        val ich = _inj().new_ICH(aGenType, this)
        val best = ich.lookupTypeName(aTyn1, s)
        val clsinv = ich.getClassInvocation(constructorName, aTyn1, aGenType, best)
        if (co != null) {
            genTypeCI_and_ResolveTypeToClass(co, clsinv)
        }
        pte.classInvocation = clsinv
        pte.resolvedElement = best
        // set FunctionInvocation with pte args
        run {
            var cc: ConstructorDef? = null
            if (constructorName != null) {
                val cs = best.constructors
                for (c in cs) {
                    if (c.name() == constructorName) {
                        cc = c
                        break
                    }
                }
            }
            // TODO also check arguments
            run {
                // TODO is cc ever null (default_constructor)
                if (cc == null) {
                    // assert pte.getArgs().size() == 0;
                    for (item in best.items) {
                        if (item is ConstructorDef) {
                            if (item.args.size == pte.args?.size) {
                                // TODO we now have to find a way to check arg matching of two different types
                                // of arglists. This is complicated by the fact that constructorDef doesn't have
                                // to specify the argument types and currently, pte args is underspecified

                                // TODO this is explicitly wrong, but it works for now

                                cc = item
                                break
                            }
                        }
                    }
                }
                // TODO do we still want to do this if cc is null?
                val fi = deduceTypes2.newFunctionInvocation(cc, pte, clsinv, deduceTypes2.phase)
                pte.functionInvocation = fi
            }
        }
    }

    private fun _inj(): DeduceTypes2Injector {
        return deduceTypes2._inj()
    }

    @Throws(FCA_Stop::class)
    fun action(aContext: Context?) {
        if (expression is IntegerIA) {
            action_IntegerIA()
        } else if (expression is IdentIA) {
            action_IdentIA(aContext)
        } else {
            throw IllegalStateException("this.expression is of the wrong type")
        }

        deduceTypes2.activePTE(pte, pte.classInvocation)
    }

    @Throws(FCA_Stop::class)
    fun action_IdentIA(aContext: Context?) {
        val idte = (expression as IdentIA).entry
        val deducePath = idte.buildDeducePath(generatedFunction)

        if (pte.dpc == null) {
            pte.dpc = _inj().new_DeduceProcCall(pte)
            pte.dpc.setDeduceTypes2(deduceTypes2, aContext, generatedFunction, deduceTypes2._errSink()) // TODO setting
            // here
            // seems
            // right.
            // Don't
            // check
            // member
        }

        val dpc = pte.dpc

        dpc.targetP2()!!.then(DoneCallback { target: DeduceElement ->
            val xxv = target.declAnchor()
            SimplePrintLoggerToRemoveSoon.println_out_4("144 $xxv")

            run {
                // for class_instantiation2: class Bar {constructor x{}} class Main {main(){var
                // bar:Bar[SysInt];construct bar.x ...}}
                // deducePath.getElement(0) == [bar]
                // deducePath.getElement(1) == [x]
                // deducePath.
                if (target != null) {
                    deducePath.setTarget(target)
                }
            }
            action_IdentIA___0001(idte, deducePath, dpc, target)
        })
    }

    private fun action_IdentIA___0001(idte: IdentTableEntry, deducePath: DeducePath,
                                      dpc: DeduceProcCall, target: DeduceElement) {
        var el3: OS_Element?
        var ectx = generatedFunction.fd.context
        for (i in 0 until deducePath.size()) {
            val ia2 = deducePath.getIA(i)

            el3 = deducePath.getElement(i)

            var p = false

            if (ia2 is IntegerIA) {
                val vte = ia2.entry
                assert(vte.vtt != VariableTableType.TEMP)
                assert(el3 != null)
                assert(i == 0)
                ectx = deducePath.getContext(i)
            } else if (ia2 is IdentIA) {
                val idte2: IdentTableEntry = ia2.entry
                val s = idte2.ident.toString()
                val lrl = ectx!!.lookup(s)

                if (el3 == null) {
                    val yy = 2
                }

                if (lrl == null && ectx is MemberContext) {
                    p = (action_IdentIA___0001_5(deducePath, ectx, i, idte2, s))
                } else {
                    assert(lrl != null // README 12/29 born to fail
                    )
                    val el2 = lrl!!.chooseBest(null)
                    if (el2 == null) {
                        action_IdentIA___0001_3(deducePath, el3, i, idte2, s)
                        p = true
                    } else {
                        if (i + 1 == deducePath.size() && deducePath.size() > 1) {
                            if (el2 is ConstructorDef) {
                                action_IdentIA___0001_2(deducePath, i, idte2, s)
                            } else if (el2 is ClassStatement) {
                                action_IdentIA___0001_1(idte, deducePath, dpc, target, i, idte2, s)
                            } else throw NotImplementedException()
                        } else {
                            ectx = deducePath.getContext(i)
                        }
                    }
                }
            }

            if (p) break
        }
    }

    private fun action_IdentIA___0001_1(idte: IdentTableEntry, deducePath: DeducePath,
                                        dpc: DeduceProcCall, target: DeduceElement, i: Int,
                                        idte2: IdentTableEntry, s: String) {
        var type = deducePath.getType(i)

        // FIXME or idte2??
        if (idte.type == null) {
            val osType = _inj().new_OS_UserType((target.element() as VariableStatementImpl).typeName())
            idte.type = dpc._generatedFunction()!!.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, osType)

            type = idte.type?.genType

            deducePath.injectType(i, type)
        }

        if (type!!.nonGenericTypeName != null) {
            if ((type.nonGenericTypeName as NormalTypeName).genericPart.size() > 0) {
                // goal: create an equavalent (Regular)TypeName without the genericPart
                // method: copy the context and the name
                val rtn = _inj().new_RegularTypeNameImpl(type.nonGenericTypeName.context)
                rtn.setName((type.nonGenericTypeName as NormalTypeName).realName)
                type.nonGenericTypeName = rtn
            }
            // type.nonGenericTypeName = Objects.requireNonNull(deducePath.getType(i -
            // 1)).nonGenericTypeName; // HACK. not guararnteed to work!
        }

        implement_construct_type(idte2, type.typeName, s, type)

        val x = deducePath.getEntry(i - 1) as VariableTableEntry?
        if (type.ci == null && type.node == null) type.genCIForGenType2(deduceTypes2)
        assert(x != null)
        x!!.resolveTypeToClass(type.node)
    }

    private fun action_IdentIA___0001_2(deducePath: DeducePath, i: Int,
                                        idte2: IdentTableEntry, s: String) {
        val type = deducePath.getType(i)
        if (type!!.nonGenericTypeName == null) {
            type.nonGenericTypeName = Objects.requireNonNull(deducePath.getType(i - 1))?.nonGenericTypeName // HACK.
            // not
            // guararnteed
            // to
            // work!
        }
        val ty: OS_Type = _inj().new_OS_UserType(type.nonGenericTypeName)
        implement_construct_type(idte2, ty, s, type)

        val x = deducePath.getEntry(i - 1) as VariableTableEntry?
        if (type.ci == null && type.node == null) type.genCIForGenType2(deduceTypes2)
        assert(x != null)
        x!!.resolveTypeToClass(type.node)
    }

    private fun action_IdentIA___0001_3(deducePath: DeducePath, el3: OS_Element?,
                                        i: Int, idte2: IdentTableEntry, s: String) {
        assert(el3 is VariableStatementImpl)
        val vs = el3 as VariableStatementImpl?
        val tn = vs!!.typeName()
        val ty: OS_Type = _inj().new_OS_UserType(tn)

        var resolved: GenType? = null
        if (idte2.type == null) {
            // README Don't remember enough about the constructors to select a different one
            val tte = generatedFunction.newTypeTableEntry(TypeTableEntry.Type.TRANSIENT, ty)
            try {
                resolved = deduceTypes2.resolve_type(ty, tn.context)
                deduceTypes2.LOG.err("892 resolved: $resolved")
                tte.setAttached(resolved)
            } catch (aResolveError: ResolveError) {
                deduceTypes2._errSink().reportDiagnostic(aResolveError)
            }

            idte2.type = tte
        }
        // s is constructor name
        implement_construct_type(idte2, ty, s, null)

        if (resolved == null) {
            try {
                resolved = deduceTypes2.resolve_type(ty, tn.context)
            } catch (aResolveError: ResolveError) {
                deduceTypes2._errSink().reportDiagnostic(aResolveError)
                assert(false)
            }
        }
        val x = deducePath.getEntry(i - 1) as VariableTableEntry?
        x!!.resolveType(resolved!!)
        resolved.genCIForGenType2(deduceTypes2)
        return
    }

    private fun action_IdentIA___0001_4(deducePath: DeducePath, i: Int,
                                        idte2: IdentTableEntry, s: String, ty: OS_Type,
                                        resolved: Operation2<GenType>) {
        val success = resolved.success()

        idte2.setStatus(BaseTableEntry.Status.KNOWN,
                _inj().new_GenericElementHolder(success.resolved.element))

        deduceTypes2.LOG.err("892 resolved: $success")

        implement_construct_type(idte2, ty, s, null)

        /*
		 * if (success == null) { try { success = resolve_type(ty, ectx); } catch
		 * (ResolveError aResolveError) { errSink.reportDiagnostic(aResolveError); //
		 * aResolveError.printStackTrace(); assert false; } }
		 */
        val x = deducePath.getEntry(i - 1) as VariableTableEntry?
        x!!.resolveType(success)
        // success.genCIForGenType2(DeduceTypes2.this);
        return
    }

    private fun action_IdentIA___0001_5(deducePath: DeducePath,
                                        ectx: MemberContext, i: Int, idte2: IdentTableEntry,
                                        s: String): Boolean {
        val de3_idte = deduceTypes2._zero_getIdent(idte2, generatedFunction,
                deduceTypes2)
        val de3_idte_type = de3_idte.type()

        val ty = de3_idte_type.genType().typeName

        Preconditions.checkState(ty.type == OS_Type.Type.USER)

        val resolved = de3_idte_type.resolved(ectx)

        if (resolved.mode() == Mode.FAILURE) {
            deduceTypes2._errSink().reportDiagnostic(resolved.failure())
        } else {
            action_IdentIA___0001_4(deducePath, i, idte2, s, ty, resolved)
            return true
        }
        return false
    }

    fun action_IntegerIA() {
        val vte = (expression as IntegerIA).entry
        val attached = vte.typeTableEntry.attached
        //			assert attached != null; // TODO will fail when empty variable expression
        if (attached != null && attached.type == OS_Type.Type.USER) {
            implement_construct_type(vte, attached, null, vte.typeTableEntry.genType)
        } else {
            val ty2 = vte.typeTableEntry.genType.typeName!!
            implement_construct_type(vte, ty2, null, vte.typeTableEntry.genType)
        }
    }

    private fun genTypeCI_and_ResolveTypeToClass(co: Constructable,
                                                 aClsinv: ClassInvocation) {
        if (co is IdentTableEntry) {
            co.type?.genTypeCI(aClsinv)
            aClsinv.resolvePromise().then { gn: EvaClass? -> co.resolveTypeToClass(gn) }
        } else if (co is VariableTableEntry) {
            co.typeTableEntry.genTypeCI(aClsinv)
            aClsinv.resolvePromise().then { aNode: EvaClass? -> co.resolveTypeToClass(aNode) }
        }
    }

    private fun implement_construct_type(co: Constructable?, aTy: OS_Type,
                                         constructorName: String?, aGenType: GenType?) {
        check(aTy.type == OS_Type.Type.USER) { "must be USER type" }

        val tyn = aTy.typeName
        if (tyn is NormalTypeName) {
            _implement_construct_type(co, constructorName, tyn, aGenType)
        }

        val classInvocation = pte.classInvocation
        if (co != null) {
            co.setConstructable(pte)
            assert(classInvocation != null)
            classInvocation!!.resolvePromise().done { aNode: EvaClass? -> co.resolveTypeToClass(aNode) }
        }

        if (classInvocation != null) {
            if (classInvocation.constructorName != null) {
                val classStatement = classInvocation.klass
                val generateFunctions = deduceTypes2
                        .getGenerateFunctions(classStatement.context.module())
                var cc: ConstructorDef? = null
                run {
                    val cs = classStatement.constructors
                    for (c in cs) {
                        if (c.name() == constructorName) {
                            cc = c
                            break
                        }
                    }
                }

                assert(cc != null // README 12/29 born to fail
                )
                val gen = _inj().new_WlGenerateCtor(generateFunctions, pte.functionInvocation,
                        cc!!.nameNode, deduceTypes2._phase().codeRegistrar)
                gen.run(null)
                val gc = gen.result
                classInvocation.resolveDeferred().then { result: EvaClass ->
                    result.addConstructor(gc?.cd!!, gc)
                    val wl = _inj().new_WorkList()
                    val coll: List<BaseEvaFunction> = ArrayList()

                    wl.addJob(deduceTypes2._inj().new_WlDeduceFunction(gen, coll, deduceTypes2))
                    deduceTypes2.wm.addJobs(wl)
                }
            }
        }
    }
}
