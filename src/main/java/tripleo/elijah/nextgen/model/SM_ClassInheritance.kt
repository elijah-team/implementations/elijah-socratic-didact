package tripleo.elijah.nextgen.model

interface SM_ClassInheritance {
    fun names(): List<SM_Name?>?
}
