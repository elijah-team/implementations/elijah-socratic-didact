/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp

import org.jetbrains.annotations.Contract
import tripleo.elijah.comp.AccessBus.*
import tripleo.elijah.comp.i.CB_Output
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.internal.CR_State
import tripleo.elijah.comp.internal.Provenance
import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.notation.GN_GenerateNodesIntoSink
import tripleo.elijah.comp.notation.GN_GenerateNodesIntoSinkEnv
import tripleo.elijah.g.GPipelineAccess
import tripleo.elijah.lang.i.OS_Element2
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.nextgen.outputtree.EOT_OutputFile
import tripleo.elijah.nextgen.outputtree.EOT_OutputFileImpl
import tripleo.elijah.nextgen.outputtree.EOT_OutputType
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_fn.EvaFunction
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_generic.DoubleLatch
import tripleo.elijah.stages.gen_generic.pipeline_impl.DefaultGenerateResultSink
import tripleo.elijah.stages.gen_generic.pipeline_impl.ProcessedNode
import tripleo.elijah.stages.gen_generic.pipeline_impl.ProcessedNode1
import tripleo.elijah.stages.write_stage.pipeline_impl.EOT_FileNameProvider__ofString
import tripleo.elijah.util.Helpers
import tripleo.elijah.util2.DebugFlags
import java.util.function.Consumer

/**
 * Created 8/21/21 10:16 PM
 */
class EvaPipeline @Contract(pure = true) constructor(pa0: GPipelineAccess) : PipelineMember(), AB_LgcListener {
    private val ce: CompilationEnclosure
    private val pa = pa0 as IPipelineAccess
    private val grs: DefaultGenerateResultSink
    private val latch2: DoubleLatch<List<EvaNode>>
    private val functionStatements: MutableList<FunctionStatement> = ArrayList()
    private var _processOutput: CB_Output? = null
    private var _processState: CR_State? = null

    @Suppress("unused")
    private var _lgc: List<EvaNode>? = null

    @Suppress("unused")
    private var pipelineLogic: PipelineLogic? = null


    init {
        //
        ce = pa.compilationEnclosure

        //
        pa.subscribePipelineLogic({ pipelineLogic = it!! })
        pa.subscribe_lgc({ this._lgc = it!! })

        //
        latch2 = DoubleLatch(Consumer { this.lgc_slot(it as List<EvaNode>) })

        //
        grs = DefaultGenerateResultSink(pa)
        pa.registerNodeList { att: List<EvaNode> -> latch2.notifyData(att) }
        pa.generateResultSink = grs

        pa.setEvaPipeline(this)

        pa.install_notate(Provenance.EvaPipeline__lgc_slot, GN_GenerateNodesIntoSink::class.java, GN_GenerateNodesIntoSinkEnv::class.java)
    }

    override fun lgc_slot(aLgc1: List<EvaNode>) {
        val nodesThatWereProcesed = ArrayList(aLgc1)

        val nodes = processLgc(nodesThatWereProcesed)
        val y = 2

        for (evaNode in nodesThatWereProcesed) {
            _processOutput!!.logProgress(160, "EvaPipeline.recieve >> $evaNode")
        }

        nodesThatWereProcesed.forEach(Consumer { evaNode: EvaNode -> this.nodeToOutputFileDump(evaNode) })
        functionStatements.forEach(Consumer { functionStatement: FunctionStatement -> this.functionStatementToOutputFileDump(functionStatement) })

        val compilationEnclosure = pa.compilationEnclosure

        compilationEnclosure.pipelineAccessPromise.then { pa: IPipelineAccess ->
            val env = GN_GenerateNodesIntoSinkEnv(
                    nodes,
                    grs,  //ce.getCompilation().world().modules(),
                    compilationEnclosure.compilationAccess.testSilence(),
                    pa.accessBus.gr,
                    compilationEnclosure
            )
            _processOutput!!.logProgress(117, "EvaPipeline >> GN_GenerateNodesIntoSink")
            pa.notate(Provenance.EvaPipeline__lgc_slot, env)
        }
    }

    private fun functionStatementToOutputFileDump(functionStatement: FunctionStatement) {
        val filename = functionStatement.getFilename(pa)
        val seq = EG_Statement.of(functionStatement.text, EX_Explanation.withMessage("dump2"))
        if (DebugFlags.writeDumps) {
            val cot = ce.compilation.outputTree
            val off: EOT_OutputFile = EOT_OutputFileImpl(Helpers.List_of(), EOT_FileNameProvider__ofString(filename), EOT_OutputType.DUMP, seq)
            cot.add(off)
        }
    }

    private fun nodeToOutputFileDump(evaNode: EvaNode) {
        val filename1: EOT_FileNameProvider
        var filename: String? = null
        val sb = StringBuffer()

        if (evaNode is EvaClass) {
            filename = "C_" + evaNode.code + evaNode.name
            sb.append("CLASS %d %s\n".formatted(evaNode.code, evaNode.name))
            for (varTableEntry in evaNode.varTable) {
                sb.append("MEMBER %s %s".formatted(varTableEntry.nameToken, varTableEntry.varType))
            }
            for ((_, v) in evaNode.functionMap) {
                sb.append("FUNCTION %d %s\n".formatted(v.code1234567, v.fd.nameNode.text))

                pa.activeFunction(v)
            }

            filename1 = EOT_FileNameProvider2( {
                val filename2 = "C_" + evaNode.code + evaNode.name
                filename2
            })

            pa.activeClass(evaNode)
        } else if (evaNode is EvaNamespace) {
            filename = "N_" + evaNode.code1234567 + evaNode.name
            sb.append("NAMESPACE %d %s\n".formatted(evaNode.code1234567, evaNode.name))
            for (varTableEntry in evaNode.varTable) {
                sb.append("MEMBER %s %s\n".formatted(varTableEntry.nameToken, varTableEntry.varType))
            }
            for ((_, v) in evaNode.functionMap) {
                sb.append("FUNCTION %d %s\n".formatted(v.code1234567, v.fd.nameNode.text))
            }

            filename1 = EOT_FileNameProvider2( {
                val filename2 = "N_" + evaNode.code1234567 + evaNode.name
                filename2
            })

            pa.activeNamespace(evaNode)
        } else if (evaNode is EvaFunction) {
            var code: Int = evaNode.code1234567

            if (code == 0) {
                val cr = ce.pipelineLogic.dp.codeRegistrar
                cr.registerFunction1(evaNode)

                code = evaNode.code1234567
                assert(code != 0)
            }

            val functionName: String = evaNode.functionName
            filename = "F_$code$functionName"

            val finalCode = code
            filename1 = EOT_FileNameProvider2( {
                val functionName: String = evaNode.functionName
                val filename2 = "F_$finalCode$functionName"
                filename2
            })

            val str = "FUNCTION %d %s %s\n".formatted(code, functionName,
                    (evaNode.fd.parent as OS_Element2?)!!.name())
            sb.append(str)
            pa.activeFunction(evaNode)
        } else {
            throw IllegalStateException("Can't determine node")
        }

        val seq = EG_Statement.of(sb.toString(), EX_Explanation.withMessage("dump"))
        if (DebugFlags.writeDumps) {
            val cot = pa.compilation.outputTree
            val off: EOT_OutputFile = EOT_OutputFileImpl(Helpers.List_of(), filename1, EOT_OutputType.DUMP, seq)
            cot.add(off)
        }
    }

    fun addFunctionStatement(aFunctionStatement: FunctionStatement) {
        functionStatements.add(aFunctionStatement)
    }

    fun grs(): DefaultGenerateResultSink {
        return grs
    }

    override fun run(aSt: CR_State, aOutput: CB_Output?) {
        _processState = aSt
        _processOutput = aOutput

        latch2.notifyLatch(true)
    }

    override fun finishPipeline_asString(): String {
        return this.javaClass.toString()
    }

    companion object {
        fun processLgc(aLgc: List<EvaNode>): List<ProcessedNode> {
            val l: MutableList<ProcessedNode> = ArrayList()

            for (evaNode in aLgc) {
                l.add(ProcessedNode1(evaNode))
            }

            return l
        }
    }
}

@Suppress("FunctionName")
fun EOT_FileNameProvider2(x: ()->String): EOT_FileNameProvider {
    return `__`.EOT_FileNameProvider2(x)
}