package tripleo.elijah.stages.deduce.nextgen

import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.stages.gen_fn.ProcTableEntry

class DR_ProcCall(private val z: IExpression, private val pte: ProcTableEntry, private val baseEvaFunction: BaseEvaFunction) : DR_Item {
    val functionInvocation: FunctionInvocation
        get() = pte.functionInvocation!!
}
