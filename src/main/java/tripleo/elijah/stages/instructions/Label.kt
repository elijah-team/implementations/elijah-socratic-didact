/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.instructions

import tripleo.elijah.stages.gen_fn.BaseEvaFunction

/**
 * Created 9/10/20 3:17 PM
 */
class Label //	public Label(String name) {
//		this.name = name;
//	}
(private val gf: BaseEvaFunction) : InstructionArgument {
    /**
     * Corresponds to pc
     *
     * @param index pc
     */
    var index: Long = 0
    var name: String? = null

    /**
     * Corresponds to the number of labels
     *
     * @param number
     */
    var number: Int = 0

    override fun toString(): String {
        return String.format("<Label %s index:%d number:%d>", name, index, number)
    }
} //
//
//

