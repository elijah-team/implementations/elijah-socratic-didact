/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.ci_impl

import antlr.Token
import lombok.Setter
import tripleo.elijah.ci.CiExpressionList
import tripleo.elijah.ci.CiIndexingStatement
import tripleo.elijah.ci.CompilerInstructions

/**
 * @author Tripleo
 *
 * Created Apr 15, 2020 at 4:59:21 AM
 * Created 1/8/21 7:19 AM
 */
// TODO data class??
class CiIndexingStatementImpl(private val parent: CompilerInstructions) : CiIndexingStatement {
    @Setter
    private var exprs: CiExpressionList? = null

    @Setter
    private var name: Token? = null

    override fun setExprs(arg0: CiExpressionList) {
        // 24/01/04 back and forth
        this.exprs = arg0
    }

    override fun setName(arg0: Token) {
        // 24/01/04 back and forth
        this.name = arg0
    }
} //
//
//

