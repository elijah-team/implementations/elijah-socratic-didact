package tripleo.elijah.nextgen.outputstatement

// README a mini EG_Statement
interface IReasonedString {
    fun text(): String?

    fun reason(): String
}
