/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import org.jdeferred2.DoneCallback
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.nextgen.reactive.DefaultReactive
import tripleo.elijah.stages.deduce.DeduceElement
import tripleo.elijah.stages.deduce.DeduceTypes2
import tripleo.elijah.stages.deduce.DeduceTypes2.ExpectationBase
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.OnGenClass
import tripleo.elijah.stages.deduce.nextgen.*
import tripleo.elijah.stages.gen_fn.IEvaFunctionBase.BaseEvaFunction_Reactive
import tripleo.elijah.stages.gen_generic.Dependency
import tripleo.elijah.stages.gen_generic.IDependencyReferent
import tripleo.elijah.stages.instructions.*
import tripleo.elijah.util.*
import tripleo.elijah.util2.Eventual
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah.world.impl.DefaultLivingFunction
import tripleo.util.range.Range
import java.util.*
import java.util.function.Consumer

/**
 * Created 9/10/20 2:57 PM
 */
abstract class BaseEvaFunction
	: AbstractDependencyTracker(), EvaNode, ExpectationBase, IDependencyReferent, IEvaFunctionBase {

	private val _p_assignEvaClass: Eventual<EvaClass> = Eventual()
	private val _p_assignGenType: Eventual<GenType> = Eventual()
	private val dependency: Dependency = Dependency(this)
	private val labelList: MutableList<Label> = ArrayList()



//	override fun addVariableTableEntry(
//			name: String,
//			vtt: VariableTableType,
//			type: TypeTableEntry,
//			el: OS_Element,
//	): Int {
//		val vte = VariableTableEntry(vte_list.size, vtt, name, type, el)
//		vte_list.add(vte)
//		return vte.index
//	}




	@JvmField
	var drs: MutableList<DR_Item> = ArrayList()
	var _living: DefaultLivingFunction? = null

	@JvmField
	var cte_list: MutableList<ConstantTableEntry> = ArrayList()

	@JvmField
	var deducedAlready: Boolean = false

	@JvmField
	var deferred_calls: MutableList<Int?> = ArrayList()

	@JvmField
	var fi: FunctionInvocation? = null

	@JvmField
	var idte_list: MutableList<IdentTableEntry> = ArrayList()

	@JvmField
	var instructionsList: MutableList<Instruction> = ArrayList()

	@JvmField
	var prte_list: MutableList<ProcTableEntry> = ArrayList()

	@JvmField
	var tte_list: MutableList<TypeTableEntry> = ArrayList()

	@JvmField
	var vte_list: MutableList<VariableTableEntry> = ArrayList()
	private val elements: MutableMap<OS_Element, DeduceElement> = HashMap()
	private var _nextTemp: Int = 0
	private var _reactive: __Reactive? = null
	private var code: Int = 0
	private var genClass: EvaNode? = null
	private var instruction_index: Int = 0
	private var label_count: Int = 0
	private var parent: EvaContainerNC? = null

	override fun add(aName: InstructionName, args_: List<InstructionArgument>, ctx: Context): Int {
		val i: Instruction = Instruction()
		i.index = instruction_index
		instruction_index++
		i.name = aName
		i.setArgs(args_)
		i.context = ctx
		instructionsList.add(i)
		return i.index
	}

	override fun addContext(context: Context, r: Range) {
//		contextToRangeMap.put(r, context);
	}

	override fun addElement(aElement: OS_Element, aDeduceElement: DeduceElement) {
		elements.put(aElement, aDeduceElement)
	}

	override fun addIdentTableEntry(ident: IdentExpression, context: Context): Int {
		for (i in idte_list.indices) {
			if (idte_list.get(i).getIdent() === ident && idte_list.get(i).pc === context) return i
		}
		val idte: IdentTableEntry = IdentTableEntry(idte_list.size, ident, context, this)

		idte._ident = (getIdent(idte))

		idte_list.add(idte)
		return idte.index
	}

	override fun addLabel(): Label {
		return addLabel("__label", true)
	}

	override fun addLabel(base_name: String, append_int: Boolean): Label {
		val label: Label = Label(this)
		val name: String
		if (append_int) {
			label.number = label_count
			name = String.format("%s%d", base_name, label_count)
			label_count++
		} else {
			name = base_name
			label.number = label_count
		}
		label.name = name
		labelList.add(label)
		return label
	}

	override fun findLabel(index: Int): Label? {
		for (label: Label in labelList) {
			if (label.index == index.toLong()) return label
		}
		return null
	}

	override fun get_assignment_path(
			expression: IExpression,
			generateFunctions: GenerateFunctions,
			context: Context,
	): InstructionArgument {
		when (expression.getKind()) {
			ExpressionKind.DOT_EXP -> {
				val de: DotExpression = expression as DotExpression
				val left_part: InstructionArgument = get_assignment_path(de.getLeft(), generateFunctions, context)
				return get_assignment_path(left_part, de.getRight(), generateFunctions, context)
			}

			ExpressionKind.QIDENT -> throw NotImplementedException()
			ExpressionKind.PROCEDURE_CALL -> {
				// keep us forever in the presence
				val pce: ProcedureCallExpression = expression as ProcedureCallExpression
				if (pce.getLeft() is IdentExpression) {
					val identExpression = pce.left as IdentExpression
					val idte_index: Int = addIdentTableEntry(identExpression, identExpression.getContext())
					val identIA = IdentIA(idte_index, this)
					val args_types: List<TypeTableEntry> = generateFunctions.get_args_types(pce.getArgs(), this, context)
					val i: Int = generateFunctions.addProcTableEntry(pce, identIA, args_types, this)
					return ProcIA(i, this)
				}
				// TODO this looks wrong. what are we supposed to be doing here?
				return get_assignment_path(pce.getLeft(), generateFunctions, context)
			}

			ExpressionKind.GET_ITEM -> throw NotImplementedException()
			ExpressionKind.IDENT -> {
				val ie: IdentExpression = expression as IdentExpression
				val text: String = ie.getText()
				val lookup: InstructionArgument? = vte_lookup(text) // IntegerIA(variable) or ConstTableIA or null
				if (lookup != null) return lookup
				val ite: Int = addIdentTableEntry(ie, context)
				return IdentIA(ite, this)
			}

			else -> throw IllegalStateException("Unexpected value: " + expression.getKind())
		}
	}

	fun getCode1234567(): Int {
		return code
	}

	override fun getConstTableEntry(index: Int): ConstantTableEntry {
		return cte_list[index]
	}

	override fun getContextFromPC(pc: Int): Context {
//		for (Map.Entry<Range, ContextImpl> rangeContextEntry : contextToRangeMap.entrySet()) {
//			if (rangeContextEntry.getKey().has(pc))
//				return rangeContextEntry.getValue();
//		}
//		return null;
		return instructionsList.get(pc).context!!
	}

	override fun getDependency(): Dependency {
		return dependency
	}

	//	Map<Range, ContextImpl> contextToRangeMap = new HashMap<Range, ContextImpl>();
	override fun getFunctionName(): String {
		// TODO change to abstract with override??
		if (this is EvaConstructor) {
			val y: Int = 2
			val constructorName: IdentExpression = getFD().getNameNode()
			val constructorNameText: String
			if (constructorName === LangGlobals.emptyConstructorName) {
				constructorNameText = ""
			} else {
				constructorNameText = constructorName.getText()
			}
			return constructorNameText
		} else {
			return getFD().getNameNode().getText()
		}
	}

	override fun getGenClass(): EvaNode {
		return (genClass)!!
	}

	/**
	 * Returns a string that represents the path encoded by ia2. Does not transform
	 * the string into target language (ie C). Called from
	 * [DeduceTypes2.do_assign_call]
	 * or [DeduceTypes2.deduce_generated_function]
	 * or
	 * [DeduceTypes2.resolveIdentIA_]
	 *
	 * @param ia2 the path
	 * @return a string that represents the path encoded by ia2
	 */
	override fun getIdentIAPathNormal(ia2: IdentIA): String {
		val s: List<InstructionArgument> = _getIdentIAPathList(ia2)

		//
		// TODO NOT LOOKING UP THINGS, IE PROPERTIES, MEMBERS
		//
		val sl: MutableList<String> = ArrayList()
		for (ia: InstructionArgument? in s) {
			val text: String
			if (ia is IntegerIA) {
				val vte: VariableTableEntry = getVarTableEntry(DeduceTypes2.to_int(ia))
				text = vte.name
			} else if (ia is IdentIA) {
				val idte: IdentTableEntry = getIdentTableEntry(ia.index)
				text = idte.getIdent()!!.getText()
			} else if (ia is ProcIA) {
				val prte: ProcTableEntry = getProcTableEntry(DeduceTypes2.to_int(ia))
				assert(prte.__debug_expression is ProcedureCallExpression)
				text = (prte.__debug_expression as ProcedureCallExpression).printableString()
			} else throw NotImplementedException()
			sl.add(text)
		}
		return Helpers.String_join(".", sl)
	}

	override fun getIdentTableEntry(index: Int): IdentTableEntry {
		return idte_list.get(index)
	}

	/**
	 * Returns first [IdentTableEntry] that matches expression Only works for
	 * IdentExpressions
	 *
	 * @param expression [IdentExpression] to test for
	 * @return IdentTableEntry or null
	 */
	override fun getIdentTableEntryFor(expression: IExpression): IdentTableEntry? {
		for (identTableEntry: IdentTableEntry in idte_list) {
			// TODO make this work for Qualidents and DotExpressions
			if (((identTableEntry.getIdent()!!.getText() == (expression as IdentExpression).getText()) && identTableEntry.backlink == null)) {
				return identTableEntry
			}
		}
		return null
	}

	override fun getInstruction(anIndex: Int): Instruction {
		return instructionsList.get(anIndex)
	}

	override fun getParent(): EvaContainerNC {
		return (parent)!!
	}

	override fun getProcTableEntry(index: Int): ProcTableEntry {
		return prte_list.get(index)
	}

	override fun getTypeTableEntry(index: Int): TypeTableEntry {
		return tte_list.get(index)
	}

	override fun getVarTableEntry(index: Int): VariableTableEntry {
		return vte_list.get(index)
	}

	override fun instructions(): List<Instruction> {
		return instructionsList
	}

	override fun labels(): List<Label> {
		return labelList
	}

	override fun newTypeTableEntry(type1: TypeTableEntry.Type, type: OS_Type?): TypeTableEntry {
		return newTypeTableEntry(type1, type, null, null)
	}

	override fun newTypeTableEntry(
			type1: TypeTableEntry.Type,
			type: OS_Type?,
			initialValue: IExpression?,
	): TypeTableEntry {
		return newTypeTableEntry(type1, type, initialValue, null)
	}

	override fun newTypeTableEntry(
			type1: TypeTableEntry.Type,
			type: OS_Type?,
			expression: IExpression?,
			aTableEntryIV: TableEntryIV?,
	): TypeTableEntry {
		val typeTableEntry: TypeTableEntry = TypeTableEntry(tte_list.size, type1, type, expression, aTableEntryIV)
		typeTableEntry.setAttached(type) // README make sure tio call callback
		tte_list.add(typeTableEntry)
		return typeTableEntry
	}

	override fun newTypeTableEntry(
			type1: TypeTableEntry.Type,
			type: OS_Type,
			aTableEntryIV: TableEntryIV,
	): TypeTableEntry {
		return newTypeTableEntry(type1, type, null, aTableEntryIV)
	}

	override fun nextTemp(): Int {
		++_nextTemp
		return _nextTemp
	}

	override fun place(label: Label) {
		label.index = instruction_index.toLong()
	}

	override fun resolveTypeDeferred(aType: GenType) {
		if (_p_assignGenType.isPending) _p_assignGenType.resolve(aType)
		else {
			val holder: Holder<GenType> = Holder()
			_p_assignGenType.then(object : DoneCallback<GenType> {
				override fun onDone(result: GenType) {
					holder.set(result)
				}
			})
			SimplePrintLoggerToRemoveSoon.println_err_2(String.format("Trying to resolve function twice 1) %s 2) %s",
					holder.get().asString(), aType.asString()))
		}
	}

	override fun setClass(aNode: EvaNode) {
		assert(aNode is EvaClass || aNode is EvaNamespace)
		//assert ((EvaContainerNC) aNode).getCode() != 0;
		if ((aNode as EvaContainerNC).code == 0) {
			//throw new AssertionError();
			SimplePrintLoggerToRemoveSoon.println_err_4("504504 node is not coded in setClass " + aNode.identityString())
		}

		genClass = aNode

		if (aNode is EvaClass) {
			_p_assignEvaClass.resolve(aNode)
		} else {
			throw IllegalArgumentException("504512 aNode is not EvaClass")
		}
	}

	override fun setParent(aGeneratedContainerNC: EvaContainerNC) {
		parent = aGeneratedContainerNC
	}

	override fun typeDeferred(): Eventual<GenType> {
		return _p_assignGenType
	}

	override fun typePromise(): Eventual<GenType> {
		return _p_assignGenType
	}

	/**
	 * @param text variable name from the source file
	 * @return [IntegerIA] or [ConstTableIA] or null if not found,
	 * meaning not a local variable
	 */
	override fun vte_lookup(text: String): InstructionArgument? {
		var index: Int = 0
		for (variableTableEntry: VariableTableEntry in vte_list) {
			val variableTableEntryName: String = variableTableEntry.name
			if (variableTableEntryName != null) // null when temp
				if ((variableTableEntryName == text)) return IntegerIA(index, this)
			index++
		}
		index = 0
		for (constTableEntry: ConstantTableEntry in cte_list) {
			val constTableEntryName: String? = constTableEntry.name
			if (constTableEntryName != null) // null when not assigned
				if ((constTableEntryName == text)) return ConstTableIA(index, this)
			index++
		}
		return null
	}

	override fun setCode(aCode: Int) {
		code = aCode
	}

	fun buildDrTypeFromNonGenericTypeName(aNonGenericTypeName: TypeName?): DR_Type {
		val r: DR_Type = DR_Type(this, aNonGenericTypeName)
		r.build()
		return r
	}

	fun elements(): Map<OS_Element, DeduceElement> {
		return elements
	}

	override fun expectationString(): String {
		return toString()
	}

	private fun get_assignment_path(
			prev: InstructionArgument,
			expression: IExpression,
			generateFunctions: GenerateFunctions,
			context: Context,
	): InstructionArgument {
		when (expression.getKind()) {
			ExpressionKind.DOT_EXP -> {
				val de: DotExpression = expression as DotExpression
				val left_part: InstructionArgument = get_assignment_path(de.getLeft(), generateFunctions, context)
				if (left_part is IdentIA) {
					left_part.setPrev(prev)
					//				getIdentTableEntry(to_int(left_part)).addStatusListener(new DeduceTypes2.FoundParent());
				} else throw NotImplementedException()
				return get_assignment_path(left_part, de.getRight(), generateFunctions, context)
			}

			ExpressionKind.QIDENT -> throw NotImplementedException()
			ExpressionKind.PROCEDURE_CALL -> throw NotImplementedException()
			ExpressionKind.GET_ITEM -> throw NotImplementedException()
			ExpressionKind.IDENT -> {
				val ie: IdentExpression = expression as IdentExpression
				val ite: Int = addIdentTableEntry(ie, context)
				val identIA: IdentIA = IdentIA(ite, this)
				identIA.setPrev(prev)
				//			getIdentTableEntry(ite).addStatusListener(new DeduceTypes2.FoundParent()); // inject!
				return identIA
			}

			else -> throw IllegalStateException("Unexpected value: " + expression.getKind())
		}
	}

	fun getIdent(aIdent: IdentExpression?, aVteBl1: VariableTableEntry?): DR_Ident {
		val e: DR_Ident = DR_Ident.create(aIdent, aVteBl1, this)
		drs.add(e)
		return e
	}

	fun getIdent(aIdentTableEntry: IdentTableEntry): DR_Ident {
		val e: DR_Ident = DR_Ident.create(aIdentTableEntry, this)
		drs.add(e)
		return e
	}

	fun getIdent(aVteBl1: VariableTableEntry?): DR_Ident {
		val e: DR_Ident = DR_Ident.create(aVteBl1, this)
		drs.add(e)
		return e
	}

	fun getProcCall(aZ: IExpression?, aPte: ProcTableEntry?): DR_ProcCall {
		val e: DR_ProcCall = DR_ProcCall((aZ)!!, (aPte)!!, this)
		drs.add(e)
		return e
	}

	fun getVar(aElement: VariableStatement?): DR_Variable {
		val e: DR_Variable = DR_Variable((aElement)!!, this)
		drs.add(e)
		return e
	}

	/*
 * Hook in for GeneratedClass
 */
	override fun onGenClass(aOnGenClass: OnGenClass) {
		_p_assignEvaClass.then(DoneCallback({ aGeneratedClass: EvaClass? -> aOnGenClass.accept(aGeneratedClass) }))
	}

	fun reactive(): __Reactive {
		if (_reactive == null) _reactive = __Reactive()
		return (_reactive)!!
	}

	fun findIdentTableIndex(aIdentTableEntry: IdentTableEntry): Int {
		for (i in idte_list.indices) {
			val identTableEntry: IdentTableEntry = idte_list.get(i)
			if (identTableEntry === aIdentTableEntry) {
				return i
			}
		}
		return -1
	}

	inner class __Reactive : DefaultReactive(), BaseEvaFunction_Reactive {
		override fun <T> addListener(t: Consumer<T>?) {
			throw UnintendedUseException()
		}
	}

	companion object {
		fun printTables(gf: EvaFunction) {
			SimplePrintLoggerToRemoveSoon.println_out_2("VariableTable ")
			for (variableTableEntry: VariableTableEntry? in gf.vte_list) {
				SimplePrintLoggerToRemoveSoon.println_out_2("\t" + variableTableEntry)
			}
			SimplePrintLoggerToRemoveSoon.println_out_2("ConstantTable ")
			for (constantTableEntry: ConstantTableEntry? in gf.cte_list) {
				SimplePrintLoggerToRemoveSoon.println_out_2("\t" + constantTableEntry)
			}
			SimplePrintLoggerToRemoveSoon.println_out_2("ProcTable     ")
			for (procTableEntry: ProcTableEntry? in gf.prte_list) {
				SimplePrintLoggerToRemoveSoon.println_out_2("\t" + procTableEntry)
			}
			SimplePrintLoggerToRemoveSoon.println_out_2("TypeTable     ")
			for (typeTableEntry: TypeTableEntry? in gf.tte_list) {
				SimplePrintLoggerToRemoveSoon.println_out_2("\t" + typeTableEntry)
			}
			SimplePrintLoggerToRemoveSoon.println_out_2("IdentTable    ")
			for (identTableEntry: IdentTableEntry? in gf.idte_list) {
				SimplePrintLoggerToRemoveSoon.println_out_2("\t" + identTableEntry)
			}
		}


		@JvmStatic
		fun _getIdentIAPathList(oo: InstructionArgument): List<InstructionArgument> {
			var oo: InstructionArgument? = oo
			val s: LinkedList<InstructionArgument> = LinkedList()
			while (oo != null) {
				if (oo is IntegerIA) {
					s.addFirst(oo)
					oo = null
				} else if (oo is IdentIA) {
					val ite1: IdentTableEntry = oo.entry
					s.addFirst(oo)
					oo = (ite1.backlink)!!
				} else if (oo is ProcIA) {
					s.addFirst(oo)
					oo = null
				} else throw IllegalStateException("Invalid InstructionArgument")
			}
			return s
		}
	}
}

fun _getIdentIAResolvableList(oo: InstructionArgument): List<DT_Resolvable> {
	var oo: InstructionArgument? = oo
	val R: LinkedList<DT_Resolvable> = LinkedList()
	while (oo != null) {
		if (oo is IntegerIA) {
			val vte: VariableTableEntry = oo.entry

			if (vte._vs == null) {
				val el: Array<OS_Element?> = arrayOf(null)
				vte._p_elementPromise.then(DoneCallback({ el1: OS_Element? -> el[0] = el1 }))

				assert(el.get(0) != null)
				R.addFirst(DT_Resolvable.from(oo, el.get(0), null))
			} else {
				R.addFirst(DT_Resolvable.from(oo, vte._vs, null))
			}
			oo = null
		} else if (oo is IdentIA) {
			val ite1: IdentTableEntry = oo.entry

			val el: Array<OS_Element?> = arrayOf(null)
			ite1._p_resolvedElementPromise.then(DoneCallback({ el1: OS_Element? -> el[0] = el1 }))

			// assert el[0] != null;
			var cfi: FunctionInvocation? = null
			if (ite1._callable_pte() != null) {
				val cpte: ProcTableEntry? = ite1._callable_pte()
				if (cpte!!.functionInvocation != null) {
					cfi = cpte.functionInvocation
				}
			}

			// assert cfi != null;
			// ^^ fails for folders.forEach
			R.addFirst(DT_Resolvable.from(oo, el.get(0), cfi))
			oo = (ite1.backlink)!!
		} else if (oo is ProcIA) {
			val pte: ProcTableEntry = oo.entry
			assert(pte != null)
			val el: Array<OS_Element?> = arrayOf(null)
			pte!!._p_elementPromise.then(DoneCallback({ el1: OS_Element? -> el[0] = el1 }))

			assert(el.get(0) != null)
			var cfi: FunctionInvocation? = null
			if (pte.functionInvocation != null) {
				cfi = pte.functionInvocation
			}

			assert(cfi != null)
			R.addFirst(DT_Resolvable.from(oo, el.get(0), cfi))
			oo = null
		} else throw IllegalStateException("Invalid InstructionArgument")
	}
	return R
}

fun _getIdentIAResolvable(aIdentIA: IdentIA): DT_Resolvabley {
	val x: List<DT_Resolvable> = _getIdentIAResolvableList(aIdentIA)

	return DT_Resolvabley(x)
}
