package tripleo.elijah.stages.garish

import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink

class GarishNamespace_Generator(private val carrier: EvaNamespace) {
    private var generatedAlready = false

    fun generatedAlready(): Boolean {
        return generatedAlready
    }

    fun provide(aResultSink: GenerateResultSink,
                aGarishNamespace: GarishNamespace,
                aGr: GenerateResult?,
                aGenerateC: GenerateC) {
        check(!generatedAlready) { "GarishNamespace generated already" }

        // TODO do we need `self' parameters for namespace?
        if (!carrier.varTable.isEmpty()) { // TODO should we let this through?
            aResultSink.add(GarishNamespace__addClass_1(aGarishNamespace, aGr, aGenerateC))
        }

        generatedAlready = true
    }
}
