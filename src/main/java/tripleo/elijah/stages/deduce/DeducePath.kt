/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import org.jdeferred2.DoneCallback
import org.jdeferred2.FailCallback
import org.jetbrains.annotations.Contract
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.ContextImpl
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.deduce.nextgen.DR_Ident
import tripleo.elijah.stages.deduce.nextgen.DR_IdentUnderstandings.ElementUnderstanding
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Created 7/9/21 6:10 AM
 */
class DeducePath @Contract(pure = true) constructor(aIdentTableEntry: IdentTableEntry, aX: List<InstructionArgument>) {
    interface DeducePathItem {
        // base = aIdentTableEntry;
        // ias = aX;
        fun context(): MemberContext?

        fun element(): OS_Element?

        val index: Int

        fun instructionArgument(): InstructionArgument

        fun type(): GenType?
    }

    inner class DeducePathItemImpl(private val instructionArgument: InstructionArgument, override val index: Int) : DeducePathItem {
        private val context: MemberContext? = null
        private val element: OS_Element? = null
        private val type: GenType? = null

        override fun context(): MemberContext? {
            return context
        }

        override fun element(): OS_Element? {
            return element
        }

        override fun instructionArgument(): InstructionArgument {
            return instructionArgument
        }

        override fun type(): GenType? {
            return type
        }
    }

    inner class MemberContext(aDeducePath: DeducePath, aIndex: Int, aElement: OS_Element?) : ContextImpl() {
        private val deducePath: DeducePath
        private val element: OS_Element?
        private val index: Int
        private val type: GenType?

        init {
            assert(aIndex >= 0)
            deducePath = aDeducePath
            index = aIndex
            element = aElement

            type = deducePath.getType(aIndex)
        }

        override fun getParent(): Context? {
            if (index == 0) return element!!.context.parent
            return deducePath.getContext(index - 1)
        }

        override fun lookup(name: String, level: Int, Result: LookupResultList,
                            alreadySearched: ISearchList, one: Boolean): LookupResultList? {
//			if (index == 0)

            if (type!!.resolved == null) {
                // c = getContext(this.index)

                val ell = deducePath.getElement(this.index)
                if (ell == null) {
                    throw AssertionError("202 no element found")
                } else {
                    if (ell is VariableStatementImpl) {
                        val ctx2 = ell.parent!!.context

                        var n2: String? = null
                        if (type.nonGenericTypeName != null) {
                            val ngtn = type.typeName.typeName as RegularTypeName
                            n2 = ngtn.name
                        }

                        if (n2 != null) {
                            return ctx2.lookup(n2, level + 1, Result, alreadySearched, one)
                        }
                    }
                }
                return null
            }

            return type.resolved.element.context.lookup(name, level, Result, alreadySearched, one)
            //			else
//				return null;
        }
    }

    private val base: IdentTableEntry
    private val contexts: Array<MemberContext?>

    private val elements: Array<OS_Element?> // arrays because they never need to be resized

    private val ias: List<InstructionArgument>

    private val types: Array<GenType?>

    init {
        val size = aX.size
        assert(size > 0)
        base = aIdentTableEntry
        ias = aX

        elements = arrayOfNulls(size)
        types    = arrayOfNulls(size)
        contexts = arrayOfNulls(size)
    }

    private fun elementForIndex(aIndex: Int, ia2: IdentIA): OS_Element? {
        var el: OS_Element?
        el = null
        val identTableEntry = ia2.entry
        identTableEntry.onResolvedElement { re: OS_Element? ->
            identTableEntry.setStatus(BaseTableEntry.Status.KNOWN,  // _inj().new_
                    GenericElementHolder(re!!))
            elements[aIndex] = re
        }
        if (identTableEntry.hasResolvedElement()) {
            el = identTableEntry.resolvedElement
            if (aIndex == 0) if (identTableEntry.resolvedElement !== el) identTableEntry.setStatus(BaseTableEntry.Status.KNOWN,  // _inj().new_
                    GenericElementHolder(el!!))
        } else {
            // TODO 06/19 maybe redundant

            val drIdent = identTableEntry.definedIdent
            SimplePrintLoggerToRemoveSoon.println_err_4("" + drIdent)

            //			identTableEntry.elementPromise((x)->{}, null);
            identTableEntry.deduceElement.resolvedElementPromise().then { x: OS_Element? ->
                identTableEntry.setStatus(BaseTableEntry.Status.KNOWN,  // _inj().new_
                        GenericElementHolder(x!!))
            }
        }
        return el
    }

    private fun elementForIndex(aIndex: Int, ia2: IntegerIA): OS_Element? {
        val el: OS_Element
        val vte = ia2.entry
        el = vte.resolvedElement!!
        if (el == null) {
            // never called bc above will NEVER be true due to construction of vte
            vte.elementPromise({ el2: OS_Element? ->
                vte.setStatus(BaseTableEntry.Status.KNOWN,
                        GenericElementHolderWithIntegerIA(el2, (ias[aIndex] as IntegerIA)))
            }, null)
        } else {
            // set this to set resolved_elements of remaining entries
            vte.setStatus(BaseTableEntry.Status.KNOWN,  // dt2._inj().new_
                    GenericElementHolderWithIntegerIA(el, (ias[aIndex] as IntegerIA)))
        }
        return el
    }

    private fun elementForIndex(ia2: ProcIA): OS_Element? {
        var el: OS_Element
        val procTableEntry = ia2.entry
        el = procTableEntry.resolvedElement!! // .expression?

        // TODO no setStatus here?
        val dt2 = procTableEntry._deduceTypes2()

        if (procTableEntry.expression_num is IdentIA) {
            val identIA = procTableEntry.expression_num as IdentIA
            val id: DR_Ident = procTableEntry.__gf!!.getIdent(identIA.entry)

            if (el != null) {
                val understanding = dt2._inj().new_DR_Ident_ElementUnderstanding(el)
                id.u.add(understanding)
            } else {
                for (understanding in id.u) {
                    if (understanding is ElementUnderstanding) {
                        el = understanding.element
                    }
                }
            }
        }

        // assert el != null;
        if (el == null) {
            if (procTableEntry.expression_num is IdentIA) {
                val identIA = procTableEntry.expression_num as IdentIA
                val ite: IdentTableEntry = (identIA.entry)
                SimplePrintLoggerToRemoveSoon.println_err_4("139 element not found for " + ite.ident.text)
            }
            // throw new AssertionError();
        }
        return el
    }

    fun getContext(aIndex: Int): Context? {
        if (contexts[aIndex] == null) {
            val memberContext = MemberContext(this, aIndex, getElement(aIndex))
            contexts[aIndex] = memberContext
            return memberContext
        } else return contexts[aIndex]
    }

    fun getElement(aIndex: Int): OS_Element? {
        if (elements[aIndex] == null) {
            val ia2 = getIA(aIndex)
            val el = if (ia2 is IntegerIA) {
                elementForIndex(aIndex, ia2)
            } else if (ia2 is IdentIA) {
                elementForIndex(aIndex, ia2)
            } else if (ia2 is ProcIA) {
                elementForIndex(ia2)
            } else null // README shouldn't be calling for other subclasses

            elements[aIndex] = el
            return el
        } else {
            return elements[aIndex]
        }
    }

    fun getElementPromise(aIndex: Int,
                          aOS_elementDoneCallback: DoneCallback<OS_Element>?,
                          aDiagnosticFailCallback: FailCallback<Diagnostic>?) {
        getEntry(aIndex)!!.elementPromise(aOS_elementDoneCallback, aDiagnosticFailCallback)
    }

    fun getEntry(aIndex: Int): BaseTableEntry? {
        val ia2 = getIA(aIndex)
        if (ia2 is IntegerIA) {
            val vte = ia2.entry
            return vte
        } else if (ia2 is IdentIA) {
            val identTableEntry = ia2.entry
            return identTableEntry
        } else if (ia2 is ProcIA) {
            val procTableEntry = ia2.entry
            return procTableEntry
        }
        return null
    }

    fun getIA(index: Int): InstructionArgument {
        return ias[index]
    }

    fun getType(aIndex: Int): GenType? {
        if (types[aIndex] != null) {
            return types[aIndex]
        }

        val ia2 = getIA(aIndex)
        val gt: GenType?
        if (ia2 is IntegerIA) {
            val vte = ia2.entry
            gt = vte.typeTableEntry.genType
            assert(gt != null)
        } else if (ia2 is IdentIA) {
            val identTableEntry = ia2.entry
            if (identTableEntry.type != null) {
                gt = identTableEntry?.type?.genType
                assert(gt != null)
            } else {
                gt = null
            }
        } else if (ia2 is ProcIA) {
            val procTableEntry = ia2.entry
            gt = null // procTableEntry.getResolvedElement(); // .expression?
            //				assert gt != null;
        } else gt = null // README shouldn't be calling for other subclasses

        types[aIndex] = gt
        return gt
    }

    fun injectType(index: Int, aType: GenType?) {
        types[index] = aType
    }

    fun setTarget(aTarget: DeduceElement) {
        // assert elements[0] == null;
        elements[0] = aTarget.element()
    }

    fun size(): Int {
        return ias.size
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

