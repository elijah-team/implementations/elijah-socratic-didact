package tripleo.elijah.stages.gen_fn

import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action
import tripleo.elijah.stages.deduce.post_bytecode.setup_GenType_Action_Arena

class SGTA_SetFunctionInvocation(private val fi: FunctionInvocation) : setup_GenType_Action {
    override fun run(gt: GenType, arena: setup_GenType_Action_Arena) {
        gt.functionInvocation = fi
    }
}
