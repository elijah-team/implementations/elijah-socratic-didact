package tripleo.elijah.nextgen.outputstatement

interface EX_Explanation {
    fun message(): String

    companion object {
        @JvmStatic
//        @Deprecated
        fun withMessage(message: String): EX_Explanation {
            return EX_Explanation_withMessage(message = message)
        }
    }
}

public fun EX_Explanation_withMessage(message: String): EX_Explanation {
    return object : EX_Explanation {
        override fun message(): String {
            return message
        }
    }
}
