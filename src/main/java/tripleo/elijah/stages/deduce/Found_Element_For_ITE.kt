/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */ /*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.AliasStatementImpl
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceClient1
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceTypes2Injector
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.logging.ElLog

/**
 * Created 9/2/21 11:36 PM
 */
internal class Found_Element_For_ITE(private val generatedFunction: BaseEvaFunction, private val ctx: Context, aEnv: DT_Env,
                                     private val dc: DeduceClient1) {
	private val central: DeduceCentral = aEnv.central()
	private val errSink: ErrSink = aEnv.errSink()
	private val LOG: ElLog = aEnv.LOG()
	private val deduceTypes2: DeduceTypes2 = dc._deduceTypes2()

	private fun _inj(): DeduceTypes2Injector {
		return deduceTypes2._inj()
	}

	fun action(ite: IdentTableEntry) {
		action2(ite)

		generatedFunction.getIdent(ite).resolve()
	}

	fun action_AliasStatement(ite: IdentTableEntry, y: AliasStatementImpl) {
		LOG.err("396 AliasStatementImpl")
		val x = dc._resolveAlias(y)
		if (x == null) {
			ite.setStatus(BaseTableEntry.Status.UNKNOWN, null)
			errSink.reportError("399 resolveAlias returned null")
		} else {
			ite.setStatus(BaseTableEntry.Status.KNOWN, _inj().new_GenericElementHolder(x))

			// if (x instanceof AliasStatement) {} else
			// dc.found_element_for_ite(generatedFunction, ite, x, ctx);
		}
	}

	fun action_ClassStatement(ite: IdentTableEntry, classStatement: ClassStatement) {
		val attached = classStatement.oS_Type
		if (ite.type == null) {
			ite.makeType(generatedFunction, TypeTableEntry.Type.TRANSIENT, attached)
		} else ite.type?.attached = attached
	}

	fun action_FunctionDef(ite: IdentTableEntry, functionDef: FunctionDef) {
		val attached: OS_Type = functionDef.oS_Type
		if (ite.type == null) {
			ite.makeType(generatedFunction, TypeTableEntry.Type.TRANSIENT, attached)
		} else ite.type?.attached = attached
	}

	fun action_PropertyStatement(ite: IdentTableEntry, ps: PropertyStatement) {
		val attached: OS_Type
		attached = when (ps.typeName.kindOfType()) {
			TypeName.Type.GENERIC -> _inj().new_OS_UserType(ps.typeName)
			TypeName.Type.NORMAL -> try {
				dc.resolve_type(_inj().new_OS_UserType(ps.typeName), ctx).resolved.classOf
						.oS_Type
			} catch (resolveError: ResolveError) {
				LOG.err("378 resolveError")
				resolveError.printStackTrace()
				return
			}

			else -> throw IllegalStateException("Unexpected value: " + ps.typeName.kindOfType())
		}
		if (ite.type == null) {
			ite.makeType(generatedFunction, TypeTableEntry.Type.TRANSIENT, attached)
		} else ite.type?.attached = attached
		dc.genCIForGenType2(ite.type?.genType!!)
		val yy = 2
	}

	fun action_VariableStatement(ite: IdentTableEntry, vs: VariableStatementImpl) {
		val typeName = vs.typeName()
		if (ite.type == null || ite.type?.attached == null) {
			if (!(typeName.isNull)) {
				if (ite.type == null) ite.makeType(generatedFunction, TypeTableEntry.Type.TRANSIENT, vs.initialValue())
				ite.type?.attached = _inj().new_OS_UserType(typeName)
			} else {
				val parent = vs.parent!!.parent
				if (parent is NamespaceStatement || parent is ClassStatement) {
					val state = if (generatedFunction is EvaFunction) {
						parent !== generatedFunction.getFD().parent
					} else {
						parent !== generatedFunction.fd.parent
					}
					if (state) {
						val invocation = dc.getInvocationFromBacklink(ite.backlink)
						val dew_parent = DeduceElementWrapper(parent)
						val dm = dc.deferred_member(dew_parent, invocation, vs, ite)
						dm.typePromise().done { result: GenType ->
							if (ite.type == null) ite.makeType(generatedFunction, TypeTableEntry.Type.TRANSIENT, vs.initialValue())
							assert(result.resolved != null)
							if (result.ci == null) {
								genCIForGenType(result)
							}
							ite.setGenType(result)
							if (ite.fefi) {
								ite.fefiDone(result)
							}

							val normal_path = generatedFunction
									.getIdentIAPathNormal(_inj().new_IdentIA(ite.index, generatedFunction))
							ite.resolveExpectation?.satisfy(normal_path)
						}
					} else {
						val invocation: IInvocation?
						if (ite.backlink == null) {
							if (parent is ClassStatement) {
								val ci = dc.registerClassInvocation(parent, null)!!
								invocation = ci
							} else {
								invocation = null // TODO shouldn't be null
							}
						} else {
							invocation = dc.getInvocationFromBacklink(ite.backlink)
						}
						val dew_parent = DeduceElementWrapper(parent)
						val dm = dc.deferred_member(dew_parent, invocation, vs, ite)
						dm.typePromise().then { result ->
							if (ite.type == null) ite.makeType(generatedFunction, TypeTableEntry.Type.TRANSIENT, vs.initialValue())
							assert(result.resolved != null)
							ite.setGenType(result)

							//								ite.resolveTypeToClass(result.node); // TODO setting this has no effect on output
							val normal_path = generatedFunction
									.getIdentIAPathNormal(_inj().new_IdentIA(ite.index, generatedFunction))
							ite.resolveExpectation?.satisfy(normal_path)
						}
					}

					var genType: GenType? = null
					when (parent) {
						is NamespaceStatement -> genType = _inj().new_GenTypeImpl(parent as NamespaceStatement?)
						is ClassStatement -> genType = _inj().new_GenTypeImpl(parent as ClassStatement?)
					}

					generatedFunction.addDependentType(genType!!)
				}
				//				LOG.err("394 typename is null " + vs.getName());
			}
		}
	}

	private fun action2(ite: IdentTableEntry) {
		val y = ite.resolvedElement

		if (y is VariableStatementImpl) {
			action_VariableStatement(ite, y)
		} else if (y is ClassStatement) {
			action_ClassStatement(ite, y)
			central.note_Class(y as ClassStatement?, ctx).attach(ite, generatedFunction)
		} else if (y is FunctionDef) {
			action_FunctionDef(ite, y)
		} else if (y is PropertyStatement) {
			action_PropertyStatement(ite, y)
		} else if (y is AliasStatementImpl) {
			action_AliasStatement(ite, y)
		} else {
			// LookupResultList exp = lookupExpression();
			LOG.info("2009 $y")
			return
		}

		val normal_path = generatedFunction
				.getIdentIAPathNormal(deduceTypes2._inj().new_IdentIA(ite.index, generatedFunction))
		when {
			ite.resolveExpectation?.isSatisfied!! -> {}
			else -> ite.resolveExpectation?.satisfy(normal_path)
		}
	}

	/**
	 * Sets the invocation (`genType#ci`) and the node for a GenType
	 *
	 * @param aGenType the GenType to modify.
	 */
	fun genCIForGenType(aGenType: GenType) {
		// assert aGenType.nonGenericTypeName != null ;//&& ((NormalTypeName)
		// aGenType.nonGenericTypeName).getGenericPart().size() > 0;

		dc.genCI(aGenType, aGenType.nonGenericTypeName)
		val invocation = aGenType.ci
		if (invocation is NamespaceInvocation) {
			invocation.resolveDeferred().then { result -> aGenType.node = result }
		} else if (invocation is ClassInvocation) {
			invocation.resolvePromise().then { result -> aGenType.node = result }
		} else throw IllegalStateException("invalid invocation")
	}
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

