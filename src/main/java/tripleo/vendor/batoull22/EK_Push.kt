package tripleo.vendor.batoull22

import java.util.*

/**
 * @author tripleo
 */
class EK_Push(private val predicating: EK_Fact?, private val resultant: EK_Fact?) {
    fun predicating(): EK_Fact? {
        return predicating
    }

    fun resultant(): EK_Fact? {
        return resultant
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as EK_Push
        return this.predicating == that.predicating && (this.resultant == that.resultant)
    }

    override fun hashCode(): Int {
        return Objects.hash(predicating, resultant)
    }

    override fun toString(): String {
        return "EK_Push[" +
                "predicating=" + predicating + ", " +
                "resultant=" + resultant + ']'
    }
}
