package tripleo.elijah.comp

import tripleo.elijah.comp.Finally.Out2
import tripleo.elijah.comp.Finally.Outs
import tripleo.elijah.nextgen.outputtree.EOT_FileNameProvider
import tripleo.elijah.nextgen.outputtree.EOT_Nameable
import tripleo.elijah.nextgen.outputtree.EOT_OutputFile

private val EOT_FileNameProvider.filename: String
    get() {
        return getFilename()
    }

class Finally_ : Finally {
    override fun codeOutputSize(): Int {
        return outputs.size
    }

    override fun getCodeOutputs(): List<String> {
        val l: MutableList<String> = ArrayList()
        for (output in outputs) {
            l.add((output as Output_).fileNameProvider.filename)
        }
        return l
    }

    override fun inputCount(): Int {
        return finallyInputs.size
    }

    override fun outputCount(): Int {
        return outputs.size
    }

    private val outputOffs: MutableSet<Outs> = HashSet()

    private val finallyInputs: MutableList<Finally_Input> = ArrayList()

    //	public void addInput(final CompilerInput aInp, final Out2 ty) {
    //		inputs.add(new Input(aInp, ty));
    //	}

    private val outputs: MutableList<Output> = ArrayList()

    private var turnAllOutputOff = false

    //	public void addInput(final CompFactory.InputRequest aInp, final Out2 ty) {
    //		inputs.add(new Input(aInp, ty));
    //	}

    override fun addCodeOutput(aFileNameProvider: EOT_FileNameProvider, aOff: EOT_OutputFile) {
        outputs.add(Output_(aFileNameProvider, aOff))
    }

    override fun addCodeOutput(outputFile: EOT_OutputFile, aFileNameProvider: EOT_FileNameProvider) {
        outputs.add(Output_(aFileNameProvider, outputFile))
    }

    override fun addInput(aNameable: EOT_Nameable, ty: Out2) {
        finallyInputs.add(FinallyInput_(aNameable, ty))
    }

    override fun containsCodeOutput(s: String): Boolean {
        return outputs.stream().anyMatch { i: Output -> i.name() == s }
    }

    override fun containsInput(aS: String): Boolean {
        return finallyInputs.stream().anyMatch { i: Finally_Input -> i.name() == aS }
    }

    override fun outputOn(aOuts: Outs): Boolean {
        return !turnAllOutputOff && !outputOffs.contains(aOuts)
    }

    override fun turnAllOutputOff() {
        turnAllOutputOff = true
    }

    override fun turnOutputOff(aOut: Outs) {
        outputOffs.add(aOut)
    }

    override fun getCodeInputs(): List<String> {
        return this.finallyInputs.stream().map { it.name() }.toList()
    }

    class Output_(val fileNameProvider: EOT_FileNameProvider, private val off: EOT_OutputFile) : Output {
        override fun name(): String {
            return fileNameProvider.filename
        }
    }

    class FinallyInput_ //			tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("66 Add Input >> " + aNameable.getName());
    (private val nameable: EOT_Nameable, private val ty: Out2) : Finally_Input {
        //		public Input(final CompilerInput aInp, final Out2 aTy) {
        //			nameable = new Finally._CompilerInputNameable(aInp);
        //			ty  = aTy;
        //		}
        //		public Input(final CompFactory.InputRequest aInp, final Out2 aTy) {
        //			nameable = new Finally.InputRequestNameable(aInp);
        //			ty = aTy;
        //		}
        override fun name(): String {
            return nameable.nameableString
        }

        override fun toString(): String {
            return "Input{" + "name=" + nameable.nameableString + ", ty=" + ty + '}'
        }
    } //	private class _CompilerInputNameable implements Nameable {
    //		private final CompilerInput aInp;
    //
    //		public _CompilerInputNameable(CompilerInput aInp) {
    //			this.aInp = aInp;
    //		}
    //
    //		@Override
    //		public String getName() {
    //			return aInp.getInp();
    //		}
    //	}
    //	private class InputRequestNameable implements Nameable {
    //		private final CompFactory.InputRequest aInp;
    //
    //		public InputRequestNameable(CompFactory.InputRequest aInp) {
    //			this.aInp = aInp;
    //		}
    //
    //		@Override
    //		public String getName() {
    //			return aInp.file().toString();
    //		}
    //	}
}
