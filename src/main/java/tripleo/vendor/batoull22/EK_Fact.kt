package tripleo.vendor.batoull22

import org.jetbrains.annotations.Contract

/**
 * @author tripleo
 */
class EK_Fact(private val ch: Char) {
    fun ch(): Char {
        return ch
    }

    override fun hashCode(): Int {
        return  /*Objects.hash*/(ch.code) // ??
    }

    override fun equals(aO: Any?): Boolean {
        if (this === aO) return true
        if (aO == null || javaClass != aO.javaClass) return false
        val ekFact = aO as EK_Fact
        return ch == ekFact.ch
    }

    @Contract(pure = true)
    override fun toString(): String {
        return "<FACT %c>".formatted(ch) // ??!! you don't actually know java, do you?
    }
}
