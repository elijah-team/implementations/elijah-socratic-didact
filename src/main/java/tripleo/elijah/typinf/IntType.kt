package tripleo.elijah.typinf

class IntType : Type {
    override fun equals(obj: Any?): Boolean {
        return obj is IntType || super.equals(obj)
    }

    override fun toString(): String {
        return "Int"
    }
}
