package tripleo.elijah.comp.notation

import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.work.WorkList
import tripleo.elijah.work.WorkList__
import tripleo.elijah.work.WorkManager
import java.util.function.Supplier

@Suppress("unused")
class GM_GenerateModuleResult(val generateResult: GenerateResult,
                              val generateNodesIntoSink: GN_GenerateNodesIntoSink,
                              val generateModuleRequest: GM_GenerateModuleRequest,
                              val figs: Supplier<GenerateResultEnv?>) {
    fun doResult(wm: WorkManager) {
        // TODO find GenerateResultEnv and centralise them
        val wl: WorkList = WorkList__()
        val generateFiles1 = generateModuleRequest.getGenerateFiles(figs)
        val gr = gr()

        generateFiles1.finishUp(gr, wm, wl)

        wm.addJobs(wl)
        wm.drain()

        gr!!.additional(generateResult)
    }

    private fun gr(): GenerateResult? {
        return generateNodesIntoSink._env().gr
    }
}
