package tripleo.elijah.nextgen.model

enum class SM_ClassSubtype {
    ABSTRACT, INTERFACE, NORMAL, SIGNATURE, STRUCT
}
