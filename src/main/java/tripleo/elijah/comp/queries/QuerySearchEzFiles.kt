package tripleo.elijah.comp.queries

import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.Compilation
import tripleo.elijah.comp.graph.i.CK_SourceFile
import tripleo.elijah.comp.i.CompilationClosure
import tripleo.elijah.comp.nextgen.wonka.CK_SourceFileFactory
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.diagnostic.Locatable
import tripleo.elijah.util2.UnintendedUseException
import tripleo.wrap.File
import java.io.FilenameFilter
import java.io.PrintStream
import java.util.regex.Pattern

class QuerySearchEzFiles(private val cc: CompilationClosure) {
    private val c = cc.compilation as Compilation
    private val ez_files_filter: FilenameFilter = EzFilesFilter()

    fun process(directory: File): CompilerInstructions_Result {
        val R: CompilerInstructions_Result = CompilerInstructions_ResultImpl()

        val list = directory.list(ez_files_filter)
        if (list != null) {
            for (file_name in list) {
                val sf: CK_SourceFile<CompilerInstructions> = CK_SourceFileFactory.get(directory, file_name, CK_SourceFileFactory.K.ElaboratedEzFile) as CK_SourceFile<CompilerInstructions>
                sf.associate(cc)
                val cio = sf.process_query()

                // reason obv is it is elaborated in the directory ...
                val reasoning = QSEZ_Reasonings.create(null)
                R.add(cio, reasoning)
            }
        }

        return R
    }

    class Diagnostic_9995(private val file: File) : Diagnostic {
        private val code = 9995

        override fun code(): String? {
            return "" + code
        }

        override fun primary(): Locatable {
            throw UnintendedUseException("no clue now")
        }

        override fun report(stream: PrintStream) {
        }

        override fun secondary(): List<Locatable> {
            return listOf()
        }

        override fun severity(): Diagnostic.Severity {
            return Diagnostic.Severity.ERROR
        }
    }

    class EzFilesFilter : FilenameFilter {
        override fun accept(file: java.io.File, s: String): Boolean {
            val matches2 = Pattern.matches(".+\\.ez$", s)
            return matches2
        }
    }
}
