package tripleo.elijah.stages.gen_c

import org.apache.commons.lang3.tuple.Pair
import org.jdeferred2.impl.DeferredObject
import tripleo.elijah.comp.i.CompProgress
import tripleo.elijah.nextgen.reactive.Reactivable
import tripleo.elijah.nextgen.reactive.ReactiveDimension
import tripleo.elijah.stages.garish.GarishNamespace__addClass_1
import tripleo.elijah.stages.gen_c.GenerateC.GetTypeName
import tripleo.elijah.stages.gen_fn.EvaNamespace
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.util.*
import tripleo.elijah.util2.DebugFlags
import tripleo.elijah.world.i.LivingNamespace

class WhyNotGarish_Namespace(private val en: EvaNamespace, private val generateC: GenerateC) : WhyNotGarish_Item {
    private val fileGenPromise = DeferredObject<GenerateResultEnv, Void, Void>()
    private val gcfn = GCFN()

    init {
        en.reactive().add(gcfn)

        fileGenPromise.then { aFileGen: GenerateResultEnv -> this.onFileGen(aFileGen) }
    }

    private fun onFileGen(aFileGen: GenerateResultEnv) {
        NotImplementedException.raise()

        if (!DebugFlags.MANUAL_DISABLED) {
            gcfn.respondTo(this.generateC)
        }

        val world = generateC._ce().compilation.world()
        val gn = world.getNamespace(en).garish
        val sink = aFileGen.resultSink()

        if (sink != null) {
            GarishNamespace__addClass_1(gn, aFileGen.gr(), generateC).action(sink)
        } else {
            logProgress(9993, "sink failed")
        }
    }

    private fun logProgress(code: Int, message: String) {
        generateC._ce().logProgress(CompProgress.GenerateC, Pair.of(code, message))
    }

    val typeNameString: String
        get() = GetTypeName.Companion.forGenNamespace(en)

    override fun hasFileGen(): Boolean {
        return fileGenPromise.isResolved
    }

    override fun provideFileGen(fg: GenerateResultEnv) {
        fileGenPromise.resolve(fg)
    }

    inner class GCFN : Reactivable {
        override fun respondTo(aDimension: ReactiveDimension?) {
            if (aDimension is GenerateC) {
                fileGenPromise.then { fileGen: GenerateResultEnv ->
                    val livingNamespace: LivingNamespace = aDimension._ce().compilation.world().getNamespace(en)
                    livingNamespace.garish.garish(aDimension, fileGen.gr(), fileGen.resultSink())
                }
            }
        }
    }
}
