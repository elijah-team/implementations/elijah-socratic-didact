/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce

import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.LangGlobals
import tripleo.elijah.stages.deduce.DeduceTypes2.DeduceClient2
import tripleo.elijah.stages.deduce.nextgen.DR_Ident
import tripleo.elijah.stages.deduce.post_bytecode.DG_ClassStatement
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_fn.BaseTableEntry.StatusListener
import tripleo.elijah.stages.instructions.IdentIA
import tripleo.elijah.stages.instructions.InstructionArgument
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.ProcIA
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * Created 9/10/21 3:42 AM
 */
class ProcTableListener(private val pte: ProcTableEntry, private val generatedFunction: BaseEvaFunction,
                        private val dc: DeduceClient2) : StatusListener {
    internal inner class E_Is_FunctionDef(private val pte: ProcTableEntry, private val fd: FunctionDef, private val parent: OS_Element?) {
        var fi: FunctionInvocation? = null
            private set
        var genType: GenType? = null
            private set

        private fun ci_null__fi_not_null(typeName: TypeName?) {
            if (parent is ClassStatement) {
                parentIsClass(parent, typeName)
            } else if (parent is NamespaceStatement) {
                parentIsNamespace(parent)
            }
        }

        private fun ci_null__fi_null(typeName: TypeName?) {
            if (parent is NamespaceStatement) {
                parentIsNamespace(parent)
            } else if (parent is ClassStatement) {
                parentIsClass(parent, typeName)
            } else if (parent is FunctionDef) {
                if (pte.expression_num == null) {
                    // TODO need the instruction to get args from FnCallArgs
                    fi = null
                }
            } else throw IllegalStateException("Unknown parent")
        }

        /**
         * Create genType and set ci; set fi
         *
         * @param typeName an optional typename, used for generics in `genCI`
         * @return a "function object" with genType and hopefully fi set
         */
        /* @ensures genType != null && genType.ci != null; */ /* @ ///// ensures fi != null ; */
        fun invoke(typeName: TypeName?): E_Is_FunctionDef {
            if (pte.classInvocation == null && pte.functionInvocation == null) {
                var ci: ClassInvocation
                ci_null__fi_null(typeName)
                if (fi != null) pte.functionInvocation = fi
            } else if (pte.classInvocation == null && pte.functionInvocation != null) {
                ci_null__fi_not_null(typeName)
            } else {
                // don't create _inj().new_objects when alrady populated
                genType = GenTypeImpl()
                val classInvocation = pte.classInvocation
                genType!!.setResolved(classInvocation?.klass?.oS_Type)
                genType!!.setCi(classInvocation)
                fi = pte.functionInvocation
            }
            return this
        }

        private fun parentIsClass(classStatement: ClassStatement, typeName: TypeName?) {
            var hint: CI_Hint? = null

            val currentFunction = generatedFunction.fd
            val currentClass0 = currentFunction.parent

            if (currentClass0 is ClassStatement) {
                val inh: Map<TypeName, ClassStatement> = currentClass0.context.inheritance()
                if (inh.containsValue(fd.parent)) {
                    // the function referenced by pte.expression is an inherited method
                    // defined in genType.resolved and called in generatedFunction
                    // for whatever reason we don't have a CodePoint (gf, instruction...)

                    hint = InheritedMethodCalledFromInheritee(fd, classStatement, generatedFunction)
                }
            }

            genType = GenTypeImpl(classStatement)
            // ci = _inj().new_ClassInvocation(classStatement, null);
            // ci = phase.registerClassInvocation(ci);
            // genType.ci = ci;
            val ci = dc.genCI(genType!!, typeName)
            pte.classInvocation = ci
            fi = dc.newFunctionInvocation(fd, pte, ci)

            if (hint != null) {
                ci.hint = hint
                fi!!.hint = hint
            }
        }

        private fun parentIsNamespace(namespaceStatement: NamespaceStatement) {
            genType = GenType.of(namespaceStatement) { dc.registerNamespaceInvocation(namespaceStatement) }
            fi = dc.newFunctionInvocation(fd, pte, genType!!.ci)
        }
    }

    //
    private val LOG = dc.log

    private fun __resolved_element_pte_FunctionDef_IdentIA(co: Constructable?, pte: ProcTableEntry,
                                                           depTracker: AbstractDependencyTracker?, fd: FunctionDef, num: IdentIA) {
        val dp = num.entry.buildDeducePath(generatedFunction)

        val genType: GenType
        val fi: FunctionInvocation

        if (dp.size() > 1) {
            val el_self = dp.getElement(dp.size() - 2)

            val parent = el_self
            if (parent is IdentExpression) {
                resolved_element_pte_FunctionDef_IdentExpression(co, pte, depTracker, fd, parent)
            } else if (parent is FormalArgListItem) {
                resolved_element_pte_FunctionDef_FormalArgListItem(co, pte, depTracker, fd, parent)
            } else if (parent is VariableStatement) {
                val p: OS_Element?
                val ia: InstructionArgument?
                if (dp.size() > 2) {
                    p = dp.getElement(dp.size() - 3)
                    ia = dp.getIA(dp.size() - 3)
                } else {
                    p = null
                    ia = null
                }
                resolved_element_pte_FunctionDef_VariableStatement(co, depTracker, pte, fd, p, ia,
                        parent)
            } else {
                val e_Is_FunctionDef = E_Is_FunctionDef(pte, fd, parent).invoke(null)
                fi = e_Is_FunctionDef.fi!!
                if (fi != null) { // TODO
                    genType = e_Is_FunctionDef.genType!!
                    // NOTE read note below
                    genType.resolved = fd.oS_Type
                    genType.functionInvocation = fi // DeduceTypes2.Dependencies#action_type
                    finish(co, depTracker, fi, genType)
                }
            }
        } else {
            val parent = fd.parent
            val e_Is_FunctionDef = E_Is_FunctionDef(pte, fd, parent).invoke(null)
            fi = e_Is_FunctionDef.fi!!
            genType = e_Is_FunctionDef.genType!!
            // NOTE genType.ci will likely come out as a ClassInvocation here
            // This is incorrect when pte.expression points to a Function(Def)
            // It is actually correct, but what I mean is that genType.resolved
            // will come out as a USER_CLASS when it should be FUNCTION
            //
            // So we correct it here
            genType.resolved = fd.oS_Type
            genType.functionInvocation = fi // DeduceTypes2.Dependencies#action_type
            finish(co, depTracker, fi, genType)
        }
    }

    fun finish(co: Constructable?, depTracker: AbstractDependencyTracker?,
               aFi: FunctionInvocation, aGenType: GenType?) {
        if (co != null && aGenType != null) co.setGenType(aGenType)

        if (depTracker != null) {
            if (aGenType == null) SimplePrintLoggerToRemoveSoon.println_err_2("247 genType is null")

            if ( /* aGenType == null && */aFi.function is ConstructorDef) {
                val c = aFi.classInvocation!!.klass
                val genType2: GenType = GenTypeImpl(c)
                depTracker.addDependentType(genType2)
                // TODO why not add fi?
            } else {
                depTracker.addDependentFunction(aFi)
                if (aGenType != null) depTracker.addDependentType(aGenType)
            }
        }
    }

    override fun onChange(eh: IElementHolder, newStatus: BaseTableEntry.Status) {
        var co: Constructable? = null
        if (eh is ConstructableElementHolder) {
            co = eh.constructable
        }
        if (newStatus != BaseTableEntry.Status.UNKNOWN) {
            // means eh is null
            val depTracker: AbstractDependencyTracker? = if (co is IdentIA) {
                co.gf
            } else if (co is IntegerIA) {
                co.gf
            } else null

            set_resolved_element_pte(co, eh.element, pte, depTracker)

            val expressionNum = pte.expression_num

            if (expressionNum is IdentIA) {
                val entry: IdentTableEntry = expressionNum.entry
                val ident = generatedFunction.getIdent(entry)
                ident.resolve(eh, pte)
            } else {
                SimplePrintLoggerToRemoveSoon.println_err_4("*************************** i still refuse")
            }
        }
    }

    private fun resolved_element_pte_ClassStatement(co: Constructable?, e: ClassStatement,
                                                    pte: ProcTableEntry) {
        val fi: FunctionInvocation
        var ci: ClassInvocation?

        val dcs = dc.deduceTypes2().DG_ClassStatement(e)

        ci = dcs.classInvocation()
        ci = dc.registerClassInvocation(ci)
        fi = dc.newFunctionInvocation(LangGlobals.defaultVirtualCtor, pte, ci!!) // TODO might not be virtual ctor, so
        // check
        pte.functionInvocation = fi

        val entry = (pte.expression_num as IdentIA).entry
        generatedFunction.getIdent(entry).resolve(dcs)

        dcs.attach(fi, pte)

        if (co != null) {
            co.setConstructable(pte)
            ci.resolvePromise().done { result: EvaClass ->
                resolved_element_pte_ClassStatement_EvaClass(result, e, co, dcs)
            }
        }
    }

    private fun resolved_element_pte_FunctionDef(co: Constructable?, pte: ProcTableEntry,
                                                 depTracker: AbstractDependencyTracker?, fd: FunctionDef) {
        if (pte.expression_num != null) {
            if (pte.expression_num is IdentIA) {
                val num = pte.expression_num  as IdentIA
                __resolved_element_pte_FunctionDef_IdentIA(co, pte, depTracker, fd, num)

                val dr_ident: DR_Ident = generatedFunction.getIdent(num.entry)
                dr_ident.resolve()
            } else if (pte.expression_num is IntegerIA) {
                val integerIA = pte.expression_num
                val variableTableEntry: VariableTableEntry = integerIA.entry

                VTE_TypePromises.resolved_element_pte(co, pte, depTracker, fd, variableTableEntry, this)

                val y = 2
            }
        } else {
            val parent = pte.resolvedElement!! // for dunder methods

            resolved_element_pte_FunctionDef_dunder(co, depTracker, pte, fd, parent)
        }
    }

    private fun resolved_element_pte_FunctionDef_dunder(co: Constructable?, depTracker: AbstractDependencyTracker?,
                                                        pte: ProcTableEntry, fd: FunctionDef, parent: OS_Element?) {
        var parent = parent
        val fi: FunctionInvocation
        val genType: GenType
        if (parent is IdentExpression) {
            val vte_ia = generatedFunction.vte_lookup(parent.text)!!
            val variableTableEntry = (vte_ia as IntegerIA).entry
            VTE_TypePromises.resolved_element_pte(co, pte, depTracker, fd, variableTableEntry, this)
        } else {
            var typeName: TypeName? = null

            if (fd === parent) {
                parent = fd.parent
                val xx = pte.args?.get(0)
                val x = xx!!
                // TODO highly specialized condition...
                if (x.attached == null && x.tableEntry == null) {
                    val text = (x.__debug_expression as IdentExpression).text
                    val vte_ia = generatedFunction.vte_lookup(text)
                    if (vte_ia != null) {
                        val gt = (vte_ia as IntegerIA).entry.typeTableEntry.genType
                        typeName = if (gt.nonGenericTypeName != null) gt.nonGenericTypeName
                        else gt.typeName.typeName
                    } else {
                        if (parent is ClassStatement) {
                            // TODO might be wrong in the case of generics. check.
                            typeName = null // _inj().new_OS_Type((ClassStatement) parent);
                            SimplePrintLoggerToRemoveSoon.println_err_2("NOTE ineresting in genericA/__preinc__")
                        }
                    }
                }
            }

            val e_Is_FunctionDef = E_Is_FunctionDef(pte, fd, parent).invoke(typeName)
            fi = e_Is_FunctionDef.fi!!
            genType = e_Is_FunctionDef.genType!!
            finish(co, depTracker, fi, genType)
        }
    }

    private fun resolved_element_pte_FunctionDef_FormalArgListItem(co: Constructable?, pte: ProcTableEntry,
                                                                   depTracker: AbstractDependencyTracker?, fd: FunctionDef, parent: FormalArgListItem) {
        val fali = parent
        val vte_ia = generatedFunction.vte_lookup(fali.name())!!
        val variableTableEntry = (vte_ia as IntegerIA).entry
        VTE_TypePromises.resolved_element_pte(co, pte, depTracker, fd, variableTableEntry, this)
    }

    private fun resolved_element_pte_FunctionDef_IdentExpression(co: Constructable?, pte: ProcTableEntry,
                                                                 depTracker: AbstractDependencyTracker?, fd: FunctionDef, parent: IdentExpression) {
        val vte_ia = generatedFunction.vte_lookup(parent.text)!!
        val variableTableEntry = (vte_ia as IntegerIA).entry
        VTE_TypePromises.resolved_element_pte(co, pte, depTracker, fd, variableTableEntry, this)
    }

    private fun resolved_element_pte_FunctionDef_VariableStatement(co: Constructable?,
                                                                   depTracker: AbstractDependencyTracker?, pte: ProcTableEntry, fd: FunctionDef,
                                                                   parent: OS_Element?, ia: InstructionArgument?,
                                                                   variableStatement: VariableStatement) {
        if (ia != null) {
            if (ia is IdentIA) {
                val identTableEntry = ia.entry
                val y = 2
            } else if (ia is ProcIA) {
                val procTableEntry: ProcTableEntry = ia.entry

                val ci = procTableEntry.functionInvocation?.classInvocation
                if (ci != null) {
                    VTE_TypePromises.resolved_element_pte_VariableStatement(co, depTracker, fd, variableStatement,
                            procTableEntry, ci, this)
                } else {
                    assert(false)
                }
            } else {
                val y = 2
            }
            return
        }
        // TODO lookupVariableStatement?
        // we really want DeduceVariableStatement < DeduceElement (with type/promise)
        val vte_ia = generatedFunction.vte_lookup(variableStatement.name)!!
        val variableTableEntry = (vte_ia as IntegerIA).entry
        VTE_TypePromises.resolved_element_pte_VariableStatement2(co, depTracker, pte, fd, variableTableEntry, this)
    }

    fun set_resolved_element_pte(co: Constructable?, e: OS_Element,
                                 pte: ProcTableEntry, depTracker: AbstractDependencyTracker?) {
        if (e is ClassStatement) {
            resolved_element_pte_ClassStatement(co, e, pte)
        } else if (e is FunctionDef) {
            resolved_element_pte_FunctionDef(co, pte, depTracker, e)
        } else {
            LOG.err("845 Unknown element for ProcTableEntry $e")
        }
    }

    companion object {
        private fun resolved_element_pte_ClassStatement_EvaClass(result: EvaClass,
                                                                 e: ClassStatement, co: Constructable, dcs: DG_ClassStatement) {
            // tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("828282 "+((ClassStatement) e).name());
            if ((e.name()) == "Foo") {
                SimplePrintLoggerToRemoveSoon.println_out_4("828282 Foo found")
            }

            co.resolveTypeToClass(result)

            dcs.attachClass(result) // T168-089
        }
    }
} //
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//

