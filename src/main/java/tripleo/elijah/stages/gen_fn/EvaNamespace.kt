/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import tripleo.elijah.g.GEvaNamespace
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.*
import tripleo.elijah.nextgen.reactive.DefaultReactive
import tripleo.elijah.nextgen.reactive.Reactive
import tripleo.elijah.stages.gen_generic.CodeGenerator
import tripleo.elijah.stages.gen_generic.GenerateResultEnv
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.util.Helpers0
import tripleo.elijah.util.NotImplementedException
import tripleo.elijah.util.ProgramIsLikelyWrong
import tripleo.elijah.util2.UnintendedUseException
import tripleo.elijah.world.impl.DefaultLivingNamespace
import java.util.function.Consumer

/**
 * Created 12/22/20 5:39 PM
 */
class EvaNamespace(
		@JvmField
		//@field:Getter
		val namespaceStatement: NamespaceStatement,
		private val module: OS_Module,
) : EvaContainerNC(), GEvaNamespace { //
	internal inner class _Reactive_EvaNamespace : DefaultReactive() {
		override fun <T> addListener(t: Consumer<T>?) {
			throw UnintendedUseException()
		}
	}

	var living: DefaultLivingNamespace? = null

	private val reactiveEvaNamespace = _Reactive_EvaNamespace()

	fun addAccessNotation(an: AccessNotationImpl?) {
		throw NotImplementedException()
	}

	fun createCtor0() {
		// TODO implement me
		val fd: FunctionDef = FunctionDefImpl(namespaceStatement, namespaceStatement.context)
		fd.setName(Helpers0.string_to_ident("<ctor$0>"))
		val scope3 = Scope3Impl(fd)
		fd.scope(scope3)
		for (varTableEntry in varTable) {
			if (varTableEntry.initialValue !== LangGlobals.UNASSIGNED) {
				val left: IExpression = varTableEntry.nameToken
				val right = varTableEntry.initialValue

				val e: IExpression = ExpressionBuilder.build(left, ExpressionKind.ASSIGNMENT, right)
				scope3.add(StatementWrapperImpl(e, fd.context, fd))
			} else {
				if (getPragma("auto_construct")) {
					scope3.add(ConstructStatementImpl(fd, fd.context, varTableEntry.nameToken, null, null))
				}
			}
		}
	}

	var code1234567: Int
		get() = living!!.code
		set(value) {
			throw ProgramIsLikelyWrong()
		}
	override var code: Int
		get() = code1234567
		set(value) {
			code1234567 = value
		}

	override fun generateCode(aFileGen: GenerateResultEnv?, aGgc: CodeGenerator) {
		throw NotImplementedException()
	}

	override fun getElement(): OS_Element {
		return namespaceStatement
	}

	val name: String
		get() = namespaceStatement.name

	private fun getPragma(auto_construct: String): Boolean { // TODO this should be part of ContextImpl
		return false
	}

//	override fun getPGn(): GNCoded { // Eventual vs whatever kotlin does
	override fun getGn(): GNCoded {
		val _c = this
		return object : GNCoded {
			override fun getCode1234567(): Int = code

			override fun getRole(): GNCoded.Role = GNCoded.Role.NAMESPACE

			override fun register(aCr: ICodeRegistrar) = aCr.registerNamespace(_c)

			override fun setCode(aCode: Int) {
				living!!.code = aCode
			}
		}
	}

	//	@Override
	//	public @NotNull Maybe<VarTableEntry> getVariable(String aVarName) {
	//		for (VarTableEntry varTableEntry : varTable) {
	//			if (varTableEntry.nameToken.getText().equals(aVarName))
	//				return new Maybe<>(varTableEntry, null);
	//		}
	//		return new Maybe<>(null, _def_VarNotFound);
	//	}
	override fun identityString(): String = namespaceStatement.toString()

	override fun module(): OS_Module = module

	fun reactive(): Reactive = reactiveEvaNamespace
}
