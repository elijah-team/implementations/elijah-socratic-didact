package tripleo.elijah.comp

import tripleo.elijah.ci.CompilerInstructions
import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.comp.impl.LCM_Event_RootCI
import tripleo.elijah.comp.internal.USE_Reasonings.Companion.instruction_doer_addon
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.util.CompletableProcess
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

class InstructionDoer(private val compilation1: Compilation) : CompletableProcess<CompilerInstructions> {
    var root: CompilerInstructions? = null

    override fun add(item: CompilerInstructions) {
        val __cr = compilation1.compilationEnclosure.compilationRunner
        if (root == null) {
            root = item
            try {
                if (false) {
                    if (false) {
                        val rootCI = compilation1.rootCI

                        val compilationEnclosure = compilation1.compilationEnclosure
                        compilationEnclosure.pipelineAccessPromise.then { apa: IPipelineAccess? -> compilation1.runner.start(rootCI, apa!!) }

                    } else {
                        val lcm = compilation1.lcmAccess.model
                        val ler = LCM_Event_RootCI.INSTANCE // why is this (Event) a Singleton??
                        lcm.asv(root, ler)
                    }
                } else {
                    compilation1.rootCI = root
                    compilation1.signals()
                            .signalRunStepLoop(root)
                }
            } catch (aE: Exception) {
                throw RuntimeException(aE)
            }
        } else {
            SimplePrintLoggerToRemoveSoon.println_err_4("second: " + item.filename)

            compilation1.use(item, instruction_doer_addon(item))
        }
    }

    override fun complete() {
        SimplePrintLoggerToRemoveSoon.println_err_4("InstructionDoer::complete")
    }

    override fun error(d: Diagnostic) {
        SimplePrintLoggerToRemoveSoon.println_err_4("InstructionDoer::error")
    }

    override fun preComplete() {
        SimplePrintLoggerToRemoveSoon.println_err_4("InstructionDoer::preComplete")
    }

    override fun start() {
        SimplePrintLoggerToRemoveSoon.println_err_4("InstructionDoer::start")
    }
}
