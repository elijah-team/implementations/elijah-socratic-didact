package tripleo.elijah.nextgen.outputtree

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import tripleo.elijah.nextgen.outputstatement.EG_Naming
import tripleo.elijah.nextgen.outputstatement.EG_SequenceStatement
import tripleo.elijah.nextgen.outputstatement.mk_EG_SequenceStatement
import tripleo.elijah.stages.write_stage.pipeline_impl.EOT_FileNameProvider__ofString

/**
 * @author tripleo
 */
class EOT_OutputTreeImpl : EOT_OutputTree {
    override val list: MutableList<EOT_OutputFile> = ArrayList()

    override fun add(aOff: EOT_OutputFile) {
        // 05/18 System.err.printf("[add] %s %s%n", aOff.getFilename(),
        // aOff.getStatementSequence().getText());

        list.add(aOff)
    }

    override fun addAll(aLeof: List<EOT_OutputFile>) {
        list.addAll(aLeof)
    }

//    /*override*/ fun getList(): List<EOT_OutputFile> {
//        return list
//    }

    override fun recompute() {
        // TODO big wtf
        val mmfn: Multimap<String?, EOT_OutputFile> = ArrayListMultimap.create()
        for (outputFile in list) {
            mmfn.put(outputFile.filename, outputFile)
        }

        for ((filename, tt) in mmfn.asMap()) {
            if (tt.size > 1) {
                list.removeAll(tt)

                val model = tt.iterator().next()

                val type = model.type
                val inputs = model.inputs // FIXME can possibly contain others

                val list2 = _EOT_OutputTree__Utils._extractStatementSequenceFromAllOutputFiles(tt)

                val seq = mk_EG_SequenceStatement(EG_Naming("redone"), list2)
                val ofn = EOT_OutputFileImpl(inputs, EOT_FileNameProvider__ofString( filename!!), type, seq)

                list.add(ofn)
            }
        }
    }

    override fun set(aLeof: List<EOT_OutputFile>) {
        list.addAll(aLeof)
    }
}
