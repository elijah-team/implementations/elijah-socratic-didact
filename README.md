Elijah socratic-didact
=======================

Elijah is:

- ... a high-level language built to explore code generation and other interesting techniques.
- ... a historical curiosity.
- ... meant to be easy to use standalone or with existing projects.
- ... free software (LGPL) intended for use on all systems, aka Linux.
- ... philosophically opposed to semicolons.
- ... obsessed with curly braces and brackets.

`socratic-didact` is:

- ... a hard fork of `elevated-potential` implemented in Kotlin
- ... probably going to use mill, but for now whatever's there (tldr maven)

Instructions
-------------

[https://gitlab.com/elijah-team/implementations/socratic-didact](https://gitlab.com/elijah-team/implementations/socratic-didact)

```shell
git clone https://gitlab.com/elijah-team/implementations/socratic-didact -b get-to-it
cd socratic-didact
mkdir COMP
mvnd clean test
# or 
nix-shell -p maven jdk17 --pure --command "mvn clean test"
```


Goals
------

- [G1][1] Make progress towards results (cf `meson-demo`)
- [G2][2] Make it "fun" to look at (`datalog-ts`, Glamorous Toolkit)
- [G5][5]Stop concentrating on "architecture"...
- ... while simultaneously improving architecture
- [G3][3] Procrastinate about clojure
- [G4][4] Procrastinate about `dlog`


Revised Goals
--------------

- Stop wasting our time with object hierarchies
- Waste everybody else's time with web services


Lineage
--------

- [evelated-potential](https://github.com/elijah-team/elevated-potential/tree/giveup)


[1]: https://gitlab.com/elijah-team/documentation/petal-to-the-medal/ginitiatives/G1.md
[2]: https://gitlab.com/elijah-team/documentation/petal-to-the-medal/ginitiatives/G2.md
[3]: https://gitlab.com/elijah-team/documentation/petal-to-the-medal/ginitiatives/G3.md
[4]: https://gitlab.com/elijah-team/documentation/petal-to-the-medal/ginitiatives/G4.md
[5]: https://gitlab.com/elijah-team/documentation/petal-to-the-medal/ginitiatives/G5.md
