package tripleo.elijah.nextgen.outputstatement

import tripleo.small.ES_Item
import tripleo.small.ES_String
import tripleo.small.ES_Symbol

/**
 * @author Tripleo Nova
 */
data class EG_SyntheticStatement(
    private val naming: EG_Naming,
    private val rule: EX_Rule,
    private val s: ES_Item,
    override var text: String
) : EG_Statement {

    constructor(naming: EG_Naming, s: String, exRule: EX_Rule) : this(naming, exRule, object : ES_Item {}, s)
    constructor(naming: EG_Naming, esSymbol: ES_Symbol, exRule: EX_Rule) : this(naming, exRule, esSymbol, esSymbol.text)

    init {
        doNaming(naming, s)
    }

    // FIXME 11/29 this def. should not be here
    internal fun doNaming(aNaming: EG_Naming?, aS: ES_Item) {
        if (aNaming == null) return

        val ss = aNaming.s
        val s1 = aNaming.s1

        if (ss == "include") {
            if (s1 == "local") {
                val text1 = (aS as ES_String).text
                text = String.format("#include \"%s\"", text1)
            } else if ((s1 == "system") && (aS as ES_Symbol).text == "Prelude") {
                val text1 = aS.text
                text = String.format("#include \"%s.h\"", text1)
            }
        }
    }

    override val explanation: EX_Explanation
        get() = EX_Explanation_withMessage("EG_SyntheticStatement")

//    fun getText(): String {
//        return text!!
//    }
}

public fun mk_EG_SyntheticStatement(aNaming: EG_Naming, aSymbol: ES_Symbol, aRule: EX_Rule): EG_SyntheticStatement {
    val result = EG_SyntheticStatement(aNaming, aRule, aSymbol, text = aSymbol.text)
    result.doNaming(aNaming, aSymbol)
    return result
}
public fun mk_EG_SyntheticStatement(aNaming: EG_Naming, aS: String, aRule: EX_Rule): EG_SyntheticStatement {
    val s = ES_String(aS)
    val result = EG_SyntheticStatement(aNaming, aRule, s, text = aS)
    result.doNaming(aNaming, s)
    return result
}
