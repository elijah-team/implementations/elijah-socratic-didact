package tripleo.elijah.util2;

public interface _Extensionable {

	Object getExt(Class<?> aClass);

	void putExt(Class<?> aClass, Object o);

}
