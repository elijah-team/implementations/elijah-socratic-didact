package tripleo.elijah.comp.notation

import tripleo.elijah.comp.PipelineLogic
import tripleo.elijah.stages.deduce.DeducePhase.GeneratedClasses
import tripleo.elijah.stages.gen_fn.*
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.world.i.WorldModule

internal class ResolvedNodes(private val cr: ICodeRegistrar) {
    val resolved_nodes: MutableList<EvaNode> = ArrayList()

    fun init(c: GeneratedClasses) {
        // 09/26 tripleo.elijah.util.SimplePrintLoggerToRemoveSoon.println_err_4("2222 " + c);

        for (evaNode in c) {
            check(evaNode is GNCoded) { "node must be coded" }

            when (evaNode.role) {
                GNCoded.Role.FUNCTION -> {
                    cr.registerFunction1(evaNode as BaseEvaFunction)
                }

                GNCoded.Role.CLASS -> {
                    val evaClass = evaNode as EvaClass

                    // assert (evaClass.getCode() != 0);
                    if (evaClass.code == 0) {
                        cr.registerClass1(evaClass)
                    }

                    //					if (generatedClass.getCode() == 0)
//						generatedClass.setCode(mod.getCompilation().nextClassCode());
                    for (evaClass2 in evaClass.classMap.values) {
                        if (evaClass2.code == 0) {
                            // evaClass2.setCode(mod.getCompilation().nextClassCode());
                            cr.registerClass1(evaClass2)
                        }
                    }
                    for (generatedFunction in evaClass.functionMap.values) {
                        for (identTableEntry in generatedFunction.idte_list) {
                            if (identTableEntry.isResolved) {
                                val node = identTableEntry.resolvedType()
                                resolved_nodes.add(node!!)
                            }
                        }
                    }
                }

                GNCoded.Role.NAMESPACE -> {
                    val evaNamespace = evaNode as EvaNamespace
                    if (evaNode.getCode1234567() == 0) {
                        // coded.setCode(mod.getCompilation().nextClassCode());
                        cr.registerNamespace(evaNamespace)
                    }
                    for (evaClass3 in evaNamespace.classMap.values) {
                        if (evaClass3.code == 0) {
                            // evaClass.setCode(mod.getCompilation().nextClassCode());
                            cr.registerClass1(evaClass3)
                        }
                    }
                    for (generatedFunction in evaNamespace.functionMap.values) {
                        for (identTableEntry in generatedFunction.idte_list) {
                            if (identTableEntry.isResolved) {
                                val node = identTableEntry.resolvedType()
                                resolved_nodes.add(node!!)
                            }
                        }
                    }
                }

                else -> throw IllegalStateException("Unexpected value: " + evaNode.role)
            }
        }
    }

    fun part2() {
        resolved_nodes.stream()
                .filter { evaNode: EvaNode? -> evaNode is GNCoded }
                .map { evaNode: EvaNode -> evaNode as GNCoded }
                .filter { coded: GNCoded -> coded.code1234567 == 0 }.forEach { coded: GNCoded ->
                    SimplePrintLoggerToRemoveSoon.println_err_4("-*-*- __processResolvedNodes [NOT CODED] $coded")
                    coded.register(cr)
                }
    }

    fun part3(pipelineLogic: PipelineLogic,
              mod: WorldModule?,
              lgc: GeneratedClasses?) {
        pipelineLogic.dp.deduceModule(mod!!, lgc!!, pipelineLogic.verbosity)
    }
}
