package tripleo.elijah.stages.garish

import tripleo.elijah.comp.i.extra.IPipelineAccess
import tripleo.elijah.nextgen.output.NG_OutputClass
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_generic.GRS_Addable
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink

class GarishClass__addClass_1 //generateResult = aGenerateResult;
(private val garishClass: GarishClass, aGenerateResult: GenerateResult?, //private final GenerateResult generateResult;
 private val generateC: GenerateC) : GRS_Addable {
    override fun action(aResultSink: GenerateResultSink) {
        val o = NG_OutputClass()
        o.setClass(garishClass, generateC)
        (aResultSink.pa() as IPipelineAccess).addOutput(o)
    }
}
