package tripleo.elijah.stages.deduce.fluffy.impl

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import tripleo.elijah.comp.internal.CompilationImpl
import tripleo.elijah.entrypoints.MainClassEntryPoint
import tripleo.elijah.lang.i.*
import tripleo.elijah.lang.impl.OS_ModuleImpl.Complaint
import tripleo.elijah.stages.deduce.fluffy.i.FluffyComp
import tripleo.elijah.stages.deduce.fluffy.i.FluffyModule
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.util2.Eventual
import java.util.*
import java.util.stream.Collectors

class FluffyCompImpl(private val _comp: CompilationImpl) : FluffyComp {
    private val fluffyModuleMap: MutableMap<OS_Module, FluffyModule> = HashMap()
    var __inj: FluffyCompImplInjector = FluffyCompImplInjector()
    private val _eventuals: MutableList<Eventual<*>> = ArrayList()

    override fun find_multiple_items(aModule: OS_Module, aComplaint: Complaint) {
        val items_map: Multimap<String, ModuleItem> = ArrayListMultimap.create(aModule.items.size, 1)

        aModule.items.stream()
                .filter { obj: ModuleItem? -> Objects.nonNull(obj) }
                .filter { x: ModuleItem? -> x !is ImportStatement }
                .forEach { item: ModuleItem ->
                    // README likely for member functions.
                    // README Also note elijah has single namespace
                    items_map.put(item.name(), item)
                }

        for (key in items_map.keys()) {
            var warn = false

            val moduleItems = items_map[key]
            if (moduleItems.size == 1) continue

            val t: Collection<ElObjectType> = moduleItems.stream()
                    .map { input: ModuleItem? -> DecideElObjectType.getElObjectType(input!!) }
                    .collect(Collectors.toList())

            val st: Set<ElObjectType> = HashSet(t)
            if (st.size > 1) warn = true
            if (moduleItems.size > 1) {
                if (moduleItems.iterator().next() !is NamespaceStatement || st.size != 1) {
                    warn = true
                }
            }

            //
            //
            //
            if (warn) {
                aComplaint.reportWarning(aModule, key)
            }
        }
    }

    override fun module(aModule: OS_Module): FluffyModule {
        if (fluffyModuleMap.containsKey(aModule)) {
            return fluffyModuleMap[aModule]!!
        }

        val fluffyModule = _inj().new_FluffyModuleImpl(aModule, _comp)
        fluffyModuleMap[aModule] = fluffyModule
        return fluffyModule
    }

    private fun _inj(): FluffyCompImplInjector {
        return __inj
    }

    class FluffyCompImplInjector {
        fun new_FluffyModuleImpl(aModule: OS_Module, aComp: CompilationImpl?): FluffyModuleImpl {
            return FluffyModuleImpl(aModule, aComp)
        }
    }

    override fun checkFinishEventuals() {
        val y = 0
        for (eventual in _eventuals) {
            if (eventual.isResolved) {
                SimplePrintLoggerToRemoveSoon.println_err_4("[FluffyCompImpl::checkEventual] ok for " + eventual.description())
            } else {
                SimplePrintLoggerToRemoveSoon.println_err_4("[FluffyCompImpl::checkEventual] failed for " + eventual.description())
            }
        }
    }

    override fun <P> register(e: Eventual<P>) {
        _eventuals.add(e)
    }

    companion object {
        fun isMainClassEntryPoint(input: OS_Element2): Boolean {
            // TODO 08/27 Use understanding/~ processor for this
            val fd = input as FunctionDef
            return MainClassEntryPoint.is_main_function_with_no_args(fd)
        }
    }
}
