package tripleo.elijah.nextgen.output

import tripleo.elijah.lang.i.OS_Module

class NG_OutDep(var module: OS_Module) {
    var filename: String = module.fileName

    fun module(): OS_Module {
        return module
    }
}
