/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import lombok.Getter
import tripleo.elijah.lang.i.NamespaceStatement
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.stages.deduce.FunctionInvocation
import tripleo.elijah.stages.deduce.FunctionMapDeferred
import tripleo.elijah.stages.deduce.nextgen.DeduceCreationContext
import tripleo.elijah.stages.gen_generic.ICodeRegistrar
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon
import tripleo.elijah.work.WorkJob
import tripleo.elijah.work.WorkManager

/**
 * Created 5/16/21 12:46 AM
 */
class WlGenerateFunction(private val generateFunctions: GenerateFunctions, private val functionInvocation: FunctionInvocation,
                         private val cr: ICodeRegistrar) : WorkJob { //
    private val functionDef = functionInvocation.function
    private var _isDone = false// 24/01/04 back and forth

    // FIXME 24/01/04 use eventual
    @Getter
    var result: EvaFunction? = null
        private set

    constructor(aModule: OS_Module?,
                aFunctionInvocation: FunctionInvocation,
                aCl: DeduceCreationContext) : this(aCl.generatePhase.getGenerateFunctions(aModule!!),
            aFunctionInvocation,
            aCl.deducePhase.codeRegistrar)

    private fun __registerClass(result: EvaClass, gf: EvaFunction) {
        if (result.getFunction(functionDef!!) == null) {
            cr.registerClass1(result)
            result.addFunction(gf)
        }
        result.functionMapDeferreds.put(functionDef, FunctionMapDeferred { val y = 2 })
        gf.setClass(result)
    }

    private fun __registerNamespace(result: EvaNamespace, gf: EvaFunction) {
        if (result.getFunction(functionDef!!) == null) {
            cr.registerNamespace(result)
            // cr.registerFunction1(gf);
            result.addFunction(gf)
        }
        result.functionMapDeferreds.put(functionDef, FunctionMapDeferred { val y = 2 })
        gf.setClass(result)
    }

    override fun isDone(): Boolean {
        return _isDone
    }

    override fun run(aWorkManager: WorkManager?) {
        if (_isDone) return

        if (functionInvocation.generated == null) {
            val parent = functionDef!!.parent
            val gf = generateFunctions.generateFunction(functionDef, parent, functionInvocation)

            run {
                var i = 0
                for (tte in functionInvocation.args) {
                    i = i + 1
                    if (tte.attached == null) {
                        SimplePrintLoggerToRemoveSoon
                                .println_err_2(String.format("4949 null tte #%d %s in %s", i, tte, gf))
                    }
                }
            }

            //			lgf.add(gf);
            if (parent is NamespaceStatement) {
                val nsi = functionInvocation.namespaceInvocation!!
                nsi.resolvePromise().done { result: EvaNamespace ->
                    __registerNamespace(result, gf)
                }
            } else {
                val ci = functionInvocation.classInvocation
                ci!!.resolvePromise().done { result: EvaClass ->
                    __registerClass(result, gf)
                }
            }
            result = gf
            functionInvocation.generated = result
            functionInvocation.generateDeferred().resolve(result!!)
        } else {
            result = functionInvocation.generated as EvaFunction?
        }
        _isDone = true
    }
}
//
//

