/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn

import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_ConstantTableEntry

/**
 * Created 9/10/20 4:47 PM
 */
class ConstantTableEntry(val index: Int, @JvmField val name: String?, @JvmField val initialValue: IExpression,
                         val typeTableEntry: TypeTableEntry) {
    val type: TypeTableEntry
        get() = typeTableEntry

    private var _de3: DeduceElement3_ConstantTableEntry? = null

    val deduceElement3: DeduceElement3_ConstantTableEntry
        get() {
            if (_de3 == null) {
                _de3 = DeduceElement3_ConstantTableEntry(this)
                //			_de3.
            }
            return _de3!!
        }

    //    public void setName(String name) {
    //        this.name = name;
    //    }
    override fun toString(): String {
        return ("ConstantTableEntry{" + "index=" + index + ", name='" + name + '\'' + ", initialValue=" + initialValue
                + ", type=" + typeTableEntry + '}')
    }
}
