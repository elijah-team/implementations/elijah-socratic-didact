package tripleo.elijah.world.impl

import tripleo.elijah.comp.internal_move_soon.CompilationEnclosure
import tripleo.elijah.comp.nextgen.inputtree.EIT_ModuleInput
import tripleo.elijah.comp.notation.GN_PL_Run2.GenerateFunctionsRequest
import tripleo.elijah.lang.i.OS_Module
import tripleo.elijah.nextgen.inputtree.EIT_InputType
import tripleo.elijah.nextgen.inputtree.EIT_ModuleInputImpl
import tripleo.elijah.stages.inter.ModuleThing
import tripleo.elijah.util2.Eventual
import tripleo.elijah.world.i.WorldModule

class DefaultWorldModule(private val mod: OS_Module, private val ce: CompilationEnclosure) : WorldModule {
    private val erq = Eventual<GenerateFunctionsRequest>()
    private var thing: ModuleThing? = null
    private var rq: GenerateFunctionsRequest? = null

    init {
        val mt = ce.addModuleThing(mod)
        setThing(mt)
    }

    override fun getEITInput(): EIT_ModuleInput {
        return EIT_ModuleInputImpl(mod, ce.compilation, EIT_InputType.ELIJAH_SOURCE)
    }

    override fun getErq(): Eventual<GenerateFunctionsRequest> {
        return erq
    }

    override fun module(): OS_Module {
        return mod
    }

    override fun rq(): GenerateFunctionsRequest {
        return rq!!
        // throw new NotImplementedException("Unexpected");
    }

    fun setRq(aRq: GenerateFunctionsRequest?) {
        rq = aRq

        // throw new NotImplementedException("Unexpected");
        erq.resolve(rq!!)
    }

    fun setThing(aThing: ModuleThing?) {
        thing = aThing
    }

    fun thing(): ModuleThing? {
        return thing
    }

    override fun toString(): String {
        return "DefaultWorldModule{%s}".formatted(mod.fileName)
    }
}
