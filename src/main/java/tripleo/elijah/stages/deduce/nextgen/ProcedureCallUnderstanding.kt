package tripleo.elijah.stages.deduce.nextgen

import tripleo.elijah.stages.deduce.nextgen.DR_Ident.Understanding
import tripleo.elijah.stages.gen_fn.ProcTableEntry

class ProcedureCallUnderstanding(private val procTableEntry: ProcTableEntry) : Understanding {
    override fun asString(): String {
        return "ProcedureCallUnderstanding: " + procTableEntry.__debug_expression
    }
}
