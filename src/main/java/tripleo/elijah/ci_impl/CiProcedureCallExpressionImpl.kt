package tripleo.elijah.ci_impl

import tripleo.elijah.ci.CiExpressionList
import tripleo.elijah.ci.CiProcedureCallExpression
import tripleo.elijah.lang.i.ExpressionKind
import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.util2.UnintendedUseException

class CiProcedureCallExpressionImpl : CiProcedureCallExpression {
    private var _left: IExpression? = null
    private var expressionList: CiExpressionList = CiExpressionListImpl()

    /**
     * Get the argument list
     *
     * @return the argument list
     */
    override fun exprList(): CiExpressionList {
        return expressionList
    }

    override fun getExpressionList(): CiExpressionList {
        return expressionList
    }

    /**
     * change then argument list all at once
     *
     * @param ael the new value
     */
    override fun setExpressionList(ael: CiExpressionList) {
        expressionList = ael
    }

    override fun getKind(): ExpressionKind {
        return ExpressionKind.PROCEDURE_CALL
    }

    override fun setKind(aExpressionKind: ExpressionKind) {
        throw UnintendedUseException()
    }

    override fun getLeft(): IExpression {
        return _left!!
    }

    /**
     * @see .identifier
     */
    override fun setLeft(iexpression: IExpression) {
        _left = iexpression
    }

    override fun is_simple(): Boolean {
        throw UnintendedUseException()
    }

    /**
     * Set the left hand side of the procedure call expression, ie the method name
     *
     * @param xyz a method name might come as DotExpression or IdentExpression
     */
    override fun identifier(xyz: IExpression) {
        left = xyz
    }

    override fun printableString(): String {
        return String.format("%s%s", left, expressionListPrintableString())
    }

    private fun expressionListPrintableString(): String {
        return if (expressionList != null) expressionList.toString() else "()"
    }

    override fun repr_(): String {
        return "ProcedureCallExpression{%s %s}".formatted(left, expressionListPrintableString())
    }

    override fun toString(): String {
        return repr_()
    }

    override fun setArgs(aEl: CiExpressionList) {
        throw UnintendedUseException()
    }
}
