package tripleo.elijah.stages.deduce.nextgen

import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.lang.i.VariableStatement
import tripleo.elijah.stages.deduce.nextgen.DR_Ident.Understanding
import tripleo.elijah.stages.deduce.post_bytecode.DG_ClassStatement
import tripleo.elijah.stages.gen_fn.ProcTableEntry
import tripleo.elijah.stages.instructions.InstructionArgument

class DR_IdentUnderstandings {
    internal class BacklinkUnderstanding(private val ia: InstructionArgument) : Understanding {
        override fun asString(): String {
            return String.format("BacklinkUnderstanding %s", ia)
        }
    }

    internal class ClassUnderstanding(private val dcs: DG_ClassStatement) : Understanding {
        override fun asString(): String {
            return "ClassUnderstanding " + dcs.classInvocation()
        }
    }

    class ElementUnderstanding(val element: OS_Element) : Understanding {
        override fun asString(): String {
            var xx = element.toString()

            if (element is VariableStatement) {
                xx = element.name
            }

            return "ElementUnderstanding $xx"
        }
    }

    internal class PTEUnderstanding(@JvmField val pte: ProcTableEntry) : Understanding {
        override fun asString(): String {
            return String.format("PTEUnderstanding " + pte.__debug_expression)
        }
    }
}
