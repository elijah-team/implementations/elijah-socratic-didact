/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
/**
 * Created Mar 25, 2019 at 3:00:39 PM
 */
package tripleo.elijah.comp

import org.apache.commons.lang3.tuple.Pair
import tripleo.elijah.comp.i.ErrSink
import tripleo.elijah.diagnostic.Diagnostic
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon

/**
 * @author tripleo(sb)
 */
class StdErrSink : ErrSink {
    private var _errorCount = 0

    var _list: MutableList<Pair<ErrSink.Errors, Any>> = ArrayList()

    override fun errorCount(): Int {
        return _errorCount
    }

    override fun exception(e: Exception) {
        _errorCount++
        SimplePrintLoggerToRemoveSoon.println_err_4("exception: $e")
        e.printStackTrace(System.err)
        _list.add(Pair.of(ErrSink.Errors.EXCEPTION, e))
    }

    override fun info(message: String) {
        _list.add(Pair.of(ErrSink.Errors.INFO, message))
        System.err.printf("INFO: %s%n", message)
    }

    override fun list(): List<Pair<ErrSink.Errors, Any>> {
        return _list
    }

    override fun reportDiagnostic(diagnostic: Diagnostic) {
        if (diagnostic.severity() == Diagnostic.Severity.ERROR) _errorCount++
        _list.add(Pair.of(ErrSink.Errors.DIAGNOSTIC, diagnostic))
        // 08/13 diagnostic.report(System.err);
    }

    override fun reportError(s: String) {
        _errorCount++
        _list.add(Pair.of(ErrSink.Errors.ERROR, s))
        System.err.printf("ERROR: %s%n", s)
    }

    override fun reportWarning(s: String) {
        _list.add(Pair.of(ErrSink.Errors.WARNING, s))
        System.err.printf("WARNING: %s%n", s)
    }
} //
//
//

