package tripleo.elijah.stages.gen_c

import tripleo.elijah.stages.gen_c.Generate_Code_For_Method.AOG
import tripleo.elijah.stages.instructions.ConstTableIA
import tripleo.elijah.stages.instructions.Instruction
import tripleo.elijah.stages.instructions.IntegerIA
import tripleo.elijah.stages.instructions.Label

internal class GCFM_Inst_JE(private val generateCodeForMethod: Generate_Code_For_Method, private val gc: GenerateC,
                            private val gf: WhyNotGarish_BaseFunction, private val instruction: Instruction) {
    val text: String
        get() {
            val lhs = instruction.getArg(0)
            val rhs = instruction.getArg(1)
            val target = instruction.getArg(2)

            val realTarget = target as Label

            val vte = gf.getVarTableEntry((lhs as IntegerIA).index)
            assert(rhs != null)
            val s1: String
            val s2: String

            if (rhs is ConstTableIA) {
                val cte = gf.getConstTableEntry(rhs.index)
                val realTargetName = gc.getRealTargetName(gf, lhs, AOG.GET)
                s1 = (String.format("vsb = %s == %s;", realTargetName, gc.getAssignmentValue(gf.self, rhs, gf)))
                s2 = (String.format("if (!vsb) goto %s;", realTarget.name))
            } else {
                //
                // TODO need to lookup special __eq__ function
                //
                val realTargetName = gc.getRealTargetName(gf, lhs, AOG.GET)
                s1 = (String.format("vsb = %s == %s;", realTargetName, gc.getAssignmentValue(gf.self, rhs, gf)))
                s2 = (String.format("if (!vsb) goto %s;", realTarget.name))

                val y = 2
            }

            return String.format("%s%n%s%n", s1, s2)
        }
}
