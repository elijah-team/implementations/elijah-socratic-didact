package tripleo.elijah.stages.deduce

import tripleo.elijah.lang.i.NamespaceStatement
import tripleo.elijah.lang.i.OS_Element
import java.util.*

class DeduceElementWrapper(private val element: OS_Element) {
    val isNamespaceStatement: Boolean
        get() = element is NamespaceStatement

    fun element(): OS_Element {
        return element
    }

    override fun equals(obj: Any?): Boolean {
        if (obj === this) return true
        if (obj == null || obj.javaClass != this.javaClass) return false
        val that = obj as DeduceElementWrapper
        return this.element == that.element
    }

    override fun hashCode(): Int {
        return Objects.hash(element)
    }

    override fun toString(): String {
        return "DeduceElementWrapper[" +
                "element=" + element + ']'
    }
}
