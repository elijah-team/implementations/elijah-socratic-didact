package tripleo.elijah.world.impl

import org.jetbrains.annotations.Contract
import tripleo.elijah.g.GGarishClass
import tripleo.elijah.g.GGenerateResult
import tripleo.elijah.g.GGenerateResultSink
import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.stages.garish.GarishClass
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_fn.EvaClass
import tripleo.elijah.stages.gen_generic.GenerateFiles
import tripleo.elijah.stages.gen_generic.GenerateResult
import tripleo.elijah.stages.gen_generic.pipeline_impl.GenerateResultSink
import tripleo.elijah.world.i.LivingClass

class DefaultLivingClass(private val _gc: EvaClass) : LivingClass {
    private val _element: ClassStatement = _gc.klass
    private var _garish: GarishClass? = null

    private var _code = 0
    private var _generatedFlag = false

    override fun evaNode(): EvaClass {
        return _gc
    }

    override fun getCode(): Int {
        return _code
    }

    override fun setCode(aCode: Int) {
        _code = aCode
    }

    override fun getElement(): ClassStatement {
        return _element
    }

    @Contract(mutates = "this")
    override fun getGarish(): GGarishClass {
        if (_garish == null) {
            _garish = GarishClass(this)
        }

        return _garish!!
    }

    @Contract(mutates = "this")
    override fun generateWith(aResultSink: GGenerateResultSink,
                              aGarishClass: GGarishClass,
                              aGenerateResult: GGenerateResult,
                              aGenerateFiles: GenerateFiles) {
        if (_generatedFlag) {
            return
        }

        val generateC = aGenerateFiles as GenerateC
        val evaClass = evaNode()
        val garishClassGenerator = evaClass.generator() //new GarishClass_Generator(evaClass);

        garishClassGenerator.provide((aResultSink as GenerateResultSink),
                (aGarishClass as GarishClass),
                (aGenerateResult as GenerateResult),
                generateC)

        _generatedFlag = true
    }
}
