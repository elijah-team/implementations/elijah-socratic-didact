package tripleo.elijah.nextgen.model

interface SM_ClassDeclaration : SM_Node {
    fun classBody(): SM_ClassBody?

    fun inheritance(): SM_ClassInheritance?

    fun name(): SM_Name?

    fun subType(): SM_ClassSubtype?
}
