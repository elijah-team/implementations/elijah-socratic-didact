package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.IExpression
import tripleo.elijah.nextgen.outputstatement.EG_CompoundStatement
import tripleo.elijah.nextgen.outputstatement.EG_SingleStatement
import tripleo.elijah.nextgen.outputstatement.EG_Statement
import tripleo.elijah.nextgen.outputstatement.EX_Explanation
import tripleo.elijah.nextgen.outputstatement.EX_Explanation.Companion.withMessage
import tripleo.elijah.stages.deduce.post_bytecode.DeduceElement3_ProcTableEntry
import tripleo.elijah.stages.gen_fn.ProcTableEntry
import tripleo.elijah.stages.instructions.InstructionArgument

enum class _GF {
    ;

    internal interface __Pte_Dispatch {
        fun statementForExpression(expression: IExpression): EG_Statement

        fun statementForExpressionNum(expreesion_num: InstructionArgument): EG_Statement

        companion object {
            fun dispatch(pte: ProcTableEntry, xy: __Pte_Dispatch): EG_Statement {
                return if (pte.expression_num == null) {
                    xy.statementForExpression(pte.__debug_expression!!)
                } else {
                    xy.statementForExpressionNum(pte.expression_num)
                }
            }
        }
    }

    companion object {
        private fun forDeduceElement3_ProcTableEntry(
                de_pte: DeduceElement3_ProcTableEntry, gc: GenerateC): EG_Statement {
            val middle: EG_Statement
            val indent = false

            val pte = de_pte.tablePrincipal

            val gf = de_pte.generatedFunction
            val instruction = de_pte.instruction

            val sb = __Pte_Dispatch.dispatch(pte, object : __Pte_Dispatch {
                // README funny thing is, this is a class vv
                override fun statementForExpression(expression: IExpression): EG_Statement {
                    return __Pte_Dispatch_IExpression_Statement(expression, instruction!!, gf, gc)
                }

                override fun statementForExpressionNum(expression_num: InstructionArgument): EG_Statement {
                    return __Pte_Dispatch_InstructionArgument_Statement(expression_num, instruction!!, gf, gc)
                }
            })

            val beginning = EG_SingleStatement("",
                    withMessage("forDeduceElement3_ProcTableEntry >> beginning"))
            val ending = EG_SingleStatement("", withMessage("forDeduceElement3_ProcTableEntry >> ending"))
            val explanation: EX_Explanation = EX_ProcTableEntryExplanation(de_pte)
            middle = sb

            val stmt = EG_CompoundStatement(beginning, ending, middle, indent, explanation)
            return stmt
        }
    }
}
