package tripleo.elijah.stages.deduce.nextgen

import tripleo.elijah.lang.i.IdentExpression
import tripleo.elijah.lang.i.RegularTypeName
import tripleo.elijah.lang.i.TypeName
import tripleo.elijah.lang.nextgen.names.i.EN_Usage
import tripleo.elijah.lang.nextgen.names.impl.ENU_IsTypeName
import tripleo.elijah.stages.gen_fn.BaseEvaFunction
import tripleo.elijah.util.NotImplementedException

class DR_Type(private val evaFunction: BaseEvaFunction, aNonGenericTypeName: TypeName?) {
    private val nonGenericTypeName = aNonGenericTypeName as RegularTypeName?
    private var base: IdentExpression? = null

    fun addUsage(us: EN_Usage?) {
        base!!.name.addUsage(us)
    }

    fun build() {
        if (null == nonGenericTypeName) {
            NotImplementedException.raise()
        }

        if (nonGenericTypeName!!.realName.parts().size == 1) {
            base = nonGenericTypeName.realName.parts()[0]
            // base.getName().addUnderstanding(_inj().new_ENU_IsTypeName());
            base?.getName()?.addUnderstanding(ENU_IsTypeName())
        } else {
            throw NotImplementedException()
        }

        for (typeName in nonGenericTypeName.genericPart.p()) {
        }
    }
}
