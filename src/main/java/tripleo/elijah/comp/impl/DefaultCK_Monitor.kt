package tripleo.elijah.comp.impl

import tripleo.elijah.comp.graph.i.CK_Monitor
import tripleo.elijah.util2.UnintendedUseException

class DefaultCK_Monitor : CK_Monitor {
    override fun reportSuccess() {
        throw UnintendedUseException()
    }

    override fun reportFailure() {
        throw UnintendedUseException()
    }
}
