package tripleo.elijah.nextgen.outputstatement

import java.util.stream.Collectors

data class EG_SequenceStatement(

    val beginning: String?,
    val ending: String?,
    val list: List<EG_Statement>,
    val naming: EG_Naming?

) : EG_Statement {
//    constructor(EG_Naming: EG_Naming, EG_SingleStatements: List<EG_Statement>) : this(
//        beginning = null, ending = null, list = EG_SingleStatements, naming = EG_Naming
//    )

    fun _list(): List<EG_Statement> = list

    override val explanation: EX_Explanation
        get() = EX_Explanation_withMessage("EG_SequenceStatement")

    override val text: String
        get() {
            val ltext = String_join(" ", list.stream().map { it.text }.collect(Collectors.toList()))
            return if (beginning != null) {
                String.format("%s%s%s", beginning, ltext, ending)
            } else {
                String.format("%s", ltext)
            }
        }
}

fun mk_EG_SequenceStatement(aNaming: EG_Naming, aList: List<EG_Statement>): EG_SequenceStatement {
    return EG_SequenceStatement(null, null, aList, aNaming)
}

fun mk_EG_SequenceStatement(aNaming: EG_Naming, aNewBeginning: String?, aNewEnding: String?, aList: List<EG_Statement>): EG_SequenceStatement {
    return EG_SequenceStatement(aNewBeginning, aNewEnding, aList, aNaming)
}

fun mk_EG_SequenceStatement(aBeginning: String?, aEnding: String?, aList: List<EG_Statement>): EG_SequenceStatement {
    return EG_SequenceStatement(aBeginning, aEnding, aList, null)
}
