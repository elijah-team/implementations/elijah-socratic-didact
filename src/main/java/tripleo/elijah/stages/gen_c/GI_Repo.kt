package tripleo.elijah.stages.gen_c

import tripleo.elijah.lang.i.ClassStatement
import tripleo.elijah.lang.i.FunctionDef
import tripleo.elijah.lang.i.OS_Element
import tripleo.elijah.lang.i.PropertyStatement
import tripleo.elijah.lang.impl.VariableStatementImpl
import tripleo.elijah.stages.instructions.ProcIA

class GI_Repo(val generateC: GenerateC) {
    private val items: MutableMap<Any?, GenerateC_Item> = HashMap()

    fun itemFor(e: OS_Element?): GenerateC_Item? {
        if (items.containsKey(e)) return items[e]

        if (e is ClassStatement) {
            val gci = GI_ClassStatement(e, this)

            items[e] = gci

            return gci
        } else if (e is FunctionDef) {
            val gfd = GI_FunctionDef(e, this)

            items[e] = gfd

            return gfd
        } else if (e is PropertyStatement) {
            val gps = GI_PropertyStatement(e, this)

            items[e] = gps

            return gps
        } else if (e is VariableStatementImpl) {
            val gvs = GI_VariableStatement(e, this)

            items[e] = gvs

            return gvs
        }

        return null
    }

    fun itemFor(aProcIA: ProcIA): GI_ProcIA? {
        val gi_proc: GI_ProcIA?
        if (items.containsKey(aProcIA)) {
            gi_proc = items[aProcIA] as GI_ProcIA?
        } else {
            gi_proc = GI_ProcIA(aProcIA, generateC)
            items[aProcIA] = gi_proc
        }
        return gi_proc
    }
}
