package tripleo.elijah.nextgen.reactive

/**
 * Convenience class to:
 *
 * 1. Hold a list of [Reactivable]s
 * 2. Have them respond when `this` [Reactive]
 * gets notified that it is noticed by a [ReactiveDimension]
 */
abstract class DefaultReactive : Reactive {
    private val ables: MutableList<Reactivable> = ArrayList()

    override fun add(aReactivable: Reactivable) {
        ables.add(aReactivable)
    }

    override fun join(aDimension: ReactiveDimension?) {
        for (reactivable in ables) {
            reactivable.respondTo(aDimension)
        }
    }
}
